// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import { Enum } from "typescript-string-enums";

export const ENUM_CODE_DeliveryStatus = Enum("NE", "OR", "PI", "PC", "FD", "CO" );
export type ENUM_CODE_DeliveryStatus = Enum<typeof ENUM_CODE_DeliveryStatus>;

enum ENUM_ID_DeliveryStatus{
  New = 1,
  OrderReceived = 2,
  PartialIncomplete = 3,
  PartialComplete = 4,
  FullDelivered = 5,
  CompleteOrder = 6,
}

enum ENUM_ID_USER_TYPE{
  TelephoenicAdmin = 1,
  VendorAdmin = 2,
  PosUser = 3,
  SalesSupervisorUser=4,
  SalesAgentUser=5,
  WarehouseAdmin=6,
  DeliveryUser=7

}

enum ENUM_CODE_USER_TYPE{
  TelephoenicAdmin = "TA",
  VendorAdmin = "VA",
  PosUser ="PU",
  SalesSupervisorUser="SU",
  SalesAgentUser="SA",
  WarehouseAdmin="WA",
  DeliveryUser="DU",
}


export const environment = {
  ENUM_ID_USER_TYPE,
  ENUM_CODE_USER_TYPE,
  ENUM_ID_DeliveryStatus,
  DATE_FORMAT: (v: Date) => new Date(v).toDateString(),
  production: false,
  baseURL: "http://localhost:8080/orders360-web/",
  //baseURL: "http://192.168.1.59:8585/orders360-web/",
  // baseURL: "http://192.168.1.59:8686/orders360-web/",
  maxListFieldOptions: 50,
  initialViewRowSize:25
};