import { BaseFormComponent } from "../../common/form/base-form-component";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Warehouse } from "../warehouse-model";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { WarehouseService } from "../service/warehouse.service";
import { NgForm } from "@angular/forms";
import { AuthService } from "../../auth/service/auth.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { City } from "../../location/city/City.model";
import { CityService } from "../../location/city/service/city.service";
import { Region } from "../../location/region/Region.model";
import { RegionService } from "../../location/region/service/region.service";
import { VendorService } from "../../vendor/Services/vendor.service";
import { Vendor } from "../../vendor/vendor.model";

@Component({
    selector: 'app-warehouse',
    templateUrl: './warehouse-form.component.html',
    styleUrls: ['./warehouse-form.component.css']
})
export class WarehouseFormComponent extends BaseFormComponent implements OnInit {
    user: any;
    _isEdit: boolean = false;
    _vendorId: number;
    listOfCities: City[];
    listOfRegions: Region[];
    warehouse: Warehouse;
    @ViewChild("warehouseForm") warehouseForm: NgForm;
    constructor(protected warehouseService: WarehouseService,
        protected authService: AuthService,
        protected vendorAdminService: VendorAdminService,
        protected cityService: CityService,
        protected vendorService: VendorService,
        protected regionService: RegionService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected snackBar: MatSnackBar) {
        super(warehouseService, route, router, snackBar);
    }

    ngOnInit() {
        this.getVendorIdByLoggedVenodrAdmin();
        if (this.route.snapshot.queryParams["edit"] == '1') {
            this._isEdit = true;
            this.warehouse = this.warehouseService.warehouse;
            this.onCityChange(this.warehouse.region.city.id);
        } else {
            this.warehouse = new Warehouse();
            this.warehouse.name = '';
        }

    }
    getVendorIdByLoggedVenodrAdmin() {
        this.user = this.authService.getCurrentLoggedInUser();
        this.vendorAdminService.getVendorByVendorAdminId(this.user.id)
            .subscribe((vendorId: number) => {
                console.log(vendorId);
                this._vendorId = vendorId;
                this.vendorService.findVendorById(this._vendorId).subscribe(
                    (data: Vendor) => {
                        this.warehouse.vendor = data;
                    });
                this.loadCities();
            });

    }

    loadCities() {

        this.cityService.findCityByVendorCountry(this._vendorId).subscribe(
            data => {
                console.log(data)
                this.listOfCities = data;
            });
        console.log(this.listOfCities);

    }

    loadRegions(listOfCitesIds: number[]) {
        this.regionService.findByCityIdWhereIn(listOfCitesIds).subscribe(
            data => {
                this.listOfRegions = data;
            }
        )
    }

    onCityChange(event) {
        let citiesIds: number[] = [];
        if (event.value == undefined && this.isEdit) {
            this.cityService.getCityById(this.warehouse.region.city.id).subscribe((data: City) => {
                this.warehouse.region.city = data;
            });
            citiesIds.push(this.warehouse.region.city.id)

        } else {
            this.warehouse.region = new Region();
            this.cityService.getCityById(event.value).subscribe((data: City) => {
                this.warehouse.region.city = data;
            });
            citiesIds.push(event.value);
        }
        this.loadRegions(citiesIds);
    }

    onRegionChange(event) {
        this.regionService.getRegionById(event.value).subscribe((data: Region) => {
            this.warehouse.region = data;
        });
    }

    warehouseNameChange(event){
        this.warehouse.name = event.target.value.trim();
    }

    warehouseAddressChange(event){
        this.warehouse.address = event.target.value.trim();
    }

    onSaveCompleted() {
        super.onSaveCompleted();
        this.snackBar.open("Adding Warehouse successfully","close",{duration:3000});
       
    }

    onSaveAndNewCompleted(){
        super.onSaveAndNewCompleted();
        this.getVendorIdByLoggedVenodrAdmin();
        this.warehouse.region = new Region();
        this.listOfRegions = [];
    }


    get isEdit(): boolean {
        return this._isEdit;
    }

    set isEdit(isEdit: boolean) {
        this._isEdit = isEdit;
    }

    get model(): Warehouse {
        return this.warehouse;
    }
    set model(warehouse: Warehouse) {
        this.warehouse = warehouse;
    }
    initModel(): Warehouse {
        return new Warehouse();
    }
    ngFormInstance(): NgForm {
        return this.warehouseForm;
    }
}  