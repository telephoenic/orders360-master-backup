import { Vendor } from "../vendor/vendor.model";
import { Region } from "../location/region/Region.model";
import { City } from "../location/city/City.model";


export class Warehouse{
    
    id: number;
    inactive: boolean;
    name: string;
    address: string;
    vendor: Vendor;
    region: Region;
    constructor(id?: number,name?: string, address?:string,city?:City){
        this.id = id;
        this.name = name;
        this.address = address;
        this.region = new Region();
    }
}