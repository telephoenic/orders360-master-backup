import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Warehouse } from '../warehouse-model';
import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';
import { BaseModel } from '../../common/model/base-model';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class WarehouseService extends BaseService {

    warehouse: Warehouse;
    navigateToAfterCompletion: string;

    constructor(http: HttpClient,
        protected authService: AuthService) {
        super(http, "warehouse", authService)
    }

    public getWarehouses(pageNumber: number, pageSize: number, vendorId: number,
        searchTerm: string, sortBy: string, sortOrder: string): Observable<any> {
        return this.http.get(environment.baseURL + "warehouse/get-warehouses/" + pageNumber + "/"
            + pageSize + "/" + vendorId,
            {
                params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
                headers: this.getBasicHeaders()
            });
    }

    public getAllWarehousesForWarehouseAdmin(vendorId: number): Observable<any> {
        return this.http.get(environment.baseURL + "warehouse/get-warehouses-for-user/" + vendorId);
    }

    public findWarehouseByVendorId(vendorId: number): Observable<any> {
        return this.http.get(environment.baseURL + "warehouse/find_by_vendorId/" + vendorId)
    }

    public getSelectedWarehouses(warehouseAdminId: number): Observable<any> {
        return this.http.get(environment.baseURL + "warehouse/get-warehouses-selected/" + warehouseAdminId);
    }

    public findWarehouseByWarehouseUser(userId: number): Observable<any> {
        return this.http.get(environment.baseURL + "warehouse/find_by_warehouse_user/" + userId)
    }


    get model() {
        return this.warehouse;
    }
    set model(model: BaseModel) {
        this.warehouse = <Warehouse>model;
    }


}