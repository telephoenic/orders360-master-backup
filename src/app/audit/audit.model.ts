
export class Audit{

	createdBy: string;
	updatedBy: string;
	createdDate: Date;
    updatedDate: Date;
    

    constructor() {}
}