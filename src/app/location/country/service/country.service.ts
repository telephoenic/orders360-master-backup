import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../../common/service/base.service';
import { AuthService } from '../../../auth/service/auth.service';
import { BaseModel } from '../../../common/model/base-model';
import { Injectable } from '@angular/core';
import { Country } from '../country.model';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable()
export class CountryService extends BaseService {
    country:Country;
    navigateToAfterCompletion: string;
    constructor(http: HttpClient, protected authService:AuthService) {
        super(http, "country", authService)
      }
    
      findCountries<T extends BaseModel>(): Observable<T> {
        return this.http.get<T>(environment.baseURL+"country/all");
      }
    
      get model(){
        return this.country;
      }
      
      set model(model:BaseModel) {
        this.country = <Country>model;
      }
    

}