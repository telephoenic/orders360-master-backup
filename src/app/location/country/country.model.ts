import { BaseModel } from "../../common/model/base-model";

export class Country extends BaseModel {
    inactive: boolean;
    id: number;
    code: string;
    name: string;
    countryCode: string;
    mobileCode: string;
    constructor() {
        super();
    }

}