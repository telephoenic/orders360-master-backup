import { City } from "../city/City.model";
import { BaseModel } from "../../common/model/base-model";

export class Region extends BaseModel {
    
    id: number;
    code: string;
    name: string;
    city: City = new City();
    inactive:boolean;
}