import { Injectable, ViewChild } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { MatSort } from '@angular/material';
import { TdDataTableSortingOrder } from '@covalent/core';
import { HttpClient } from '@angular/common/http';
import { Region } from '../Region.model';
import { BaseService } from '../../../common/service/base.service';
import { AuthService } from '../../../auth/service/auth.service';
import { BaseModel } from '../../../common/model/base-model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class RegionService extends BaseService {
  region:Region;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient, protected authService:AuthService) {
    super(http, "region", authService)
  }

  countByNameAndCityIdEquals(regionName : string, cityId : number){
    return this.http.get(environment.baseURL+"region/countByAreaName/" + regionName + "/" + cityId);
  }

  countByNameAndCityIdEqualsAndIdNot(regionName : string, cityId : number,id : number){
    return this.http.get(environment.baseURL+"region/countByAreaName/" + regionName + "/" + cityId +"/" + id);
  }

  searchFilter(searchTerm : string,pageNumber  : number,pageSize : number){
    return this.http.get<Region>(environment.baseURL+"region/search/"+pageNumber+"/"+pageSize ,
    { params: this.prepareSearchParameters(searchTerm,'',''), 
      headers: this.getBasicHeaders()});
  }

  findByCityIdWhereIn(listOfCitiesIds:number[]): Observable<any>{
    return this.http.post(environment.baseURL + "region/regions-in-cities-vendor-country",listOfCitiesIds);
  }

  getRegionById(regionId: number) {
    return this.http.get(environment.baseURL + "region/get-region-by-id/" + regionId);
  }

  getNotSelectedRegionsForDeliveryUsers(citiesIds:number[],deliveryUserId:number): Observable<any>{
    citiesIds.push(deliveryUserId);
    return this.http.post(environment.baseURL + "region/regions-not-for-delivery-user",citiesIds);
  }

  getSelectedRegionsForDeliveryUsers(deliveryUserId:number): Observable<any>{
    return this.http.get(environment.baseURL + "region/get-regions-for-delivery-users/" + deliveryUserId);
  }
  get model(){
    return this.region;
  }
  
  set model(model:BaseModel) {
    this.region = <Region>model;
  }
}