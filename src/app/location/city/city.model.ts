import { BaseModel } from "../../common/model/base-model";
import { Country } from "../country/country.model";

export class City extends BaseModel {
    
    id:number;
    inactive:boolean;
    code: string;
    name: string;
    country:Country;

    constructor();
    constructor(id: number, name: string);
    constructor(id?: number, name?: string) {
        super();
        this.id = id;
        this.name = name;
    }
}