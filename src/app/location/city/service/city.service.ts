import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { City } from '../City.model';
import { BaseService } from '../../../common/service/base.service';
import { AuthService } from '../../../auth/service/auth.service';
import { environment } from '../../../../environments/environment';
import { BaseModel } from '../../../common/model/base-model';
import { Observable } from 'rxjs';
import { Industry } from '../../../common/industry/industry.model';

@Injectable()
export class CityService extends BaseService {
  navigateToAfterCompletion: string;
  city: City;

  constructor(protected http: HttpClient,
    protected authService: AuthService) {

    super(http, "city", authService);
  }

  findCities(countryId: any): Observable<City> {
    return this.http.get<City>(environment.baseURL + "city/findByCountryId/" + countryId);
  }

  findAllCiityByCountryId(countryId: any): Observable<any> {
    return this.http.get<any>(environment.baseURL + "city/findByCountryId/" + countryId);
  }


  findCityByVendorId(vendorId: number): Observable<any> {
    return this.http.get<any>(environment.baseURL + "city/get-city-by-vendor/" + vendorId);
  }

  findByName(name: string) {
    return this.http.get<City>(environment.baseURL + "city/findByName/" + name);
  }

  countByCityName(name: string, countryId: number, id: number, code: string) {
    let parameterList: { paramName: string, paramValue: any }[] = [];
    parameterList.push({ paramName: "city_code", paramValue: code });
    return this.http.get(environment.baseURL + "city/countByCityName/" + name + "/" + countryId + "/" + id,
      {
        params: this.prepareSearchParameters('', '', '', (parameterList)),
        headers: this.getBasicHeaders()
      });
  }

  countByCityNameAndCountryIdEquals(name: string, countryId: number, code: string) {
    let parameterList: { paramName: string, paramValue: any }[] = [];
    parameterList.push({ paramName: "city_code", paramValue: code });

    return this.http.get(environment.baseURL + "city/countByCityName/" + name + "/" + countryId,
      {
        params: this.prepareSearchParameters('', '', '', (parameterList)),
        headers: this.getBasicHeaders()
      });
  }

  searchFilter(searchTerm: string, pageNumber: number, pageSize: number) {
    return this.http.get<City>(environment.baseURL + "city/search/" + pageNumber + "/" + pageSize,
      {
        params: this.prepareSearchParameters(searchTerm, '', ''),
        headers: this.getBasicHeaders()
      });
  }

  getActiveCities(): Observable<City[]> {
    return this.http.get<City[]>(environment.baseURL + "city/active-cities");
  }

  findCityByVendorCountry(vendorId: number): Observable<City[]> {
    return this.http.get<City[]>(environment.baseURL + "city/get-city-by-vendor/" + vendorId)
  }

  getCityById(cityId: number) {
    return this.http.get(environment.baseURL + "city/get-city-by-id/" + cityId);
  }

  get model() {
    return this.city;
  }

  set model(model: City) {
    this.city = <City>model;
  }
}
