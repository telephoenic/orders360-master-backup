import { BaseModel } from "../common/model/base-model";

export class Priority extends BaseModel{
    id:number;
    code:string;
    description:string;
    inactive: boolean;
}