import { Injectable } from '@angular/core';
import { Priority } from './priority';
import { BaseService } from '../common/service/base.service';
import { Http } from '@angular/http';
import { BaseModel } from '../common/model/base-model';
import { AuthService } from '../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PriorityService extends BaseService{
  navigateToAfterCompletion: string;
  priority:Priority;

  constructor(protected http:HttpClient,
    protected authService:AuthService) { 
    super(http, "priority", authService);
  }

  get model() {
    return this.priority;
  }

  set model(model:Priority) {
    this.priority = <Priority> model;
  }
}
