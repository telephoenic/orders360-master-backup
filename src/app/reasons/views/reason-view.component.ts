import { BaseViewComponent } from "../../common/view/base-view-component";
import { TdDataTableSortingOrder, TdPagingBarComponent, TdDataTableComponent, ITdDataTableColumn, TdDialogService } from "@covalent/core";
import { OnInit, Component, ViewChild, Inject } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { MatSnackBar, MAT_DIALOG_DATA, MatDialogRef, MatDialog } from "@angular/material";
import { AuthService } from "../../auth/service/auth.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { environment } from "../../../environments/environment";
import { OrderRejectedReasonService } from "../services/reason-service";
import { OrderRejectedReason } from "../reason.model";
import { User } from "../../auth/user.model";
import { BaseFormComponent } from "../../common/form/base-form-component";
import { NgForm, FormControl, Validators } from "@angular/forms";
import { VendorService } from "../../vendor/Services/vendor.service";
import { Vendor } from "../../vendor/vendor.model";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
    selector: 'reason-view',
    templateUrl: '../../reasons/views/reason-view.component.html',
    styleUrls: ['../../reasons/views/reason-view.component.css']
})
export class ViewOrderRejectedReason extends BaseViewComponent implements OnInit {

    private _currentPage: number = 1;
    private _fromRow: number = 1;

    private _allReasons: OrderRejectedReason[] = [];
    private _errors: any = '';
    private _selectedRows: OrderRejectedReason[] = [];
    private _pageSize: number = 50;
    private _filteredTotal: number;
    private _sortBy: string = '';
    private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
    private _searchTerm: string = '';

    @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
    @ViewChild('dataTable') dataTable: TdDataTableComponent;

    constructor(protected orderRejectedReasonService: OrderRejectedReasonService,
        protected router: Router,
        protected dialogService: TdDialogService,
        protected matDialog: MatDialog,
        protected snackBar: MatSnackBar,
        protected authService?: AuthService,
        protected vendorAdminService?: VendorAdminService) {
        super(orderRejectedReasonService, router, dialogService, snackBar, authService, vendorAdminService);
    }

    columnsConfig: ITdDataTableColumn[] = [
        { name: 'name', label: 'Name', tooltip: "Name" },
        { name: 'parent.name', label: 'Type', tooltip: 'Type' },
        { name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive", width: 100 },
        { name: 'id', label: 'Edit', tooltip: "Edit", sortable: false, width: 100 }
    ];


    ngOnInit() {
        super.ngOnInit();
    }

    filterByVendor(pageNumber: number,
        pageSize: number,
        searchTerm: string,
        sortBy: string,
        sortOrder: string,
        vendorId: number) {
        this.getReasons();
    }

    public getReasons() {
        let currentUser = this.vendorAdminService.getCurrentLoggedInUser();
        let vendorId;

        if (currentUser.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin) {
            this.vendorAdminService.getVendorByVendorAdminId(currentUser.id)
                .subscribe(result => {
                    vendorId = result;
                    this.orderRejectedReasonService.getReasons(this.currentPage - 1, this.pageSize, vendorId, this.searchTerm, this.sortBy, this.sortOrder)
                        .subscribe(data => {
                            this._allReasons = data['content'];
                            this.filteredTotal = data['totalElements'];
                        })
                });

        }
    }

    openDialog(row: OrderRejectedReason) {
        let reasonDialog = this.matDialog.open(OrderRejectedReasonDialog, {
            width: '500px',
            disableClose: true,
            data: row
        });
        reasonDialog.afterClosed().subscribe(() => {
            this.getReasons();
        });
    }

    onAdd() {
        this.openDialog(new OrderRejectedReason());
    }

    onEdit(id: number, row: OrderRejectedReason) {
        this.orderRejectedReasonService.orderRejectedReason = row;
        this.openDialog(row);
    }

    getPagingBar(): TdPagingBarComponent {
        return this.pagingBar;
    }
    getDataTable(): TdDataTableComponent {
        return this.dataTable;
    }

    public get currentPage(): number {
        return this._currentPage;
    }

    public set currentPage(value: number) {
        this._currentPage = value;
    }

    public get fromRow(): number {
        return this._fromRow;
    }

    public set fromRow(value: number) {
        this._fromRow = value;
    }

    public get allModels(): OrderRejectedReason[] {
        return this._allReasons;
    }

    public set allModels(value: OrderRejectedReason[]) {
        this._allReasons = value;
    }

    public get errors(): any {
        return this._errors;
    }

    public set errors(value: any) {
        this._errors = value;
    }

    public get selectedRows(): OrderRejectedReason[] {
        return this._selectedRows;
    }

    public set selectedRows(value: OrderRejectedReason[]) {
        this._selectedRows = value;
    }

    public get pageSize(): number {
        return this._pageSize;
    }

    public set pageSize(value: number) {
        this._pageSize = value;
    }

    public get filteredTotal(): number {
        return this._filteredTotal;
    }

    public set filteredTotal(value: number) {
        this._filteredTotal = value;
    }

    public get sortBy(): string {
        return this._sortBy;
    }

    public set sortBy(value: string) {
        this._sortBy = value;
    }

    public get sortOrder(): TdDataTableSortingOrder {
        return this._sortOrder;
    }

    public set sortOrder(value: TdDataTableSortingOrder) {
        this._sortOrder = value;
    }
    public get searchTerm(): string {
        return this._searchTerm;
    }

    public set searchTerm(value: string) {
        this._searchTerm = value;
    }
}

@Component({
    selector: 'reason-dialog',
    templateUrl: 'reason-dialog.html',
})
export class OrderRejectedReasonDialog extends BaseFormComponent implements OnInit {

    @ViewChild("reasonDialogForm") reasonDialogForm: NgForm;

    private _isEdit: boolean = false;
    private _loggedUser: User;
    private _orderRejectedReason: OrderRejectedReason;
    public dialogTitle: string;
    parentTemp: OrderRejectedReason;
    listOfParents: OrderRejectedReason[] = [];
    _vendorId: number;

    constructor(protected orderRejectedReasonService: OrderRejectedReasonService,
        protected route: ActivatedRoute,
        public dialogReason: MatDialogRef<OrderRejectedReasonDialog>,
        @Inject(MAT_DIALOG_DATA) public data: OrderRejectedReason,
        protected router: Router,
        protected snackBar: MatSnackBar,
        protected dialogService: TdDialogService,
        protected vendorService: VendorService,
        protected vendorAdminService: VendorAdminService,
        protected authService: AuthService) {

        super(orderRejectedReasonService,
            route,
            router,
            snackBar);
    }

    ngOnInit() {
        if (this.data.id != null) {
            this._isEdit = true;
            this.dialogTitle = "Edit Reason";
        } else {
            this.data.parent = new OrderRejectedReason();
            this.dialogTitle = "Add Reason";
        }
        this._orderRejectedReason = this.data;
        this.getVendorIdByLoggedVenodrAdmin();
    }

    getVendorIdByLoggedVenodrAdmin() {
        this._loggedUser = this.authService.getCurrentLoggedInUser();
        this.vendorAdminService.getVendorByVendorAdminId(this._loggedUser.id)
            .subscribe((vendorId: number) => {
                console.log(vendorId);
                this._vendorId = vendorId;
                this.vendorService.findVendorById(this._vendorId).subscribe(
                    (data: Vendor) => {
                        this.data.vendor = data;
                    },
                    (error) =>{
                        console.log(error);
                    },
                    ()=>{
                       this.getParents();
                    });
            });

    }


    onParentSelectionChange(parent: OrderRejectedReason) {
        this.data.parent = parent;
    }

    getParents() {
        this.orderRejectedReasonService.getParents().subscribe(data => {
            this.listOfParents = data;
        });
    }


    onSave() {
        this.baseService.navigateToAfterCompletion = this.router.url;
        super.onSave();
    }

    onSaveCompleted() {
        this.dialogReason.close();
        this.snackBar.open("Adding reject reason successfully.", "Close", { duration: 3000 });
    }
    onEditCompleted() {
        this.dialogReason.close();
    }
    onNoClick(): void {
        this.dialogReason.close();
    }

    changeParent(event) {
        let parent: OrderRejectedReason;
        for (let i = 0; i < this.listOfParents.length; i++) {
            parent = this.listOfParents[i];
            console.log(parent.id)
            if (parent.id == this.data.parent.id) {
                break;
            }
        }
    }


    onSaveAndNewCompleted() {
        this.reset();
        super.onSaveAndNewCompleted();
    }

    reset() {
        this.data.parent = new OrderRejectedReason();
        this._orderRejectedReason = this.data;
        this.getVendorIdByLoggedVenodrAdmin();
    }

    get model(): OrderRejectedReason {
        return this._orderRejectedReason;
    }
    set model(orderRejectedReason: OrderRejectedReason) {
        this._orderRejectedReason = orderRejectedReason;
    }
    initModel(): OrderRejectedReason {
        return new OrderRejectedReason();
    }
    ngFormInstance(): NgForm {
        return this.reasonDialogForm;
    }

    get isEdit(): boolean {
        return this._isEdit;
    }
    set isEdit(isEdit: boolean) {
        this._isEdit = isEdit;
    }

}
