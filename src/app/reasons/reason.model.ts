import { Vendor } from "../vendor/vendor.model";

export class OrderRejectedReason{
    
    id: number;
    name: string;
    code: string
    vendor:Vendor;
    inactive:boolean;
    parent: OrderRejectedReason;
    
    constructor (){
        this.vendor = new Vendor();
    }

}