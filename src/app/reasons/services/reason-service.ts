import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';
import { BaseModel } from '../../common/model/base-model';
import { Observable } from 'rxjs/Observable';
import { OrderRejectedReason } from '../reason.model';


@Injectable()
export class OrderRejectedReasonService extends BaseService {

    orderRejectedReason: OrderRejectedReason;
    navigateToAfterCompletion: string;

    constructor(http: HttpClient,
        protected authService: AuthService) {
        super(http, "order-rejected-reason", authService)
    }

    public getReasons(pageNumber: number, pageSize: number, vendorId: number, searchTerm: string, sortBy: string, sortOrder: string) :Observable<any> {
       return this.http.get(environment.baseURL + "order-rejected-reason/reasons/" + pageNumber + "/" + pageSize + "/" + vendorId, {
            params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
            headers: this.getBasicHeaders()
        })
    }

    public getParents():Observable<any> {
        return this.http.get(environment.baseURL + "order-rejected-reason/get-parents");
    }

    findRejctedReasonByCode(code: string): Observable<any> {
        return this.http.get(environment.baseURL + "order-rejected-reason/find_reason_by_code/" + code)
    }

    findRejctedReasonByParentId(parentId: number, vendorId: number): Observable<any> {
        return this.http.get(environment.baseURL + "order-rejected-reason/find_reason_by_parentId/" + parentId + "/" + vendorId)
    }
    //by Code and vendorId
    findRejctedReasonByParentIdByCode(cod: string, vendorId: number): Observable<any> {
        return this.http.get(environment.baseURL + "order-rejected-reason/find_reasons_by_parent/" + cod + "/" + vendorId)
    }


    get model() {
        return this.orderRejectedReason;
    }
    set model(model: BaseModel) {
        this.orderRejectedReason = <OrderRejectedReason>model;
    }


}