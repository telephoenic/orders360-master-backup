import { AppComponent } from "./app.component";
import * as Stomp from "stompjs";
import * as SockJS from "sockjs-client";
import { environment } from "../environments/environment";
import { MessageService } from "primeng/api";
import { Injectable } from "@angular/core";
import { NotificationService } from "./notification/notification-service.service";
import { VendorAdminService } from "./vendor-admin/service/vendor-admin.service";
import { Router, NavigationEnd } from "@angular/router";

@Injectable()
export class WebSocketAPI {
  private stompClient;
  disabled = true;
  message: any = "omar";
  notifications: any[] = [];
  counter :number = 0;
  constructor() {
    this.connect();
  }
  setConnected(connected: boolean) {
    this.disabled = !connected;
  }

  // Open connection with the back-end socket
  public connect() {
    const socket = new SockJS(environment.baseURL + "socket");
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      _this.stompClient.connected = true;
      _this.stompClient.subscribe("/topic/receiver", function(notifiaction) {
        let listOfNumbers: any[] = [];
        notifiaction.body.split(",").forEach(obj => {
          listOfNumbers.push(obj);
        });
        if (
          !isNaN(listOfNumbers[1]) &&
          localStorage.getItem("vendorId") == listOfNumbers[0]
        ) {
          AppComponent.countNotifications = listOfNumbers[1];
        } else {
          AppComponent.countNotifications = listOfNumbers[1];
        }
        AppComponent.setCountNotification(listOfNumbers[1]);
        _this.notifications.push(notifiaction.body);
      });
    });
    return this.stompClient;
  }

  disconnect() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }

    this.setConnected(false);
    console.log("Disconnected!");
  }

  /*showGreeting(message) {
        this.greetings.push(message);
      }
    */
  sendName() {
    this.stompClient.send(
      "/app/notifiaction",
      {},
      JSON.stringify({ message: this.message })
    );
  }
}
