import { Component, OnInit, ViewChild, Inject, Input } from '@angular/core';
import { BrandService } from '../../service/brand.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TdDialogService, TdDataTableSortingOrder, TdPagingBarComponent, TdDataTableComponent, ITdDataTableColumn } from '@covalent/core';
import { AuthService } from '../../../auth/service/auth.service';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { environment } from '../../../../environments/environment';
import { Brand } from '../../brand-model';
import { User } from '../../../auth/user.model';
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';
import { NgForm } from '@angular/forms';
import { NgxGalleryComponent, NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { HttpErrorResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { BaseFormComponent } from '../../../common/form/base-form-component';
import { NgxPermissionsService } from 'ngx-permissions';
import { RelatedProductDialogComponent } from '../related-product-dialog/related-product-dialog.component';

@Component({
  selector: 'app-view-brands',
  templateUrl: './view-brands.component.html',
  styleUrls: ['./view-brands.component.css']
})
export class ViewBrandsComponent extends BaseViewComponent implements OnInit {


  private _errors: any = '';
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private _searchTerm: string = '';
  private _pageSize: number = environment.initialViewRowSize;
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _isEdit: boolean = true;
  private _allBrands: Brand[] = [];
  vendorId: number;
  @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
  @ViewChild('allBrandsDt') allBrandsDt: TdDataTableComponent;

  private _selectedRows: Brand[] = [];
  brand: Brand = new Brand();

  columnsConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name', tooltip: "Name" },
    { name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive", width: 100 },
    { name: 'id', label: 'Edit', tooltip: "Edit", sortable: false, width: 100 }
  ];

  constructor(protected brandService: BrandService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected authService: AuthService,
    protected snackBar: MatSnackBar,
    protected matDialog: MatDialog,
    protected permissionsService: NgxPermissionsService,
    protected vendorAdminService: VendorAdminService
  ) {
    super(brandService, router, dialogService, snackBar);
  }

  ngOnInit() {
    super.ngOnInit();
    this.permissionsService.hasPermission("ROLE_BRAND_EDIT").then((result: boolean) => {
      if (!result) {
        for (let i = 0; i < this.allBrandsDt.columns.length; i++) {
          if (this.allBrandsDt.columns[i].name == 'id') {
            this.allBrandsDt.columns.splice(i, 1);
          }
        }
      }
    });

    this.permissionsService.hasPermission("ROLE_BRAND_ACTIVATE_DEACTIVATE_PER_ROW").then((result: boolean) => {
      if (!result) {
        for (let i = 0; i < this.allBrandsDt.columns.length; i++) {
          if (this.allBrandsDt.columns[i].name == 'inactive') {
            this.allBrandsDt.columns.splice(i, 1);
          }
        }
      }
    });

  }


  filterByVendor(pageNumber: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    vendorId: number) {
    this.brandService.getAllBrandsByVendorId(pageNumber, pageSize, searchTerm, sortBy,
      sortOrder, vendorId)
      .subscribe(data => {
        this._allBrands = data["content"],
          this.filteredTotal = data["totalElements"]
      });
  }

  onDeleteFailed(errorResponse: HttpErrorResponse) {     
    this.snackBar.open(errorResponse.error["message"], "More", { duration: 10000 })
      .onAction()
      .subscribe(() => this.showDetails(errorResponse));

  }

  showDetails(errorResponse: HttpErrorResponse) {    
    let relatedProductsObject: any[] = errorResponse.error['extraInfo']['BRAND_RELATED_PRODUCTS'];    
    this.matDialog.open(RelatedProductDialogComponent,
      {
        data: {
          relatedProducts: relatedProductsObject
        }
      });

  }

  onActivateDeactivePerRow(value, row: Brand) {
    this.baseService.changeStatusTo(value, [row.id])
      .subscribe(data => {
        row.inactive = !value;
        this.getDataTable().refresh();
      },
        (errorResponse: HttpErrorResponse) => {
          if (value) {
            super.onActivationFailed(errorResponse);
          } else {
            this.onDeactivationFailed(errorResponse);
          }
        }
      );
  }

  onActivateDeactivateSelectedRows(activate: boolean) {
    if (this.selectedRows.length > 0) {
      this.dialogService.openConfirm({
        message: "Are you sure you want to " + (activate ? "activate" : "deactivate") + " the selected row(s)?",
        title: "confirmation",
        cancelButton: "Disagree",
        acceptButton: "Agree"
      })
        .afterClosed().subscribe((accept: boolean) => {
          if (accept) {
            let selectedSystemAdminsIDs: number[] = [];

            this.selectedRows.forEach(systemAdmin => {
              selectedSystemAdminsIDs.push(systemAdmin.id);
            });

            this.baseService.changeStatusTo(activate, selectedSystemAdminsIDs).subscribe(
              data => {
                this.filter(this.currentPage - 1, this.pageSize);
                this.snackBar.open((activate ? "Activated" : "Deactivated") + " successfully", "Close", { duration: 3000 });
                this.selectedRows = [];
              },
              (errorResponse: HttpErrorResponse) => {
                if (activate) {
                  super.onActivationFailed(errorResponse);
                } else {
                  this.onDeactivationFailed(errorResponse);
                }
              }
            )
          }
        });
    } else if (this.allModels.length == 0) {
      this.onNoDataFoundToActivateOrDeactivate(activate);
    } else {
      this.onNoSelectedRowsToActivateOrDeactivate(activate);
    }
  }

  onDeactivationFailed(errorResponse: HttpErrorResponse) {
    this.filter(this.currentPage - 1, this.pageSize);
    this.snackBar.open(errorResponse.error["message"], "More", { duration: 10000 })
      .onAction()
      .subscribe(() => this.showDetails(errorResponse));
  }
  onAdd() {
    this.openDialog(new Brand());
  }

  onEdit(id: number, row: Brand) {
    this.brandService.brand = row;
    this.openDialog(row);
  }

  openDialog(row: Brand) {
    let brandDialog = this.matDialog.open(BrandDialog, {
      width: '500px',
      disableClose: true,
      data: row
    });
    brandDialog.afterClosed().subscribe(() => {
      this.filter(this.currentPage - 1, this.pageSize);
    });
  }



  public get selectedRows(): Brand[] {
    return this._selectedRows;
  }
  public set selectedRows(value: Brand[]) {
    this._selectedRows = value;
  }

  getDataTable(): TdDataTableComponent {
    return this.allBrandsDt;
  }

  public get allModels(): Brand[] {
    return this._allBrands;
  }

  public set allModels(value: Brand[]) {
    this._allBrands = value;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }

  public get errors(): any {
    return this._errors;
  }
  public set errors(value: any) {
    this._errors = value;
  }
  public get filteredTotal(): number {
    return this._filteredTotal;
  }
  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }
  public get sortBy(): string {
    return this._sortBy;
  }
  public set sortBy(value: string) {
    this._sortBy = value;
  }
  public get pageSize(): number {
    return this._pageSize;
  }
  public set pageSize(value: number) {
    this._pageSize = value;
  }
  public get currentPage(): number {
    return this._currentPage;
  }
  public set currentPage(value: number) {
    this._currentPage = value;
  }
  public get fromRow(): number {
    return this._fromRow;
  }
  public set fromRow(value: number) {
    this._fromRow = value;
  }
  public get searchTerm(): string {
    return this._searchTerm;
  }
  public set searchTerm(value: string) {
    this._searchTerm = value;
  }
  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }
  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }
  public get isEdit(): boolean {
    return this._isEdit;
  }
  public set isEdit(value: boolean) {
    this._isEdit = value;
  }

}

@Component({
  selector: 'brand-dialog',
  templateUrl: 'brand-dialog.html',
})
export class BrandDialog extends BaseFormComponent implements OnInit {


  @ViewChild("brandDialogForm") brandDialogForm: NgForm;
  @ViewChild("ngxGallery") ngxGallery: NgxGalleryComponent;


  @Input()
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[] = [];

  // attImageAsFile: File = null;
  imageUrl: string = null;

  attImagesAsArray: File[] = [];
  imageUrlsArray: string[] = [];

  private _isEdit: boolean = false;
  private _loggedUser: User;
  private _brand: Brand;
  public dialogTitle: string;

  constructor(protected brandService: BrandService,
    protected route: ActivatedRoute,
    public dialogBrand: MatDialogRef<BrandDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Brand,
    protected router: Router,
    protected snackBar: MatSnackBar,
    protected dialogService: TdDialogService,
    protected vendorAdminService: VendorAdminService,
    protected authService: AuthService) {

    super(brandService,
      route,
      router,
      snackBar);
  }

  ngOnInit() {

    if (this.data.id != null) {
      this._isEdit = true;
      this.dialogTitle = "Edit Brand";
    } else {
      this.dialogTitle = "Add Brand";
    }
    this._loggedUser = this.authService.getCurrentLoggedInUser();
    this._brand = this.data;
    this.fillImages();
    this.initGalleryOptions();

  }


  onNoClick(): void {
    this.dialogBrand.close();
  }

  onSelectImages(selectedImage: File) {

    this.resetAttachmentsFields();
    if (selectedImage instanceof File && selectedImage.type.startsWith("image/")) {
      // this.attImageAsFile = selectedImage;
      this.attImagesAsArray[0] = selectedImage;
      let fileReader = new FileReader();
      fileReader.onload = () => {
        let image = new Image();
        image.src = fileReader.result;
        this.ngxGallery.images.push({
          small: image.src,
          medium: image.src,
          big: image.src
        });
      }
      fileReader.readAsDataURL(selectedImage);
    }
    else {
      this.snackBar.open("File type not allowed images allowed only.",
        "Close",
        { duration: 4000 });
    }
  }

  resetAttachmentsFields() {
    // this.attImageAsFile = null;
    this.attImagesAsArray = [];
    this.ngxGallery.images = [];
  }

  onDeleteImage() {
    this.dialogService.openConfirm({
      message: "Are you sure you want to delete the selected image?",
      title: "Confirmation",
      cancelButton: "Disagree",
      acceptButton: "Agree"
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        // this.attImageAsFile = null;
        this.attImagesAsArray.splice(this.ngxGallery.selectedIndex, 1);
        this.ngxGallery.images.splice(this.ngxGallery.selectedIndex, 1);
      }
    });
  }

  onSaveWithAttachments() {
    this.vendorAdminService.getVendorByVendorAdminId(this._loggedUser.id).subscribe(vendorId => {      
      this._brand.name = this.data.name;
      this._brand.vendor.id = vendorId;
      this.brandService.brand = this._brand;
      // this.attImageAsFile = this.attImagesAsArray[0];
      this.validateSelectedImages();
      this.baseService.navigateToAfterCompletion = this.router.url;
      super.onSaveWithAttachments(this.attImagesAsArray);
    });
  }

  onSaveAndNewWithAttachments() {
    this.vendorAdminService.getVendorByVendorAdminId(this._loggedUser.id).subscribe(vendorId => {
      this._brand.name = this.data.name;
      this._brand.vendor.id = vendorId;
      this.brandService.brand = this._brand;
      // this.attImageAsFile = this.attImagesAsArray[0];
      this.validateSelectedImages();
      this.baseService.navigateToAfterCompletion = this.router.url;
      super.onSaveAndNewWithAttachments(this.attImagesAsArray);
    });
  }

  onChange(event: any): void {
    event.srcElement.value = "";
  }


  fillImages() {

    if (this._brand.imageName != null) {
      let imgURL = this.brandService.getAttachmentUrl(this._brand.imageName);
      this.imageUrlsArray.push(imgURL);
      this.brandService.getAttachment(imgURL).subscribe((blob: Blob) => {
        let fileReader = new FileReader();
        fileReader.onload = () => {
          this.attImagesAsArray.push(new File([fileReader.result], this._brand.imageName));
        };
        let img = new Image();
        img.src = imgURL;

        this.galleryImages.push({
          small: imgURL,
          medium: imgURL,
          big: imgURL
        });
        if (blob) {
          fileReader.readAsArrayBuffer(blob);
        }
      });
    }
  }


  initGalleryOptions() {
    this.galleryOptions = [
      {
        previewInfinityMove: true,
        imageArrowsAutoHide: true,
        imageInfinityMove: true,
        previewCloseOnEsc: true,
        previewKeyboardNavigation: true,
        previewZoom: true,
        width: '400px',
        height: '300px',
        thumbnails: false,
        imageAnimation: NgxGalleryAnimation.Slide,
        imageSize: 'contain'
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '400px',
        imagePercent: 80
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];
  }

  onSaveWithAttachmentsError(errorResponse: HttpErrorResponse) {
    this.snackBar.open(errorResponse.error.message, "Close", { duration: 3000 });
  }

  onSaveWithAttachmentsCompleted() {
    this.dialogBrand.close();
    this.snackBar.open("Saved successfully", "Close", { duration: 3000 });
    this.resetAttachmentsFields();
  }

  onSaveAndNewWithAttachmentsCompleted(){
    this.reset();
  }


  onEditWithAttachmentsCompleted() {
    this.dialogBrand.close();
    this.snackBar.open("Saved successfully", "Close", { duration: 3000 });
    this.resetAttachmentsFields();
  }

  validateSelectedImages() {    
    if (this.attImagesAsArray == null || this.attImagesAsArray.length == 0) {
      this.snackBar.open("Brand image is required.", "Close", { duration: 3000 })
      throw new Error("Brand image is required");
    }
  }

  reset() {
    this._brand = new Brand();
    this.attImagesAsArray = [];
    this.ngxGallery.images = [];
  }

  get model(): Brand {
    return this._brand;
  }
  set model(brand: Brand) {
    this._brand = brand;
  }
  initModel(): Brand {
    return new Brand();
  }
  ngFormInstance(): NgForm {
    return this.brandDialogForm;
  }

  get isEdit(): boolean {
    return this._isEdit;
  }
  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

}
