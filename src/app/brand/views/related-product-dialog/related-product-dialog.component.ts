import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-related-product-dialog',
  templateUrl: './related-product-dialog.component.html',
  styleUrls: ['./related-product-dialog.component.css']
})
export class RelatedProductDialogComponent implements OnInit {

  ngOnInit(): void {
    
  }

   constructor(@Inject(MAT_DIALOG_DATA) public data:any) { }

  

}
