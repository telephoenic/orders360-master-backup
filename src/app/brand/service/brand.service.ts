import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';

import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Brand } from '../brand-model';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { BaseModel } from '../../common/model/base-model';

@Injectable()
export class BrandService extends BaseService {

  brand: Brand;
  navigateToAfterCompletion: string;

  get model() {
    return this.brand;
  }

  set model(model:BaseModel) {
    this.brand = <Brand>model;
  }

  constructor(protected http: HttpClient,
    protected authService: AuthService) {
    super(http, "brand", authService);
  }

  getAllBrandsByVendorId(pageNum: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    vendorId: number): Observable<any> {
    return this.http.get(environment.baseURL + "brand/get-all-brands/" + pageNum + "/" + pageSize + "/" + vendorId,
      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      });
  }


}
