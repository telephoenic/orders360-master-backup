import { Vendor } from "../vendor/vendor.model";
import { BaseModel } from "../common/model/base-model";

export class Brand extends BaseModel {

    id: number;
    inactive: boolean;
    name: string;
    imageName: string;
    vendor: Vendor;

    constructor() {
        super();
        this.vendor = new Vendor();
    }
}