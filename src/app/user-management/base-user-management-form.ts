import { BaseFormComponent } from "../common/form/base-form-component";
import { BaseModel } from "../common/model/base-model";
import { UserPrivilegeProfileComponent } from "../user-privilege/views/user-privilege-profile.component";
import { UserPrivilegeProfileAware } from "./user-privilege-profile-aware";
import { BaseViewComponent } from "../common/view/base-view-component";
import { AfterViewInit } from "@angular/core";
import { User } from "../auth/user.model";
import { throwError } from "rxjs";

export abstract class BaseUserManagementForm extends BaseFormComponent implements AfterViewInit {

    selectedSecreen: string;
    loggedUser: User;

    ngAfterViewInit() {
        if (this.isEdit) {
            this.getSelectedPrivilegeProfiles().filterPrivilegeProfileForUser(this.model.id, false, this.selectedSecreen);
            this.getNotSelectedPrivilegeProfiles().filterPrivilegeProfileForUser(this.model.id, true, this.selectedSecreen);
                
        } else {
            this.getNotSelectedPrivilegeProfiles().filterPrivilegeProfileForUser(this.loggedUser.id, null, this.selectedSecreen);
        }
    }

    onSelectingPrivilegeProfile(event) {
        if (event.movedAsAssigned) {
            this.pushTo(this.getSelectedPrivilegeProfiles(), event);
            this.removeFrom(this.getNotSelectedPrivilegeProfiles(), event);
        } else {
            this.pushTo(this.getNotSelectedPrivilegeProfiles(), event);
            this.removeFrom(this.getSelectedPrivilegeProfiles(), event);
        }

        this.getSelectedPrivilegeProfiles().dataTable.refresh();
        this.getNotSelectedPrivilegeProfiles().dataTable.refresh();
    }

    pushTo(userPrivilegeProfiles: BaseViewComponent, event) {
        if (event.rows) {
            for (let i = 0; i < event.rows.length; i++) {
                userPrivilegeProfiles.allModels.push(event.rows[i]);
            }
        }
    }

    removeFrom(userPrivilegeProfiles: BaseViewComponent, event) {
        if (event.rows) {
            for (let i = 0; i < event.rows.length; i++) {
                userPrivilegeProfiles.allModels.splice(userPrivilegeProfiles.allModels.indexOf(event.rows[i]), 1);
            }
        }
    }

    onBeforeSave(userModel: UserPrivilegeProfileAware) {
        debugger;
        this.showSpinner = true;
        this.fillSelectedPrivilegeProfiles(userModel);
    }

    onBeforeSaveAndNew(userModel: UserPrivilegeProfileAware) {
        this.showSpinnerOnSaveAndNew = true;
        this.fillSelectedPrivilegeProfiles(userModel);
    }
    onBeforeEdit(userModel: UserPrivilegeProfileAware) {
        this.showSpinner = true;
        this.fillSelectedPrivilegeProfiles(userModel);
    }

    fillSelectedPrivilegeProfiles(userModel: UserPrivilegeProfileAware) {
        userModel.privilegeProfiles = this.getSelectedPrivilegeProfiles().allModels;
        console.log(userModel.privilegeProfiles.length)
        if(userModel.privilegeProfiles.length === 0){
            this.showSpinner = false;
            this.showSpinnerOnSaveAndNew = false;
            this.snackBar.open("POS must be have at least one profile privilege.","Close",{duration: 3000})
            throw new Error("POS must be have at least one profile privilege.");

        }
        console.log(this.showSpinner);
    }

    abstract getSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent;
    abstract getNotSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent;
}