import { UserPrivilegeProfile } from "../user-privilege/user-privilege-profiles";
import { BaseModel } from "../common/model/base-model";

export abstract class UserPrivilegeProfileAware extends BaseModel {
    abstract get privilegeProfiles():UserPrivilegeProfile[];

    abstract set privilegeProfiles(userPrivilegeProfile: UserPrivilegeProfile[]);
}