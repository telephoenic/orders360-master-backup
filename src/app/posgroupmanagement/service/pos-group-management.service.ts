import { BaseModel } from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { PosGroupManagement } from '../pos-group-management.model';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class PosGroupManagementService extends BaseService {
  posGroupManagement: PosGroupManagement;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService) {
    super(http, "pos_group", authService);
  }



  getAllGroupsByVendorId(pageNum: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string): Observable<any> {
    return this.http.get(environment.baseURL + "pos_group/groups_for_vendor/" + pageNum + "/" + pageSize + "/" + vendorId,
      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      });
  }
  getGroupsByVednor(vendorId: number): Observable<any> {
    return this.http.get(environment.baseURL + "pos_group/allPosGroups/" + vendorId)
  }

  getAllGroups() : Observable<any>{

    return this.http.get(environment.baseURL + "pos_group/allPosGroups");
  }

  getPosGroupForProduct(productId:number) : Observable<any>{
    return this.http.get(environment.baseURL + "pos_group/product-pos-group/get-product-pos-group/" + productId);
  }

  get model() {
    return this.posGroupManagement;
  }

  set model(model: BaseModel) {
    this.posGroupManagement = <PosGroupManagement>model;
  }

}