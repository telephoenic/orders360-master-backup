import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgForm, Validators} from '@angular/forms';
import { MatSnackBar} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { AuthService } from '../../auth/service/auth.service';
import { User } from '../../auth/user.model';
import { VendorService } from '../../vendor/Services/vendor.service';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';
import { PosGroupManagement } from '../pos-group-management.model';
import { PosGroupManagementService } from '../service/pos-group-management.service';

@Component({
  selector: 'app-pos-group-management-form',
  templateUrl: './pos-group-management-form.component.html',
  styleUrls: ['./pos-group-management-form.component.css']
})
export class PosGroupManagementFormComponent extends BaseFormComponent implements OnInit {

  protected loggedUser: User;
  @ViewChild("posGroupManagementForm") posGroupManagementForm : NgForm;  
  _posGroupManagement : PosGroupManagement;

  @Input()
  _isEdit:boolean = false;
  vendorId:number;

  constructor(protected posGroupManagementService: PosGroupManagementService,
              protected vendorAdminService: VendorAdminService,
              protected route:ActivatedRoute,
              protected authService: AuthService,
              protected router:Router,
              protected snackBar:MatSnackBar) { 

    super(posGroupManagementService, 
      route,
      router,
      snackBar);
  }

  ngOnInit() {
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    if(this.route.snapshot.queryParams["edit"] == '1') {
      this._isEdit = true;
      this._posGroupManagement = this.posGroupManagementService.posGroupManagement;
    } 
    else {
      this._posGroupManagement = new PosGroupManagement();
    }

    this.getVendor();
  }

  onBeforeSave(posGroup:PosGroupManagement){
    this.showSpinner = true;
    this.getVendor();
    this._posGroupManagement.vendor.id = this.vendorId;
  }
  onBeforeSaveAndNew(posGroup:PosGroupManagement){
    this.showSpinnerOnSaveAndNew = true;
    this.getVendor();
    this._posGroupManagement.vendor.id = this.vendorId;
  }
  getVendor(){
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(
      vendorId => { this.vendorId = vendorId;
      }
        );

  }

  onMinOrderChange(event){
    console.log("here")
    this._posGroupManagement.minOrder = event;
  }

  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
  
  get model() : PosGroupManagement {
    return this._posGroupManagement;
  }

  set model(posGroupManagement:PosGroupManagement){
    this._posGroupManagement = posGroupManagement;
  }
  
  initModel(): PosGroupManagement {
    return new PosGroupManagement();
  }

  ngFormInstance(): NgForm {
    return this.posGroupManagementForm;
  }
}
