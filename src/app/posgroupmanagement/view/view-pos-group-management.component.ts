import {VendorAdminService} from '../../vendor-admin/service/vendor-admin.service';
import { TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import {
		 TdPagingBarComponent, TdDialogService,
		TdDataTableComponent
} from '@covalent/core';
import { Component, ViewChild, OnInit } from '@angular/core';
import {
	MatSnackBar
} from "@angular/material";
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { BaseViewComponent } from '../../common/view/base-view-component';
import { User } from '../../auth/user.model';
import { PosGroupManagement } from '../pos-group-management.model';
import { PosGroupManagementService } from '../service/pos-group-management.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'app-view',
	templateUrl: './view-pos-group-management.component.html',
	styleUrls: ['./view-pos-group-management.component.css']
})
export class ViewPosGroupManagementComponent extends BaseViewComponent implements OnInit {

	private _currentPage: number = 1;
	private _fromRow: number = 1;
	private user: User;
	private _allPosGroups: PosGroupManagement[] = [];
	private _errors: any = '';
	private _selectedRows: PosGroupManagement[] = [];
	private _pageSize: number;
	private _filteredTotal: number;
	private _sortBy: string = '';
	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
	private _searchTerm: string = '';

	@ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
	@ViewChild('dataTable') dataTable: TdDataTableComponent;



	columnsConfig: ITdDataTableColumn[] = [
		{ name: 'name', label: 'Name',width: 1100 ,tooltip:"Name" },
		{ name: 'id', label: 'Edit', tooltip:"Edit" }
	];

	constructor(protected posGroupManagementService: PosGroupManagementService,
		protected router: Router,
		protected authService: AuthService,
		protected dialogService: TdDialogService,
		protected snackBar: MatSnackBar,
		protected vendorAdminService: VendorAdminService,
		protected permissionsService: NgxPermissionsService) {
		super(posGroupManagementService, router, dialogService, snackBar);
	}


	filterByVendor(pageNumber: number, pageSize: number,
		searchTerm: string, sortBy: string, sortOrder: string, vendorId: number) {
		this.posGroupManagementService.getAllGroupsByVendorId(pageNumber, pageSize, vendorId, searchTerm, sortBy, sortOrder).subscribe(
			data => {
				this.allModels = data["content"];
				this.filteredTotal = data["totalElements"]
			}
		);
	}

	onDeleteFailed(errorResponse:HttpErrorResponse) {
		this.snackBar.open("Process cannot be completed, the selected option is in use.","close",{ duration:3000 });
	}

	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}
	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

	public get currentPage(): number {
		return this._currentPage;
	}

	public set currentPage(value: number) {
		this._currentPage = value;
	}

	public get fromRow(): number {
		return this._fromRow;
	}

	public set fromRow(value: number) {
		this._fromRow = value;
	}

	public get allModels(): PosGroupManagement[] {
		return this._allPosGroups;
	}

	public set allModels(value: PosGroupManagement[]) {
		this._allPosGroups = value;
	}

	public get errors(): any {
		return this._errors;
	}

	public set errors(value: any) {
		this._errors = value;
	}

	public get selectedRows(): PosGroupManagement[] {
		return this._selectedRows;
	}

	public set selectedRows(value: PosGroupManagement[]) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string {
		return this._sortBy;
	}

	public set sortBy(value: string) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder) {
		this._sortOrder = value;
	}

	public get searchTerm(): string {
		return this._searchTerm;
	}

	public set searchTerm(value: string) {
		this._searchTerm = value;
	}
}
