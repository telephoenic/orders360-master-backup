import { Vendor } from "../vendor/vendor.model";

export class PosGroupManagement {
    
    id: number;
    name:string;
    inactive:boolean;
    vendor:Vendor;
    minOrder:number;
    
    constructor (){
        this.vendor = new Vendor();
    }
    

}