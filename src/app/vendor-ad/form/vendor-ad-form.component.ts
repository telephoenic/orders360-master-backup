import { DatePipe } from "@angular/common";
import { Component, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { MatSnackBar, NativeDateAdapter } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { TdDialogService } from "@covalent/core";
import {
  NgxGalleryAnimation,
  NgxGalleryComponent,
  NgxGalleryImage,
  NgxGalleryOptions
} from "ngx-gallery";
import { environment } from "../../../environments/environment";
import { User } from "../../auth/user.model";
import { BaseFormComponent } from "../../common/form/base-form-component";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { Vendor } from "../../vendor/vendor.model";
import { VendorAdService } from "../services/vendor-ad.service";
import { VendorAd } from "../vendor-ad.model";

@Component({
  selector: "app-vendor-ad-form",
  templateUrl: "./vendor-ad-form.component.html",
  styleUrls: ["./vendor-ad-form.component.css"]
})
export class VendorAdFormComponent extends BaseFormComponent implements OnInit {
  @ViewChild("vendorAdForm") vendorAdForm: NgForm;
  @ViewChild("ngxGallery") ngxGallery: NgxGalleryComponent;

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[] = [];
  today = new Date();

  attImagesAsFiles: File[] = [];
  imageUrlsArray: string[] = [];
  startDate: Date;
  _isEdit: boolean = false;
  vendorAd: VendorAd;

  originStartDate: Date;
  originEndDate: Date;
  dateChanged: boolean;

  startDateChanged: boolean;

  startDateMillisecond;
  todayMillisecond;

  endDateChanged: boolean;

  minStartDate: Date;

  strDate = 'Mon Nov 07 2016 09:44:12 GMT+0530';

  constructor(
    protected vendorAdService: VendorAdService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected snackBar: MatSnackBar,
    protected vendorAdminService: VendorAdminService,
    protected dialogService: TdDialogService,
    private datePipe: DatePipe
  ) {
    super(vendorAdService, route, router, snackBar);
    this.vendorAd = new VendorAd();
    this.today.setHours(0, 0, 0, 0);
  }

  ngOnInit() {
    debugger;
    if (this.route.snapshot.queryParams["edit"] == "1") {
      this._isEdit = true;
      this.vendorAd = this.vendorAdService.vendorAd;

      this.vendorAd.startDate = new Date(
        this.datePipe.transform(this.vendorAd.startDate, "mediumDate")
      );
      if (this.vendorAd.endDate != null) {
        this.vendorAd.endDate = new Date(
          this.datePipe.transform(this.vendorAd.endDate, "mediumDate")
        );
      }
    } else {
      this.vendorAd = new VendorAd();
    }

    this.getVendorByVendorAdmin();
    this.initGalleryOptions();
    this.fillImages();

    debugger;
    if (this.isEdit) {
      this.originStartDate = this.vendorAd.startDate;
      this.originEndDate = this.vendorAd.endDate;

      this.startDateMillisecond = this.originStartDate.getTime();
      this.todayMillisecond = this.today.getTime();

      if (this.todayMillisecond < this.startDateMillisecond) {
        this.minStartDate = this.today;
      } else {
        this.minStartDate = this.originStartDate;
      }
    } else {
      this.minStartDate = this.today;
    }
  }

  getVendorByVendorAdmin() {
    debugger;
    let user: User = this.vendorAdminService.getCurrentLoggedInUser();
    if (user.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin) {
      this.vendorAdminService
        .getVendorByVendorAdminId(user.id)
        .subscribe(data => {
          this.vendorAd.vendor = new Vendor();
          this.vendorAd.vendor.id = data;
        });
    }
  }

  onSaveWithAttachmentsCompleted() {
    this.showSuccessfulMsg();
  }

  onSaveAndNewWithAttachmentsCompleted() {
    this.showSuccessfulMsg();
    this.clearImage();
    this.getVendorByVendorAdmin();
  }

  onEditWithAttachmentsCompleted() {
    console.log("here");
    this.showSuccessfulEditMsg();
    this.clearImage();
  }

  showSuccessfulMsg() {
    this.snackBar.open("Adding adv successfully.", "close", {
      duration: 3000
    });
  }

  showSuccessfulEditMsg() {
    this.snackBar.open("Saved Successfully", "close", {
      duration: 3000
    });
  }

  pushAttachment(event) {
    debugger;
    if (this.attImagesAsFiles.length >= 1) {
      this.attImagesAsFiles = [];
    }
    this.attImagesAsFiles.push(event);
  }

  onDeleteImage() {
    debugger;
    this.dialogService
      .openConfirm({
        message: "Are you sure you want to delete the selected image?",
        title: "Confirmation",
        cancelButton: "Disagree",
        acceptButton: "Agree"
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.clearImage();
        }
      });
  }

  clearImage() {
    this.attImagesAsFiles.splice(this.ngxGallery.selectedIndex, 1);
    this.ngxGallery.images.splice(this.ngxGallery.selectedIndex, 1);
  }

  onBeforeSave(vendorAd: VendorAd) {
    this.validateSelectedImages();
    this.vendorAd._name = this.vendorAd._name.trim();
    // this.validateDuplicateVendorAdName();
  }

  onBeforeSaveAndNew(vendorAd: VendorAd) {
    debugger;
    this.validateSelectedImages();
    this.vendorAd._name = this.vendorAd._name.trim();
    // this.validateDuplicateVendorAdName();
  }

  onBeforeEdit(vendorAd: VendorAd) {
    this.validateSelectedImages();
    // this.validateDuplicateVendorAdName();
  }

  onBeforeSaveWithAttachments(vendorAd: VendorAd, files: File | FileList) {
    this.validateSelectedImages();
    this.vendorAd._name = this.vendorAd._name.trim();
    // this.validateDuplicateVendorAdName();
  }

  onBeforeSaveAndNewWithAttachments(
    vendorAd: VendorAd,
    files: File | FileList
  ) {
    this.validateSelectedImages();
    this.vendorAd._name = this.vendorAd._name.trim();
  }

  onBeforeEditWithAttachments(vendorAd: VendorAd, files: File | FileList) {
    this.validateSelectedImages();
  }

  validateSelectedImages() {
    if (this.attImagesAsFiles == null || this.attImagesAsFiles.length == 0) {
      this.snackBar.open(
        "At least one image required for the Advertisements.",
        "Close",
        {
          duration: 3000
        }
      );
      throw new Error("At least one image required for the ad");
    }
  }

  onChange(event: any): void {
    event.srcElement.value = "";
  }

  onSelectImages(selectedImage: File) {
    debugger;
    if (
      selectedImage instanceof File &&
      selectedImage.type.startsWith("image/")
    ) {
      this.attImagesAsFiles[0] = selectedImage;
      let fileReader = new FileReader();
      fileReader.onload = () => {
        let image = new Image();
        image.src = fileReader.result;
        this.ngxGallery.images.push({
          small: image.src,
          medium: image.src,
          big: image.src
        });
      };
      fileReader.readAsDataURL(selectedImage);
    } else {
      this.snackBar.open(
        "File type not allowed images allowed only.",
        "Close",
        { duration: 4000 }
      );
    }
  }

  focusOutVendorAdName() {
    debugger;
    if (this.vendorAd._name != null) {
      this.vendorAd._name = this.vendorAd._name.trim();
    }
  }

  fillImages() {
    debugger;
    if (this._isEdit) {
      let imageNamesArr = this.vendorAd.imagesName.split(";");

      if (imageNamesArr != null) {
        imageNamesArr.forEach(imageName => {
          let imgURL = this.vendorAdService.getAttachmentUrl(imageName);
          this.imageUrlsArray.push(imgURL);

          this.vendorAdService.getAttachment(imgURL).subscribe((blob: Blob) => {
            let fileReader = new FileReader();
            fileReader.onload = () => {
              this.attImagesAsFiles.push(
                new File([fileReader.result], imageName)
              );
            };
            let img = new Image();
            img.src = imgURL;

            this.galleryImages.push({
              small: imgURL,
              medium: imgURL,
              big: imgURL
            });
            if (blob) {
              fileReader.readAsArrayBuffer(blob);
            }
          });
        });
      }
    }
  }

  initGalleryOptions() {
    this.galleryOptions = [
      {
        previewInfinityMove: true,
        imageArrowsAutoHide: true,
        imageInfinityMove: true,
        previewCloseOnEsc: true,
        previewKeyboardNavigation: true,
        previewZoom: true,
        width: "400px",
        height: "300px",
        thumbnails: false,
        imageAnimation: NgxGalleryAnimation.Slide,
        imageSize: "contain"
      },
      {
        breakpoint: 800,
        width: "100%",
        height: "400px",
        imagePercent: 80
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];
  }

  startDateValueChange() {
    debugger;
    if (this.originStartDate != this.vendorAd.startDate) {
      this.dateChanged = true;
    } else if (!this.endDateChanged) {
      this.dateChanged = false;
    }
  }

  endDateValueChange() {
    if (this.originEndDate != this.vendorAd.endDate) {
      this.dateChanged = true;
    } else if (!this.startDateChanged) {
      this.dateChanged = false;
    }
  }

  get model(): VendorAd {
    return this.vendorAd;
  }

  set model(vendorAd: VendorAd) {
    this.vendorAd = vendorAd;
  }

  initModel(): VendorAd {
    return new VendorAd();
  }

  ngFormInstance(): NgForm {
    return this.vendorAdForm;
  }

  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
}

export const MY_FORMATS = {
  parse: {
    dateInput: "MM/YYYY"
  },
  display: {
    dateInput: "MM/YYYY",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY"
  }
};
