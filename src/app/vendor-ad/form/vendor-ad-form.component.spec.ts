import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorAdFormComponent } from './vendor-ad-form.component';

describe('VendorAdFormComponent', () => {
  let component: VendorAdFormComponent;
  let fixture: ComponentFixture<VendorAdFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorAdFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorAdFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
