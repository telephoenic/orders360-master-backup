import { TestBed, inject } from '@angular/core/testing';

import { VendorAdService } from './vendor-ad.service';

describe('VendorAdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VendorAdService]
    });
  });

  it('should be created', inject([VendorAdService], (service: VendorAdService) => {
    expect(service).toBeTruthy();
  }));
});
