import { Injectable } from "@angular/core";
import { BaseService } from "../../common/service/base.service";
import { VendorAd } from "../vendor-ad.model";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../../auth/service/auth.service";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class VendorAdService extends BaseService {
  vendorAd: VendorAd;
  static readonly serviceURL = "vendor_ad";

  get model(): VendorAd {
    return this.vendorAd;
  }
  set model(model: VendorAd) {
    this.vendorAd = <VendorAd>model;
  }

  constructor(http: HttpClient, protected authService: AuthService) {
    super(http, VendorAdService.serviceURL, authService);
  }

  getAllAdsByVendorId(pageNumber: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string): Observable<any> {
    return this.http.get(environment.baseURL + VendorAdService.serviceURL + "/ads_for_vendor/" + pageNumber + "/" +
      pageSize + "/" +
      vendorId, {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      });
  }
}
