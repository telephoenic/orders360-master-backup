import { Vendor } from "../vendor/vendor.model";
import { BaseModel } from "../common/model/base-model";

export class VendorAd extends BaseModel {
  id: number;
  inactive: boolean;
  vendor: Vendor;
  name: string;
  imagesName: string = "";
  startDate: Date;
  endDate?: Date;

  public set _name(name: string) {
    if (name != null) {
      name = name.trim();
    }

    this.name = name;
  }

  public get _name() {
    if (this.name != null) {
      this.name = this.name.trim();
    }

    return this.name;
  }
}
