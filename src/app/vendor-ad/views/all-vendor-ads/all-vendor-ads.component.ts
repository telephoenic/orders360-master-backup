import { VendorAd } from "./../../vendor-ad.model";
import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';
import { BaseViewComponent } from "../../../common/view/base-view-component";
import {
  TdDataTableSortingOrder,
  TdPagingBarComponent,
  TdDataTableComponent,
  TdDialogService,
  ITdDataTableColumn,
} from "@covalent/core";
import { VendorAdService } from "../../services/vendor-ad.service";
import { Router, ActivatedRoute } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { AuthService } from "../../../auth/service/auth.service";
import { DatePipe } from "@angular/common";
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';
import { NgxPermissionsService } from 'ngx-permissions';
const DECIMAL_FORMAT: (v: any) => any = (v: number) => v.toFixed(2);

@Component({
  selector: "app-vendor-ad-all",
  templateUrl: "./all-vendor-ads.component.html",
  styleUrls: ["./all-vendor-ads.component.css"]
})


export class VendorAdAllComponent extends BaseViewComponent implements OnInit {
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  public _allAds: VendorAd[] = [];
  private _errors: any = "";
  private _selectedRows: VendorAd[] = [];
  public _pageSize: number = 50;
  private _filteredTotal: number;
  private _sortBy: string = "";
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = "";
  private pageNumber : number = 1;
  private vendorId:number ;

  @ViewChild("pagingBar") pagingBar: TdPagingBarComponent;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;

  columnsConfig: ITdDataTableColumn[] = [
    { name: "name", label: "Name" ,tooltip: "Name"},
    { name: "startDate", label: "Start Date", sortable: true, format: v => this.datePipe.transform(v, "mediumDate")},
    { name: "endDate", label: "End Date", format: v => this.datePipe.transform(v, "mediumDate")},
    { name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive", width: 100},
    { name: "id", label: "Edit", sortable: false, width: 100 }
  ];

  constructor(
      protected vendorAdService: VendorAdService,
      protected vendorAdminService: VendorAdminService,
      protected router: Router,
      protected dialogService: TdDialogService,
      protected snackBar: MatSnackBar,
      protected authService: AuthService,
      protected route: ActivatedRoute,
      protected permissionsService: NgxPermissionsService,
      protected datePipe: DatePipe
  ) {
    super(vendorAdService, router, dialogService, snackBar);
  }

  ngOnInit() {
    super.ngOnInit();
    this.permissionsService.hasPermission("ROLE_AD_EDIT").then((result: boolean) => {
      if (!result) {
        for (let i = 0; i < this.dataTable.columns.length; i++) {
          if (this.dataTable.columns[i].name == 'id') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });

    this.permissionsService.hasPermission("ROLE_AD_ACTIVATE_DEACTIVATE_PER_ROW").then((result: boolean) => {
      if (!result) {
        for (let i = 0; i < this.dataTable.columns.length; i++) {
          if (this.dataTable.columns[i].name == 'inactive') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });
  }

  filterByVendor(pageNumber: number,
                  pageSize: number,
                  searchTerm: string,
                  sortBy: string,
                  sortOrder: string,
                  vendorId: number) {
                    this.vendorId = vendorId;
                    this.pageNumber = pageNumber;
    this.vendorAdService.getAllAdsByVendorId(pageNumber, pageSize, vendorId, searchTerm, sortBy, sortOrder).subscribe(
      data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"]
      }
    );
  }

  onActivationFailed(errorResponse:HttpErrorResponse) {
    debugger;
    this.snackBar.open(errorResponse.error.message, "close", {duration: 3000});
    this.vendorAdService.getAllAdsByVendorId(this.pageNumber, this.pageSize, this.vendorId, '', '', '').subscribe(
      data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"]
      }
    );

  }

  onActivateDeactivePerRow(value, row:VendorAd) {
      this.baseService.changeStatusTo(value, [row.id])
                      .subscribe( data => {
                                      row.inactive = !value;
                                      this.getDataTable().refresh();
                                  },
                                  (errorResponse:HttpErrorResponse) => {
                                      if(value) {
                                          row.inactive = value;
                                          this.getDataTable().refresh();
                                          this.onActivationFailed(errorResponse);
                                      } else {
                                          this.onDeactivationFailed(errorResponse);
                                      }
                                  }
                              );
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get allModels(): VendorAd[] {
    return this._allAds;
  }

  public set allModels(value: VendorAd[]) {
    this._allAds = value;
  }

  public get errors(): any {
    return this._errors;
  }

  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): VendorAd[] {
    return this._selectedRows;
  }

  public set selectedRows(value: VendorAd[]) {
    this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }

  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }

  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get searchTerm(): string {
    return this._searchTerm;
  }

  public set searchTerm(value: string) {
    this._searchTerm = value;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }
}
