import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgForm} from '@angular/forms';
import { MatSnackBar} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Reward } from '../reward.model';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { RewardService } from '../service/reward.service';
import { ViewProductRewardComponent } from '../view/view-products/view-product-reward.component';
import { VendorService } from '../../vendor/Services/vendor.service';
import { User } from '../../auth/user.model';
import { AuthService } from '../../auth/service/auth.service';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';

@Component({
  selector: 'app-reward-form',
  templateUrl: './reward-form.component.html',
  styleUrls: ['./reward-form.component.css']
})
export class RewardFormComponent extends BaseFormComponent implements OnInit {

  @ViewChild("rewardForm") rewardForm : NgForm;
  @ViewChild(ViewProductRewardComponent) viewProductRewardComponent : ViewProductRewardComponent;

  _reward : Reward;
  protected loggedUser: User;
  private currencyName: string;

  @Input()
  _isEdit:boolean = false;

  constructor(protected rewardService: RewardService,
              protected vendorService: VendorService,
              protected vendorAdminService: VendorAdminService,
              protected route:ActivatedRoute,
              protected authService: AuthService,
              protected router:Router,
              protected snackBar:MatSnackBar) { 

    super(rewardService, 
      route,
      router,
      snackBar);
  }

  ngOnInit() {
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    if(this.route.snapshot.queryParams["edit"] == '1') {
      this._isEdit = true;
      this._reward = this.rewardService.reward;
    } else {
      this._reward = new Reward();
    }

    this.loadCurrency();
  }
  
  loadCurrency(){
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(
      vendorId => this.vendorService.getCurrencyNameByVendorId(vendorId).subscribe(
          data =>{
            this.currencyName = data;
          }
      ));
  }

  onSave(){
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId => {
      this._reward.vendor.id = vendorId;
      this.rewardService.reward = this._reward;
      this.viewProductRewardComponent.onSave();
      super.onSave();
    });
  
  }

  onSaveAndNew(){
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId => {
      this._reward.vendor.id = vendorId;
      this.rewardService.reward = this._reward;
      this.viewProductRewardComponent.onSave();
      super.onSaveAndNew();
    });
    
  }

  onSaveAndNewCompleted(){
    super.onSaveAndNewCompleted();
    this.viewProductRewardComponent._allProductReward = [];
  }
  
  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
  
  get model() : Reward {
    return this._reward;
  }

  set model(reward:Reward){
    this._reward = reward;
  }
  
  initModel(): Reward {
    return new Reward();
  }

  ngFormInstance(): NgForm {
    return this.rewardForm;
  }


}
