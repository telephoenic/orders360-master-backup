import { Product } from "../product/product";
import { Reward } from "./reward.model";

export class ProductReward {

    id: number;
    inactive:boolean;
    product:Product;
    productRewardPoints:number;

    constructor (){
    }
    
    get Product() {
        return this.product;
    }
    
    set Product(product:Product) {
        this.product = product;
    }

    get number() {
        return this.productRewardPoints;
    }
    
    set number(productRewardPoints:number) {
        this.productRewardPoints = productRewardPoints;
    }

}