import { ProductReward } from "./product-reward.model";
import { Vendor } from "../vendor/vendor.model";

export class Reward {
    
    id: number;
    name: string;
    vendor:Vendor;
    rewardFactor:number;
    inactive:boolean;
    listOfProductReward:ProductReward[] = [];

    constructor ( ){
        this.vendor = new Vendor();
    }
    

}