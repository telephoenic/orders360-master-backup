import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';

import { BaseService } from '../../common/service/base.service';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { Reward } from '../reward.model';
import { Observable } from '../../../../node_modules/rxjs/Observable';

@Injectable()
export class RewardService extends BaseService{
  reward:Reward;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
              protected authService:AuthService) {
    super(http, "reward", authService);
  }


  getAllSurveyByVendorId(pageNum:number, 
    pageSize:number, 
    vendorId:number , 
    searchTerm:string, 
    sortBy:string, 
    sortOrder:string ) : Observable<any>{
return this.http.get(environment.baseURL + "survey/survey_for_vendor/"+ pageNum+"/"+pageSize+"/"+vendorId, 
   { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder), 
     headers: this.getBasicHeaders()});
}


  
  getAllRewardsByVendorId(pageNum:number,
                          pageSize:number, 
                          vendorId:number,
                          searchTerm:string, 
                          sortBy:string, 
                          sortOrder:string) : Observable<any>{
    return this.http.get(environment.baseURL + "reward/reward_for_vendor/"+ pageNum+"/"+pageSize+"/"+vendorId,
    { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder), 
      headers: this.getBasicHeaders()});
  }

  get model() {
    return this.reward;
  }

  set model(model:BaseModel) {
    this.reward = <Reward>model;
  }
}