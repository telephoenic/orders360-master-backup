import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';

import { BaseService } from '../../common/service/base.service';
import { environment } from '../../../environments/environment';

import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Product } from '../../product/product';
import { Observable } from 'rxjs/Observable';
import { Reward } from '../reward.model';

@Injectable()
export class ProductRewardService extends BaseService{
  reward:Reward;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
              protected authService:AuthService) {
    super(http, "product_reward", authService);
  }

  getAllProducts(rewardId:number) : Observable<any>{
    return this.http.get(environment.baseURL + "product_reward/items"+"/"+rewardId);
  }

  get model(){
    return this.reward;
  }
  set model(model:BaseModel) {
    this.reward = <Reward>model;
  }
}