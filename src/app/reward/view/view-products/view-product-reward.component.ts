import { TdDataTableSortingOrder, ITdDataTableColumn } from "@covalent/core";
import {
  TdPagingBarComponent,
  TdDialogService,
  TdDataTableComponent
} from "@covalent/core";
import { Component, ViewChild, Input, OnInit, Inject } from "@angular/core";
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog } from "@angular/material";
import { BaseViewComponent } from "../../../common/view/base-view-component";
import { Product } from "../../../product/product";
import { ProductService } from "../../../product/service/product.service";
import { NgForm, FormControl, Validators } from "@angular/forms";
import { User } from "../../../auth/user.model";
import { AuthService } from "../../../auth/service/auth.service";
import { VendorAdminService } from "../../../vendor-admin/service/vendor-admin.service";
import { ProductReward } from "../../product-reward.model";
import { RewardService } from "../../service/reward.service";
import { ProductRewardService } from "../../service/product-reward.service";

@Component({
  selector: "view-product-reward",
  templateUrl: "./view-product-reward.component.html"
})
export class ViewProductRewardComponent extends BaseViewComponent
  implements OnInit {
  @Input() isAssign: boolean;
  @Input() rewardFactor: number;
  @Input() rewardFactorField: boolean;

  private _allProducts: Product[] = [];
  private _allDeassignedProducts: ProductReward[] = [];
  private user: User;

  private _currentPage: number = 1;
  private _fromRow: number = 1;
  public _allProductReward: ProductReward[] = [];
  public _allExistsProductReward: ProductReward[] = [];
  private _errors: any = "";
  private _selectedRows: Product[] = [];
  private _selectedRewardRows: ProductReward[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = "";
  private _sortOrder: TdDataTableSortingOrder =
    TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = "";
  private isDeleted: boolean;
  private deletedRows: ProductReward[] = [];

  @ViewChild("pagingBar") pagingBar: TdPagingBarComponent;
  @ViewChild("pagingRewardBar") pagingRewardBar: TdPagingBarComponent;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;
  @ViewChild("dataRewardTable") dataRewardTable: TdDataTableComponent;

  @ViewChild("dataAssignedTable") dataAssignedTable: TdDataTableComponent;
  @ViewChild("dataDeassignedTable") dataDeassignedTable: TdDataTableComponent;

  columnsConfig: ITdDataTableColumn[] = [
    { name: "name", label: "Product Name" },
    { name: "price", label: "Price", width: 110 }
  ];

  columnsDeassignedConfig: ITdDataTableColumn[] = [
    { name: "product.name", label: "Product Name" },
    { name: "product.price", label: "Price" },
    { name: "id", label: "", width: 100, sortable: false }
  ];

  columnsRewardConfig: ITdDataTableColumn[] = [
    { name: "product.name", label: "Product Name", width: 215 },
    { name: "product.price", label: "Price" },
    { name: "productRewardPoints", label: "Ponits" }
  ];

  constructor(
    protected rewardService: RewardService,
    protected productService: ProductService,
    protected vendorAdminService: VendorAdminService,
    protected productRewardService: ProductRewardService,
    protected authService: AuthService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    super(productService, router, dialogService, snackBar);
  }

  refreshData() {
    if (this.route.snapshot.queryParams["edit"] == "1") {
      this.loadAllProductReward();
    } else {
      this._allProductReward = [];
    }
  }

  loadAllProductReward() {
    this.productRewardService
      .getAllProducts(this.rewardService.reward.id)
      .subscribe(data => {
        this._allProductReward = data;
        for (
          let counter = 0;
          counter < this._allProductReward.length;
          counter++
        ) {
          this._allProductReward[counter].productRewardPoints = Math.round(
            this._allProductReward[counter].product.price *
              this.rewardService.reward.rewardFactor
          );
        }
      });
  }

  ngOnInit() {
    this.pageSize = this.pagingBar.pageSize;
    this.user = this.authService.getCurrentLoggedInUser();

    this.refreshData();
    super.ngOnInit();
  }

  filterByVendor(
    pageNumber: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    vendorId: number
  ) {
    this.productService
      .getProductsByReward(
        pageNumber,
        pageSize,
        vendorId,
        searchTerm,
        sortBy,
        sortOrder
      )
      .subscribe(data => {
        this._allProducts = data["content"];
        this.filteredTotal = data["totalElements"];
      });
  }

  getLeafProductInstance() {
    let product = new Product();
    return product;
  }

  onSave() {
    this.validateSelectedReward();
    if (
      this._allDeassignedProducts !== null &&
      this._allDeassignedProducts.length !== 0
    )
      this.productRewardService.delete(this._allDeassignedProducts).subscribe();
    if (
      this._allProductReward !== null &&
      this._allProductReward.length !== 0
    ) {
      this.rewardService.reward.listOfProductReward = this._allProductReward;
      this.dataRewardTable.refresh();
    }
  }

  validateSelectedReward() {
    if (this._allProductReward == null || this._allProductReward.length == 0) {
      this.snackBar.open(
        "At least one product required for the reward.",
        "Close",
        { duration: 3000 }
      );
      throw new Error("At least one product required for the reward.");
    }
  }

  onAssignedProducts() {
    this.selectedRows.forEach(product => {
      this._allProductReward.forEach(productReward => {
        if (productReward.product.id == product.id) {
          this.snackBar.open("Product is already aded.", "close", {
            duration: 3000
          });
          throw new Error("Product is already aded.");
        }
      });
    });
    for (let counter = 0; counter < this._selectedRows.length; counter++)
      this._allProductReward = this._allProductReward.concat(
        this.getLeafProductRewardInstance(this._selectedRows[counter])
      );
    let unSelected = this._allProducts.filter(
      item => this.selectedRows.indexOf(item) < 0
    );
    this._allProducts = unSelected;
    this._selectedRows = [];
  }

  onDeassignedProducts() {
    this._allDeassignedProducts = this._allDeassignedProducts.concat(
      this._selectedRewardRows
    );
    let unSelected = this._allProductReward.filter(
      item => this._selectedRewardRows.indexOf(item) < 0
    );
    this._allProductReward = unSelected;
    this._selectedRewardRows = [];
  }

  onRemoveDeassigned(row: ProductReward) {
    this._allDeassignedProducts = this._allDeassignedProducts.filter(
      obj => obj !== row
    );
    this._allProductReward = this._allProductReward.concat(row);

    this.dataDeassignedTable.refresh();
    this.dataRewardTable.refresh();
  }

  onRemoveAssigned(row: ProductReward) {
    this._allProductReward = this._allProductReward.filter(obj => obj !== row);
    this._allProducts = this._allProducts.concat(row.product);
    this.dataAssignedTable.refresh();
    this.dataTable.refresh();
  }

  getLeafProductRewardInstance(product: Product) {
    let productReward = new ProductReward();
    productReward.product = product;
    if (this.route.snapshot.queryParams["edit"] == "1") {
      productReward.productRewardPoints = Math.round(
        productReward.product.price * this.rewardService.reward.rewardFactor
      );
    } else {
      productReward.productRewardPoints = Math.round(
        product.price * this.rewardFactor
      );
    }

    return productReward;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get allModels(): Product[] {
    return this._allProducts;
  }

  public set allModels(value: Product[]) {
    this._allProducts = value;
  }

  public get errors(): any {
    return this._errors;
  }

  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): Product[] {
    return this._selectedRows;
  }

  public set selectedRows(value: Product[]) {
    this._selectedRows = value;
  }

  public get selectedRewardRows(): ProductReward[] {
    return this._selectedRewardRows;
  }

  public set selectedRewardRows(value: ProductReward[]) {
    this._selectedRewardRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }

  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }

  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get searchTerm(): string {
    return this._searchTerm;
  }

  public set searchTerm(value: string) {
    this._searchTerm = value;
  }
}

@Component({
  selector: "reward-dialog",
  templateUrl: "reward-dialog.html"
})
export class DialogProductReward {
  @ViewChild("rewardDialogForm") rewardDialogForm: NgForm;

  constructor(
    public dialogRef: MatDialogRef<DialogProductReward>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  discountItemField = new FormControl("", [
    Validators.required,
    Validators.min(0),
    Validators.max(100)
  ]);

  onNoClick(): void {
    this.dialogRef.close();
  }
}
