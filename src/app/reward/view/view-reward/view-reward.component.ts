import { RewardService } from '../../service/reward.service';
import { Reward} from '../../reward.model';
import { TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import { TdPagingBarComponent, TdDialogService, TdDataTableComponent } from '@covalent/core';
import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatSnackBar
} from "@angular/material";

import { Router } from '@angular/router';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { NgxPermissionsService } from 'ngx-permissions';
import { AuthService } from '../../../auth/service/auth.service';
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';
import { User } from '../../../auth/user.model';

@Component({
  selector: 'app-view',
  templateUrl: './view-reward.component.html',
  styleUrls: ['./view-reward.component.css']
})
export class ViewRewardsComponent extends BaseViewComponent implements OnInit{

  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private user:User;
  private _allRewards: Reward[] = [];
  private _errors: any = '';
  private _selectedRows: Reward[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string ='';
  
  @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
  @ViewChild('dataTable') dataTable : TdDataTableComponent;
 
	

  columnsConfig: ITdDataTableColumn[] = [
		{ name: 'name', label: 'Name' ,tooltip:"Name"},
		{ name: 'rewardFactor', label: 'Reward Earned Points Conv. Rate/ 1' ,tooltip:"Factor" , width:250},
		{ name: "inactive", label: "Active/Inactive", tooltip : "Active or Inactive", width:100},
    { name: 'id', label: 'Edit', tooltip:"Edit", width:100 }
  ];

	constructor(protected rewardService: RewardService,
							protected vendorAdminService: VendorAdminService,
              protected router:Router,
							protected dialogService : TdDialogService,
							protected authService: AuthService,
              protected snackBar:MatSnackBar  , 
              protected permissionsService:NgxPermissionsService) { 
    super(rewardService, router, dialogService, snackBar);
  }


  ngOnInit() {

		super.ngOnInit();
		
    this.permissionsService.hasPermission("ROLE_REWARD_EDIT").then((result:boolean)=> {
      if(!result) {
        for(let i =0; i < this.dataTable.columns.length; i++) {
          if(this.dataTable.columns[i].name == 'id') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });


    this.permissionsService.hasPermission("ROLE_REWARD_ACTIVATE_DEACTIVATE_PER_ROW").then((result:boolean)=> {
      if(!result) {
        for(let i =0; i < this.dataTable.columns.length; i++) {
          if(this.dataTable.columns[i].name == 'inactive') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });
	}

	filterByVendor(pageNumber: number, pageSize: number,
		searchTerm: string, sortBy: string, sortOrder: string, vendorId: number) {
		this.rewardService.getAllRewardsByVendorId(pageNumber, pageSize, vendorId, searchTerm, sortBy, sortOrder).subscribe(
			data => {
				this.allModels = data["content"];
				this.filteredTotal = data["totalElements"]
			}
		);
	}

	/*
	onAfterDelete() {
		this.filterRewardByUser();
	}
	*/


  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): Reward[]  {
		return this._allRewards;
	}

	public set allModels(value: Reward[] ) {
		this._allRewards = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): Reward[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: Reward[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
	}
}
