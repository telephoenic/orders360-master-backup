import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupporticketTypeComponent } from './supporticket-type.component';

describe('SupporticketTypeComponent', () => {
  let component: SupporticketTypeComponent;
  let fixture: ComponentFixture<SupporticketTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupporticketTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupporticketTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
