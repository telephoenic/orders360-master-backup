import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTicketTypeComponent } from './view-ticket-type.component';

describe('ViewTicketTypeComponent', () => {
  let component: ViewTicketTypeComponent;
  let fixture: ComponentFixture<ViewTicketTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTicketTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTicketTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
