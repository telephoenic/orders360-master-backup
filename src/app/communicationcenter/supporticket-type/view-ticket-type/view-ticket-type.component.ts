import { Component, OnInit, ViewChild, Input, Inject } from '@angular/core';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { TicketTypeService } from '../service/ticket-type.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TdDialogService, TdDataTableSortingOrder, TdPagingBarComponent, TdDataTableComponent, ITdDataTableColumn } from '@covalent/core';
import { AuthService } from '../../../auth/service/auth.service';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgxPermissionsService } from 'ngx-permissions';
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';
import { environment } from '../../../../environments/environment';
import { SupportTicketType } from '../ticket-type-model';
import { HttpErrorResponse } from '@angular/common/http';
import { BaseFormComponent } from '../../../common/form/base-form-component';
import { NgForm } from '@angular/forms';
import { SupportTicket } from '../../supporticket/supporticket.model';
import { User } from '../../../auth/user.model';
import { RelatedTicketsComponent } from '../related-tickets/related-tickets.component';

@Component({
  selector: 'view-ticket-type',
  templateUrl: './view-ticket-type.component.html',
  styleUrls: ['./view-ticket-type.component.css']
})
export class ViewTicketTypeComponent extends BaseViewComponent implements OnInit {


  private _errors: any = '';
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private _searchTerm: string = '';
  private _pageSize: number = environment.initialViewRowSize;
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _isEdit: boolean = true;
  private _allTypes: SupportTicketType[] = [];
  vendorId: number;

  @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
  @ViewChild('allTypesDt') allTypesDt: TdDataTableComponent;

  private _selectedRows: SupportTicketType[] = [];
  supportTicketType: SupportTicketType = new SupportTicketType();

  columnsConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name', tooltip: "Name" },
    { name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive", width: 100 },
    { name: 'id', label: 'Edit', tooltip: "Edit", sortable: false, width: 100 }
  ];

  constructor(protected ticketTypeService: TicketTypeService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected authService: AuthService,
    protected snackBar: MatSnackBar,
    protected matDialog: MatDialog,
    protected permissionsService: NgxPermissionsService,
    protected vendorAdminService: VendorAdminService
  ) {
    super(ticketTypeService, router, dialogService, snackBar);
  }

  ngOnInit() {
    super.ngOnInit();
    this.permissionsService.hasPermission("ROLE_TICKET_SUPPORT_TYPE_ADD").then((result: boolean) => {
      if (!result) {
        for (let i = 0; i < this.allTypesDt.columns.length; i++) {
          if (this.allTypesDt.columns[i].name == 'id') {
            this.allTypesDt.columns.splice(i, 1);
          }
        }
      }
    });

    this.permissionsService.hasPermission("ROLE_TICKET_SUPPORT_TYPE_ACTIVATE_DEACTIVATE_PER_ROW").then((result: boolean) => {
      if (!result) {
        for (let i = 0; i < this.allTypesDt.columns.length; i++) {
          if (this.allTypesDt.columns[i].name == 'inactive') {
            this.allTypesDt.columns.splice(i, 1);
          }
        }
      }
    });
  }

  filterByVendor(pageNumber: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    vendorId: number) {
    this.ticketTypeService.getAllTicketsByVendorId(pageNumber, pageSize, searchTerm, sortBy,
      sortOrder, vendorId)
      .subscribe(data => {
        this._allTypes = data["content"],
          this.filteredTotal = data["totalElements"]
      });
  }

  onActivateDeactivePerRow(value, row: SupportTicketType) {
    this.dialogService.openConfirm({
      message: "Are you sure you want to " + (value ? "activate" : "deactivate") + " the selected row(s)?",
      title: "confirmation",
      cancelButton: "Disagree",
      acceptButton: "Agree"
    })
      .afterClosed().subscribe((accept: boolean) => {
        if (accept) {
          this.ticketTypeService.changeStatusTo(value, [row.id])
            .subscribe(data => {
              row.inactive = !value;
              this.getDataTable().refresh();
            },
              (errorResponse: HttpErrorResponse) => {
                if (value) {
                  super.onActivationFailed(errorResponse);
                } else {
                  this.onDeactivationFailed(errorResponse);
                }
              }
            );
        } else {
          super.onRefresh();
        }
      }
      )
  };



  onAdd() {
    this.openDialog(new SupportTicketType());
  }

  onEdit(id: number, row: SupportTicketType) {
    this.ticketTypeService.supportTicketType = row;
    this.openDialog(row);
  }

  openDialog(row: SupportTicketType) {
    let supportTicketTypeDialog = this.matDialog.open(SupportTicketTypeDialog, {
      width: '500px',
      disableClose: true,
      data: row
    });
    supportTicketTypeDialog.afterClosed().subscribe(() => {
      this.filter(this.currentPage - 1, this.pageSize);
    });
  }


  // onDeleteFailed(errorResponse: HttpErrorResponse) {     
  //   this.snackBar.open(errorResponse.error["message"], "More", { duration: 10000 })
  //     .onAction()
  //     .subscribe(() => this.showDetails(errorResponse));

  // }

  // showDetails(errorResponse: HttpErrorResponse) {    
  //   let relatedTicketsObject: any[] = errorResponse.error['extraInfo']['RELATED_TICKETS'];    
  //   this.matDialog.open(RelatedTicketsComponent,
  //     {
  //       data: {
  //         relatedTickets: relatedTicketsObject
  //       }
  //     });

  // }

  public get selectedRows(): SupportTicketType[] {
    return this._selectedRows;
  }
  public set selectedRows(value: SupportTicketType[]) {
    this._selectedRows = value;
  }

  getDataTable(): TdDataTableComponent {
    return this.allTypesDt;
  }

  public get allModels(): SupportTicketType[] {
    return this._allTypes;
  }

  public set allModels(value: SupportTicketType[]) {
    this._allTypes = value;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }

  public get errors(): any {
    return this._errors;
  }
  public set errors(value: any) {
    this._errors = value;
  }
  public get filteredTotal(): number {
    return this._filteredTotal;
  }
  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }
  public get sortBy(): string {
    return this._sortBy;
  }
  public set sortBy(value: string) {
    this._sortBy = value;
  }
  public get pageSize(): number {
    return this._pageSize;
  }
  public set pageSize(value: number) {
    this._pageSize = value;
  }
  public get currentPage(): number {
    return this._currentPage;
  }
  public set currentPage(value: number) {
    this._currentPage = value;
  }
  public get fromRow(): number {
    return this._fromRow;
  }
  public set fromRow(value: number) {
    this._fromRow = value;
  }
  public get searchTerm(): string {
    return this._searchTerm;
  }
  public set searchTerm(value: string) {
    this._searchTerm = value;
  }
  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }
  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }
  public get isEdit(): boolean {
    return this._isEdit;
  }
  public set isEdit(value: boolean) {
    this._isEdit = value;
  }

}

@Component({
  selector: 'support-ticket-type-dialog',
  templateUrl: 'support-ticket-type-dialog.html',
})
export class SupportTicketTypeDialog extends BaseFormComponent implements OnInit {


  @ViewChild("ticketTypeDialogForm") ticketTypeDialogForm: NgForm;


  @Input()

  private _isEdit: boolean = false;
  private _loggedUser: User;
  private _supportTicketType: SupportTicketType;
  public dialogTitle: string;

  constructor(protected ticketTypeService: TicketTypeService,
    protected route: ActivatedRoute,
    public supportTicketTypeDialog: MatDialogRef<SupportTicketTypeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: SupportTicketType,
    protected router: Router,
    protected snackBar: MatSnackBar,
    protected dialogService: TdDialogService,
    protected vendorAdminService: VendorAdminService,
    protected authService: AuthService) {

    super(ticketTypeService,
      route,
      router,
      snackBar);
  }

  ngOnInit() {

    if (this.data.id != null) {
      this._isEdit = true;
      this.dialogTitle = "Edit Ticket Type";
    } else {
      this.dialogTitle = "Add Ticket Type";
    }
    this._loggedUser = this.authService.getCurrentLoggedInUser();
    this._supportTicketType = this.data;

  }

  onNoClick(): void {
    this.supportTicketTypeDialog.close();
  }

  onSave() {
    this.vendorAdminService.getVendorByVendorAdminId(this._loggedUser.id).subscribe(vendorId => {
      this._supportTicketType.vendor.id = vendorId;
      this._supportTicketType.name = this.data.name;
      this.ticketTypeService.supportTicketType = this._supportTicketType;
      this.baseService.navigateToAfterCompletion = this.router.url;
      super.onSave();
    });
  }

  onSaveAndNew() {
    this.vendorAdminService.getVendorByVendorAdminId(this._loggedUser.id).subscribe(vendorId => {
      this._supportTicketType.vendor.id = vendorId;
      this._supportTicketType.name = this.data.name;
      this.ticketTypeService.supportTicketType = this._supportTicketType;
      this.baseService.navigateToAfterCompletion = this.router.url;
      super.onSaveAndNew();
    });
  }

  onSaveCompleted() {
    this.showSpinnerOnSaveAndNew = false;
    this.supportTicketTypeDialog.close();
    this.snackBar.open("Saved successfully", "Close", { duration: 3000 });
    this.reset();
  }

  onEditCompleted() {
    this.showSpinnerOnSaveAndNew = false;
    this.supportTicketTypeDialog.close();
    this.snackBar.open("Saved successfully", "Close", { duration: 3000 });
  }

  onSaveAndNewCompleted() {
    this.showSpinnerOnSaveAndNew = false;
    this.reset();
  }

  reset() {
    this._supportTicketType = new SupportTicketType();
  }

  get model(): SupportTicketType {
    return this._supportTicketType;
  }
  set model(supportTicketType: SupportTicketType) {
    this._supportTicketType = supportTicketType;
  }
  initModel(): SupportTicketType {
    return new SupportTicketType();
  }
  ngFormInstance(): NgForm {
    return this.ticketTypeDialogForm;
  }

  get isEdit(): boolean {
    return this._isEdit;
  }
  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

}
