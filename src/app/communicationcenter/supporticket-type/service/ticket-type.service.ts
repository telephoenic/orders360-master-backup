import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SupportTicketType } from '../ticket-type-model';
import { BaseModel } from '../../../common/model/base-model';
import { AuthService } from '../../../auth/service/auth.service';
import { BaseService } from '../../../common/service/base.service';
import { environment } from '../../../../environments/environment';

@Injectable()
export class TicketTypeService extends BaseService {

  supportTicketType: SupportTicketType;
  navigateToAfterCompletion: string;

  get model() {
    return this.supportTicketType;
  }

  set model(model:BaseModel) {
    this.supportTicketType = <SupportTicketType>model;
  }

  constructor(protected http: HttpClient,
    protected authService: AuthService) {
    super(http, "ticket-type", authService);
  }


  getAllTicketsByVendorId(pageNum: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    vendorId: number): Observable<any> {      
    return this.http.get(environment.baseURL + "ticket-type/get-all-tickets/" + pageNum + "/" + pageSize + "/" + vendorId,
      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      });
  }

}
