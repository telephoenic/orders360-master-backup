import { BaseModel } from "../../common/model/base-model";
import { Vendor } from "../../vendor/vendor.model";
export class SupportTicketType extends BaseModel {
    id: number;
    inactive: boolean;
    name: string;
    vendor: Vendor;
    constructor() {
        super();
        this.vendor = new Vendor();
    }
}
