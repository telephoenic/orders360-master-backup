import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-related-tickets',
  templateUrl: './related-tickets.component.html',
  styleUrls: ['./related-tickets.component.css']
})
export class RelatedTicketsComponent implements OnInit {

  ngOnInit(): void {
  }

   constructor(@Inject(MAT_DIALOG_DATA) public data:any) { }


}
