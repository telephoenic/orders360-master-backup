import { TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import { TdPagingBarComponent, TdDialogService, TdDataTableComponent } from '@covalent/core';
import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material";


import { ActivatedRoute, Router } from '@angular/router';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { NgxPermissionsService } from 'ngx-permissions';
import { AuthService } from '../../../auth/service/auth.service';
import { User } from '../../../auth/user.model';
import { environment } from '../../../../environments/environment';
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';
import { SupportTicket } from '../supporticket.model';
import { SupportTicketService } from '../service/supporticket.service';
import { FormControl, Validators } from '@angular/forms';
import { audit } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { SupportTicketType } from '../../supporticket-type/ticket-type-model';

@Component({
	selector: 'app-view',
	templateUrl: './view-supporticket.component.html',
	styleUrls: ['./view-supporticket.component.css']
})
export class ViewSupportTicketComponent extends BaseViewComponent implements OnInit {

	private _currentPage: number = 1;
	private _fromRow: number = 1;
	private user: User;
	private _allTickets: SupportTicket[] = [];
	private _errors: any = '';
	private _selectedRows: SupportTicket[] = [];
	private _pageSize: number;
	private _filteredTotal: number;
	private _sortBy: string = '';
	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
	private _searchTerm: string = '';
	private startDate: any;
	private endDate: any;
	vendorId:number;

	@ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
	@ViewChild('dataTable') dataTable: TdDataTableComponent;



	columnsConfig: ITdDataTableColumn[] = [
		{ name: 'posUser.name', label: 'POS name', tooltip: "POS Name" },
		{ name: 'posUser.mobileNumber', label: 'Mobile number', tooltip: "Mobile Number" },
		{ name: 'ticketStatus.status', label: 'Active/Inactive', tooltip: "Active/Inactive" },
		{ name: 'supportTicketType.name', label: 'Ticket Type', tooltip: 'Ticket Type' },
		{ name: 'audit.createdDate', label: 'Create Date', tooltip: 'Create Date', format: date => this.formatDate(date) },
		{ name: 'audit.updatedDate', label: 'Reply Date', tooltip: 'Reply Date', format: date => this.formatDate(date) },
		{ name: 'id', label: 'Reply', tooltip: "Reply" }
	];

	constructor(protected supportTicketService: SupportTicketService,
		protected vendorAdminService: VendorAdminService,
		protected router: Router,
		protected authService: AuthService,
		protected dialogService: TdDialogService,
		protected snackBar: MatSnackBar,
		protected permissionsService: NgxPermissionsService,
		protected datePipe: DatePipe,
		protected dialog: MatDialog) {
		super(supportTicketService, router, dialogService, snackBar);
	}

	ngOnInit() {
		super.ngOnInit();
	}

	formatDate(value): any {
		if (value == null) {
			return '';
		}
		return new Date(value).toLocaleString('en-US')
	}

	filterByVendor(pageNumber: number, pageSize: number,
		searchTerm: string, sortBy: string, sortOrder: string, vendorId: number) {
			this.vendorId = vendorId;
		this.supportTicketService.getAllTicketsByVendorId(pageNumber, pageSize, vendorId, searchTerm, sortBy, sortOrder,this.startDate,this.endDate).subscribe(
			data => {
				this.allModels = data["content"];
				this.filteredTotal = data["totalElements"]
			});
	}



	openDialog(id: number, row: SupportTicket): void {
		debugger;
		if(row.supportTicketType == null){
			row.supportTicketType = new SupportTicketType();
			row.supportTicketType.name = "";
		}
		let dialogRef = this.dialog.open(DialogTicket, {
			width: '500px',
			data: {
				subject: row.subject, pmsg: row.pmsg, vmsg: row.vmsg, ticketStatus: row.ticketStatus,
				supportTicketType: row.supportTicketType.name, createDate: this.formatDate(row.audit.createdDate),
				replyDate: this.formatDate(row.audit.updatedDate)
			}

		});
		dialogRef.afterClosed().subscribe(result => {
			if (result != null && result != "") {

				row.vmsg = result.vmsg;
				row.ticketStatus.id = 2;
				if(row.supportTicketType.name == ""){
					row.supportTicketType = null;
				}
				this.supportTicketService.update(row).subscribe(
					data => {
						//this.updateStatus();
						this.user = this.authService.getCurrentLoggedInUser();
						this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(vendorId => {
							this.supportTicketService.getAllTicketsByVendorId(0, this.pageSize, vendorId, "", "", "",this.startDate,this.endDate).subscribe(data => {
								this.allModels = data["content"];
								this.filteredTotal = data["totalElements"]
							});
						});

					}
				);
				this.dataTable.refresh();
			}
		});

	}

	/*
	updateStatus(){
		this.pageSize = this.getPagingBar().pageSize;
		let pageNumber: number = this._currentPage-1;
		this.baseService.page(pageNumber, this.pageSize).subscribe(
			data => {
				this._allTickets = data["content"];
				this.filteredTotal = data["totalElements"]
			}
		);
	}

	*/

	public startDateChange(event: any) {
		this.startDate = this.datePipe.transform(event.value, 'yyyy-MM-dd');
		if (this.startDate == null) {
			this.getPagingBar().navigateToPage(1);
		}

	}

	public endDateChange(event: any) {
		this.endDate = this.datePipe.transform(event.value, 'yyyy-MM-dd');
		if(this.startDate == undefined && this.endDate == null){
			this.getPagingBar().navigateToPage(1);
		}
	}
	handleFilterEvent() {
		if (this.startDate > this.endDate) {
			this.snackBar.open("End date must be after than start date.", "close", {
				duration: 3000
			});
			return;
		}
			this.getPagingBar().navigateToPage(1);

			this.supportTicketService.getAllTicketsByVendorId(this.currentPage - 1, this.pageSize, this.vendorId, this.searchTerm, this.sortBy, this.sortOrder,this.startDate,this.endDate).subscribe(
				data => {
					this.allModels = data["content"];
					this.filteredTotal = data["totalElements"]
				});
	

	}


	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}
	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

	public get currentPage(): number {
		return this._currentPage;
	}

	public set currentPage(value: number) {
		this._currentPage = value;
	}

	public get fromRow(): number {
		return this._fromRow;
	}

	public set fromRow(value: number) {
		this._fromRow = value;
	}

	public get allModels(): SupportTicket[] {
		return this._allTickets;
	}

	public set allModels(value: SupportTicket[]) {
		this._allTickets = value;
	}

	public get errors(): any {
		return this._errors;
	}

	public set errors(value: any) {
		this._errors = value;
	}

	public get selectedRows(): SupportTicket[] {
		return this._selectedRows;
	}

	public set selectedRows(value: SupportTicket[]) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string {
		return this._sortBy;
	}

	public set sortBy(value: string) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder) {
		this._sortOrder = value;
	}

	public get searchTerm(): string {
		return this._searchTerm;
	}

	public set searchTerm(value: string) {
		this._searchTerm = value;
	}
}
@Component({
	selector: 'ticket-dialog',
	templateUrl: 'ticket-dialog.html',
	styleUrls: ['./ticket-dialog.css']

})
export class DialogTicket {
	constructor(
		public dialogRef: MatDialogRef<DialogTicket>,
		@Inject(MAT_DIALOG_DATA) public data: any) { }

	vmsgField = new FormControl('', [
		Validators.required]);


	onNoClick(): void {
		this.dialogRef.close();
	}

}
