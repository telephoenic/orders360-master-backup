
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../../common/service/base.service';
import { SupportTicket } from '../supporticket.model';
import { AuthService } from '../../../auth/service/auth.service';
import { BaseModel } from '../../../common/model/base-model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';

@Injectable()
export class SupportTicketService extends BaseService {
  supportTicket: SupportTicket;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService,
    protected datePipe: DatePipe) {
    super(http, "support_ticket", authService);
  }


  getAllTicketsByVendorId(pageNum: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    startDate: any,
    endDate: any): Observable<any> {
    let parameterList: { paramName: string, paramValue: any }[] = [];
      console.log(startDate)
    if(startDate == undefined){
      startDate = this.datePipe.transform(new Date('1970-04-01').getDate(), 'yyyy-MM-dd');
    }
    if(endDate == undefined){
      endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    }
    parameterList.push({ paramName: "startDate", paramValue: startDate });
    parameterList.push({ paramName: "endDate", paramValue: endDate });

    return this.http.get(environment.baseURL + "support_ticket/ticket_for_vendor/" + pageNum + "/" + pageSize + "/" + vendorId,
      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder,(parameterList)),
        headers: this.getBasicHeaders()
      });
  }

  get model() {
    return this.supportTicket;
  }

  set model(model: BaseModel) {
    this.supportTicket = <SupportTicket>model;
  }
}