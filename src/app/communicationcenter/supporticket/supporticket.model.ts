import { PosUser } from "../../pos-user/pos-user.model";
import { Vendor } from "../../vendor/vendor.model";
import { TicketStatus } from "../../lookups/ticket-status.model";
import { SupportTicketType } from "../supporticket-type/ticket-type-model";
import { Audit } from "../../audit/audit.model";


export class SupportTicket {
    
    id: number;
    inactive:boolean;
    subject:string;
    pmsg:string;
    vmsg:string;
    posUser:PosUser;
    vendor:Vendor;
    supportTicketType:SupportTicketType;
    ticketStatus:TicketStatus;
    audit: Audit;


    constructor ( ){}
    

}