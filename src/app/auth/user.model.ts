import { UserType } from "../lookups/user-type.model";

export class User {
    id:number;
    type:UserType;
    name:string;
    email:string;
    mobileNumber:string;
    authToken:string;
    permissions:string[];
    inactive:boolean;
}