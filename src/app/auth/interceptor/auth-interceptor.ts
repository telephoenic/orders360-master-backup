import { HttpInterceptor, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpRequest, HttpEvent, HttpErrorResponse, HttpHeaders } from "@angular/common/http";

import { Injectable } from "@angular/core";
import { AuthService } from "../service/auth.service";
import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/do';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService:AuthService,
                private router:Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let headers = new HttpHeaders();
        const clonedReq = req.clone({headers:req.headers.set("x-auth-token", this.authService.getCurrentLoggedInUser().authToken)})
        
        return next.handle(clonedReq)
                    .do((event:HttpEvent<any>) => {

                    }, error => {
                        if(error instanceof HttpErrorResponse) {
                            if(error.status === 401) {
                                this.router.navigate(["login"]);
                            }
                        }
                    });
    }
}