import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User } from '../user.model';
import { Http, Headers } from '@angular/http';
import { environment } from "../../../environments/environment";
import { NgxPermissionsService } from 'ngx-permissions';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

const URL_AUTH = "auth/";
const URL_LOGOUT = "logout";

const KEY_LOGGED_IN_USER_OBJ = "loggedInUser";

@Injectable()
export class AuthService {
  public loggedIn = new BehaviorSubject<boolean>(false);

  constructor(private http:Http,
    private ngxPermServices:NgxPermissionsService,
    private router:Router) { }

  get isLoggedIn():Observable<boolean> {
    return this.loggedIn.asObservable();
  }

  authenticate(username:string, password:string, rememberMe:boolean) {
    if(username && password) {

      return this.http.get(environment.baseURL + "auth/login",
                           {headers: this.prepareLoginHeaders(username, password), 
                            params: {"remember_me": rememberMe} });
    }
  }

  prepareLoginHeaders(username:string, password:string) {
    return new Headers({'Authorization' : "Basic "+btoa(username+":"+password)});
  }
  
  onAuthenticationSuccess(response) {
    this.loggedIn.next(true);
    let user:User =  response.json()["user"];
    user.authToken = this.getAuthTokenFrom(response);
    localStorage.setItem(KEY_LOGGED_IN_USER_OBJ, JSON.stringify(user));
    this.loadUserPermissions(user);
  }

  getAuthTokenFrom(data) : string {
    return data.headers['_headers'].get('x-auth-token')[0];
  }
  
  onAuthenticationFailure() {
    this.loggedIn.next(false);
  }

  loadUserPermissions(user:User) {
    this.ngxPermServices.loadPermissions(user.permissions);
  }

  logout() {
    return this.http.post(environment.baseURL+ URL_AUTH + URL_LOGOUT, 
                          '', 
                          {headers : this.prepareLogoutHeaders()});
  }
  
  prepareLogoutHeaders() {
      return new Headers({  'x-auth-token' : 
                            this.getCurrentLoggedInUser().authToken
                          });
  }

  getCurrentLoggedInUser() : User {
    let userObjAsJson = localStorage.getItem(KEY_LOGGED_IN_USER_OBJ);

    if(!userObjAsJson) {
      this.loggedIn.next(false);
      this.router.navigate(["login"]);
      throw new Error("You are not logged in, please login first");
    } 

    return JSON.parse(userObjAsJson);
  }

  onLogoutSuccess(data) {
    this.clearLocalStorage();
    this.clearUserPermissions();
    this.loggedIn.next(false);
  }

  clearUserPermissions() {
    this.ngxPermServices.flushPermissions();
  }

  clearLocalStorage() {
    localStorage.clear();
  }

  onLogoutFailure(data) {

  }
}
