import { VendorService } from "./../../vendor/Services/vendor.service";
import { VendorAdminService } from "./../../vendor-admin/service/vendor-admin.service";
import { Component, OnInit, ViewChild, HostListener } from "@angular/core";
import { User } from "../user.model";
import { AuthService } from "../service/auth.service";
import { MatSnackBar } from "@angular/material";
import { NgForm } from "@angular/forms";
import { NgxPermissionsService } from "ngx-permissions";
import { NgxRolesService } from "ngx-permissions";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { WebSocketAPI } from "../../WebSocketAPI";
import { MessageService } from "primeng/api";
import { NotificationService } from "../../notification/notification-service.service";
import { AppComponent, SessionTimeOutDialog } from "../../app.component";
import { environment } from "../../../environments/environment";
import { WarehouseAdminService } from "../../warehouse-admin/service/warehouse-admin.service";
import { TdDialogService } from "@covalent/core";
import { Subject } from "rxjs";

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent {
  @ViewChild("loginForm")
  private loginForm: NgForm;
  username: string;
  password: string;
  rememberMe: boolean;
  isAttribute: boolean = false;
  showSpinner: boolean = false;
  webSocketAPI: WebSocketAPI;
  isUserSelectLogOption: boolean = false;

  constructor(
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router,
    protected vendorAdminService: VendorAdminService,
    protected vendorService: VendorService,
    private messageService: MessageService,
    private notificationService: NotificationService,
    private warehouseAdminService: WarehouseAdminService,
    protected dialogService: TdDialogService
  ) {}

  onLoginSubmit() {
    AppComponent.countRequstNotificationIconPrivilege = 1;
    AppComponent.listOfObjects[0].countCheck = 1
    this.showSpinner = true;
    AppComponent.isNotificationIconShown = false;
    AppComponent.isUserLoggedIn = true;
    this.authService
      .authenticate(this.username, this.password, this.rememberMe)
      .subscribe(
        successResData => {
          if (this.validateUser(successResData)) {
            this.authService.onAuthenticationSuccess(successResData);
            let user: User = this.authService.getCurrentLoggedInUser();
            if (user.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin) {
              this.vendorAdminService
                .getVendorByVendorAdminId(user.id)
                .subscribe(data => {
                  localStorage.setItem("vendorId", data);
                  this.vendorService.findVendorById(data).subscribe(vendor => {
                    if (vendor.promotionType.type == "Attribute") {
                      this.isAttribute = true;
                      localStorage.setItem(
                        "attributeParam",
                        this.isAttribute.toString()
                      );
                    } else {
                      this.isAttribute = false;
                      localStorage.setItem(
                        "attributeParam",
                        this.isAttribute.toString()
                      );
                    }
                  });
                });
            } else if (
              user.type.code == environment.ENUM_CODE_USER_TYPE.WarehouseAdmin
            ) {
              this.warehouseAdminService
                .getVendorIdByUserId(user.id)
                .subscribe(vendorId => {
                  localStorage.setItem("vendorId", vendorId + "");
                });
            }
            this.router.navigate(["home"]);
          } else {
            this.onError("User is inactive.");
          }
        },

        (errorResponse: HttpErrorResponse) => {
          console.log(errorResponse);
          this.showSpinner = false;
          this.onError("Invalid username or password.");
        },

        () => {
          this.showSpinner = false;
          this.loginForm.resetForm();
          this.webSocketAPI = new WebSocketAPI();
          this.notificationService
            .getCountUnreadNotifications(
              this.authService.getCurrentLoggedInUser().id
            )
            .subscribe(
               (data: number) => {
                AppComponent.countNotifications = data;;
              },
              error => {},
              () => {}
            );
        }
      );
  }

  @HostListener("mousemove") pauseUserState() {
    clearTimeout(AppComponent.userActivity);
  }

  setCurrentModuleName(value: string, appendOrders360Title: boolean) {
    if (appendOrders360Title) {
      value = AppComponent.KEY_VALUE_ORDERS_360 + " - " + value;
    }

    localStorage.setItem(AppComponent.KEY_NAME_MODULE_NAME, value);
  }

  validateUser(response) {
    if (response.json()["user"].inactive) {
      return false;
    }
    return true;
  }

  onError(errorMessage: string) {
    this.authService.onAuthenticationFailure();
    this.snackBar.open(errorMessage, "Close", { duration: 3000 });
  }
}
