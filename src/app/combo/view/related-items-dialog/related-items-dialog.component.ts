import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatTabGroup, MatTab } from '@angular/material';
import { TdVirtualScrollContainerComponent } from '@covalent/core'; 

@Component({
  selector: 'related-items-dialog',
  templateUrl: './related-items-dialog.component.html',
  styleUrls: ['./related-items-dialog.component.css']
})
export class RelatedItemsDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data:any) { }

  ngOnInit() {
  }

}
