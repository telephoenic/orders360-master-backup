import { TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import { TdPagingBarComponent, TdDialogService,
  TdDataTableComponent } from '@covalent/core';
import {Component, ViewChild, OnInit } from '@angular/core';
import {
  MatSnackBar, MatDialog
} from "@angular/material";

import { Router } from '@angular/router';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { NgxPermissionsService } from 'ngx-permissions';
import { Combo } from '../../combo.model';
import { ComboService } from '../../service/combo.service';
import { DatePipe } from '@angular/common';
import { User } from '../../../auth/user.model';
import { environment } from '../../../../environments/environment';
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';
import { AuthService } from '../../../auth/service/auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { RelatedItemsDialogComponent } from '../related-items-dialog/related-items-dialog.component';

@Component({
  selector: 'app-view',
  templateUrl: './view-combo.component.html',
  styleUrls: ['./view-combo.component.css']
})
export class ViewComboComponent extends BaseViewComponent implements OnInit{

  private _currentPage: number = 1;
  private _fromRow: number = 1;
  
	private user:User;
  private _allCombos: Combo[] = [];
  private _errors: any = '';
  private _selectedRows: Combo[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string ='';
  
  @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
  @ViewChild('dataTable') dataTable : TdDataTableComponent;

	
  columnsConfig: ITdDataTableColumn[] = [
		{ name: 'name', label: 'Name' ,tooltip: "Name"},
		{ name: 'startDate', label: 'Start Date', format: v => this.datePipe.transform(v, 'mediumDate') ,tooltip: "Start Date", width:160},
		{ name: 'endDate', label: 'End Date', format: v => this.datePipe.transform(v, 'mediumDate') ,tooltip: "End Date", width:160},
		{ name: 'price', label: 'Price',tooltip: "Price", width:100 },
		{ name: "inactive", label: "Active/Inactive", tooltip : "Active or Inactive", width:100},
    { name: 'id', label: 'Edit' ,tooltip: "Edit",sortable:false, width:100}
  ];

	constructor(protected comboService: ComboService,
							protected vendorAdminService: VendorAdminService,
              protected router:Router,
							protected dialogService : TdDialogService,
							protected authService: AuthService,
							protected snackBar:MatSnackBar, 
    					protected matDialog: MatDialog, 
							private datePipe:DatePipe,
              protected permissionsService:NgxPermissionsService) { 
    super(comboService, router, dialogService, snackBar);
  }

  ngOnInit() {
		super.ngOnInit();
    this.permissionsService.hasPermission("ROLE_COMBO_EDIT").then((result:boolean)=> {
      if(!result) {
        for(let i =0; i < this.dataTable.columns.length; i++) {
          if(this.dataTable.columns[i].name == 'id') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });

    this.permissionsService.hasPermission("ROLE_COMBO_ACTIVATE_DEACTIVATE_PER_ROW").then((result:boolean)=> {
      if(!result) {
        for(let i =0; i < this.dataTable.columns.length; i++) {
          if(this.dataTable.columns[i].name == 'inactive') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });
	}
	
	 filterByVendor(pageNumber:number, pageSize:number, 
                 searchTerm:string, sortBy:string,sortOrder:string,vendorId:number){
          this.comboService.getAllCombosByVendorId(pageNumber, pageSize, vendorId , searchTerm, sortBy, sortOrder).subscribe(
            data => {
              this.allModels = data["content"];
              this.filteredTotal = data["totalElements"]
            }
          );
  }

/*
	onAfterDelete() {
		this.filterCombosByUser();
	}
	*/

	// onEdit(comboId:number, row:Combo){
	// 	row.startDate = (v:any) => this.datePipe.transform(v, 'mediumDate');
	// 	row.endDate = (v:any) => this.datePipe.transform(v, 'mediumDate');
	// 	this.comboService.combo = row;

	// }

	onDeleteFailed(errorResponse:HttpErrorResponse) {
		this.snackBar.open(errorResponse.error["message"], "More", {duration: 10000})
									.onAction()
									.subscribe(() => this.showDetails(errorResponse));
	}
	
	showDetails(errorResponse:HttpErrorResponse) {
		let childCategoriesObject:any[] = errorResponse.error['extraInfo']['CATEGORY_PARENT_CHILD'];
		let relatedProductsObject:any[] = errorResponse.error['extraInfo']['CATEGORY_RELATED_PRODUCTS'];

		this.matDialog.open(RelatedItemsDialogComponent, 
													{data: {relatedProducts: relatedProductsObject, 
																	childCategories: childCategoriesObject}});

	}

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): Combo[]  {
		return this._allCombos;
	}

	public set allModels(value: Combo[] ) {
		this._allCombos = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): Combo[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: Combo[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
	}
}
