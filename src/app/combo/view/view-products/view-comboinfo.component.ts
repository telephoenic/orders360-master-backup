
import { TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import {
	TdPagingBarComponent, TdDialogService,
	TdDataTableComponent
} from '@covalent/core';
import { Component, ViewChild, Input, OnInit, Inject } from '@angular/core';
import {
	MatSnackBar, MatDialogRef, MAT_DIALOG_DATA
} from "@angular/material";

import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { Product } from '../../../product/product';
import { ProductService } from '../../../product/service/product.service';
import { ComboService } from '../../service/combo.service';
import { ComboInfo } from '../../ComboInfo.model';
import { ComboInfoService } from '../../service/comboinfo.service';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { templateJitUrl } from '@angular/compiler';
import { User } from '../../../auth/user.model';
import { AuthService } from '../../../auth/service/auth.service';
import { environment } from '../../../../environments/environment';
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';


@Component({
	selector: 'view-comboinfo',
	templateUrl: './view-comboinfo.component.html'
})

export class ViewComboInfoComponent extends BaseViewComponent implements OnInit {


	@Input() isAssign: boolean;


	private dataTitle: string;

	private discountAmount: number;
	private quantity: number;
	private price: number;
	private product: String;
	public products: Product[] = [];
	private user: User;


	private _currentPage: number = 1;
	private _fromRow: number = 1;
	public _allInfo: ComboInfo[] = [];
	public _allProducts: Product[] = [];
	private _errors: any = '';
	private _selectedRows: ComboInfo[] = [];
	private _pageSize: number;
	private _filteredTotal: number;
	private _sortBy: string = '';
	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
	private _searchTerm: string = '';
	private isDeleted: boolean;
	private deletedRows: ComboInfo[] = [];
	private isDisabled: boolean;

	@ViewChild('pagingComboInfoBar') pagingComboInfoBar: TdDataTableComponent;
	@ViewChild('dataComboInfoTable') dataComboInfoTable: TdDataTableComponent;

	@ViewChild('dataTable') dataTable: TdDataTableComponent;
	@ViewChild('pagingBar') pagingBar: TdPagingBarComponent;

	columnsComboInfoConfig: ITdDataTableColumn[] = [
		{ name: 'product.name', label: 'Product Name', sortable:true, width: 300 },
		{ name: 'price', label: 'Price', sortable:true, width: 83 },
		{ name: 'quantity', label: 'QTY', sortable:true, width: 50 },
		{ name: 'discountAmount', label: 'Dis %', sortable:true, width: 55 },
		{ name: 'edit', label: 'Edit', sortable: true, width: 45 },
		{ name: 'id', label: 'Delete', sortable: true },
	];

	columnsConfig: ITdDataTableColumn[] = [
		{ name: 'name', label: 'Product Name', sortable: true, width: 305 },
		{ name: 'price', label: 'Price', sortable:true, width: 95 },
		{ name: 'id', label: '', width: 90, sortable: false },
	];

	constructor(protected comboService: ComboService,
		protected vendorAdminService: VendorAdminService,
		protected comboInfoService: ComboInfoService,
		protected productService: ProductService,
		protected authService: AuthService,
		protected route: ActivatedRoute,
		protected router: Router,
		protected dialogService: TdDialogService,
		protected snackBar: MatSnackBar,
		public dialog: MatDialog,
		public dialogAddProduct: MatDialog) {
		super(productService, router, dialogService, snackBar);
	}

	refreshData() {
		if (this.route.snapshot.queryParams["edit"] == '1') {
			this.loadAllComboInfo();
		} else {
			this._allInfo = [];
		}
	}

	loadAllComboInfo() {
		this.comboInfoService.getAllComboInfo(this.comboService.combo.id).subscribe(
			data => {
				this._allInfo = data;
			});
	}

	ngOnInit() {
		this.pageSize = this.pagingBar.pageSize;
		this.user = this.authService.getCurrentLoggedInUser();
		this.refreshData();
	//	this.refreshData();
		super.ngOnInit();
	}

	
	loadAllProducts() {
		this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId=>{
			this.productService.getAllProductsByVendorId(0, environment.maxListFieldOptions,vendorId, "", "","")
			.subscribe(data => this._allProducts = data["content"]);
		});
	}

	getLeafProductInstance() {
		let product = new Product();
		return product;
	}

	// search(searchTerm:string ){
	// 	if(this.isSearchTermCleared(searchTerm))
	// 	{
	// 		this.refreshData();
	// 	} 
	// }

	onSave() {
		let totalPrice: number = 0;
		this.validateSelectedComboInfo();
		if (this.isDeleted) {
			this.comboInfoService.delete(this.deletedRows).subscribe(
				data => {
					this.deletedRows = [];
				}
			)
			this.isDeleted = false;
		}

		for (let counter = 0; counter < this._allInfo.length; counter++) {
			totalPrice = Math.round((totalPrice + this._allInfo[counter].price) * 100) / 100;
		}
		this.comboService.combo.price = totalPrice;
		this.comboService.combo.listOfComboInfo = this._allInfo;

	}

	onReset() {
		this._allInfo = [];
		this.dataComboInfoTable.refresh();
	}
	validateSelectedComboInfo() {
		if (this._allInfo == null || this._allInfo.length == 0) {
			this.snackBar.open("At least one product required for the bundle.", "Close", { duration: 3000 })
			throw new Error("At least one product required for the bundle.");
		}
	}


	onDeleteComboInfo(row: ComboInfo) {

		this.dialogService.openConfirm({

			message: "Are you sure you want to delete this row ?",
			title: "confirmation",
			cancelButton: "Disagree",
			acceptButton: "Agree"
		}).afterClosed().subscribe((accept: boolean) => {
			if (accept) {
				this.deletedRows.push(row);
				let unSelected = this._allInfo.filter(item => this.deletedRows.indexOf(item) < 0);
				this._allInfo = unSelected;
				this.isDeleted = true;
			}
		}
		);
	}




	onEditDialog(id: number, row: ComboInfo) {
		this.isDisabled = true;
		this.product = row.product.name;
		this.quantity = row.quantity;
		this.discountAmount = row.discountAmount;

		this.openDialog(row);

	}

	filterByVendor(pageNumber: number, pageSize: number,
		searchTerm: string, sortBy: string, sortOrder: string, vendorId: number) {
		this.productService.getAllProductsByVendorId(pageNumber, pageSize, vendorId, searchTerm, sortBy, sortOrder).subscribe(
			data => {
				this._allProducts = data["content"];
				this.filteredTotal = data["totalElements"]
			}
		);
	}


	openDialog(row: ComboInfo): void {

		let dialogRef = this.dialog.open(DialogComboInfo, {
			width: '500px',
			data: { isDisabled: this.isDisabled, product: this.product, products: this.products, quantity: this.quantity, discountAmount: this.discountAmount }
		});

		dialogRef.afterClosed().subscribe(result => {
			let temProduct: Product = this._allProducts.find(product => product.name === result.product);
			if (row != null) {
				row.quantity = result.quantity;
				row.discountAmount = result.discountAmount;
				row.price = (Math.round((result.quantity * temProduct.price - ((result.quantity * temProduct.price) * result.discountAmount) / 100) * 100) / 100);
			}
			else {

				if (!this._allInfo.find(product => product.product.name === temProduct.name)) {
					this.price = (Math.round((result.quantity * temProduct.price - ((result.quantity * temProduct.price) * result.discountAmount) / 100) * 100) / 100);
					this._allInfo.push(this.getLeafProductComboInstance(this.price, result.quantity, result.discountAmount, temProduct));
					// this.totalPrice += this.price;
				}
				else {
					this.snackBar.open("This product is already added.", "close", { duration: 3000 })
				}
			}
			this.dataComboInfoTable.refresh();
		});
	}

	openProductDialog(row: Product): void {

		let dialogRef = this.dialogAddProduct.open(DialogAddProductCombo, {
			width: '500px',
			data: { isDisabled: this.isDisabled, productName: row.name, productPrice: row.price }
		});

		dialogRef.afterClosed().subscribe(result => {
			this.price = (Math.round((result.quantity * row.price - ((result.quantity * row.price) * result.discountAmount) / 100) * 100) / 100);
			this._allInfo.push(this.getLeafProductComboInstance(this.price, result.quantity, result.discountAmount, row));



			this.dataComboInfoTable.refresh();
		});
	}

	getLeafProductComboInstance(price: number,
		quantity: number,
		discountAmount: number,
		product: Product) {
		let comboInfo = new ComboInfo(price, quantity, discountAmount, product);
		return comboInfo;
	}

	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}
	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

	public get currentPage(): number {
		return this._currentPage;
	}

	public set currentPage(value: number) {
		this._currentPage = value;
	}

	public get fromRow(): number {
		return this._fromRow;
	}

	public set fromRow(value: number) {
		this._fromRow = value;
	}

	public get allModels(): Product[] {
		return this._allProducts;
	}

	public set allModels(value: Product[]) {
		this._allProducts = value;
	}

	public get errors(): any {
		return this._errors;
	}

	public set errors(value: any) {
		this._errors = value;
	}

	public get selectedRows(): ComboInfo[] {
		return this._selectedRows;
	}

	public set selectedRows(value: ComboInfo[]) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string {
		return this._sortBy;
	}

	public set sortBy(value: string) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder) {
		this._sortOrder = value;
	}

	public get searchTerm(): string {
		return this._searchTerm;
	}

	public set searchTerm(value: string) {
		this._searchTerm = value;
	}



}


@Component({
	selector: 'combo-dialog',
	templateUrl: 'combo-dialog.html',
})
export class DialogComboInfo {
	@ViewChild("comboDialogForm") comboDialogForm: NgForm;

	constructor(
		public dialogRef: MatDialogRef<DialogComboInfo>,
		@Inject(MAT_DIALOG_DATA) public data: any) { }

	discountItemField = new FormControl('', [
		Validators.required,
		Validators.min(0),
		Validators.max(100)]);

	onNoClick(): void {
		this.dialogRef.close();
	}

}

@Component({
	selector: 'product-combo-dialog',
	templateUrl: 'product-combo-dialog.html',
})
export class DialogAddProductCombo {
	@ViewChild("dialogAddProductCombo") dialogAddProductCombo: NgForm;
	myControl = new FormControl();

	constructor(
		public dialogRef: MatDialogRef<DialogAddProductCombo>,
		@Inject(MAT_DIALOG_DATA) public data: any) { }

	discountItemField = new FormControl('', [
		Validators.required,
		Validators.min(0),
		Validators.max(100)]);

	onNoClick(): void {
		this.dialogRef.close();
	}

}