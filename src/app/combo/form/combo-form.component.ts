import { Component, OnInit, Inject, ViewChild, Input, AfterViewInit } from '@angular/core';
import {FormControl, Validators, NgForm} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { Combo } from '../combo.model';
import { ComboService } from '../service/combo.service';
import { DatePipe } from '@angular/common';
import { ViewComboInfoComponent } from '../view/view-products/view-comboinfo.component';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation, NgxGalleryComponent } from 'ngx-gallery';
import { TdDialogService } from '@covalent/core';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';
import { PosGroupManagementService } from '../../posgroupmanagement/service/pos-group-management.service';
import { User } from '../../auth/user.model';
import { AuthService } from '../../auth/service/auth.service';
import { PosGroupManagement } from '../../posgroupmanagement/pos-group-management.model';

@Component({
  selector: 'app-combo-form',
  templateUrl: './combo-form.component.html',
  styleUrls: ['./combo-form.component.css']
})
export class ComboFormComponent extends BaseFormComponent implements OnInit {

  @ViewChild("comboForm") comboForm : NgForm;
  @ViewChild("ngxGallery") ngxGallery:NgxGalleryComponent ;
  @ViewChild(ViewComboInfoComponent) viewComboInfoComponent : ViewComboInfoComponent;

  

  _combo :Combo;
  strDate:Date;
  enDate:Date;
  loggedUser:User;
  selectedGroup:number[]=[];

  @Input()
  _isEdit:boolean = false;
  startDateControl:FormControl;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[]=[];
  listOfPosGroup:PosGroupManagement[]=[];
  
  attImagesAsFiles:File[] = [];
  imageUrlsArray:string[]=[];

  posGroupForm: FormControl=null;
  posGroup:PosGroupManagement[]=[];
  
  constructor(protected comboService: ComboService,
              protected route:ActivatedRoute,
              protected router:Router,
              private datePipe:DatePipe,
              protected snackBar:MatSnackBar,
              protected dialogService : TdDialogService,
              protected vendorAdminService:VendorAdminService,
              protected posGroupManagementService:PosGroupManagementService,
              protected authService:AuthService) { 

    super(comboService, 
      route,
      router,
      snackBar);
  }
  ngOnInit() {
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    if(this.route.snapshot.queryParams["edit"] == '1') {
      this._isEdit = true;
      this._combo = this.comboService.combo;
      this._combo.posGroup.forEach(obj=>{
        this.selectedGroup.push(obj.id);
      })
      this.strDate = new Date(this.datePipe.transform(this._combo.startDate, 'mediumDate'));
      if(this._combo.endDate != null)
        this.enDate = new Date(this.datePipe.transform(this._combo.endDate, 'mediumDate'));

    } else {
      this._combo = new Combo();
      this.selectedGroup=null;
    }

    this.fillImages();
    this.initGalleryOptions();
    this.initGalleryImages();
    this.loadPosGroup();
  }

  fillImages() {
    if(this._isEdit && this._combo.logoImageName != null) {
      let imageNamesArr = this._combo.logoImageName.split(";");

      imageNamesArr.forEach((imageName)=> {
        let imgURL = this.comboService.getAttachmentUrl(imageName);
        this.imageUrlsArray.push(imgURL);

        this.comboService.getAttachment(imgURL).subscribe((blob:Blob) => {
          let fileReader = new FileReader();
          fileReader.onload = () => {
            this.attImagesAsFiles.push(new File([fileReader.result], imageName));
          };
          let img = new Image();
          img.src = imgURL;
          if(blob) {
            fileReader.readAsArrayBuffer(blob);
          }
        });
      });
    }
  }

  initGalleryOptions() {
    this.galleryOptions = [
      {
          previewInfinityMove:true,
          imageArrowsAutoHide:true,
          imageInfinityMove:true,
          previewCloseOnEsc:true,
          previewKeyboardNavigation:true,
          previewZoom: true,
          width: '400px',
          height: '300px',
          thumbnails: false,
          imageAnimation: NgxGalleryAnimation.Slide,
          imageSize: 'contain'
      },
      // max-width 800
      {
          breakpoint: 800,
          width: '100%',
          height: '400px',
          imagePercent: 80
      },
      // max-width 400
      {
          breakpoint: 400,
          preview: false
      }
    ];
  }

  onDeleteImage() {
    this.dialogService.openConfirm({
        message: "Are you sure you want to delete the selected image?",
        title: "Confirmation",
        cancelButton: "Disagree",
        acceptButton: "Agree"
      }).afterClosed().subscribe((accept:boolean) => {
          if(accept) {
            this.attImagesAsFiles.splice(this.ngxGallery.selectedIndex, 1); 
            this.ngxGallery.images.splice(this.ngxGallery.selectedIndex, 1);
          }
      });
  }

  
  loadPosGroup() {
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(
      vendorId => {
        this.posGroupManagementService.getGroupsByVednor(vendorId).subscribe(data => {
          this.listOfPosGroup=data;
        });

      });

}


  onSaveWithAttachments(){
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId=>{
      this.validateSelectedDates();
      this._combo.vendor.id=vendorId;
      this.comboService.combo = this._combo;
      this.setComboDates();
      this.viewComboInfoComponent.onSave();
      super.onSaveWithAttachments(this.attImagesAsFiles);
    })
  }
  onSaveAndNewWithAttachments(){
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId=>{
      this._combo.vendor.id=vendorId;
      this.validateSelectedDates();
      this.comboService.combo = this._combo;
      this.setComboDates();
      this.viewComboInfoComponent.onSave();
      super.onSaveAndNewWithAttachments(this.attImagesAsFiles);
      
    })

  }
  onBeforeEditWithAttachments(){
    this.showSpinner = true;
    this.validateSelectedDates();
  }
  onSaveAndNewWithAttachmentsCompleted() {
    this.resetAttachmentsFields();
    this.viewComboInfoComponent.onReset();
  }

  
  onSaveWithAttachmentsCompleted() {
    this.resetAttachmentsFields();
  }

  resetAttachmentsFields() {
    this.attImagesAsFiles=[];
    this.ngxGallery.images = [];
  }

  onSelectImages(selectedImage:File|FileList) {
    this.resetAttachmentsFields();
    if(selectedImage instanceof File) {
      this.attImagesAsFiles.push(selectedImage);
      
      let fileReader = new FileReader();
      fileReader.onload = () => {
        let image = new Image();
        image.src = fileReader.result;

        this.ngxGallery.images.push({
          small:image.src,
          medium:image.src,
          big:image.src
        });
      }
      fileReader.readAsDataURL(selectedImage);
    } else {
      for(let i:number = 0; i< selectedImage.length; i++) {
        this.attImagesAsFiles.push(selectedImage[i]);

        let fileReader = new FileReader();
        fileReader.onload = () => {
          let image = new Image();
          image.src = fileReader.result;

          this.ngxGallery.images.push({
            small:image.src,
            medium:image.src,
            big:image.src
          });
        }
        fileReader.readAsDataURL(selectedImage[i]);
      }
    }
  }

  onChangeTab(){
    //this.viewComboInfoComponent.loadAllComboInfo();
  }
  initGalleryImages() {
    for(let i=0; i<this.imageUrlsArray.length; i++) {
      
      this.galleryImages.push({
        small:this.imageUrlsArray[i],
        medium:this.imageUrlsArray[i],
        big:this.imageUrlsArray[i]
      });
    }
  }

  // onBeforeSave(combo:Combo) {
  //   this.validateSelectedDates();
  //   this.comboService.combo = this._combo;
  //   this.setComboDates();
  //   this.viewComboInfoComponent.onSave();
  // }

  // onBeforeEdit(){
  //   this.validateSelectedDates();
  //   this.comboService.combo = this._combo;
  //   this.setComboDates();
  //   this.viewComboInfoComponent.onSave();
  // }
  onBeforeSaveAndNew() {
    debugger;
    this.showSpinnerOnSaveAndNew = true;
    this.validateSelectedDates();
  }

  validateSelectedDates(){
    let tempCurrentDate :Date = new Date();
    tempCurrentDate.setHours(0);
    tempCurrentDate.setMinutes(0);
    tempCurrentDate.setSeconds(0);
    tempCurrentDate.setMilliseconds(0);

    if(this.enDate !== null && this.strDate > this.enDate){
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("End date must be after than start date.", "Close", {duration: 3000})
      throw new Error("End date must be after than start date.");
    }
  }

  setComboDates(){
    this._combo.startDate = this.strDate;
    this._combo.endDate = this.enDate;
  }

  onChange(event: any): void {
    event.srcElement.value = "";
  }


  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
  
  get model() : Combo {
    return this._combo;
  }

  set model(combo:Combo){
    this._combo = combo;
  }

  initModel(): Combo {
    return new Combo();
  }

  ngFormInstance(): NgForm {
    return this.comboForm;
  }

  

}
// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};