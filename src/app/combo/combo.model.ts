import { ComboInfo } from "./ComboInfo.model";
import { PosGroupManagement } from "../posgroupmanagement/pos-group-management.model";
import { Vendor } from "../vendor/vendor.model";

export class Combo {

    id: number;
    inactive: boolean;
    name: string;
    price: number = 0;
    startDate: Date;
    endDate: Date;
    logoImageName: string;
    listOfComboInfo: ComboInfo[] = [];
    posGroup: PosGroupManagement[] = [];
    vendor: Vendor;

    constructor() {
        this.vendor = new Vendor();
    }


}