import { Product } from "../product/product";

export class ComboInfo {

    id: number;
    inactive:boolean;

    
    constructor (public price:number,
                 public quantity:number,
                 public discountAmount:number,
                 public product:Product){
    }
    

}