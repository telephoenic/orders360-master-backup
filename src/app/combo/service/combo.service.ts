import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { Combo } from '../combo.model';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { environment } from '../../../environments/environment';
import { Observable } from '../../../../node_modules/rxjs/Observable';

@Injectable()
export class ComboService extends BaseService{
  combo:Combo;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
              protected authService:AuthService) {
    super(http, "combo", authService);
  }


  
  getAllCombosByVendorId(pageNum:number, 
                         pageSize:number,
                         vendorId:number,
                         searchTerm:string,
                         sortBy:string,
                         sortOrder:string ) : Observable<any>{
    return this.http.get(environment.baseURL + "combo/combo_for_vendor/"+ pageNum+"/"+pageSize+"/"+vendorId,
    { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder), 
      headers: this.getBasicHeaders()});
  }
  
  get model() {
    return this.combo;
  }

  set model(model:BaseModel) {
    this.combo = <Combo>model;
  }
}