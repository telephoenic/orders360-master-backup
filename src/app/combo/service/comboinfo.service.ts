import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';

import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { ComboInfo } from '../ComboInfo.model';
import { environment } from '../../../environments/environment';
import { Observable } from '../../../../node_modules/rxjs/Observable';

@Injectable()
export class ComboInfoService extends BaseService{
  comboinfo:ComboInfo;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
              protected authService:AuthService) {
    super(http, "combo_info", authService);
  }

  getAllComboInfo(comboId:number) : Observable<any>{
    return this.http.get(environment.baseURL + "combo_info/items"+"/"+comboId);
  }

  get model(){
    return this.comboinfo;
  }
  set model(model:BaseModel) {
    this.comboinfo = <ComboInfo>model;
  }
}