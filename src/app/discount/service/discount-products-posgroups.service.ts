import { Injectable } from "@angular/core";
import { BaseService } from "../../common/service/base.service";
import { DiscountProductsPosGroups } from "../model/discount-products-posgroups.model";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../../auth/service/auth.service";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";


@Injectable()
export class DiscountProductsPosGroupsService extends BaseService{
    discountProductsPosGroups: DiscountProductsPosGroups;
    navigateToAfterCompletion: string;

    constructor(protected http: HttpClient,
        protected authService: AuthService) {
        super(http, "pos_group_management", authService);
      }

      public validateIfProductsHaveDiscountOnSamePosGroup(productsIds:number[],
                                                                  discountId:number,
                                                                  posGroupId:number) : Observable<any>{
           return  this.http.post(environment.baseURL + "discount_product_posgroup/products-has-discount/"
                           + posGroupId + "/" + discountId,productsIds);                                                          

      }

      public countPosGroupsHasDiscountsOnProducts(discountId: number): Observable<any>{
        return this.http.get(environment.baseURL + "discount_product_posgroup/check-discount-has-products/"
                             + discountId);
      }
      get model() {
        return this.discountProductsPosGroups;
      }
      set model(model: DiscountProductsPosGroups) {
        this.discountProductsPosGroups = model;
      }

  
}