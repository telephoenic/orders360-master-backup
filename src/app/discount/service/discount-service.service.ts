import { BaseService } from "../../common/service/base.service";
import { Injectable } from "@angular/core";
import { AuthService } from "../../auth/service/auth.service";
import { HttpClient } from "@angular/common/http";
import { Discount } from "../model/discount-model";
import { TdDataTableSortingOrder } from "@covalent/core";
import { environment } from "../../../environments/environment";


@Injectable()
export class DiscountService extends BaseService{

    discount: Discount;
    navigateToAfterCompletion: string;

    constructor(protected http: HttpClient,
      protected authService: AuthService) {
      super(http, "discount", authService);
    }

    public getDiscounts(pageNumber:number, pageSize:number,vendorId:number
                        ,searchTerm:string,sortBy:string,sortOrder:TdDataTableSortingOrder){
           return this.http.get<Discount>(environment.baseURL + "discount/discounts_for_vendor/"
           +pageNumber + "/" + pageSize + "/" + vendorId,
           {params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
            headers: this.getBasicHeaders()});                 

    }

    public saveDsicount(discount: Discount,
                        deassignedProductsIds:number[],
                        deassignedposGroupssIds:number[]){
             let parameterList: { paramName: string, paramValue: any }[] = [];   
             parameterList.push({paramName :'deassignedProducts', paramValue :deassignedProductsIds})   
             parameterList.push({paramName :'deassignedPosGroups', paramValue :deassignedposGroupssIds})         
      return this.http.post<Discount>(environment.baseURL + "discount/save-discount", discount,
                        {params: this.prepareSearchParameters('', '', '',parameterList),
                         headers: this.getBasicHeaders()});
    }

    public updateDiscount(discount: Discount){

      return this.http.post<Discount>(environment.baseURL + "discount/update-discount" ,discount);
    }

    get model() {
        return this.discount;
      }
      set model(model: Discount) {
        this.discount = model;
      }
    
  
} 