import { BaseViewComponent } from "../../common/view/base-view-component";
import { OnInit, Component, ViewChild } from "@angular/core";
import { TdPagingBarComponent, TdDataTableComponent, TdDataTableSortingOrder, ITdDataTableColumn, TdDialogService } from "@covalent/core";
import { Discount } from "../model/discount-model";
import { DiscountService } from "../service/discount-service.service";
import { AuthService } from "../../auth/service/auth.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { NgxPermissionsService } from "ngx-permissions";
import { environment } from "../../../environments/environment";


@Component({
    selector: 'app-all-discounts-view',
    templateUrl: '../../discount/view/all-discounts-component.html',
    styleUrls: ['../../discount/view/all-discounts-component.css']
  })

  export class AllDiscountsViewComponent extends BaseViewComponent implements OnInit{
      
    private _currentPage: number = 1;
    private _fromRow: number = 1;
  
    private _allDiscounts: Discount[] = [];
    private _errors: any = '';
    private _selectedRows: Discount[] = [];
    private _pageSize: number;
    private _filteredTotal: number;
    private _sortBy: string = '';
    private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
    private _searchTerm: string = '';
    @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
    @ViewChild('dataTable') dataTable: TdDataTableComponent;
  
    columnsConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name' ,tooltip:"Name"}, 
    { name: 'discountValue', label: 'Discount %', tooltip:'Discount %',width:100},
    { name: 'startDate', label: 'Start Date' , format: v => this.datePipe.transform(v, 'mediumDate'), tooltip:"Start Date", width:160},
    { name: 'endDate', label: 'End Date' , format: v => this.datePipe.transform(v, 'mediumDate'), tooltip:"End Date", width:160},
    { name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive", width:100 },
    { name: 'id', label: 'Edit',tooltip:"Edit",sortable:false, width:100 }
    ];

    ngOnInit(){
        super.ngOnInit();
    }
    
    constructor(protected discountService: DiscountService,
      protected authService: AuthService,
      protected vendorAdminService: VendorAdminService,
      protected datePipe:DatePipe,
      protected router: Router,
      protected dialogService: TdDialogService,
      protected snackBar: MatSnackBar,
      protected permissionsService: NgxPermissionsService) {
      super(discountService, router, dialogService, snackBar);
    }
  
    filterByVendor(pageNumber: number,
      pageSize: number,
      searchTerm: string,
      sortBy: string,
      sortOrder: string,
      vendorId: number) {
        let currentUser = this.vendorAdminService.getCurrentLoggedInUser();  
        if(currentUser.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin){
          this.vendorAdminService.getVendorByVendorAdminId(currentUser.id)
          .subscribe(result=>{
            vendorId = result;
            this.discountService.getDiscounts(this.currentPage -1,this.pageSize,
              vendorId,this.searchTerm,this.sortBy,this.sortOrder)
              .subscribe(
                data=>{
                  this.allModels = data['content'];
                  this.filteredTotal = data['totalElements'];
                }
              );
          });

    }
  }
  
    getDiscountsByVendor(){
      let currentUser = this.vendorAdminService.getCurrentLoggedInUser();
      let vendorId;

        if(currentUser.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin){
            this.vendorAdminService.getVendorByVendorAdminId(currentUser.id)
            .subscribe(result=>{
              vendorId = result;
              this.discountService.getDiscounts(this.currentPage -1,this.pageSize,
                vendorId,this.searchTerm,this.sortBy,this.sortOrder)
                .subscribe(
                  data=>{
                    this.allModels = data['content'];
                    this.filteredTotal = data['totalElements'];
                  }
                );
            });
            
        }
    }
    
    
    getPagingBar(): TdPagingBarComponent {
        return this.pagingBar;
      }
      getDataTable(): TdDataTableComponent {
        return this.dataTable;
      }
    
      public get currentPage(): number {
        return this._currentPage;
      }
    
      public set currentPage(value: number) {
        this._currentPage = value;
      }
    
      public get fromRow(): number {
        return this._fromRow;
      }
    
      public set fromRow(value: number) {
        this._fromRow = value;
      }
    
      public get allModels(): Discount[] {
        return this._allDiscounts;
      }
    
      public set allModels(value: Discount[]) {
        this._allDiscounts = value;
      }
    
      public get errors(): any {
        return this._errors;
      }
    
      public set errors(value: any) {
        this._errors = value;
      }
    
      public get selectedRows(): Discount[] {
        return this._selectedRows;
      }
    
      public set selectedRows(value: Discount[]) {
        this._selectedRows = value;
      }
    
      public get pageSize(): number {
        return this._pageSize;
      }
    
      public set pageSize(value: number) {
        this._pageSize = value;
      }
    
      public get filteredTotal(): number {
        return this._filteredTotal;
      }
    
      public set filteredTotal(value: number) {
        this._filteredTotal = value;
      }
    
      public get sortBy(): string {
        return this._sortBy;
      }
    
      public set sortBy(value: string) {
        this._sortBy = value;
      }
    
      public get sortOrder(): TdDataTableSortingOrder {
        return this._sortOrder;
      }
    
      public set sortOrder(value: TdDataTableSortingOrder) {
        this._sortOrder = value;
      }
    
      public get searchTerm(): string {
        return this._searchTerm;
      }
    
      public set searchTerm(value: string) {
        this._searchTerm = value;
      }
    
  }
  