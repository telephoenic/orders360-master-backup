import { BaseModel } from "../../common/model/base-model";
import { Product } from "../../product/product";
import { PosGroupManagement } from "../../posgroupmanagement/pos-group-management.model";
import { Discount } from "./discount-model";

export class DiscountProductsPosGroups{
    id: number;   
    inactive: boolean;
    product: Product;
    posGroup: PosGroupManagement;
    discount: Discount;

 
    constructor(id?:number,
                inactive?:boolean,
                product?:Product,
                posGroup?:PosGroupManagement,
                discount?:Discount){
                    this.id = id;
                    this.inactive = inactive;
                    this.product = product;
                    this.posGroup = posGroup;
                    this.discount = discount;
    }
}