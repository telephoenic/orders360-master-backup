import { BaseModel } from "../../common/model/base-model";
import { Vendor } from "../../vendor/vendor.model";
import { Product } from "../../product/product";
import { PosGroupManagement } from "../../posgroupmanagement/pos-group-management.model";
import { DiscountProductsPosGroups } from "./discount-products-posgroups.model";


export class Discount{
    id: number;
    inactive: boolean;
    name: string;
    startDate: any;
    endDate: any;
    discountValue: number;
    vendor: Vendor;
    discountProductPosGroup: DiscountProductsPosGroups[];

    constructor(id?: number, name?: string, startDate?:any, endDate?: any,discountValue?: number, discountProductsPosGroup? : DiscountProductsPosGroups[]) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.vendor = new Vendor();
        this.discountValue = discountValue;
        this.discountProductPosGroup = discountProductsPosGroup;
    }
}