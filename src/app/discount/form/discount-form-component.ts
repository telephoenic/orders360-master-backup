import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { NgForm } from '@angular/forms';
import { TdDataTableComponent, TdDialogService } from '@covalent/core';
import { DiscountService } from '../service/discount-service.service';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';
import { AuthService } from '../../auth/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatSnackBar } from '@angular/material';
import { ProductService } from '../../product/service/product.service';
import { VendorService } from '../../vendor/Services/vendor.service';
import { Discount } from '../model/discount-model';
import { Vendor } from '../../vendor/vendor.model';
import { BaseModel } from '../../common/model/base-model';
import { ProductDiscountComponent } from '../product-discount/product-discount-component';
import { User } from '../../auth/user.model';
import { Product } from '../../product/product';
import { SelectItem } from 'primeng/components/common/selectitem';
import { PosGroupManagement } from '../../posgroupmanagement/pos-group-management.model';
import { PosGroupManagementService } from '../../posgroupmanagement/service/pos-group-management.service';
import { ThrowStmt } from '@angular/compiler';
import { DiscountProductsPosGroups } from '../model/discount-products-posgroups.model';
import { DiscountProductsPosGroupsService } from '../service/discount-products-posgroups.service';
import { HttpErrorResponse } from '@angular/common/http';
import { GroupedObservable } from 'rxjs';
import { timingSafeEqual } from 'crypto';
@Component({
  selector: 'app-discount-form',
  templateUrl: '../../discount/form/discount-form-component.html',
  styleUrls: ['../../discount/form/discount-form-component.css']
})
export class DiscountFormComponent extends BaseFormComponent implements OnInit {
  initModel(): Discount {
    return new Discount();
  }
  ngFormInstance(): NgForm {
    return this.discountForm;
  }
  @ViewChild("discountForm") discountForm: NgForm;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;
  @ViewChild(ProductDiscountComponent) productDiscountView: ProductDiscountComponent;

  private user: User;
  private _discount: Discount;
  private _isEdit: boolean = false;
  private _vendorId: number;
  private posGroupId: number;
  private _vendor: Vendor;
  selectedPosGroup: SelectItem;
  groups: SelectItem[] = [];
  private posGroups: PosGroupManagement[];
  pageNumber:number = 1;
  pageSize = 25;
  totalProductsForDiscount: number = 0;
  constructor(
    protected discountService: DiscountService,
    protected vendorAdminService: VendorAdminService,
    protected authService: AuthService,
    protected route: ActivatedRoute,
    protected router: Router,
    private datePipe: DatePipe,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    protected productService: ProductService,
    protected posGroupManagementService: PosGroupManagementService,
    protected discountProductsPosGroupsService: DiscountProductsPosGroupsService,
    protected vendorService: VendorService) {

    super(discountService,
      route,
      router,
      snackBar);

  }
  ngOnInit() {
    this.getVendorIdByLoggedVenodrAdmin();
    if (this.route.snapshot.queryParams["edit"] == '1') {
      this._isEdit = true;
      this._discount = this.discountService.discount;
      this._discount.startDate = new Date(this.datePipe.transform(this._discount.startDate, 'mediumDate'));
      if (this._discount.endDate != undefined) {
        this._discount.endDate = new Date(this.datePipe.transform(this._discount.endDate, 'mediumDate'));
      }
    } else {
      this._discount = new Discount();
    }
  }

  getVendorIdByLoggedVenodrAdmin() {
    this.user = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService.getVendorByVendorAdminId(this.user.id)
      .subscribe((vendorId: number) => {
        console.log(vendorId);
        this._vendorId = vendorId;
        this.loadPosGroupsOptions(vendorId);
      });

  }

  onSelectButtonPosGroupChange(event) {
    this.productDiscountView.getPagingBar().navigateToPage(1);
    this.productDiscountView.selectedRows = [];
    this.productDiscountView.searchTerm = '';
    this.posGroupId = event.value;
    if(this.productDiscountView.checkProductIsExist){
      this.productDiscountView.loadSelectedProducts();
    }
    this.loadSelectedProducts();
    if (!this.productDiscountView.assigndProductsMap.has(this.posGroupId)) {
      this.productDiscountView.assigndProductsMap.set(this.posGroupId, []);
    }
    this.productDiscountView.getProducts(this.productDiscountView.currentPage - 1, this.productDiscountView.pageSize);
    this.productDiscountView.listOfAssignedProducts = this.productDiscountView.assigndProductsMap.get(this.posGroupId);
  }

  loadSelectedProducts() {
    if (this.productDiscountView.assigndProductsMap.get(this.posGroupId) == undefined && this._discount.id != undefined) {
      this.productService.getSelectedProductsForDiscount(this.posGroupId, this._discount.id).subscribe(
        data => {
          this.productDiscountView.listOfAssignedProducts = data;
          this.productDiscountView.assigndProductsMap.set(this.posGroupId, data);
        });
    }
  }

  loadPosGroupsOptions(vendorId: number) {

    this.posGroupManagementService.getGroupsByVednor(vendorId).subscribe(
      data => {
        this.posGroups = data;
        this.posGroups.forEach((posGroupObj: PosGroupManagement) => {
          this.groups.push({ label: posGroupObj.name, value: posGroupObj.id });
        });
      });

  }

  setDiscountProductsPosGroupsList() {
    let discountProductsPosGroups: DiscountProductsPosGroups[] = [];
    this.posGroups.forEach(posGroup => {
      if (this.productDiscountView.assigndProductsMap.get(posGroup.id) == undefined) {
        this.productDiscountView.assigndProductsMap.set(posGroup.id, []);
      }
      this.productDiscountView.assigndProductsMap.get(posGroup.id).forEach(product => {
        let discountProductPosGroup: DiscountProductsPosGroups = new DiscountProductsPosGroups();
        discountProductPosGroup.posGroup = posGroup;
        discountProductPosGroup.product = product;
        discountProductPosGroup.discount = null;
        discountProductsPosGroups.push(discountProductPosGroup);
      });
    });
    this._discount.discountProductPosGroup = discountProductsPosGroups;

  }

  /*checkDiscountHasProductsInAddCase(){
    this.posGroups.forEach(posGroup =>{
      if(this.productDiscountView.assigndProductsMap.has(posGroup.id)){
        debugger;
        if(this.productDiscountView.assigndProductsMap.get(posGroup.id).length > 0){
          this.isDiscountHasProducts = true;
          return;
        }else{
          if(this.isEdit)
          this.isDiscountHasProducts = false;
        }
      }
    });
  }*/

  onDiscountPercentageChange(event: any) {
    this._discount.discountValue = event;
    this.productDiscountView.assigndProductsMap.forEach((products:any[],posGroupId) =>{
        this.productDiscountView.assigndProductsMap.get(posGroupId).forEach(
          product=>{
            product.priceWithDiscount = product.price - (product.price * (this._discount.discountValue/ 100));
          });
    });
  }

  onBeforeSave() {
    this.showSpinner = true;
    if (this.productDiscountView.numberOfProducts < 1) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("Discount must be have at least one product.", "Close", { duration: 3000 })
      throw new Error("Discount must be have at least one product.");
    } else {
      this.validateSelectedDates();
      this._discount.vendor.id = this._vendorId;
      this.setDiscountProductsPosGroupsList();

    }
  }

  onBeforeEdit() {
    this.showSpinner = true;
    if (this.productDiscountView.numberOfProducts < 1) {
      this.showSpinner = false;
      this.snackBar.open("Discount must be have at least one product.", "Close", { duration: 3000 })
      throw new Error("Discount must be have at least one product.");
    }
    this.validateSelectedDates();
    let vendor: Vendor = new Vendor();
    this._discount.id = this.productDiscountView.discountId;
    vendor.id = this._vendorId;
    this._discount.vendor = vendor;
    this.setDiscountProductsPosGroupsList();

  }

  onSaveAndNew() {
    this.onBeforeSave();
    this.showSpinner = false;
    this.showSpinnerOnSaveAndNew = true;
    if (this.productDiscountView.assigndProductsMap.size >= 1) {
      this.validateSelectedDates()
      this._discount.vendor.id = this._vendorId;
      this.setDiscountProductsPosGroupsList();
      this.discountService.saveDsicount(this._discount,
                                        this.productDiscountView.deassignedProductsIds,
                                        this.productDiscountView.deassignedPosGroupsIds).subscribe(
        data => {
          this._discount = this.initModel();
          this.snackBar.open("Saved successfully", "Close", { duration: 3000 });
          this.ngFormInstance().resetForm();
          this.resetAfterSaveNew();
        },
        (errorResponse: HttpErrorResponse) => {
          this.showSpinner = false;
          this.showSpinnerOnSaveAndNew = false;
          this.onSaveAndNewError(errorResponse);
        },
        () => this.onSaveAndNewCompleted()
      );
    } else {
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("Discount must be have at least one product.", "Close", { duration: 3000 })
      throw new Error("Discount must be have at least one product.");
    }
  }

  onCahngeTab() {
    if(this.selectedPosGroup != null){
      this.productDiscountView.getProducts(this.productDiscountView.currentPage - 1,this.productDiscountView.pageSize);
      this.productDiscountView.loadSelectedProducts();  
    }
  }

 /* checkDiscountIfHasProducts() {
    debugger;
    this.posGroups.forEach(posGroup =>{
      if(this.productDiscountView.assigndProductsMap.has(posGroup.id)){
        debugger;
        if(this.productDiscountView.assigndProductsMap.get(posGroup.id).length > 0){
          this.isDiscountHasProducts = true;
          return;
        }else{
          this.isDiscountHasProducts = false;
        }
      }
    })
  }*/

  resetAfterSaveNew() {
    this._discount = new Discount();
    this.posGroupId = 0;
    this.productDiscountView.assigndProductsMap = new Map();
    this.productDiscountView.listOfAssignedProducts = [];
    this.productDiscountView.allProduct = [];
  }


  validateSelectedDates() {
    let tempCurrentDate: Date = new Date();
    tempCurrentDate.setHours(0);
    tempCurrentDate.setMinutes(0);
    tempCurrentDate.setSeconds(0);
    tempCurrentDate.setMilliseconds(0);

    if (this._discount.endDate != undefined && this._discount.startDate > this._discount.endDate) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("End date must be after than start date.", "Close", { duration: 3000 })
      throw new Error("End date must be after than start date.");
    }
  }

  onSaveDiscount() {
    if (!this.isEdit) {
      this.onBeforeSave();
    } else {
      this.onBeforeEdit();
    }
    this.discountService.saveDsicount(this._discount,
                                      this.productDiscountView.deassignedProductsIds,
                                      this.productDiscountView.deassignedPosGroupsIds).subscribe(
      data => this.router.navigateByUrl(this.baseService.navigateToAfterCompletion),
      (errorResponse: HttpErrorResponse) => {
        this.onSaveError(errorResponse);
      },
      () => this.onSaveCompleted()
    );

  }

  onSaveAndNewCompleted() {
    this.selectedPosGroup = null;
    this.showSpinnerOnSaveAndNew = false;
  }

  public get model() {
    return this._discount;
  }

  public set model(value: Discount) {
    this._discount = value;
  }

  public get isEdit() {
    return this._isEdit;
  }

  public set isEdit(value: boolean) {
    this._isEdit = value;
  }
}
