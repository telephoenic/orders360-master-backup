import { Component, ViewChild, Inject, OnInit, Input } from "@angular/core";
import { Discount } from "../model/discount-model";
import { Product } from "../../product/product";
import { environment } from "../../../environments/environment";
import { TdDataTableSortingOrder, TdDialogService, IPageChangeEvent, TdDataTableComponent, TdPagingBarComponent, ITdDataTableColumn, _TdChipsMixinBase, ITdDataTableSortChangeEvent, ITdDataTableColumnWidth } from "@covalent/core";
import { AuthService } from "../../auth/service/auth.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { DiscountService } from "../service/discount-service.service";
import { ActivatedRoute, Router } from "@angular/router";
import { PosTypeService } from "../../pos-user/service/pos-type.service";
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { ProductService } from "../../product/service/product.service";
import { User } from "../../auth/user.model";
import { DatePipe } from "@angular/common";
import { BaseViewComponent } from "../../common/view/base-view-component";
import { DiscountProductsPosGroupsService } from "../service/discount-products-posgroups.service";
import { DiscountProductsPosGroups } from "../model/discount-products-posgroups.model";
import { PosGroupManagementService } from "../../posgroupmanagement/service/pos-group-management.service";
import { debug } from "util";


const DECIMAL_FORMAT: (v: any) => any = (v: number) => Number(Math.round((v * 100)).toFixed(2)) / 100;

@Component({
  selector: 'app-product-discount',
  templateUrl: '../../discount/product-discount/product-discount-component.html',
  styleUrls: ['../../discount/product-discount/product-discount-component.css']
})
export class ProductDiscountComponent extends BaseViewComponent implements OnInit {


  @ViewChild('allProductDt') allProductDt: TdDataTableComponent;
  @ViewChild('selectedProductDt') selectedProductDt: TdDataTableComponent;
  @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
  @ViewChild('confirmationDialog') confirmationDialog: ConfrimationDialog;

  allProductColumnConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name', width: 260 },
    { name: 'price', label: 'Price', format: DECIMAL_FORMAT, width: 90 },
    { name: 'discountValue', label: 'Discount%', width: 90 },
    { name: 'id', label: 'Details', width: 80 }
  ];

  selectedProductColumnConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name', width: 215, },
    { name: 'price', label: 'Price', width: 90 },
    { name: 'priceWithDiscount', label: 'Price With Discount', format: DECIMAL_FORMAT, width: 160 }
  ];

  private _user: User;
  /*private _listOfPosTypes: PosType[];*/
  private _discount: Discount;
  private _errors: any = '';
  private _selectedRows: any[] = [];
  private _assignSelectedRows: Product[] = [];
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _pageSize: number = environment.initialViewRowSize;
  checkProductIsExist: boolean = false;
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private _searchTerm: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _allProducts: Product[] = [];
  private _allProductTemp: Product[] = [];
  private _assignProducts: Product[] = [];
  private _assignProductTemp: Product[] = [];
  private _selectedProductsIds: number[] = [];
  private _productsHaveDiscountOnSamePosGroup: any[] = [];
  tempList: any[] = [];
  deassignedProductsIds: number[] = [];
  deassignedPosGroupsIds: number[] = [];
  @Input()
  discountId: number;

  @Input()
  posGroupId: number;

  @Input()
  discountValue: number;

  _isEdit: boolean = false;
  assigndProductsMap = new Map<number, any[]>();
  listOfAssignedProducts: any[] = [];
  valueSelectedButton: any = -1;
  numberOfProducts: number = 0;
  isSelectedProductsHaveCurrentDiscount: boolean = false;
  countOfProductsHaveAnotherDiscount: number = 0;
  countOfProductshaveNoAnotherDiscount: number = 0;

  constructor(protected authService: AuthService,
    protected vendorAdminService: VendorAdminService,
    protected discountService: DiscountService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    protected discountProdcutsPosGroupsService: DiscountProductsPosGroupsService,
    protected productService: ProductService,
    protected matDialog: TdDialogService,
    protected posGroupManagmentService: PosGroupManagementService) {
    super(discountService, router, dialogService, snackBar);
    this._discount = new Discount();

  }


  ngOnInit() {
    if (this.route.snapshot.queryParams["edit"] == 1) {
      this._isEdit = true;
      this.loadSelectedProducts();
      this.checkDiscountIfHasProductsOnAllPos();
      this.discount = this.discountService.discount;
      console.log(this.numberOfProducts);
    }
  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize = pagingEvent.pageSize;
    this.getProducts(this.currentPage - 1, this.pageSize);
  }

  getProducts(pageNumber: number, pageSize: number) {
    this._user = this.authService.getCurrentLoggedInUser();
    if (this.user.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
      this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(vendorId => {
        let url = "/product_not_for_discount/" + pageNumber + "/" + pageSize + "/" + vendorId + "/" + this.posGroupId;
        this.productService.filterPageable(this.searchTerm, this.sortBy, this.sortOrder, url).subscribe(data => {
          this._allProducts = data['content']
          this._filteredTotal = data['totalElements'];
          console.log(this._allProducts);
        })
      });
    }

  }

  sort(sortEvent: ITdDataTableSortChangeEvent): void {

    this.sortBy = sortEvent.name;
    this.sortOrder = sortEvent.order;

    this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(vendorId => {
      let url = "/product_not_for_discount/" + (this.currentPage - 1) + "/" + this.pageSize + "/" + vendorId + "/" + this.posGroupId;
      this.productService.filterPageable('', this.sortBy, this.sortOrder, url).subscribe(data => {
        this._allProducts = data['content']
        this._filteredTotal = data['totalElements'];
      })
    });
  }

  loadSelectedProducts() {
    if (this.posGroupId != undefined) {
      this.productService.filterNotPageable("", "", "", "/products_for_discount/" + this.posGroupId + "/" + this.discountId)
        .subscribe((data: Product[]) => {
          this.assignProducts = data;
        });
    }
  }

  sortList(sortEvent: ITdDataTableSortChangeEvent) {
    console.log(sortEvent.name)
    if ((sortEvent.order == 'DESC' && sortEvent.name == 'priceWithDiscount')
      || (sortEvent.order == 'DESC' && sortEvent.name == 'price')) {
      this.assigndProductsMap.get(this.posGroupId).sort((a, b) => b[sortEvent.name] - a[sortEvent.name]);
    } else if ((sortEvent.order == 'ASC' && sortEvent.name == 'priceWithDiscount')
      || (sortEvent.order == 'ASC' && sortEvent.name == 'price')) {
      this.assigndProductsMap.get(this.posGroupId).sort((a, b) => a[sortEvent.name] - b[sortEvent.name]);
    }

    if (sortEvent.name == 'name' && sortEvent.order == 'ASC') {
      this.assigndProductsMap.get(this.posGroupId).sort((a, b) => a.name.localeCompare(b.name));
    } else if (sortEvent.name == 'name' && sortEvent.order == 'DESC') {
      this.assigndProductsMap.get(this.posGroupId).sort((a, b) => b.name.localeCompare(a.name));
    }
    this.listOfAssignedProducts = this.assigndProductsMap.get(this.posGroupId);
    this.selectedProductDt.refresh();
  }

  search(searchTerm: string): void {
    this.assigndProductsMap.forEach((assignedProducts, posGroupId) => {
      assignedProducts.forEach(product => {
        if (product.name == searchTerm) {
          searchTerm = '';
        }
      })
    })
    this._user = this.authService.getCurrentLoggedInUser();
    this.getPagingBar().navigateToPage(1);
    if (this.user.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
      this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(vendorId => {
        let url = "/product_not_for_discount/" + 0 + "/" + this.pageSize + "/" + vendorId + "/" + this.posGroupId;
        this.productService.filterPageable(searchTerm, this.sortBy, this.sortOrder, url).subscribe(data => {
          this._allProducts = data['content'];
          this._filteredTotal = data['totalElements'];
        })
      });
    }
  }

  public resetArrays() {
    this.selectedProductsIds = [];
    this.productsHaveDiscountOnSamePosGroup = [];
  }

  public onAssign() {
    this.resetArrays();
    this.selectedRows.forEach(product => {
      if (product.discountName == this.discount.name) {
        this.isSelectedProductsHaveCurrentDiscount = true;
      } else {
        this.isSelectedProductsHaveCurrentDiscount = false;
        if (product.discountName != "")
          this.countOfProductsHaveAnotherDiscount++;
      }
    });
    this.checkProductIsExist = false;
    let count = 0;
    this._selectedRows.forEach(temp => {
      this.checkProductIsExist = false;
      this.selectedProductsIds.push(temp.id);
      this.tempList.push(temp);
      this.isSelectedProductsHaveCurrentDiscount = true;

      count += this.assigndProductsMap.get(this.posGroupId).filter(product => product.id == temp.id).length;
      
      if (!this.assigndProductsMap.get(this.posGroupId).find(product => product.id === temp.id)) {
        if (this.assigndProductsMap.has(this.posGroupId) && !this.checkProductIsExist) {
          if (!this.assigndProductsMap.get(this.posGroupId).find(product => product.id == temp.id)) {
            this.assigndProductsMap.get(this.posGroupId).push(temp);
          } else {
            this.isSelectedProductsHaveCurrentDiscount = false;
          }
        } else {
          if (this.isSelectedProductsHaveCurrentDiscount && this.countOfProductsHaveAnotherDiscount == 0)
            this.snackBar.open("This Product is already added.", "close", { duration: 3000 });
        }
        const index: number = this.allProduct.indexOf(temp);
        console.log(index);
        if (index !== -1) {
          this.allProduct.splice(index, 1);
        }
      } else {
        this.loadSelectedProducts();
        if (count == this._selectedRows.length) {
          this.snackBar.open("This Product is already added.", "close", { duration: 3000 });
          this._selectedRows = [];
          throw new Error("This Product is already added.");
        }
        this.checkProductIsExist = true;
      }
    });

    this.validateIfProductsHaveDiscountOnSamePosGroup();
    this.assigndProductsMap.get(this.posGroupId).forEach(product => {
      product.priceWithDiscount = product.price;
      console.log(product.price);
      product.priceWithDiscount = product.price - (product.price * (this.discountValue / 100));
      product.priceWithDiscount = Number(Math.round((product.priceWithDiscount * 100)).toFixed(2)) / 100;
    });
    if (count == this._selectedRows.length) {
      this.snackBar.open("This Product is already added.", "close", { duration: 3000 });
      this._selectedRows = [];
      throw new Error("This Product is already added.");
    }

    this.assigndProductsMap.set(this.posGroupId, this.listOfAssignedProducts);
    this.assignProducts = [];
    this.tempList = [];
    this.numberOfProducts += this.selectedRows.length;
    this.countOfProductsHaveAnotherDiscount = 0;
    this.getProducts(this._currentPage - 1,this.pageSize);
  }


  public onDeassign() {
    this.numberOfProducts -= this._assignSelectedRows.length;
    this._assignSelectedRows.forEach(temp => {
      if (!this._isEdit) {
        console.log(this._allProducts);
        if (!this.allProduct.find(obj => obj.id === temp.id)) {
          this.allProduct.push(temp)
        } else {
          const index: number = this._allProductTemp.indexOf(temp);
          if (index !== -1) {
            this.allProduct.splice(index, 1);
          }
        }

      } else {
        if (!this.allProduct.find(obj => obj.id === temp.id)) {
          this._allProducts.push(temp)
        }
      }
      const index: number = this.assignProducts.indexOf(temp);
      //this.assigndProductsMap.get(this.posGroupId).splice(index, 1);
      if (index !== -1) {
        this.assignProducts.splice(index, 1);
        this.listOfAssignedProducts.splice(index, 1);
        if (!this._isEdit) {
          this.assignProductTemp.splice(index, 1);
          this.assignProducts.splice(index, 1);
          this.listOfAssignedProducts.splice(index, 1);
        } else {
          this.assigndProductsMap.set(this.posGroupId, this.listOfAssignedProducts);
        }
      }
      this.assignSelectedRows.forEach(product => {
        this.deassignedProductsIds.push(product.id);
        let indexProduct = this.assigndProductsMap.get(this.posGroupId).indexOf(product);
        if (indexProduct !== -1) {
          this.assigndProductsMap.get(this.posGroupId).splice(indexProduct, 1);
        }
      });
      console.log(this.deassignedProductsIds);
      this.deassignedPosGroupsIds.push(this.posGroupId);
      this.selectedProductDt.refresh();
      this.allProductDt.refresh();
      this.selectedProductDt.refresh();
      this._assignSelectedRows = [];
    });
  }

  checkDiscountIfHasProductsOnAllPos() {
    this.discountProdcutsPosGroupsService.countPosGroupsHasDiscountsOnProducts(this.discountId)
      .subscribe(countOfProducts => {
        this.numberOfProducts = countOfProducts;
      });
  }


  filterByVendor(pageNumber: number, pageSize: number,
    searchTerm: string, sortBy: string, sortOrder: string, vendorId: number) {
    let url = "/product_not_for_discount/" + pageNumber + "/" + pageSize + "/" + vendorId;
    this.productService.filterPageable(searchTerm, sortBy, sortOrder, url).subscribe(data => {
      this._allProducts = data['content'];
      this._filteredTotal = data['totalElements'];
    });

  }

  checkValdationAssign() {
    this.onAssignFailed();
  }

  onAssignFailed() {
    if (this.productsHaveDiscountOnSamePosGroup.length > 0) {
      let dialogRef = this.dialogService.open(ConfrimationDialog, {
        width: '500px',
        data: {
          disableClose: true,
          productsIds: this.selectedProductsIds,
          discountId: this.discountId,
          productsHaveDiscountOnSamePosGroup: this.productsHaveDiscountOnSamePosGroup
        }
      });

      dialogRef.afterClosed().subscribe(data => {
        console.log(this.valueSelectedButton);
        this.valueSelectedButton = localStorage.getItem("valueOfSelectedButton");
        let checkIfDialogIsClosed = localStorage.getItem("checkIfDialogIsClosed");
        console.log(localStorage.getItem("valueOfSelectedButton"));
        if (this.valueSelectedButton == 1) {
          this.assignAfterValidation();
          this.valueSelectedButton = -1;
        } else {

          this.afterCancelAssigen();
          console.log("here");
          //this.checkProductIsExist = true;
          this.getProducts(this.currentPage - 1, this.pageSize);
        }
        this.valueSelectedButton = -1;
      });
    } else {
      this.assignAfterValidation();
    }
  }

  afterCancelAssigen() {
    console.log(this.selectedRows);
    console.log(this.assigndProductsMap.get(this.posGroupId));
    if (this.productsHaveDiscountOnSamePosGroup.length > 0) {
      this.selectedRows.forEach(temp => {
        let firstIndex = this.listOfAssignedProducts.indexOf(temp);
        let secondIndex = this.assigndProductsMap.get(this.posGroupId).indexOf(temp);
        if (firstIndex !== -1 && secondIndex !== -1) {
          this.listOfAssignedProducts.splice(firstIndex, 1);
          this.assigndProductsMap.get(this.posGroupId).splice(secondIndex, 1);
        }
      });
    }
    this.tempList = [];
    this.assignProducts = [];
    console.log(this.assigndProductsMap.get(this.posGroupId));
  }

  assignAfterValidation() {
    this.selectedProductDt.refresh();
    this.allProductDt.refresh();
    this.selectedRows = [];
    this.assignProducts.forEach(assignedProduct => { this.selectedProductsIds.push(assignedProduct.id) });
    if (!this.assigndProductsMap.has(this.posGroupId)) {
      this.assigndProductsMap.set(this.posGroupId, this.assignProducts);
    }
    //this.listOfAssignedProducts = this.assignProducts;
    this.listOfAssignedProducts = this.assigndProductsMap.get(this.posGroupId);
  }



  showDetailsForSelectedProduct(row) {
    if (row.discountName == '') {
      this.snackBar.open("No discount for selected product.", "close", { duration: 3000 });
    } else {
      this.dialogService.open(SingleProductHasDiscountDialog, {
        width: '850px',
        data: {
          disableClose: true,
          product: row
        }
      });

    }
  }

  public validateIfProductsHaveDiscountOnSamePosGroup() {
    if (this.discountId == undefined) {
      this.discountId = 0;
    }

    this.discountProdcutsPosGroupsService.validateIfProductsHaveDiscountOnSamePosGroup(
      this.selectedProductsIds, this.discountId, this.posGroupId)
      .subscribe(data => {
        this.productsHaveDiscountOnSamePosGroup = data;
        this.checkValdationAssign();
      });

  }

  public get discount(): Discount {
    return this._discount;
  }
  public set discount(value: Discount) {
    this._discount = value;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }

  getDataTable(): TdDataTableComponent {
    return this.allProductDt;
  }

  public get errors(): any {
    return this._errors;
  }
  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): any[] {
    return this._selectedRows;
  }
  public set selectedRows(value: any[]) {
    this._selectedRows = value;
  }

  public get allModels(): Product[] {
    return null;
  }

  public set allModels(value: Product[]) {
    this._allProducts = value;
  }
  public get filteredTotal(): number {
    return this._filteredTotal;
  }
  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }
  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }
  public set pageSize(value: number) {
    this._pageSize = value;
  }


  public get currentPage(): number {
    return this._currentPage;
  }
  public set currentPage(value: number) {
    this._currentPage = value;
  }
  public get fromRow(): number {
    return this._fromRow;
  }
  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get user() {
    return this._user;
  }

  public set user(value: User) {
    this._user = value;
  }

  public get searchTerm(): string {
    return this._searchTerm;
  }
  public set searchTerm(value: string) {
    this._searchTerm = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }
  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get allProduct(): Product[] {
    return this._allProducts;
  }
  public set allProduct(value: Product[]) {
    this._allProducts = value;
  }

  public get allProductTemp(): Product[] {
    return this._allProductTemp;
  }
  public set allProductTemp(value: Product[]) {
    this._allProductTemp = value;
  }

  public get assignProducts(): Product[] {
    return this._assignProducts;
  }
  public set assignProducts(value: Product[]) {
    this._assignProducts = value;
  }

  public get assignProductTemp(): Product[] {
    return this._assignProductTemp;
  }
  public set assignProductTemp(value: Product[]) {
    this._assignProductTemp = value;
  }

  public get assignSelectedRows(): Product[] {
    return this._assignSelectedRows;
  }
  public set assignSelectedRows(value: Product[]) {
    this._assignSelectedRows = value;
  }

  public get selectedProductsIds() {
    return this._selectedProductsIds;
  }

  public set selectedProductsIds(value: number[]) {
    this._selectedProductsIds = value;
  }

  public set productsHaveDiscountOnSamePosGroup(value: any[]) {
    this._productsHaveDiscountOnSamePosGroup = value;
  }

  public get productsHaveDiscountOnSamePosGroup(): any[] {
    return this._productsHaveDiscountOnSamePosGroup;
  }
}

@Component({
  selector: 'confirm-dialog',
  templateUrl: 'confirm-dialog.html',
})
export class ConfrimationDialog {
  valueOfSelectedButton: any = -1;
  checkIfDialogIsClosed: any;
  @ViewChild("productDiscountComponent") productDiscountComponent: ProductDiscountComponent;

  constructor(
    public dialogRef: MatDialogRef<ConfrimationDialog>,
    protected matDialog: TdDialogService,
    protected discountProdcutsPosGroupsService: DiscountProductsPosGroupsService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    dialogRef.disableClose = true;
  }

  onDetails() {
    console.log("d")
    this.valueOfSelectedButton = 2;
    localStorage.setItem("valueOfSelectedButton", this.valueOfSelectedButton);
    localStorage.setItem("checkIfDialogIsClosed", this.checkIfDialogIsClosed);
    //this.checkIfDialogIsClosed = true;
    this.matDialog.open(ProductsHaveDiscountDialog, {
      width: '850px',
      data: {
        disableClose: true,
        productsHaveDiscount: this.data.productsHaveDiscountOnSamePosGroup
      }
    });

  }

  onOk() {
    console.log("o")
    this.valueOfSelectedButton = 1;
    localStorage.setItem("valueOfSelectedButton", this.valueOfSelectedButton
    );

    this.dialogRef.close();
  }

  onNoClick() {
    console.log("o")
    this.valueOfSelectedButton = -1;
    localStorage.setItem("valueOfSelectedButton", this.valueOfSelectedButton);
    this.checkIfDialogIsClosed = true;
    localStorage.setItem("checkIfDialogIsClosed", this.checkIfDialogIsClosed);
    this.dialogRef.close();
  }
}


@Component({
  selector: 'products-discount-dialog',
  templateUrl: 'products-discount-dialog.html',
  styleUrls: ['../../discount/product-discount/product-discount-component.css']
})
export class ProductsHaveDiscountDialog {

  selectedProductsColumnConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name', width: 350 },
    { name: 'discountName', label: 'Discount Name', width: 320 },
    { name: 'discountValue', label: 'Discount %', width: 130, format: DECIMAL_FORMAT }
  ];


  constructor(
    public dialogRef: MatDialogRef<ProductsHaveDiscountDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    dialogRef.disableClose = true;
  }


  onClose(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'product-discount-dialog',
  templateUrl: 'product-discount-dialog.html',
  styleUrls: ['../../discount/product-discount/product-discount-component.css']
})
export class SingleProductHasDiscountDialog {
  selectedProductColumnConfig: ITdDataTableColumn[] =
    [
      { name: 'name', label: 'Name', width: 350 },
      { name: 'discountName', label: 'Discount Name', width: 320 },
      { name: 'discountValue', label: 'Discount %', format: DECIMAL_FORMAT, width: 130 },
    ];

  productList: any[] = [];

  constructor(
    public dialogRef: MatDialogRef<SingleProductHasDiscountDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.productList.push(data.product);
    dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

  onClose(): void {
    this.dialogRef.close();
  }

}
