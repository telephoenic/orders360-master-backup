import {LoyaltyFormComponent} from './loyalty-form.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

describe('loyaltyFormComponent', () => {
  let component: LoyaltyFormComponent;
  let fixture: ComponentFixture<LoyaltyFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoyaltyFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoyaltyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
