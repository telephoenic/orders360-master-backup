import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgForm} from '@angular/forms';
import { MatSnackBar} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { Loyalty } from '../loyalty.model';
import { LoyaltyService } from '../service/loyalty.service';
import { ViewProductLoyaltyComponent } from '../view/view-products/view-product-loyalty.component';
import { AuthService } from '../../auth/service/auth.service';
import { User } from '../../auth/user.model';
import { VendorService } from '../../vendor/Services/vendor.service';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';

@Component({
  selector: 'app-loyalty-form',
  templateUrl: './loyalty-form.component.html',
  styleUrls: ['./loyalty-form.component.css']
})
export class LoyaltyFormComponent extends BaseFormComponent implements OnInit {

  protected loggedUser: User;
  private currencyName: string;
  private vendorId: number;
  @ViewChild("loyaltyForm") loyaltyForm : NgForm;  
  _loyalty : Loyalty;
  @ViewChild(ViewProductLoyaltyComponent) viewProductLoyaltyComponent : ViewProductLoyaltyComponent;

  @Input()
  _isEdit:boolean = false;

  constructor(protected loyaltyService: LoyaltyService,
              protected vendorAdminService: VendorAdminService,
              protected vendorService: VendorService,
              protected route:ActivatedRoute,
              protected authService: AuthService,
              protected router:Router,
              protected snackBar:MatSnackBar) { 

    super(loyaltyService, 
      route,
      router,
      snackBar);
  }

  ngOnInit() {
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    if(this.route.snapshot.queryParams["edit"] == '1') {
      this._isEdit = true;
      this._loyalty = this.loyaltyService.loyalty;
    } 
    else {
      this._loyalty = new Loyalty();
    }

    this.loadCurrency();
  }
  
  loadCurrency(){
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(
    vendorId => this.vendorService.getCurrencyNameByVendorId(vendorId).subscribe(
          data =>{
            this.currencyName = data;
          }
      ));
  }
  onSave(){
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId => {
      this._loyalty.vendor.id = vendorId;
      this.loyaltyService.loyalty = this._loyalty;
      this.viewProductLoyaltyComponent.onSave();
      super.onSave();
    });
 
  }

  onSaveAndNew(){
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId => {
      this._loyalty.vendor.id = vendorId;
      this.loyaltyService.loyalty = this._loyalty;
      this.viewProductLoyaltyComponent.onSave();
      super.onSaveAndNew();
    });
    
  }

  onSaveAndNewCompleted(){
    super.onSaveAndNewCompleted();
    this.viewProductLoyaltyComponent._allProductLoyalty = [];
  }

  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
  
  get model() : Loyalty {
    return this._loyalty;
  }

  set model(loyalty:Loyalty){
    this._loyalty = loyalty;
  }
  
  initModel(): Loyalty {
    return new Loyalty();
  }

  ngFormInstance(): NgForm {
    return this.loyaltyForm;
  }


}
