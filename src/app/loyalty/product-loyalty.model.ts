import { Product } from "../product/product";
import { Loyalty } from "./loyalty.model";

export class ProductLoyalty {

    id: number;
    inactive:boolean;
    product:Product;
    productLoyaltyPoints:number;

    constructor (){
    }
    
    get Product() {
        return this.product;
    }
    
    set Product(product:Product) {
        this.product = product;
    }

    get number() {
        return this.productLoyaltyPoints;
    }
    
    set number(productLoyaltyPoints:number) {
        this.productLoyaltyPoints = productLoyaltyPoints;
    }

}