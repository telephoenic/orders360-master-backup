import {
	TdDataTableSortingOrder,
	ITdDataTableColumn,
	IPageChangeEvent
  } from "@covalent/core";
  import {
	TdPagingBarComponent,
	TdDialogService,
	TdDataTableComponent
  } from "@covalent/core";
  import { Component, ViewChild, Input, OnInit } from "@angular/core";
  import { MatSnackBar } from "@angular/material";
  
  import { ActivatedRoute, Router } from "@angular/router";
  import { MatDialog } from "@angular/material";
  import { BaseViewComponent } from "../../../common/view/base-view-component";
  import { Product } from "../../../product/product";
  import { ProductService } from "../../../product/service/product.service";
  import { User } from "../../../auth/user.model";
  import { AuthService } from "../../../auth/service/auth.service";
  import { VendorAdminService } from "../../../vendor-admin/service/vendor-admin.service";
  import { ProductLoyaltyService } from "../../service/product-loyalty.service";
  import { LoyaltyService } from "../../service/loyalty.service";
  import { ProductLoyalty } from "../../product-loyalty.model";
  
  @Component({
	selector: "view-product-loyalty",
	templateUrl: "./view-product-loyalty.component.html"
  })
  export class ViewProductLoyaltyComponent extends BaseViewComponent
	implements OnInit {
	@Input() isAssign: boolean;
	@Input() loyaltyFactor: number;
	@Input() loyaltyFactorField: boolean;
  
	private dataTitle: string;
  
	private discountAmount: number;
	private quantity: number;
	private price: number;
	private productName: String;
	private _allProducts: Product[] = [];
	private _allDeassignedProducts: ProductLoyalty[] = [];
  
	private user: User;
  
	private _currentPage: number = 1;
	private _fromRow: number = 1;
	public _allProductLoyalty: ProductLoyalty[] = [];
	private _errors: any = "";
	private _selectedRows: Product[] = [];
	private _selectedLoyaltyRows: ProductLoyalty[] = [];
	private _pageSize: number;
	private _filteredTotal: number;
	private _sortBy: string = "";
	private _sortOrder: TdDataTableSortingOrder =
	  TdDataTableSortingOrder.Ascending;
	private _searchTerm: string = "";
	private deletedRows: ProductLoyalty[] = [];
  
	@ViewChild("pagingBar") pagingBar: TdPagingBarComponent;
	@ViewChild("pagingLoyaltyBar") pagingLoyaltyBar: TdPagingBarComponent;
	@ViewChild("dataTable") dataTable: TdDataTableComponent;
	@ViewChild("dataLoyaltyTable") dataLoyaltyTable: TdDataTableComponent;
  
	@ViewChild("dataAssignedTable") dataAssignedTable: TdDataTableComponent;
	@ViewChild("dataDeassignedTable") dataDeassignedTable: TdDataTableComponent;
  
	columnsConfig: ITdDataTableColumn[] = [
	  { name: "name", label: "Product Name" },
	  { name: "price", label: "Price", width: 110 }
	];
  
	columnsDeassignedConfig: ITdDataTableColumn[] = [
	  { name: "product.name", label: "Product Name" },
	  { name: "product.price", label: "Price" },
	  { name: "id", label: "", width: 100, sortable: false }
	];
  
	columnsLoyaltyConfig: ITdDataTableColumn[] = [
	  { name: "product.name", label: "Product Name", width: 215 },
	  { name: "product.price", label: "Price" },
	  { name: "productLoyaltyPoints", label: "Ponits" }
	];
  
	constructor(
	  protected loyaltyService: LoyaltyService,
	  protected productService: ProductService,
	  protected vendorAdminService: VendorAdminService,
	  protected productLoyaltyService: ProductLoyaltyService,
	  protected authService: AuthService,
	  protected route: ActivatedRoute,
	  protected router: Router,
	  protected dialogService: TdDialogService,
	  protected snackBar: MatSnackBar,
	  public dialog: MatDialog
	) {
	  super(productService, router, dialogService, snackBar);
	}
  
	refreshData() {
	  if (this.route.snapshot.queryParams["edit"] == "1") {
		this.loadAllProductLoyalty();
	  } else {
		this._allProductLoyalty = [];
	  }
	}
  
	ngOnInit() {
	  this.pageSize = this.pagingBar.pageSize;
	  this.user = this.authService.getCurrentLoggedInUser();
  
	  this.refreshData();
	  super.ngOnInit();
	}
  
	filterByVendor(
	  pageNumber: number,
	  pageSize: number,
	  searchTerm: string,
	  sortBy: string,
	  sortOrder: string,
	  vendorId: number
	) {
	  this.productService
		.getProductsByLoyalty(
		  pageNumber,
		  pageSize,
		  vendorId,
		  searchTerm,
		  sortBy,
		  sortOrder
		)
		.subscribe(data => {
		  this._allProducts = data["content"];
		  this.filteredTotal = data["totalElements"];
		});
	}
  
	loadAllProductLoyalty() {
	  this.productLoyaltyService
		.getAllProducts(this.loyaltyService.loyalty.id)
		.subscribe(data => {
		  this._allProductLoyalty = data;
		  for (
			let counter = 0;
			counter < this._allProductLoyalty.length;
			counter++
		  ) {
			this._allProductLoyalty[counter].productLoyaltyPoints = Math.round(
			  this._allProductLoyalty[counter].product.price *
				this.loyaltyService.loyalty.loyaltyFactor
			);
		  }
		});
	}
  
	getLeafProductInstance() {
	  let product = new Product();
	  return product;
	}
  
	onSave() {
	  this.validateSelectedLoyalty();
	  if (
		this._allDeassignedProducts !== null &&
		this._allDeassignedProducts.length !== 0
	  )
		this.productLoyaltyService
		  .delete(this._allDeassignedProducts)
		  .subscribe();
  
	  if (
		this._allProductLoyalty !== null &&
		this._allProductLoyalty.length !== 0
	  ) {
		this.loyaltyService.loyalty.listOfProductLoyalty = this._allProductLoyalty;
		this.dataLoyaltyTable.refresh();
	  }
	}
  
	validateSelectedLoyalty() {
	  if (
		this._allProductLoyalty == null ||
		this._allProductLoyalty.length == 0
	  ) {
		this.snackBar.open(
		  "At least one product required for the loyalty.",
		  "Close",
		  { duration: 3000 }
		);
		throw new Error("At least one product required for the loyalty.");
	  }
	}
  
	onAssignedProducts() {
	  this.selectedRows.forEach(product => {
		this._allProductLoyalty.forEach(productLoyalty => {
		  if (productLoyalty.product.id == product.id) {
			this.snackBar.open("Product is already aded.", "close", {
			  duration: 3000
			});
			throw new Error("Product is already aded.");
		  }
		});
	  });
	  for (let counter = 0; counter < this._selectedRows.length; counter++)
		this._allProductLoyalty = this._allProductLoyalty.concat(
		  this.getLeafProductLoyaltyInstance(this._selectedRows[counter])
		);
	  let unSelected = this._allProducts.filter(
		item => this.selectedRows.indexOf(item) < 0
	  );
  
	  this._allProducts = unSelected;
	  this._selectedRows = [];
	}
  
	onDeassignedProducts() {
	  this._allDeassignedProducts = this._allDeassignedProducts.concat(
		this._selectedLoyaltyRows
	  );
	  let unSelected = this._allProductLoyalty.filter(
		item => this._selectedLoyaltyRows.indexOf(item) < 0
	  );
	  this._allProductLoyalty = unSelected;
	  this._selectedLoyaltyRows = [];
	}
  
	onRemoveDeassigned(row: ProductLoyalty) {
	  this._allDeassignedProducts = this._allDeassignedProducts.filter(
		obj => obj !== row
	  );
	  this._allProductLoyalty = this._allProductLoyalty.concat(row);
	  this.dataDeassignedTable.refresh();
	  this.dataLoyaltyTable.refresh();
	}
  
	onRemoveAssigned(row: ProductLoyalty) {
	  this._allProductLoyalty = this._allProductLoyalty.filter(
		obj => obj !== row
	  );
	  this._allProducts = this._allProducts.concat(row.product);
	  this.dataAssignedTable.refresh();
	  this.dataTable.refresh();
	}
  
	getLeafProductLoyaltyInstance(product: Product) {
	  let productLoyalty = new ProductLoyalty();
	  productLoyalty.product = product;
	  if (this.route.snapshot.queryParams["edit"] == "1") {
		productLoyalty.productLoyaltyPoints = Math.round(
		  product.price * this.loyaltyService.loyalty.loyaltyFactor
		);
	  } else {
		productLoyalty.productLoyaltyPoints = Math.round(
		  product.price * this.loyaltyFactor
		);
	  }
  
	  return productLoyalty;
	}
  
	getPagingBar(): TdPagingBarComponent {
	  return this.pagingBar;
	}
	getDataTable(): TdDataTableComponent {
	  return this.dataTable;
	}
  
	public get currentPage(): number {
	  return this._currentPage;
	}
  
	public set currentPage(value: number) {
	  this._currentPage = value;
	}
  
	public get fromRow(): number {
	  return this._fromRow;
	}
  
	public set fromRow(value: number) {
	  this._fromRow = value;
	}
  
	public get allModels(): Product[] {
	  return this._allProducts;
	}
  
	public set allModels(value: Product[]) {
	  this._allProducts = value;
	}
  
	public get errors(): any {
	  return this._errors;
	}
  
	public set errors(value: any) {
	  this._errors = value;
	}
  
	public get selectedRows(): Product[] {
	  return this._selectedRows;
	}
  
	public set selectedRows(value: Product[]) {
	  this._selectedRows = value;
	}
	public get selectedLoyaltyRows(): ProductLoyalty[] {
	  return this._selectedLoyaltyRows;
	}
  
	public set selectedLoyaltyRows(value: ProductLoyalty[]) {
	  this._selectedLoyaltyRows = value;
	}
	public get pageSize(): number {
	  return this._pageSize;
	}
  
	public set pageSize(value: number) {
	  this._pageSize = value;
	}
  
	public get filteredTotal(): number {
	  return this._filteredTotal;
	}
  
	public set filteredTotal(value: number) {
	  this._filteredTotal = value;
	}
  
	public get sortBy(): string {
	  return this._sortBy;
	}
  
	public set sortBy(value: string) {
	  this._sortBy = value;
	}
  
	public get sortOrder(): TdDataTableSortingOrder {
	  return this._sortOrder;
	}
  
	public set sortOrder(value: TdDataTableSortingOrder) {
	  this._sortOrder = value;
	}
  
	public get searchTerm(): string {
	  return this._searchTerm;
	}
  
	public set searchTerm(value: string) {
	  this._searchTerm = value;
	}
  }
  