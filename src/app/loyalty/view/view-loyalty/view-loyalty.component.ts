import { TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import {
		 TdPagingBarComponent, TdDialogService,
		TdDataTableComponent
} from '@covalent/core';
import { Component, ViewChild, OnInit } from '@angular/core';
import {
	MatSnackBar
} from "@angular/material";
import { Router } from '@angular/router';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { LoyaltyService } from '../../service/loyalty.service';
import { Loyalty } from '../../loyalty.model';
import { NgxPermissionsService } from 'ngx-permissions';
import { AuthService } from '../../../auth/service/auth.service';
import { User } from '../../../auth/user.model';
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';

@Component({
	selector: 'app-view',
	templateUrl: './view-loyalty.component.html',
	styleUrls: ['./view-loyalty.component.css']
})
export class ViewLoyaltyComponent extends BaseViewComponent implements OnInit {

	private _currentPage: number = 1;
	private _fromRow: number = 1;
	private user: User;
	private _allLoyalties: Loyalty[] = [];
	private _errors: any = '';
	private _selectedRows: Loyalty[] = [];
	private _pageSize: number;
	private _filteredTotal: number;
	private _sortBy: string = '';
	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
	private _searchTerm: string = '';

	@ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
	@ViewChild('dataTable') dataTable: TdDataTableComponent;



	columnsConfig: ITdDataTableColumn[] = [
		{ name: 'name', label: 'Name',tooltip:"Name" },
		{ name: 'loyaltyFactor', label: 'Earned Points Conv. Rate/ 1' ,tooltip:"Factor", width:250 },
		{ name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive", width:100 },
		{ name: 'id', label: 'Edit', tooltip:"Edit", width:100 }
	];

	constructor(protected loyaltyService: LoyaltyService,
		protected vendorAdminService: VendorAdminService,
		protected router: Router,
		protected authService: AuthService,
		protected dialogService: TdDialogService,
		protected snackBar: MatSnackBar,
		protected permissionsService: NgxPermissionsService) {
		super(loyaltyService, router, dialogService, snackBar);
	}

	ngOnInit() {
		super.ngOnInit();

		this.permissionsService.hasPermission("ROLE_LOYALTY_EDIT").then((result: boolean) => {
			if (!result) {
				for (let i = 0; i < this.dataTable.columns.length; i++) {
					if (this.dataTable.columns[i].name == 'id') {
						this.dataTable.columns.splice(i, 1);
					}
				}
			}
		});

		this.permissionsService.hasPermission("ROLE_LOYALTY_ACTIVATE_DEACTIVATE_PER_ROW").then((result: boolean) => {
			if (!result) {
				for (let i = 0; i < this.dataTable.columns.length; i++) {
					if (this.dataTable.columns[i].name == 'inactive') {
						this.dataTable.columns.splice(i, 1);
					}
				}
			}
		});
	}



	filterByVendor(pageNumber: number, pageSize: number,
		searchTerm: string, sortBy: string, sortOrder: string, vendorId: number) {
		this.loyaltyService.getAllLoyaltiesByVendorId(pageNumber, pageSize, vendorId, searchTerm, sortBy, sortOrder).subscribe(
			data => {
				this.allModels = data["content"];
				this.filteredTotal = data["totalElements"]
			}
		);
	}

	/*
		onAfterDelete() {
			this.filterLoyaltyByUser();
		}
	
		*/

						
					
				
			
		

	// onEdit(loyaltyId:number, row:Loyalty){
	// 	this.loyaltyService.loyalty = row;
	// }
	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}
	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

	public get currentPage(): number {
		return this._currentPage;
	}

	public set currentPage(value: number) {
		this._currentPage = value;
	}

	public get fromRow(): number {
		return this._fromRow;
	}

	public set fromRow(value: number) {
		this._fromRow = value;
	}

	public get allModels(): Loyalty[] {
		return this._allLoyalties;
	}

	public set allModels(value: Loyalty[]) {
		this._allLoyalties = value;
	}

	public get errors(): any {
		return this._errors;
	}

	public set errors(value: any) {
		this._errors = value;
	}

	public get selectedRows(): Loyalty[] {
		return this._selectedRows;
	}

	public set selectedRows(value: Loyalty[]) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string {
		return this._sortBy;
	}

	public set sortBy(value: string) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder) {
		this._sortOrder = value;
	}

	public get searchTerm(): string {
		return this._searchTerm;
	}

	public set searchTerm(value: string) {
		this._searchTerm = value;
	}
}
