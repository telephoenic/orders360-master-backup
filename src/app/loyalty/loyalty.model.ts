import { ProductLoyalty } from "./product-loyalty.model";
import { Vendor } from "../vendor/vendor.model";

export class Loyalty {
    
    id: number;
    loyaltyPoints:number;
    vendor:Vendor;
    loyaltyFactor:number;
    name:string;
    inactive:boolean;
    listOfProductLoyalty:ProductLoyalty[] = [];

    constructor (){ 
        this.vendor = new Vendor();
    }
    

}