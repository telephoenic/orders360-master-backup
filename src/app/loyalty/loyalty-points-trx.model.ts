import { Order } from "../orders/order.model";
import { Product } from "../product/product";
import { PosUser } from "../pos-user/pos-user.model";
import { LoyaltyPointsTrxSource } from "./loyalty-points-trx-source.model";

export class LoyaltyPointsTrx {

    id:number;
    inactive:boolean;
    points:number;
    order:Order;
    product:Product;
    posUser:PosUser;
    trxSource:LoyaltyPointsTrxSource ;
    
    constructor(points:number,
                order:Order,
                posUser:PosUser,
                product:Product,
                trxSource:LoyaltyPointsTrxSource){
        this.points = points;
        this.order = order;
        this.posUser = posUser;
        this.product = product;
        this.trxSource = trxSource;

    }       


}