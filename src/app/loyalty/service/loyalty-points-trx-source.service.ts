import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';

import { BaseService } from '../../common/service/base.service';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { LoyaltyPointsTrxSource } from '../loyalty-points-trx-source.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoyaltyPointsTrxSourceService extends BaseService{
  loyaltyPointsTrxSource:LoyaltyPointsTrxSource;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
              protected authService:AuthService) {
    super(http, "loyalty/trx/source", authService);
  }
  

  getSourceByCode(code: string) : Observable<any>{
    return this.http.get(environment.baseURL + "loyalty/trx/source/find"+"?code="+code);
  }


  get model() {
    return this.loyaltyPointsTrxSource;
  }

  set model(model:BaseModel) {
    this.loyaltyPointsTrxSource = <LoyaltyPointsTrxSource>model;
  }
}