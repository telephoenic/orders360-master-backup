import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';

import { BaseService } from '../../common/service/base.service';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { LoyaltyPointsTrx } from '../loyalty-points-trx.model';

@Injectable()
export class LoyaltyPointsTrxService extends BaseService{
  loyaltyPointsTrx:LoyaltyPointsTrx;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
              protected authService:AuthService) {
    super(http, "loyalty/trx", authService);
  }
  
  get model() {
    return this.loyaltyPointsTrx;
  }

  set model(model:BaseModel) {
    this.loyaltyPointsTrx = <LoyaltyPointsTrx>model;
  }
}