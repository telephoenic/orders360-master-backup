import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';

import { BaseService } from '../../common/service/base.service';
import { environment } from '../../../environments/environment';
import { Loyalty } from '../loyalty.model';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Product } from '../../product/product';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductLoyaltyService extends BaseService{
  loyalty:Loyalty;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
              protected authService:AuthService) {
    super(http, "product_loyalty", authService);
  }

  getAllProducts(loyaltyId:number) : Observable<any>{
    return this.http.get(environment.baseURL + "product_loyalty/items"+"/"+loyaltyId);
  }

  // getPointsWhereProductId(productId: number) : Observable<any>{
  //   return this.http.get(environment.baseURL + "product_loyalty/points"+"/"+productId);
  // }

  get model(){
    return this.loyalty;
  }
  set model(model:BaseModel) {
    this.loyalty = <Loyalty>model;
  }
}