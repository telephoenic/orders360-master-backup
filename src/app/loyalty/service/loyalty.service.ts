import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';

import { BaseService } from '../../common/service/base.service';
import { environment } from '../../../environments/environment';
import { Loyalty } from '../loyalty.model';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { Observable } from '../../../../node_modules/rxjs/Observable';
import { Currency } from '../../lookups/currency.model';

@Injectable()
export class LoyaltyService extends BaseService{
  loyalty:Loyalty;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
              protected authService:AuthService) {
    super(http, "loyalty", authService);
  }


  getAllSurveyByVendorId(pageNum:number, 
    pageSize:number, 
    vendorId:number , 
    searchTerm:string, 
    sortBy:string, 
    sortOrder:string ) : Observable<any>{
return this.http.get(environment.baseURL + "survey/survey_for_vendor/"+ pageNum+"/"+pageSize+"/"+vendorId, 
   { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder), 
     headers: this.getBasicHeaders()});
}


  
  getAllLoyaltiesByVendorId(pageNum:number, 
                            pageSize:number, 
                            vendorId:number,
                            searchTerm:string, 
                            sortBy:string, 
                            sortOrder:string) : Observable<any>{
    return this.http.get(environment.baseURL + "loyalty/loyalty_for_vendor/"+ pageNum+"/"+pageSize+"/"+vendorId, 
    { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder), 
      headers: this.getBasicHeaders()});
  }

  get model() {
    return this.loyalty;
  }

  set model(model:BaseModel) {
    this.loyalty = <Loyalty>model;
  }

}