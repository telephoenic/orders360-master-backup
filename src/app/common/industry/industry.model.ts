import { BaseModel } from '../../common/model/base-model';

export class Industry extends BaseModel {

    id: number;
    inactive:boolean;
    name:string;
    constructor() {
        super();
    }
}