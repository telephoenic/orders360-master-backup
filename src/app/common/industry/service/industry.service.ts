import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../../common/service/base.service';
import { AuthService } from '../../../auth/service/auth.service';
import { BaseModel } from '../../../common/model/base-model';
import { Injectable } from '@angular/core';
import { Industry } from '../industry.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class IndustryService extends BaseService {
    industry: Industry;
    navigateToAfterCompletion: string;
    constructor(http: HttpClient, protected authService: AuthService) {
        super(http, "industry", authService)
    }
    findIndustries<T extends BaseModel>(): Observable<T> {
        return this.http.get<T>(environment.baseURL+"country/all");
      }
    
    get model() {
        return this.industry;
    }

    set model(model: BaseModel) {
        this.industry = <Industry>model;
    }


}