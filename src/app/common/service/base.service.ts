import { Injectable, ViewChild } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, ResponseContentType } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { DataSource } from '@angular/cdk/collections';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { environment } from "../../../environments/environment";
import { BaseModel } from '../model/base-model';
import { forEach } from '@angular/router/src/utils/collection';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { User } from '../../auth/user.model';
import { NumberSymbol } from '@angular/common';

export abstract class BaseService {
  static readonly SERVER_URL = environment.baseURL;
  static readonly HEADER_CONTENT_TYPE_JSON = { "Content-Type": "application/json" };

  static readonly URL_DELETE = "/delete";
  static readonly URL_SAVE = "/save";
  static readonly URL_SAVE_ALL = "/save-all";
  static readonly URL_SAVE_WITH_ATTACHMENTS = "/save-with-attachments";
  static readonly URL_UPDATE = "/update";
  static readonly URL_UPDATE_WITH_ATTACHMENTS = "/update-with-attachments";
  static readonly URL_FILTER = "/filter";
  static readonly URL_STATUS = "/status";
  static readonly URL_FIND = "/find";
  static readonly URL_FIND_ALL = "/all";
  static readonly URL_FIND_BY_EXAMPLE = "/find-by-example";
  static readonly URL_GET_ATTACHMENT = "/attachments";

  static readonly HEADERS = new HttpHeaders(BaseService.HEADER_CONTENT_TYPE_JSON);

  navigateToAfterCompletion: string;

  constructor(protected http: HttpClient,
              protected serviceURL: string,
              protected authService:AuthService) { }

  getBasicHeaders() : HttpHeaders {
    let basicHeaders = new HttpHeaders({"Content-Type": "application/json"});

    return basicHeaders;
  }

  save<T extends BaseModel>(model: T) {
    return this.http.post(environment.baseURL + this.serviceURL + BaseService.URL_SAVE,
                          JSON.stringify(model), 
                          { headers: this.getBasicHeaders() });
  }
  
  saveAll<T extends BaseModel>(model: T[]) {
    return this.http.post(environment.baseURL + this.serviceURL + BaseService.URL_SAVE_ALL,
                          JSON.stringify(model), 
                          
                          { headers: this.getBasicHeaders() });
                          
  }

  saveWithAttachments<T extends BaseModel>(model: T, files:File[]) {
    let headers = new HttpHeaders();
    headers.append("Accept", "application/json")
    
    let formData = new FormData();
    
    for(let i:number = 0; i < files.length; i++) {
      formData.append("attachments", files[i] );
    }
    
    formData.append("model", JSON.stringify(model));
    
    return this.http.post(environment.baseURL + 
                            this.serviceURL + 
                            BaseService.URL_SAVE_WITH_ATTACHMENTS,
                          formData, 
                          { headers: headers });
  }

  update<T extends BaseModel>(model: T) {
    return this.http.post(environment.baseURL + this.serviceURL + BaseService.URL_UPDATE,
                          JSON.stringify(model), 
                          { headers:  this.getBasicHeaders() });
  }

  updateWithAttachments<T extends BaseModel>(model: T, files:File[]) {
    let headers = new HttpHeaders();
    headers.append("Accept", "application/json")

    let formData = new FormData();

    for(let i:number = 0; i < files.length; i++) {
      formData.append("attachments", files[i]);
    }
    
    formData.append("model", JSON.stringify(model));
    
    return this.http.post(environment.baseURL + 
                            this.serviceURL + 
                            BaseService.URL_UPDATE_WITH_ATTACHMENTS,
                          formData, 
                          { headers: headers });
  }

  filter<T extends BaseModel>(pageNumber: number,
                              pageSize: number,
                              searchTerm: string,
                              sortBy: string,
                              sortOrder: string,
                              filterationURL?:string, 
                              additionalParams?:{paramName:string, paramValue:string}[]): Observable<T> {

    return this.http.get<T>( environment.baseURL + 
                          this.serviceURL + 
                          (filterationURL == undefined? this.getFilterURL(): filterationURL)+ 
                          "/" + 
                          pageNumber + 
                          "/" + 
                          pageSize,
                          
                        { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder, additionalParams), 
                          headers: this.getBasicHeaders()});
  }

  filterNotPageable<T>( searchTerm: string,
                                          sortBy: string,
                                          sortOrder: string,
                                          filterationURL?:string, 
                                          additionalParams?:{ paramName:string, 
                                                              paramValue:string}[]): Observable<T> {

    return this.http.get<T>(environment.baseURL + 
                            this.serviceURL + 
                            (filterationURL == undefined? this.getFilterURL(): filterationURL),
                            { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder, additionalParams), 
                            headers: this.getBasicHeaders()});
  }



  
  filterPageable<T>( 
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    filterationURL?:string,
    additionalParams?:{ paramName:string, paramValue:string}[] ): Observable<T> {

      return this.http.get<T>(environment.baseURL + 
        this.serviceURL + 
        (filterationURL == undefined? this.getFilterURL(): filterationURL),
        { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder, additionalParams), 
        headers: this.getBasicHeaders()});
}





  getFilterURL() {
    return BaseService.URL_FILTER
  }

  prepareSearchParameters(searchTerm: string, sortBy: string, sortOrder: string, additionalParams?:{paramName:string, paramValue:string}[]): HttpParams {
    let searchParams: HttpParams = new HttpParams();

    if (searchTerm != null && searchTerm !== '') {
      searchParams = searchParams.set("search_term", searchTerm);
    }
    if (sortBy != null && sortBy !== '') {
      searchParams = searchParams.set("sort_by", sortBy);

      if (sortOrder != null && sortOrder !== '') {
        searchParams = searchParams.set("sort_order", sortOrder);
      }
    }
    
    if(additionalParams) {
      for(let i=0;i<additionalParams.length;i++) {
        if(additionalParams[i].paramName != null && additionalParams[i].paramName!==''){
          searchParams = searchParams.append(   additionalParams[i].paramName,
                                               additionalParams[i].paramValue);
        }
      }
    }

    return searchParams;
  }

  sort<T extends BaseModel>(sortBy: string, sortOrder: string, pageNumber: number, pageSize: number): Observable<T> {
    let searchParams: HttpParams = new HttpParams();
    searchParams.set("sort_by", sortBy);
    searchParams.set("sort_order", sortOrder);
    return this.http.get<T>(environment.baseURL + this.serviceURL + BaseService.URL_FILTER + "/" + pageNumber + "/" + pageSize,
                        { params: searchParams, headers: this.getBasicHeaders()});
  }

  page<T extends BaseModel>(pageNumber: number, pageSize: number): Observable<T> {
    return this.http.get<T>(environment.baseURL + this.serviceURL + "/" + pageNumber + "/" + pageSize, 
                          {headers: this.getBasicHeaders()});
  }

  delete<T extends BaseModel>(systemAdmins: T[]) {
    return this.http.post(environment.baseURL + this.serviceURL + BaseService.URL_DELETE, 
                          JSON.stringify(systemAdmins), 
                          { headers:  this.getBasicHeaders() } );
  }

  changeStatusTo<T extends BaseModel>(status, systemAdminId: number[]) {
    return this.http.post(environment.baseURL + this.serviceURL + BaseService.URL_STATUS+"/"+status, 
                          JSON.stringify(systemAdminId), 
                          { headers:  this.getBasicHeaders() });
  }

  find<T extends BaseModel>(id:number): Observable<T> {
    return this.http.get<T>(environment.baseURL + this.serviceURL + BaseService.URL_FIND + "/" + id, 
                        {headers: this.getBasicHeaders()});
  }


  
  findAll<T extends BaseModel>(): Observable<T> {
    return this.http.get<T>(environment.baseURL + this.serviceURL + BaseService.URL_FIND_ALL, 
                        {headers: this.getBasicHeaders()});
  }

  findByExample<T extends BaseModel>(baseModel:BaseModel): Observable<any> {
    return this.http.post(environment.baseURL + this.serviceURL + BaseService.URL_FIND_BY_EXAMPLE,
                          JSON.stringify(baseModel),
                          {headers:  this.getBasicHeaders()});
  }

  findByExamplePageable<T extends BaseModel>( baseModel:BaseModel, 
                                              pageNumber: number,
                                              pageSize: number,): Observable<any> {

    return this.http.post(environment.baseURL + this.serviceURL + BaseService.URL_FIND_BY_EXAMPLE + "/" + pageNumber + "/" + pageSize,
                          JSON.stringify(baseModel),
                          {headers:  this.getBasicHeaders()});
  }

  

  

  getAttachmentUrl(attachmentName:string):string {
    return BaseService.SERVER_URL + 
            this.serviceURL + 
            BaseService.URL_GET_ATTACHMENT + 
            "/" + 
            this.model.id + 
            "/" + 
            attachmentName;
  }

  getAttachment(url:string) : Observable<Blob>{
    return this.http.get(url, {responseType:"blob", 
                                headers:  this.getBasicHeaders()});
  }

  getCurrentLoggedInUser():User{
    return this.authService.getCurrentLoggedInUser();
  }

  abstract get model();
  abstract set model(model:BaseModel);
}
