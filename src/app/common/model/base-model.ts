export abstract  class BaseModel {
    abstract get id();
    abstract get inactive():boolean;
    abstract set inactive(inactive:boolean);
}