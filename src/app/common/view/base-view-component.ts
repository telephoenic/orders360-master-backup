import { VendorAdminService } from './../../vendor-admin/service/vendor-admin.service';
import { AuthService } from './../../auth/service/auth.service';
import { User } from './../../auth/user.model';
import {BaseModel} from '../model/base-model';
import { BaseService } from "../service/base.service";
import { Router } from "@angular/router";
import {ITdDataTableSortChangeEvent, 
    TdDataTableSortingOrder,  
    TdPagingBarComponent,   
    TdDataTableComponent,    
    IPageChangeEvent,     
    TdDialogService} from '@covalent/core';
import { MatSnackBar } from "@angular/material";
import { OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
export abstract class BaseViewComponent implements OnInit {

   // protected authService: AuthService;
    protected loggedUser: User;


    constructor(protected baseService: BaseService,
                protected router:Router,
                protected dialogService : TdDialogService,
                protected snackBar:MatSnackBar,
                protected authService?: AuthService,
                protected vendorAdminService?: VendorAdminService) {}


    ngOnInit() {
        // this.pageSize = this.getPagingBar().pageSize;
        // this.baseService.page(0, this.pageSize).subscribe(
        //   data => {
        //     this.allModels = data["content"];
        //     this.filteredTotal = data["totalElements"]
        //   }
        // );

        this.pageSize = this.getPagingBar().pageSize;
        this.filter(0, this.pageSize);
    }
    search(searchTerm: string): void {
        this.searchTerm = searchTerm;

        if(this.isSearchTermCleared(searchTerm)){ 
            this.getPagingBar().navigateToPage(1);
        } else {
            this.selectedRows = [];
            this.getPagingBar().navigateToPage(1);
            this.filter(0, this.pageSize);
        }
    }

    page(pagingEvent: IPageChangeEvent): void {
        this.fromRow = pagingEvent.fromRow;
        this.currentPage = pagingEvent.page;
        this.pageSize = pagingEvent.pageSize;

        this.filter(pagingEvent.page - 1, this.pageSize);
    }


    sort(sortEvent: ITdDataTableSortChangeEvent): void {
        this.sortBy = sortEvent.name;
        this.sortOrder = sortEvent.order;
        
        this.filter(0, this.pageSize);
    }
    
    filter(pageNumber:number, pageSize:number) {
        this.loggedUser = this.authService.getCurrentLoggedInUser(); 

        if(this.loggedUser.type.id == environment.ENUM_ID_USER_TYPE.TelephoenicAdmin){
            this.baseService.filter(pageNumber,
                pageSize, 
                this.searchTerm, 
                this.sortBy, 
                this.sortOrder.toString())
                        .subscribe(data => {
                                this.allModels = data['content'];
                                this.filteredTotal = data['totalElements'];
                        });
        }else if(this.loggedUser.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin){
            this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId=>{
                this.filterByVendor(pageNumber, pageSize, this.searchTerm, this.sortBy, this.sortOrder,vendorId);
            });
            

        }
    }

    filterByVendor(	pageNumber:number, 
        pageSize:number, 
        searchTerm:string,
        sortBy:string,
        sortOrder:string,
        vendorId:number) { 
          
        }


  
    isSearchTermCleared(searchTerm) : boolean {
        return searchTerm == null || searchTerm == '';
    }

    compareWith(row: any, model: any): boolean { 
        return row.id === model.id;
    }

    onNew() {
        this.baseService.navigateToAfterCompletion = this.router.url;
    }

    onDelete() {
        if(this.selectedRows.length > 0) {
            this.dialogService.openConfirm({
                message: "Are you sure you want to delete the selected row(s)?",
                title: "confirmation",
                cancelButton: "Disagree",
                acceptButton: "Agree"})
            .afterClosed().subscribe((accept:boolean) => {
                if(accept) {
                    this.baseService.delete(this.selectedRows).subscribe(
                    data => {
                        for(let i:number = 0; i < this.selectedRows.length; i++){
                            let index:number = this.allModels.indexOf(this.selectedRows[i]);
                            this.allModels.slice(index);
                        }

                        if(this.allModels.length == 0){
                            this.onRefresh();
                        }

                        this.onAfterDelete();
                        // this.filter(this.currentPage-1, this.pageSize);
                        
                        this.snackBar.open("Deleted successfully", "Close", {duration: 3000});
                        this.selectedRows = [];
                    },
                    (errorResponse:HttpErrorResponse) => {                   
                        this.onDeleteFailed(errorResponse);
                    }             
                )}
            });
        } else if(this.allModels.length == 0) {
            this.onNoDataFoundToDelete();
        } else {
            this.onNoSelectedRowsToDelete();
        }
    }

    onAfterDelete() {
        this.filter(this.currentPage-1, this.pageSize);
    }
    
    onNoDataFoundToDelete() {
        this.snackBar.open("No data found.", "close", {duration: 3000});
    }

    onNoSelectedRowsToDelete() {
        this.snackBar.open("No selected rows.", "close", {duration: 3000});
    }

    onDeleteFailed(errorResponse:HttpErrorResponse) {
        this.snackBar.open("Delete process failed. Please try again.", "close", {duration: 3000});
    }

    onEdit(id: number, row:BaseModel) {
        this.baseService.model = row;
        this.baseService.navigateToAfterCompletion = this.router.url;
    } 

    onActivateDeactivePerRow(value, row:BaseModel) {
        this.baseService.changeStatusTo(value, [row.id])
                        .subscribe( data => {
                                        row.inactive = !value;
                                        this.getDataTable().refresh();
                                    },
                                    (errorResponse:HttpErrorResponse) => {
                                        if(value) {
                                            this.onActivationFailed(errorResponse);
                                        } else {
                                            this.onDeactivationFailed(errorResponse);
                                        }
                                    }
                                );
    }

    onActivateDeactivateSelectedRows(activate:boolean) {
        if(this.selectedRows.length > 0) {
            this.dialogService.openConfirm({
                    message: "Are you sure you want to " + (activate?"activate": "deactivate") + " the selected row(s)?",
                    title: "confirmation",
                    cancelButton: "Disagree",
                    acceptButton: "Agree"})
                .afterClosed().subscribe((accept:boolean) => {
                    if(accept) {
                        let selectedSystemAdminsIDs : number[]=[];

                        this.selectedRows.forEach(systemAdmin => {
                            selectedSystemAdminsIDs.push(systemAdmin.id);
                        });

                        this.baseService.changeStatusTo(activate, selectedSystemAdminsIDs).subscribe(
                            data => {
                                this.filter(this.currentPage-1, this.pageSize);
                                this.snackBar.open((activate?"Activated": "Deactivated") + " successfully", "Close", {duration: 3000});
                                this.selectedRows = [];
                            },
                            (errorResponse:HttpErrorResponse) => {
                                if(activate) {
                                    this.onActivationFailed(errorResponse);
                                } else {
                                    this.onDeactivationFailed(errorResponse);
                                }
                            }
                        )
                    }
                });
        } else if(this.allModels.length == 0) {
            this.onNoDataFoundToActivateOrDeactivate(activate);
        } else {
            this.onNoSelectedRowsToActivateOrDeactivate(activate);
        }
    }

    onNoDataFoundToActivateOrDeactivate(activate:boolean) {
        this.snackBar.open("No data found.", "close", {duration: 3000});
    }

    onNoSelectedRowsToActivateOrDeactivate(activate:boolean) {
        this.snackBar.open("No selected rows.", "close", {duration: 3000});
    }

    onActivationFailed(errorResponse:HttpErrorResponse) {
        this.snackBar.open("Activation process failed. Please try again.", "close", {duration: 3000});
    }

    onDeactivationFailed(errorResponse:HttpErrorResponse) {
        this.snackBar.open("Deactivation process failed. Please try again.", "close", {duration: 3000});
    }

    onRefresh() {
        if(this.allModels.length == 0) {
            if(this.currentPage != 1) {
            this.filter(this.currentPage-2, this.pageSize);    
            } 
        } else {
            this.filter(this.currentPage-1, this.pageSize);    
        }
    }


    abstract get currentPage(): number;
    
    abstract set currentPage(value: number );

    abstract get fromRow(): number;

    abstract set fromRow(value: number );

    abstract get allModels(): BaseModel[];

    abstract set allModels(value: BaseModel[] );

    abstract get errors(): any;

    abstract set errors(value: any );

    abstract get selectedRows(): BaseModel[];

    abstract set selectedRows(value: BaseModel[] );

    abstract get pageSize(): number;

    abstract set pageSize(value: number);
    
    abstract get filteredTotal(): number;

    abstract set filteredTotal(value: number);

    abstract get sortBy(): string;

    abstract set sortBy(value: string );

    abstract get sortOrder(): TdDataTableSortingOrder;

    abstract set sortOrder(value: TdDataTableSortingOrder );

    abstract get searchTerm(): string;

    abstract set searchTerm(value: string );

    abstract getPagingBar() : TdPagingBarComponent ;

    abstract getDataTable() : TdDataTableComponent ;
}