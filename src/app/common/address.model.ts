import { Country } from "../location/country/country.model";
import { City } from "../location/city/City.model";

export class Address {
    
    constructor (public city: City,
        public zipCode: string,
        public line1: string,
        public line2: string
         ){ }

}