import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { BaseService } from "../service/base.service";
import { OnInit } from "@angular/core";
import { BaseModel } from "../model/base-model";
import { NgForm } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { MatProgressButtonOptions } from "mat-progress-buttons";

export abstract class BaseFormComponent {
    showSpinner:boolean=false;
    showSpinnerOnSaveAndNew:boolean=false;
    constructor(protected baseService: BaseService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected snackBar: MatSnackBar) { }

    onCancel() {
        this.onBeforeCancel();
        this.router.navigateByUrl(this.baseService.navigateToAfterCompletion);
        this.baseService.navigateToAfterCompletion = "";
        this.onAfterCancel();
    }

    onBeforeCancel() { }
    onAfterCancel() { }

    onSave() {
        if (this.isEdit) {
            this.onBeforeEdit(this.model);
            this.baseService.update(this.model).subscribe(
                data => this.router.navigateByUrl(this.baseService.navigateToAfterCompletion),
                (errorResponse: HttpErrorResponse) => {
                    this.showSpinner = false;
                    this.onEditError(errorResponse);
                },
                () => this.onEditCompleted()
            );

            this.onAfterEdit(this.model);
        } else {
            this.onBeforeSave(this.model);

            this.baseService.save(this.model).subscribe(
                data => this.router.navigateByUrl(this.baseService.navigateToAfterCompletion),
                (errorResponse: HttpErrorResponse) => {
                    this.showSpinner = false;
                    this.showSpinnerOnSaveAndNew = false;
                    this.onSaveError(errorResponse);
                },
                () => this.onSaveCompleted()
            );

            this.onAfterSave(this.model);
        }
    }

    onSaveWithAttachments(files: File[]) {
        if (this.isEdit) {
            this.onBeforeEditWithAttachments(this.model, files);
            this.baseService.updateWithAttachments(this.model, files).subscribe(
                data => {this.router.navigateByUrl(this.baseService.navigateToAfterCompletion)
                },
                (errorResponse: HttpErrorResponse) => {
                    this.showSpinner = false;
                    this.onEditWithAttachmentsError(errorResponse);
                },
                () => this.onEditWithAttachmentsCompleted()
            );

            this.onAfterEditWithAttachments(this.model, files);
        } else {
            this.onBeforeSaveWithAttachments(this.model, files);

            this.baseService.saveWithAttachments(this.model, files).subscribe(
                data => this.router.navigateByUrl(this.baseService.navigateToAfterCompletion),
                (errorResponse: HttpErrorResponse) => {
                    this.showSpinner = false;
                    this.onSaveWithAttachmentsError(errorResponse);
                },
                () => this.onSaveWithAttachmentsCompleted()
            );

            this.onAfterSaveWithAttachments(this.model, files);
        }

    }

    onBeforeEditWithAttachments(model: BaseModel, files: any) {
       this.showSpinner = true;
     }

    onBeforeSaveWithAttachments(model: BaseModel, files: any) {
        this.showSpinner = true;
     }

    onAfterEditWithAttachments(model: BaseModel, files: any) {    }

    onAfterSaveWithAttachments(model: BaseModel, files: any) {     }

    onEditWithAttachmentsError(errorResponse: HttpErrorResponse) {
        this.snackBar.open(errorResponse.error.message, "Close", { duration: 3000 });
    }

    onEditWithAttachmentsCompleted() { 
        this.showSpinner = false;
    }

    onSaveWithAttachmentsCompleted() {
        this.showSpinner = false;
    }

    onSaveWithAttachmentsError(errorResponse: HttpErrorResponse) {
        this.showSpinner = false;
        this.snackBar.open(errorResponse.error.message, "Close", { duration: 3000 });
    }

    onBeforeEdit(model) { 
        this.showSpinner = true;
    }

    onAfterEdit(model) {   }

    onBeforeSave(model) {
        this.showSpinner = true;
     }
    

    onAfterSave(model) {  }

    onEditCompleted() {
        this.showSpinner = false;
    }

    onSaveCompleted() { 
      this.showSpinner =false;
    }

    onEditError(errorResponse: HttpErrorResponse) {
        this.showSpinner = false;
        this.snackBar.open(errorResponse.error.message, "Close", { duration: 3000 });
    }

    onSaveError(errorResponse: HttpErrorResponse) {
        this.showSpinner = false;
        this.snackBar.open(errorResponse.error.message, "Close", { duration: 3000 });
    }

    onSaveAndNew() {
        this.onBeforeSaveAndNew(this.model);

        this.baseService.save(this.model).subscribe(
            data => {
                this.model = this.initModel();
                this.snackBar.open("Saved successfully", "Close", { duration: 3000 });
                this.ngFormInstance().resetForm();
            },
            (errorResponse: HttpErrorResponse) => {
                this.showSpinnerOnSaveAndNew = false;
                this.onSaveAndNewError(errorResponse);
            },
            () => this.onSaveAndNewCompleted()
        );
        this.onAfterSaveAndNew(this.model);
    }

    onBeforeSaveAndNew(model) { 
        this.showSpinnerOnSaveAndNew = true;
    }

    onAfterSaveAndNew(model) {     }

    onSaveAndNewError(errorResponse: HttpErrorResponse) {
        console.log(errorResponse);
        this.snackBar.open(errorResponse.error.message, "Close", { duration: 3000 });
    }

    onSaveAndNewCompleted() { 
        this.showSpinnerOnSaveAndNew = false;
    }

    onSaveAndNewWithAttachments(files: File[]) {
        this.onBeforeSaveAndNewWithAttachments(this.model, files);
        console.log(this.showSpinnerOnSaveAndNew);
        this.baseService.saveWithAttachments(this.model, files).subscribe(
            data => {
                this.model = this.initModel();
                this.snackBar.open("Saved successfully", "Close", { duration: 3000 });
                this.ngFormInstance().resetForm();
                this.showSpinnerOnSaveAndNew = false;
            },
            (errorResponse: HttpErrorResponse) => {
                this.showSpinnerOnSaveAndNew = false;
                this.onSaveAndNewWithAttachmentsError(errorResponse);
            },
            () => this.onSaveAndNewWithAttachmentsCompleted()
        );

        this.onAfterSaveAndNewWithAttachments(this.model, files);
    }

    onBeforeSaveAndNewWithAttachments(model: BaseModel, files: any) { 
        this.showSpinnerOnSaveAndNew = true;
    }

    onSaveAndNewWithAttachmentsError(errorResponse: HttpErrorResponse) {
        this.showSpinnerOnSaveAndNew = false;
        this.snackBar.open(errorResponse.error.message, "Close", { duration: 3000 });
    }

    onSaveAndNewWithAttachmentsCompleted() {     }

    onAfterSaveAndNewWithAttachments(model: BaseModel, files: any) {    }

    abstract get isEdit(): boolean;

    abstract set isEdit(isEdit: boolean);

    abstract get model(): BaseModel;

    abstract set model(baseModel: BaseModel);

    abstract initModel(): BaseModel;

    abstract ngFormInstance(): NgForm;
}