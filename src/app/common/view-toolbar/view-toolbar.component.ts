import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MatButton, MatIcon, MatTooltip } from "@angular/material";
import { TdDialogService } from '@covalent/core';

// @Component({
//   selector: 'view-toolbar',
//   templateUrl: './view-toolbar.component.html',
//   styleUrls: ['./view-toolbar.component.css']
// })
export class ViewToolbarComponent implements OnInit {

  @Input() isAddVisible: boolean = true;
  @Input() isDeleteVisible: boolean = true;
  @Input() isActivateVisible: boolean = true;
  @Input() isDeactivateVisible: boolean = true;
  @Input() isRefreshVisible: boolean = true;

  @Input() addClickRouteTo: string = "";
  @Input() addClickQueryParams: string = "";

  @Input() addTooltip: string = "Add";
  @Input() deleteTooltip: string = "Delete";
  @Input() activateTooltip: string = "Activate";
  @Input() deactivateTooltip: string = "Deactivate";
  @Input() refreshTooltip: string = "Refresh data";
  @Input() numberOfRecords: number = 0;

  @Output() addClick = new EventEmitter<any>();
  @Output() deleteClick = new EventEmitter<any>();
  @Output() activateDeactivateClick = new EventEmitter<any>();
  @Output() refreshClick = new EventEmitter<any>();

  constructor(private dialog: TdDialogService) { }

  ngOnInit() { }

  onAddClick(routeTo, params) {
    this.addClick.emit({routeTo, params});
  }

  onDeleteClick() {
    if (this.numberOfRecords > 0) {
      this.dialog.openConfirm({ message: "Are you sure you want to delete the selected row(s)?", })
        .afterClosed().subscribe((accepted: boolean) => {
          this.deleteClick.emit();
        });
    }
  }

  onActivateDeactivateClick(changeStatusTo: boolean) {
    if (this.numberOfRecords > 0) {
      this.dialog.openConfirm({ message: "Are you sure you want to " + (changeStatusTo ? " activate " : " deactivate ") + "the selected row(s)?", })
                  .afterClosed()
                  .subscribe((accepted: boolean) => {
                          this.activateDeactivateClick.emit(changeStatusTo);
                  });
    }
  }

  onRefreshClick() {
    this.refreshClick.emit();
  }
}