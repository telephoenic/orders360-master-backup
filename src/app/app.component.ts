import { Component, OnInit, Inject, HostListener } from "@angular/core";
import { AuthService } from "./auth/service/auth.service";
import { Router, ActivatedRoute } from "@angular/router";
import {
  MatSnackBar,
  MatMenu,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material";
import { HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { User } from "./auth/user.model";
import { LoginComponent } from "./auth/login/login.component";
import { WebSocketAPI } from "./WebSocketAPI";
import { MessageService } from "primeng/api";
import { NotificationService } from "./notification/notification-service.service";
import { VendorAdminService } from "./vendor-admin/service/vendor-admin.service";
import { UserNotificationService } from "./user-notification/user-notification.service";
import { OrderService } from "./orders/service/order.service";
import { ShippmentOrderService } from "./shippment-order/service/shippment-order.service";
import { Order } from "./orders/order.model";
import { Subject } from "rxjs";

const KEY_LOGGED_IN_USER_OBJ = "loggedInUser";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  isLoggedInObservable: Observable<boolean>;
  isUserLoggedIn: boolean = false;
  isAttribute: string = "false";
  isNotificationShown: boolean = true;
  //Test Changes on cloud - by omar iii

  static readonly KEY_NAME_MODULE_NAME: string = "moduleName";
  //static readonly KEY_VALUE_ORDERS_360:string = "Electro 360";
  static readonly KEY_VALUE_ORDERS_360: string = "Orders 360";
  //static readonly KEY_VALUE_ORDERS_360:string = "Act plus";
  title = AppComponent.KEY_VALUE_ORDERS_360;
  lastFourNotifications: any[] = [];
  static countNotifications: any = 0;
  count: any = 0;
  matBadgeVisable: boolean = false;
  isNotificationIconDisplayed: boolean = false;
  static isNotificationIconShown: boolean = false;
  static isLogoutInvoke: boolean = false;
  countCheck: number = 0;
  static userActivity;
  static userInactive: Subject<any> = new Subject();
  static isUserLoggedIn: boolean;
  static countRequstNotificationIconPrivilege: number = 1;
  static isSendToWarehouseNotification: boolean = false;
  static isCountNtificationChamged: boolean = false;
  static shippmentOrder: any;
  static listOfObjects: AppComponent[] = [];
  static countCheck: number = 1;
  constructor(
    public authService?: AuthService,
    private router?: Router,
    private snackBar?: MatSnackBar,
    private notificationService?: NotificationService,
    private vendorAdminService?: VendorAdminService,
    protected userNotificationService?: UserNotificationService,
    protected orderService?: OrderService,
    protected shippmentOrderService?: ShippmentOrderService
  ) {
    AppComponent.listOfObjects.push(this);
  }

  ngOnInit() {
    this.lastFourNotifications = [];
    localStorage.setItem(
      AppComponent.KEY_NAME_MODULE_NAME,
      AppComponent.KEY_VALUE_ORDERS_360
    );
    this.isLoggedInObservable = this.authService.isLoggedIn;
    this.setUserLoggedInStatus();
    localStorage.setItem("shipment_order_id", "");
    localStorage.setItem("sendTowarehuse", "false");
  }

  setCurrentModuleName(value: string, appendOrders360Title: boolean) {
    if (appendOrders360Title) {
      value = AppComponent.KEY_VALUE_ORDERS_360 + " - " + value;
    }

    localStorage.setItem(AppComponent.KEY_NAME_MODULE_NAME, value);
  }

  getCurrentModuleName(): string {
    return localStorage.getItem(AppComponent.KEY_NAME_MODULE_NAME);
  }

  static setCountNotification(counter: number) {
    debugger;
    AppComponent.listOfObjects[0].notificationService
      .getCountUnreadNotifications(
        AppComponent.listOfObjects[0].authService.getCurrentLoggedInUser().id
      )
      .subscribe((data: number) => {
        AppComponent.listOfObjects[0].count = data;
      });
  }
  setUserLoggedInStatus() {
    this.isLoggedInObservable.subscribe((result) => {
      this.isUserLoggedIn = result;
      this.count = localStorage.getItem("countNotifications");
    });
  }

  setPromotionAttribute(): string {
    return localStorage.getItem("attributeParam");
  }

  setDiscount(): string {
    return localStorage.getItem("Discount");
  }

  onLogout() {
    this.authService.logout().subscribe(
      (data) => {
        AppComponent.isUserLoggedIn = false;
        AppComponent.isLogoutInvoke = true;
        this.isNotificationIconDisplayed = false;
        this.authService.onLogoutSuccess(data);
        this.router.navigate(["login"]);
        this.setCurrentModuleName(AppComponent.KEY_VALUE_ORDERS_360, false);
      },
      (errorResponse: HttpErrorResponse) => {
        this.authService.onLogoutFailure(errorResponse);
        this.snackBar.open("Failed to logout. Please try again.", "Close", {
          duration: 3000,
        });
      }
    );
  }

  showNotifications(event) {
    this.vendorAdminService
      .getVendorByVendorAdminId(this.authService.getCurrentLoggedInUser().id)
      .subscribe((vendorId: number) => {
        this.notificationService
          .getLastFourNotificationsForUser(
            this.authService.getCurrentLoggedInUser().id
          )
          .subscribe(
            (notifications) => {
              this.lastFourNotifications = notifications;
            },
            (error) => {
              this.lastFourNotifications = [];
            }
          );
      });
  }

  openNotification(notification) {
    this.userNotificationService
      .updateUserNotification(notification.userNotificationId)
      .subscribe((data) => {
        this.notificationService
          .getCountUnreadNotifications(
            this.authService.getCurrentLoggedInUser().id
          )
          .subscribe((data) => {
            localStorage.setItem("countOfNotifications", data + "");
            AppComponent.countNotifications = data;
            this.count = data;
          });
      });
    if (
      notification.moduleName == "Order" &&
      !notification.isSendToWarehouseFunction
    ) {
      let vendorId = Number(localStorage.getItem("vendorId"));
      this.orderService
        .getOrderByOrderNumber(notification.orderNumber)
        .subscribe((data: number) => {
          localStorage.setItem("orderId", data + "");
          this.orderService.findOrderById(data, vendorId).subscribe(
            async (data) => {
              this.orderService.order = data;
              await this.router.navigateByUrl("orders/all-orders", {
                skipLocationChange: true,
              });
              this.router.navigateByUrl("orders/form");
            },
            (error) => {},
            () => {}
          );
        });
    } else if (notification.moduleName == "Survey") {
      this.router.navigateByUrl("dashboard");
    } else if (notification.moduleName == "Role Support Ticket") {
      this.router.navigateByUrl("ticket/all-tickets");
    } else if (
      notification.moduleName == "Order" &&
      notification.isSendToWarehouseFunction
    ) {
      this.orderService
        .getOrderByOrderNumber(notification.orderNumber)
        .subscribe((orderId: number) => {
          this.orderService
            .findOrderByIdOnly(orderId)
            .subscribe((order: Order) => {
              localStorage.setItem("vend", order.vendor.id + "");
              localStorage.setItem("ordership", order.id + "");
              this.userNotificationService
                .getShippmentOrderIdByUserNotificationId(
                  notification.userNotificationId
                )
                .subscribe((shippmentOrder: number) => {
                  localStorage.setItem("shipmentId", shippmentOrder + "");
                  this.shippmentOrderService
                    .findShipmentOrderById(shippmentOrder, order.vendor.id)
                    .subscribe(async (res) => {
                      this.shippmentOrderService.shippmentOrder = res;
                      AppComponent.shippmentOrder = res;
                      localStorage.setItem(
                        "shipment_order_id",
                        this.shippmentOrderService.shippmentOrder.id + ""
                      );
                      AppComponent.isSendToWarehouseNotification = true;
                      await this.router.navigateByUrl(
                        "shipment_order/all-shipment",
                        {
                          skipLocationChange: true,
                        }
                      );
                      this.router.navigateByUrl("shipment_order/shipment_item");
                    });
                });
            });
        });
    }
  }

  @HostListener("mouseover") getCountNotifications() {
    if (
      !AppComponent.isNotificationIconShown &&
      !this.isUserLoggedIn &&
      this.countCheck == 0
    ) {
      this.isNotificationIconDisplayed = false;
      this.countCheck++;
    } else if (AppComponent.isNotificationIconShown && this.isUserLoggedIn) {
      this.isNotificationIconDisplayed = true;
    }
    if (
      this.isUserLoggedIn &&
      AppComponent.countRequstNotificationIconPrivilege == 1
    ) {
      this.userNotificationService
        .getUserNotificationPrivilege(
          this.authService.getCurrentLoggedInUser().id
        )
        .subscribe((data) => {
          if (data > 0) {
            this.isNotificationIconDisplayed = true;
          }
          AppComponent.countRequstNotificationIconPrivilege++;
          if (AppComponent.countNotifications == 0 && this.isUserLoggedIn) {
            this.matBadgeVisable = true;
          } else if (
            AppComponent.countNotifications != 0 &&
            this.isUserLoggedIn
          ) {
            this.matBadgeVisable = false;
          }
          if (AppComponent.countNotifications == 0 && this.isUserLoggedIn) {
            this.matBadgeVisable = true;
          } else if (
            AppComponent.countNotifications != 0 &&
            this.isUserLoggedIn &&
            this.countCheck == 1
          ) {
            this.matBadgeVisable = false;
            this.count = AppComponent.countNotifications;
            this.countCheck++;
          }
        });
    }
  }
}
@Component({
  selector: "session-time-out-dialog",
  templateUrl: "./session-time-out-dialog.html",
  styleUrls: ["./app.component.css"],
})
export class SessionTimeOutDialog {
  constructor(
    public dialogRef: MatDialogRef<SessionTimeOutDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {}

  onClose(): void {
    this.dialogRef.close();
  }
}
