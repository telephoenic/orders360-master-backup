import { Component, OnInit } from '@angular/core';
import { MatMenuItem, MatButton, MatIcon, MatMenu, MatTooltip, MatSnackBar } from "@angular/material";
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../auth/service/auth.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedInObservable: Observable<boolean>;

  constructor(public authService:AuthService,
              private router:Router,
              private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.isLoggedInObservable = this.authService.isLoggedIn;
  }

  onLogout() {
    this.authService.logout()
                    .subscribe (data => {
                      this.authService.onLogoutSuccess(data);
                      this.router.navigate(["login"]);
                    }, 
                    (errorResponse:HttpErrorResponse)  => {
                      this.authService.onLogoutFailure(errorResponse);
                      this.snackBar.open( "Failed to logout. Please try again.", 
                                          "Close", 
                                          {duration: 3000});
                    });
  }
}
