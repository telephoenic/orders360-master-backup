import { AuthService } from './../../../auth/service/auth.service';
import { VendorAdminService } from './../../../vendor-admin/service/vendor-admin.service';
import {BaseViewComponent} from '../../../common/view/base-view-component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
  MatTable, MatHeaderCellDef, MatCellDef,
  MatColumnDef, MatPaginator, MatSort,
  MatInput, MatCheckbox,
  MatButton,
  MatSnackBar
} from "@angular/material";
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromEvent';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from "../../../../environments/environment";

import {
  ITdDataTableColumn,
  TdSearchBoxComponent,
  TdSearchInputComponent,
  TdPagingBarComponent,
  ITdDataTableSortChangeEvent,
  TdDataTableSortingOrder,
  IPageChangeEvent,
  TdDataTableService,
  TdDialogService,
  TdDataTableComponent
} from '@covalent/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { Subscription } from '../../subscription.model';
import { SubscriptionService } from '../../service/subscription.service';

@Component({
  selector: 'all-subscriptions',
  templateUrl: './all-subscriptions.component.html',
  styleUrls: ['./all-subscriptions.component.css']
})
export class AllSubscriptionsComponent extends BaseViewComponent implements OnInit{
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  
  private _allSubscriptions: Subscription[] = [];
  private _errors: any = '';
  private _selectedRows: Subscription[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string ='';
  
  @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
  @ViewChild('dataTable') dataTable : TdDataTableComponent;

	

  columnsConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name' ,tooltip:"Name"},
    { name: 'type.name', label: 'Type',tooltip:"Type" , width:200},
    { name: "inactive", label: "Active/Inactive", tooltip : "Active or Inactive", width:100},
    { name: 'id', label: 'Edit', tooltip:"Edit" , width:100}
  ];

  constructor(protected subscriptionService: SubscriptionService,
              protected router:Router,
              protected dialogService : TdDialogService,
              protected snackBar:MatSnackBar  , 
              protected authService: AuthService,
              protected permissionsService:NgxPermissionsService,
              protected vendorAdminService: VendorAdminService) { 
    super(subscriptionService, router, dialogService, snackBar,authService);
  }

  ngOnInit() {
    super.ngOnInit();

    this.permissionsService.hasPermission("ROLE_SUBSCRIPTION_EDIT").then((result:boolean)=> {
      if(!result) {
        for(let i =0; i < this.dataTable.columns.length; i++) {
          if(this.dataTable.columns[i].name == 'id') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });


    this.permissionsService.hasPermission("ROLE_SUBSCRIPTION_ACTIVATE_DEACTIVATE_PER_ROW").then((result:boolean)=> {
      if(!result) {
        for(let i =0; i < this.dataTable.columns.length; i++) {
          if(this.dataTable.columns[i].name == 'inactive') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): Subscription[]  {
		return this._allSubscriptions;
	}

	public set allModels(value: Subscription[] ) {
		this._allSubscriptions = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): Subscription[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: Subscription[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
	}
}