import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormControl, Validators, NgControlStatus, NgForm } from '@angular/forms';
import { MatFormFieldControl, MatButton, MatList, MatSuffix, MatInput, MatSnackBar, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material";
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { VendorService } from '../../vendor/Services/vendor.service';
import { environment } from '../../../environments/environment';
import { Vendor } from '../../vendor/vendor.model';
import { UserPrivilegeProfileComponent } from '../../user-privilege/views/user-privilege-profile.component';
import { BaseViewComponent } from '../../common/view/base-view-component';
import { BaseUserManagementForm } from '../../user-management/base-user-management-form';
import { CityService } from '../../location/city/service/city.service';
import { City } from '../../location/city/City.model';
import { Region } from '../../location/region/Region.model';
import { RegionService } from '../../location/region/service/region.service';
import { Subscription } from '../subscription.model';
import { SubscriptionService } from '../service/subscription.service';
import { LOCATION_INITIALIZED } from '@angular/common';
import { ITdDataTableColumn, TdDialogService, TdDataTableComponent } from '@covalent/core';
import { LoyaltyPointsTrxSourceService } from '../../loyalty/service/loyalty-points-trx-source.service';
import { PosSubscriptionService } from '../service/pos-subscription.service';
import { PosSubscription } from '../pos-subscription.model';

@Component({
  selector: 'subscription-form',
  templateUrl: './subscription-form.component.html',
  styleUrls: ['./subscription-form.component.css']
})
export class SubscriptionFormComponent extends BaseFormComponent implements OnInit {
 
  @ViewChild("subscriptionForm") 
  subscriptionForm : NgForm; 

  @ViewChild("minToMaxDataTable") 
  minToMaxDataTable : TdDataTableComponent;

  minToMaxDataTableData:{id: number, minNumber:number, maxNumber:number}[] = [];

  minToMaxDataTableColumnsConfig: ITdDataTableColumn[] = [ 
    { name: 'minNumber', label: 'Min Number', sortable:false},
    { name: 'maxNumber', label: 'Max Number', sortable:false}
  ];

  minToMaxDataTableSelectedRows:{id:number, minNumber:number, maxNumber:number}[]=[];

  subscription : Subscription;
  deletedPosSubscriptionIdsTransient : number[] = [];
  _isEdit:boolean = false;
  _isPosSubscription:boolean = false;


  constructor(protected subscriptionService: SubscriptionService,
              private vendorService: VendorService,
              private cityService: CityService,
              private regionService: RegionService,
              private posSubscriptionService: PosSubscriptionService,
              protected route:ActivatedRoute,
              protected router:Router,
              protected snackBar:MatSnackBar,
              private minToMaxDialog:MatDialog,
              protected dialogService : TdDialogService) { 
    
    super(subscriptionService, 
          route,
          router,
          snackBar);
  }
 
  ngOnInit() {
    if(this.route.snapshot.queryParams["edit"] == '1') {
      this.isEdit = true;
      this.subscription = this.subscriptionService.subscription;

      if(this.subscription.type.code == 'PS') { //TODO define as enum - const
        this._isPosSubscription = true;
      }
    } else {
      this.subscription = new Subscription();
    }

    this.loadMinToMax();
  }  

  getLeafPosSubscriptionInstance(subscriptionId:number) {
    let posSubscription = new PosSubscription();
    let subscription = new Subscription();
    subscription.id = subscriptionId;
    posSubscription.subscription = subscription;
    
    return posSubscription;
  }

  loadMinToMax() {
    if(this.isEdit && this.isPosSubscription) { 
      this.posSubscriptionService.findByExample(this.getLeafPosSubscriptionInstance(this.subscription.id)).subscribe(
        data => this.minToMaxDataTableData = data["content"]
      );
    }
  }

  removeFrom(view:BaseViewComponent, event) {
    if(event.rows) {
      for(let i=0; i<event.rows.length;i++) {
        view.allModels.splice(view.allModels.indexOf(event.rows[i]), 1);
      }
    }
  }

  compareWith(row: any, model: any): boolean { 
    return row.minNumber === model.minNumber;
  }

  onNewMinMax() {
    this.openDialog({id:null, minNumber: null, maxNumber: null}, false);
  }

  onDeleteMinMax() {
    if(this.minToMaxDataTableSelectedRows.length > 0) {
      this.dialogService.openConfirm({
          message: "Are you sure you want to delete the selected row(s)?",
          title: "confirmation",
          cancelButton: "Disagree",
          acceptButton: "Agree"})
      .afterClosed().subscribe((accept:boolean) => {
          if(accept) {
            for(let i=0; i< this.minToMaxDataTableSelectedRows.length; i++) {
              let elementIndexToDelete = 
                      this.minToMaxDataTableData.indexOf(this.minToMaxDataTableSelectedRows[i]);

              this.deletedPosSubscriptionIdsTransient.push(this.minToMaxDataTableSelectedRows[i].id);
              this.minToMaxDataTableData.splice(elementIndexToDelete, 1);
            }
            this.minToMaxDataTableSelectedRows.splice(0, this.minToMaxDataTableSelectedRows.length);
            this.minToMaxDataTable.refresh();
          }
      });
    } else if(this.minToMaxDataTableData.length ==0) {
        this.snackBar.open("No data found.", "close", {duration: 3000});
    } else {
      this.snackBar.open("No selected rows.", "close", {duration: 3000});
    }
  }

  onEditMinToMax(row) {
    this.openDialog(row, true);
  }

  openDialog(minToMaxRow:{id:number, minNumber:number, maxNumber:number}, isEdit:boolean): void {
    let dialogRef = this.minToMaxDialog.open(SubscriptionMinToMaxDialog, {
      width: '250px',
      data: {id: minToMaxRow.id, minNumber: minToMaxRow.minNumber, maxNumber: minToMaxRow.maxNumber }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.onBeforeSaveMinToMax(result);

        if(isEdit) {
          let index =this.minToMaxDataTableData.indexOf(minToMaxRow);
          this.minToMaxDataTableData[index] = result;
        } else {
          this.minToMaxDataTableData.push(result);
        }

        this.minToMaxDataTable.refresh();
      }
    });
  }

  onBeforeSaveMinToMax(result) {
    let minNumber = result.minNumber;
    let maxNumber = result.maxNumber;

    if(minNumber > maxNumber) {
      this.snackBar.open( "Min number shouldn't be greater than max number", "close", {duration: 3000});
      throw Error("Min number shouldn't be greater than max number");
    }
    
  }

  onBeforeEdit(subscription:Subscription) {
    this.prepareModelProperties(subscription);
  }

  prepareModelProperties(subscription:Subscription) { 
    if(this.minToMaxDataTableData.length == 0) {
      this.snackBar.open( "At least one min number and max number record should be defined", 
                          "close", 
                          {duration: 3000});

      throw Error("At least one quantity and value record should be defined");
    }

    subscription.subscriptionMinToMaxTransient = this.minToMaxDataTableData;
    subscription.deletedPosSubscriptionIdsTransient = this.deletedPosSubscriptionIdsTransient;
  }

  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get isPosSubscription() : boolean {
    return this._isPosSubscription;
  }

  set isPosSubscription(isPosSubscription: boolean) {
    this._isPosSubscription = isPosSubscription;
  }
  
  get model() : Subscription {
    return this.subscription;
  }

  set model(subscription:Subscription){
    this.subscription = subscription;
  }
  
  initModel(): Subscription {
    return new Subscription();
  }

  ngFormInstance(): NgForm {
    return this.subscriptionForm;
  }
}

@Component({
  selector: 'subscription-min-to-max-dialog',
  templateUrl: 'subscription-min-to-max-dialog.html',
})
export class SubscriptionMinToMaxDialog {

  constructor(public dialogRef: MatDialogRef<SubscriptionMinToMaxDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}