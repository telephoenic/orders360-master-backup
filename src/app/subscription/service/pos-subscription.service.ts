import { Injectable } from "@angular/core";
import { BaseService } from "../../common/service/base.service";
import { PosSubscription } from "../pos-subscription.model";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../../auth/service/auth.service";
import { BaseModel } from "../../common/model/base-model";


@Injectable()
export class PosSubscriptionService extends BaseService {
  posSubscripion:PosSubscription;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient, protected authService:AuthService) {
    super(http, "pos_subscription", authService)
  }

  get model(){
    return this.posSubscripion;
  }

  set model(model:BaseModel) {
    this.posSubscripion = <PosSubscription>model;
  }
}