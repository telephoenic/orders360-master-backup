import {BaseModel} from '../../common/model/base-model';
import { Injectable, ViewChild } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { DataSource } from '@angular/cdk/collections';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { MatSort } from '@angular/material';
import { TdDataTableSortingOrder } from '@covalent/core';
import { environment } from "../../../environments/environment";
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Subscription } from '../subscription.model';

@Injectable()
export class SubscriptionService extends BaseService {
  subscription:Subscription;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient, protected authService:AuthService) {
    super(http, "subscription", authService)
  }

  get model(){
    return this.subscription;
  }

  set model(model:BaseModel) {
    this.subscription = <Subscription>model;
  }
}