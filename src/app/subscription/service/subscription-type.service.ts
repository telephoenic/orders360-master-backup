import {BaseModel} from '../../common/model/base-model';
import { Injectable, ViewChild } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { DataSource } from '@angular/cdk/collections';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { MatSort } from '@angular/material';
import { TdDataTableSortingOrder } from '@covalent/core';
import { environment } from "../../../environments/environment";
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Subscription } from '../subscription.model';
import { SubscriptionType } from '../subscription-type.model';

@Injectable()
export class SubscriptionTypeService extends BaseService {
  subscriptionType:SubscriptionType;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient, protected authService:AuthService) {
    super(http, "subscription_type", authService)
  }

  get model(){
    return this.subscriptionType;
  }

  set model(model:BaseModel) {
    this.subscriptionType = <SubscriptionType>model;
  }
}