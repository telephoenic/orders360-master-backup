import {BaseModel} from '../common/model/base-model';
import { Vendor } from '../vendor/vendor.model';
import { UserPrivilegeProfile } from '../user-privilege/user-privilege-profiles';
import { UserPrivilegeProfileAware } from '../user-management/user-privilege-profile-aware';
import { Region } from '../location/region/Region.model';

export class SubscriptionType extends BaseModel {
    
    id: number;
    code: string;
    name: string;
    inactive:boolean;
    
    constructor () {
        super();
    }
}