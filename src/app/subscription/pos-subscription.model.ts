import {BaseModel} from '../common/model/base-model';
import { Subscription } from './subscription.model';

export class PosSubscription extends BaseModel {
    
    id: number;
    minNumber: number;
    maxNumber:number;
    subscription:Subscription;
    inactive:boolean; 
    
    constructor (){
        super();

        this.subscription = new Subscription();
    }

}