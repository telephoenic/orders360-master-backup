import {BaseModel} from '../common/model/base-model';
import { Vendor } from '../vendor/vendor.model';
import { UserPrivilegeProfile } from '../user-privilege/user-privilege-profiles';
import { UserPrivilegeProfileAware } from '../user-management/user-privilege-profile-aware';
import { Region } from '../location/region/Region.model';
import { SubscriptionType } from './subscription-type.model';

export class Subscription extends BaseModel {
    
    id: number;
    name: string;
    type:SubscriptionType;
    subscriptionMinToMaxTransient:{minNumber:number, maxNumber:number}[]=[];
    inactive:boolean; 
    deletedPosSubscriptionIdsTransient:number[]=[];
    
    constructor (){
        super();

        this.type = new SubscriptionType();     
    }

}