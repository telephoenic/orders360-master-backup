import { Component, OnInit } from '@angular/core';
import { MatSidenavContainer, MatSidenav, MatButton, MatCard  } from "@angular/material";
import { TdDialogService } from "@covalent/core";
import { Router } from '@angular/router';
import { SubscriptionService } from './service/subscription.service';


@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit {

  constructor(private dialogService : TdDialogService,
              private subscriptionService: SubscriptionService,
              private router:Router) { }

  ngOnInit() { }
  
}