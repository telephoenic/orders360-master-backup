import { Injectable } from '@angular/core';
import { Application } from './application';
import { BaseModel } from '../../common/model/base-model';
import { Http } from "@angular/http";
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApplicationService  extends BaseService{
  
    app:Application;
    navigateToAfterCompletion: string;
  
    constructor(protected http:HttpClient,
      protected authService:AuthService) { 
      super(http, "structure/app", authService);
    }
  
    get model(){
      return this.app;
    }
    set model(model:BaseModel) {
      this.app = <Application>model;
    }
}
