import { BaseModel } from "../../common/model/base-model";

export class Application extends BaseModel {
    id: number;
    inactive:boolean;
    name: string;
    technicalName:string;
}