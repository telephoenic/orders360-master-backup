import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { Module } from './module';
import { Http } from "@angular/http";
import { BaseModel } from '../../common/model/base-model';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class ModuleService  extends BaseService{
  
    module:Module;
    navigateToAfterCompletion: string;
  
    constructor(protected http:HttpClient,
      protected authService:AuthService) { 
      super(http, "module", authService);
    }

    getModules():Observable<any>{
      return this.http.get(environment.baseURL+"structure/module/modules-for-notification");
    }

  
    get model(){
      return this.module;
    }
    set model(model:BaseModel) {
      this.module = <Module>model;
    }
}
