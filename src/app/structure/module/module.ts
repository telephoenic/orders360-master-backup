import { BaseModel } from "../../common/model/base-model";
import { Application } from "../application/application";

export class Module extends BaseModel {
    id: number;
    inactive:boolean;
    name: string;
    technicalName:string;
    application:Application;

    constructor() {
        super();
        this.application = new Application();
    }
}