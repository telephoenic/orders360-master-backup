import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { View } from './view';
import { Http } from "@angular/http";
import { ApplicationRole } from '../../role/application/application-role';
import { BaseModel } from '../../common/model/base-model';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ViewService extends BaseService{
  
    view:View;
    navigateToAfterCompletion: string;
  
    constructor(protected http:HttpClient,
      protected authService:AuthService) { 
      super(http, "structure/view", authService);
    }
  
    get model(){
      return this.view;
    }
    set model(model:BaseModel) {
      this.view = <View>model;
    }
}
