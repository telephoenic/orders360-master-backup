import { BaseModel } from "../../common/model/base-model";
import { Module } from "../module/module";

export class View extends BaseModel {
    id: number;
    inactive:boolean;
    name: string;
    technicalName:string;
    module:Module;
}