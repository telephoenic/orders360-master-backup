import { BaseModel } from "../../common/model/base-model";
import { Application } from "../application/application";
import { Module } from "../module/module";

export class Block extends BaseModel {
    id: number;
    inactive:boolean;
    name: string;
    technicalName:string;
    module:Module;
}