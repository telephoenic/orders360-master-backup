import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { Block } from './block';
import { Http } from "@angular/http";
import { BaseModel } from '../../common/model/base-model';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BlockService  extends BaseService{
  
    block:Block;
    navigateToAfterCompletion: string;
  
    constructor(protected http:HttpClient,
      protected authService:AuthService) { 
      super(http, "structure/block", authService);
    }
  
    get model(){
      return this.block;
    }
    set model(model:BaseModel) {
      this.block = <Block>model;
    }
}
