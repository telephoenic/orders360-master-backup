import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { ApplicationRole } from './application-role';
import { Http } from "@angular/http";
import { BaseModel } from '../../common/model/base-model';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApplicationRoleService extends BaseService{

  appRole:ApplicationRole;
  navigateToAfterCompletion: string;

  constructor(protected http:HttpClient,
    protected authService:AuthService) { 
    super(http, "role/app", authService);
  }

  get model(){
    return this.appRole;
  }
  set model(model:BaseModel) {
    this.appRole = <ApplicationRole>model;
  }

}
