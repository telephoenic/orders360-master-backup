import { BaseModel } from "../../common/model/base-model";
import { Application } from "../../structure/application/application";

export class ApplicationRole extends BaseModel {
    id: number;
    inactive:boolean;
    name: string;
    technicalName:string;
    application:Application;

    constructor() {
        super();

        this.application = new Application();
    }
    
}