import { BaseModel } from "../../common/model/base-model";
import { Module } from "../../structure/module/module";

export class ModuleRole extends BaseModel {
    id: number;
    inactive:boolean;
    name: string;
    technicalName:string;
    module:Module;
    selectedInPrivilegeProfile:boolean;
    
    constructor() {
        super();

        this.module = new Module();
    }
}