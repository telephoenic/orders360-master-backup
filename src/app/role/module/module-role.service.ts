import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { ModuleRole } from '../module/module-role';
import { Http } from "@angular/http";
import { BaseModel } from '../../common/model/base-model';
import { environment } from '../../../environments/environment';
import { Observable } from "rxjs/Observable";
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ModuleRoleService extends BaseService{

  static readonly URL_FILTER_BY_APP_ROLE_ID = "/by-app-role";
  moduleRole:ModuleRole;
  navigateToAfterCompletion: string;

  constructor(protected http:HttpClient,
    protected authService:AuthService) { 
    super(http, "role/module", authService);
  }

  get model(){
    return this.moduleRole;
  }
  set model(model:BaseModel) {
    this.moduleRole = <ModuleRole>model;
  }
}
