import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { ViewRole } from '../view/view-role';
import { Http } from "@angular/http";
import { BaseModel } from '../../common/model/base-model';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ViewRoleService extends BaseService {
  static readonly URL_VIEW_ROLE_FILTER: string = "/by-mod-role";

  viewRole: ViewRole;
  navigateToAfterCompletion: string;

  constructor(protected http: HttpClient,
    protected authService:AuthService) {
    super(http, "role/view", authService);
  }

  get model() {
    return this.viewRole;
  }
  set model(model: BaseModel) {
    this.viewRole = <ViewRole>model;
  }

  getFilterURL() {
    return ViewRoleService.URL_VIEW_ROLE_FILTER;
  }
}