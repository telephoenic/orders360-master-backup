import { BaseModel } from "../../common/model/base-model";
import { View } from "../../structure/view/view";

export class ViewRole extends BaseModel {
    id: number;
    inactive:boolean;
    name: string;
    technicalName:string;
    view:View;
    selectedInPrivilegeProfile:boolean;
}