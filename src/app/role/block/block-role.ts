import { BaseModel } from "../../common/model/base-model";
import { Block } from "../../structure/block/block";

export class BlockRole extends BaseModel {
    id: number;
    inactive:boolean;
    name: string;
    technicalName:string;
    block:Block;
    selectedInPrivilegeProfile:boolean;
}