import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { BlockRole } from './block-role';
import { Http } from "@angular/http";
import { BaseModel } from '../../common/model/base-model';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BlockRoleService extends BaseService {
  static readonly URL_BLOCK_ROLE_FILTER: string = "/filter/by-module";

  blockRole: BlockRole;
  navigateToAfterCompletion: string;

  constructor(protected http: HttpClient,
    protected authService:AuthService) {
    super(http, "role/block", authService);
  }

  get model() {
    return this.blockRole;
  }
  set model(model: BaseModel) {
    this.blockRole = <BlockRole>model;
  }

  getFilterURL() {
    return BlockRoleService.URL_BLOCK_ROLE_FILTER;
  }

  filterByAppId(pageNumber: number,
                pageSize: number,
                searchTerm: string,
                sortBy: string,
                sortOrder: string,
                moduleRoleIds: number[],
                privilegeProfileId?: number): Observable<BaseModel> {
                let parameterList: { paramName: string, paramValue: string }[] = [];

    if (privilegeProfileId != undefined) {
      parameterList.push({ paramName: "privilege_profile_id", paramValue: String(privilegeProfileId) })
    }

    if (moduleRoleIds != undefined) {
      for (let i = 0; i < moduleRoleIds.length; i++) {
        parameterList.push({ paramName: "m_id", paramValue: String(moduleRoleIds[i]) })
      }
    }

    return this.filter( pageNumber - 1,
                        pageSize,
                        searchTerm,
                        sortBy,
                        sortOrder,
                        BlockRoleService.URL_BLOCK_ROLE_FILTER,
                        (parameterList.length > 0 ? parameterList : undefined));
  }
}
