import {BaseModel} from '../../common/model/base-model';
import { Injectable, ViewChild } from '@angular/core';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { SalesSupervisor } from "../sales-supervisor.model";
import { environment } from "../../../environments/environment";
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SalesSupervisorService extends BaseService{
  salesSupervisor:SalesSupervisor;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient, protected authService:AuthService) {
    super(http, "sales_supervisor", authService)
  }

  getAllSalesSupervisorsByVendorId(pageNumber:number, 
                                   pageSize:number, 
                                   vendorId:number,
                                   searchTerm:string,
                                   sortBy:string, 
                                   sortOrder:string) : Observable<any> {
    return this.http.get( environment.baseURL + 
                          "sales_supervisor/salessupervisor_for_vendor/" + 
                          pageNumber + "/" + 
                          pageSize + "/" + 
                          vendorId, { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder), 
                          headers: this.getBasicHeaders()});
}



  get model(){
    return this.salesSupervisor;
  }

  set model(model:BaseModel) {
    this.salesSupervisor = <SalesSupervisor>model;
  }
}