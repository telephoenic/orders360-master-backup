import {BaseModel} from '../common/model/base-model';
import { Vendor } from '../vendor/vendor.model';
import { UserPrivilegeProfile } from '../user-privilege/user-privilege-profiles';
import { UserPrivilegeProfileAware } from '../user-management/user-privilege-profile-aware';
import { Region } from '../location/region/Region.model';
import { UserType } from '../lookups/user-type.model';

export class SalesSupervisor extends UserPrivilegeProfileAware {
    
    id: number;
    name: string;
    email: string;
    mobileNumber: string;
    type: UserType;
    vendor:Vendor;
    inactive:boolean;
    regions:Region[];
    privilegeProfiles:UserPrivilegeProfile[] = [];
    
    constructor (){
        super();

        this.vendor = new Vendor();
        this.type = new UserType(4);
    }

}