import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, NgControlStatus, NgForm } from '@angular/forms';
import { MatFormFieldControl, MatButton, MatList, MatSuffix, MatInput, MatSnackBar } from "@angular/material";
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { VendorService } from '../../vendor/Services/vendor.service';
import { environment } from '../../../environments/environment';
import { Vendor } from '../../vendor/vendor.model';
import { UserPrivilegeProfileComponent } from '../../user-privilege/views/user-privilege-profile.component';
import { BaseViewComponent } from '../../common/view/base-view-component';
import { BaseUserManagementForm } from '../../user-management/base-user-management-form';
import { CityService } from '../../location/city/service/city.service';
import { City } from '../../location/city/City.model';
import { SalesSupervisorRegionComponent } from '../views/region/sales-supervisor-region.component';
import { Region } from '../../location/region/Region.model';
import { RegionService } from '../../location/region/service/region.service';
import { SalesSupervisor } from '../sales-supervisor.model';
import { SalesSupervisorService } from '../service/sales-supervisor.service';
import { User } from '../../auth/user.model';
import { AuthService } from '../../auth/service/auth.service';
import { UserType } from '../../lookups/user-type.model';
import { UserTypeService } from '../../lookups/service/user-type.service';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';

@Component({
  selector: 'sales-supervisor-form',
  templateUrl: './sales-supervisor-form.component.html',
  styleUrls: ['./sales-supervisor-form.component.css']
})
export class SalesSupervisorFormComponent extends BaseUserManagementForm {
 
  @ViewChild("salesSupervisorForm") 
  salesSupervisorForm : NgForm;

  @ViewChild("allNotSelectedSalesSupervisorRegion")
  allNotSelectedSalesSupervisorRegion:SalesSupervisorRegionComponent;

  @ViewChild("allSelectedSalesSupervisorRegion")
  allSelectedSalesSupervisorRegion:SalesSupervisorRegionComponent;

  @ViewChild("notSelectedPrivilegeProfiles")
  notSelectedPrivilegeProfiles:UserPrivilegeProfileComponent;

  @ViewChild("selectedPrivilegeProfiles")
  selectedPrivilegeProfiles:UserPrivilegeProfileComponent;

  salesSupervisor : SalesSupervisor;
  _isEdit:boolean = false;
  _isUserLoggedInVendor:boolean = false;
  loggedInVendor:Vendor;
  _cityId:number;
  listOfVendors:Vendor[]; 
  listOfCities:City[];
  countryId:number;
  constructor(protected salesSupervisorService: SalesSupervisorService,
              protected authService:AuthService,
              private vendorService: VendorService,
              private cityService: CityService,
              private regionService: RegionService,
              private userTypeService: UserTypeService,
              protected route:ActivatedRoute,
              protected vendorAdminService:VendorAdminService,
              protected router:Router,
              protected snackBar:MatSnackBar) { 
    
    super(salesSupervisorService, 
          route,
          router,
          snackBar);
  }

  ngOnInit() {
    this.selectedSecreen=environment.ENUM_CODE_USER_TYPE.SalesAgentUser;
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    if(this.route.snapshot.queryParams["edit"] == '1') {
      this.isEdit = true;
      this.salesSupervisor = this.salesSupervisorService.salesSupervisor;
    } else {
      this.salesSupervisor = new SalesSupervisor();
      //this.loadUserType();
    }

    this.loadVendorOptions();
    this.loadCityOptions();
    this.loadRegions();
  }

  // loadUserType() {
  //   this.userTypeService.findByExample(this.getLeafUserTypeInstance()).subscribe(
  //     data => this.salesSupervisor.type = data["content"]
  //   );
  // }

  // getLeafUserTypeInstance() {
  //   let userType = new UserType();
  //   userType.code = environment.ENUM_ID_USER_TYPE_CODE.SalesSupervisor;
  //   return userType;
  // }

  loadVendorOptions() {
    let loggedInUser:User = this.authService.getCurrentLoggedInUser();

    if(loggedInUser != undefined) {
      if(loggedInUser.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
        this.vendorService.filterNotPageable("", "", "", "/vendor_by_vendor_admin_id/" + loggedInUser.id).subscribe(
          (data:Vendor) => { 
              data => this.loggedInVendor = data;
              if(data != null) {
                this.loggedInVendor = new Vendor(data['id'], data['name']);
              }
              this.onCompleteLoadingVendor(this.loggedInVendor);
            }
          );
      } else if(loggedInUser.type.id == environment.ENUM_ID_USER_TYPE.TelephoenicAdmin) {
        this.vendorService.filterNotPageable("", "", "", "/vendor_active_list")
                      .subscribe((data:Vendor[]) => { 
                          this.listOfVendors = data; }
                      );
      }
    }
  }

  onCompleteLoadingVendor(loggedInVendor:Vendor) {
    if(loggedInVendor == undefined) {
      this._isUserLoggedInVendor = false;
      this.vendorService.filter(0, environment.maxListFieldOptions, "", "", "").subscribe(
          data => this.listOfVendors = data['content'] 
      );
    } else {
      this._isUserLoggedInVendor = true;
      this.salesSupervisor.vendor = loggedInVendor;
    }
  }

  loadCityOptions() {
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(
      (vendorId:number)=>{
        this.vendorService.getCountryIdByVendorId(vendorId)
        .subscribe((countryId:number) => {
                    this.countryId = countryId;
                    this.cityService.findAllCiityByCountryId(countryId)
                    .subscribe(data=>{
                      this.listOfCities = data;
                      this.loadRegions();
                        });
                     });  
              });
  }

  loadRegions() {
    this.loadSelectedRegions();
    this.loadNotSelectedRegions();
  }

  loadSelectedRegions() {
    if( this.isEdit ) {
      this.regionService.filterNotPageable("", "", "", "/region_for_sales_supervisor/" + this.salesSupervisor.id)
                      .subscribe((data:Region[]) => {
                          this.allSelectedSalesSupervisorRegion.allModels = data;
                      });
    }
  }


  loadNotSelectedRegions() {
    let parameterList:{paramName:string, paramValue: string}[]=[];
    let salesSupervisorId = this.salesSupervisor.id;
    let cityId = this.cityId;
    
    if(salesSupervisorId != undefined) {
      parameterList.push({paramName:"sales_supervisor_id", paramValue: String(salesSupervisorId)})
    }

    if(cityId != undefined) {
      parameterList.push({paramName:"city_id", paramValue: String(cityId)})
    }
    parameterList.push({paramName:"country_id", paramValue: String(this.countryId) });
    this.regionService.filterNotPageable("", "", "", "/region_not_for_sales_supervisor",
                                    (parameterList.length > 0?parameterList:undefined))
                            
                    .subscribe((data:Region[]) => {
                      this.allNotSelectedSalesSupervisorRegion.allModels = data;
                    });
  }

  onSelectingRegion(event) {
    if(event.movedAsAssigned) {
      this.pushTo(this.allSelectedSalesSupervisorRegion, event);
      this.removeFrom(this.allNotSelectedSalesSupervisorRegion, event);
    } else {
      this.pushTo(this.allNotSelectedSalesSupervisorRegion, event);
      this.removeFrom(this.allSelectedSalesSupervisorRegion, event);
    }

    this.allSelectedSalesSupervisorRegion.dataTable.refresh();
    this.allNotSelectedSalesSupervisorRegion.dataTable.refresh();
  }

  onCityChange(event) {
    this.loadNotSelectedRegions();
    this.allSelectedSalesSupervisorRegion.dataTable.refresh();
  }

  onBeforeEdit(salesSupervisor:SalesSupervisor) {
    super.onBeforeEdit(salesSupervisor);
    this.prepareModelProperties(salesSupervisor);
  }

  onBeforeSave(salesSupervisor:SalesSupervisor) {
    super.onBeforeSave(salesSupervisor);
    this.prepareModelProperties(salesSupervisor);
  }
 
  onBeforeSaveAndNew(salesSupervisor:SalesSupervisor){
    super.onBeforeSaveAndNew(salesSupervisor);
    this.prepareModelProperties(salesSupervisor);

    if(this.isUserLoggedInVendor) {
      salesSupervisor.vendor = this.loggedInVendor;
    }
  }

  onSaveAndNewCompleted() {
    this.allSelectedSalesSupervisorRegion.allModels = [];
    this.allSelectedSalesSupervisorRegion.dataTable.refresh();
    
    this.loadNotSelectedRegions();
    this.allNotSelectedSalesSupervisorRegion.dataTable.refresh();

    this.getNotSelectedPrivilegeProfiles().filterPrivilegeProfileForUser();
    this.notSelectedPrivilegeProfiles.dataTable.refresh();

    this.selectedPrivilegeProfiles.allModels = [];
    this.selectedPrivilegeProfiles.dataTable.refresh();

    if(this.isUserLoggedInVendor) {
      this.salesSupervisor.vendor = this.loggedInVendor;
    }
  }

  prepareModelProperties(salesSupervisor:SalesSupervisor) {
    salesSupervisor.regions = this.allSelectedSalesSupervisorRegion.allModels;
    
    if(this.allSelectedSalesSupervisorRegion.allModels.length == 0) {
      this.snackBar.open( "At least one Area should be selected.", 
                          "close", 
                          {duration: 3000});

      throw Error("At least one Area should be selected.");
    }

    if(this.selectedPrivilegeProfiles.allModels.length == 0) {
      this.snackBar.open( "At least one privilege profile should be selected.", 
                          "close", 
                          {duration: 3000});

      throw Error("At least one region should be selected.");
    }
  }

  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get isUserLoggedInVendor() : boolean {
    return this._isUserLoggedInVendor;
  }

  set isUserLoggedInVendor(isUserLoggedInVendor: boolean) {
    this._isUserLoggedInVendor = isUserLoggedInVendor;
  }
  
  get model() : SalesSupervisor {
    return this.salesSupervisor;
  }

  set model(salesSupervisor:SalesSupervisor){
    this.salesSupervisor = salesSupervisor;
  }

  get cityId() : number {
    return this._cityId;
  }

  set cityId(cityId:number){
    this._cityId = cityId;
  }
  
  initModel(): SalesSupervisor {
    return new SalesSupervisor();
  }

  ngFormInstance(): NgForm {
    return this.salesSupervisorForm;
  }

  getSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.selectedPrivilegeProfiles;
  }
  getNotSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.notSelectedPrivilegeProfiles;
  }
}