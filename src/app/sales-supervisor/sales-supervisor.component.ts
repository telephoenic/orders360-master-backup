import { Component, OnInit } from '@angular/core';
import { MatSidenavContainer, MatSidenav, MatButton, MatCard  } from "@angular/material";
import { TdDialogService } from "@covalent/core";
import { SalesSupervisorService } from './service/sales-supervisor.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sales-supervisor',
  templateUrl: './sales-supervisor.component.html',
  styleUrls: ['./sales-supervisor.component.css']
})
export class SalesSupervisorComponent implements OnInit {

  constructor(private dialogService : TdDialogService,
              private salesSupervisorService: SalesSupervisorService,
              private router:Router) { }

  ngOnInit() { }
  
}