import { Component, OnInit, ViewChild } from "@angular/core";
import { UserPrivilegeProfileComponent } from "../../../user-privilege/views/user-privilege-profile.component";
import { NgForm } from "@angular/forms";
import { ViewAssignedVendorFormComponent } from "../../../pos-user/view/view-assigned-vendor/view-assigned-vendor.component";
import { PosGroupManagement } from "../../../posgroupmanagement/pos-group-management.model";
import { VendorAdminService } from "../../../vendor-admin/service/vendor-admin.service";
import { PosGroupManagementService } from "../../../posgroupmanagement/service/pos-group-management.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { FcmService } from "../../../fcm/services/fcm.service";
import { AuthService } from "../../../auth/service/auth.service";
import { PosRequestService } from "../../service/pos-request.service";
import {
  ITdDataTableColumn,
  TdPagingBarComponent,
  TdDataTableComponent,
  TdDialogService
} from "@covalent/core";
import { BaseFormComponent } from "../../../common/form/base-form-component";
import { User } from "../../../auth/user.model";
import { MatDialog } from "@angular/material";
import { RejectReasonDialogComponent } from "../reject-reason-dialog/reject-reason-dialog.component";
import { DatePipe } from "@angular/common";
import { PosRequestStatusService } from "../../service/pos-request-status.service";
import { environment } from "../../../../environments/environment";
import { HttpErrorResponse } from "@angular/common/http";
import { PosRequest } from "../../model/pos-request";
import { PosRequestDto } from "../../model/pos-request-dto";
import { PosRequestStatusId } from "../../enum/enum-pos-request-status-id";

@Component({
  selector: "app-pos-request-vendor-form",
  templateUrl: "./pos-request-vendor-form.component.html",
  styleUrls: ["./pos-request-vendor-form.component.css"]
})
export class PosRequestVendorFormComponent extends BaseFormComponent
  implements OnInit {
  @ViewChild("posForm") posForm: NgForm;
  @ViewChild(ViewAssignedVendorFormComponent)
  @ViewChild("notSelectedPrivilegeProfiles")
  notSelectedPrivilegeProfiles: UserPrivilegeProfileComponent;
  @ViewChild("selectedPrivilegeProfiles")
  selectedPrivilegeProfiles: UserPrivilegeProfileComponent;
  @ViewChild("pagingBar") pagingBar: TdPagingBarComponent;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;

  private listOfPosGroup: PosGroupManagement[] = [];
  _isEdit: boolean = false;
  private _allPosRequest: PosRequest[] = [];
  selectedSecreen: string;
  loggedUser: User;
  private vendorId: number;
  posRequestDto: PosRequestDto = new PosRequestDto();
  posRequest : PosRequest = new PosRequest();
  _filteredTotal: number;
  isPending: boolean = false;


  columnsConfig: ITdDataTableColumn[] = [
    {
      name: "posRequestVendor.status.name",
      label: "Status",
      tooltip: "status",
      width: 180
    },
    {
      name: "posRequestVendor.audit.updatedDate",
      label: "Date",
      format: v => this.datePipe.transform(v, "medium"),
      tooltip: "Date",
      width: 240
    },

    {
      name: "posRequestVendor.audit.updatedBy",
      label: "User",
      tooltip: "user",
      width: 180
    },

    {
      name: "reason.rejectedReason.name",
      label: "Rejected Reason ",
      tooltip: "rejectreason ",
      sortable: false,
      width: 180
    },

    {
      name: "reason.description",
      label: "Description",
      tooltip: "description ",
      sortable: false,
      width: 180
    }
  ];

  constructor(
    private datePipe: DatePipe,

    protected vendorAdminService: VendorAdminService,
    protected posGroupManagementService: PosGroupManagementService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected snackBar: MatSnackBar,
    protected fcmService: FcmService,
    protected authService: AuthService,
    protected dialogService: TdDialogService,
    protected posRequestService: PosRequestService,
    protected statusService: PosRequestStatusService,
    protected rejectReasonDialog: MatDialog
  ) {
    super(posRequestService, route, router, snackBar);
  }

  ngOnInit() {
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    this.getVendorIdByLoggedVenodrAdmin();
    this._allPosRequest[0] = this.posRequestService.posRequest;
    this.loadPosGroup();
    this.checkStatus();

  }

  loadPosGroup() {
    this.vendorAdminService
      .getVendorByVendorAdminId(this.loggedUser.id)
      .subscribe(vendorId => {
        this.posGroupManagementService
          .getGroupsByVednor(vendorId)
          .subscribe(data => {
            this.listOfPosGroup = data;
          });
      });
  }

  getVendorIdByLoggedVenodrAdmin() {
    this.vendorAdminService
      .getVendorByVendorAdminId(this.loggedUser.id)
      .subscribe((vendorId: number) => {
        this.vendorId = vendorId;
      });
  }

  openDialog() {
    let dialogRef = this.rejectReasonDialog.open(RejectReasonDialogComponent, {
      width: "30%",
      height: "300px",
      data: this.posRequestService.rejectedReasonList
    });
    dialogRef.afterClosed().subscribe(result => {});
  }

  
  openApproveDialog() {
    this.dialogService
      .openConfirm({
        message: "Are you sure you want to approve the request ?",
        title: "Confirmation",
        cancelButton: "Cancel",
        acceptButton: "Save"
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.posRequestDto.posRequest = this.posRequestService.posRequest;
          this.posRequestService.onApproveRequest(this.posRequestDto).subscribe(
            () => {},
            (errorResponse: HttpErrorResponse) => {
              this.onSaveError(errorResponse);
          },
            ()=>{this.router.navigate(["/pos-requests"]);
          }
          );
        }
      });
  }
  onCancel() {
    this.router.navigate(["/pos-requests"]);
  }

  checkStatus() {
  
    let statusId = this.posRequestService.posRequest.posRequestVendor.status.id;
    if (statusId == PosRequestStatusId.Pending ) {
      this.isPending = true;
    }
  }

  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): PosRequestDto {
    return this.posRequestDto;
  }

  set model(posRequestDto: PosRequestDto) {
    this.posRequestDto = posRequestDto;
  }

  initModel(): PosRequest {
    return new PosRequest();
  }

  ngFormInstance(): NgForm {
    return this.posForm;
  }

  getSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.selectedPrivilegeProfiles;
  }
  getNotSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.notSelectedPrivilegeProfiles;
  }
  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }
}
