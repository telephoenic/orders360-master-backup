import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosRequestVendorFormComponent } from './pos-request-vendor-form.component';

describe('PosRequestVendorFormComponent', () => {
  let component: PosRequestVendorFormComponent;
  let fixture: ComponentFixture<PosRequestVendorFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosRequestVendorFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosRequestVendorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
