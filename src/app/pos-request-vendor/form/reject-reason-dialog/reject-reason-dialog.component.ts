import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from "@angular/material";
import { OrderRejectedReason } from "../../../reasons/reason.model";
import { OrderRejectedReasonService } from "../../../reasons/services/reason-service";
import { BaseFormComponent } from "../../../common/form/base-form-component";
import { User } from "../../../auth/user.model";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { OrderRejectedReasonDialog } from "../../../reasons/views/reason-view.component";
import { TdDialogService } from "@covalent/core";
import { VendorService } from "../../../vendor/Services/vendor.service";
import { VendorAdminService } from "../../../vendor-admin/service/vendor-admin.service";
import { AuthService } from "../../../auth/service/auth.service";
import { environment } from "../../../../environments/environment";
import { PosRequestService } from "../../service/pos-request.service";
import { HttpErrorResponse } from "@angular/common/http";
import { PosRequestDto } from "../../model/pos-request-dto";
import { OrderRejectedReasonEnum } from "../../../shippment-order/enum/order-rejected-reason-enum";
@Component({
  selector: "app-reject-reason-dialog",
  templateUrl: "./reject-reason-dialog.component.html",
  styleUrls: ["./reject-reason-dialog.component.css"]
})
export class RejectReasonDialogComponent extends BaseFormComponent
  implements OnInit {
  @ViewChild("reasonDialogForm") reasonDialogForm: NgForm;

  posRequestDto = new PosRequestDto();
  private _isEdit: boolean = false;
  private _loggedUser: User;
  private rejectedReason: OrderRejectedReason;
  rejectedReasonList: OrderRejectedReason[] = [];
  _vendorId: number;

  constructor(
    protected posRequestService: PosRequestService,
    protected orderRejectedReasonService: OrderRejectedReasonService,
    protected route: ActivatedRoute,
    public dialogReason: MatDialogRef<OrderRejectedReasonDialog>,
    @Inject(MAT_DIALOG_DATA) public data: OrderRejectedReason,
    protected router: Router,
    protected snackBar: MatSnackBar,
    protected dialogService: TdDialogService,
    protected vendorService: VendorService,
    protected vendorAdminService: VendorAdminService,
    protected authService: AuthService
  ) {
    super(orderRejectedReasonService, route, router, snackBar);
  }

  ngOnInit() {
    this.getVendorIdByLoggedVenodrAdmin();
    this.posRequestDto.posRequest = this.posRequestService.posRequest;
  }

  getVendorIdByLoggedVenodrAdmin() {
    this._loggedUser = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService
      .getVendorByVendorAdminId(this._loggedUser.id)
      .subscribe(
        (vendorId: number) => {
          console.log(vendorId);
          this._vendorId = vendorId;
        },
        () => {},
        () => {
          this.findRejctedReasonByParentId();
        }
      );
  }

  findRejctedReasonByParentId() {
    this.orderRejectedReasonService
      .findRejctedReasonByParentIdByCode(
        OrderRejectedReasonEnum.SignupPosRequest,
        this._vendorId
      )
      .subscribe(data => {
        this.rejectedReasonList = data;
      });
  }
  onSave() {
    this.posRequestService.onRejectRequest(this.posRequestDto).subscribe(
      () => {},
      (errorResponse: HttpErrorResponse) => {
        this.onSaveError(errorResponse);
        this.dialogReason.close();
      },
      () => {
        this.onSaveCompleted();
      }
    );
  }

  onSaveCompleted() {
    this.dialogReason.close();
    this.router.navigate(["/pos-requests"]);
    this.snackBar.open("Pos Request Rejected successfully.", "Close", {
      duration: 3000
    });
  }

  onNoClick(): void {
    this.dialogReason.close();
  }

  get model(): OrderRejectedReason {
    return this.rejectedReason;
  }
  set model(orderRejectedReason: OrderRejectedReason) {
    this.rejectedReason = orderRejectedReason;
  }
  initModel(): OrderRejectedReason {
    return new OrderRejectedReason();
  }
  ngFormInstance(): NgForm {
    return this.reasonDialogForm;
  }

  get isEdit(): boolean {
    return this._isEdit;
  }
  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
}
