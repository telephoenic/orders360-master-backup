
import { BaseModel } from "../../common/model/base-model";
import { PosRequest } from "./pos-request";
import { PosRequestVendor } from "./pos-request-vendor";
import { PosRequestRejectedReason } from "./pos-request-rejected-reason";



export class PosRequestDto extends BaseModel{
    public id:number;
    public inactive:boolean;
    public name:string;
    public posRequest:PosRequest
    public posRequestVendor:PosRequestVendor;
    public posGroup:number;
    public posCode:number;
    posRequestRejectedReason: PosRequestRejectedReason

    constructor ( ){
        super();
        this.posRequest=new PosRequest();
        this.posRequestVendor=new PosRequestVendor();
        this.posRequestRejectedReason= new PosRequestRejectedReason();
    }

    
}