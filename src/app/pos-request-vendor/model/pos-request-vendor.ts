
import { Vendor } from '../../vendor/vendor.model'
import { PosRequestVendorComponent } from "../pos-request-vendor.component";
import { Audit } from "../../audit/audit.model";
import { PosRequestStatus } from './pos-request-status';
import { PosRequest } from './pos-request';


export class PosRequestVendor{
    public audit :Audit
    public id : number;
    public status : PosRequestStatus;
    public posRequest: PosRequest;
    public vendor :Vendor;
    

    constructor(){
       this.vendor = new Vendor();
       this.status= new PosRequestStatus();
      
    }
}