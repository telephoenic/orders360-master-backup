
import { OrderRejectedReason } from "../../reasons/reason.model";
import { PosRequestVendor } from "./pos-request-vendor";




export class PosRequestRejectedReason {
    posRequestVendor :PosRequestVendor;
    rejectedReason : OrderRejectedReason 
    description : string
    constructor(){
        this.rejectedReason = new OrderRejectedReason();
        this.posRequestVendor= new PosRequestVendor();
    }
    
}