
import { BaseModel } from '../../common/model/base-model';
import { UserPrivilegeProfile } from '../../user-privilege/user-privilege-profiles';
import { Audit } from '../../audit/audit.model';
import { PosRequestVendor } from './pos-request-vendor';
import { PosRequestRejectedReason } from './pos-request-rejected-reason';
import { Region } from '../../location/region/Region.model';


export class PosRequest extends BaseModel{
    
    public id : number;
    public inactive:boolean;
    public name : string;
    public mobileNumber : string;
    public ownerName :string;
    public ownerMobileNumber :string;
    public landNumber :string;
    public fax: string;
    public email : string;
    public region: Region;
    public trn :string;
    public audit :Audit
    public posRequestVendor :PosRequestVendor;
    public latitude:number;
    public longitude:number;
    public reason :PosRequestRejectedReason;
   
    constructor (){
        super();
        this.posRequestVendor= new PosRequestVendor();
        this.reason= new PosRequestRejectedReason();
       
    }
}