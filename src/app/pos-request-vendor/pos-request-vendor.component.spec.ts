import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosRequestVendorComponent } from './pos-request-vendor.component';

describe('PosRequestVendorComponent', () => {
  let component: PosRequestVendorComponent;
  let fixture: ComponentFixture<PosRequestVendorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosRequestVendorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosRequestVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
