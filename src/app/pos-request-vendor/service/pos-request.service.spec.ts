import { TestBed, inject } from '@angular/core/testing';

import { PosRequestService } from './pos-request.service';

describe('PosRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PosRequestService]
    });
  });

  it('should be created', inject([PosRequestService], (service: PosRequestService) => {
    expect(service).toBeTruthy();
  }));
});
