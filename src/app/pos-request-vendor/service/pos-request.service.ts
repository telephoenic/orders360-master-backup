import { Injectable } from "@angular/core";
import { BaseService } from "../../common/service/base.service";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../../auth/service/auth.service";
import { DatePipe } from "@angular/common";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { BaseModel } from "../../common/model/base-model";
import { OrderRejectedReason } from "../../reasons/reason.model";
import { PosRequestDto } from "../model/pos-request-dto";
import { PosRequest } from "../model/pos-request";

@Injectable({
  providedIn: "root"
})
export class PosRequestService extends BaseService {
  rejectedReasonList: OrderRejectedReason[] = [];
  selectedScreen: boolean = false;
  posRequestDto: PosRequestDto = new PosRequestDto();
  posRequest : PosRequest= new PosRequest();
  navigateToAfterCompletion: string;

  constructor(
    http: HttpClient,
    protected authService: AuthService,
    private datePipe: DatePipe
  ) {
    super(http, "posRequest", authService);
  }

  getPosRequestByVendorId(
    pageNum: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    startDate,
    endDate,
    requestStatusId: number
  ): Observable<any> {
    if (startDate == undefined || startDate == null) {
      startDate = this.datePipe.transform(
        new Date("1970-04-01").getDate(),
        "yyyy-MM-dd"
      );
    }
    if (endDate == undefined || endDate == null) {
      endDate = this.datePipe.transform(new Date(), "yyyy-MM-dd");
    }
    let parameterList: { paramName: string; paramValue: any }[] = [];

    if (requestStatusId != undefined) {
      parameterList.push({
        paramName: "statusId",
        paramValue: requestStatusId
      });
    }

    parameterList.push({ paramName: "startDate", paramValue: startDate });
    parameterList.push({ paramName: "endDate", paramValue: endDate });
    return this.http.get(
      environment.baseURL +
        "pos-request/pos-requests-for-vendor/" +
        pageNum +
        "/" +
        pageSize +
        "/" +
        vendorId,
      {
        params: this.prepareSearchParameters(
          searchTerm,
          sortBy,
          sortOrder,
          parameterList
        ),
        headers: this.getBasicHeaders()
      }
    );
  }

 
  onApproveRequest(requestVendor: PosRequestDto): Observable<any> {
   
    return this.http.post(
      environment.baseURL + "pos-request/approve",
      JSON.stringify(requestVendor),
      { headers: this.getBasicHeaders() }
    );
  }

  onRejectRequest(requestVendor: PosRequestDto): Observable<any> {
  
    return this.http.post(
      environment.baseURL + "pos-request/reject",
      JSON.stringify(requestVendor),
      { headers: this.getBasicHeaders() }
    );
  }

  get model() {
    return this.posRequest;
  }
  set model(model: BaseModel) {
    this.posRequest = <PosRequest>model;
  }
}
