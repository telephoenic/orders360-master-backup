import { TestBed, inject } from '@angular/core/testing';

import { PosRequestDtoService } from './pos-request-dto.service';

describe('PosRequestDtoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PosRequestDtoService]
    });
  });

  it('should be created', inject([PosRequestDtoService], (service: PosRequestDtoService) => {
    expect(service).toBeTruthy();
  }));
});
