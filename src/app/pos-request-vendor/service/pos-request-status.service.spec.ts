import { TestBed, inject } from '@angular/core/testing';

import { PosRequestStatusService } from './pos-request-status.service';

describe('PosRequestStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PosRequestStatusService]
    });
  });

  it('should be created', inject([PosRequestStatusService], (service: PosRequestStatusService) => {
    expect(service).toBeTruthy();
  }));
});
