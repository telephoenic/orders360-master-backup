import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { AuthService } from '../../auth/service/auth.service';
import { BaseModel } from '../../common/model/base-model';

import { Observable } from 'rxjs';
import { environment } from '../.././../environments/environment';
import { PosRequestStatus } from '../model/pos-request-status';

@Injectable({
  providedIn: 'root'
})
export class PosRequestStatusService extends BaseService {
  model: BaseModel;
  private _requestStatus: PosRequestStatus = new PosRequestStatus();
  

  constructor(http: HttpClient,
      protected authService: AuthService,
      protected snackBar: MatSnackBar) {
      super(http, "pos_request_status", authService)
  }


  public getPosReuestStatus(): Observable<any>{
      return this.http.get(environment.baseURL + "pos-request-status/all-status");
  }

  public getPosRequestStatusByCode(code :string): Observable<any>{
    return this.http.get(environment.baseURL + "pos-request-status/get-status-by-cod/"+code);
}

}