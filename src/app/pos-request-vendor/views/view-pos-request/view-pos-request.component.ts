import { Component, OnInit, ViewChild } from "@angular/core";
import { BaseViewComponent } from "../../../common/view/base-view-component";
import { User } from "././../.././../auth/user.model";
import {
  TdDataTableSortingOrder,
  TdPagingBarComponent,
  TdDataTableComponent,
  ITdDataTableColumn,
  TdDialogService,
  IPageChangeEvent
} from "@covalent/core";
import { FcmService } from "../../../fcm/services/fcm.service";
import { VendorAdminService } from "../../../vendor-admin/service/vendor-admin.service";
import { ProductLoyaltyService } from "../../../loyalty/service/product-loyalty.service";
import { Router } from "@angular/router";
import { AuthService } from "../../../auth/service/auth.service";
import { MatSnackBar } from "@angular/material";
import { DatePipe } from "@angular/common";
import { NgxPermissionsService } from "ngx-permissions";
import { SalesAgentService } from "../../../sales-agent/service/sales-agent.service";
import { RegionService } from "../../../location/region/service/region.service";

import { PosRequestStatusService } from "../../service/pos-request-status.service";

import { PosRequestService } from "../../service/pos-request.service";
import { PosRequest } from "../../model/pos-request";
import { PosRequestStatus } from "../../model/pos-request-status";


@Component({
  selector: "app-view-pos-request",
  templateUrl: "./view-pos-request.component.html",
  styleUrls: ["./view-pos-request.component.css"]
})
export class ViewPosRequestComponent extends BaseViewComponent
  implements OnInit {
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private user: User;
  private _allPosRequest: PosRequest[] = [];
  private _errors: any = "";
  private _selectedRows: PosRequest[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = "";
  private _sortOrder: TdDataTableSortingOrder =
    TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = "";
  protected loggedUser: User;
  private startDate: any;
  private endDate: any;
  private _pageNum: number = 1;
  private _filterFlag: boolean = false;
  private vendorId: number;
  private _listOfrequestStatus: PosRequestStatus[] = [];
  private requestStatus: PosRequestStatus = new PosRequestStatus();
  @ViewChild("pagingBar") pagingBar: TdPagingBarComponent;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;

  columnsConfig: ITdDataTableColumn[] = [
    { name: "name", label: "Shop Name", tooltip: "Shop Name", width: 180 },
    {
      name: "mobileNumber",
      label: "Mobile Number",
      tooltip: "Mobile Number",
      width: 180
    },
    {
      name: "audit.createdDate",
      label: "Date",
      format: v => this.datePipe.transform(v, "medium"),
      tooltip: "Date",
      width: 240
    },
    { name: "region.city.name", label: "City", tooltip: "City" },
    { name: "region.name", label: "Area", tooltip: "Area", width: 180 },
    {
      name: "posRequestVendor.status.name",
      label: "Status",
      tooltip: "Status",
      width: 180
    },
    { name: "id", label: "View", tooltip: "View", sortable: false, width: 120 }
  ];

  constructor(
    protected posRequestService: PosRequestService,
    protected fcmService: FcmService,
    protected vendorAdminService: VendorAdminService,
    protected productLoyaltyService: ProductLoyaltyService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected authService: AuthService,
    protected snackBar: MatSnackBar,
    private datePipe: DatePipe,
    protected permissionsService: NgxPermissionsService,
    protected salesAgentService: SalesAgentService,
    protected requestStatusService: PosRequestStatusService,
    protected regionService: RegionService
  ) {
    super(posRequestService, router, dialogService, snackBar);
  }

  ngOnInit() {
    super.ngOnInit();
    this.user = this.authService.getCurrentLoggedInUser();
    this.loadPosRequestStatus();
  }

  filterByVendor(
    pageNumber: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    vendorId: number
  ) {
  
    this.vendorId = vendorId;
    this.posRequestService
      .getPosRequestByVendorId(
        pageNumber,
        pageSize,
        vendorId,
        searchTerm,
        sortBy,
        sortOrder,
        this.startDate,
        this.endDate,
        this.requestStatus.id
      )
      .subscribe(data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"];
      });
  }

  public startDateChange(event: any) {
    this._filterFlag = true;
    this.startDate = this.datePipe.transform(event.value, "yyyy-MM-dd");
    if (this.startDate == null) {
      this.getPagingBar().navigateToPage(1);
    }
  }

  public endDateChange(event: any) {
    this.endDate = this.datePipe.transform(event.value, "yyyy-MM-dd");
  }

  public onStatusChange(event) {
    this.requestStatus.code = event.value;
    this.posRequestService
      .getPosRequestByVendorId(
        this.pageNum - 1,
        this.pageSize,
        this.vendorId,
        this.searchTerm,
        this.sortBy,
        this.sortOrder,
        this.startDate,
        this.endDate,
        this.requestStatus.id
      )
      .subscribe(data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"];
      });
    this.getPagingBar().navigateToPage(1);
    // this.filterByDateHandler();
  }

  public loadPosRequestStatus() {
    this.requestStatusService.getPosReuestStatus().subscribe(data => {
      this._listOfrequestStatus = data;
    });
  }

  public paging(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.pageSize = pagingEvent.pageSize;
    this.pageNum = pagingEvent.page;
    // this.filterByDateHandler();
  }

  public determinePageMethod(pagingEvent: IPageChangeEvent) {
    if (this.startDate != null) {
      this.paging(pagingEvent);
    } else {
      this.page(pagingEvent);
    }
  }

  handleFilterEvent() {
    if (this.startDate > this.endDate) {
      this.snackBar.open("End date must be after than start date.", "close", {
        duration: 3000
      });
      return;
    }
    if (this._filterFlag) {
      this.getPagingBar().navigateToPage(1);
    }

    this.filterByDateHandler();
  }

  filterByDateHandler() {
    this.pageSize = this.getPagingBar().pageSize;
    this.posRequestService
      .getPosRequestByVendorId(
        this.pageNum - 1,
        this.pageSize,
        this.vendorId,
        this.searchTerm,
        this.sortBy,
        this.sortOrder,
        this.startDate,
        this.endDate,
        this.requestStatus.id
      )
      .subscribe(data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"];
      });
  }

  onNoDataFound() {
    this.snackBar.open("No data found.", "close", { duration: 3000 });
  }

  onNoSelectedRows() {
    this.snackBar.open("No selected rows.", "close", { duration: 3000 });
  }

  getObj(row: any) {
    this.posRequestService.posRequest = row;
    this.router.navigate(["/pos-requests/form"]);
  }

  get allModels(): PosRequest[] {
    return this._allPosRequest;
  }
  set allModels(value: PosRequest[]) {
    this._allPosRequest = value;
  }

  public get pageNum() {
    return this._pageNum;
  }

  public get filterFlag() {
    return this._filterFlag;
  }

  public set filterFlag(value: boolean) {
    this._filterFlag = value;
  }

  public set pageNum(value: number) {
    this._pageNum = value;
  }
  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get errors(): any {
    return this._errors;
  }

  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): PosRequest[] {
    return this._selectedRows;
  }

  public set selectedRows(value: PosRequest[]) {
    this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }

  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }

  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get searchTerm(): string {
    return this._searchTerm;
  }

  public set searchTerm(value: string) {
    this._searchTerm = value;
  }
}
