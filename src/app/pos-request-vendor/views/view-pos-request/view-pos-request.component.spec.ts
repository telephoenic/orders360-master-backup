import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPosRequestComponent } from './view-pos-request.component';

describe('ViewPosRequestComponent', () => {
  let component: ViewPosRequestComponent;
  let fixture: ComponentFixture<ViewPosRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPosRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPosRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
