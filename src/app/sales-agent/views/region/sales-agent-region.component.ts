import { Component, OnInit, Output, ViewChild, Input, EventEmitter } from '@angular/core';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { environment } from '../../../../environments/environment';
import { TdDataTableSortingOrder, TdPagingBarComponent, 
  TdDataTableComponent, ITdDataTableColumn, TdDialogService } from '@covalent/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Region } from '../../../location/region/Region.model';
import { RegionService } from '../../../location/region/service/region.service';

@Component({
  selector: 'sales-agent-region',
  templateUrl: './sales-agent-region.component.html',
  styleUrls: ['./sales-agent-region.component.css']
})
export class SalesAgentRegionComponent extends BaseViewComponent implements OnInit {

  	private _currentPage: number = 1;
	private _fromRow: number = 1;	
	
	public salesAgentRegion:Region[] = [];
  
  	private _errors: any = '';
  	private _selectedRows: Region[] = [];
  	private _pageSize: number = environment.initialViewRowSize;
  	private _filteredTotal: number;
  	private _sortBy: string = '';
  	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  	private _searchTerm: string ='';

	@Output("rowSelected")
	selectedEventEmitter = new EventEmitter();

	@ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
	@ViewChild('dataTable') dataTable : TdDataTableComponent;
  
	@Input("selectedSide")
	selectedSide:boolean;

	@Input("otherSalesAgentRegionComponent") 
	otherModuleRolesComponent: SalesAgentRegionComponent;

	columnsConfig: ITdDataTableColumn[] = [
		{ name: 'code', label: 'Code', sortable: true, width: 60 },
		{ name: 'name', label: 'Area Name', sortable: true, width: 200 },
		{ name: 'city.name', label: 'City Name', sortable: true, width: 220},
	];

  	constructor(protected regionService:RegionService,

			protected router:Router,
			protected dialogService : TdDialogService,
			protected snackBar:MatSnackBar) { 

      	super(	regionService,
				router,
				dialogService,
				snackBar);
    }

	ngOnInit() {}

	compareWith(row: Region, model: Region): boolean { 
		if(row.id == null) {
		  return row.id == model.id;
		} 
		return row.id === model.id;
	}

	onMoveRows() {
		this.selectedEventEmitter.emit({	rows: this._selectedRows, 
												movedAsAssigned: !this.selectedSide});
		this.selectedRows = [];
	}

	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}

	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

  	public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): Region[]  {
		return this.salesAgentRegion;
	}

	public set allModels(value: Region[] ) {
		this.salesAgentRegion = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): Region[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: Region[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
  	}

}
