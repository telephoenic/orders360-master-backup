import {BaseModel} from '../../common/model/base-model';
import { Injectable, ViewChild } from '@angular/core';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { SalesAgent } from "../sales-agent.model";
import { environment } from "../../../environments/environment";
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SalesAgentService extends BaseService{
  salesAgent:SalesAgent;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient, protected authService:AuthService) {
    super(http, "sales_agent", authService)
  }

  getAllSalesAgentsByVendorId(pageNumber:number, 
                                   pageSize:number, 
                                   vendorId:number,
                                   searchTerm:string,
                                   sortBy:string, 
                                   sortOrder:string) : Observable<any> {
    return this.http.get( environment.baseURL + 
                          "sales_agent/salesagent_for_vendor/" + 
                          pageNumber + "/" + 
                          pageSize + "/" + 
                          vendorId, { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder), 
                          headers: this.getBasicHeaders()});
}

getSalesAgentsByVendorId(vendorId:number): Observable<any> {

  return this.http.get( environment.baseURL +
                        "sales_agent/salesagent_for_vendor/"
                        + vendorId);
}

deleteSalesAgentUserAndRegionOnRegionsWhereIdIn(regionIds:number[]): Observable<any>{
  return this.http.post(environment.baseURL + "sales_agent/deassign-regions",regionIds);
}

  get model(){
    return this.salesAgent;
  }

  set model(model:BaseModel) {
    this.salesAgent = <SalesAgent>model;
  }
}