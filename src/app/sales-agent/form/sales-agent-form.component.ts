import { Component, OnInit, ViewChild } from "@angular/core";
import {
  FormControl,
  Validators,
  NgControlStatus,
  NgForm
} from "@angular/forms";
import {
  MatFormFieldControl,
  MatButton,
  MatList,
  MatSuffix,
  MatInput,
  MatSnackBar
} from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { BaseFormComponent } from "../../common/form/base-form-component";
import { VendorService } from "../../vendor/Services/vendor.service";
import { environment } from "../../../environments/environment";
import { Vendor } from "../../vendor/vendor.model";
import { UserPrivilegeProfileComponent } from "../../user-privilege/views/user-privilege-profile.component";
import { BaseViewComponent } from "../../common/view/base-view-component";
import { BaseUserManagementForm } from "../../user-management/base-user-management-form";
import { CityService } from "../../location/city/service/city.service";
import { City } from "../../location/city/City.model";
import { SalesAgentRegionComponent } from "../views/region/sales-agent-region.component";
import { Region } from "../../location/region/Region.model";
import { RegionService } from "../../location/region/service/region.service";
import { SalesAgent } from "../sales-agent.model";
import { SalesAgentService } from "../service/sales-agent.service";
import { User } from "../../auth/user.model";
import { AuthService } from "../../auth/service/auth.service";
import { UserType } from "../../lookups/user-type.model";
import { UserTypeService } from "../../lookups/service/user-type.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { strictEqual } from "assert";
import { UserPrivilegeProfileAware } from "../../user-management/user-privilege-profile-aware";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: "sales-agent-form",
  templateUrl: "./sales-agent-form.component.html",
  styleUrls: ["./sales-agent-form.component.css"]
})
export class SalesAgentFormComponent extends BaseUserManagementForm {
  @ViewChild("salesAgentForm")
  salesAgentForm: NgForm;

  @ViewChild("allNotSelectedSalesAgentRegion")
  allNotSelectedSalesAgentRegion: SalesAgentRegionComponent;

  @ViewChild("allSelectedSalesAgentRegion")
  allSelectedSalesAgentRegion: SalesAgentRegionComponent;

  @ViewChild("notSelectedPrivilegeProfiles")
  notSelectedPrivilegeProfiles: UserPrivilegeProfileComponent;

  @ViewChild("selectedPrivilegeProfiles")
  selectedPrivilegeProfiles: UserPrivilegeProfileComponent;

  salesAgent: SalesAgent;
  _isEdit: boolean = false;
  _isUserLoggedInVendor: boolean = false;
  loggedInVendor: Vendor;
  _cityId: number;
  listOfVendors: Vendor[];
  listOfCities: City[];
  countryId: number = 1;
  regex;
  isSaveFormError:boolean = false;
  constructor(
    protected salesAgentService: SalesAgentService,
    protected authService: AuthService,
    private vendorService: VendorService,
    private cityService: CityService,
    private regionService: RegionService,
    private userTypeService: UserTypeService,
    protected route: ActivatedRoute,
    protected vendorAdminService: VendorAdminService,
    protected router: Router,
    protected snackBar: MatSnackBar
  ) {
    super(salesAgentService, route, router, snackBar);
  }

  ngOnInit() {
    this.selectedSecreen = environment.ENUM_CODE_USER_TYPE.SalesAgentUser;
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    if (this.route.snapshot.queryParams["edit"] == "1") {
      this.isEdit = true;
      this.salesAgent = this.salesAgentService.salesAgent;
    } else {
      this.salesAgent = new SalesAgent();
      //this.loadUserType();
    }

    this.loadVendorOptions();
    this.loadCityOptions();
    this.loadRegions();
  }

  // loadUserType() {
  //   this.userTypeService.findByExample(this.getLeafUserTypeInstance()).subscribe(
  //     data => this.salesAgent.type = data["content"]
  //   );
  // }

  // getLeafUserTypeInstance() {
  //   let userType = new UserType();
  //   userType.code = environment.ENUM_ID_USER_TYPE_CODE.SalesAgent;
  //   return userType;
  // }

  loadVendorOptions() {
    let loggedInUser: User = this.authService.getCurrentLoggedInUser();

    if (loggedInUser != undefined) {
      if (loggedInUser.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
        this.vendorService
          .filterNotPageable(
            "",
            "",
            "",
            "/vendor_by_vendor_admin_id/" + loggedInUser.id
          )
          .subscribe((data: Vendor) => {
            data => (this.loggedInVendor = data);
            if (data != null) {
              this.loggedInVendor = new Vendor(data["id"], data["name"]);
              this.salesAgent.vendor = data;
              this.regex = new RegExp("^("+this.salesAgent.vendor.country.mobileCode+")[0-9]{8,12}$")
              console.log(this.regex);
            }
            this.onCompleteLoadingVendor(this.loggedInVendor);
          });
      } else if (
        loggedInUser.type.id == environment.ENUM_ID_USER_TYPE.TelephoenicAdmin
      ) {
        this.vendorService
          .filterNotPageable("", "", "", "/vendor_active_list")
          .subscribe((data: Vendor[]) => {
            this.listOfVendors = data;
          });
      }
    }
  }

  onCompleteLoadingVendor(loggedInVendor: Vendor) {
    if (loggedInVendor == undefined) {
      this._isUserLoggedInVendor = false;
      this.vendorService
        .filter(0, environment.maxListFieldOptions, "", "", "")
        .subscribe(data => (this.listOfVendors = data["content"]));
    } else {
      this._isUserLoggedInVendor = true;
      this.salesAgent.vendor = loggedInVendor;
    }
  }

  loadCityOptions() {
    this.vendorAdminService
      .getVendorByVendorAdminId(this.loggedUser.id)
      .subscribe((vendorId: number) => {
        this.vendorService
          .getCountryIdByVendorId(vendorId)
          .subscribe((countryId: number) => {
            this.countryId = countryId;
            this.cityService
              .findAllCiityByCountryId(this.countryId)
              .subscribe(data => {
                this.listOfCities = data;
                this.loadRegions();
              });
          });
      });
  }

  loadRegions() {
    this.loadSelectedRegions();
    this.loadNotSelectedRegions();
  }

  loadSelectedRegions() {
    if (this.isEdit) {
      this.regionService
        .filterNotPageable(
          "",
          "",
          "",
          "/region_for_sales_agent/" + this.salesAgent.id
        )
        .subscribe((data: Region[]) => {
          this.allSelectedSalesAgentRegion.allModels = data;
        });
    }
  }

  loadNotSelectedRegions() {
    let parameterList: { paramName: string; paramValue: string }[] = [];
    let salesAgentId = this.salesAgent.id;
    let cityId = this.cityId;

    if (salesAgentId != undefined) {
      parameterList.push({
        paramName: "sales_agent_id",
        paramValue: String(salesAgentId)
      });
    }

    if (cityId != undefined) {
      parameterList.push({ paramName: "city_id", paramValue: String(cityId) });
    }

    parameterList.push({
      paramName: "country_id",
      paramValue: String(this.countryId)
    });
    this.regionService
      .filterNotPageable(
        "",
        "",
        "",
        "/region_not_for_sales_agent",
        parameterList.length > 0 ? parameterList : undefined
      )

      .subscribe((data: Region[]) => {
        this.allNotSelectedSalesAgentRegion.allModels = data;
      },(error)=>{console.log(error)},
      ()=>{
        this.allNotSelectedSalesAgentRegion.allModels =
         this.allNotSelectedSalesAgentRegion.allModels
         .filter(obj =>!this.allSelectedSalesAgentRegion.allModels.find(temp=>obj.id == temp.id))
      });
  }

  onSelectingRegion(event) {
    if (event.movedAsAssigned) {
      this.pushTo(this.allSelectedSalesAgentRegion, event);
      this.removeFrom(this.allNotSelectedSalesAgentRegion, event);
    } else {
      let salesAgentregionIds : number[] = [];
      if(this._isEdit){
        salesAgentregionIds.push(this.salesAgent.id);
        event.rows.forEach(region =>{
          salesAgentregionIds.push(region.id);
        });
        this.salesAgentService.deleteSalesAgentUserAndRegionOnRegionsWhereIdIn(salesAgentregionIds)
        .subscribe(data=>{
          console.log(data);
        });
      }
      this.pushTo(this.allNotSelectedSalesAgentRegion, event);
      this.removeFrom(this.allSelectedSalesAgentRegion, event);
    }

    this.allSelectedSalesAgentRegion.dataTable.refresh();
    this.allNotSelectedSalesAgentRegion.dataTable.refresh();
  }

  onCityChange(event) {
    this.loadNotSelectedRegions();
    this.allSelectedSalesAgentRegion.dataTable.refresh();
  }

  onBeforeEdit(salesAgent: SalesAgent) {
    super.onBeforeEdit(salesAgent);
    this.prepareModelProperties(salesAgent);
    this.fillSelectedPrivilegeProfiles(salesAgent);
  }

  onBeforeSave(salesAgent: SalesAgent) {
    super.onBeforeSave(salesAgent);
    this.prepareModelProperties(salesAgent);
    this.fillSelectedPrivilegeProfiles(salesAgent);
  }

  onBeforeSaveAndNew(salesAgent: SalesAgent) {
    super.onBeforeSaveAndNew(salesAgent);
    this.prepareModelProperties(salesAgent);

    if (this.isUserLoggedInVendor) {
      salesAgent.vendor = this.loggedInVendor;
    }
  }

  onEditError(errorResponse: HttpErrorResponse) {
    super.onEditError(errorResponse);
    this.isSaveFormError = true;
    this.loadVendorOptions();
  }
  onSaveError(errorResponse: HttpErrorResponse) {
    super.onSaveError(errorResponse);
    this.isSaveFormError = true;
    this.loadVendorOptions();
  }

  onSaveAndNewError(errorResponse: HttpErrorResponse) {
    super.onSaveAndNewError(errorResponse);
    this.isSaveFormError = true;
    this.loadVendorOptions();
  }


  onSaveAndNewCompleted() {
    debugger;
    this.showSpinnerOnSaveAndNew = false;
    this.allSelectedSalesAgentRegion.allModels = [];
    this.allSelectedSalesAgentRegion.dataTable.refresh();

    this.loadNotSelectedRegions();
    this.allNotSelectedSalesAgentRegion.dataTable.refresh();

    this.getNotSelectedPrivilegeProfiles().filterPrivilegeProfileForUser(
      this.loggedUser.id,
      null,
      this.selectedSecreen
    );

    this.selectedPrivilegeProfiles.allModels = [];
    this.selectedPrivilegeProfiles.dataTable.refresh();
    this.notSelectedPrivilegeProfiles.dataTable.refresh();

    if (this.isUserLoggedInVendor) {
      this.salesAgent.vendor = this.loggedInVendor;
    }
  }

  fillSelectedPrivilegeProfiles(userModel: UserPrivilegeProfileAware) {
    userModel.privilegeProfiles = this.getSelectedPrivilegeProfiles().allModels;
    if (userModel.privilegeProfiles.length === 0) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open(
        "Sales Agent must be have at least one profile privilege.",
        "Close",
        { duration: 3000 }
      );
      throw new Error(
        "Sales Agent must be have at least one profile privilege"
      );
    }
  }

  prepareModelProperties(salesAgent: SalesAgent) {
    salesAgent.regions = this.allSelectedSalesAgentRegion.allModels;

    if (this.allSelectedSalesAgentRegion.allModels.length == 0) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("At least one Area should be selected.", "close", {
        duration: 3000
      });

      throw Error("At least one Area should be selected.");
    }
  }

  onCahngeTab() {
    this.loadRegions();
  }

  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get isUserLoggedInVendor(): boolean {
    return this._isUserLoggedInVendor;
  }

  set isUserLoggedInVendor(isUserLoggedInVendor: boolean) {
    this._isUserLoggedInVendor = isUserLoggedInVendor;
  }

  get model(): SalesAgent {
    return this.salesAgent;
  }

  set model(salesAgent: SalesAgent) {
    this.salesAgent = salesAgent;
  }

  get cityId(): number {
    return this._cityId;
  }

  set cityId(cityId: number) {
    this._cityId = cityId;
  }

  initModel(): SalesAgent {
    return new SalesAgent();
  }

  ngFormInstance(): NgForm {
    return this.salesAgentForm;
  }

  getSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.selectedPrivilegeProfiles;
  }
  getNotSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.notSelectedPrivilegeProfiles;
  }
}
