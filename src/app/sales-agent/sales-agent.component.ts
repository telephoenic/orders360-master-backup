import { Component, OnInit } from '@angular/core';
import { MatSidenavContainer, MatSidenav, MatButton, MatCard  } from "@angular/material";
import { TdDialogService } from "@covalent/core";
import { SalesAgentService } from './service/sales-agent.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sales-agent',
  templateUrl: './sales-agent.component.html',
  styleUrls: ['./sales-agent.component.css']
})
export class SalesAgentComponent implements OnInit {

  constructor(private dialogService : TdDialogService,
              private salesAgentService: SalesAgentService,
              private router:Router) { }

  ngOnInit() { }
  
}