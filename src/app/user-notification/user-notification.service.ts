import { BaseService } from "../common/service/base.service";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../auth/service/auth.service";
import { BaseModel } from "../common/model/base-model";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { DatePipe } from "@angular/common";
import { UserNotification } from "../notification/user-notification.model";

@Injectable()
export class UserNotificationService extends BaseService {
  userNotification: UserNotification;
  navigateToAfterCompletion: string;

  constructor(
    protected http: HttpClient,
    protected authService: AuthService,
    private datePipe: DatePipe
  ) {
    super(http, "user-notification", authService);
  }

  public updateUserNotification(userNotificationId: number) {
    return this.http.post(
      environment.baseURL + "user-notification/update-user-notification",
      userNotificationId
    );
  }
  public getUserNotificationPrivilege(userId: number) {
    return this.http.get(
      environment.baseURL + "user/get-user-notification-privilege/" + userId
    );
  }

  public getShippmentOrderIdByUserNotificationId(shippmentOrderId: number) {
    return this.http.get(
      environment.baseURL +
        "user-notification/get-shippment-id-by-user-notification-id/" +
        shippmentOrderId
    );
  }
  get model(): BaseModel {
    return this.userNotification;
  }
  set model(model: BaseModel) {
    this.userNotification = <UserNotification>model;
  }
}
