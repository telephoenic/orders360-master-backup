import { Component, OnInit, ViewChild } from "@angular/core";
import { NotificationService } from "../notification/notification-service.service";
import { AuthService } from "../auth/service/auth.service";
import { VendorAdminService } from "../vendor-admin/service/vendor-admin.service";
import { Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { IPageChangeEvent, TdPagingBarComponent } from "@covalent/core";
import { MatOption, MatSnackBar } from "@angular/material";
import { ModuleService } from "../structure/module/module.service";
import { FormControl } from "@angular/forms";
import { BaseService } from "../common/service/base.service";
import { UserNotification } from "../notification/user-notification.model";
import { UserNotificationService } from "./user-notification.service";
import { OrderService } from "../orders/service/order.service";
import { Order } from "../orders/order.model";
import { environment } from "../../environments/environment";
import { AppComponent } from "../app.component";
import { WarehouseAdminService } from "../warehouse-admin/service/warehouse-admin.service";
import { ShippmentOrderService } from "../shippment-order/service/shippment-order.service";

@Component({
  selector: "app-user-notification",
  templateUrl: "./user-notification-view.component.html",
  styleUrls: ["./user-notification-view.component.css"]
})
export class UserNotifiactionComponent implements OnInit {
  @ViewChild("allModulesSelected") private allModulesSelected: MatOption;
  @ViewChild("pagingBar") pagingBar: TdPagingBarComponent;

  moduleFormControl: FormControl = new FormControl();
  startDateFormControl: FormControl = new FormControl();
  endDateFormControl: FormControl = new FormControl();
  listOfNotifications: any[] = [];
  pageSize: number = 50;
  pageNumber: number = 1;
  filteredTotal: number = 0;
  fromRow: number = 0;
  startDate: any = null;
  endDate: any = null;
  modules: any[] = [];
  listOfModules: any[] = [];
  vendorId: number;
  constructor(
    private notificationService: NotificationService,
    private authService: AuthService,
    protected router: Router,
    private datePipe: DatePipe,
    protected snackBar: MatSnackBar,
    private vendorAdminService: VendorAdminService,
    protected moduleService: ModuleService,
    protected orderService: OrderService,
    protected userNotificationService: UserNotificationService,
    protected warehouseAdminService: WarehouseAdminService,
    protected shippmentOrderService: ShippmentOrderService
  ) {}

  ngOnInit() {
    this.getvendorByVendorAdmin();
    this.getAllNotifications();
    this.getModules();
  }

  public options = function(option, value): boolean {
    if (value != null) {
      return option.id === value.id;
    }
  };

  getvendorByVendorAdmin() {
    let currentUser = this.vendorAdminService.getCurrentLoggedInUser();

    if (currentUser.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin) {
      this.vendorAdminService
        .getVendorByVendorAdminId(currentUser.id)
        .subscribe(result => {
          this.vendorId = result;
        });
    } else if (
      currentUser.type.code == environment.ENUM_CODE_USER_TYPE.WarehouseAdmin
    ) {
      this.warehouseAdminService
        .getVendorIdByUserId(currentUser.id)
        .subscribe((vendorId: number) => {
          console.log(this.vendorId);
          this.vendorId = vendorId;
        });
    }
  }

  getAllNotifications() {
    let user = this.vendorAdminService.getCurrentLoggedInUser();
    let vendorId;
    this.modules = [];
    if (this.endDate < this.startDate) {
      this.snackBar.open("End date must be after than start date.", "close", {
        duration: 3000
      });
      return;
    }
    this.getPagingBar().navigateToPage(1);
    if (this.moduleFormControl.value != null) {
      this.moduleFormControl.value.forEach(module => {
        if (module.name == "Support Ticket") {
          module.name = "Role Support Ticket";
        }
        this.modules.push(module.name);
      });
    }
    this.vendorAdminService
      .getVendorByVendorAdminId(user.id)
      .subscribe(result => {
        vendorId = result;
        this.notificationService
          .getAllNotifications(
            user.id,
            this.pageNumber - 1,
            this.pageSize,
            this.startDate,
            this.endDate,
            this.modules
          )
          .subscribe((data: any) => {
            this.listOfNotifications = data["content"];
            this.filteredTotal = data["totalElements"];
          });
      });
  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.pageNumber = pagingEvent.page;
    this.pageSize = pagingEvent.pageSize;
    this.pageNotifications(this.pageNumber - 1, this.pageSize);
  }

  pageNotifications(pageNumber: number, pageSize: number) {
    let user = this.vendorAdminService.getCurrentLoggedInUser();
    let vendorId;
    this.modules = [];
    if (this.moduleFormControl.value != null) {
      this.moduleFormControl.value.forEach(module => {
        if (module.name == "Support Ticket") {
          module.name = "Role Support Ticket";
        }
        this.modules.push(module.name);
      });
    }
    this.vendorAdminService
      .getVendorByVendorAdminId(user.id)
      .subscribe(result => {
        vendorId = result;
        this.notificationService
          .getAllNotifications(
            user.id,
            this.pageNumber - 1,
            this.pageSize,
            this.startDate,
            this.endDate,
            this.modules
          )
          .subscribe((data: any) => {
            this.listOfNotifications = data["content"];
            this.filteredTotal = data["totalElements"];
          });
      });
  }

  showNotification(notification) {
    this.userNotificationService
      .updateUserNotification(notification.userNotificationId)
      .subscribe(data => {
        this.notificationService
          .getCountUnreadNotifications(
            this.authService.getCurrentLoggedInUser().id
          )
          .subscribe((data: number) => {
            localStorage.setItem("countOfNotifications", data + "");
            AppComponent.setCountNotification(data);
          });
      });
    if (
      notification.moduleName == "Order" &&
      !notification.isSendToWarehouseFunction
    ) {
      let vendorId = Number(localStorage.getItem("vendorId"));
      this.orderService
        .getOrderByOrderNumber(notification.orderNumber)
        .subscribe((data: number) => {
          localStorage.setItem("orderId", data + "");
          this.orderService.findOrderById(data, vendorId).subscribe(
            async data => {
              this.orderService.order = data;
              await this.router.navigateByUrl("orders/all-orders", {
                skipLocationChange: true
              });
              this.router.navigateByUrl("orders/form");
            },
            error => {},
            () => {}
          );
        });
    } else if (
      notification.moduleName == "Order" &&
      notification.isSendToWarehouseFunction
    ) {
      this.orderService
        .getOrderByOrderNumber(notification.orderNumber)
        .subscribe((orderId: number) => {
          this.orderService
            .findOrderByIdOnly(orderId)
            .subscribe((order: Order) => {
              localStorage.setItem("vend", order.vendor.id + "");
              localStorage.setItem("ordership", order.id + "");
              this.userNotificationService
                .getShippmentOrderIdByUserNotificationId(
                  notification.userNotificationId
                )
                .subscribe((shippmentOrder: number) => {
                  localStorage.setItem("shipmentId", shippmentOrder + "");
                  this.shippmentOrderService
                    .findShipmentOrderById(shippmentOrder, order.vendor.id)
                    .subscribe(async res => {
                      this.shippmentOrderService.shippmentOrder = res;
                      AppComponent.shippmentOrder = res;
                      localStorage.setItem(
                        "shipment_order_id",
                        this.shippmentOrderService.shippmentOrder.id + ""
                      );
                      AppComponent.isSendToWarehouseNotification = true;
                      await this.router.navigateByUrl(
                        "shipment_order/all-shipment",
                        {
                          skipLocationChange: true
                        }
                      );
                      this.router.navigateByUrl("shipment_order/shipment_item");
                    });
                });
            });
        });
    } else if (notification.moduleName == "Survey") {
      this.router.navigateByUrl("dashboard");
    } else if (notification.moduleName == "Role Support Ticket") {
      this.router.navigateByUrl("ticket/all-tickets");
    }
  }

  startDateChange(event: any) {
    this.startDate = this.datePipe.transform(event.value, "yyyy-MM-dd");
    this.startDateFormControl.setValue(this.startDate);
  }

  public endDateChange(event: any) {
    if (event.value == undefined) {
      this.endDate = this.startDate;
    } else {
      this.endDate = this.datePipe.transform(event.value, "yyyy-MM-dd");
    }
    this.endDateFormControl.setValue(this.endDate);
  }

  selectModulePerOne(all) {
    if (this.allModulesSelected.selected) {
      this.allModulesSelected.deselect();
      return false;
    }

    if (this.moduleFormControl.value.length == this.listOfModules.length) {
      this.allModulesSelected.select();
    }
  }

  getModules() {
    this.moduleService.getModules().subscribe(data => {
      this.listOfModules = data;
      this.listOfModules.forEach(module => {
        if (module.name == "Role Support Ticket") {
          module.name = "Support Ticket";
        }
      });
    });
  }

  reset() {
    this.startDate = null;
    this.endDate = null;
    this.modules = [];
    this.startDateFormControl.setValue(this.startDate);
    this.endDateFormControl.setValue(this.endDate);
    this.moduleFormControl.setValue([]);
    this.getAllNotifications();
  }
  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
}
