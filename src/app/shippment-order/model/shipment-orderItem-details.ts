export class ShipmentOrderItemDetails {

    orderType: string;
    id: number;
    price: number;
    name: string;
    totalAmount: number;
    orderdQuantity: number;
    quantityToCanceled: number;
    deliveryQuantity: number;
    fulfilledQuantity:number;
    shipmentDeliveryQty:number;
    shipmentOrderItemId:number;
}