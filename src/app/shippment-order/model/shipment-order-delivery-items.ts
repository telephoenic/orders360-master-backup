import { ShipmentOrderDelivery } from "./shipment-order-delivery";
import { ShipmentOrderItem } from "./shipment-order-item";

export class ShipmentOrderDeliveryItems {

    id: number;
    deleted: boolean;
    inactive: boolean;
    amount: number;
    deliveryQuantity: number;
    shipmentOrderDelivery: ShipmentOrderDelivery;
    shipmentOrderItem: ShipmentOrderItem;


    constructor() {
        this.shipmentOrderDelivery = new ShipmentOrderDelivery();
        this.shipmentOrderItem = new ShipmentOrderItem();
    }

}