import { ShipmentOrder } from "./shipment-order";
import { DeliveryUser } from "../../delivery-user/delivery-user.model";
import { BaseModel } from "../../common/model/base-model";
import { ShipmentOrderDeliveryItems } from "./shipment-order-delivery-items";

export class ShipmentOrderDelivery extends BaseModel{
    
    id:number;
    inactive:boolean;
    shipmentOrder:ShipmentOrder;
    deliveryUser:DeliveryUser;
    invoiceNo:number;
    shipmentOrderDeliveryItems:ShipmentOrderDeliveryItems[];

    constructor(){
        super();
        this.shipmentOrder=new ShipmentOrder();
        this.deliveryUser=new DeliveryUser();  
      }


}