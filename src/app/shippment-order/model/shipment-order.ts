import { Order } from "../../orders/order.model"
import { Warehouse } from "../../warehouse/warehouse-model";
import { OrderStatusType } from "../../orders/order-status-type";
import { BaseModel } from "../../common/model/base-model";
import { ShipmentOrderItem } from "./shipment-order-item";
import { ShipmentOrderDelivery } from "./shipment-order-delivery";

export class ShipmentOrder extends BaseModel {
    id: number;
    inactive: boolean;
    order: Order;
    warehouse: Warehouse;
    orderStatusType: OrderStatusType;
    deliveryAmount: number;
    shipmentOrderItem:ShipmentOrderItem[]=[];
    shipmentQty:number;
    deliveryUser:string;
    shipmentOrderDeliveryList:ShipmentOrderDelivery[]=[];
    assignToDeliveryUser:boolean;

    constructor() {
        super();
        this.order = new Order();
        this.warehouse = new Warehouse();
        this.orderStatusType = new OrderStatusType();
    }
}