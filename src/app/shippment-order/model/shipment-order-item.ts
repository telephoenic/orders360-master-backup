import { OrderItem } from "../..//orders/order-item.model";
import { ShipmentOrder } from "./shipment-order";


export class ShipmentOrderItem {
    id:number;
    orderItem: OrderItem;
    shipmentOrder:ShipmentOrder;
    orderHistoryIds:number[]=[];
    deliveryQuantity:number;
    deliveryAmount:number;


    constructor() {
        this.orderItem = new OrderItem();
        this.shipmentOrder=new ShipmentOrder();
    }
}