export class BackToOrderHistoryDto {

    orderType: string;
    name: string;
    warehouseQty: number;
    backToOrder: number;
    newQty: number;
    backToOrderDate: Date;
    userCreated: string;

    constructor() {

    }
}