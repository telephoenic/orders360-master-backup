import { ShipmentOrderItem } from "./shipment-order-item";
import { BaseModel } from "../../.../../common/model/base-model"
import { OrderRejectedReason } from "../../reasons/reason.model";

export class ShipmentOrderItemHistory extends BaseModel{

    id:number;
    inactive:boolean;

    warehouseQuantity: number;
    backToOrder: number;
    newQuantityWarehouse: number;
    shipmentOrderItem: ShipmentOrderItem;
    orderRejectedReason:OrderRejectedReason;

    constructor() {
        super();
        this.shipmentOrderItem = new ShipmentOrderItem();
        this.orderRejectedReason=new OrderRejectedReason();
    }
}