import {
  Component,
  OnInit,
  ViewChild,
  Inject,
  ChangeDetectorRef
} from "@angular/core";
import {
  ITdDataTableColumn,
  TdPagingBarComponent,
  TdDataTableComponent,
  TdDialogService
} from "@covalent/core";
import { ShippmentOrderService } from "../../service/shippment-order.service";
import { OrderService } from "../../../orders/service/order.service";
import { ShipmentOrderItemDetails } from "../../model/shipment-orderItem-details";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
  MatSnackBar
} from "@angular/material";
import { ShipmentOrderItemHistory } from "../../model/shipment-order-item-history";
import { FormControl, Validators } from "@angular/forms";
import { ShipmentOrderItem } from "../../model/shipment-order-item";
import { ShipmentOrderHistoryService } from "../../service/shipment-order-history.service";
import { OrderItem } from "../../../orders/order-item.model";
import { PosService } from "../../../pos-user/service/pos.service";
import { DeliveryUserSerivce } from "../../../delivery-user/services/delivery-user.service";
import { VendorAdminService } from "../../../vendor-admin/service/vendor-admin.service";
import { User } from "../../../auth/user.model";
import { AuthService } from "../../../auth/service/auth.service";
import { DeliveryUser } from "../../../delivery-user/delivery-user.model";
import { PosAssignedVendorService } from "../../../pos-user/service/pos-assigned-vendor.service";
import { PosAassignedVendor } from "../../../pos-user/pos-assigned-vendor";
import { ShipmentOrderDelivery } from "../../model/shipment-order-delivery";
import { ShipmnetOrderDeliveryService } from "../../service/shipmnet-order-delivery.service";
import { DatePipe } from "@angular/common";
import { BackToOrderHistoryDto } from "../../model/back-to-order-history-dto";
import { OrderRejectedReason } from "../../../reasons/reason.model";
import { OrderRejectedReasonEnum } from "../../enum/order-rejected-reason-enum";
import { OrderRejectedReasonService } from "../../../reasons/services/reason-service";
import { ShipmentSerialDlg } from "../../../orders/view/view-details/view-order-details.component";
import { ShipmentOrderDeliveryItems } from "../../model/shipment-order-delivery-items";
import { Router } from "@angular/router";
import { AppComponent } from "../../../app.component";

const DECIMAL_FORMAT: (v: any) => any = (v: number) => v.toFixed(2);

@Component({
  selector: "app-shipment-order-item",
  templateUrl: "./shipment-order-item.component.html",
  styleUrls: ["./shipment-order-item.component.css"]
})
export class ShipmentOrderItemComponent implements OnInit {
  @ViewChild("pagingBar") pagingBar: TdPagingBarComponent;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;

  private showSpinnerOnAssignToDeliveryUser: boolean = false;
  counterOfGetPosUserId: number = 1;

  shipmentOrderColumnConfig: ITdDataTableColumn[] = [
    {
      name: "orderType",
      label: "Order Type",
      tooltip: "order Type",
      width: 150
    },
    { name: "name", label: "Name", tooltip: "Name", width: 350 },
    {
      name: "deliveryQuantity",
      label: "Delivered Qty",
      tooltip: "Delivered Qty",
      width: 180
    },
    {
      name: "shipmentDeliveryQty",
      label: "Qty to Deliver",
      tooltip: "Qty to Deliver",
      width: 180
    },
    {
      name: "fulfilledQuantity",
      label: "In Warehouse",
      tooltip: "In Warehouse",
      width: 180
    },
    {
      name: "amount",
      label: "Amount",
      tooltip: "Amount",
      format: DECIMAL_FORMAT,
      width: 180
    },
    {
      name: "serial",
      label: "Item Serial",
      tooltip: "Item Serial",
      format: DECIMAL_FORMAT,
      width: 110
    },
    {
      name: "id",
      label: "Details",
      tooltip: "Details",
      sortable: false,
      width: 170
    }
  ];

  private _shipmentOrderItem: ShipmentOrderItemDetails[] = [];
  private _user: User;
  public orderRejectedReason: OrderRejectedReason = new OrderRejectedReason();
  public orderRejectedReasonArr: OrderRejectedReason[] = [];
  isButtonDetailsClicked: boolean = false;
  posUserId;
  constructor(
    protected shipmentOrderService: ShippmentOrderService,
    protected posUserService: PosService,
    protected orderService: OrderService,
    protected snackBar: MatSnackBar,
    protected shipmentOrderDeliveryService: ShipmnetOrderDeliveryService,
    protected posAssignVendorService: PosAssignedVendorService,
    protected deliveryUserService: DeliveryUserSerivce,
    protected vendorAdminService: VendorAdminService,
    protected reasonService: OrderRejectedReasonService,
    protected authService: AuthService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected matDialog: MatDialog,
    protected shipmentOrderHistoryService: ShipmentOrderHistoryService
  ) {}

  ngOnInit() {
    this._user = this.authService.getCurrentLoggedInUser();
    this.loadShipmentOrderDetails();
  }

  loadShipmentOrderDetails() {
    if (AppComponent.isSendToWarehouseNotification) {
      this.shipmentOrderService.shippmentOrder = AppComponent.shippmentOrder;
      this.shipmentOrderService
        .findShipmentOrderDetails(this.shipmentOrderService.shippmentOrder.id)
        .subscribe(
          data => {
            this._shipmentOrderItem = data;
          },
          err => {},
          () => {
            this.checkDeliveryUserFlag();
            this.dataTable.refresh();
          }
        );
    } else {
      this.shipmentOrderService
        .findShipmentOrderDetails(this.shipmentOrderService.shippmentOrder.id)
        .subscribe(
          data => {
            this._shipmentOrderItem = data;
          },
          err => {},
          () => {
            this.checkDeliveryUserFlag();
            this.dataTable.refresh();
          }
        );
    }
    this.posUserService
      .getPosUserIdByMobileNumber(
        this.shipmentOrderService.shippmentOrder.order.posUser.mobileNumber
      )
      .subscribe((posId: number) => {
        this.counterOfGetPosUserId++;
        this.posUserId = posId;
        this.shipmentOrderService.shippmentOrder.order.posUser.id = posId;
      });
  }

  loadShipmentOrder() {
    this.shipmentOrderService
      .findShipmentOrderById(
        this.shipmentOrderService.shippmentOrder.id,
        this.shipmentOrderService.shippmentOrder.order.vendor.id
      )
      .subscribe(
        data => {
          this.shipmentOrderService.shippmentOrder = data;
        },
        err => {},
        () => {}
      );
  }

  findSerialItems(row: any) {
    let shipmentSerialArr: any[] = [];
    this.shipmentOrderService
      .findShipmentSerialByShipmentId(row.shipmentOrderItemId)
      .subscribe(
        data => {
          shipmentSerialArr = data;
        },
        err => {},
        () => {
          let dialogRef = this.dialogService.open(ShipmentSerialDlg, {
            width: "500px",
            height: "610px",
            disableClose: true,
            data: { shipmentSerialArr }
          });
        }
      );
  }

  shipmentOrderItemHistory(row: any) {
    this.isButtonDetailsClicked = true;
    this.reasonService
      .findRejctedReasonByCode(OrderRejectedReasonEnum.BackToOrder)
      .subscribe(
        data => {
          this.orderRejectedReason = data;
        },
        err => {},
        () => {
          this.findOrderRejctedReasonByParentId(
            this.orderRejectedReason.id,
            row
          );
        }
      );
  }

  findOrderRejctedReasonByParentId(id: number, row: ShipmentOrderItemDetails) {
    let vendorId: number;
    this.vendorAdminService.getVendorByAdminType(this.user.id).subscribe(
      data => {
        vendorId = data;
      },
      err => {},
      () => {
        this.reasonService.findRejctedReasonByParentId(id, vendorId).subscribe(
          data => {
            this.orderRejectedReasonArr = data;
          },
          err => {},
          () => {
            let dialogRef = this.dialogService.open(
              ShipmentOrderItemHistoryDialog,
              {
                width: "70%",
                disableClose: true,
                data: {
                  shipmentOrderDataTable: this.dataTable,
                  shipmentOrderItemHistory: row,
                  orderRejectedReasonArr: this.orderRejectedReasonArr,
                  shipmentOrder: this.shipmentOrderService.shippmentOrder
                }
              }
            );
            dialogRef.afterClosed().subscribe(result => {
              this.isButtonDetailsClicked = false;
            });
          }
        );
      }
    );
  }

  checkDeliveryUserFlag() {
    this.shipmentOrderHistoryService.assignToDeliveyUserFlag = false;
    let shipmentOrderListFilter = this._shipmentOrderItem.filter(
      item => item.fulfilledQuantity == 0 && item.shipmentDeliveryQty == 0
    ).length;
    if (this._shipmentOrderItem.length == shipmentOrderListFilter) {
      this.shipmentOrderHistoryService.assignToDeliveyUserFlag = true;
    }
  }

  assignToDeliveryMan() {
    this.showSpinnerOnAssignToDeliveryUser = true;
    let vendorId: number;
    this.vendorAdminService.getVendorByAdminType(this._user.id).subscribe(
      data => {
        vendorId = data;
      },
      err => {},
      () => {
        debugger;
        let posAssignVendor: PosAassignedVendor = new PosAassignedVendor();
        this.posAssignVendorService
          .findPosUserById(this.posUserId, vendorId)
          .subscribe(
            data => {
              posAssignVendor = data;
            },
            err => {
              this.showSpinnerOnAssignToDeliveryUser = false;
              this.shipmentOrderService.shippmentOrder.order.posUser.id = this.posUserId;
            },
            () => {
              let deliveryUsers: DeliveryUser[] = [];
              this.deliveryUserService
                .findDeliveryUserByVendorId(vendorId)
                .subscribe(
                  data => {
                    deliveryUsers = data;
                  },
                  err => {},
                  () => {
                    let shipmentOrderDelivery: ShipmentOrderDelivery = new ShipmentOrderDelivery();
                    this.shipmentOrderDeliveryService
                      .findShipmentOrderDeliveryByShipmentOrder(
                        this.shipmentOrderService.shippmentOrder.id
                      )
                      .subscribe(
                        data => {
                          shipmentOrderDelivery = data;
                        },
                        err => {},
                        () => {
                          this.shipmentOrderService
                            .findShipmentOrderDetails(
                              this.shipmentOrderService.shippmentOrder.id
                            )
                            .subscribe(
                              data => {
                                this._shipmentOrderItem = data;
                              },
                              err => {},
                              () => {
                                let dialogRef = this.dialogService
                                  .open(AssignToDeliveryDlg, {
                                    width: "60%",
                                    height: "80%",
                                    disableClose: true,
                                    data: {
                                      posAssignVendor: posAssignVendor,
                                      deliveryUsers: deliveryUsers,
                                      shipmentOrder: this.shipmentOrderService
                                        .shippmentOrder,
                                      shipmentOrderItemList: this
                                        ._shipmentOrderItem,
                                      shipmentOrderItemDt: this.dataTable,
                                      shipmentOrderDelivery: shipmentOrderDelivery,
                                      dataTable: this.dataTable
                                    }
                                  })
                                  .afterClosed()
                                  .subscribe((accept: boolean) => {
                                    debugger;
                                    this.loadShipmentOrder();
                                    this.loadShipmentOrderDetails();
                                    this.dataTable.refresh();
                                    this.showSpinnerOnAssignToDeliveryUser = false;
                                  });
                              }
                            );
                        }
                      );
                  }
                );
            }
          );
      }
    );
  }

  openShipmentOrderHistory() {
    this.dialogService.open(ShipmentOrderHistoryDlg, {
      width: "70%",
      disableClose: true,
      data: {
        shipmentOrder: this.shipmentOrderService.shippmentOrder
      }
    });
  }

  public get user(): User {
    return this._user;
  }
  public set user(value: User) {
    this._user = value;
  }

  public get shipmentOrderItem(): ShipmentOrderItemDetails[] {
    return this._shipmentOrderItem;
  }
  public set shipmentOrderItem(value: ShipmentOrderItemDetails[]) {
    this._shipmentOrderItem = value;
  }
}

@Component({
  selector: "app-shipment-order-item-history-dlg",
  templateUrl: "./shipment-order-item-history-dlg.html",
  providers: [ShipmentOrderItemComponent]
})
export class ShipmentOrderItemHistoryDialog implements OnInit {
  @ViewChild("backToOrderDt") backToOrderDt: TdDataTableComponent;

  backToOrderHistoryColumnConfig: ITdDataTableColumn[] = [
    { name: "userCreated", label: "User", tooltip: "User", width: 110 },
    {
      name: "orderType",
      label: "Order Type",
      tooltip: "Order Type",
      width: 150
    },
    {
      name: "name",
      label: "Product Name",
      tooltip: "Product Name",
      width: 300
    },
    {
      name: "warehouseQty",
      label: "In Warehouse Qty",
      tooltip: "In Warehouse Qty",
      width: 120
    },
    {
      name: "backToOrder",
      label: "Back to Order Qty",
      tooltip: "Back to Order Qty",
      width: 120
    },
    { name: "newQty", label: "New Qty", tooltip: "New Qty", width: 100 },
    {
      name: "backToOrderDate",
      label: "Back to Order Date",
      tooltip: "Back to Order Date",
      format: v => this.datePipe.transform(v, "medium"),
      width: 220
    }
  ];

  backToOrderReasonFormControl = new FormControl("", [Validators.required]);

  backToOrderMax = new FormControl("", [
    Validators.max(this.data.shipmentOrderItemHistory.fulfilledQuantity),
    Validators.min(1),
    Validators.required
  ]);

  private _shipmentOrderItemHistory: ShipmentOrderItemHistory = new ShipmentOrderItemHistory();
  private _backToOrderHistoryDtoList: BackToOrderHistoryDto[] = [];

  constructor(
    public dialogRef: MatDialogRef<ShipmentOrderItemHistoryDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected shipmentOrderHistoryService: ShipmentOrderHistoryService,
    protected dialogService: TdDialogService,
    protected orderService: OrderService,
    protected snackBar: MatSnackBar,
    protected datePipe: DatePipe,
    protected shipmentOrderService: ShippmentOrderService
  ) {}

  ngOnInit() {
    this.getBackToOrderHistory();
    if (
      this.data.shipmentOrder.assignToDeliveryUser ||
      this.data.shipmentOrderItemHistory.fulfilledQuantity == 0
    ) {
      this.backToOrderMax.disable();
    }
  }

  backToOrderEvent() {
    if (
      this._shipmentOrderItemHistory.backToOrder != null &&
      this._shipmentOrderItemHistory.backToOrder > 0
    ) {
      this._shipmentOrderItemHistory.newQuantityWarehouse =
        this.data.shipmentOrderItemHistory.fulfilledQuantity -
        this._shipmentOrderItemHistory.backToOrder;
    } else {
      this._shipmentOrderItemHistory.newQuantityWarehouse = 0;
    }
  }

  public getBackToOrderHistory() {
    this.shipmentOrderHistoryService
      .findAllBackToOrdersHistory(
        this.data.shipmentOrderItemHistory.shipmentOrderItemId
      )
      .subscribe(
        data => {
          this._backToOrderHistoryDtoList = data;
        },
        err => {},
        () => {}
      );
  }

  onSaveBackToOrder() {
    this.dialogService
      .openConfirm({
        message: "Are you sure you want to return Qty to order original Qty?",
        title: "Back To Order",
        cancelButton: "Cancel",
        acceptButton: "OK",
        disableClose: true
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          let shipmentOrderItem: ShipmentOrderItem = new ShipmentOrderItem();
          let orderitem: OrderItem = new OrderItem();
          orderitem.id = this.data.shipmentOrderItemHistory.id;
          orderitem.orderedQuantity = this.data.shipmentOrderItemHistory.orderdQuantity;
          shipmentOrderItem.id = this.data.shipmentOrderItemHistory.shipmentOrderItemId;
          shipmentOrderItem.orderItem = orderitem;
          this._shipmentOrderItemHistory.warehouseQuantity = this.data.shipmentOrderItemHistory.fulfilledQuantity;
          this._shipmentOrderItemHistory.shipmentOrderItem = shipmentOrderItem;
          this._shipmentOrderItemHistory.backToOrder = -this
            ._shipmentOrderItemHistory.backToOrder;
          this._shipmentOrderItemHistory.orderRejectedReason = this.backToOrderReasonFormControl.value;
          this.shipmentOrderHistoryService
            .save(this._shipmentOrderItemHistory)
            .subscribe(
              data => {},
              err => {},
              () => {
                this.shipmentOrderService
                  .findShipmentOrderDetails(
                    this.shipmentOrderService.shippmentOrder.id
                  )
                  .subscribe(
                    data => {
                      this.data.shipmentOrderDataTable.data = data;
                      this.data.shipmentOrderDataTable.refresh();
                    },
                    err => {},
                    () => {
                      this.orderService
                        .findOrderById(
                          this.data.shipmentOrder.order.id,
                          this.data.shipmentOrder.order.vendor.id
                        )
                        .subscribe(
                          data => {
                            this.shipmentOrderService.shippmentOrder.order = data;
                          },
                          err => {},
                          () => {
                            this.checkDeliveryUserFlag();
                            this.data.shipmentOrderDataTable.refresh();
                          }
                        );
                    }
                  );
              }
            );
        }
      });
  }

  checkDeliveryUserFlag() {
    this.shipmentOrderHistoryService.assignToDeliveyUserFlag = false;
    let shipmentOrderListFilter = this.data.shipmentOrderDataTable.data.filter(
      item => item.fulfilledQuantity == 0 && item.shipmentDeliveryQty == 0
    ).length;
    if (
      this.data.shipmentOrderDataTable.data.length == shipmentOrderListFilter
    ) {
      this.shipmentOrderHistoryService.assignToDeliveyUserFlag = true;
    }
  }

  onNoClick() {
    this.dialogRef.close();
  }

  public get shipmentOrderItemHistory(): ShipmentOrderItemHistory {
    return this._shipmentOrderItemHistory;
  }
  public set shipmentOrderItemHistory(value: ShipmentOrderItemHistory) {
    this._shipmentOrderItemHistory = value;
  }

  public get backToOrderHistoryDtoList(): BackToOrderHistoryDto[] {
    return this._backToOrderHistoryDtoList;
  }
  public set backToOrderHistoryDtoList(value: BackToOrderHistoryDto[]) {
    this._backToOrderHistoryDtoList = value;
  }
}

declare var google: any;
@Component({
  selector: "order_delivery_dlg",
  templateUrl: "./order_delivery_dlg.html",
  styleUrls: ["./shipment-order-item.component.css"]
})
export class AssignToDeliveryDlg implements OnInit {
  deliveryUserFormControl = new FormControl("", [Validators.required]);
  private _shipmentOrderDelivery: ShipmentOrderDelivery = new ShipmentOrderDelivery();
  isChangeDeliveryUser: boolean = false;

  options: any;
  overlays: any[];

  constructor(
    public dialogRef: MatDialogRef<AssignToDeliveryDlg>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected shipmentOrderDeliveryService: ShipmnetOrderDeliveryService,
    protected shipmentOrderservice: ShippmentOrderService,
    protected snackBar: MatSnackBar,
    protected dialogService: TdDialogService
  ) {}

  ngOnInit() {
    if (this.data.shipmentOrderDelivery != null) {
      this.data.shipmentOrderDelivery.id = null;
      this._shipmentOrderDelivery = this.data.shipmentOrderDelivery;
      this.deliveryUserFormControl.setValue(
        this.shipmentOrderDelivery.deliveryUser.id
      );
    } else {
      this.isChangeDeliveryUser = true;
    }

    this.options = {
      center: {
        lat: this.data.posAssignVendor.posUser.latitude,
        lng: this.data.posAssignVendor.posUser.longitude
      },
      zoom: 12
    };
    this.overlays = [
      new google.maps.Marker({
        position: {
          lat: this.data.posAssignVendor.posUser.latitude,
          lng: this.data.posAssignVendor.posUser.longitude
        },
        title: "Konyaalti"
      })
    ];
  }

  save() {
    let shipmentOrderDeliveryItems: ShipmentOrderDeliveryItems[] = [];
    this.shipmentOrderDelivery.shipmentOrder = this.data.shipmentOrder;
    this.shipmentOrderDelivery.deliveryUser = this.deliveryUserFormControl.value;

    this.data.shipmentOrderItemList
      .filter(
        item => item.fulfilledQuantity != 0 || item.shipmentDeliveryQty != 0
      )
      .forEach(element => {
        let shipmentOrderDeliveryItem: ShipmentOrderDeliveryItems = new ShipmentOrderDeliveryItems();
        let shipmentOrderItem: ShipmentOrderItem = new ShipmentOrderItem();
        shipmentOrderItem.id = element.shipmentOrderItemId;
        shipmentOrderDeliveryItem.amount =
          element.price *
          (element.fulfilledQuantity == 0
            ? element.shipmentDeliveryQty
            : element.fulfilledQuantity);
        shipmentOrderDeliveryItem.deliveryQuantity =
          element.fulfilledQuantity == 0
            ? element.shipmentDeliveryQty
            : element.fulfilledQuantity;
        shipmentOrderDeliveryItem.shipmentOrderItem = shipmentOrderItem;
        shipmentOrderDeliveryItems.push(shipmentOrderDeliveryItem);
      });

    this.shipmentOrderDelivery.shipmentOrderDeliveryItems = shipmentOrderDeliveryItems;

    this.shipmentOrderDeliveryService
      .onSaveShipmentOrderDelivery(this.shipmentOrderDelivery)
      .subscribe(
        data => {
          this.snackBar.open(data, "Close", { duration: 3000 });
        },
        err => {
          this.snackBar.open(err.error.message, "Close", { duration: 3000 });
        },
        () => {
          this.data.dataTable.refresh();
        }
      );
  }

  updateShipmentDeliveryQty(id: number, deliveryQty: number) {
    this.shipmentOrderservice
      .updateShipmentDeliveryQty(id, deliveryQty)
      .subscribe(
        data => {},
        err => {},
        () => {
          this.findShipmentOrderDetails();
        }
      );
  }

  findShipmentOrderDetails() {
    this.shipmentOrderservice
      .findShipmentOrderDetails(this.shipmentOrderservice.shippmentOrder.id)
      .subscribe(data => {
        this.data.dataTable.data = data;
        this.data.dataTable.refresh();
      });
  }

  onChangeDeliveryUser(event) {
    if (this.data.shipmentOrderDelivery != null) {
      if (this.data.shipmentOrderDelivery.deliveryUser.id != null) {
        if (event.value != null) {
          if (event.value == this.data.shipmentOrderDelivery.deliveryUser.id) {
            this.isChangeDeliveryUser = false;
          } else {
            this.isChangeDeliveryUser = true;
          }
        }
      }
    }
  }

  close() {
    this.dialogRef.close();
  }

  public get shipmentOrderDelivery(): ShipmentOrderDelivery {
    return this._shipmentOrderDelivery;
  }
  public set shipmentOrderDelivery(value: ShipmentOrderDelivery) {
    this._shipmentOrderDelivery = value;
  }
}

@Component({
  selector: "shipment_order_delivery_dlg",
  templateUrl: "./shipment-order-history-dlg.html",
  styleUrls: ["./shipment-order-item.component.css"]
})
export class ShipmentOrderHistoryDlg implements OnInit {
  @ViewChild("shipmentOrderHistory") shipmentOrderHistory: TdDataTableComponent;

  shipmentOrderHistoryColumnConfig: ITdDataTableColumn[] = [
    { name: "audit.createdBy", label: "User", tooltip: "User", width: 150 },
    {
      name: "audit.createdDate",
      label: "Assign Date",
      tooltip: "Assign Date",
      format: v => this.datePipe.transform(v, "medium"),
      width: 220
    },
    {
      name: "deliveryUser.name",
      label: "Delivery User",
      tooltip: "Delivery User",
      width: 200
    },
    {
      name: "shipmentOrder.orderStatusType.name",
      label: "Shipment Status",
      tooltip: "Shipment Status",
      width: 200
    },
    {
      name: "shipmentOrder.audit.createdDate",
      label: "Shipment Date",
      tooltip: "Shipment Date",
      format: v => this.datePipe.transform(v, "medium"),
      width: 225
    }
  ];

  private _shipmnetOrderHistory: ShipmentOrderDelivery[] = [];

  constructor(
    public dialogRef: MatDialogRef<AssignToDeliveryDlg>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected shipmentOrderDeliveryService: ShipmnetOrderDeliveryService,
    protected shipmentOrderHistoryService: ShipmentOrderHistoryService,
    protected datePipe: DatePipe,
    protected dialogService: TdDialogService
  ) {}

  ngOnInit() {
    this.getShipmentHistoryList();
  }

  public getShipmentHistoryList() {
    this.shipmentOrderDeliveryService
      .findShipmentOrderHistory(this.data.shipmentOrder.id)
      .subscribe(
        data => {
          this._shipmnetOrderHistory = data;
        },
        err => {},
        () => {}
      );
  }

  close() {
    this.dialogRef.close();
  }

  public get shipmnetOrderHistory(): ShipmentOrderDelivery[] {
    return this._shipmnetOrderHistory;
  }
  public set shipmnetOrderHistory(value: ShipmentOrderDelivery[]) {
    this._shipmnetOrderHistory = value;
  }
}
