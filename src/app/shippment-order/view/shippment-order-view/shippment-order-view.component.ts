import { Component, OnInit, ViewChild } from '@angular/core';
import { TdPagingBarComponent, TdDataTableComponent, ITdDataTableColumn, TdDialogService, TdDataTableSortingOrder, IPageChangeEvent } from '@covalent/core';
import { DatePipe } from '@angular/common';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { OrderService } from '../../../orders/service/order.service';
import { FcmService } from '../../../fcm/services/fcm.service';
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../auth/service/auth.service';
import { MatSnackBar } from '@angular/material';
import { NgxPermissionsService } from 'ngx-permissions';
import { OrderStatusService } from '../../../orders/service/order-status.service';
import { ShippmentOrderService } from '../../service/shippment-order.service';
import { User } from '../../../auth/user.model';
import { ShipmentOrder } from '../../model/shipment-order';
import { WarehouseService } from '../../../warehouse/service/warehouse.service';
import { Warehouse } from '../../../warehouse/warehouse-model';
import { FormControl } from '@angular/forms';
import { OrderStatusType } from '../../../orders/order-status-type';
import { OrderStatusTypeCode } from '../../../orders/enums/order-status-type-code.enum';
import { DeliveryUserSerivce } from '../../../delivery-user/services/delivery-user.service';
import { DeliveryUser } from '../../../delivery-user/delivery-user.model';
import { TSMap } from 'typescript-map';
import { environment } from '../../../../environments/environment';
import { AppComponent } from '../../../app.component';

const DECIMAL_FORMAT: (v: any) => any = (v: number) => v.toFixed(2);


@Component({
  selector: 'app-shippment-order-view',
  templateUrl: './shippment-order-view.component.html',
  styleUrls: ['./shippment-order-view.component.css']
})
export class ShippmentOrderViewComponent extends BaseViewComponent implements OnInit {

  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private _allShipmentOrder: ShipmentOrder[] = [];
  private _errors: any = '';
  private _selectedRows: ShipmentOrder[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = '';
  private _user: User;
  private _warehouses: Warehouse[] = [];
  private _shipmentStatus: OrderStatusType[] = [];
  public vendorId: number;
  private _deliveryUsers: DeliveryUser[] = [];
  private _deliveryUser: DeliveryUser = new DeliveryUser();
  private _shipmnetViewFilterMap = new TSMap<string, number[]>();
  private _endDate: any;
  private _startDate: any;



  warehousesFormControl = new FormControl();
  shipmentStatusFormControl = new FormControl();
  shipmentOrderDeliveryUser = new FormControl();

  @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
  @ViewChild('dataTable') dataTable: TdDataTableComponent;


  columnsConfig: ITdDataTableColumn[] = [

    { name: 'order.orderNumber', label: 'Order #', tooltip: "Order #", width: 90 },
    { name: 'shipmentNo', label: 'Shipment #', tooltip: "Shipment #", width: 90 },
    { name: 'order.posUser.name', label: 'Point of Sale', tooltip: "Point of Sale", width: 170 },
    { name: 'warehouse.name', label: 'Warehouse', tooltip: "Warehouse", width: 170 },
    { name: 'order.audit.createdDate', label: 'Order Date', format: v => this.datePipe.transform(v, 'medium'), tooltip: "Order date", width: 230 },
    { name: 'deliveryAmount', label: 'Shipment Amount', tooltip: "Shipment Amount", format: DECIMAL_FORMAT, width: 120 },
    { name: 'orderStatusType.name', label: 'Shipment Status', tooltip: "Shipment Status", width: 170 },
    { name: 'audit.createdDate', label: 'Shipment Date', format: v => this.datePipe.transform(v, 'medium'), tooltip: "Shipment date", width: 230 },
    { name: 'deliveryUser', label: 'Delivery User', tooltip: "Delivery User", sortable: false, width: 100 },
    {
      name: 'id', label: 'Details', tooltip: "Details", sortable: false, width: 100
    },
  ];

  constructor(protected orderService: OrderService,
    protected fcmService: FcmService,
    protected vendorAdminService: VendorAdminService,
    protected deliveryUserService: DeliveryUserSerivce,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected authService: AuthService,
    protected snackBar: MatSnackBar,
    private datePipe: DatePipe,
    protected permissionsService: NgxPermissionsService,
    protected orderStatusTypeService: OrderStatusService,
    protected shipmentOrderService: ShippmentOrderService,
    protected warehouseService: WarehouseService) {
    super(shipmentOrderService, router, dialogService, snackBar);
  }

  ngOnInit() {
    this.user = this.authService.getCurrentLoggedInUser();
    this._pageSize=environment.initialViewRowSize
    this.loadWarehouse();
    this.loadShipmentStatus();
    this.onLoadDeliveryUser();
    this.findShipmnetsOrderByLoggedUesr();
  }

  filterByVendor(pageNumber: number, pageSize: number,
    searchTerm: string, sortBy: string, sortOrder: string, vendorId: number) {

  }

  findShipmnetsOrderByLoggedUesr() {
    this.shipmentOrderService.findAllShipmentOrderByVendorId(this._currentPage - 1, this.getPagingBar().pageSize, this.user.id, this._searchTerm,
      this._sortBy, this._sortOrder, this.startDate, this.endDate, this._shipmnetViewFilterMap).subscribe(
        data => {
          this.allModels = data["content"];
          this.filteredTotal = data["totalElements"]
        }
      );
  }

  search(searchTerm: string): void {
    this.searchTerm = searchTerm;
    this.selectedRows = [];
    this.getPagingBar().navigateToPage(1);
    this.getPagingBar().pageSize = environment.initialViewRowSize;
    this.findShipmnetsOrderByLoggedUesr();
  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize = pagingEvent.pageSize;

    this.shipmentOrderService.findAllShipmentOrderByVendorId(this._currentPage - 1, this.pageSize, this.user.id, this._searchTerm,
    this._sortBy, this._sortOrder, this.startDate, this.endDate, this._shipmnetViewFilterMap).subscribe(
      data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"]
      }
    );
}


  loadWarehouse() {
    this.warehouseService.findWarehouseByWarehouseUser(this.user.id).subscribe(warehouses => {
      this._warehouses = warehouses;
    });
  }

  loadShipmentStatus() {
    this.orderStatusTypeService.findOrderStatusTypeByCode(OrderStatusTypeCode.Delivery).subscribe(shipmentStatus => {
      this.orderStatusTypeService.findOrderStatusByParentId(shipmentStatus.id).subscribe(data => {
        this._shipmentStatus = data;
      })

    })
  }

  shipmentItem(id: number, shippmentItem: any) {
    AppComponent.isSendToWarehouseNotification = false;
    this.shipmentOrderService.shippmentOrder = shippmentItem;
    this.loadShipmentOrder();
  }

  loadShipmentOrder() {
    this.shipmentOrderService.findShipmentOrderById(this.shipmentOrderService.shippmentOrder.id,
      this.shipmentOrderService.shippmentOrder.order.vendor.id).subscribe(
        data => {
          this.shipmentOrderService.shippmentOrder = data
        },
        err => { },
        () => {
          this.router.navigate(["/shipment_order/shipment_item"]);
        }
      )
  }

  onWharehouseChange(event) {
    this.shipmnetViewFilterMap.set("warehouses", event.value.map(item => item.id))
    if (this.shipmnetViewFilterMap.get("warehouses").length === 0) {
      this.shipmnetViewFilterMap.set("warehouses", null)
    }
    this.getShipmnetOrder();
  }

  dateSearch() {
    this.getShipmnetOrder();
  }

  validateDate() {
    if (this._endDate < this._startDate) {
      this.snackBar.open("End date must be after than start date.", "close", {
        duration: 3000
      });
      throw new Error("End date must be after than start date")
    }
  }


  onChangeDeliveryUser(event: any) {
    this.shipmnetViewFilterMap.set("deliveryUser", event.value.map(item => item.id))

    if (this.shipmnetViewFilterMap.get("deliveryUser").length === 0) {
      this.shipmnetViewFilterMap.set("deliveryUser", null)
    }
    this.getShipmnetOrder();
  }

  onChangeShipmentStatus(event) {
    this.shipmnetViewFilterMap.set("status", event.value.map(item => item.id))

    if (this.shipmnetViewFilterMap.get("status").length === 0) {
      this.shipmnetViewFilterMap.set("status", null)
    }
    this.getShipmnetOrder();
  }



  onLoadDeliveryUser() {
    this.vendorAdminService.getVendorByWarehouseUserId(this.user.id).subscribe(
      data => { this.vendorId = data },
      err => { },
      () => {
        if (this.vendorId != null) {
          this.getDeliveryUser(this.vendorId)
        }

      }
    )

  }

  getDeliveryUser(vendorId: number) {
    this.deliveryUserService.findDeliveryUserByVendorId(vendorId).subscribe(
      data => { this._deliveryUsers = data },
      err => { },
      () => { }
    )
  }


  public onChangeDate(event: any) {
    this.startDate = this.datePipe.transform(event.value, 'yyyy-MM-dd');
  }

  public endDateChange(event: any) {
    this.endDate = this.datePipe.transform(event.value, 'yyyy-MM-dd');
  }


  public getShipmnetOrder() {
    this.validateDate();
    if (this._pageSize == undefined) {
      this._pageSize = environment.initialViewRowSize;
    }
    this.shipmentOrderService.findAllShipmentOrderByVendorId(this.currentPage - 1, this._pageSize, this.user.id, this.searchTerm,
      this.sortBy, this.sortOrder, this.startDate, this.endDate, this._shipmnetViewFilterMap).subscribe(
        data => {
          this.allModels = data["content"];
          this.filteredTotal = data["totalElements"]
        })
  }


  public options = function (option, value): boolean {
    if (value != null) {
      return option.id === value.id;
    }
  }

  public get shipmnetViewFilterMap() {
    return this._shipmnetViewFilterMap;
  }
  public set shipmnetViewFilterMap(value) {
    this._shipmnetViewFilterMap = value;
  }

  public get startDate(): any {
    return this._startDate;
  }
  public set startDate(value: any) {
    this._startDate = value;
  }


  public get endDate(): any {
    return this._endDate;
  }
  public set endDate(value: any) {
    this._endDate = value;
  }

  public get deliveryUsers(): DeliveryUser[] {
    return this._deliveryUsers;
  }
  public set deliveryUsers(value: DeliveryUser[]) {
    this._deliveryUsers = value;
  }
  public get deliveryUser(): DeliveryUser {
    return this._deliveryUser;
  }
  public set deliveryUser(value: DeliveryUser) {
    this._deliveryUser = value;
  }

  public get currentPage(): number {
    return this._currentPage;
  }
  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }
  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get errors(): any {
    return this._errors;
  }
  public set errors(value: any) {
    this._errors = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }
  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }
  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }
  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }
  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }
  public get searchTerm(): string {
    return this._searchTerm;
  }
  public set searchTerm(value: string) {
    this._searchTerm = value;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get user(): User {
    return this._user;
  }
  public set user(value: User) {
    this._user = value;
  }

  public get allShipmentOrder(): ShipmentOrder[] {
    return this._allShipmentOrder;
  }
  public set allShipmentOrder(value: ShipmentOrder[]) {
    this._allShipmentOrder = value;
  }
  public get selectedRows(): ShipmentOrder[] {
    return this._selectedRows;
  }
  public set selectedRows(value: ShipmentOrder[]) {
    this._selectedRows = value;
  }

  get allModels(): ShipmentOrder[] {
    return this._allShipmentOrder;
  }
  set allModels(shippmentOrder: ShipmentOrder[]) {
    this._allShipmentOrder = shippmentOrder;
  }

  public get warehouses(): Warehouse[] {
    return this._warehouses;
  }
  public set warehouses(value: Warehouse[]) {
    this._warehouses = value;
  }

  public get shipmentStatus(): OrderStatusType[] {
    return this._shipmentStatus;
  }
  public set shipmentStatus(value: OrderStatusType[]) {
    this._shipmentStatus = value;
  }

}
