import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippmentOrderViewComponent } from './shippment-order-view.component';

describe('ShippmentOrderViewComponent', () => {
  let component: ShippmentOrderViewComponent;
  let fixture: ComponentFixture<ShippmentOrderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShippmentOrderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippmentOrderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
