import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { ShipmentOrderDelivery } from '../model/shipment-order-delivery';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ShipmnetOrderDeliveryService extends BaseService {

  shipmentOrderDelivery: ShipmentOrderDelivery;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService,
    private datePipe: DatePipe, ) {
    super(http, "shipment_order_delivery", authService)
  }

  public findShipmentOrderHistory(shipmentOrderId:number): Observable<any> {
    return this.http.get(environment.baseURL + "shipment_order_delivery/find_all_shipment_history/"+shipmentOrderId)
  }

  findShipmentOrderDeliveryByShipmentOrder(id:number):Observable<any>{
    return this.http.get(environment.baseURL+"shipment_order_delivery/find_shipment_order_delivery_by_shipment_id/"+id)
  }
  
  onSaveShipmentOrderDelivery(shipmentOrderDelivery: ShipmentOrderDelivery):Observable<any> {
    return this.http.post(environment.baseURL + "shipment_order_delivery/save_shipment_order_delivery", JSON.stringify(shipmentOrderDelivery),
      { headers: this.getBasicHeaders(),responseType: 'text'})
  }

  get model(): ShipmentOrderDelivery {
    return this.shipmentOrderDelivery;
  }
  set model(model: ShipmentOrderDelivery) {
    this.shipmentOrderDelivery = <ShipmentOrderDelivery>model;
  }

}
