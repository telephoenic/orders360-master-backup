import { Injectable } from "@angular/core";
import { BaseService } from "../../common/service/base.service";
import { ShipmentOrder } from "../model/shipment-order";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../../auth/service/auth.service";
import { DatePipe } from "@angular/common";
import { BaseModel } from "../../common/model/base-model";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { ShipmentOrderItemHistory } from "../model/shipment-order-item-history";
import { ShipmentOrderItem } from "../model/shipment-order-item";
import { TSMap } from "typescript-map";

@Injectable({
  providedIn: "root"
})
export class ShippmentOrderService extends BaseService {
  shippmentOrder: ShipmentOrder;
  navigateToAfterCompletion: string;
  
  constructor(
    http: HttpClient,
    protected authService: AuthService,
    private datePipe: DatePipe
  ) {
    super(http, "shipment_order", authService);
  }

  findAllShipmentOrderByVendorId(
    pageNum: number,
    pageSize: number,
    loggedId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    startDate: Date,
    endDate: Date,
    shipmentViewFilter: TSMap<string, number[]>
  ): Observable<any> {
    let parameterList: { paramName: string; paramValue: any }[] = [];
    if (startDate != undefined || startDate != null) {
      parameterList.push({ paramName: "start_date", paramValue: startDate });
    }
    if (endDate != undefined || endDate != null) {
      parameterList.push({ paramName: "end_date", paramValue: endDate });
    }

    return this.http.post(
      environment.baseURL +
        "shipment_order/find_all_shipment_order/" +
        pageNum +
        "/" +
        pageSize +
        "/" +
        loggedId,
      shipmentViewFilter.toJSON(),
      {
        params: this.prepareSearchParameters(
          searchTerm,
          sortBy,
          sortOrder,
          parameterList
        ),
        headers: this.getBasicHeaders()
      }
    );
  }

  findAllShipmentOrder(pageNumber: number, pageSize: number, orderId) {
    return this.http.get(
      environment.baseURL +
        "shipment_order/find_shipments_by_order_id/" +
        pageNumber +
        "/" +
        pageSize +
        "/" +
        orderId
    );
  }

  getShipmentOrderByOrderId(orderId: number) {
    return this.http.get(
      environment.baseURL + "shipment_order/find_shipments_order_by_id/" + orderId
    );
  }

  onSaveShipmentOrderItemHistory(
    shipmentOrderItemHistory: ShipmentOrderItemHistory
  ) {
    return this.http.post(
      environment.baseURL + "shipment_order/shipment_history_save",
      JSON.stringify(shipmentOrderItemHistory),
      { headers: this.getBasicHeaders() }
    );
  }

  findShipmentOrderDetails(shipmentOrder: number): Observable<any> {
    return this.http.get(
      environment.baseURL +
        "shipment_order_item/find_shipment_detail/" +
        shipmentOrder
    );
  }

  findShipmentOrderById(shipmentOrder: number, vendorId): Observable<any> {
    return this.http.get(
      environment.baseURL +
        "shipment_order/find_shipment_order_by_id/" +
        shipmentOrder +
        "/" +
        vendorId
    );
  }

  saveShipmentOrderItem(shipmentOrderItem: ShipmentOrderItem): Observable<any> {
    return this.http.post(
      environment.baseURL + "shipment_order_item/save",
      JSON.stringify(shipmentOrderItem),
      { headers: this.getBasicHeaders() }
    );
  }

  saveOrderShipment(shippmentOrder: ShipmentOrder): Observable<any> {
    return this.http.post(
      environment.baseURL + "shipment_order/save",
      JSON.stringify(shippmentOrder),
      { headers: this.getBasicHeaders() }
    );
  }

  updateShipmentDeliveryQty(id: number, deliveryQty: number) {
    return this.http.post(
      environment.baseURL +
        "shipment_order_item/update_shipment_deliveryQty/" +
        deliveryQty,
      JSON.stringify(id),
      { headers: this.getBasicHeaders() }
    );
  }

  findShipmentSerialByShipmentId(shipmentItemId: number): Observable<any> {
    return this.http.get(
      environment.baseURL +
        "shipment_serial/find_serial_by_shipment_item/" +
        shipmentItemId
    );
  }

  get model() {
    return null;
  }
  set model(model: BaseModel) {
    this.shippmentOrder = <ShipmentOrder>model;
  }
}
