import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { ShipmentOrderItemHistory } from '../model/shipment-order-item-history';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { DatePipe } from '@angular/common';
import { BaseModel } from '../../common/model/base-model';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShipmentOrderHistoryService extends BaseService {

  shippmentOrderHistory: ShipmentOrderItemHistory;
  navigateToAfterCompletion: string;
  public assignToDeliveyUserFlag = false;
  

  constructor(http: HttpClient,
    protected authService: AuthService,
    private datePipe: DatePipe, ) {
    super(http, "shipment_history", authService)

  }

  updateOrderQuantity(orderQty: number) {
    return this.http.post(environment.baseURL + "shipment_history/update_order_qty", JSON.stringify(orderQty),
      { headers: this.getBasicHeaders() })
  }

  findAllBackToOrderItemHistory(itemId: number): Observable<any> {
    return this.http.get(environment.baseURL + "shipment_history/all_back_to_order/" + itemId)
  }

  findAllBackToOrdersHistory(shipmentOrderId:number): Observable<any> {
    return this.http.get(environment.baseURL + "shipment_history/find_all_back_to_order_history/"+shipmentOrderId)
  }

  get model() {
    return null;
  }
  set model(model: BaseModel) {
    this.shippmentOrderHistory = <ShipmentOrderItemHistory>model;
  }
}
