import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippmentOrderComponent } from './shippment-order.component';

describe('ShippmentOrderComponent', () => {
  let component: ShippmentOrderComponent;
  let fixture: ComponentFixture<ShippmentOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShippmentOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippmentOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
