
export enum OrderRejectedReasonEnum{
    OrderItemCancelation="OIC",
    RejectOrder="RO",
    BackToOrder="BTO",
    CancelOrder="CO",
    SignupPosRequest = "SPR"
}