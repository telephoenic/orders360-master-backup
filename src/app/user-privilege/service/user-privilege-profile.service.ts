import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { UserPrivilegeProfile } from '../user-privilege-profiles';

import { AuthService } from '../../auth/service/auth.service';
import { BaseModel } from '../../common/model/base-model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserPrivilegeProfileService extends BaseService{
  static readonly URL_FILTER_BY_USER_ID = "/by-user-id";
  userPrivilegeProfile:UserPrivilegeProfile;
  navigateToAfterCompletion: string;
  vendorId:number;
  constructor(protected http:HttpClient,
              public authService:AuthService) { 
    super(http, "upp", authService);
  }

  get model(){
    return this.userPrivilegeProfile;
  }
  set model(model:UserPrivilegeProfile) {
    this.userPrivilegeProfile = <UserPrivilegeProfile>model;
  }

  filterByUserId( searchTerm: string,
                  sortBy: string,
                  sortOrder: string,
                  selectedPrivilegeProfiles:boolean,
                  extraParam:boolean,
                  selectedSecreen:string,
                  userId:number){

                    console.log(this.vendorId);  
    
    let parameterList:{paramName:string, paramValue: string}[]=[];
    
    if(userId != undefined) {
      parameterList.push({paramName:"user_id", paramValue: String(userId)})
    }

    if(this.vendorId != undefined) {
      parameterList.push({paramName:"vendor_id", paramValue: String(this.vendorId)})
    }

    if(selectedPrivilegeProfiles != undefined) {
      parameterList.push({paramName:"selected_pp", paramValue: String(selectedPrivilegeProfiles)})
    }

    if(extraParam != undefined) {
      parameterList.push({paramName:"extra_param", paramValue: String(extraParam)})
    }

    if(selectedSecreen !=undefined){
      parameterList.push({paramName:"selected_secreen", paramValue: String(selectedSecreen)})
    }

    return this.filterNotPageable(  searchTerm, 
                                    sortBy, 
                                    sortOrder,
                                    UserPrivilegeProfileService.URL_FILTER_BY_USER_ID, 
                                    (parameterList.length > 0?parameterList:undefined));  
    
  }
}
