import { Component, OnInit } from '@angular/core';
import { BaseViewComponent } from '../../common/view/base-view-component';
import { TdDataTableSortingOrder, TdDataTableComponent, ITdDataTableColumn } from '@covalent/core';
import { UserPrivilegeProfile } from './../user-privilege-profiles';
import { TdPagingBarComponent } from '@covalent/core';
import { AllModuleRolesByAppComponent } from '../../privilege/aware/module-role/views/all-module-roles-by-app/all-module-roles-by-app.component';
import { Input, Output } from '@angular/core';
import { ViewChild } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { environment } from '../../../environments/environment';
import { MatSnackBar } from '@angular/material';
import { TdDialogService } from '@covalent/core';
import { Router } from '@angular/router';
import { UserPrivilegeProfileService } from './../service/user-privilege-profile.service';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { User } from '../../auth/user.model';
import { AuthService } from '../../auth/service/auth.service';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';

@Component({
	selector: 'user-privilege-profile',
	templateUrl: './user-privilege-profile.component.html',
	styleUrls: ['./user-privilege-profile.component.css']
})
export class UserPrivilegeProfileComponent extends BaseViewComponent implements OnInit {
	private _currentPage: number = 1;
	private _fromRow: number = 1;
	public userPrivilegeProfiles: UserPrivilegeProfile[] = [];
	private _errors: any = '';
	private _selectedRows: UserPrivilegeProfile[] = [];
	private _pageSize: number = environment.initialViewRowSize;
	private _filteredTotal: number;
	private _sortBy: string = '';
	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
	private _searchTerm: string = '';
	loggedUser: User;
	private _vendorId: number;


	@Output("rowSelected")
	selectedModulesEventEmitter = new EventEmitter();


	@ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
	@ViewChild('dataTable') dataTable: TdDataTableComponent;

	@Input("selectedPrivilegeProfilesSide")
	selectedPrivilegeProfilesSide: boolean;

	@Input("otherUserPrivilegeProfileComponent")
	otherUserPrivilegeProfileComponent: AllModuleRolesByAppComponent

	columnsConfig: ITdDataTableColumn[] = [
		{ name: 'privilegeProfile.name', label: 'Name', sortable: false },
	];

	constructor(protected userPrivilegeProfileService: UserPrivilegeProfileService,
		protected router: Router,
		protected dialogService: TdDialogService,
		protected authService: AuthService,
		protected snackBar: MatSnackBar,
		protected vednorAdminS: VendorAdminService) {
		super(userPrivilegeProfileService,
			router,
			dialogService,
			snackBar);
	}

	ngOnInit() {


	}

	compareWith(row: UserPrivilegeProfile, model: UserPrivilegeProfile): boolean {
		if (row.id == null) {
			return row.privilegeProfile.id == model.privilegeProfile.id;
		}

		return row.id === model.id;
	}

	filterPrivilegeProfileForUser(userId?: number, extraParam?: boolean, selectedSecreen?: string,selectedVendor?:number ,functionToExecuteOnComplete?: Function, callerObj?: UserPrivilegeProfile) {
		this.loggedUser=this.authService.getCurrentLoggedInUser();
		this.vednorAdminS.getVendorByVendorAdminId(this.loggedUser.id).subscribe(venodrId => {
			if(venodrId == null && this.loggedUser.type.code=="TA"){
				this.userPrivilegeProfileService.vendorId=selectedVendor;
			}else{
				this.userPrivilegeProfileService.vendorId=venodrId;
			}
			this.userPrivilegeProfileService
				.filterByUserId(this.searchTerm,
					this.sortBy,
					this.sortOrder.toString(),
					this.selectedPrivilegeProfilesSide,
					extraParam,
					selectedSecreen,
					userId)
				.subscribe((data: UserPrivilegeProfile) => {
					this.userPrivilegeProfiles = data['content'];
					this.filteredTotal = this.userPrivilegeProfiles.length;
				},
					() => { },
					() => { });

		})
	}

	onMoveRows() {
		this.selectedModulesEventEmitter.emit({
			rows: this._selectedRows,
			movedAsAssigned: !this.selectedPrivilegeProfilesSide
		});

		this.selectedRows = [];
	}

	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}

	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

	public get currentPage(): number {
		return this._currentPage;
	}

	public set currentPage(value: number) {
		this._currentPage = value;
	}

	public get fromRow(): number {
		return this._fromRow;
	}

	public set fromRow(value: number) {
		this._fromRow = value;
	}

	public get allModels(): UserPrivilegeProfile[] {
		return this.userPrivilegeProfiles;
	}

	public set allModels(value: UserPrivilegeProfile[]) {
		this.userPrivilegeProfiles = value;
	}

	public get errors(): any {
		return this._errors;
	}

	public set errors(value: any) {
		this._errors = value;
	}

	public get selectedRows(): UserPrivilegeProfile[] {
		return this._selectedRows;
	}

	public set selectedRows(value: UserPrivilegeProfile[]) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string {
		return this._sortBy;
	}

	public set sortBy(value: string) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder) {
		this._sortOrder = value;
	}

	public get searchTerm(): string {
		return this._searchTerm;
	}

	public set searchTerm(value: string) {
		this._searchTerm = value;
	}

	public get vendorId(): number {
		return this._vendorId;
	}
	public set vendorId(value: number) {
		this._vendorId = value;
	}
}
