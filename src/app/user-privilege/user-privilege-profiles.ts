import { User } from "../auth/user.model";
import { PrivilegeProfile } from "../privilege/privilege-profile";
import { BaseModel } from "../common/model/base-model";

export class UserPrivilegeProfile extends BaseModel {
    id:number;
    inactive:boolean;
    user:User;
    privilegeProfile:PrivilegeProfile;
}