export class Node {
    id: number;
    index: number ;
    name: string;
    inactive:boolean;
    description: string;
    haschild:boolean ;
    children: Node[] = [];
    expand: boolean = false;
    firstNode: boolean;
    lastNode: boolean;
    ancestors: Node[] = [];

}
