import { BaseModel } from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment";
import { Http, Headers, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { DataSource } from '@angular/cdk/collections';
import { Product } from '../product';
import { BaseService } from '../../common/service/base.service';
import { RequestOptions } from '@angular/http/src/base_request_options';
import { Body } from '@angular/http/src/body';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ProductService extends BaseService {
  product: Product;
  categoryId: number;
  navigateToAfterCompletion: string;

  constructor(protected http: HttpClient,
    protected authService: AuthService) {
    super(http, "product", authService);
  }

  getAllProductsByVendorId(pageNum: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string): Observable<any> {
    return this.http.get(environment.baseURL + "product/product_for_vendor/" + pageNum + "/" + pageSize + "/" + vendorId,
      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      });
  }

  getAllDropListProductsByVendorId(vendorId: number): Observable<any> {
    return this.http.get(environment.baseURL + "product/product_list/" + vendorId);
  }

  getProductsByVendorCategoryId(pageNum: number,
    pageSize: number,
    vendorId: number,
    categoryId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
  ): Observable<any> {

    if (categoryId != undefined) {
      return this.http.get(environment.baseURL + "product/product_for_vendor_category/" + pageNum + "/" +
        pageSize + "/" + vendorId + "/" + categoryId,


        {
          params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
          headers: this.getBasicHeaders()
        });
    } else {
      return this.http.get(environment.baseURL + "product/product_for_vendor/" + pageNum + "/" +
        pageSize + "/" + vendorId,

        {
          params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
          headers: this.getBasicHeaders()
        });
    }
  }

  getProductsByLoyalty(pageNum: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
  ): Observable<any> {
    return this.http.get(environment.baseURL + "product/product_for_loyalty/" + pageNum + "/" +
      pageSize + "/" + vendorId,

      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      });
  }


  getAllPrmotionProductsByPosGroup(posGroupId: number, vendorId: number, pageNum: number,
    pageSize: number, sortBy: string, sortOrder: string,
    searchTerm: string) {
    return this.http.get(environment.baseURL + "product/all_promotion_product_by_pos_group/" + pageNum + "/" + 
                                                                                               pageSize + "/" + 
                                                                                               posGroupId + "/" + vendorId,
      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      });

  }

  getProductsByReward(pageNum: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
  ): Observable<any> {
    return this.http.get(environment.baseURL + "product/product_for_reward/" + pageNum + "/" +
      pageSize + "/" + vendorId,

      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      });
  }

  public getSelectedProductsForDiscount(posGroupId: number,
    discountId: number): Observable<any> {
    return this.http.get(environment.baseURL + "product/products_for_discount/" + posGroupId
      + "/" + discountId);
  }

  getAllStore(): Observable<any> {
    return this.http.get(environment.baseURL + "store/all");
  }


  getManufacutreByStoreId(storeId: number): Observable<any> {
    return this.http.get(environment.baseURL + "manufacture/find_manufacture_by_store/" + storeId);
  }

  getAllManufactures(): Observable<any> {
    return this.http.get(environment.baseURL + "manufacture/all");
  }
  getManufaturesByVendorId(vendorId): Observable<any> {
    return this.http.get(environment.baseURL + "manufacture/getByVendorId/" + vendorId);
  }
  getStoresByVendorId(vendorId): Observable<any> {
    return this.http.get(environment.baseURL + "store/getByVendorId/" + vendorId);
  }
  getBrandsByVendorId(vendorId): Observable<any> {
    return this.http.get(environment.baseURL + "brand/getByVendorId/" + vendorId);
  }
  get model() {
    return this.product;
  }

  set model(model: BaseModel) {
    this.product = <Product>model;
  }

  setCategoryId(categoryId: number): void {
    this.categoryId = categoryId;
  }

  getCategoryId(): number {
    return this.categoryId;
  }
}
