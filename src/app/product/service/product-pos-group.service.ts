import { BaseModel } from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment";
import { Http, Headers, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { BaseService } from '../../common/service/base.service';
import { RequestOptions } from '@angular/http/src/base_request_options';
import { Body } from '@angular/http/src/body';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { ProductPosGroup } from '../product-pos-group.model';

@Injectable()
export class ProductPosGroupService extends BaseService {
  productPosGroup: ProductPosGroup;
  navigateToAfterCompletion: string;

  constructor(protected http: HttpClient,
    protected authService: AuthService) {
    super(http, "product-pos-group", authService);
  }

  get model() {
    return this.productPosGroup;
  }

  set model(model: BaseModel) {
    this.productPosGroup = <ProductPosGroup>model;
  }

}
