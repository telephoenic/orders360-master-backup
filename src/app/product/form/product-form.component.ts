import { Store } from "../../store/store";
import { Category } from "../../category/category";
import { ProductService } from "../service/product.service";
import { BaseFormComponent } from "../../common/form/base-form-component";

import { Router, ActivatedRoute } from "@angular/router";
import { Component, OnInit, ViewChild, Injectable } from "@angular/core";
import { NgForm, NgModel, Validators, FormControl } from "@angular/forms";

import { MatSnackBar, MatDialog, MatOption } from "@angular/material";
import { Priority } from "../../priority/priority";
import { PriorityService } from "../../priority/priority.service";

import { CategoryService } from "../../category/service/category.service";
import { environment } from "../../../environments/environment";
import { Vendor } from "../../vendor/vendor.model";
import { VendorService } from "../../vendor/Services/vendor.service";
import { DecimalPipe, DatePipe } from "@angular/common";
import { TdFileInputComponent, TdDialogService } from "@covalent/core";
import { BaseService } from "../../common/service/base.service";
import { forEach } from "@angular/router/src/utils/collection";
import { NgxGalleryImage, NgxGalleryLayout } from "ngx-gallery";
import { NgxGalleryOptions } from "ngx-gallery";
import { NgxGalleryAnimation } from "ngx-gallery";
import { NgxGalleryComponent } from "ngx-gallery";
import { HttpErrorResponse } from "@angular/common/http";
import { User } from "../../auth/user.model";
import { AuthService } from "../../auth/service/auth.service";
import { Product } from "../product";
import { Manufacture } from "../../manufacture/manufacture";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Brand } from "../../brand/brand-model";
import { BrandService } from "../../brand/service/brand.service";
import { PosGroupManagementService } from "../../posgroupmanagement/service/pos-group-management.service";
import { PosGroupManagement } from "../../posgroupmanagement/pos-group-management.model";
import { ProductPosGroup } from "../product-pos-group.model";
import { ProductPosGroupService } from "../service/product-pos-group.service";

@Component({
  selector: "product-form",
  templateUrl: "./product-form.component.html",
  styleUrls: ["./product-form.component.css"]
})
@Injectable()
export class ProductForm extends BaseFormComponent implements OnInit {
  @ViewChild("productForm") productForm: NgForm;
  @ViewChild("ngxGallery") ngxGallery: NgxGalleryComponent;
  @ViewChild("allPosGroupSelected") private allPosGroupSelected: MatOption;

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[] = [];

  attImagesAsFiles: File[] = [];
  imageUrlsArray: string[] = [];

  product: Product;
  _isEdit: boolean = false;
  _isUserLoggedInVendor: boolean = false;
  loggedInVendor: Vendor;
  listOfPriorities: Priority[];
  listOfCategories: Category[];
  listOfVendors: Vendor[];
  _selectedParentCategory: Category = new Category();
  category: Category = new Category();
  vat: number = 0;
  defaultVate: number;
  currentProductPrice: number = 0;
  priceValue: string;
  priceValueFromEvent: number;
  retailPriceValueFormEvent: number;
  listOfStore: Store[] = [];
  listOfManufacture: Manufacture[] = [];
  listOfBrands: Brand[] = [];
  counter: number;
  vatIsNull: boolean = false;
  counterVatChanges: number = 0;
  currentProductRetailPrice: number = 0;
  posGroupProductFormControl: FormControl = new FormControl();
  posGroups: PosGroupManagement[] = [];
  productPosGroupTempList: any[] = [];
  constructor(
    protected productService: ProductService,
    protected route: ActivatedRoute,
    protected authService: AuthService,
    protected router: Router,
    private datePipe: DatePipe,
    protected snackBar: MatSnackBar,
    private priorityService: PriorityService,
    private categoryService: CategoryService,
    private vendorService: VendorService,
    private brandService: BrandService,
    protected posGroupService: PosGroupManagementService,
    private vendorAdminService: VendorAdminService,
    protected productPosGroupService: ProductPosGroupService,
    protected dialogService: TdDialogService
  ) {
    super(productService, route, router, snackBar);
  }

  ngOnInit() {
    if (this.route.snapshot.queryParams["edit"] == "1") {
      this.isEdit = true;
      this.product = this.productService.product;
      this.initForm(this.product);
      this.initStoreAndManufacture();
      if (this.product.expiryDate != undefined) {
        this.product.expiryDate = new Date(
          this.datePipe.transform(this.product.expiryDate, "mediumDate")
        );
      }
    } else {
      this.product = new Product();
      this.posGroupProductFormControl = new FormControl();
      this.product.category = this._selectedParentCategory;
      this.initStoreAndManufacture();
      this.counter = 0;
    }

    this.loadVendorOptions();

    this.loadCategoryOptions();

    this.loadPriorityOptions();

    this.fillImages();

    this.initGalleryOptions();

    this.loadStore();
    this.loadManufacturesOptions();
    this.loadBrand();
    this.posGroupProductFormControl.setValidators(Validators.required);

    this.categoryService
      .find(this.route.snapshot.queryParams["categoryId"])
      .subscribe(data => {
        if (data != null) {
          this._selectedParentCategory.id = data["id"];
          this._selectedParentCategory.name = data["name"];
          this._selectedParentCategory.description = data["description"];
          this._selectedParentCategory.parent = data["parent"];
        }
      });
    this.category.parent = this._selectedParentCategory;
  }

  initForm(product: Product) {
    if (this.product.manufacture == undefined) {
      this.product.manufacture = new Manufacture();
    }

    if (this.product.store == undefined) {
      this.product.store = new Store();
    }
    if (this.product.brand == undefined) {
      this.product.brand = new Brand();
    }
  }

  initStoreAndManufacture() {
    if (this.product.manufacture == null) {
      this.product.manufacture = new Manufacture();
    }

    if (this.product.store == null) {
      this.product.store = new Store();
    }
  }

  getSelectedPosGroupsForProduct(productId: number) {
    this.posGroupService.getPosGroupForProduct(productId).subscribe(data => {
      this.product.productPosGroup = data;
      this.posGroupProductFormControl.setValue(data);
      this.productPosGroupTempList = this.product.productPosGroup;
    });
  }

  onPriceChange(event) {
    if (String(event).length == 0) {
      this.product.price = Number.NaN;
      this.currentProductPrice = this.product.price;
    } else {
      this.product.price = parseFloat(event);
      this.currentProductPrice = parseFloat(event);
      this.priceValueFromEvent = this.calculateNetPrice(
        this.currentProductPrice,
        this.vat
      );
      this.priceValueFromEvent =
        parseFloat(Math.round(this.priceValueFromEvent * 100).toFixed(2)) / 100;
    }
  }

  onVatChange(event: any) {
    this.product.vat = event.target.value;
    if (event.target.value == "") {
      this.product.vat = Number.NaN;
      this.vatIsNull = true;
    } else {
      this.vatIsNull = false;
      this.vat = event.target.value;
      this.priceValueFromEvent = this.calculateNetPrice(
        this.currentProductPrice,
        this.vat
      );
      this.priceValueFromEvent =
        parseFloat(Math.round(this.priceValueFromEvent * 100).toFixed(2)) / 100;
      if (
        this.product.retailPrice != null ||
        this.product.retailPrice != undefined
      ) {
        this.retailPriceValueFormEvent = this.calculateRetailPriceWithVat(
          this.currentProductRetailPrice,
          this.vat
        );
        this.retailPriceValueFormEvent =
          parseFloat(
            Math.round(this.retailPriceValueFormEvent * 100).toFixed(2)
          ) / 100;
      }
    }
  }

  onRetailPriceChange(event) {
    if (String(event).length == 0) {
      event = null;
      this.product.retailPrice = null;
      this.retailPriceValueFormEvent = this.calculateRetailPriceWithVat(
        0,
        this.vat
      );
      this.retailPriceValueFormEvent =
        parseFloat(
          Math.round(this.retailPriceValueFormEvent * 100).toFixed(2)
        ) / 100;
    } else {
      this.product.retailPrice = parseFloat(event);
      this.currentProductRetailPrice = parseFloat(event);
      this.retailPriceValueFormEvent = this.calculateRetailPriceWithVat(
        this.currentProductRetailPrice,
        this.vat
      );
      this.retailPriceValueFormEvent =
        parseFloat(
          Math.round(this.retailPriceValueFormEvent * 100).toFixed(2)
        ) / 100;
    }
  }

  onAfterSaveAndNewWithAttachments() {
    if (this.product.manufacture == null) {
      this.product.manufacture = new Manufacture();
    }
    if (this.product.store == null) {
      this.product.store = new Store();
    }
  }

  calculateNetPrice(price: number, vat: number) {
    return (price * vat) / 100 + price;
  }

  calculateRetailPriceWithVat(retailPrice: number, vat: number) {
    return (retailPrice * vat) / 100 + retailPrice;
  }

  getVatRatio(price: number, vat: number) {
    return (price * vat) / 100;
  }

  initGalleryOptions() {
    this.galleryOptions = [
      {
        previewInfinityMove: true,
        imageArrowsAutoHide: true,
        imageInfinityMove: true,
        previewCloseOnEsc: true,
        previewKeyboardNavigation: true,
        previewZoom: true,
        width: "500px",
        height: "300px",
        thumbnailsColumns: 5,
        imageAnimation: NgxGalleryAnimation.Slide,
        layout: NgxGalleryLayout.ThumbnailsTop,
        imageSize: "contain"
      },
      // max-width 800
      {
        breakpoint: 200,
        width: "500px",
        height: "300px",
        imagePercent: 80,
        thumbnailsPercent: 30,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];
  }

  options: any = {
    spellChecker: true,
    showIcons: ["code", "table"]
  };

  fillImages() {
    if (this._isEdit) {
      let imageNamesArr = this.product.imagesName.split(";");

      imageNamesArr.forEach(imageName => {
        let imgURL = this.productService.getAttachmentUrl(imageName);
        this.imageUrlsArray.push(imgURL);

        this.productService.getAttachment(imgURL).subscribe((blob: Blob) => {
          let fileReader = new FileReader();
          fileReader.onload = () => {
            this.attImagesAsFiles.push(
              new File([fileReader.result], imageName)
            );
          };
          let img = new Image();
          img.src = imgURL;

          this.galleryImages.push({
            small: imgURL,
            medium: imgURL,
            big: imgURL
          });
          if (blob) {
            fileReader.readAsArrayBuffer(blob);
          }
        });
      });
    }
  }

  initGalleryImages() {
    for (let i = 0; i < this.imageUrlsArray.length; i++) {
      this.galleryImages.push({
        small: this.imageUrlsArray[i],
        medium: this.imageUrlsArray[i],
        big: this.imageUrlsArray[i]
      });
    }
  }

  selectPosGroupPerOne(all) {
    if (this.allPosGroupSelected.selected) {
      this.allPosGroupSelected.deselect();
      return false;
    }

    if (this.posGroupProductFormControl.value.length == this.posGroups.length) {
      this.allPosGroupSelected.select();
    }
  }

  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): Product {
    return this.product;
  }

  set model(product: Product) {
    this.product = product;
  }

  initModel(): Product {
    return new Product();
  }

  ngFormInstance(): NgForm {
    return this.productForm;
  }

  loadStore() {
    let loggedInUser = this.authService.getCurrentLoggedInUser();
    let vendorId;
    if (loggedInUser.id != undefined) {
      if (loggedInUser.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
        this.vendorAdminService
          .getVendorByVendorAdminId(loggedInUser.id)
          .subscribe(data => {
            vendorId = data;
            this.productService
              .getStoresByVendorId(vendorId)
              .subscribe(data => {
                this.listOfStore = data;
              });
          });
      } else {
        this.productService.getAllStore().subscribe(data => {
          this.listOfStore = data["content"];
        });
      }
    }
  }

  /*onChangeStore(event: any){
    if(event.value != undefined){
      this.productService.getManufacutreByStoreId(event.value).subscribe(data=>{
        this.listOfManufacture=data;
      })
    }else{
      this.listOfManufacture=[];
    }
  }*/

  loadManufacturesOptions() {
    let loggedInUser = this.authService.getCurrentLoggedInUser();
    let vendorId;
    if (loggedInUser.id != undefined) {
      if (
        loggedInUser.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin
      ) {
        this.vendorAdminService
          .getVendorByVendorAdminId(loggedInUser.id)
          .subscribe(data => {
            vendorId = data;
            this.productService
              .getManufaturesByVendorId(vendorId)
              .subscribe(data => {
                this.listOfManufacture = data;
              });
          });
      } else {
        this.productService.getAllManufactures().subscribe(data => {
          this.listOfManufacture = data["content"];
        });
      }
    }
  }

  loadVendorOptions() {
    let loggedInUser: User = this.authService.getCurrentLoggedInUser();

    if (loggedInUser != undefined) {
      if (loggedInUser.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
        this.vendorService
          .filterNotPageable(
            "",
            "",
            "",
            "/vendor_by_vendor_admin_id/" + loggedInUser.id
          )
          .subscribe((data: Vendor) => {
            data => (this.loggedInVendor = data);
            this.loggedInVendor = new Vendor(
              data["id"],
              data["name"],
              data["vat"]
            );
            if (this.product.vat == null) {
              this.vat = data["vat"];
              this.product.vat = this.vat;
              this.defaultVate = data["vat"];
            }
            if (this.product.vat != null && this.isEdit == true) {
              this.vat = this.product.vat;
            }
            this.priceValueFromEvent = this.product.price;
            this.retailPriceValueFormEvent = this.product.retailPrice;
            // calculate the orgin price from the price with vat
            this.product.price =
              Math.round((this.product.price / (1 + this.vat / 100)) * 100) /
              100;
            this.currentProductPrice = this.product.price;
            if (this.product.retailPrice != null) {
              this.product.retailPrice =
                Math.round(
                  (this.product.retailPrice / (1 + this.vat / 100)) * 100
                ) / 100;
              this.currentProductRetailPrice = this.product.retailPrice;
            }
            this.onCompleteLoadingVendor(this.loggedInVendor);
          });
      } else if (
        loggedInUser.type.id == environment.ENUM_ID_USER_TYPE.TelephoenicAdmin
      ) {
        this.vendorService
          .filterNotPageable("", "", "", "/vendor_active_list")
          .subscribe((data: Vendor[]) => {
            this.listOfVendors = data;
          });
      }
    }
  }

  loadBrand() {
    let loggedInUser = this.authService.getCurrentLoggedInUser();
    let vendorId;
    if (loggedInUser.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
      this.vendorAdminService
        .getVendorByVendorAdminId(loggedInUser.id)
        .subscribe(data => {
          vendorId = data;
          this.productService.getBrandsByVendorId(vendorId).subscribe(data => {
            this.listOfBrands = data;
          });
        });
    }
  }

  onCompleteLoadingVendor(loggedInVendor: Vendor) {
    if (loggedInVendor == undefined) {
      this._isUserLoggedInVendor = false;
      this.vendorService
        .filter(0, environment.maxListFieldOptions, "", "", "")
        .subscribe(data => (this.listOfVendors = data["content"]));
    } else {
      this._isUserLoggedInVendor = true;
      this.product.vendor = loggedInVendor;
    }

    this.posGroupService.getGroupsByVednor(loggedInVendor.id).subscribe(
      data => {
        this.posGroups = data;
      },
      error => {
        console.log(error);
      },
      () => {
        this.getSelectedPosGroupsForProduct(this.product.id);
      }
    );
  }

  //we should not return all categories, but leafs only.
  loadPriorityOptions() {
    this.priorityService
      .filter(0, environment.maxListFieldOptions, "", "", "")
      .subscribe(data => (this.listOfPriorities = data["content"]));
  }

  public optionsGroup = function(option, value): boolean {
    if (value != null) {
      return option.id === value.id;
    }
  };

  loadCategoryOptions() {
    let user: User = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService
      .getVendorByVendorAdminId(user.id)
      .subscribe(vendorId => {
        this.categoryService.findLeaveCategory(vendorId).subscribe(data => {
          this.listOfCategories = data;
        });

        //this.categoryService.findByExample(this.getLeafCategoriesInstance()).subscribe(
        //TODO remove static 1000 data until solve paging problem
        // this.categoryService.findByExamplePageable(this.getLeafCategoriesInstance(), 0, 1000).subscribe(
        //     data => {

        //     this.listOfCategories = data["content"]
        //     console.log(data["content"]);
        //   }

        // );
      });
  }

  getLeafCategoriesInstance() {
    let category = new Category();
    category.hasChilds = false;
    return category;
  }

  onBeforeSaveAndNewWithAttachments(product: Product, files: File | FileList) {
    this.showSpinnerOnSaveAndNew = true;
    this.validateExpiryDate();
    this.validateSelectedImages();
    this.validateCommission(product);
    //product.price = this.priceValueFromEvent;
    if (this.isUserLoggedInVendor) {
      product.vendor = this.loggedInVendor;
    }
    this.resetOptionalObjects(product);
    this.preparePosGroup();
  }

  onBeforeEditWithAttachments(product: Product, files: File | FileList) {
    this.showSpinner = true;
    this.validateExpiryDate();
    this.validateSelectedImages();
    this.validateCommission(product);
    this.preparePosGroup();
  }

  onBeforeSaveWithAttachments(product: Product, files: File | FileList) {
    this.showSpinner = true;
    this.validateExpiryDate();
    this.validateSelectedImages();
    this.validateCommission(product);
    this.resetOptionalObjects(product);
    this.preparePosGroup();
  }

  resetOptionalObjects(product: Product) {
    if (product.manufacture.id == undefined && !this.isEdit) {
      product.manufacture = new Manufacture();
    }

    if (product.store.id == undefined && !this.isEdit) {
      product.store = new Store();
    }
  }

  preparePosGroup() { 
    if (this.isEdit) {
      this.product.productPosGroup = [];
    }
    this.posGroupProductFormControl.value.forEach(posGroup => {
      let productPosGroup = new ProductPosGroup();
      productPosGroup.posGroup = posGroup;
      productPosGroup.product = null;
      this.product.productPosGroup.push(productPosGroup);
    });
    
  }

  onBeforeSave(product: Product) {
    this.showSpinner = true;
    this.validateExpiryDate();
    this.validateSelectedImages();
    this.validateCommission(product);
  }

  onBeforeSaveAndNew(product: Product) {
    this.showSpinnerOnSaveAndNew = true;
    this.validateExpiryDate();
    this.validateSelectedImages();
    this.validateCommission(product);
  }

  onBeforeEdit(product: Product) {
    this.showSpinner = true;
    this.validateExpiryDate();
    this.validateSelectedImages();
    this.validateCommission(product);
  }

  validateExpiryDate() {
    if (
      this.product.expiryDate != undefined &&
      !this.validateDateWithCurrent(this.product.expiryDate)
    ) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("Expiry date must be in the future.", "Close", {
        duration: 3000
      });
      throw new Error("Expiry date must be in the future.");
    }
  }

  validateSelectedImages() {
    if (this.attImagesAsFiles == null || this.attImagesAsFiles.length == 0) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open(
        "At least one image required for the product.",
        "Close",
        { duration: 3000 }
      );
      throw new Error("At least one image required for the product");
    }
  }

  validateCommission(product: Product) {
    if (product.commission != null) {
      if (product.commission < 0 || product.commission > 100) {
        this.snackBar.open(
          "Please enter a value between 0 and 100 for Commission field",
          "Close",
          { duration: 3000 }
        );
        throw new RangeError("commission is out of range (0, 100)");
      }
    }
  }

  onSelectImages(selectedImage: File | FileList) {
    if (selectedImage instanceof File) {
      if (selectedImage.type.startsWith("image/")) {
        this.attImagesAsFiles.push(selectedImage);

        let fileReader = new FileReader();
        fileReader.onload = () => {
          let image = new Image();
          image.src = fileReader.result;

          this.ngxGallery.images.push({
            small: image.src,
            medium: image.src,
            big: image.src
          });
        };
        fileReader.readAsDataURL(selectedImage);
      } else {
        this.snackBar.open(
          "File type not allowed (" +
            selectedImage.type +
            "). Images allowed only.",
          "Close",
          { duration: 4000 }
        );
      }
    } else {
      let isAllAttachmentsImages: boolean = true;

      for (let i: number = 0; i < selectedImage.length; i++) {
        if (!selectedImage[i].type.startsWith("image/")) {
          isAllAttachmentsImages = false;
        }
      }

      if (isAllAttachmentsImages) {
        for (let i: number = 0; i < selectedImage.length; i++) {
          this.attImagesAsFiles.push(selectedImage[i]);

          let fileReader = new FileReader();
          fileReader.onload = () => {
            let image = new Image();
            image.src = fileReader.result;

            this.ngxGallery.images.push({
              small: image.src,
              medium: image.src,
              big: image.src
            });
          };
          fileReader.readAsDataURL(selectedImage[i]);
        }
      } else {
        this.snackBar.open(
          "Non images file type(s) detected. Please select images only.",
          "Close",
          { duration: 4000 }
        );
      }
    }
  }

  onSaveAndNewWithAttachmentsCompleted() {
    this.resetAttachmentsFields();
    if (this.isUserLoggedInVendor) {
      this.product.vendor = this.loggedInVendor;
    }
    this.product.category = this.category.parent;
    this.priceValueFromEvent = 0;
    this.ngFormInstance().controls["vat"].setValue(this.defaultVate);
    this.ngFormInstance().controls["category"].setValue(
      this.product.category.id
    );
  }

  onSaveWithAttachmentsCompleted() {
    this.router.navigate(["product-category"], {
      queryParams: { categoryId: this.route.snapshot.queryParams["categoryId"] }
    });
    this.resetAttachmentsFields();
  }
  onCancel() {
    this.router.navigate(["product-category"], {
      queryParams: { categoryId: this.route.snapshot.queryParams["categoryId"] }
    });
  }

  resetAttachmentsFields() {
    this.attImagesAsFiles = [];
    this.ngxGallery.images = [];
    this.posGroupProductFormControl =  new FormControl();
  }

  onDeleteImage() {
    this.dialogService
      .openConfirm({
        message: "Are you sure you want to delete the selected image?",
        title: "Confirmation",
        cancelButton: "Disagree",
        acceptButton: "Agree"
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.attImagesAsFiles.splice(this.ngxGallery.selectedIndex, 1);
          this.ngxGallery.images.splice(this.ngxGallery.selectedIndex, 1);
        }
      });
  }

  onChange(event: any): void {
    event.srcElement.value = "";
  }

  onSaveWithAttachmentsError(errorResponse: HttpErrorResponse) {
    this.snackBar.open(errorResponse.error.message, "Close", {
      duration: 5000
    });
  }

  onSaveAndNewWithAttachmentsError(errorResponse: HttpErrorResponse) {
    this.snackBar.open(errorResponse.error.message, "Close", {
      duration: 5000
    });
  }

  onEditWithAttachmentsError(errorResponse: HttpErrorResponse) {
    this.snackBar.open(errorResponse.error.message, "Close", {
      duration: 5000
    });
  }

  pushAttachment(event) {
    this.attImagesAsFiles.push(event);
  }

  get isUserLoggedInVendor(): boolean {
    return this._isUserLoggedInVendor;
  }

  set isUserLoggedInVendor(isUserLoggedInVendor: boolean) {
    this._isUserLoggedInVendor = isUserLoggedInVendor;
  }

  validateDateWithCurrent(date: Date): boolean {
    let tempCurrentDate: Date = new Date();
    tempCurrentDate.setHours(0);
    tempCurrentDate.setMinutes(0);
    tempCurrentDate.setSeconds(0);
    tempCurrentDate.setMilliseconds(0);

    if (date < tempCurrentDate) {
      return false;
    }
    return true;
  }
}

export const MY_FORMATS = {
  parse: {
    dateInput: "MM/YYYY"
  },
  display: {
    dateInput: "MM/YYYY",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY"
  }
};
