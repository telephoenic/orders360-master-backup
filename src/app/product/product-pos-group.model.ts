import { Region } from "../location/region/Region.model";
import { Product } from "./product";
import { PosGroupManagement } from "../posgroupmanagement/pos-group-management.model";

export class ProductPosGroup {
    id: number;
    inactive: boolean;
    product:Product;
    posGroup:PosGroupManagement;
    constructor(id?: number,
        inactive?: boolean,
        product?:Product,
        posGroup?:PosGroupManagement
    ) {
        this.id = id;
        this.inactive = inactive;
        this.product = product;
        this.posGroup = posGroup;
    }
}