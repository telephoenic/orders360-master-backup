import {BaseModel} from '../common/model/base-model';
import { Category } from '../category/category';
import { Priority } from '../priority/priority';
import { Vendor } from '../vendor/vendor.model';
import { Manufacture } from '../manufacture/manufacture';
import { Store } from '../store/store';
import { DiscountProductsPosGroups } from '../discount/model/discount-products-posgroups.model';
import { Brand } from '../brand/brand-model';
import { ProductPosGroup } from './product-pos-group.model';


export class Product extends BaseModel {
    id:number;
    inactive:boolean;
    name:string;
    alternativeName:string;
    barcode:string;
    itemCode:string;
    expiryDate:Date; 
    price:number;
    commission:number;
    description:number;
    category:Category;
    vat:number;
    vendor:Vendor;
    priority:Priority;
    imagesName:string;
    retailPrice:number;
    manufacture: Manufacture;
    store: Store;
    brand: Brand;
    productPosGroup:ProductPosGroup[] = [];
    constructor() {
        super();
        this.category = new Category();
        this.priority = new Priority();
        this.vendor = new Vendor();
        this.manufacture = new Manufacture();
        this.store = new Store();
        this.brand = new Brand();
    }
}