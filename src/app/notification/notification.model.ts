import { Vendor } from "../vendor/vendor.model";
import { BaseModel } from "../common/model/base-model";
import { Module } from "../structure/module/module";
import { FunctionNotification } from "./notification-function.model";

export class NotificationModel  extends BaseModel{
    
    id: number;
    name: string;
    body: string;
    functionNotification:FunctionNotification;
    vendor:Vendor;
    inactive:boolean;
    
    constructor (){
        super();
        this.vendor = new Vendor();
        this.functionNotification = new FunctionNotification();
    }

}