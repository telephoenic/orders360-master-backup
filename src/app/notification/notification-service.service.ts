import { BaseService } from "../common/service/base.service";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../auth/service/auth.service";
import { BaseModel } from "../common/model/base-model";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { DatePipe } from "@angular/common";
import { NotificationModel } from "./notification.model";

@Injectable()
export class NotificationService extends BaseService {
  notification: NotificationModel;
  navigateToAfterCompletion: string;

  constructor(
    protected http: HttpClient,
    protected authService: AuthService,
    private datePipe: DatePipe
  ) {
    super(http, "notification", authService);
  }

  getNotifications(
    vendorId: number,
    pageNumber: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string
  ): Observable<any> {
    return this.http.get(
      environment.baseURL +
        "notification/notifications/" +
        pageNumber +
        "/" +
        pageSize +
        "/" +
        vendorId,
      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      }
    );
  }

  getNotificationFunctions(moduleId: number): Observable<any> {
    return this.http.get(
      environment.baseURL +
        "function-notifcation/get-functions-by-module-id/" +
        moduleId
    );
  }

  getCountUnreadNotifications(userId: number):Observable<any> {
    return this.http.get(
      environment.baseURL +
        "user-notification/count-notifications-for-user/" +
        userId
    );
  }

  getLastFourNotificationsForUser(
    userId: number): Observable<any> {
    return this.http.get(
      environment.baseURL +
        "notification/get-last-four-notifications/" +
        userId);
  }

  getAllNotifications(
    userId: number,
    pageNumber: number,
    pageSize: number,
    startDate: any,
    endDate: any,
    modules: string[]
  ) {
    if(startDate == null){
      startDate = this.datePipe.transform(new Date('1970-04-01').getDate(), 'yyyy-MM-dd');
    }
    if(endDate == null){
      endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    }
    if(modules == null){
      modules = [];
    }

    let parameterList: { paramName: string; paramValue: any }[] = [];
    parameterList.push({ paramName: "startDate", paramValue: startDate });
    parameterList.push({ paramName: "endDate", paramValue: endDate });
    parameterList.push({ paramName: "modules", paramValue: modules });

    return this.http.get(
      environment.baseURL +
        "notification/get-all-notification/" +
        userId +
        "/" +
        pageNumber +
        "/" +
        pageSize,
      {
        params: this.prepareSearchParameters("", "", "", (parameterList)),
        headers: this.getBasicHeaders()
      }
    );
  }

  get model(): BaseModel {
    return this.notification;
  }
  set model(model: BaseModel) {
    this.notification = <NotificationModel>model;
  }
}
