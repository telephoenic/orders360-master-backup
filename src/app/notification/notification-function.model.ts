import { BaseModel } from "../common/model/base-model";
import { Module } from "../structure/module/module";

export class FunctionNotification  extends BaseModel{
    
    id: number;
    name: string;
    inactive:boolean;
    module: Module;

    constructor (id?:number,name?:string,inactive?:boolean){
        super();
        this.id = id;
        this.name = name;
        this.inactive = inactive;
        this.module = new Module();
    }

}