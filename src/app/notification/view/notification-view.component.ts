import { BaseViewComponent } from "../../common/view/base-view-component";
import { OnInit, Component, ViewChild } from "@angular/core";
import {
  TdPagingBarComponent,
  TdDataTableComponent,
  TdDataTableSortingOrder,
  ITdDataTableColumn,
  TdDialogService
} from "@covalent/core";
import { NotificationService } from "../notification-service.service";
import { AuthService } from "../../auth/service/auth.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { MatSnackBar } from "@angular/material";
import { NgxPermissionsService } from "ngx-permissions";
import { environment } from "../../../environments/environment";
import { NotificationModel } from "../notification.model";

@Component({
  selector: "app-all-notifications-view",
  templateUrl: "./notification-view.component.html",
  styleUrls: ["./notification-view.component.css"]
})
export class NotificationViewComponent extends BaseViewComponent
  implements OnInit {
  private _allNotifiactions: NotificationModel[];
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private _errors: any = "";
  private _selectedRows: NotificationModel[] = [];
  private _pageSize: number = 50;
  private _filteredTotal: number;
  private _sortBy: string = "";
  private _sortOrder: TdDataTableSortingOrder =
    TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = "";

  @ViewChild("pagingBar") pagingBar: TdPagingBarComponent;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;

  ngOnInit() {
    super.ngOnInit();
  }

  constructor(
    protected notificationService: NotificationService,
    protected authService: AuthService,
    protected vendorAdminService: VendorAdminService,
    protected datePipe: DatePipe,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    protected permissionsService: NgxPermissionsService
  ) {
    super(notificationService, router, dialogService, snackBar);
  }

  columnsConfig: ITdDataTableColumn[] = [
    { name: "name", label: "Name", tooltip: "Name" },
    { name: "functionNotification.module.name", label: "Module", tooltip: "Module", width: 100 },
    {
      name: "inactive",
      label: "Active/Inactive",
      tooltip: "Active or Inactive",
      width: 100
    },
    { name: "id", label: "Edit", tooltip: "Edit", sortable: false, width: 100 }
  ];

  filterByVendor(
    pageNumber: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    vendorId: number
  ) {
    this.getNotifications();
  }

  getNotifications() {
    let currentUser = this.vendorAdminService.getCurrentLoggedInUser();
    let vendorId;

    if (currentUser.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin) {
      this.vendorAdminService
        .getVendorByVendorAdminId(currentUser.id)
        .subscribe(result => {
          vendorId = result;
          this.notificationService
            .getNotifications(
              vendorId,
              this.currentPage - 1,
              this.pageSize,
              this.searchTerm,
              this.sortBy,
              this.sortOrder
            )
            .subscribe(data => {
              this.allModels = data["content"];
              this.allModels.forEach(notification =>{
                  if(notification.functionNotification.module.name == "Role Support Ticket"){
                      notification.functionNotification.module.name = "Support Ticket";
                  }
              })
              this.filteredTotal = data["totalElements"];
            });
        });
    }
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get allModels(): NotificationModel[] {
    return this._allNotifiactions;
  }

  public set allModels(value: NotificationModel[]) {
    this._allNotifiactions = value;
  }

  public get errors(): any {
    return this._errors;
  }

  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): NotificationModel[] {
    return this._selectedRows;
  }

  public set selectedRows(value: NotificationModel[]) {
    this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }

  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }

  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }
  public get searchTerm(): string {
    return this._searchTerm;
  }

  public set searchTerm(value: string) {
    this._searchTerm = value;
  }
}
