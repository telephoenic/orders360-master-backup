import { Component, OnInit, ViewChild } from "@angular/core";
import { BaseFormComponent } from "../../common/form/base-form-component";
import { BaseModel } from "../../common/model/base-model";
import { NgForm } from "@angular/forms";
import { NotificationService } from "../notification-service.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { AuthService } from "../../auth/service/auth.service";
import { ActivatedRoute, Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { TdDialogService } from "@covalent/core";
import { MatSnackBar } from "@angular/material";
import { VendorService } from "../../vendor/Services/vendor.service";
import { ModuleService } from "../../structure/module/module.service";
import { Vendor } from "../../vendor/vendor.model";
import { FunctionNotification } from "../notification-function.model";
import { NotificationModel } from "../notification.model";
import { TdTextEditorComponent } from "@covalent/text-editor";

@Component({
  selector: "app-notification-form",
  templateUrl: "./notification-form.component.html",
  styleUrls: ["./notification-form.component.css"]
})
export class NotificationFormComponent extends BaseFormComponent
  implements OnInit {
  @ViewChild("notificationForm") notificationForm: NgForm;
  @ViewChild("bodyTE") bodyTE:TdTextEditorComponent;

  ngOnInit() {
    this.getVendorIdByLoggedVenodrAdmin();
    if (this.route.snapshot.queryParams["edit"] == "1") {
      this._isEdit = true;
      this._notifiaction = this.notificationService.notification;
      this.notificationService
        .getNotificationFunctions(
          this._notifiaction.functionNotification.module.id
        )
        .subscribe(data => {
          this.listOfFunctions = data;
        });
    } else {
      this._notifiaction = new NotificationModel();
    }
  }
  user: any;
  _vendorId: number;
  private _isEdit: boolean;
  private _notifiaction: NotificationModel;
  listOfModules: any[] = [];
  listOfFunctions: any[] = [];
  options: any = {
    spellChecker: true,
    showIcons: ["code", "table"]
  };

  constructor(
    protected notificationService: NotificationService,
    protected vendorAdminService: VendorAdminService,
    protected authService: AuthService,
    protected route: ActivatedRoute,
    protected router: Router,
    private datePipe: DatePipe,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    protected vendorService: VendorService,
    protected moduleService: ModuleService
  ) {
    super(notificationService, route, router, snackBar);
  }

  getVendorIdByLoggedVenodrAdmin() {
    this.user = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService
      .getVendorByVendorAdminId(this.user.id)
      .subscribe((vendorId: number) => {
        console.log(vendorId);
        this._vendorId = vendorId;
        this.vendorService
          .findVendorById(this._vendorId)
          .subscribe((data: Vendor) => {
            this._notifiaction.vendor = data;
            this.loadModules();
          });
      });
  }

  onModuleChange(event) {
    let moduelId;
    if (event.value === undefined) {
      moduelId = this._notifiaction.functionNotification.module.id;
    } else {
      moduelId = event.value;
    }
    console.log(this._notifiaction.functionNotification.name);
    this._notifiaction.functionNotification.id = null;
    this.notificationService
      .getNotificationFunctions(moduelId)
      .subscribe(data => {
        this.listOfFunctions = data;
      });
  }

  onFunctionChange(event) {
    this._notifiaction.functionNotification = new FunctionNotification();
    this._notifiaction.functionNotification.id = event.value;
    this.listOfFunctions.forEach(functionNotification => {
      if (
        functionNotification.id == this._notifiaction.functionNotification.id
      ) {
        this._notifiaction.functionNotification = functionNotification;
      }
    });
  }

  notifcationNameChange(event) {
    this._notifiaction.name = event.target.value.trim();
  }

  resetAfterSaveNew() {
    this._notifiaction = new NotificationModel();
  }

  onSaveAndNewCompleted() {
    super.onSaveAndNewCompleted();
    this._notifiaction = new NotificationModel();
    this.getVendorIdByLoggedVenodrAdmin();
  }

  loadModules() {
    this.moduleService.getModules().subscribe(data => {
      this.listOfModules = data;
      this.listOfModules.forEach(module => {
        if (module.name == "Role Support Ticket") {
          module.name = "Support Ticket";
        }
      });
    });
  }

  onSaveCompleted() {
    super.onSaveCompleted();
    this.snackBar.open("Notification Setup saved successfully", "close", {
      duration: 3000
    });
  }

  onBeforeSave(notification: NotificationModel) {
    super.onBeforeSave(notification);
    this.validationNotificationBody(notification);
  }

  onBeforeSaveAndNew(notification: NotificationModel) {
    super.onBeforeSaveAndNew(notification);
    this.validationNotificationBody(notification);
  }

  onBeforeEdit(notification: NotificationModel) {
    super.onBeforeEdit(notification);
    this.validationNotificationBody(notification);
  }

  validationNotificationBody(notification: NotificationModel) {
    if (notification.body.length < 5) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("Body must be at least 5 characters", "close", {
        duration: 3000
      });
      throw new Error("Body must be at least 5 characters");
    }

    if (notification.body.length > 200) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("Max text length allowed is 200 characters", "close", {
        duration: 3000
      });
      throw new Error("Max text length allowed is 200 characters");
    }
  }

  get isEdit(): boolean {
    return this._isEdit;
  }
  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
  get model(): BaseModel {
    return this._notifiaction;
  }
  set model(baseModel: BaseModel) {
    this._notifiaction = <NotificationModel>baseModel;
  }
  initModel(): BaseModel {
    return new NotificationModel();
  }
  ngFormInstance(): NgForm {
    return this.notificationForm;
  }
}
