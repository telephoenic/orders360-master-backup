import { Vendor } from "../vendor/vendor.model";
import { BaseModel } from "../common/model/base-model";
import { Module } from "../structure/module/module";
import { FunctionNotification } from "./notification-function.model";
import { NotificationModel } from "./notification.model";

export class UserNotification  extends BaseModel{
    
    id: number;
    notification: NotificationModel;
    status: boolean;
    inactive:boolean;
    
    constructor (){
        super();
        this.notification = new NotificationModel();
    }

}