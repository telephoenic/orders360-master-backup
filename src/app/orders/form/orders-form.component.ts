import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {FormControl, Validators, NgForm} from '@angular/forms';
import {Address} from '../../common/address.model';
import {MatSnackBar} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { AllOrderItemsComponent } from '../view/view-details/view-order-details.component';
import { Order } from '../order.model';
import { OrderService } from '../service/order.service';




@Component({
  selector: 'app-orders',
  templateUrl: './orders-form.component.html',
  styleUrls: ['./orders-form.component.css']
})
export class OrdersFormComponent extends BaseFormComponent implements OnInit {


  @ViewChild("orderForm") orderForm : NgForm;
  @ViewChild(AllOrderItemsComponent) allOrderItemsComponent: AllOrderItemsComponent;
  _order : Order;
  _isEdit:boolean = false;

	constructor(
    protected orderService: OrderService,
    protected router:Router,
    protected route:ActivatedRoute,
    protected snackBar:MatSnackBar) { 
      super(orderService, 
        route,
        router,
        snackBar);
}

  ngOnInit() {
    this._order = this.orderService.order;
  }
  
  // onSave(){
  //   console.log("Helloo Items !!");
  //   this.allOrderItemsComponent.onSave();
  //   super.onSave();
  // }

  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
  
  get model() : Order {
    return this._order;
  }

  set model(vendor:Order){
    this._order = vendor;
  }
  
  initModel(): Order {
    return new Order();
  }

  ngFormInstance(): NgForm {
    return this.orderForm;
  }

}

