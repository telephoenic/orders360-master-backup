import { Product } from "../product/product";
import { OrderItem } from "./order-item.model";
import { Warehouse } from "../warehouse/warehouse-model";


export class OrderItemHistory {

    id:number;
    inactive:boolean;

    orderItem:OrderItem;
    quantity:number;
    deliveryDate:Date;
    numberOfItems:number;
    totalAmount:number;
    promotionDetailId:number;
    remark:string;
    quantityToFulfilled:number;
    warehouse:Warehouse ;
    quantityToCanceled:number;

    constructor (){
        this.orderItem=new OrderItem();
        this.warehouse=new Warehouse();
    }
    

}