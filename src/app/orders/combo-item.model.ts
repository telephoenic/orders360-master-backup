import { Combo } from './../combo/combo.model';

import { OrderItem } from "./order-item.model";


export class ComboItem extends OrderItem{

    id:number;
    inactive:boolean;

    combo:Combo;

    constructor ( ){
        super();
    }
    

}