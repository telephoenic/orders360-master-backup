import { Audit } from "../audit/audit.model";
import { BaseModel } from "../common/model/base-model";

export class OrderStatusType extends BaseModel{

    id: number;
    inactive: boolean;
    deleted: boolean;
    audit: Audit;
    name: string;
    code: string;
    parent: OrderStatusType;


    constructor(){
        super();
    }
}