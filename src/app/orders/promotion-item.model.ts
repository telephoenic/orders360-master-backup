import { Promotion } from './../promotion/model/promotion';
import { Product } from './../product/product';
import { OrderItem } from './order-item.model';



export class PromotionItem extends OrderItem {

    id:number;
    inactive:boolean;
    private _totalAmount: number;
    private _promotionDetail: String;
    private _orderQuantity: number;
    private _product: Product;
    private _promotion: Promotion;
    private _freeItems: number;
    private _numberOfItems: number;
    private _quantity: number;
    private _promotionPrice: number;
    private _promotionDetailId: number;


    constructor(){
        super();
        this._promotion=new Promotion();
        this._product=new Product();
    }


    public get promotion(): Promotion {
        return this._promotion;
    }
    public set promotion(value: Promotion) {
        this._promotion = value;
    }
    public get product(): Product {
        return this._product;
    }
    public set product(value: Product) {
        this._product = value;
    }


    public get totalAmount(): number {
        return this._totalAmount;
    }
    public set totalAmount(value: number) {
        this._totalAmount = value;
    }

    public get promotionDetail(): String {
        return this._promotionDetail;
    }
    public set promotionDetail(value: String) {
        this._promotionDetail = value;
    }

    public get orderQuantity(): number {
        return this._orderQuantity;
    }
    public set orderQuantity(value: number) {
        this._orderQuantity = value;
    }
    
    public get freeItems(): number {
        return this._freeItems;
    }
    public set freeItems(value: number) {
        this._freeItems = value;
    }
    public get numberOfItems(): number {
        return this._numberOfItems;
    }
    public set numberOfItems(value: number) {
        this._numberOfItems = value;
    }

    public get quantity(): number {
        return this._quantity;
    }
    public set quantity(value: number) {
        this._quantity = value;
    }
  
    public get promotionPrice(): number {
        return this._promotionPrice;
    }
    public set promotionPrice(value: number) {
        this._promotionPrice = value;
    }
   
    public get promotionDetailId(): number {
        return this._promotionDetailId;
    }
    public set promotionDetailId(value: number) {
        this._promotionDetailId = value;
    }


}