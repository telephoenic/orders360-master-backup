import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { DatePipe } from '@angular/common';
import { RejectedOrder } from '../rejected-order';
import { BaseModel } from '../../common/model/base-model';

@Injectable({
  providedIn: 'root'
})
export class RejectedOrderService extends BaseService{

  rejectedOrder:RejectedOrder;

  constructor(http: HttpClient,
    protected authService: AuthService,
    private datePipe: DatePipe) {
    super(http, "rejected_order", authService)
  }


  get model() {
    return this.rejectedOrder;
  }
  set model(model: BaseModel) {
    this.rejectedOrder = <RejectedOrder>model;
  }
}
