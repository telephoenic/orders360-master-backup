import { BaseModel } from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { BaseService } from '../../common/service/base.service';
import { Order } from '../order.model';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import { start } from 'repl';
import { FcmService } from '../../../app/fcm/services/fcm.service';
import { PosUser } from '../../pos-user/pos-user.model';
import { OrderStatusType } from '../order-status-type';
import { OrderStatus } from '../enums/order-status.enum';
import { stat } from 'fs';


@Injectable()
export class OrderService extends BaseService {
  order: Order;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService,
    private datePipe: DatePipe,
    protected fcmService: FcmService) {
    super(http, "order", authService)
  }

  getAllOrdersByVendorId(pageNum: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    startDate,
    endDate,
    salesAgentId,
    orderStatus: String[]): Observable<any> {
    debugger
    // if (startDate == undefined || startDate == null) {
    //   startDate = this.datePipe.transform(new Date('1970-04-01').getDate(), 'yyyy-MM-dd');
    // }
    // if (endDate == undefined || endDate == null) {
    //   endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    // }
    let parameterList: { paramName: string, paramValue: any }[] = [];

    if (startDate != undefined || startDate != null) {
      parameterList.push({ paramName: "startDate", paramValue: startDate });
    }
    if (endDate != undefined || endDate != null) {
      parameterList.push({ paramName: "endDate", paramValue: endDate });
    }


    if (salesAgentId != null || salesAgentId != undefined) {
      parameterList.push({ paramName: "salesAgentId", paramValue: salesAgentId });
    }
    if (orderStatus != undefined) {
      parameterList.push({ paramName: "order_status", paramValue: orderStatus });
    }
    // parameterList.push({ paramName: "startDate", paramValue: startDate });
    // parameterList.push({ paramName: "endDate", paramValue: endDate });
    return this.http.get(environment.baseURL + "order/order_for_vendor/" + pageNum + "/" + pageSize + "/" + vendorId,
      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder, (parameterList)),
        headers: this.getBasicHeaders()
      });

  }

  onPrintOrder(row: Order): Observable<any> {
    return this.http.get(environment.baseURL + "report/generate_report/" + row.id);
  }

  public findOrderByIdOnly(orderId:number){
    return this.http.get(environment.baseURL + "order/get-order-id-only/"+orderId);
  }

  onGetOrderDetails(row: Order, status: string[]): Observable<any> {
    let params: HttpParams = new HttpParams();
    if (status != null && status.length > 0) {
      params = params.append("fulfilment_status", status.join(', '));
    }
    return this.http.get(environment.baseURL + "order/order_detail/" + row.id,
      {
        params: params,
        headers: this.getBasicHeaders()
      });
  }

  updateOrderStatus(status: string, orderIds: number[]) {
    return this.http.post(environment.baseURL + "order/update_statuses" + "/" + status,
      JSON.stringify(orderIds),
      { headers: this.getBasicHeaders() });
  }

  onUpdateOrderStatus(status: string, orderId: number) {
    return this.http.post(environment.baseURL + "order/update_status" + "/" + status,
      JSON.stringify(orderId),
      { headers: this.getBasicHeaders() });
  }

  onCalculateAndSaveLoyaltyTrxPoints(orders: Order[]) {
    return this.http.post(environment.baseURL + "order/all_points",
      JSON.stringify(orders),
      { headers: BaseService.HEADERS });
  }

  onCalculateAndSaveLoyaltyTrxPartialCompletePoints(orderId: number) {
    return this.http.post(environment.baseURL + "order/points",
      JSON.stringify(orderId),
      {
        headers: this.getBasicHeaders()
      });
  }

  public getOrderByOrderNumber(orderNumber: string) {
    let parameterList: { paramName: string; paramValue: any }[] = [];
    parameterList.push({ paramName: "order_number", paramValue: orderNumber });
    return this.http.get(
      environment.baseURL + "order/get-order-by-order-number",
      {
        params: this.prepareSearchParameters("", "", "", parameterList),
        headers: this.getBasicHeaders()
      }
    );
  }


  onSendNotification(order: Order, body: string) {
    let orderNumberSub: string[] = [];
    orderNumberSub = order.orderNumber.toString().split("/");
    let title: string = "Your order! " + orderNumberSub[0] + "-" + orderNumberSub[1];
    let posUsers: PosUser[] = [order.posUser];
    this.fcmService.onSend(posUsers, body, title).subscribe(
      send => { },
      err => { },
      () => {
        this.findOrderById(order.id, order.vendor.id).subscribe(
          data => { this.order = data },
          err => { },
          () => {

          },
        );
      }

    );
  }

  checkOrderStatus(data: any) {
    if (data.order.orderStatus.code == OrderStatus.Acknowledged) {
      let order: number[] = [this.order.id];
      this.updateOrderStatus(OrderStatus.Open, order).subscribe(
        openStatus => { },
        err => { },
        () => {
          this.onSendNotification(this.order, "your order has been open !")
        }
      )
    }
  }

  getAllOrderByPosAndVendorId(posIds: number[], vendorId: number): Observable<any> {
    return this.http.post(environment.baseURL + "order/check_order_pos/" + vendorId,
      JSON.stringify(posIds),
      { headers: BaseService.HEADERS });
  }

  findOrderById(id: number, vendorId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order/find_by_id/" + id + "/" + vendorId);
  }

  get model() {
    return this.order;
  }
  set model(model: BaseModel) {
    this.order = <Order>model;
  }


  // onCalculateAndSaveLoyaltyTrxPartialCompletePoints(orders: Order) {
  //   return this.http.post(environment.baseURL + "order/points",
  //     JSON.stringify(orders),
  //     { headers: BaseService.HEADERS });
  // }
}