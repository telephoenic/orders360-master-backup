import { Injectable } from '@angular/core';
import { BaseService } from '../../../app/common/service/base.service';
import { OrderStatusType } from '../order-status-type';
import { BaseModel } from '../../../app/common/model/base-model';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../app/auth/service/auth.service';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderStatusService extends BaseService {

  orderStatusType: OrderStatusType;

  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService,
    private datePipe: DatePipe) {
    super(http, "order_status", authService)
  }

  public findOrderStatusTypeByCode(code: String): Observable<any> {
    return this.http.get(environment.baseURL + "order_status/find_by_code/" + code);
  }

  public findOrderStatusByParentId(parentId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order_status/find_by_parentId/" + parentId);
  }

  getModel() {
    return this.orderStatusType;
  }

  set model(model: BaseModel) {
    this.orderStatusType = <OrderStatusType>model;
  }


}
