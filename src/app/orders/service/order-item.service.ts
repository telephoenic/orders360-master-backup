import { BaseModel } from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { OrderItem } from '../order-item.model';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class OrderItemService extends BaseService {
  orderItem: OrderItem;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService) {
    super(http, "order_item", authService)
  }

  getPartialCompleteQuantityForOrder(orderId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order_item/quantity_order" + "/" + orderId);
  }

  updateOrderItemStatus(status: String, itemId: number) {
    return this.http.post(environment.baseURL + "order_item/update_status" + "/" + status,
      JSON.stringify(itemId),
      { headers: this.getBasicHeaders() });
  }


  updateDeliveryQuantity(orderItem:number){
    return this.http.post(environment.baseURL+"order_item/update_delivery_quantity",JSON.stringify(orderItem), 
    { headers: this.getBasicHeaders() });
  }

  get model() {
    return this.orderItem;
  }
  set model(model: BaseModel) {
    this.orderItem = <OrderItem>model;
  }
}