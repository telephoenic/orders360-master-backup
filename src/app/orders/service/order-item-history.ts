import { BaseModel } from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { BaseService } from '../../common/service/base.service';
import { OrderItem } from '../order-item.model';
import { OrderItemHistory } from '../order-item-history.model';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class OrderItemHistoryService extends BaseService {
  orderItemHistory: OrderItemHistory;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService) {
    super(http, "order_history", authService)
  }

  getAllHistoryItems(itemId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order_history/items" + "/" + itemId);
  }

  getHistoryItemsList(itemId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order_history/allItems" + "/" + itemId);
  }

  gePromotiontHistoryItemsList(promotionDetailId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order_history/promotion-history-item" + "/" + promotionDetailId);
  }

  getQuantityForItem(itemId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order_history/quantity_item" + "/" + itemId);
  }

  getQuantityForOrder(orderId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order_history/quantity_order" + "/" + orderId);
  }


  getSumOfQuantityToFulfilledAndCanceld(itemId: number, promotionDetailId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order_history/quantity_promotion_item" + "/" + itemId + "/" + promotionDetailId);
  }

  getSumOfBackToOrder(itemId:number): Observable<any> {
    return this.http.get(environment.baseURL + "order_history/find_back_to_order" + "/" + itemId);
  }

  getSumOfQuantityToFulfilledAndCanceldByItemId(itemId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order_history/find_item_history_by_id" + "/" + itemId);
  }

  findAllOrderItemHistory(itemId: number): Observable<any> {
    return this.http.get(environment.baseURL + "order_history/find_all_order_history_by_item_Id/" + itemId)
  }

  findOrderItemHistoryByItemId(itemIds: number[]): Observable<any> {
    return this.http.post(environment.baseURL + "order_history/find_order_history", JSON.stringify(itemIds), { headers: this.getBasicHeaders() })
  }

  findOrderItemHistoryByItemIdGroup(itemIds: number[]): Observable<any> {
    return this.http.post(environment.baseURL + "order_history/find_order_history_by_item_Id/", JSON.stringify(itemIds), { headers: this.getBasicHeaders() });
  }

  updateOrderItemHistory(ids: number[]) {
    return this.http.post(environment.baseURL + "order_history/update_order_item_history", JSON.stringify(ids), { headers: this.getBasicHeaders() })
  }


  get model() {
    return this.orderItemHistory;
  }
  set model(model: BaseModel) {
    this.orderItemHistory = <OrderItemHistory>model;
  }
}