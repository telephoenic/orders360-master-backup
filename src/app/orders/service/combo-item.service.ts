import {environment} from '../../../environments/environment';
import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ComboItem } from '../combo-item.model';

@Injectable()
export class ComboItemService extends BaseService{
  comboItem:ComboItem;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService:AuthService) {
   super(http, "combo_item", authService)
   }

   getAllComboItems(itemId: number) : Observable<any>{
    return this.http.get(environment.baseURL + "combo_item/items"+"/"+itemId);
  }

   getOrderItemWherOrderId(orderId: number) : Observable<any>{
    return this.http.get(environment.baseURL + "order_item/all_items"+"/"+orderId);
    }

    getOrderItemsWherOrderIds(orderId: number[]) : Observable<any>{
      return this.http.get(environment.baseURL + "order_item/all_items"+"/"+orderId);
      }
    
   get model(){
     return this.comboItem;
   }
   set model(model:BaseModel) {
     this.comboItem = <ComboItem>model;
   }
}