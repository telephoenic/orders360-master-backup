import { Injectable } from '@angular/core';
import { OrderStatusHistory } from '../order-status-history';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../app/auth/service/auth.service';
import { BaseModel } from '../../common/model/base-model';
import { BaseService } from '../../common/service/base.service';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderStatusHistoryService extends BaseService {

  orderStatusHistory: OrderStatusHistory;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService) {
    super(http, "order_status_history", authService)
  }

  public findAllOrderStatusHisotryByOrder(orderId: number):Observable<any>{
    return this.http.get(environment.baseURL + "order_status_history/find_all_order_status_history/" + orderId)
  }

  get model() {
    return this.orderStatusHistory;
  }
  set model(model: BaseModel) {
    this.orderStatusHistory = <OrderStatusHistory>model;
  }
}
