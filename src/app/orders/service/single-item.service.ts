import {environment} from '../../../environments/environment';
import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SingleItem } from '../single-item.model';

@Injectable()
export class SingleItemService extends BaseService{
  singleItem:SingleItem;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService:AuthService) {
   super(http, "single_item", authService)
   }

   getAllSingleItems(itemId: number) : Observable<any>{
    return this.http.get(environment.baseURL + "single_item/items"+"/"+itemId);
  }

   getOrderItemWherOrderId(orderId: number) : Observable<any>{
    return this.http.get(environment.baseURL + "order_item/all_items"+"/"+orderId);
    }

    getOrderItemsWherOrderIds(orderId: number[]) : Observable<any>{
      return this.http.get(environment.baseURL + "order_item/all_items"+"/"+orderId);
      }
    
   get model(){
     return this.singleItem;
   }
   set model(model:BaseModel) {
     this.singleItem = <SingleItem>model;
   }
}