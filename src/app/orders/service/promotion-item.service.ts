import { PromotionItem } from './../promotion-item.model';
import { AuthService } from './../../auth/service/auth.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {BaseModel} from '../../common/model/base-model';
import { BaseService } from '../../common/service/base.service';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PromotionItemService extends BaseService{

  promotionItem:PromotionItem;
  navigateToAfterCompletion: string;


  constructor(http: HttpClient,
    protected authService:AuthService) {
   super(http, "promotionItem", authService)
   }


   getAllPromotionItems(orderId:number) : Observable<any>{
    return this.http.get(environment.baseURL + "promotionItem/promotiondetails/"+orderId);
  }

  
  getAllPromotionItemsById(itemId:number) : Observable<any>{
   return this.http.get(environment.baseURL + "promotionItem/promotiondetailsById/"+itemId);
 }

  getPartialCompleteQuantityForPromotionOrder(orderId: number) : Observable<any>{
  return this.http.get(environment.baseURL + "promotionItem_details/quantity_promotion_order"+"/"+orderId);
 }

   get model(){
    return this.promotionItem;
  }
  set model(model:BaseModel) {
    this.promotionItem = <PromotionItem>model;
  }

}
