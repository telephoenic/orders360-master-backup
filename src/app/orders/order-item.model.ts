import { Product } from "../product/product";
import { OrderItemHistory } from "./order-item-history.model";
import { OrderStatusType } from "./order-status-type";


export class OrderItem {

    id: number;
    inactive: boolean;
    deliverdQuantity: number;
    orderItemHistory: OrderItemHistory[];
    totalAmount: number;
    orderedQuantity: number;
    price: number;
    status: OrderStatusType;
    deliveryQuantity: number;

    constructor() {

    }


}