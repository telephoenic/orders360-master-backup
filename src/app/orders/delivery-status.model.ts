export class DeliveryStatus {
    
    private _id:number;

	
    private _code:string;
    private _status: string;
    public get status_1(): string {
        return this._status;
    }
    public set status_1(value: string) {
        this._status = value;
    }

    constructor (){}
    
    public get id(): number {
        return this._id;
    }

    public set id(value: number){
        this._id = value;
    }
    public get code(): string {
		return this._code;
	}

	public set code(value: string) {
		this._code = value;
	}
    
    }