import { Order } from "./order.model";
import { BaseModel } from "../common/model/base-model";
import { OrderRejectedReason } from "../reasons/reason.model";

export class RejectedOrder extends BaseModel {

    id: number;
    inactive: boolean;
    reason:string;
    description: string;
    order: Order;
    orderRejectedReason:OrderRejectedReason;


    constructor() {
        super();
        this.orderRejectedReason=new OrderRejectedReason();
    }
}