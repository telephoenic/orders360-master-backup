import { BaseModel } from "../common/model/base-model";
import { User } from "../auth/user.model";
import { OrderStatusType } from "./order-status-type";
import { OrderRejectedReason } from "../reasons/reason.model";

export class OrderStatusHistory extends BaseModel{

    id:number
    deleted:boolean;
    inactive:boolean;
    user:User;
    orderStatusType:OrderStatusType;
    orderRejectedReason:OrderRejectedReason;

    constructor(){
        super();
    }

}