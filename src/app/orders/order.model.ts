import { PosUser } from "../pos-user/pos-user.model";
import { DeliveryStatus } from "./delivery-status.model";
import { OrderItem } from "./order-item.model";
import { SingleItem } from "./single-item.model";
import { ComboItem } from "./combo-item.model";
import { PosAassignedVendor } from "../pos-user/pos-assigned-vendor";
import { OrderStatusType } from "./order-status-type";
import { Vendor } from "../vendor/vendor.model";


export class Order {
    
        public id:number;
        public orderNumber:number;
        public orderStatus:OrderStatusType;
        public singleItems:SingleItem[];
        public comboItems:ComboItem[];
        public createdDate:Date;
        public totalAmount:number;
        public posUser:PosUser;
        public inactive:boolean;
        public posGroup:string;
        public vendor:Vendor;
        constructor (){}
        
    
    }