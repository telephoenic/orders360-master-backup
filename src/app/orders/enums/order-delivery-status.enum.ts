export enum OrderDeliveryStatus {

  InProcess="InProcess",
  InWarehouse="InWarehouse",
  ReadyToShip="ReadyToShip",
  Shipped="Shipped",
  Delivered="Delivered",
  returned="returned"
}
