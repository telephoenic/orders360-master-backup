export enum OrderFulfilmentStatus {

    Unfulfilled = "Unfulfilled",
    PartialFulfilment = "PartialFulfilment",
    Fulfilled = "Fulfilled",
    UnfulfilledCode = "UNF",
    PartialFulfilmentCode = "PF",
    FulfilledCode = "FUL"


}