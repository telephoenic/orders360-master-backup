export enum OrderShippmentStatus {
    IN_WAREHOUSE = 'IN',
    READY_TOSHIP = 'RTS',
    SHIPPED = 'SH',
    DELIVERED = 'DE',
    RETURNRD = 'RET'
}