export enum OrderStatus{
    New="NE",
    Acknowledged="AC",
    Open="OP",
    Canceled="CA",
    Closed="CL",
    Rejected="RE",
    Acknowledge="Acknowledged",
    Reject="Rejected"
}