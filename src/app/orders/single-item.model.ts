import { Product } from "../product/product";
import { OrderItemHistory } from "./order-item-history.model";
import { OrderItem } from "./order-item.model";
import { PromotionQtyValueDto } from "../promotion/model/promotion-qty-value-dto";


export class SingleItem extends OrderItem{

    id:number;
    inactive:boolean;
    promotionQtyToValueTransient:PromotionQtyValueDto;
    discountedPrice:number;
    discountPercentage:any;
 
    product:Product;
    constructor ( ){
        super();
    }
    

}