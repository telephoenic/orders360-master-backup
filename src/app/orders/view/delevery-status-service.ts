import { BaseService } from "../../common/service/base.service";
import { DeliveryStatus } from "../delivery-status.model";
import { HttpClient } from '@angular/common/http';
import { AuthService } from "../../auth/service/auth.service";
import { MatSnackBar } from "@angular/material";
import { Injectable } from "@angular/core";
import { BaseModel } from "../../common/model/base-model";
import { environment } from "../../../environments/environment";
import { Observable } from "rxjs";

@Injectable()
export class DeliveryStatusService extends BaseService {
    model: BaseModel;
    private _deleveryStatus: DeliveryStatus = new DeliveryStatus();
    public get deleveryStatus(): DeliveryStatus {
        return this._deleveryStatus;
    }
    public set deleveryStatus(value: DeliveryStatus) {
        this._deleveryStatus = value;
    }

    constructor(http: HttpClient,
        protected authService: AuthService,
        protected snackBar: MatSnackBar) {
        super(http, "delivery_status", authService)
    }

    public getCurrentUsedDeliveryStatus(): Observable<any>{
        return this.http.get(environment.baseURL + "delivery_status/getDeliveryStatus");
    }


}