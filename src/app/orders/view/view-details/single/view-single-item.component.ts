
import { TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import {
	TdPagingBarComponent, TdDialogService,
	TdDataTableComponent
} from '@covalent/core';
import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { FormControl, Validators, NgForm } from '@angular/forms';
import {
	MatSnackBar,
	MatDialogRef,
	MAT_DIALOG_DATA,
	MatDialog
} from "@angular/material";
import { ActivatedRoute, Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { BaseViewComponent } from '../../../../common/view/base-view-component';
import { OrderItem } from '../../../order-item.model';
import { LoyaltyPointsTrx } from '../../../../loyalty/loyalty-points-trx.model';
import { LoyaltyPointsTrxSource } from '../../../../loyalty/loyalty-points-trx-source.model';
import { OrderService } from '../../../service/order.service';
import { ProductLoyaltyService } from '../../../../loyalty/service/product-loyalty.service';
import { LoyaltyPointsTrxService } from '../../../../loyalty/service/loyalty-points-trx.services';
import { LoyaltyPointsTrxSourceService } from '../../../../loyalty/service/loyalty-points-trx-source.service';
import { AllOrdersComponent } from '../../view-orders/view-orders.component';
import { BaseFormComponent } from '../../../../common/form/base-form-component';
import { OrderItemHistory } from '../../../order-item-history.model';
import { ENUM_CODE_DeliveryStatus, environment } from '../../../../../environments/environment';
import { SingleItem } from '../../../single-item.model';
import { SingleItemService } from '../../../service/single-item.service';
import { OrderItemHistoryService } from '../../../service/order-item-history';
import { Observable } from '../../../../../../node_modules/rxjs/Observable';
import { OrderItemService } from '../../../service/order-item.service';
import { Order } from '../../../order.model';


@Component({
	selector: 'view-single-item',
	templateUrl: './view-single-item.component.html',
})


export class AllSingleItemsComponent extends BaseViewComponent implements OnInit {

	public static minDeliverItem: number = 0;
	public static maxDeliverItem: number;
	public static LOYALTY_POINTS_TRX_SOURCE_CREDIT = "CR";

	private _currentPage: number = 1;
	private _fromRow: number = 1;
	private deliveryNewItem: number;
	public _allSingleItems: SingleItem[] = [];
	private _errors: any = '';
	private _selectedRows: SingleItem[] = [];
	private _pageSize: number;
	private _filteredTotal: number;
	private _sortBy: string = '';
	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
	private _searchTerm: string = '';
	private loyaltyPointsTrx: LoyaltyPointsTrx;
	private listOfLoyaltyPointsTrxSource: LoyaltyPointsTrxSource;
	@ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
	@ViewChild('dataTable') dataTable: TdDataTableComponent;

	columnsConfig: ITdDataTableColumn[] = [

		{ name: 'product.name', label: 'Product', width: 305 },
		{ name: 'totalAmount', label: 'Total Amount', width: 200 },
		{ name: 'orderedQuantity', label: 'Ordered Qty' },
		{ name: 'id', label: '', sortable: false, width: 300 }	];

	constructor(protected singleItemService: SingleItemService,
		protected orderItemService: OrderItemService,
		protected orderItemHistoryService: OrderItemHistoryService,
		protected orderService: OrderService,
		protected productLoyaltyService: ProductLoyaltyService,
		protected loyaltyPointsTrxService: LoyaltyPointsTrxService,
		protected loyaltyPointsTrxSourceService: LoyaltyPointsTrxSourceService,
		protected ordersView: AllOrdersComponent,
		protected router: Router,
		protected dialogService: TdDialogService,
		protected snackBar: MatSnackBar,
		public dialog: MatDialog,
		protected permissionsService: NgxPermissionsService) {
		super(singleItemService, router, dialogService, snackBar);
	}

	ngOnInit() {
		//this.loadAllSingleItems();
		this.permissionsService.hasPermission("ROLE_ORDER_DETAIL_HISTORY").then((result: boolean) => {
			if (!result) {
				for (let i = 0; i < this.dataTable.columns.length; i++) {
					if (this.dataTable.columns[i].name == 'id') {
						this.dataTable.columns.splice(i, 1);
					}
				}
			}
		});

		this.loadLoyaltyTrxSourceOptions();
	}

	loadAllSingleItems() {
		this.singleItemService.getAllSingleItems(this.orderService.order.id).subscribe(
			data => {
				this._allSingleItems = data;
			});
	}

	loadLoyaltyTrxSourceOptions() {
		this.loyaltyPointsTrxSourceService.getSourceByCode(AllSingleItemsComponent.LOYALTY_POINTS_TRX_SOURCE_CREDIT).subscribe(
			data => {
				this.listOfLoyaltyPointsTrxSource = data;
			}
		);
	}

	// onOpenDialog(row: SingleItem): void {
	// 	super.onNew();
	// 	this.orderItemHistoryService.getAllHistoryItems(row.id).subscribe(
	// 		data => {
	// 			row.orderItemHistory = data;
	// 			row.deliverdQuantity = this.setDeliverdQuantityForItem(row);
	// 			this.openDialog(row);
	// 		});
	// }

	// openDialog(row: SingleItem): void {
	// 	if (this.orderService.order.deliveryStatus.code == ENUM_CODE_DeliveryStatus.NE) {
	// 		this.onCheckNew();
	// 	} else {
	// 		let isCompleteOrder = false;
	// 		if (this.orderService.order.deliveryStatus.code == ENUM_CODE_DeliveryStatus.CO)
	// 		 	isCompleteOrder = true;
	// 		AllSingleItemsComponent.maxDeliverItem = row.orderedQuantity - row.deliverdQuantity;
	// 		let dialogRef = this.dialog.open(DialogSingleHistory, {
	// 			width: '500px',
	// 			height: '370px',
	// 			// maxHeight:'370px',
	// 			data: { deliveryNewItem: this.deliveryNewItem, singleItem: row, isCompleteOrder: isCompleteOrder}
	// 		});


	// 		dialogRef.afterClosed().subscribe(result => {
	// 			if (result != null) {
	// 				row.deliverdQuantity = row.deliverdQuantity + result.deliveryNewItem;
	// 				this.setDeliverdQtForOrder(row);

	// 				this.dataTable.refresh();
	// 			}
	// 		});
	// 	}
	// }

	setDeliverdQtForOrder(row: SingleItem) {
		this.orderItemService.getPartialCompleteQuantityForOrder(this.orderService.order.id).subscribe(
			data => {

				this.setPartialComplete(this.orderService.order, data);

			}
		);
	}

	setPartialComplete(order: Order, isPartialComplete: boolean) {
		if (isPartialComplete) {
			this.onSaveLoyaltyPointsTrx(order);
			// this.onUpdateStatus(environment.ENUM_ID_DeliveryStatus.PartialComplete);
		} 
		// else if (this.orderService.order.deliveryStatus.code != ENUM_CODE_DeliveryStatus.PI) {
		// 	this.onUpdateStatus(environment.ENUM_ID_DeliveryStatus.PartialIncomplete);
		// }
	}

	setDeliverdQuantityForItem(item: SingleItem): number {
		item.deliverdQuantity = 0;
		if (item.orderItemHistory != null) {
			for (let counter = 0; counter < item.orderItemHistory.length; counter++) {
				item.deliverdQuantity = item.deliverdQuantity + item.orderItemHistory[counter].quantity;
			}
		}
		return item.deliverdQuantity;
	}

	onSaveLoyaltyPointsTrx(row: Order) {
		this.orderService.onCalculateAndSaveLoyaltyTrxPartialCompletePoints(row.id).subscribe();
		// this.productLoyaltyService.getPointsWhereProductId(row.product.id).subscribe(
		// 	data => {
		// 		this.loyaltyPointsTrx = new LoyaltyPointsTrx(row.deliverdQuantity*data,
		// 			this.orderService.order,
		// 			this.orderService.order.posUser,
		// 			row.product,
		// 			this.listOfLoyaltyPointsTrxSource);
		// 			this.loyaltyPointsTrxService.save(this.loyaltyPointsTrx).subscribe();
		// 	});
	}

	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}
	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

	public get currentPage(): number {
		return this._currentPage;
	}

	public set currentPage(value: number) {
		this._currentPage = value;
	}

	public get fromRow(): number {
		return this._fromRow;
	}

	public set fromRow(value: number) {
		this._fromRow = value;
	}

	public get allModels(): SingleItem[] {
		return this._allSingleItems;
	}

	public set allModels(value: SingleItem[]) {
		this._allSingleItems = value;
	}

	public get errors(): any {
		return this._errors;
	}

	public set errors(value: any) {
		this._errors = value;
	}

	public get selectedRows(): SingleItem[] {
		return this._selectedRows;
	}

	public set selectedRows(value: SingleItem[]) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string {
		return this._sortBy;
	}

	public set sortBy(value: string) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder) {
		this._sortOrder = value;
	}

	public get searchTerm(): string {
		return this._searchTerm;
	}

	public set searchTerm(value: string) {
		this._searchTerm = value;
	}

}