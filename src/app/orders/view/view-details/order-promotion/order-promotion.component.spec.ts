import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPromotionComponent } from './order-promotion.component';

describe('OrderPromotionComponent', () => {
  let component: OrderPromotionComponent;
  let fixture: ComponentFixture<OrderPromotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderPromotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
