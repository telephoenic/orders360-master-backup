import { OrderItemService } from './../../../service/order-item.service';
import { AllOrdersComponent } from './../../view-orders/view-orders.component';
import { DecimalPipe } from '@angular/common';
import { forEach } from '@angular/router/src/utils/collection';
import { OrderItem } from './../../../order-item.model';
import { OrderService } from './../../../service/order.service';
import { PromotionItemService } from './../../../service/promotion-item.service';
import { PromotionItem } from './../../../promotion-item.model';
import { MatSnackBar, MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { VendorAdminService } from './../../../../vendor-admin/service/vendor-admin.service';
import { AuthService } from './../../../../auth/service/auth.service';
import { Promotion } from './../../../../promotion/model/promotion';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ITdDataTableColumn, TdPagingBarComponent, TdDataTableComponent, TdDataTableSortingOrder, TdDialogService } from '@covalent/core';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { NgxPermissionsService } from 'ngx-permissions';
import { BaseViewComponent } from '../../../../common/view/base-view-component';
import { Overlay } from '@angular/cdk/overlay';
import { OrderItemHistoryService } from '../../../service/order-item-history';
import { OrderItemHistory } from '../../../order-item-history.model';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { ENUM_CODE_DeliveryStatus, environment } from '../../../../../environments/environment';


@Component({
  selector: 'app-order-promotion',
  templateUrl: './order-promotion.component.html',
  styleUrls: ['./order-promotion.component.css']
})
export class OrderPromotionComponent extends BaseViewComponent implements OnInit {

  @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
  @ViewChild('dataTable') dataTable: TdDataTableComponent;

  

  promotionColumnsConfig: ITdDataTableColumn[] = [

    { name: 'product.name', label: 'Product',width:290 },
    { name: 'promotion.name', label: 'Promotion Name',width:250 },
    { name: 'promotionDetail', label: 'Promotion Details',width:180 },
    { name: 'orderedQuantity', label: 'Order QTY',width:120 },
    { name: 'totalAmount', label: 'Total Amount'},
    { name: 'promotionDetailId', label: '',width:100 },

    
  ];


  public static minDeliverItem: number = 0;
  public static maxDeliverItem: number;
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private _errors: any = '';
  private decimalPipe: DecimalPipe;
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = '';
  private _promotionItemList: PromotionItem[] = [];
  promotionItem: PromotionItem = new PromotionItem();
   quantityToDeliver: number;
 



  constructor(protected promotionItemService: PromotionItemService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected authService: AuthService,
    protected vendorAdminService: VendorAdminService,
    protected snackBar: MatSnackBar,
    protected matDialog: MatDialog,
    protected orderService: OrderService,
    protected orderItemService: OrderItemService,
    protected overlay: Overlay,
    private datePipe: DatePipe,
    protected ordersView: AllOrdersComponent,
    protected orderHistoryItemService: OrderItemHistoryService,
    protected permissionsService: NgxPermissionsService) {
   
    super(promotionItemService, router, dialogService, snackBar);
  }

  ngOnInit() {
    //this.getPromotionDetails();
  }

  onUpdateStatus(status:number){
		
		// this.ordersView.onChangeDeliveryStatus(status,
		// 	this.orderService.order.id);
	}



  // openPromotionDetailsDialog(id: number, row: any) {
  //   this.promotionItemService.getAllPromotionItemsById(row.promotionDetailId).subscribe(data => {
  //     this.orderHistoryItemService.getQuantityForPromotionItem(row.id,row.promotionDetailId).subscribe(obj => {
  //       row = data;
  //       if (obj == null) {
  //         row.deliverdQuantity = 0
  //       } else {
  //         row.deliverdQuantity = obj
  //       }

  //       OrderPromotionComponent.maxDeliverItem = row.orderedQuantity - row.deliverdQuantity;
  //       this.openDialog(row);
  //     });
  //   });
  // }


  // onCheckNew(){
	// 	this.dialogService.openConfirm({
	// 		message: "You have to confirm receiving this order before delivering items, are you sure?",
	// 		title: "Receiving confirmation",
	// 		cancelButton: "Cancel",
	// 		acceptButton: "OK"
	// 		}).afterClosed().subscribe((accept:boolean) => {
	// 			if(accept) {
	// 				this.onUpdateStatus(environment.ENUM_ID_DeliveryStatus.OrderReceived);
	// 				this.orderService.order.deliveryStatus.code = ENUM_CODE_DeliveryStatus.OR;
	// 		  } else {
	// 			  this.snackBar.open("Order not confirmed", "Close", {duration: 3000});
	// 		  }
	// 		});
	// }

  // openDialog(row: PromotionItem) {


  //   if(this.orderService.order.deliveryStatus.code == ENUM_CODE_DeliveryStatus.NE){
  //     this.onCheckNew();
  //   } else{
  //     let isCompleteOrder = false;
	// 		if (this.orderService.order.deliveryStatus.code == ENUM_CODE_DeliveryStatus.CO)
	// 		 	isCompleteOrder = true;
  //     let dialogRef = this.dialogService.open(PromotionDetailsDialog, {
  //       width: '850px',
  //       height: '450px',
  
  //       data: {
  //         name: row.promotion.name,
  //         productName: row.product.name,
  //         promotionDetails: row.promotionDetail,
  //         promotionPrice: Math.round(row.totalAmount*100)/100,
  //         freeItem: row.freeItems,
  //         orderedQuantity: row.orderedQuantity,
  //         numberOfItems: row.numberOfItems,
  //         quantity: row.quantity,
  //         totalAmount:row.promotionPrice,
  //         productPrice:row.product.price,
  //         promotionItemId: row.id,
  //         deliverdQuantity: row.deliverdQuantity,
  //         quantityToDeliver:this.quantityToDeliver,
  //         promotionDetailId:row.promotionDetailId,
  //         isCompleteOrder: isCompleteOrder
  
  
  //       }
  
  //     });

  //     dialogRef.afterClosed().subscribe(result => {
	// 			if(result != null) {
  //         // row.deliverdQuantity = row.deliverdQuantity + result.quantityToDeliver;
	// 				// this.setDeliverdQtForOrder(row);
 
  //         this.dataTable.refresh();
	// 			}
  //     });
  //   }

  


  // }

  



  public getPromotionDetails() {
    this.promotionItemService.getAllPromotionItems(this.orderService.order.id).subscribe(data => {
      this.promotionItemList = data;
      this.promotionItemList.forEach(element => {
        element.totalAmount = Math.round(element.totalAmount*100)/100;
      });
      this.dataTable.refresh();
    });

  }

  
	setDeliverdQtForOrder(row:PromotionItem){
		this.promotionItemService.getPartialCompleteQuantityForPromotionOrder(this.orderService.order.id).subscribe(
		 data => {
         this.setPartialComplete(data);
		 }
	 );
 }

 setPartialComplete(isPartialComplete:boolean){
  // if(isPartialComplete) {
  //   this.onUpdateStatus(environment.ENUM_ID_DeliveryStatus.PartialComplete);
  // } else if(this.orderService.order.deliveryStatus.code != ENUM_CODE_DeliveryStatus.PI){
  //   this.onUpdateStatus(environment.ENUM_ID_DeliveryStatus.PartialIncomplete);
  // }
}


  public get promotionItemList(): PromotionItem[] {
    return this._promotionItemList;
  }
  public set promotionItemList(value: PromotionItem[]) {
    this._promotionItemList = value;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number {
    return this._currentPage;
  }
  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }
  public set fromRow(value: number) {
    this._fromRow = value;
  }


  public get allModels(): Promotion[] {
    return null;
  }

  public set allModels(value: Promotion[]) {
    //this._allSurvey = value;
  }


  public get errors(): any {
    return this._errors;
  }
  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): Promotion[] {
    return;
  }
  public set selectedRows(value: Promotion[]) {
    //this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }
  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }
  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }
  public get sortBy(): string {
    return this._sortBy;
  }
  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }
  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }
  public get searchTerm(): string {
    return this._searchTerm;
  }
  public set searchTerm(value: string) {
    this._searchTerm = value;
  }



}


// @Component({
//   selector: 'app-promotion-details-dlg',
//   templateUrl: './order-promotion-details-dlg.component.html',

// })
// export class PromotionDetailsDialog implements OnInit {

//   @ViewChild('historyDt') historyDt: TdDataTableComponent;
//   private _orderhistoryList: OrderItemHistory[] = [];

//   constructor(public dialogRef: MatDialogRef<PromotionDetailsDialog>,
//     @Inject(MAT_DIALOG_DATA) public data: any,
//     protected orderHistoryItemService: OrderItemHistoryService,
//     private datePipe: DatePipe,
//     protected orderService: OrderService, ) {

//   }

//   deliveryHistoryColumnsConfig: ITdDataTableColumn[] = [


//     { name: 'deliveryDate', label: 'Delivery Date', format: v => this.datePipe.transform(v, 'medium'), width: 200 },
//     { name: 'quantity', label: 'Quantity', width: 200 },
//     { name: 'numberOfItems', label: 'Number Of Items', width: 200 },
//     { name: 'totalAmount', label: 'Total Amount', width: 200 },
//   ];

  
//   totalAmountAndnumberOfItemsShow: boolean = false;
//   isManual:boolean=true;


//   ngOnInit() {
//     this.getAllOrderHistoryItem();
//     if(this.data.name =="Manual"){
//       console.log(this.data.name =="Manual");
//       this.isManual=false;
//     }
//   }



//   onChangeQuantityToDeliver() {

//     if (this.data.quantityToDeliver == null || 
//        (this.data.quantityToDeliver <= 0 || this.data.quantityToDeliver < 1) || 
//         OrderPromotionComponent.maxDeliverItem == 0) {
//       this.totalAmountAndnumberOfItemsShow = false;
//       this.data.numberOfItems = null;
//       this.data.totalAmount = null;
//     } else {
//       this.totalAmountAndnumberOfItemsShow = true;
//       this.data.numberOfItems = (this.data.freeItem + this.data.quantity )* this.data.quantityToDeliver;
//       this.data.totalAmount = Math.round((this.data.productPrice* this.data.quantityToDeliver * this.data.quantity)* 100)/100;


//     }
//   }


//   onSave() {
//     let _orderItemHistory = new OrderItemHistory();
//     _orderItemHistory.orderItem = new OrderItem();
//     _orderItemHistory.deliveryDate = new Date();
//     _orderItemHistory.quantity = this.data.quantityToDeliver;
//     _orderItemHistory.totalAmount = this.data.totalAmount;
//     _orderItemHistory.numberOfItems = this.data.numberOfItems;
//     _orderItemHistory.orderItem.id = this.data.promotionItemId;
//     _orderItemHistory.promotionDetailId=this.data.promotionDetailId;
//     _orderItemHistory.remark = this.data.remark;
//     this.orderHistoryItemService.save(_orderItemHistory).subscribe(temp => {

//     })

//   }

//   getAllOrderHistoryItem() {
//     this.orderHistoryItemService.gePromotiontHistoryItemsList(this.data.promotionDetailId).subscribe(objs => {
//       this.orderhistoryList = objs;
//       for (let i = 0; i < this.orderhistoryList.length; i++) {
//         this._orderhistoryList[i].deliveryDate = new Date(this.datePipe.transform(this._orderhistoryList[i].deliveryDate, 'medium'));
//         this._orderhistoryList[i].totalAmount =Math.round(this._orderhistoryList[i].totalAmount * 100)/100 ;
//       }
//     })



//     this.historyDt.refresh();
//   }


  



//   deliverNewItemField = new FormControl('', [
//     Validators.required,
//     Validators.min(OrderPromotionComponent.minDeliverItem),
//     Validators.max(OrderPromotionComponent.maxDeliverItem)]);

//   onNoClick(): void {
//     this.dialogRef.close();
//   }

//   public get orderhistoryList(): OrderItemHistory[] {
//     return this._orderhistoryList;
//   }
//   public set orderhistoryList(value: OrderItemHistory[]) {
//     this._orderhistoryList = value;
//   }



// }
