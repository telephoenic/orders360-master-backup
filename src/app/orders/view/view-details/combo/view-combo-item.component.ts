
import { TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import {
	TdPagingBarComponent, TdDialogService,
	TdDataTableComponent
} from '@covalent/core';
import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { FormControl, Validators, NgForm } from '@angular/forms';
import {
	MatSnackBar,
	MatDialogRef,
	MAT_DIALOG_DATA,
	MatDialog
} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { BaseViewComponent } from '../../../../common/view/base-view-component';
import { OrderItem } from '../../../order-item.model';
import { OrderService } from '../../../service/order.service';
import { LoyaltyPointsTrxSourceService } from '../../../../loyalty/service/loyalty-points-trx-source.service';
import { AllOrdersComponent } from '../../view-orders/view-orders.component';
import { ENUM_CODE_DeliveryStatus, environment } from '../../../../../environments/environment';
import { BaseFormComponent } from '../../../../common/form/base-form-component';
import { OrderItemHistory } from '../../../order-item-history.model';
import { ComboItem } from '../../../combo-item.model';
import { ComboItemService } from '../../../service/combo-item.service';
import { OrderItemHistoryService } from '../../../service/order-item-history';
import { OrderItemService } from '../../../service/order-item.service';


@Component({
	selector: 'view-combo-item',
	templateUrl: './view-combo-item.component.html',
})


export class AllComboItemsComponent extends BaseViewComponent implements OnInit {

	public static minDeliverItem = 0;
	public static maxDeliverItem: number;

	private _currentPage = 1;
	private _fromRow = 1;
	private deliveryNewItem: number;
	public _allComboItems: ComboItem[] = [];
	private _errors: any = '';
	selectedRows: ComboItem[] = [];
	private _pageSize: number;
	private _filteredTotal: number;
	private _sortBy = '';
	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
	private _searchTerm = '';


	@ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
	@ViewChild('dataTable') dataTable: TdDataTableComponent;

	columnsConfig: ITdDataTableColumn[] = [

		{ name: 'combo.name', label: 'Combo', width: 310},
		{ name: 'totalAmount', label: 'Total Amount', width: 228 },
		{ name: 'orderedQuantity', label: 'Ordered Qty'},
		{ name: 'id', label: '', sortable: false, width: 300 }
	];

	constructor(protected comboItemService: ComboItemService,
		protected orderItemService: OrderItemService,
		protected orderItemHistoryService: OrderItemHistoryService,
		protected orderService: OrderService,
		protected loyaltyPointsTrxSourceService: LoyaltyPointsTrxSourceService,
		protected ordersView: AllOrdersComponent,
		protected router: Router,
		protected dialogService: TdDialogService,
		protected snackBar: MatSnackBar,
		public dialog: MatDialog,
		protected permissionsService: NgxPermissionsService) {
		super(comboItemService, router, dialogService, snackBar);
	}

	ngOnInit() {
		//this.loadAllComboItems();

		this.permissionsService.hasPermission('ROLE_ORDER_DETAIL_HISTORY').then((result: boolean) => {
			if (!result) {
				for (let i = 0; i < this.dataTable.columns.length; i++) {
					if (this.dataTable.columns[i].name == 'id') {
						this.dataTable.columns.splice(i, 1);
					}
				}
			}
		});

	}

	loadAllComboItems() {
		this.comboItemService.getAllComboItems(this.orderService.order.id).subscribe(
			data => {
				this._allComboItems = data;
			});
	}

	// onOpenDialog(row: ComboItem): void {

	// 	super.onNew();
	// 	this.orderItemHistoryService.getAllHistoryItems(row.id).subscribe(
	// 		data => {
	// 			row.orderItemHistory = data;
	// 			row.deliverdQuantity = this.setDeliverdQuantityForItem(row);
	// 			this.openDialog(row);
	// 		});
	// }

	// openDialog(row: OrderItem): void {
	// 	console.log(row);
	// 	super.onNew();
	// 	if (this.orderService.order.deliveryStatus.code == ENUM_CODE_DeliveryStatus.NE) {
	// 		this.onCheckNew();
	// 	} else {
	// 		let isCompleteOrder = false;
	// 		if (this.orderService.order.deliveryStatus.code == ENUM_CODE_DeliveryStatus.CO) {
	// 		 	isCompleteOrder = true;
	// 		}
	// 		AllComboItemsComponent.maxDeliverItem = row.orderedQuantity - row.deliverdQuantity;
	// 		const dialogRef = this.dialog.open(DialogComboHistory, {
	// 			width: '500px',
	// 			height: '370px',
	// 			// maxHeight:'370px',
	// 			data: { deliveryNewItem: this.deliveryNewItem, comboItem: row, isCompleteOrder: isCompleteOrder }
	// 		});

	// 		dialogRef.afterClosed().subscribe(result => {
	// 			if (result != null) {
	// 				// row.deliverdQuantity = row.deliverdQuantity + result.deliveryNewItem;
	// 				// this.setDeliverdQtForOrder(row);

	// 				this.dataTable.refresh();
	// 			}
	// 		});
	// 	}
	// }

	setDeliverdQtForOrder(row: ComboItem) {
		this.orderItemService.getPartialCompleteQuantityForOrder(this.orderService.order.id).subscribe(
			data => {

				this.setPartialComplete(data);
			}
		);
	}

	setPartialComplete(isPartialComplete: boolean) {
		// if (isPartialComplete) {
		// 	this.onUpdateStatus(environment.ENUM_ID_DeliveryStatus.PartialComplete);
		// } else if (this.orderService.order.deliveryStatus.code != ENUM_CODE_DeliveryStatus.PI) {
		// 	this.onUpdateStatus(environment.ENUM_ID_DeliveryStatus.PartialIncomplete);
		// }
	}

	setDeliverdQuantityForItem(item: ComboItem): number {
		item.deliverdQuantity = 0;
		if (item.orderItemHistory != null) {
			for (let counter = 0; counter < item.orderItemHistory.length; counter++) {
				item.deliverdQuantity = item.deliverdQuantity + item.orderItemHistory[counter].quantity;
			}
		}
		return item.deliverdQuantity;
	}

	onCheckPartialInComplete(): boolean {
		for (let counter = 0; counter < this._allComboItems.length; counter++) {
			if (this._allComboItems[counter].orderedQuantity -
				this._allComboItems[counter].deliverdQuantity == 0) {
				continue;
			} else {
				return true;
			}
		}

		return false;
	}

	onCheckNew() {
		this.dialogService.openConfirm({
			message: 'You have to confirm receiving this order before delivering items, are you sure?',
			title: 'Receiving confirmation',
			cancelButton: 'Cancel',
			acceptButton: 'OK'
		}).afterClosed().subscribe((accept: boolean) => {
			if (accept) {
				this.onUpdateStatus(environment.ENUM_ID_DeliveryStatus.OrderReceived);
				//this.orderService.order.deliveryStatus.code = ENUM_CODE_DeliveryStatus.OR;
			} else {
				this.snackBar.open('Order not confirmed', 'Close', { duration: 3000 });
			}
		});
	}
	onUpdateStatus(status: number) {

		// this.ordersView.onChangeDeliveryStatus(status,
		// 	this.orderService.order.id);
	}

	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}
	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

	public get currentPage(): number {
		return this._currentPage;
	}

	public set currentPage(value: number) {
		this._currentPage = value;
	}

	public get fromRow(): number {
		return this._fromRow;
	}

	public set fromRow(value: number) {
		this._fromRow = value;
	}

	public get allModels(): ComboItem[] {
		return this._allComboItems;
	}

	public set allModels(value: ComboItem[]) {
		this._allComboItems = value;
	}

	public get errors(): any {
		return this._errors;
	}

	public set errors(value: any) {
		this._errors = value;
	}


	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string {
		return this._sortBy;
	}

	public set sortBy(value: string) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder) {
		this._sortOrder = value;
	}

	public get searchTerm(): string {
		return this._searchTerm;
	}

	public set searchTerm(value: string) {
		this._searchTerm = value;
	}

}

