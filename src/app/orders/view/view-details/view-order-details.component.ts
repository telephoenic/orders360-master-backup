import { Combo } from "./../../../combo/combo.model";
import { User } from "./../../../auth/user.model";
import { AuthService } from "./../../../auth/service/auth.service";
import { ComboItem } from "./../../combo-item.model";
import { Product } from "./../../../product/product";

import { ITdDataTableColumn } from "@covalent/core";
import {
  TdPagingBarComponent,
  TdDialogService,
  TdDataTableComponent,
} from "@covalent/core";
import {
  Component,
  ViewChild,
  OnInit,
  AfterViewInit,
  Inject,
  HostListener,
} from "@angular/core";
import {
  MatSnackBar,
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material";
import { Router, ActivatedRoute } from "@angular/router";
import { OrderService } from "../../service/order.service";
import { AllOrdersComponent } from "../view-orders/view-orders.component";
import { NgxPermissionsService } from "ngx-permissions";
import { FcmService } from "../../../fcm/services/fcm.service";
import { OrderDetails } from "../../order-details";
import { SingleItem } from "../../single-item.model";
import { OrderItemHistoryService } from "../../service/order-item-history";
import { OrderItem } from "../../order-item.model";
import { NgForm, FormControl, Validators } from "@angular/forms";
import { ComboItemService } from "../../service/combo-item.service";
import { OrderItemHistory } from "../../order-item-history.model";
import { BaseFormComponent } from "../../../common/form/base-form-component";
import { DatePipe } from "@angular/common";
import { PromotionItemService } from "../../service/promotion-item.service";
import { PromotionItem } from "../../promotion-item.model";
import { SingleItemService } from "../../service/single-item.service";
import { Order } from "../../order.model";
import { OrderItemService } from "../../service/order-item.service";
import { OrderType } from "../../enums/order-type.enum";
import { OrderStatus } from "../../enums/order-status.enum";
import { RejectedOrderService } from "../../service/rejected-order.service";
import { RejectedOrder } from "../../rejected-order";
import { WarehouseService } from "../../../warehouse/service/warehouse.service";
import { VendorAdminService } from "../../../vendor-admin/service/vendor-admin.service";
import { Warehouse } from "../../../warehouse/warehouse-model";
import { OrderFulfilmentStatus } from "../../enums/order-fulfilment-status.enum";
import { ShipmentOrder } from "../../../shippment-order/model/shipment-order";
import { OrderStatusType } from "../../order-status-type";
import { OrderShippmentStatus } from "../../enums/order-shippment.status.enum";
import { ShippmentOrderService } from "../../../shippment-order/service/shippment-order.service";
import { ShipmentOrderItem } from "../../../shippment-order/model/shipment-order-item";
import { TSMap } from "typescript-map";
import { ShipmentOrderHistoryService } from "../../../shippment-order/service/shipment-order-history.service";
import { ShipmentOrderItemHistory } from "../../../shippment-order/model/shipment-order-item-history";
import { OrderRejectedReasonService } from "../../../reasons/services/reason-service";
import { OrderRejectedReasonEnum } from "../../../shippment-order/enum/order-rejected-reason-enum";
import { OrderRejectedReason } from "../../../../app/reasons/reason.model";
import { OrderStatusTypeCode } from "../../enums/order-status-type-code.enum";
import { OrderStatusHistory } from "../../order-status-history";
import { OrderStatusHistoryService } from "../../service/order-status-history.service";
import { OrderStatusService } from "../../service/order-status.service";
import { AppComponent } from "../../../app.component";

const DECIMAL_FORMAT: (v: any) => any = (v: number) => v.toFixed(2);

@Component({
  selector: "view-order-details",
  templateUrl: "./view-order-details.component.html",
  styleUrls: ["./view-order-details.component.css"],
})
export class AllOrderItemsComponent implements OnInit {
  public static sumOfQuantityToFulfilled: number;
  public static sumOfQuantityToCanceld: number;
  public static sumOfQuantityToFulfilledSingletem: number;
  public static sumOfQuantityToCanceldSingelItem: number;
  public static sumOfQuantityToFulfilledBundle: number;
  public static sumOfQuantityToCanceldBundle: number;
  public sendToWarehouseFlag: boolean = true;
  public static minDeliverItem: number = 0;
  public static maxDeliverItem: number;
  private deliveryNewItem: number;
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private _orderDetailsList: OrderDetails[] = [];
  protected loggedUser: User;
  totalOfDeliveredQty: number = 0;
  totalOfAmount: number = 0;
  quantityToDeliver: number;
  showSpinnerOnCompleteOrder: boolean = false;
  showSpinnerOnPrint: boolean = false;
  showSpinnerOnSendToWarehouse: boolean = false;
  public showSpinnerOnRejectOrder: boolean = false;
  warehouses: Warehouse[] = [];
  warehouse: Warehouse = new Warehouse();
  orderItemHistory: OrderItemHistory = new OrderItemHistory();

  orderStatusTypeList = new FormControl();
  fulfillmentStatus: OrderStatusType[] = [];
  isButtonDetailsClicked: boolean = false;
  countNotificationMenuOpen: number = 0;
  @ViewChild("pagingBar") pagingBar: TdPagingBarComponent;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;

  columnsConfig: ITdDataTableColumn[] = [
    { name: "product.name", label: "Product", width: 200 },
    { name: "totalAmount", label: "Total Amount", width: 90 },
    { name: "orderedQuantity", label: "Ordered Qty", width: 90 },
    { name: "deliverdQuantity", label: "Deliverd Qty", width: 90 },
    { name: "id", label: "", sortable: false, width: 90 },
  ];

  orderDetailsColumnConfig: ITdDataTableColumn[] = [
    {
      name: "orderType",
      label: "Order Type",
      tooltip: "order Type",
      width: 140,
    },
    { name: "name", label: "Name", tooltip: "Name" },
    {
      name: "orderdQuantity",
      label: "Ordered Qty",
      tooltip: "Ordered Qty",
      width: 90,
    },
    { name: "itemPrice", label: "Amount", tooltip: "Amount", width: 90 },
    {
      name: "deliveredQty",
      label: "Delivered Qty",
      tooltip: "Delivered Qty",
      width: 90,
    },
    {
      name: "quantityToCanceled",
      label: "Cancelled Qty",
      tooltip: "Cancelled Qty",
      width: 90,
    },
    {
      name: "deliveryQuantity",
      label: "Qty to Deliver",
      tooltip: "Qty to Deliver",
      width: 90,
    },
    {
      name: "quantityToFulfilled",
      label: "Qty Fulfilled",
      tooltip: "Qty Fulfilled",
      width: 90,
    },
    {
      name: "orderStatusType",
      label: "Fulfillment status",
      tooltip: "Fulfillment Status",
      width: 120,
    },
    {
      name: "totalAmount",
      label: "Fulfilled Amount",
      tooltip: "Fulfilled Amount",
      format: DECIMAL_FORMAT,
      width: 100,
    },
    {
      name: "id",
      label: "Details",
      tooltip: "Details",
      sortable: false,
      width: 90,
    },
  ];
  constructor(
    protected orderService: OrderService,
    protected orderItemService: OrderItemService,
    protected promotionItemService: PromotionItemService,
    protected fcmService: FcmService,
    protected ordersView: AllOrdersComponent,
    protected warehousService: WarehouseService,
    protected vendorAdminService: VendorAdminService,
    protected reasonService: OrderRejectedReasonService,
    protected orderStatusTypeService: OrderStatusService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    protected authService: AuthService,
    private datePipe: DatePipe,
    public dialog: MatDialog,
    protected shippmentOrderService: ShippmentOrderService,
    protected permissionsService: NgxPermissionsService,
    protected orderItemHistoryService: OrderItemHistoryService
  ) { }

  ngOnInit() {
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    this.initialDetails(this.orderService.order, null);
    this.loadFulfilmentStatus();
  }

  initialDetails(order: Order, orderStatus: string[]) {
    this.orderService
      .onGetOrderDetails(order, orderStatus)
      .subscribe((data) => {
        this.orderDetailsList = data;
        this.sumTotalAmountAndDeliveredQty();
        let filterData = this.orderDetailsList.filter(
          (item) => item.quantityToFulfilled != 0
        );
        if (filterData.length != 0) {
          this.sendToWarehouseFlag = false;
        }
      });
    this.dataTable.refresh();
  }

  public sumTotalAmountAndDeliveredQty() {
    this.totalOfAmount = 0;
    this.totalOfDeliveredQty = 0;
    this.orderDetailsList.forEach((obj) => {
      this.totalOfDeliveredQty += obj.quantityToFulfilled;
      this.totalOfAmount += obj.totalAmount;
    });
  }

  public onOrderStatusChanges(event) {
    if (this.orderStatusTypeList.value == null) {
      this.orderStatusTypeList.setValue([]);
    }
    this.initialDetails(
      this.orderService.order,
      this.orderStatusTypeList.value.map((item) => item.code)
    );
  }

  public loadFulfilmentStatus() {
    this.orderStatusTypeService
      .findOrderStatusTypeByCode(OrderStatusTypeCode.Fulfilment)
      .subscribe((orderStatus) => {
        this.orderStatusTypeService
          .findOrderStatusByParentId(orderStatus.id)
          .subscribe((data) => {
            this.fulfillmentStatus = data;
          });
      });
  }

  openOrderHistoryDlg(id: number, row: any) {
    this.orderService.findOrderById(
      this.orderService.order.id,
      this.orderService.order.vendor.id
    )
      .subscribe(
        data => {
          this.orderService.order = data;
        },
        error => { },
        () => {
          this.isButtonDetailsClicked = true;
          if (this.dialog.openDialogs.length > 1) {
            return;
          }
          if (row.orderType == OrderType.Product) {
            let singleItem: SingleItem = new SingleItem();
            let product: Product = new Product();
            singleItem.id = row.id;
            singleItem.price = row.price;
            singleItem.orderedQuantity = row.orderdQuantity;
            singleItem.discountedPrice = row.discountedPrice;
            singleItem.discountPercentage = row.discountPercentage;
            singleItem.discountPercentage =
              singleItem.discountPercentage + " %";
            product.name = row.name;
            singleItem.product = product;
            this.onOpenSingleDialog(singleItem);
          } else if (row.orderType == OrderType.Promotion) {
            this.openPromotionDetailsDialog(row);
          } else if (row.orderType == OrderType.Bundle) {
            let comboItem: ComboItem = new ComboItem();
            let combo: Combo = new Combo();
            comboItem.orderedQuantity = row.orderdQuantity;
            comboItem.price = row.price;
            comboItem.id = row.id;
            combo.name = row.name;
            comboItem.combo = combo;
            this.onOpenComboDialog(comboItem);
          }
          this.router.navigate(["/orders/form"]);
        }
      );
  }

  openPromotionDetailsDialog(row: any) {
    if (this.orderService.order.orderStatus.code == OrderStatus.New) {
      this.changeToAcknowledged(this.orderService.order);
    } else if (this.orderService.order.orderStatus.code == OrderStatus.Rejected) {
      this.snackBar.open("You can't fulfill a rejected order", "close", {
        duration: 3000,
      });

    } else if (this.orderService.order.orderStatus.code == OrderStatus.Canceled) {
      this.snackBar.open("You can't fulfill a canceled order", "close", {
        duration: 3000,
      });

    } else {
      let promotionItem: PromotionItem = new PromotionItem();
      this.promotionItemService
        .getAllPromotionItemsById(row.promotionDetailId)
        .subscribe(
          (data) => {
            promotionItem = data;
          },
          (err) => { },
          () => {
            let vendorId: number;
            this.vendorAdminService
              .getVendorByVendorAdminId(this.loggedUser.id)
              .subscribe(
                (data) => {
                  vendorId = data;
                },
                (err) => { },
                () => {
                  this.warehousService
                    .findWarehouseByVendorId(vendorId)
                    .subscribe(
                      (warehouses) => {
                        this.warehouses = warehouses;
                      },
                      (err) => { },
                      () => {
                        let orderItemhistory: OrderItemHistory = new OrderItemHistory();
                        this.orderItemHistoryService
                          .getSumOfQuantityToFulfilledAndCanceld(
                            row.id,
                            row.promotionDetailId
                          )
                          .subscribe(
                            (orderHistory) => {
                              orderItemhistory = orderHistory;
                            },
                            (err) => { },
                            () => {
                              let backToOrder: number;
                              this.orderItemHistoryService
                                .getSumOfBackToOrder(row.id)
                                .subscribe(
                                  (data) => {
                                    backToOrder = data;
                                  },
                                  (err) => { },
                                  () => {
                                    AllOrderItemsComponent.sumOfQuantityToFulfilled =
                                      promotionItem.orderedQuantity -
                                      (orderItemhistory.quantityToFulfilled +
                                        orderItemhistory.quantityToCanceled +
                                        backToOrder);

                                    AllOrderItemsComponent.sumOfQuantityToCanceld =
                                      promotionItem.orderedQuantity -
                                      (orderItemhistory.quantityToFulfilled +
                                        orderItemhistory.quantityToCanceled +
                                        backToOrder);

                                    this.openPromotionDialog(
                                      promotionItem,
                                      this.warehouses,
                                      orderItemhistory,
                                      backToOrder
                                    );
                                  }
                                );
                            }
                          );
                      }
                    );
                }
              );
          }
        );
    }
  }

  openPromotionDialog(
    row: any,
    warehouses: Warehouse[],
    orderItemhistory: OrderItemHistory,
    backToOrder: number
  ) {
    let dialogRef = this.dialogService.open(PromotionDetailsDialog, {
      width: "1500px",
      height: "500px",
      disableClose: true,
      data: {
        prmotionItem: row,
        promotionPrice: Math.round(row.totalAmount * 100) / 100,
        quantityToDeliver: this.quantityToDeliver,
        warehouses: warehouses,
        sumOfQuantityToFulfilled: orderItemhistory.quantityToFulfilled,
        sumOfQuantityToCanceld: orderItemhistory.quantityToCanceled,
        backToOrder: backToOrder,
        order: this.orderService.order,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.isButtonDetailsClicked = false;
      if (result != null) {
        this.orderService
          .onGetOrderDetails(this.orderService.order, null)
          .subscribe(
            (data) => {
              this.orderDetailsList = data;
              this.dataTable.data = data;
              this.dataTable.refresh();
            },
            (err) => { },
            () => {
              this.sumTotalAmountAndDeliveredQty();
              let filterData = this.orderDetailsList.filter(
                (item) => item.quantityToFulfilled != 0
              );
              if (filterData.length != 0) {
                this.sendToWarehouseFlag = false;
              }

              // this.checkClosedOrderStatus(this._orderDetailsList);
              this.checkIfAllItemCanceld(this._orderDetailsList);
            }
          );
      }
    });
  }

  onOpenSingleDialog(row: SingleItem): void {
    if (this.orderService.order.orderStatus.code == OrderStatus.New) {
      this.changeToAcknowledged(this.orderService.order);
    } else if (this.orderService.order.orderStatus.code == OrderStatus.Rejected) {
      this.snackBar.open("You can't fulfill a rejected order", "close", {
        duration: 3000,
      });
    } else if (this.orderService.order.orderStatus.code == OrderStatus.Canceled) {
      this.snackBar.open("You can't fulfill a canceled order", "close", {
        duration: 3000,
      });

    } else {
      let vendorId: number;
      this.vendorAdminService
        .getVendorByVendorAdminId(this.loggedUser.id)
        .subscribe(
          (data) => {
            vendorId = data;
          },
          (err) => { },
          () => {
            this.warehousService.findWarehouseByVendorId(vendorId).subscribe(
              (warehouses) => {
                this.warehouses = warehouses;
              },
              (err) => { },
              () => {
                let orderItemhistory: OrderItemHistory = new OrderItemHistory();
                this.orderItemHistoryService
                  .getSumOfQuantityToFulfilledAndCanceldByItemId(row.id)
                  .subscribe(
                    (orderHistory) => {
                      orderItemhistory = orderHistory;
                    },
                    (err) => { },
                    () => {
                      let backToOrder: number;
                      this.orderItemHistoryService
                        .getSumOfBackToOrder(row.id)
                        .subscribe(
                          (data) => {
                            backToOrder = data;
                          },
                          (err) => { },
                          () => {
                            AllOrderItemsComponent.sumOfQuantityToFulfilledSingletem =
                              row.orderedQuantity -
                              (orderItemhistory.quantityToFulfilled +
                                orderItemhistory.quantityToCanceled +
                                backToOrder);
                            AllOrderItemsComponent.sumOfQuantityToCanceldSingelItem =
                              row.orderedQuantity -
                              (orderItemhistory.quantityToFulfilled +
                                orderItemhistory.quantityToCanceled +
                                backToOrder);
                            this.openSingleDialog(
                              row,
                              this.warehouses,
                              orderItemhistory,
                              backToOrder
                            );
                          }
                        );
                    }
                  );
              }
            );
          }
        );
    }
  }

  onOpenComboDialog(row: ComboItem): void {
    if (this.orderService.order.orderStatus.code == OrderStatus.New) {
      this.changeToAcknowledged(this.orderService.order);
    } else if (this.orderService.order.orderStatus.code == OrderStatus.Rejected) {
      this.snackBar.open("You can't fulfill a rejected order", "close", {
        duration: 3000,
      });

    } else if (this.orderService.order.orderStatus.code == OrderStatus.Canceled) {
      this.snackBar.open("You can't fulfill a canceled order", "close", {
        duration: 3000,
      });
    } else {
      let vendorId: number;
      this.vendorAdminService
        .getVendorByVendorAdminId(this.loggedUser.id)
        .subscribe(
          (data) => {
            vendorId = data;
          },
          (err) => { },
          () => {
            this.warehousService.findWarehouseByVendorId(vendorId).subscribe(
              (warehouses) => {
                this.warehouses = warehouses;
              },
              (err) => { },
              () => {
                let orderItemhistory: OrderItemHistory = new OrderItemHistory();
                this.orderItemHistoryService
                  .getSumOfQuantityToFulfilledAndCanceldByItemId(row.id)
                  .subscribe(
                    (orderHistory) => {
                      orderItemhistory = orderHistory;
                    },
                    (err) => { },
                    () => {
                      let backToOrder: number;
                      this.orderItemHistoryService
                        .getSumOfBackToOrder(row.id)
                        .subscribe(
                          (data) => {
                            backToOrder = data;
                          },
                          (err) => { },
                          () => {
                            AllOrderItemsComponent.sumOfQuantityToFulfilledBundle =
                              row.orderedQuantity -
                              (orderItemhistory.quantityToFulfilled +
                                orderItemhistory.quantityToCanceled +
                                backToOrder);
                            AllOrderItemsComponent.sumOfQuantityToCanceldBundle =
                              row.orderedQuantity -
                              (orderItemhistory.quantityToFulfilled +
                                orderItemhistory.quantityToCanceled +
                                backToOrder);
                            this.openDialog(
                              row,
                              this.warehouses,
                              orderItemhistory,
                              backToOrder
                            );
                          }
                        );
                    }
                  );
              }
            );
          }
        );
    }
  }

  openDialog(
    row: OrderItem,
    warehouses: Warehouse[],
    orderItemhistory: OrderItemHistory,
    backToOrder: number
  ): void {
    const dialogRef = this.dialog.open(DialogComboHistory, {
      width: "1500px",
      height: "500px",
      disableClose: true,
      data: {
        comboItem: row,
        warehouses: warehouses,
        sumOfQuantityToFulfilled: orderItemhistory.quantityToFulfilled,
        sumOfQuantityToCanceld: orderItemhistory.quantityToCanceled,
        backToOrder: backToOrder,
        order: this.orderService.order,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.isButtonDetailsClicked = false;
      if (result != null) {
        this.warehouse = result.warehouse;
        this.orderService
          .onGetOrderDetails(this.orderService.order, null)
          .subscribe(
            (data) => {
              this.orderDetailsList = data;
              this.dataTable.data = data;
              this.dataTable.refresh();
            },
            (err) => { },
            () => {
              this.sumTotalAmountAndDeliveredQty();
              let filterData = this.orderDetailsList.filter(
                (item) => item.quantityToFulfilled != 0
              );
              if (filterData.length != 0) {
                this.sendToWarehouseFlag = false;
              }
              // this.checkClosedOrderStatus(this._orderDetailsList);
              this.checkIfAllItemCanceld(this._orderDetailsList);
            }
          );
      }
      this.isButtonDetailsClicked = false;
      this.dataTable.refresh();
    });
  }

  openSingleDialog(
    row: SingleItem,
    warehouses: Warehouse[],
    orderItemhistory: OrderItemHistory,
    backToOrder: number
  ): void {
    let dialogRef = this.dialog.open(DialogSingleHistory, {
      width: "1500px",
      height: "500px",
      disableClose: true,
      data: {
        singleItem: row,
        warehouses: warehouses,
        sumOfQuantityToFulfilled: orderItemhistory.quantityToFulfilled,
        sumOfQuantityToCanceld: orderItemhistory.quantityToCanceled,
        orderItemHistory: this.orderItemHistory,
        backToOrder: backToOrder,
        order: this.orderService.order,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.isButtonDetailsClicked = false;
      if (result != null) {
        this.orderService
          .onGetOrderDetails(this.orderService.order, null)
          .subscribe(
            (data) => {
              this.orderDetailsList = data;
              this.dataTable.data = data;
              this.dataTable.refresh();
            },
            (err) => { },
            () => {
              this.sumTotalAmountAndDeliveredQty();
              let filterData = this.orderDetailsList.filter(
                (item) => item.quantityToFulfilled != 0
              );
              if (filterData.length != 0) {
                this.sendToWarehouseFlag = false;
              }

              //this.checkClosedOrderStatus(this._orderDetailsList);
              this.checkIfAllItemCanceld(this._orderDetailsList);
            }
          );
      }
      this.dataTable.refresh();
    });
  }

  setDeliverdQtForOrder(row: SingleItem) {
    this.orderItemService
      .getPartialCompleteQuantityForOrder(this.orderService.order.id)
      .subscribe((data) => {
        this.setPartialComplete(this.orderService.order, data);
      });
  }
  setPartialComplete(order: Order, isPartialComplete: boolean) {
    if (isPartialComplete) {
      this.onSaveLoyaltyPointsTrx(order);
    }
  }

  onSaveLoyaltyPointsTrx(row: Order) {
    this.orderService
      .onCalculateAndSaveLoyaltyTrxPartialCompletePoints(row.id)
      .subscribe();
  }

  setDeliverdQuantityForItem(item: any): number {
    item.deliverdQuantity = 0;
    if (item.orderItemHistory != null) {
      for (let counter = 0; counter < item.orderItemHistory.length; counter++) {
        item.deliverdQuantity =
          item.deliverdQuantity + item.orderItemHistory[counter].quantity;
      }
    }
    return item.deliverdQuantity;
  }

  // checkClosedOrderStatus(orderDetailsArr: OrderDetails[]) {
  //   if (this.orderService.order.orderStatus.code != OrderStatus.Closed) {
  //     let count = 0;
  //     this._orderDetailsList.forEach((item) => {
  //       if (!(item.orderStatusType == "Fulfilled")) {
  //         ++count;
  //       }
  //     });
  //     this.closedOrder(count);
  //   }
  // }

  checkIfAllItemCanceld(orderDetailsArr: OrderDetails[]) {
    if (this.orderService.order.orderStatus.code != OrderStatus.Closed) {
      let count = 0;
      this._orderDetailsList.forEach((item) => {
        if (item.quantityToCanceled != item.orderdQuantity) {
          ++count;
        }
      });
      this.closedOrder(count);
    }
  }

  closedOrder(count: number) {
    if (count == 0) {
      this.orderService
        .onUpdateOrderStatus(OrderStatus.Closed, this.orderService.order.id)
        .subscribe(
          (closeStatus) => { },
          (err) => { },
          () => {
            this.orderService.onSendNotification(
              this.orderService.order,
              "your order has been closed !"
            );
          }
        );
    }
  }

  sendToWarehouse() {
    this.showSpinnerOnSendToWarehouse = true;
    this.dialogService
      .openConfirm({
        message:
          "Are you sure you want to send the fulfilled items to warehouse?",
        title: "Fulfilled items to warehouse confirmation",
        cancelButton: "Cancel",
        acceptButton: "OK",
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.totalOfDeliveredQty = 0;
          this.totalOfAmount = 0;
          let orderItemHistoryList: OrderItemHistory[] = [];
          let orderItemHistoryGroup: OrderItemHistory[] = [];
          let itemIds: number[] = [];
          let historyids: number[] = [];
          let hsitoryMap = new TSMap<number, OrderItemHistory[]>();
          let partialFulfilment = this._orderDetailsList.filter(
            (item) => item.quantityToFulfilled > 0
          );
          let historyList: OrderItemHistory[] = [];

          partialFulfilment.forEach((item) => {
            itemIds.push(item.id);
          });

          this.orderItemHistoryService
            .findOrderItemHistoryByItemId(itemIds)
            .subscribe(
              (data) => {
                orderItemHistoryList = data;
                orderItemHistoryList.forEach((h) => {
                  historyids.push(h.id);
                });

                orderItemHistoryList.forEach((itemHistory) => {
                  if (!hsitoryMap.has(itemHistory.warehouse.id)) {
                    historyList = [];
                    historyList.push(itemHistory);
                    hsitoryMap.set(itemHistory.warehouse.id, historyList);
                  } else {
                    historyList = [];
                    hsitoryMap.get(itemHistory.warehouse.id).push(itemHistory);
                  }
                });
              },
              (err) => { },
              () => {
                this.orderItemHistoryService
                  .findOrderItemHistoryByItemIdGroup(itemIds)
                  .subscribe(
                    (data) => {
                      orderItemHistoryGroup = data;
                    },
                    (err) => { },

                    () => {
                      orderItemHistoryGroup.forEach((itemHistory) => {
                        let shipmentOrderItemArr: ShipmentOrderItem[] = [];
                        let shippmentOrder: ShipmentOrder = new ShipmentOrder();
                        let orderStatusType: OrderStatusType = new OrderStatusType();
                        let warehouse: Warehouse = new Warehouse();
                        shippmentOrder.order = this.orderService.order;
                        warehouse.id = itemHistory.warehouse.id;
                        shippmentOrder.warehouse = warehouse;
                        orderStatusType.code =
                          OrderShippmentStatus.IN_WAREHOUSE;
                        shippmentOrder.orderStatusType;
                        shippmentOrder.deliveryAmount = itemHistory.totalAmount;
                        shippmentOrder.shipmentQty =
                          itemHistory.quantityToFulfilled;
                        const result = Array.from(
                          hsitoryMap
                            .get(itemHistory.warehouse.id)
                            .reduce(
                              (m, t) => m.set(t.orderItem.id, t.orderItem.id),
                              new Map()
                            )
                            .values()
                        );
                        result.forEach((item) => {
                          let historyIds: number[] = [];
                          let obj = partialFulfilment.find((t) => t.id == item);
                          let shipmentOrderItem: ShipmentOrderItem = new ShipmentOrderItem();
                          let orderItem: OrderItem = new OrderItem();
                          orderItem.id = obj.id;
                          shipmentOrderItem.orderItem = orderItem;
                          let historyItems = hsitoryMap
                            .get(itemHistory.warehouse.id)
                            .filter((object) => object.orderItem.id == obj.id);
                          historyItems.forEach((history) =>
                            historyIds.push(history.id)
                          );
                          shipmentOrderItem.orderHistoryIds = historyIds;
                          shipmentOrderItemArr.push(shipmentOrderItem);
                        });
                        shippmentOrder.shipmentOrderItem = shipmentOrderItemArr;
                        this.shippmentOrderService
                          .saveOrderShipment(shippmentOrder)
                          .subscribe(
                            (data) => { },
                            (err) => { },
                            () => {
                              this.orderItemHistoryService
                                .updateOrderItemHistory(historyids)
                                .subscribe(
                                  (data) => { },
                                  (err) => { },
                                  () => {
                                    itemIds.forEach((itemId) => {
                                      this.orderItemService
                                        .updateDeliveryQuantity(itemId)
                                        .subscribe(
                                          (t) => { },
                                          (err) => { },
                                          () => {
                                            this.orderService
                                              .onGetOrderDetails(
                                                this.orderService.order,
                                                null
                                              )
                                              .subscribe(
                                                (data) => {
                                                  this.orderDetailsList = data;

                                                  this.orderDetailsList.forEach(
                                                    (item) => {
                                                      item.quantityToFulfilled = 0;
                                                    }
                                                  );
                                                },

                                                (err) => { },

                                                () => {
                                                  this.dataTable.refresh();
                                                  this.sendToWarehouseFlag = true;
                                                }
                                              );
                                          }
                                        );
                                    });
                                  }
                                );
                            }
                          );
                      });
                      this.showSpinnerOnSendToWarehouse = false;
                      this.snackBar.open("Items sent successfully", "close", {
                        duration: 3000,
                      });
                    }
                  );
              }
            );
        } else {
          this.showSpinnerOnSendToWarehouse = false;
        }
      });
  }
  // onPrint() {
  // 	this.showSpinnerOnPrint = true;
  // 	if (this.orderService.order.deliveryStatus.code != ENUM_CODE_DeliveryStatus.CO) {
  // 		this.snackBar.open("The order should be completed before requesting print report.", "close", { duration: 3000 });
  // 	} else {
  // 		this.loggedUser = this.authService.getCurrentLoggedInUser();
  // 		window.open(environment.baseURL + "report/generate_report/" + this.orderService.order.id + "/" + this.loggedUser.id, "_blank");
  // 	}
  // 	this.showSpinnerOnPrint = false;
  // }

  public options = function (option, value): boolean {
    if (value != null) {
      return option.id === value.id;
    }
  };

  changeToAcknowledged(order: Order) {
    this.dialogService
      .openConfirm({
        message:
          "Are you sure you want to confirm Acknowledged the selected row?",
        title: "Acknowledge confirmation",
        cancelButton: "Cancel",
        acceptButton: "OK",
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          let orderId: number[] = [order.id];
          this.orderService
            .updateOrderStatus(OrderStatus.Acknowledged, orderId)
            .subscribe(
              (update) => { },
              (err) => { },
              () => {
                this.orderService.onSendNotification(
                  order,
                  "your order has been acknowledged !"
                );
              }
            );
        } else {
          this.snackBar.open("Cancelled", "Close", { duration: 3000 });
        }
      });
    this.isButtonDetailsClicked = false;
  }

  rejectedOrder() {
    if (
      this.orderService.order.orderStatus.code == OrderStatus.Acknowledged ||
      this.orderService.order.orderStatus.code == OrderStatus.New
    ) {
      this.showSpinnerOnRejectOrder = true;
      this.orderService.order.orderStatus;
      let rejectedOrderReason: OrderRejectedReason = new OrderRejectedReason();
      this.reasonService
        .findRejctedReasonByCode(OrderRejectedReasonEnum.RejectOrder)
        .subscribe(
          (data) => {
            rejectedOrderReason = data;
          },
          (err) => { },
          () => {
            this.findRejctedOrderReasonByParentId(rejectedOrderReason.id);
            this.showSpinnerOnRejectOrder = false;
          }
        );
    } else if (
      this.orderService.order.orderStatus.code == OrderStatus.Rejected
    ) {
      this.snackBar.open("Order is already rejected", "Close", {
        duration: 3000,
      });
    } else {
      this.snackBar.open(
        this.orderService.order.orderStatus.name + " order can't be rejected",
        "Close",
        { duration: 3000 }
      );
    }
  }

  orderHistory() {
    let dialogRef = this.dialog.open(OrderStatusHistoryDlg, {
      width: "1000px",
      height: "500px",
      disableClose: true,
      data: {},
    });
  }

  findRejctedOrderReasonByParentId(id: number) {
    let rejectedOrderReasonArr: OrderRejectedReason[] = [];
    let vendorId: number;
    this.vendorAdminService
      .getVendorByVendorAdminId(this.loggedUser.id)
      .subscribe(
        (data) => {
          vendorId = data;
        },
        (err) => { },
        () => {
          this.reasonService
            .findRejctedReasonByParentId(id, vendorId)
            .subscribe(
              (data) => {
                rejectedOrderReasonArr = data;
              },
              (err) => { },
              () => {
                this.showSpinnerOnRejectOrder = true;
                let dialogRef = this.dialog.open(RejectedOrdersDlg, {
                  width: "30%",
                  height: "450px",
                  disableClose: true,
                  data: {
                    order: this.orderService.order,
                    reason: rejectedOrderReasonArr,
                  },
                });
                dialogRef.afterClosed().subscribe((data) => {
                  this.showSpinnerOnRejectOrder = false;
                });
              }
            );
        }
      );
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get orderDetailsList(): OrderDetails[] {
    return this._orderDetailsList;
  }
  public set orderDetailsList(value: OrderDetails[]) {
    this._orderDetailsList = value;
  }
}

@Component({
  selector: "combo-history-dialog",
  templateUrl: "combo-history-dialog.html",
  styleUrls: ["./view-order-details.component.css"],
})
export class DialogComboHistory extends BaseFormComponent implements OnInit {
  fullfilmentHistoryColumnsConfig: ITdDataTableColumn[] = [
    { name: "audit.createdBy", label: "User", width: 110 },
    {
      name: "deliveryDate",
      label: "Fulfillment Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 220,
    },
    { name: "quantityToFulfilled", label: "Qty Fulfilled", width: 110 },
    { name: "quantityToCanceled", label: "Qty Canceled", width: 100 },
    { name: "", label: "Points Returned", width: 100 },
    { name: "warehouse.name", label: "Warehouse", width: 110 },
    {
      name: "shipmentOrderItem.shipmentOrder.shipmentNo",
      label: "Shipment Number",
      width: 110,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.orderStatusType.name",
      label: "Shipment Status",
      width: 120,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.audit.createdDate",
      label: "Shipment Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 220,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.deliveryUser",
      label: "Delivery User",
      width: 100,
    },
    { name: "shipmentOrderItem.id", label: "Item Serial", width: 100 },
  ];

  backToOrderColumnsConfig: ITdDataTableColumn[] = [
    { name: "audit.createdBy", label: "User", width: 120 },
    {
      name: "shipmentOrderItem.shipmentOrder.warehouse.name",
      label: "Warehouse",
      width: 210,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.shipmentNo",
      label: "Shipment Number",
      width: 100,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.orderStatusType.name",
      label: "Shipment Status",
      width: 110,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.audit.createdDate",
      label: "Shipment Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 210,
    },
    { name: "backToOrder", label: "Back to order Qty", width: 130 },
    {
      name: "audit.createdDate",
      label: "Back to order Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 210,
    },
  ];

  @ViewChild("orderItemHistoryForm") orderItemHistoryForm: NgForm;
  @ViewChild("historyDt") historyDt: TdDataTableComponent;
  @ViewChild("backToOrderDt") backToOrder: TdDataTableComponent;
  private _orderhistoryList: OrderItemHistory[] = [];
  private _orderItemHistory: OrderItemHistory = new OrderItemHistory();
  private _shipmentHistoryList: ShipmentOrderItemHistory[] = [];
  isFulfilled: boolean = true;
  isFulfilledReadOnly: boolean = false;
  quantityToCanceldEventValue: number = 0;
  quantityToFulfilledEventValue: number = 0;

  _orderItem: ComboItem;
  _isEdit = false;

  constructor(
    protected orderHistoryItemService: OrderItemHistoryService,
    protected shipmentHistoryService: ShipmentOrderHistoryService,
    protected comboItemService: ComboItemService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected snackBar: MatSnackBar,
    protected shipmentOrderService: ShippmentOrderService,
    protected orderItemService: OrderItemService,
    protected orderService: OrderService,
    private dialogService: TdDialogService,
    private datePipe: DatePipe,
    public dialogRef: MatDialogRef<DialogComboHistory>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(orderHistoryItemService, route, router, snackBar);
  }

  ngOnInit() {
    this.getAllOrderHistoryItem();
    this.getAllBackToOrderHistoryItem();

    let qtyToFulfilled =
      this.data.comboItem.orderedQuantity -
      (this.data.sumOfQuantityToFulfilled +
        this.data.sumOfQuantityToCanceld +
        this.data.backToOrder);
    if (qtyToFulfilled == 0) {
      this.isFulfilledReadOnly = true;
      this.isFulfilled = false;
      this.deliverNewItemField.disable();
      this.quantityCanceledFormControl.disable();
    } else {
      this.deliverNewItemField.enable();
      this.quantityCanceledFormControl.enable();
    }
  }

  public quantityToFulfilledChangeEvent(event) {
    if (event == 0 || event == null) {
      this._orderItemHistory.warehouse = new Warehouse();
    }

    if (event != undefined && event != null) {
      this.quantityToFulfilledEventValue = event;
      this.quantityCanceledFormControl.setValidators([
        Validators.max(
          AllOrderItemsComponent.sumOfQuantityToCanceldBundle -
          this.quantityToFulfilledEventValue
        ),
        Validators.required,
      ]);
      this.quantityCanceledFormControl.updateValueAndValidity();
    }
  }

  public quantityToCanceledChangeEvent(event) {
    if (event != undefined && event != null) {
      this.quantityToCanceldEventValue = event;
      this.deliverNewItemField.setValidators([
        Validators.max(
          AllOrderItemsComponent.sumOfQuantityToFulfilledBundle -
          this.quantityToCanceldEventValue
        ),
        Validators.required,
      ]);
      this.deliverNewItemField.updateValueAndValidity();
    }
  }

  onSave() {
    this.showSpinner = true;
    this._orderItemHistory.orderItem = new OrderItem();
    this._orderItemHistory.deliveryDate = new Date();
    if (this._orderItemHistory.quantityToFulfilled == null) {
      this._orderItemHistory.quantityToFulfilled = 0;
    }
    this._orderItemHistory.quantity = this._orderItemHistory.quantityToFulfilled;
    this._orderItemHistory.orderItem.id = this.data.comboItem.id;
    this._orderItemHistory.remark = this.data.remark;
    this._orderItemHistory.totalAmount =
      this._orderItemHistory.quantityToFulfilled * this.data.comboItem.price;

    this.orderHistoryItemService.save(this._orderItemHistory).subscribe(
      (save) => { },
      (err) => { },
      () => {
        if (
          this._orderItemHistory.quantityToFulfilled +
          this._orderItemHistory.quantityToCanceled +
          this.data.sumOfQuantityToFulfilled +
          this.data.sumOfQuantityToCanceld +
          this.data.backToOrder <
          this.data.comboItem.orderedQuantity
        ) {
          this.orderItemService
            .updateOrderItemStatus(
              OrderFulfilmentStatus.PartialFulfilmentCode,
              this.data.comboItem.id
            )
            .subscribe(
              (updatePartialFul) => { },
              (err) => { },
              () => {
                this.orderService.checkOrderStatus(this.data);
                this.dialogRef.close(this.data);
              }
            );
        } else if (
          this._orderItemHistory.quantityToFulfilled +
          this._orderItemHistory.quantityToCanceled +
          this.data.sumOfQuantityToFulfilled +
          this.data.sumOfQuantityToCanceld +
          this.data.backToOrder ==
          this.data.comboItem.orderedQuantity
        ) {
          this.orderItemService
            .updateOrderItemStatus(
              OrderFulfilmentStatus.FulfilledCode,
              this.data.comboItem.id
            )
            .subscribe(
              (object) => { },
              (err) => { },
              () => {
                if (this.data.order.orderStatus.code == OrderStatus.Acknowledged) {
                  let order: number[] = [this.orderService.order.id];
                  this.orderService.updateOrderStatus(OrderStatus.Open, order).subscribe(
                    openStatus => { },
                    err => { },
                    () => {
                      this.dialogRef.close(this.data);
                      this.showSpinner = false;
                      this.orderService.onSendNotification(this.orderService.order, "your order has been open !")
                    }
                  )
                } else {
                  this.dialogRef.close(this.data);
                }
              }
            );
        }
      }
    );
  }

  getAllOrderHistoryItem() {
    this.orderHistoryItemService
      .findAllOrderItemHistory(this.data.comboItem.id)
      .subscribe((objs) => {
        this.orderhistoryList = objs;
        if (objs.length != 0) {
          this.historyDt.refresh();
        }
      });
  }

  getAllBackToOrderHistoryItem() {
    this.shipmentHistoryService
      .findAllBackToOrderItemHistory(this.data.comboItem.id)
      .subscribe((objs) => {
        this._shipmentHistoryList = objs;
        if (objs.length != 0) {
          this.historyDt.refresh();
        }
      });
  }

  findSerialItems(row: any) {
    let shipmentSerialArr: any[] = [];
    this.shipmentOrderService
      .findShipmentSerialByShipmentId(row.shipmentOrderItem.id)
      .subscribe(
        (data) => {
          shipmentSerialArr = data;
        },
        (err) => { },
        () => {
          let dialogRef = this.dialogService.open(ShipmentSerialDlg, {
            width: "500px",
            height: "610px",
            disableClose: true,
            data: { shipmentSerialArr },
          });
        }
      );
  }

  deliverNewItemField = new FormControl("", [
    Validators.max(AllOrderItemsComponent.sumOfQuantityToFulfilledBundle),
    Validators.required,
  ]);

  quantityCanceledFormControl = new FormControl("", [
    Validators.max(AllOrderItemsComponent.sumOfQuantityToCanceldBundle),
    Validators.required,
  ]);

  onNoClick(): void {
    this.dialogRef.close();
  }

  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): ComboItem {
    return this._orderItem;
  }

  set model(orderItem: ComboItem) {
    this._orderItem = orderItem;
  }

  initModel(): ComboItem {
    return new ComboItem();
  }

  ngFormInstance(): NgForm {
    return this.orderItemHistoryForm;
  }

  public get orderItemHistory(): OrderItemHistory {
    return this._orderItemHistory;
  }
  public set orderItemHistory(value: OrderItemHistory) {
    this._orderItemHistory = value;
  }
  public get orderhistoryList(): OrderItemHistory[] {
    return this._orderhistoryList;
  }
  public set orderhistoryList(value: OrderItemHistory[]) {
    this._orderhistoryList = value;
  }

  public get shipmentHistoryList(): ShipmentOrderItemHistory[] {
    return this._shipmentHistoryList;
  }
  public set shipmentHistoryList(value: ShipmentOrderItemHistory[]) {
    this._shipmentHistoryList = value;
  }
}

@Component({
  selector: "app-promotion-details-dlg",
  templateUrl: "order-promotion-details-dlg.component.html",
  styleUrls: ["./view-order-details.component.css"],
})
export class PromotionDetailsDialog implements OnInit {
  @ViewChild("backToOrderDt") backToOrder: TdDataTableComponent;
  @ViewChild("historyDt") historyDt: TdDataTableComponent;
  private _orderhistoryList: OrderItemHistory[] = [];
  private _orderItemHistory: OrderItemHistory = new OrderItemHistory();
  private _shipmentHistoryList: ShipmentOrderItemHistory[] = [];
  isFulfilled: boolean = true;
  isFulfilledReadOnly: boolean = false;
  totalAmountAndnumberOfItemsShow: boolean = false;
  isManual: boolean = true;
  quantityToCanceldEventValue: number = 0;
  quantityToFulfilledEventValue: number = 0;
  showSpinner: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<PromotionDetailsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected orderHistoryItemService: OrderItemHistoryService,
    protected shipmentHistoryService: ShipmentOrderHistoryService,
    protected orderItemService: OrderItemService,
    protected shipmentOrderService: ShippmentOrderService,
    private datePipe: DatePipe,
    private dialogService: TdDialogService,
    protected orderService: OrderService
  ) { }

  fullfilmentHistoryColumnsConfig: ITdDataTableColumn[] = [
    { name: "audit.createdBy", label: "User", width: 110 },
    {
      name: "deliveryDate",
      label: "Fulfillment Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 220,
    },
    { name: "quantityToFulfilled", label: "Qty Fulfilled", width: 110 },
    { name: "quantityToCanceled", label: "Qty Canceled", width: 100 },
    { name: "", label: "Points Returned", width: 100 },
    { name: "warehouse.name", label: "Warehouse", width: 110 },
    {
      name: "shipmentOrderItem.shipmentOrder.shipmentNo",
      label: "Shipment Number",
      width: 110,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.orderStatusType.name",
      label: "Shipment Status",
      width: 120,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.audit.createdDate",
      label: "Shipment Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 220,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.deliveryUser",
      label: "Delivery User",
      width: 100,
    },
    { name: "shipmentOrderItem.id", label: "Item Serial", width: 100 },
  ];

  backToOrderColumnsConfig: ITdDataTableColumn[] = [
    { name: "audit.createdBy", label: "User", width: 120 },
    {
      name: "shipmentOrderItem.shipmentOrder.warehouse.name",
      label: "Warehouse",
      width: 210,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.shipmentNo",
      label: "Shipment Number.",
      width: 100,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.orderStatusType.name",
      label: "Shipment Status",
      width: 110,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.audit.createdDate",
      label: "Shipment Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 210,
    },
    { name: "backToOrder", label: "Back to order Qty", width: 130 },
    {
      name: "audit.createdDate",
      label: "Back to order Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 210,
    },
  ];

  ngOnInit() {
    this.getAllOrderHistoryItem();
    this.getAllBackToOrderHistoryItem();
    if (this.data.name == "Manual") {
      this.isManual = false;
    }

    let qtyToFulfilled =
      this.data.prmotionItem.orderedQuantity -
      (this.data.sumOfQuantityToFulfilled +
        this.data.sumOfQuantityToCanceld +
        this.data.backToOrder);

    if (qtyToFulfilled == 0) {
      this.isFulfilledReadOnly = true;
      this.isFulfilled = false;
      this.deliverNewItemField.disable();
    } else {
      this.deliverNewItemField.enable();
    }
  }

  public validateQuantityToFulfilled(): boolean {
    if (
      this._orderItemHistory.quantityToFulfilled +
      this._orderItemHistory.quantityToCanceled +
      (this.data.sumOfQuantityToFulfilled +
        this.data.sumOfQuantityToCanceld) >
      this.data.prmotionItem.orderedQuantity
    ) {
      return true;
    } else {
      return false;
    }
  }

  public quantityToFulfilledChangeEvent(event) {
    if (event == 0 || event == null) {
      this._orderItemHistory.warehouse = new Warehouse();
    }
    if (event != undefined && event != null) {
      this.quantityToFulfilledEventValue = event;
      this.quantityCanceledFormControl.setValidators([
        Validators.max(
          AllOrderItemsComponent.sumOfQuantityToCanceld -
          this.quantityToFulfilledEventValue
        ),
        Validators.required,
      ]);
      this.quantityCanceledFormControl.updateValueAndValidity();
    }
  }

  public quantityToCanceledChangeEvent(event) {
    if (event != undefined && event != null) {
      this.quantityToCanceldEventValue = event;
      this.deliverNewItemField.setValidators([
        Validators.max(
          AllOrderItemsComponent.sumOfQuantityToFulfilled -
          this.quantityToCanceldEventValue
        ),
        Validators.required,
      ]);
      this.deliverNewItemField.updateValueAndValidity();
    }
  }

  onSave() {
    this.showSpinner = true;
    this._orderItemHistory.orderItem = new OrderItem();
    this._orderItemHistory.deliveryDate = new Date();
    this._orderItemHistory.orderItem.id = this.data.prmotionItem.id;
    this._orderItemHistory.promotionDetailId = this.data.prmotionItem.promotionDetailId;
    if (this._orderItemHistory.quantityToFulfilled == null) {
      this._orderItemHistory.quantityToFulfilled = 0;
    }
    this._orderItemHistory.quantity = this._orderItemHistory.quantityToFulfilled;
    this._orderItemHistory.totalAmount =
      this._orderItemHistory.quantityToFulfilled *
      this.data.prmotionItem.totalAmount;

    if (this._orderItemHistory.warehouse.id == null) {
      this._orderItemHistory.warehouse = new Warehouse();
    }
    this.orderHistoryItemService.save(this._orderItemHistory).subscribe(
      (save) => { },
      (err) => { },
      () => {
        if (
          this._orderItemHistory.quantityToFulfilled +
          this._orderItemHistory.quantityToCanceled +
          this.data.sumOfQuantityToFulfilled +
          this.data.sumOfQuantityToCanceld +
          this.data.backToOrder <
          this.data.prmotionItem.orderedQuantity
        ) {
          this.orderItemService.updateOrderItemStatus(OrderFulfilmentStatus.PartialFulfilmentCode, this.data.prmotionItem.id).subscribe(
            (updateStatus) => { },
            (err) => { },
            () => {
              this.dialogRef.close(this.data);
              this.showSpinner = false;
              this.orderService.checkOrderStatus(this.data);
            }
          );
        } else if (
          this._orderItemHistory.quantityToFulfilled +
          this._orderItemHistory.quantityToCanceled +
          this.data.sumOfQuantityToFulfilled +
          this.data.sumOfQuantityToCanceld +
          this.data.backToOrder ==
          this.data.prmotionItem.orderedQuantity
        ) {
          this.orderItemService
            .updateOrderItemStatus(
              OrderFulfilmentStatus.FulfilledCode,
              this.data.prmotionItem.id
            )
            .subscribe(
              (object) => { },
              (err) => { },
              () => {
                if (this.data.order.orderStatus.code == OrderStatus.Acknowledged) {
                  let order: number[] = [this.orderService.order.id];
                  this.orderService.updateOrderStatus(OrderStatus.Open, order).subscribe(
                    openStatus => { },
                    err => { },
                    () => {
                      this.showSpinner = false;
                      this.dialogRef.close(this.data);
                      this.orderService.onSendNotification(this.orderService.order, "your order has been open !")
                    }
                  )
                } else {
                  this.dialogRef.close(this.data);
                }
              }
            );
        }
      }
    );
  }

  getAllBackToOrderHistoryItem() {
    this.shipmentHistoryService
      .findAllBackToOrderItemHistory(this.data.prmotionItem.id)
      .subscribe((objs) => {
        this._shipmentHistoryList = objs;
        if (objs.length != 0) {
          this.historyDt.refresh();
        }
      });
  }

  getAllOrderHistoryItem() {
    this.orderHistoryItemService
      .findAllOrderItemHistory(this.data.prmotionItem.id)
      .subscribe((objs) => {
        this.orderhistoryList = objs;
        if (objs.length != 0) {
          this.historyDt.refresh();
        }
      });
  }

  findSerialItems(row: any) {
    let shipmentSerialArr: any[] = [];
    this.shipmentOrderService
      .findShipmentSerialByShipmentId(row.shipmentOrderItem.id)
      .subscribe(
        (data) => {
          shipmentSerialArr = data;
        },
        (err) => { },
        () => {
          let dialogRef = this.dialogService.open(ShipmentSerialDlg, {
            width: "500px",
            height: "610px",
            disableClose: true,
            data: { shipmentSerialArr },
          });
        }
      );
  }

  deliverNewItemField = new FormControl("", [
    Validators.max(AllOrderItemsComponent.sumOfQuantityToFulfilled),
    Validators.required,
  ]);

  quantityCanceledFormControl = new FormControl("", [
    Validators.max(AllOrderItemsComponent.sumOfQuantityToCanceld),
    Validators.required,
  ]);

  onNoClick(): void {
    this.dialogRef.close();
  }

  public get orderhistoryList(): OrderItemHistory[] {
    return this._orderhistoryList;
  }
  public set orderhistoryList(value: OrderItemHistory[]) {
    this._orderhistoryList = value;
  }

  public get orderItemHistory(): OrderItemHistory {
    return this._orderItemHistory;
  }
  public set orderItemHistory(value: OrderItemHistory) {
    this._orderItemHistory = value;
  }

  public get shipmentHistoryList(): ShipmentOrderItemHistory[] {
    return this._shipmentHistoryList;
  }
  public set shipmentHistoryList(value: ShipmentOrderItemHistory[]) {
    this._shipmentHistoryList = value;
  }
}

@Component({
  selector: "history-dialog",
  templateUrl: "single-history-dialog.html",
  styleUrls: ["./view-order-details.component.css"],
})
export class DialogSingleHistory extends BaseFormComponent implements OnInit {
  @ViewChild("backToOrderDt") backToOrder: TdDataTableComponent;
  @ViewChild("orderItemHistoryForm") orderItemHistoryForm: NgForm;
  @ViewChild("historyDt") historyDt: TdDataTableComponent;
  private _orderItemHistory: OrderItemHistory = new OrderItemHistory();
  private _orderhistoryList: OrderItemHistory[] = [];
  private _shipmentHistoryList: ShipmentOrderItemHistory[] = [];
  isFulfilled: boolean = true;
  isFulfilledReadOnly: boolean = false;
  quantityToCanceldEventValue: number = 0;
  quantityToFulfilledEventValue: number = 0;

  fullfilmentHistoryColumnsConfig: ITdDataTableColumn[] = [
    { name: "audit.createdBy", label: "User", width: 110 },
    {
      name: "deliveryDate",
      label: "Fulfillment Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 220,
    },
    { name: "quantityToFulfilled", label: "Qty Fulfilled", width: 110 },
    { name: "quantityToCanceled", label: "Qty Canceled", width: 100 },
    { name: "", label: "Points Returned", width: 100 },
    { name: "warehouse.name", label: "Warehouse", width: 110 },
    {
      name: "shipmentOrderItem.shipmentOrder.shipmentNo",
      label: "Shipment Number",
      width: 110,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.orderStatusType.name",
      label: "Shipment Status",
      width: 120,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.audit.createdDate",
      label: "Shipment Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 220,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.deliveryUser",
      label: "Delivery User",
      width: 100,
    },
    { name: "shipmentOrderItem.id", label: "Item Serial", width: 100 },
  ];

  backToOrderColumnsConfig: ITdDataTableColumn[] = [
    { name: "audit.createdBy", label: "User", width: 120 },
    {
      name: "shipmentOrderItem.shipmentOrder.warehouse.name",
      label: "Warehouse",
      width: 210,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.shipmentNo",
      label: "Shipment Number.",
      width: 100,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.orderStatusType.name",
      label: "Shipment Status",
      width: 110,
    },
    {
      name: "shipmentOrderItem.shipmentOrder.audit.createdDate",
      label: "Shipment Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 210,
    },
    { name: "backToOrder", label: "Back to order Qty", width: 130 },
    {
      name: "audit.createdDate",
      label: "Back to order Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 210,
    },
  ];

  _orderItem: SingleItem;
  _isEdit: boolean = false;

  constructor(
    protected orderItemHistoryService: OrderItemHistoryService,
    protected singleItemService: SingleItemService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected snackBar: MatSnackBar,
    protected orderItemService: OrderItemService,
    protected shipmentHistoryService: ShipmentOrderHistoryService,
    protected orderService: OrderService,
    protected shipmentOrderService: ShippmentOrderService,
    protected datePipe: DatePipe,
    private dialogService: TdDialogService,
    protected orderHistoryItemService: OrderItemHistoryService,
    public dialogRef: MatDialogRef<DialogSingleHistory>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(orderItemHistoryService, route, router, snackBar);
  }

  ngOnInit() {
    this.getAllOrderHistoryItem();
    this.getAllBackToOrderHistoryItem();
    this._orderItem = this.data.singleItem;

    let qtyToFulfilled =
      this.data.singleItem.orderedQuantity -
      (this.data.sumOfQuantityToFulfilled +
        this.data.sumOfQuantityToCanceld +
        this.data.backToOrder);

    if (qtyToFulfilled == 0) {
      this.isFulfilledReadOnly = true;
      this.isFulfilled = false;
      this.deliverNewItemField.disable();
    } else {
      this.deliverNewItemField.enable();
    }
  }

  public quantityToFulfilledChangeEvent(event) {
    if (event == 0 || event == null) {
      this._orderItemHistory.warehouse = new Warehouse();
    }

    if (event != undefined && event != null) {
      this.quantityToFulfilledEventValue = event;
      this.quantityCanceledFormControl.setValidators([
        Validators.max(
          AllOrderItemsComponent.sumOfQuantityToCanceldSingelItem -
          this.quantityToFulfilledEventValue
        ),
        Validators.required,
      ]);
      this.quantityCanceledFormControl.updateValueAndValidity();
    }
  }

  public quantityToCanceledChangeEvent(event) {
    if (event != undefined && event != null) {
      this.quantityToCanceldEventValue = event;
      this.deliverNewItemField.setValidators([
        Validators.max(
          AllOrderItemsComponent.sumOfQuantityToFulfilledSingletem -
          this.quantityToCanceldEventValue
        ),
        Validators.required,
      ]);
      this.deliverNewItemField.updateValueAndValidity();
    }
  }

  onSave() {
    this.showSpinner = true;
    this._orderItemHistory.orderItem = new OrderItem();
    this._orderItemHistory.deliveryDate = new Date();
    if (this._orderItemHistory.quantityToFulfilled == null) {
      this._orderItemHistory.quantityToFulfilled = 0;
    }
    this._orderItemHistory.totalAmount =
      this._orderItemHistory.quantityToFulfilled *
      this.data.singleItem.discountedPrice;
    this._orderItemHistory.orderItem.id = this.data.singleItem.id;

    this._orderItemHistory.quantity = this._orderItemHistory.quantityToFulfilled;
    if (this._orderItemHistory.warehouse.id == null) {
      this._orderItemHistory.warehouse = new Warehouse();
    }

    this.orderHistoryItemService
      .save(this._orderItemHistory)
      .subscribe((temp) => {
        this.data.orderItemHistory = this._orderItemHistory;
        if (
          this._orderItemHistory.quantityToFulfilled +
          this._orderItemHistory.quantityToCanceled +
          this.data.sumOfQuantityToFulfilled +
          this.data.sumOfQuantityToCanceld +
          this.data.backToOrder <
          this.data.singleItem.orderedQuantity
        ) {
          this.orderItemService
            .updateOrderItemStatus(
              OrderFulfilmentStatus.PartialFulfilmentCode,
              this.data.singleItem.id
            )
            .subscribe(
              (object) => { },
              (err) => { },
              () => {
                this.dialogRef.close(this.data);
                this.orderService.checkOrderStatus(this.data);
                console.log(this.showSpinner)
                this.showSpinner = false;
              }
            );
        } else if (
          this._orderItemHistory.quantityToFulfilled +
          this._orderItemHistory.quantityToCanceled +
          this.data.sumOfQuantityToFulfilled +
          this.data.sumOfQuantityToCanceld +
          this.data.backToOrder ==
          this.data.singleItem.orderedQuantity
        ) {
          this.orderItemService
            .updateOrderItemStatus(
              OrderFulfilmentStatus.FulfilledCode,
              this.data.singleItem.id
            )
            .subscribe(
              (object) => { },
              (err) => { },
              () => {
                if (this.data.order.orderStatus.code == OrderStatus.Acknowledged) {
                  let order: number[] = [this.orderService.order.id];
                  this.orderService.updateOrderStatus(OrderStatus.Open, order).subscribe(
                    openStatus => { },
                    err => { },
                    () => {
                      this.dialogRef.close(this.data);
                      this.orderService.onSendNotification(this.orderService.order, "your order has been open !")
                    }
                  )
                } else {
                  this.dialogRef.close(this.data);
                }
              }
            );
        }
      });
  }

  getAllBackToOrderHistoryItem() {
    this.shipmentHistoryService
      .findAllBackToOrderItemHistory(this.data.singleItem.id)
      .subscribe((objs) => {
        this._shipmentHistoryList = objs;
        if (objs.length != 0) {
          this.historyDt.refresh();
        }
      });
  }

  deliverNewItemField = new FormControl("", [
    Validators.max(AllOrderItemsComponent.sumOfQuantityToFulfilledSingletem),
    Validators.required,
  ]);

  quantityCanceledFormControl = new FormControl("", [
    Validators.max(AllOrderItemsComponent.sumOfQuantityToCanceldSingelItem),
  ]);

  getAllOrderHistoryItem() {
    this.orderHistoryItemService
      .findAllOrderItemHistory(this.data.singleItem.id)
      .subscribe((objs) => {
        this.orderhistoryList = objs;

        if (objs.length != 0) {
          this.historyDt.refresh();
        }
      });
  }

  findSerialItems(row: any) {
    let shipmentSerialArr: any[] = [];
    this.shipmentOrderService
      .findShipmentSerialByShipmentId(row.shipmentOrderItem.id)
      .subscribe(
        (data) => {
          shipmentSerialArr = data;
        },
        (err) => { },
        () => {
          let dialogRef = this.dialogService.open(ShipmentSerialDlg, {
            width: "500px",
            height: "610px",
            disableClose: true,
            data: { shipmentSerialArr },
          });
        }
      );
  }

  public get shipmentHistoryList(): ShipmentOrderItemHistory[] {
    return this._shipmentHistoryList;
  }
  public set shipmentHistoryList(value: ShipmentOrderItemHistory[]) {
    this._shipmentHistoryList = value;
  }

  public get orderhistoryList(): OrderItemHistory[] {
    return this._orderhistoryList;
  }
  public set orderhistoryList(value: OrderItemHistory[]) {
    this._orderhistoryList = value;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): SingleItem {
    return this._orderItem;
  }

  set model(orderItem: SingleItem) {
    this._orderItem = orderItem;
  }

  initModel(): SingleItem {
    return new SingleItem();
  }

  ngFormInstance(): NgForm {
    return this.orderItemHistoryForm;
  }

  public get orderItemHistory(): OrderItemHistory {
    return this._orderItemHistory;
  }
  public set orderItemHistory(value: OrderItemHistory) {
    this._orderItemHistory = value;
  }
}

@Component({
  selector: "rejected-dlg",
  templateUrl: "rejected-orders-dlg.html",
})
export class RejectedOrdersDlg implements OnInit {
  rejectedOrderFormControl = new FormControl("", [Validators.required]);

  rejectedOrder: RejectedOrder = new RejectedOrder();

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected snackBar: MatSnackBar,
    protected orderService: OrderService,
    protected rejectedOrderService: RejectedOrderService,
    public dialogRef: MatDialogRef<RejectedOrdersDlg>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() { }

  agree(): void {
    this.rejectedOrder.order = this.data.order;
    this.rejectedOrder.orderRejectedReason = this.rejectedOrderFormControl.value;
    this.rejectedOrderService.save(this.rejectedOrder).subscribe(
      (data) => { },
      (err) => { },
      () => {
        let orderId: number[] = [this.data.order.id];
        this.orderService
          .updateOrderStatus(OrderStatus.Rejected, orderId)
          .subscribe(
            (update) => { },
            (err) => { },
            () => {
              this.orderService.onSendNotification(
                this.orderService.order,
                "your order has been rejected !"
              );
              this.dialogRef.close();
              this.snackBar.open("Order rejected successfully", "close", {
                duration: 3000,
              });
            }
          );
      }
    );
  }

  disAgree(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: "order-status-history",
  templateUrl: "order-status-history-dlg.html",
})
export class OrderStatusHistoryDlg implements OnInit {
  @ViewChild("orderStatusHistory") orderStatusHistory: TdDataTableComponent;

  orderStatusHistoryColumnsConfig: ITdDataTableColumn[] = [
    { name: "audit.createdBy", label: "User", width: 200 },
    { name: "orderStatusType.name", label: "Status", width: 200 },
    {
      name: "audit.createdDate",
      label: "Status Date",
      format: (v) => this.datePipe.transform(v, "medium"),
      width: 250,
    },
    { name: "orderRejectedReason.name", label: "reason", width: 250 },
  ];

  private _orderStatusHistoryArr: OrderStatusHistory[] = [];
  private _vendorId: number;
  private _loggedUser: User;

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected snackBar: MatSnackBar,
    private datePipe: DatePipe,
    protected vendorAdminService: VendorAdminService,
    protected authService: AuthService,
    protected orderService: OrderService,
    protected orderStatusHistoryService: OrderStatusHistoryService,
    public dialogRef: MatDialogRef<OrderStatusHistoryDlg>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this._loggedUser = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService
      .getVendorByVendorAdminId(this.loggedUser.id)
      .subscribe(
        (data) => {
          this._vendorId = data;
        },
        (err) => { },
        () => {
          this.findAllOrderHistoryByVendor();
        }
      );
  }

  public findAllOrderHistoryByVendor() {
    this.orderStatusHistoryService
      .findAllOrderStatusHisotryByOrder(this.orderService.order.id)
      .subscribe(
        (data) => {
          this.orderStatusHistoryArr = data;
        },
        (err) => { },
        () => { }
      );
  }

  close() {
    this.dialogRef.close();
  }

  public get vendorId(): number {
    return this._vendorId;
  }
  public set vendorId(value: number) {
    this._vendorId = value;
  }

  public get orderStatusHistoryArr(): OrderStatusHistory[] {
    return this._orderStatusHistoryArr;
  }
  public set orderStatusHistoryArr(value: OrderStatusHistory[]) {
    this._orderStatusHistoryArr = value;
  }

  public get loggedUser(): User {
    return this._loggedUser;
  }
  public set loggedUser(value: User) {
    this._loggedUser = value;
  }
}

@Component({
  selector: "shipment-serial-dlg",
  templateUrl: "shipment-serial-dlg.html",
})
export class ShipmentSerialDlg implements OnInit, AfterViewInit {
  @ViewChild("shipmentSerialDt") shipmentSerialDt: TdDataTableComponent;

  private _shipmentItemSerialArr: any[] = [];

  shipmentSerialColumnConfig: ITdDataTableColumn[] = [
    { name: "serial", label: "Serial Number", hidden: true, width: 400 },
    { name: "fromSerial", label: "From Serial", hidden: true, width: 200 },
    { name: "toSerial", label: "To Serial", hidden: true, width: 200 },
    { name: "quantity", label: "Serial Number", hidden: true, width: 400 },
  ];

  constructor(
    public dialogRef: MatDialogRef<ShipmentSerialDlg>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected shipmentOrderService: ShippmentOrderService
  ) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this._shipmentItemSerialArr = this.data.shipmentSerialArr;
    if (this._shipmentItemSerialArr.length > 0) {
      this.shipmentSerialDt.columns.filter((columns) => {
        if (this._shipmentItemSerialArr[0].shipmentSerialType.code == "SS") {
          if (columns.name == "serial") {
            columns.hidden = false;
          }
        } else if (
          this._shipmentItemSerialArr[0].shipmentSerialType.code == "RS"
        ) {
          if (columns.name == "fromSerial") {
            columns.hidden = false;
          }

          if (columns.name == "toSerial") {
            columns.hidden = false;
          }
        } else if (
          this._shipmentItemSerialArr[0].shipmentSerialType.code == "QS"
        ) {
          if (columns.name == "quantity") {
            columns.hidden = false;
          }
        }
      });
    }

    this.shipmentSerialDt.refresh();
  }

  public close() {
    this.dialogRef.close();
  }

  public get shipmentItemSerialArr(): any[] {
    return this._shipmentItemSerialArr;
  }
  public set shipmentItemSerialArr(value: any[]) {
    this._shipmentItemSerialArr = value;
  }
}
