import { BaseViewComponent } from "../../../common/view/base-view-component";
import {
  TdDataTableSortingOrder,
  ITdDataTableColumn,
  IPageChangeEvent
} from "@covalent/core";
import {
  TdPagingBarComponent,
  TdDialogService,
  TdDataTableComponent
} from "@covalent/core";
import { Component, ViewChild, OnInit } from "@angular/core";
import { MatSnackBar, MatOption } from "@angular/material";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import { Order } from "../../order.model";
import { OrderService } from "../../service/order.service";
import {
  environment,
  ENUM_CODE_DeliveryStatus
} from "../../../../environments/environment";
import { NgxPermissionsService } from "ngx-permissions";
import { ProductLoyaltyService } from "../../../loyalty/service/product-loyalty.service";
import { User } from "../../../auth/user.model";
import { AuthService } from "../../../auth/service/auth.service";
import { VendorAdminService } from "../../../vendor-admin/service/vendor-admin.service";
import { FcmService } from "../../../fcm/services/fcm.service";
import { PosUser } from "../../../pos-user/pos-user.model";
import { SalesAgent } from "../../../sales-agent/sales-agent.model";
import { SalesAgentService } from "../../../sales-agent/service/sales-agent.service";
import { RegionService } from "../../../location/region/service/region.service";
import { Region } from "../../../location/region/Region.model";
import { DeliveryStatus } from "../../delivery-status.model";
import { DeliveryStatusService } from "../delevery-status-service";
import { OrderStatusType } from "../../order-status-type";
import { OrderStatusTypeCode } from "../../enums/order-status-type-code.enum";
import { OrderStatusService } from "../../service/order-status.service";
import { OrderStatus } from "../../enums/order-status.enum";
import { FormControl } from "@angular/forms";

const DECIMAL_FORMAT: (v: any) => any = (v: number) => v.toFixed(2);

@Component({
  selector: "view-orders",
  templateUrl: "./view-orders.component.html",
  styleUrls: ["./view-orders.component.css"]
})
export class AllOrdersComponent extends BaseViewComponent implements OnInit {
  private check: boolean = false;
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private user: User;
  private _allOrders: Order[] = [];
  private _errors: any = "";
  private _selectedRows: Order[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = "";
  private _sortOrder: TdDataTableSortingOrder =
    TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = "";
  protected loggedUser: User;
  private startDate: any;
  private endDate: any;
  private _pageNum: number = 1;
  private _filterFlag: boolean = false;
  private vendorId: number;
  private salesAgent: SalesAgent = new SalesAgent();
  private _listOfDeliveryStatus: DeliveryStatus[] = [];
  private _listOfSalesAgents: SalesAgent[];
  private _orderStatusArr: OrderStatusType[] = [];
  orderStatusType: String[] = [];
  orderStatusTypeList = new FormControl();

  @ViewChild("pagingBar") pagingBar: TdPagingBarComponent;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;
  @ViewChild("allOrderStatus") allOrderStatusSelected: MatOption;

  columnsConfig: ITdDataTableColumn[] = [
    { name: "orderNumber", label: "Order #", tooltip: "Order #", width: 100 },
    { name: "posUser.name", label: "Point of Sale", tooltip: "Point of Sale" },
    {
      name: "audit.createdDate",
      label: "Order Date",
      format: v => this.datePipe.transform(v, "medium"),
      tooltip: "Order date",
      width: 240
    },
    {
      name: "totalAmount",
      label: "Total Amount",
      tooltip: "Total Amount",
      format: DECIMAL_FORMAT,
      width: 100
    },
    {
      name: "orderStatus.name",
      label: "Order Status",
      tooltip: "Order Status",
      width: 180
    },
    {
      name: "id",
      label: "Order Details",
      tooltip: "Order Details",
      sortable: false,
      width: 120
    }
  ];

  constructor(
    protected orderService: OrderService,
    protected fcmService: FcmService,
    protected vendorAdminService: VendorAdminService,
    protected productLoyaltyService: ProductLoyaltyService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected authService: AuthService,
    protected snackBar: MatSnackBar,
    private datePipe: DatePipe,
    protected permissionsService: NgxPermissionsService,
    protected salesAgentService: SalesAgentService,
    protected regionService: RegionService,
    protected orderStatusTypeService: OrderStatusService
  ) {
    super(orderService, router, dialogService, snackBar);
  }

  ngOnInit() {
    super.ngOnInit();
    this.permissionsService
      .hasPermission("ROLE_ORDER_DETAIL")
      .then((result: boolean) => {
        if (!result) {
          for (let i = 0; i < this.dataTable.columns.length; i++) {
            if (this.dataTable.columns[i].name == "id") {
              this.dataTable.columns.splice(i, 1);
            }
          }
        }
      });

    this.loadSalesAgents();
    this.loadOrderStatus();
  }

  filterByVendor(
    pageNumber: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    vendorId: number
  ) {
    debugger;
    if (this.orderStatusTypeList.value == null) {
      this.orderStatusTypeList.setValue([]);
    }

    this.orderService
      .getAllOrdersByVendorId(
        pageNumber,
        pageSize,
        vendorId,
        searchTerm,
        sortBy,
        sortOrder,
        this.startDate,
        this.endDate,
        this.salesAgent.id,
        this.orderStatusTypeList.value.map(item => item.code).length == 0
          ? null
          : this.orderStatusTypeList.value.map(item => item.code)
      )
      .subscribe(data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"];
      });
    this.vendorId = vendorId;
  }

  public startDateChange(event: any) {
    this._filterFlag = true;
    this.startDate = this.datePipe.transform(event.value, "yyyy-MM-dd");
    // if (this.startDate == null) {
    //   this.getPagingBar().navigateToPage(1);
    // }
  }

  public endDateChange(event: any) {
    this.endDate = this.datePipe.transform(event.value, "yyyy-MM-dd");
  }

  public selectAll(test) {}

  public onOrderStatusChanges(event) {
    if (this.orderStatusTypeList.value == null) {
      this.orderStatusTypeList.setValue([]);
    }
    this.orderService
      .getAllOrdersByVendorId(
        this.pageNum - 1,
        this.pageSize,
        this.vendorId,
        this.searchTerm,
        this.sortBy,
        this.sortOrder,
        this.startDate,
        this.endDate,
        this.salesAgent.id,
        this.orderStatusTypeList.value.map(item => item.code).length == 0
          ? null
          : this.orderStatusTypeList.value.map(item => item.code)
      )
      .subscribe(data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"];
      });
    this.getPagingBar().navigateToPage(1);
    this.filterOrdersByDateHandler();
  }

  public loadOrderStatus() {
    this.orderStatusTypeService
      .findOrderStatusTypeByCode(OrderStatusTypeCode.Order)
      .subscribe(orderStatus => {
        this.orderStatusTypeService
          .findOrderStatusByParentId(orderStatus.id)
          .subscribe(data => {
            this._orderStatusArr = data;
          });
      });
  }

  public paging(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.pageSize = pagingEvent.pageSize;
    this.pageNum = pagingEvent.page;
    this.filterOrdersByDateHandler();
  }

  public determinePageMethod(pagingEvent: IPageChangeEvent) {
    if (this.startDate != null) {
      this.paging(pagingEvent);
    } else {
      this.page(pagingEvent);
    }
  }

  onAcknowledge() {
    if (this.selectedRows.length > 0) {
      this.dialogService
        .openConfirm({
          message:
            "Are you sure you want to confirm Acknowledged the selected row(s)?",
          title: "Acknowledge confirmation",
          cancelButton: "Disagree",
          acceptButton: "Agree"
        })
        .afterClosed()
        .subscribe((accept: boolean) => {
          if (accept) {
            if (
              this.selectedRows.filter(
                item => item.orderStatus.code != OrderStatus.New
              ).length > 0
            ) {
              this.snackBar.open("One or more orders are not new.", "close", {
                duration: 3000
              });
            } else {
              this.orderService
                .updateOrderStatus(
                  OrderStatus.Acknowledged,
                  this.selectedRows.map(item => item.id)
                )
                .subscribe(
                  data => {},
                  err => {},
                  () => {
                    this.onRefresh();
                    this._selectedRows.forEach(order => {
                      this.orderService.onSendNotification(
                        order,
                        "your order has been acknowledged !"
                      );
                    });
                    this._selectedRows = [];
                  }
                );
            }
          }
        });
    } else if (this.allModels.length == 0) {
      this.onNoDataFound();
    } else {
      this.onNoSelectedRows();
    }
  }

  handleFilterEvent() {
    if (this.startDate > this.endDate) {
      this.snackBar.open("End date must be after than start date.", "close", {
        duration: 3000
      });
      return;
    }
    if (this._filterFlag) {
      this.getPagingBar().navigateToPage(1);
    }

    this.filterOrdersByDateHandler();
  }

  filterOrdersByDateHandler() {
    if (this.orderStatusTypeList.value == null) {
      this.orderStatusTypeList.setValue([]);
    }
    this.pageSize = this.getPagingBar().pageSize;
    this.orderService
      .getAllOrdersByVendorId(
        this.pageNum - 1,
        this.pageSize,
        this.vendorId,
        this.searchTerm,
        this.sortBy,
        this.sortOrder,
        this.startDate,
        this.endDate,
        this.salesAgent.id,
        this.orderStatusTypeList.value.map(item => item.code).length == 0
          ? null
          : this.orderStatusTypeList.value.map(item => item.code)
      )
      .subscribe(data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"];
      });
  }

  onNoDataFound() {
    this.snackBar.open("No data found.", "close", { duration: 3000 });
  }

  onNoSelectedRows() {
    this.snackBar.open("No selected rows.", "close", { duration: 3000 });
  }

  loadSalesAgents() {
    let user = this.vendorAdminService.getCurrentLoggedInUser();
    if (user.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin) {
      this.vendorAdminService
        .getVendorByVendorAdminId(user.id)
        .subscribe(vendorId => {
          this.vendorId = vendorId;
          this.salesAgentService
            .getSalesAgentsByVendorId(this.vendorId)
            .subscribe(data => {
              this.listOfSalesAgents = data;
            });
        });
    }
  }

  getSalesAgentRegions(event) {
    if (this.orderStatusTypeList.value == null) {
      this.orderStatusTypeList.setValue([]);
    }
    this.salesAgent.id = event.value;
    this.orderService
      .getAllOrdersByVendorId(
        this.pageNum - 1,
        this.pageSize,
        this.vendorId,
        this.searchTerm,
        this.sortBy,
        this.sortOrder,
        this.startDate,
        this.endDate,
        this.salesAgent.id,
        this.orderStatusTypeList.value.map(item => item.code).length == 0
          ? null
          : this.orderStatusTypeList.value.map(item => item.code)
      )
      .subscribe(data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"];
      });
    this.getPagingBar().navigateToPage(1);
  }

  showOrderDetails(orderId: number, row: Order) {
    this.orderService.order = row;
  }

  public options = function(option, value): boolean {
    if (value != null) {
      return option.id === value.id;
    }
  };

  // onPrint(orderId: number, row: Order) {
  //   if (row.deliveryStatus.code != ENUM_CODE_DeliveryStatus.CO) {
  //     this.snackBar.open("The order should be completed before requesting print report.", "close", { duration: 3000 });
  //   } else {
  //     this.orderService.order = row;
  //     this.loggedUser = this.authService.getCurrentLoggedInUser();
  //     window.open(environment.baseURL + "report/generate_report/" + this.orderService.order.id + "/" + this.loggedUser.id, "_blank");
  //   }

  // }

  public get pageNum() {
    return this._pageNum;
  }

  public get filterFlag() {
    return this._filterFlag;
  }

  public set filterFlag(value: boolean) {
    this._filterFlag = value;
  }

  public set pageNum(value: number) {
    this._pageNum = value;
  }
  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get allModels(): Order[] {
    return this._allOrders;
  }

  public set allModels(value: Order[]) {
    this._allOrders = value;
  }

  public get errors(): any {
    return this._errors;
  }

  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): Order[] {
    return this._selectedRows;
  }

  public set selectedRows(value: Order[]) {
    this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }

  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }

  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get searchTerm(): string {
    return this._searchTerm;
  }

  public set searchTerm(value: string) {
    this._searchTerm = value;
  }

  public get orderStatusArr(): OrderStatusType[] {
    return this._orderStatusArr;
  }
  public set orderStatusArr(value: OrderStatusType[]) {
    this._orderStatusArr = value;
  }

  public get listOfDeliveryStatus(): DeliveryStatus[] {
    return this._listOfDeliveryStatus;
  }
  public set listOfDeliveryStatus(value: DeliveryStatus[]) {
    this._listOfDeliveryStatus = value;
  }

  public get listOfSalesAgents(): SalesAgent[] {
    return this._listOfSalesAgents;
  }
  public set listOfSalesAgents(value: SalesAgent[]) {
    this._listOfSalesAgents = value;
  }
}
