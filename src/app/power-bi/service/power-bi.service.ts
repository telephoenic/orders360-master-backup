import { BaseModel } from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from "../../../environments/environment";
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { NgxPowerBiService } from 'ngx-powerbi';
import { PowerBIDTO } from '../power-bi-dto.model';

const KEY_POWER_BI_ACCESS_TOKEN = "powerBiAccessToken";
const KEY_POWER_BI_REPORT_EMBED_URL = "https://app.powerbi.com/reportEmbed?";

@Injectable()
export class PowerBIService extends BaseService {
  private ngxPowerBiService: NgxPowerBiService;

  constructor(http: HttpClient, protected authService: AuthService) {
    super(http, "power_bi", authService)
    this.ngxPowerBiService = new NgxPowerBiService();
  }

  embedReport(powerBiDto: PowerBIDTO) {
    this.validateInputs(powerBiDto);
    let cachedAccessToken = localStorage.getItem(KEY_POWER_BI_ACCESS_TOKEN);

    if (cachedAccessToken == 'undefined' || this.isTokenExpired(cachedAccessToken)) {
      let newAccessToken;
      this.http.get(environment.baseURL + "power_bi/access_token", { responseType: 'text' }).subscribe(
        data => {
          newAccessToken = data
        },
        (errorResponse: HttpErrorResponse) => {
          this.onError("Cannot generate new access token {} - " + errorResponse)
        },
        () => {
          console.log("generate new token...");
          this.onCompleteFetchAccessToken(powerBiDto, newAccessToken);
        }
      );
    } else {
      console.log("use cached token...");
      this.onCompleteFetchAccessToken(powerBiDto, cachedAccessToken);
    }
  }

  private validateInputs(powerBiDto: PowerBIDTO) {
    if (powerBiDto.pbiContainerElement == undefined ||
      powerBiDto.reportId == undefined ||
      powerBiDto.groupId == undefined) {

      throw new Error("data are incomplete.");
    }
  }

  private onCompleteFetchAccessToken(powerBiDto: PowerBIDTO, accessToken: string) {
    localStorage.setItem(KEY_POWER_BI_ACCESS_TOKEN, accessToken);
    let reportConfig = this.prepareConfig(powerBiDto.reportId, 
                                          powerBiDto.groupId, 
                                          powerBiDto.filterName,
                                          powerBiDto.vendorId,
                                          accessToken);
    this.ngxPowerBiService.embed(powerBiDto.pbiContainerElement, reportConfig);
  }

  private isTokenExpired(token: string) {
    const helper = new JwtHelperService();
    console.log("is token expired -> "+ helper.isTokenExpired(token));
    return helper.isTokenExpired(token);
  }

  private prepareConfig(reportId: string, 
                        groupId: string, 
                        filterName: string, 
                        vendorId: number, 
                        accessToken: string) {
    return {
      type: 'report',
      id: reportId,
      embedUrl: KEY_POWER_BI_REPORT_EMBED_URL + 
                'reportId=' + reportId + 
                '&groupId=' + groupId + 
                "&filter=VPos/vendor_id eq " + vendorId +
                " and VOrder/vendor_id eq " + vendorId +
                " and VOrder_item/vendor_id eq " + vendorId,
                //"&filter=" + filterName + " eq " + vendorId,
                
      accessToken: accessToken
    };
  }

  onError(errorMessage: string) {
    console.log("error - " + errorMessage); //Should be changed to error message
  }

  get model() {
    return null;
  }

  set model(model: BaseModel) {
  }
}