export class PowerBIDTO {
    pbiContainerElement: HTMLElement;
    reportId: string;
    groupId: string;
    filterName: string;
    vendorId: number;
    accessToken: string;

    constructor(pbiContainerElement, reportId, groupId, filterName) {
        this.pbiContainerElement = pbiContainerElement;
        this.reportId = reportId;
        this.groupId = groupId;
        this.filterName = filterName;
    }
  }