import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { PrivilegeProfile } from '../privilege-profile';
import { Http } from '@angular/http';
import { BaseModel } from '../../common/model/base-model';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class PrivilegeProfileService extends BaseService {

  privilegeProfile:PrivilegeProfile;
  navigateToAfterCompletion: string;

  constructor(protected http:HttpClient,
    protected authService:AuthService) { 
    super(http, "privilege", authService);
  }


  getProfilesForVendor(pageNumber:number,pageSize:number,vendorId:number,searchTerm:string, sortBy:string,sortOrder:string ):Observable<any>{
    return this.http.get(environment.baseURL + "privilege/find_profile_for_vendor/"+ pageNumber+"/"+pageSize+"/"+vendorId, 
    { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder), 
      headers: this.getBasicHeaders()});
 }


  

  get model(){
    return this.privilegeProfile;
  }
  set model(model:BaseModel) {
    this.privilegeProfile = <PrivilegeProfile>model;
  }

}
