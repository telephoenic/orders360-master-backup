import { Component, OnInit, ViewChild } from "@angular/core";
import { BaseFormComponent } from "../../common/form/base-form-component";
import { PrivilegeProfileService } from "../service/privilege-profile.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { PrivilegeProfile } from "../privilege-profile";
import { NgForm } from "@angular/forms";
import { Application } from "../../structure/application/application";
import { Module } from "../../structure/module/module";
import { View } from "../../structure/view/view";
import { Block } from "../../structure/block/block";
import { ApplicationService } from "../../structure/application/application.service";
import { environment } from "../../../environments/environment";
import { ApplicationRoleService } from "../../role/application/application-role.service";
import { ModuleRoleService } from "../../role/module/module-role.service";
import { ViewRoleService } from "../../role/view/view-role.service";
import { BlockRoleService } from "../../role/block/block-role.service";
import { ApplicationRole } from "../../role/application/application-role";
import { ModuleRole } from "../../role/module/module-role";
import { ViewRole } from "../../role/view/view-role";
import { BlockRole } from "../../role/block/block-role";
import { TdDialogService, TdStepComponent } from "@covalent/core";
import { Output } from "@angular/core/src/metadata/directives";
import { EventEmitter } from "@angular/core";
import { NgModel } from "@angular/forms";
import { AllModuleRolesByAppComponent } from "../aware/module-role/views/all-module-roles-by-app/all-module-roles-by-app.component";
import { PrivilegeProfileModuleRole } from "../aware/module-role/privilege-profile-module-role";
import { AllViewRolesForModulesComponent } from "../aware/view-role/views/all-view-roles-for-modules/all-view-roles-for-modules.component";
import { AllBlockRolesForModulesComponent } from "../aware/block-role/views/all-block-roles-for-modules/all-block-roles-for-modules.component";
import { PrivilegeProfileViewRole } from "../aware/view-role/privilege-profile-view-role";
import { PrivilegeProfileBlockRole } from "../aware/block-role/privilege-profile-block-role";
import { BaseViewComponent } from "../../common/view/base-view-component";
import { UserType } from "../../lookups/user-type.model";
import { UserTypeService } from "../../lookups/service/user-type.service";
import { User } from "../../auth/user.model";
import { AuthService } from "../../auth/service/auth.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { Vendor } from "../../vendor/vendor.model";
import { TSMap } from "typescript-map";
import { forEach } from "@angular/router/src/utils/collection";
import { UserNotificationService } from "../../user-notification/user-notification.service";
import { AppComponent } from "../../app.component";

@Component({
  selector: "privilege-profile-form",
  templateUrl: "./privilege-profile-form.component.html",
  styleUrls: ["./privilege-profile-form.component.css"]
})
export class PrivilegeProfileFormComponent extends BaseFormComponent
  implements OnInit {
  selectedValue: any;

  @ViewChild("selectValue")
  selectValue: any;

  @ViewChild("modulesStep")
  modulesStep: TdStepComponent;

  @ViewChild("viewsBlocksStep")
  viewsBlocksStep: any;

  @ViewChild("appList")
  appListField: NgModel;

  @ViewChild("privilegeProfileForm")
  privilegeProfileForm: NgForm;

  _privilegeProfile: PrivilegeProfile;

  protected loggedUser: User;

  @ViewChild("allNotSelectedModuleRolesByApp")
  allNotSelectedModuleRolesByApp: AllModuleRolesByAppComponent;

  @ViewChild("allSelectedModuleRolesByApp")
  allSelectedModuleRolesByApp: AllModuleRolesByAppComponent;

  //@ViewChild("allNotSelectedViewRolesForModules")
  //allNotSelectedViewRolesForModules:AllViewRolesForModulesComponent;

  @ViewChild("allSelectedViewRolesForModules")
  allSelectedViewRolesForModules: AllViewRolesForModulesComponent;

  @ViewChild("allNotSelectedBlockRolesForModules")
  allNotSelectedBlockRolesForModules: AllBlockRolesForModulesComponent;

  @ViewChild("allSelectedBlockRolesForModules")
  allSelectedBlockRolesForModules: AllBlockRolesForModulesComponent;

  _isEdit: boolean = false;
  listOfAppRoles: ApplicationRole[];
  userTypeList: any[] = [];
  userType: UserType = new UserType();
  previousSelectAppId: number;
  vendorId: number;
  tempMap = new TSMap<string, PrivilegeProfileBlockRole[]>();

  constructor(
    protected privilegeProfileService: PrivilegeProfileService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected snackBar: MatSnackBar,
    private userTypeService: UserTypeService,
    private appRoleService: ApplicationRoleService,
    private authService: AuthService,
    private moduleRoleService: ModuleRoleService,
    private viewRoleService: ViewRoleService,
    private blockRoleService: BlockRoleService,
    protected vendorAdminService: VendorAdminService,
    private dialog: TdDialogService,
    private userNotificationService: UserNotificationService
  ) {
    super(privilegeProfileService, route, router, snackBar);
  }

  ngOnInit() {
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    if (this.route.snapshot.queryParams["edit"] == "1") {
      this.isEdit = true;
      this._privilegeProfile = this.privilegeProfileService.privilegeProfile;
      this.previousSelectAppId = this._privilegeProfile.applicationRole.id;
      this.loadUserType(this.previousSelectAppId);
    } else {
      this._privilegeProfile = new PrivilegeProfile();
    }
    this.loadApplicationRoleOptions();

    this.vendorAdminService
      .getVendorByVendorAdminId(this.loggedUser.id)
      .subscribe(vendorId => {
        this.vendorId = vendorId;
      });
  }

  loadApplicationRoleOptions() {
    this.appRoleService
      .filter(
        0,
        environment.maxListFieldOptions,
        "",
        "",
        "",
        undefined,
        undefined
      )
      .subscribe(data => {
        this.listOfAppRoles = data["content"];
        if (this.isEdit) {
          this.allSelectedModuleRolesByApp.filterModuleRolesByAppRole(
            this._privilegeProfile,
            this.loadSelectedViewsAndBlocks,
            this
          );
          this.allNotSelectedModuleRolesByApp.filterModuleRolesByAppRole(
            this._privilegeProfile
          );
        }
      });
  }

  loadSelectedViewsAndBlocks() {
    /* this.allSelectedViewRolesForModules.filterViewRoleByAppAndModules( this._privilegeProfile, 
        this.allSelectedModuleRolesByApp.privilegeProfileModRole);*/

    this.allSelectedBlockRolesForModules.filterBlockRoleByAppAndModules(
      this._privilegeProfile,
      this.allSelectedModuleRolesByApp.privilegeProfileModRole,
      this._isEdit
    );
  }

  loadUserType(appId: number) {
    this.userTypeService
      .findUserTypeByApplicationId(appId, this.loggedUser.type)
      .subscribe(data => {
        this.userTypeList = data;
      });
  }

  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): PrivilegeProfile {
    return this._privilegeProfile;
  }

  set model(privilegeProfile: PrivilegeProfile) {
    this._privilegeProfile = privilegeProfile;
  }

  initModel(): PrivilegeProfile {
    let privilegeProfile = new PrivilegeProfile();

    return privilegeProfile;
  }

  ngFormInstance(): NgForm {
    return this.privilegeProfileForm;
  }

  onUserTypeChange(event) {
    this._privilegeProfile.user.id = event.value;
    this.allSelectedModuleRolesByApp.filterModuleRolesByAppRole(
      this._privilegeProfile
    );
    this.allNotSelectedModuleRolesByApp.filterModuleRolesByAppRole(
      this._privilegeProfile
    );
  }

  onAppRoleChange(event) {
    if (
      this.previousSelectAppId == undefined ||
      (this.previousSelectAppId != undefined &&
        this.allSelectedModuleRolesByApp.allModels.length == 0)
    ) {
      this.allNotSelectedModuleRolesByApp.allModels = [];
      this.loadUserType(event.value);

      // this.allSelectedModuleRolesByApp.filterModuleRolesByAppRole(this._privilegeProfile);
      // this.allNotSelectedModuleRolesByApp.filterModuleRolesByAppRole(this._privilegeProfile);
    } else {
      this.dialog
        .openConfirm({
          message:
            "Note: Changing the application will clear all previously selected roles. Are you sure you want to change the application?",
          title: "confirmation",
          cancelButton: "No",
          acceptButton: "Yes"
        })
        .afterClosed()
        .subscribe((accept: boolean) => {
          if (accept) {
            this._privilegeProfile.applicationRole.id = this.appListField.value;
            this.previousSelectAppId = this._privilegeProfile.applicationRole.id;

            this.allNotSelectedModuleRolesByApp.filterModuleRolesByAppRole(
              this._privilegeProfile
            );
            this.allSelectedModuleRolesByApp.filterModuleRolesByAppRole(
              this._privilegeProfile
            );

            this.removeFrom(this.allSelectedModuleRolesByApp, {
              rows: this.allSelectedModuleRolesByApp.allModels,
              movedAsAssigned: false
            });

            /*this.removeFrom(this.allSelectedViewRolesForModules, {rows: this.allSelectedViewRolesForModules.allModels, 
                                                                movedAsAssigned: false})*/
            this.removeFrom(this.allSelectedBlockRolesForModules, {
              rows: this.allSelectedBlockRolesForModules.allModels,
              movedAsAssigned: false
            });

            this.modulesStep.open();
          } else {
            this.resetPreviouslySelectedAppRole();
          }
        });
    }
  }

  resetPreviouslySelectedAppRole() {
    this._privilegeProfile.applicationRole.id = this.previousSelectAppId;
    this.previousSelectAppId = this._privilegeProfile.applicationRole.id;
  }

  onSelectingModule(event) {
    debugger;
    if (event.movedAsAssigned) {
      debugger;
      this.pushTo(this.allSelectedModuleRolesByApp, event);
      this.removeFrom(this.allNotSelectedModuleRolesByApp, event);

      this.allNotSelectedModuleRolesByApp.dataTable.refresh();
      this.allSelectedModuleRolesByApp.dataTable.refresh();
    } else {
      debugger;
      this.pushTo(this.allNotSelectedModuleRolesByApp, event);
      this.removeFrom(this.allSelectedModuleRolesByApp, event);

      this.removeRelatedAssignedViewsOrBlocks(event);

      this.allNotSelectedModuleRolesByApp.dataTable.refresh();
      this.allSelectedModuleRolesByApp.dataTable.refresh();
      this.allSelectedBlockRolesForModules.dataTable.refresh();
      //this.allSelectedViewRolesForModules.dataTable.refresh();
    }
  }
  //(<PrivilegeProfileModuleRole>event.rows[moduleIdx]).moduleRole.module.id;
  removeRelatedAssignedViewsOrBlocks(event) {
    if (event.rows) {
      for (let moduleIdx = 0; moduleIdx < event.rows.length; moduleIdx++) {
        let moduleId = event.rows[moduleIdx].moduleRole.module.id;
        let moduleIds = this.allSelectedBlockRolesForModules.allModels.filter(
          obj => obj.blockRole.block.module.id == moduleId
        );
        for (let i = 0; i < moduleIds.length; i++) {
          let obj = this.allSelectedBlockRolesForModules.allModels.find(
            item => item.blockRole.block.id == moduleIds[i].blockRole.block.id
          );
          this.deleteFormList(
            obj,
            this.allSelectedBlockRolesForModules.allModels
          );
        }
      }

      // for (let i = 0; i < this.allSelectedBlockRolesForModules.allModels.length; i++) {
      //   debugger
      //   if (this.allSelectedBlockRolesForModules.allModels[moduleIdx].blockRole.block.module.id == moduleId) {
      //     debugger
      //     this.allSelectedBlockRolesForModules.allModels.splice(i, 1);
      //   }
      /* for(let i = 0; i< this.allSelectedViewRolesForModules.allModels.length; i++) {
         if(this.allSelectedViewRolesForModules.allModels[moduleIdx].viewRole.view.module.id == moduleId) {
           this.allSelectedViewRolesForModules.allModels.splice(i, 1);
         }
       }*/
    }
  }

  deleteFormList(obj: any, arr: any[]) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].blockRole.block.id == obj.blockRole.block.id) {
        this.allSelectedBlockRolesForModules.allModels.splice(i, 1);
      }
    }
  }

  deleteFormListTest(obj: any, arr: any[]) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].blockRole.block.id == obj.blockRole.block.id) {
        this.allNotSelectedBlockRolesForModules.allModels.splice(i, 1);
      }
    }
  }

  /*onSelectingView(event) {
    if(event.movedAsAssigned) {
      this.pushTo(this.allSelectedViewRolesForModules, event);
      this.removeFrom(this.allNotSelectedViewRolesForModules, event);
    } else {
      this.pushTo(this.allNotSelectedViewRolesForModules, event);
      this.removeFrom(this.allSelectedViewRolesForModules, event);
    }
  
    this.allSelectedViewRolesForModules.dataTable.refresh();
    this.allNotSelectedViewRolesForModules.dataTable.refresh();
  }*/

  onSelectingBlock(event) {
    if (event.movedAsAssigned) {
      this.pushTo(this.allSelectedBlockRolesForModules, event);
      this.removeFrom(this.allNotSelectedBlockRolesForModules, event);
    } else {
      this.pushTo(this.allNotSelectedBlockRolesForModules, event);
      this.removeFrom(this.allSelectedBlockRolesForModules, event);
    }

    this.allSelectedBlockRolesForModules.dataTable.refresh();
    this.allNotSelectedBlockRolesForModules.dataTable.refresh();
  }

  pushTo(moduleRolesByApp: BaseViewComponent, event) {
    if (event.rows) {
      for (let i = 0; i < event.rows.length; i++) {
        moduleRolesByApp.allModels.push(event.rows[i]);
      }
    }
  }

  removeFrom(moduleRolesByApp: BaseViewComponent, event) {
    if (event.rows) {
      for (let i = 0; i < event.rows.length; i++) {
        moduleRolesByApp.allModels.splice(
          moduleRolesByApp.allModels.indexOf(event.rows[i]),
          1
        );
      }
    }
  }

  onAppRoleListOpened(valueBeforeChange) {
    this.previousSelectAppId = valueBeforeChange;
  }

  onViewAndBlockStep() {
    let moduleRole = new ModuleRole();
  }

  triggerFilterEvent(
    allSelectedViewRolesForModules,
    allNotSelectedViewRolesForModules,
    allSelectedBlockRolesForModules,
    allNotSelectedBlockRolesForModules
  ) {
    let tempMap = new TSMap<string, PrivilegeProfileBlockRole[]>();
    for (let i = 0; i < this.listOfAppRoles.length; i++) {
      if (
        this.listOfAppRoles[i].id == this._privilegeProfile.applicationRole.id
      ) {
        this._privilegeProfile.applicationRole.id = this.listOfAppRoles[i].id;
        this._privilegeProfile.applicationRole.application.id = this.listOfAppRoles[
          i
        ].application.id;
      }
    }

    tempMap.set(
      "selectedBlock",
      this.allSelectedBlockRolesForModules.allModels
    );

    allNotSelectedBlockRolesForModules.filterBlockRoleByAppAndModules(
      this._privilegeProfile,
      this.allSelectedModuleRolesByApp.allModels,
      this._isEdit,
      tempMap
    );
  }
  checkNotificationViewPrivilege() {
    this.userNotificationService
      .getUserNotificationPrivilege(
        this.authService.getCurrentLoggedInUser().id
      )
      .subscribe(data => {
        if(data > 0){
          AppComponent.isNotificationIconShown = true;
        }
      });
  }

  onSaveAndNewCompleted() {
    this.showSpinnerOnSaveAndNew = false;
    this.viewsBlocksStep.close();
    this.modulesStep.open();
    this.allSelectedBlockRolesForModules.privilegeProfileBlockRole = [];
    this.allSelectedModuleRolesByApp.privilegeProfileModRole = [];
    this.userTypeList = [];
    this.checkNotificationViewPrivilege();
  }

  onEditCompleted() {
    this.showSpinner = false;
    this.checkNotificationViewPrivilege();
}

  onSaveCompleted(){
    this.showSpinner = false;
    this.checkNotificationViewPrivilege();
  }

  onBeforeEdit(privilegeProfile: PrivilegeProfile) {
    this.showSpinner = true;
    let vendor: Vendor = new Vendor();
    vendor.id = this.vendorId;
    this._privilegeProfile.vendor = vendor;
    this._privilegeProfile.user = this.loggedUser;
    this.fillPrivilegeProfileSelectedRoles();
  }

  onBeforeSave() {
    debugger;
    let vendor: Vendor = new Vendor();
    vendor.id = this.vendorId;
    this.showSpinner = true;
    this._privilegeProfile.user = this.loggedUser;
    this._privilegeProfile.vendor = vendor;

    this.fillPrivilegeProfileSelectedRoles();
  }

  onBeforeSaveAndNew(privilegeProfile: PrivilegeProfile) {
    let vendor: Vendor = new Vendor();
    vendor.id = this.vendorId;
    this.showSpinnerOnSaveAndNew = true;
    this._privilegeProfile.vendor = vendor;
    this._privilegeProfile.user = this.loggedUser;
    this.fillPrivilegeProfileSelectedRoles();
  }

  fillPrivilegeProfileSelectedRoles() {
    debugger;
    if (
      this.allSelectedModuleRolesByApp.allModels != undefined &&
      this.allSelectedModuleRolesByApp.allModels.length > 0
    ) {
      debugger;
      this._privilegeProfile.privilegeProfileModuleRoles = this.allSelectedModuleRolesByApp.allModels;
    } else {
      this._privilegeProfile.privilegeProfileModuleRoles = [];
    }
    debugger;

    this._privilegeProfile.privilegeProfileViewRoles = [];

    /*if(this.allSelectedViewRolesForModules.allModels!=undefined &&
        this.allSelectedViewRolesForModules.allModels.length > 0) {
      this._privilegeProfile.privilegeProfileViewRoles = 
                                this.allSelectedViewRolesForModules.allModels;
    } else {
      this._privilegeProfile.privilegeProfileViewRoles = []
    }*/

    if (
      this.allSelectedBlockRolesForModules.allModels != undefined &&
      this.allSelectedBlockRolesForModules.allModels.length > 0
    ) {
      debugger;
      this._privilegeProfile.privilegeProfileBlockRoles = this.allSelectedBlockRolesForModules.allModels;
    } else {
      debugger;
      this._privilegeProfile.privilegeProfileBlockRoles = [];
    }
  }
}
