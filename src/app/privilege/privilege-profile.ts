import { BaseModel } from "../common/model/base-model";
import { ApplicationRole } from "../role/application/application-role";
import { ModuleRole } from "../role/module/module-role";
import { ViewRole } from "../role/view/view-role";
import { PrivilegeProfileModuleRole } from "./aware/module-role/privilege-profile-module-role";
import { PrivilegeProfileViewRole } from "./aware/view-role/privilege-profile-view-role";
import { PrivilegeProfileBlockRole } from "./aware/block-role/privilege-profile-block-role";
import { User } from "../auth/user.model";
import { UserType } from ".././lookups/user-type.model";
import { Vendor } from "../vendor/vendor.model";
  


export class PrivilegeProfile extends BaseModel{
    id:number;
    inactive:boolean;
    name:string;
    description:string;
    user:User;
    userType:UserType;
    vendor:Vendor;
    applicationRole:ApplicationRole;
    privilegeProfileModuleRoles: PrivilegeProfileModuleRole[];
    
    privilegeProfileViewRoles:PrivilegeProfileViewRole[];
    privilegeProfileBlockRoles:PrivilegeProfileBlockRole[];
    
    constructor() {
        super();
        this.user=new User();
        this.userType = new UserType();
        this.vendor=new Vendor();
        this.applicationRole= new ApplicationRole();
        this.privilegeProfileModuleRoles = [];
        this.privilegeProfileViewRoles = [];
        this.privilegeProfileBlockRoles = [];
     }
}