import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from '../../../common/service/base.service';
import { Observable } from 'rxjs/Observable';
import { BaseModel } from '../../../common/model/base-model';
import { ModuleRole } from '../../../role/module/module-role';
import { Application } from '../../../structure/application/application';
import { PrivilegeProfileBlockRole } from './privilege-profile-block-role';
import { PrivilegeProfileModuleRole } from '../module-role/privilege-profile-module-role';
import { AuthService } from '../../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PrivilegeProfileBlockRoleService extends BaseService {
  static readonly URL_FILTER_BY_MODULE_ROLE = "/by-module";

  privilegeProfileBlockRole: PrivilegeProfileBlockRole;

  constructor(protected http: HttpClient,
    protected authService:AuthService) {
    super(http, "ppbr",authService);
  }

  get model() {
    return this.privilegeProfileBlockRole;
  }

  set model(model: PrivilegeProfileBlockRole) {
    this.privilegeProfileBlockRole = <PrivilegeProfileBlockRole>model;
  }

  filterByAppAndModuleIds(pageNumber: number,
                          pageSize: number,
                          searchTerm: string,
                          sortBy: string,
                          sortOrder: string,
                          application: Application,
                          moduleRoles: PrivilegeProfileModuleRole[],
                          selectedRoles:boolean,
                          privilegeProfileId?: number,
                          userTypeId?: number): Observable<BaseModel> {
                          let parameterList: { paramName: string, paramValue: string }[] = [];

    if (privilegeProfileId != undefined) {
      parameterList.push({ paramName: "privilege_profile_id", paramValue: String(privilegeProfileId) })
    }

    if (userTypeId != undefined) {
      parameterList.push({ paramName: "user_type_id", paramValue: String(userTypeId) })
    }

    if (application) {
      parameterList.push({ paramName: "app_id", paramValue: String(application.id) });
    }

    if (moduleRoles != undefined) {
      for (let i = 0; i < moduleRoles.length; i++) {
        parameterList.push({ paramName: "m_id", paramValue: String(moduleRoles[i].moduleRole.module.id) })
      }
    }
    
    if(selectedRoles != undefined) {
      parameterList.push({paramName:"selected_roles", paramValue: String(selectedRoles)})
    }

    return this.filter( pageNumber - 1,
                        pageSize,
                        searchTerm,
                        sortBy,
                        sortOrder,
                        PrivilegeProfileBlockRoleService.URL_FILTER_BY_MODULE_ROLE,
                        (parameterList.length > 0 ? parameterList : undefined));
  }
}