import { Component, OnInit } from '@angular/core';
import {
  TdDataTableSortingOrder,
  TdDataTableComponent,
  ITdDataTableColumn
} from '@covalent/core';

import { TdPagingBarComponent } from '@covalent/core';
import { SimpleChanges, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { TdDialogService } from '@covalent/core';
import { MatSnackBar } from '@angular/material';

import { ViewChild, Output, Input, EventEmitter } from '@angular/core';
import { BaseViewComponent } from '../../../../../common/view/base-view-component';
import { ModuleRole } from '../../../../../role/module/module-role';
import { environment } from '../../../../../../environments/environment';
import { BlockRole } from '../../../../../role/block/block-role';
import { BlockRoleService } from '../../../../../role/block/block-role.service';
import { PrivilegeProfile } from '../../../../privilege-profile';
import { PrivilegeProfileBlockRoleService } from '../../../block-role/privilege-profile-block-role.service';
import { PrivilegeProfileBlockRole } from '../../../block-role/privilege-profile-block-role';
import { PrivilegeProfileViewRole } from '../../../view-role/privilege-profile-view-role';
import { PrivilegeProfileModuleRole } from '../../../module-role/privilege-profile-module-role';
import { TSMap } from 'typescript-map';

@Component({
  selector: 'all-block-roles-for-modules',
  templateUrl: './all-block-roles-for-modules.component.html',
  styleUrls: ['./all-block-roles-for-modules.component.css']
})
export class AllBlockRolesForModulesComponent extends BaseViewComponent implements OnInit {

  private _currentPage: number = 1;
  private _fromRow: number = 1;


  public privilegeProfileBlockRole: PrivilegeProfileBlockRole[] = [];

  private _errors: any = '';
  private _selectedRows: BlockRole[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = '';

  @Output("rowSelected")
  selectedBlocksEventEmitter = new EventEmitter();

  @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
  @ViewChild('dataTable') dataTable: TdDataTableComponent;

  @Input("selectedRolesSide")
  selectedRolesSide: boolean;

  columnsConfig: ITdDataTableColumn[] = [
    { name: 'blockRole.name', label: 'Role Name', sortable: false, width: 230 },
    { name: 'blockRole.block.name', label: 'Block', sortable: false, width: 180 },
    { name: 'blockRole.block.module.name', label: 'Module', sortable: false, width: 80 },
  ];

  constructor(protected privilegeProfileBlockRoleService: PrivilegeProfileBlockRoleService,
    protected blockRoleService: BlockRoleService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar) {
    super(blockRoleService,
      router,
      dialogService,
      snackBar);
  }

  ngOnInit() { }

  compareWith(row: PrivilegeProfileBlockRole, model: PrivilegeProfileBlockRole): boolean {
    if (row.id == null) {
      return row.blockRole.id == model.blockRole.id;
    }

    return row.id === model.id;
  }

  resetSelectedRows() {
    this.selectedRows = [];
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get allModels(): PrivilegeProfileBlockRole[] {
    return this.privilegeProfileBlockRole;
  }

  public set allModels(value: PrivilegeProfileBlockRole[]) {
    this.privilegeProfileBlockRole = value;
  }

  public get errors(): any {
    return this._errors;
  }

  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): BlockRole[] {
    return this._selectedRows;
  }

  public set selectedRows(value: BlockRole[]) {
    this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }

  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }

  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get searchTerm(): string {
    return this._searchTerm;
  }

  public set searchTerm(value: string) {
    this._searchTerm = value;
  }

  filterBlockRoleByAppAndModules(privilegeProfile: PrivilegeProfile,
    moduleRoles: PrivilegeProfileModuleRole[],
    isEdit: boolean,
    tempMap?: TSMap<string, PrivilegeProfileBlockRole[]>) {
    if (moduleRoles.length > 0) {
      this.privilegeProfileBlockRoleService.filterByAppAndModuleIds(this.currentPage,
        environment.initialViewRowSize,
        this.searchTerm,
        this.sortBy,
        this.sortOrder.toString(),
        privilegeProfile.applicationRole.application,
        moduleRoles,
        this.selectedRolesSide,
        privilegeProfile.id,
        privilegeProfile.userType.id)
        .subscribe(data => {
          this.privilegeProfileBlockRole = data['content'];
          if (tempMap.has("selectedBlock")) {
            if (tempMap.get("selectedBlock").length > 0) {
              for (let i = 0; i < tempMap.get("selectedBlock").length; i++) {
                let selectedBlock = this.privilegeProfileBlockRole.findIndex(item => item.blockRole.block.id == tempMap.get("selectedBlock")[i].blockRole.block.id);
                this.privilegeProfileBlockRole.splice(selectedBlock, 1)
              }
            }
          }

        });
    }
  }

  onMoveRows() {
    this.selectedBlocksEventEmitter.emit({
      rows: this.selectedRows,
      movedAsAssigned: !this.selectedRolesSide
    });

    this.selectedRows = [];
  }



  deleteFormList(obj: any, arr: PrivilegeProfileBlockRole[]) {
    let deletedObject = this.privilegeProfileBlockRole.findIndex(item => item.blockRole.block.id == obj.blockRole.block.id);
    this.privilegeProfileBlockRole.splice(deletedObject, 1);
  }
}
