
import { PrivilegeProfile } from "../../privilege-profile";
import { BaseModel } from "../../../common/model/base-model";
import { BlockRole } from "../../../role/block/block-role";

export class PrivilegeProfileBlockRole extends BaseModel {
    id:number;
    inactive: boolean;
    blockRole:BlockRole;
    privilegeProfile:PrivilegeProfile;

    constructor(id:number, 
                privilegeProfile:PrivilegeProfile, 
                blockRole:BlockRole) {
        super();
        this.id = id;
        this.privilegeProfile = privilegeProfile;
        this.blockRole = blockRole;
    }
}