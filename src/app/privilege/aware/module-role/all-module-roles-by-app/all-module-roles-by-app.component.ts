import { Component, OnInit, ViewChild } from '@angular/core';
import { ModuleRole } from '../../../../role/module/module-role';
import { TdDataTableSortingOrder } from '@covalent/core/data-table/data-table.component';
import { TdPagingBarComponent, TdDataTableComponent, ITdDataTableColumn } from '@covalent/core';
import { Input } from '@angular/core';
import { BaseViewComponent } from '../../../../common/view/base-view-component';
import { ModuleRoleService } from '../../../../role/module/module-role.service';
import { TdDialogService } from '@covalent/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { OnChanges, SimpleChanges, AfterViewChecked, AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { ApplicationRole } from '../../../../role/application/application-role';
import { environment } from '../../../../../environments/environment';
import { TdDataTableRowComponent } from '@covalent/core';
import { QueryList } from '@angular/core';
import { PrivilegeProfile } from '../../../privilege-profile';
import { PrivilegeProfileModuleRoleService } from '../../module-role/privilege-profile-module-role.service';
import { PrivilegeProfileModuleRole } from '../../module-role/privilege-profile-module-role';
import { PrivilegeProfileFormComponent } from '../../../form/privilege-profile-form.component';
import { User } from '../../../../auth/user.model';

@Component({
  selector: 'all-module-roles-by-app',
  templateUrl: './all-module-roles-by-app.component.html',
  styleUrls: ['./all-module-roles-by-app.component.css']
})
export class AllModuleRolesByAppComponent22 extends BaseViewComponent implements OnInit {
	private _currentPage: number = 1;
	private _fromRow: number = 1;	
	
	public privilegeProfileModRole:PrivilegeProfileModuleRole[] = [];
  
  	private _errors: any = '';
  	private _selectedRows: PrivilegeProfileModuleRole[] = [];
  	private _pageSize: number = environment.initialViewRowSize;
  	private _filteredTotal: number;
  	private _sortBy: string = '';
  	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  	private _searchTerm: string ='';

	@Output("rowSelected")
	selectedModulesEventEmitter = new EventEmitter();


	@ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
	@ViewChild('dataTable') dataTable : TdDataTableComponent;
  
	@Input("selectedRolesSide")
	selectedRolesSide:boolean;

	@Input("otherModuleRolesComponent") 
	otherModuleRolesComponent: AllModuleRolesByAppComponent22

	columnsConfig: ITdDataTableColumn[] = [
		{ name: 'moduleRole.name', label: 'Name'},
		{ name: 'moduleRole.module.name', label: 'Module'},
	];

  	constructor(protected privilegeProfileModRoleService:PrivilegeProfileModuleRoleService,
			protected moduleRoleService: ModuleRoleService,
			protected router:Router,
			protected dialogService : TdDialogService,
			protected snackBar:MatSnackBar) { 
      	super(	moduleRoleService,
				router,
				dialogService,
				snackBar);
    }

	ngOnInit() {
	}

	compareWith(row: PrivilegeProfileModuleRole, model: PrivilegeProfileModuleRole): boolean { 
		if(row.id == null) {
		  return row.moduleRole.id == model.moduleRole.id;
		} 
	
		return row.id === model.id;
	}

 	filterModuleRolesByAppRole(privilegeProfile:PrivilegeProfile, functionToExecuteOnComplete?:Function, callerObj?:PrivilegeProfileFormComponent) {
		let loggedUser:User=this.authService.getCurrentLoggedInUser();
		this.vendorAdminService.getVendorByVendorAdminId(loggedUser.id).subscribe(vendorId=>{
	 	this.privilegeProfileModRoleService
			.filterByAppRoleId(	this.currentPage, 
								environment.initialViewRowSize,
								this.searchTerm, 
								this.sortBy, 
								this.sortOrder.toString(),
								this.selectedRolesSide, 
								privilegeProfile.id,
								privilegeProfile.applicationRole.id,
								privilegeProfile.userType.id,
								vendorId)
			.subscribe(data => {
						this.privilegeProfileModRole = <any>data;
						//this.privilegeProfileModRole = data["content"];
						//this.filteredTotal = data['totalElements'];
			}, 
			()=> {},
			()=> {
				if(functionToExecuteOnComplete != undefined && callerObj != undefined) {
					this.onFilterModuleRolesByAppComplete(functionToExecuteOnComplete, callerObj);
				}
			});

		});
	}

	onFilterModuleRolesByAppComplete(functionToExecuteOnComplete:Function, callerObj:PrivilegeProfileFormComponent){
		functionToExecuteOnComplete.apply(callerObj);
	}
	
	onMoveRows() {
		this.selectedModulesEventEmitter.emit({	rows: this._selectedRows, 
												movedAsAssigned: !this.selectedRolesSide});

		this.selectedRows = [];
	}

	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}

	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

  	public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): PrivilegeProfileModuleRole[]  {
		return this.privilegeProfileModRole;
	}

	public set allModels(value: PrivilegeProfileModuleRole[] ) {
		this.privilegeProfileModRole = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): PrivilegeProfileModuleRole[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: PrivilegeProfileModuleRole[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
  	}
}