import { Injectable } from '@angular/core';
import { BaseService } from '../../../common/service/base.service';
import { PrivilegeProfileModuleRole } from './privilege-profile-module-role';
import { Http } from '@angular/http';
import { BaseModel } from '../../../common/model/base-model';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PrivilegeProfileModuleRoleService extends BaseService{
  static readonly URL_FILTER_BY_APP_ROLE_ID = "/by-app-role";

  privilegeProfileModuleRole:PrivilegeProfileModuleRole;

  constructor(protected http:HttpClient,
    protected authService:AuthService) { 
    super(http, "ppmr", authService);
  }

  get model(){
    return this.privilegeProfileModuleRole;
  }

  set model(model:PrivilegeProfileModuleRole) {
    this.privilegeProfileModuleRole = <PrivilegeProfileModuleRole>model;
  }

  filterByAppRoleId<ModuleRole>(pageNumber: number,
                                pageSize: number,
                                searchTerm: string,
                                sortBy: string,
                                sortOrder: string,
                                selectedRoles:boolean, 
                                privilegeProfileId?:number,
                                appRoleId?:number,
                                userId?:number,
                                vendorId?:number): Observable<BaseModel> {
      let parameterList:{paramName:string, paramValue: string}[]=[];
      
      if(privilegeProfileId != undefined) {
        parameterList.push({paramName:"privilege_profile_id", paramValue: String(privilegeProfileId)})
      }

      if(appRoleId != undefined) {
        parameterList.push({paramName:"app_role_id", paramValue: String(appRoleId)})
      }

      if(selectedRoles != undefined) {
        parameterList.push({paramName:"selected_roles", paramValue: String(selectedRoles)})
      }

      if(userId != undefined) {
        parameterList.push({paramName:"user_type_id", paramValue: String(userId)})
      }

      if(vendorId != undefined) {
        parameterList.push({paramName:"vendor_id", paramValue: String(vendorId)})
      }

      return this.filterNotPageable(
                          searchTerm, 
                          sortBy, 
                          sortOrder,
                          PrivilegeProfileModuleRoleService.URL_FILTER_BY_APP_ROLE_ID, 
                          (parameterList.length > 0?parameterList:undefined));  
  }
}
