import { PrivilegeProfile } from "./../../privilege-profile";
import { BaseModel } from "../../../common/model/base-model";
import { ModuleRole } from "../../../role/module/module-role";

export class PrivilegeProfileModuleRole extends BaseModel {
    id:number;
    inactive:boolean;
    privilegeProfile:PrivilegeProfile;
    moduleRole:ModuleRole;

    constructor(id:number, 
                privilegeProfile:PrivilegeProfile, 
                moduleRole:ModuleRole) {
    super();
        this.id = id;
        this.privilegeProfile = privilegeProfile;
        this.moduleRole = moduleRole;
    }
}