import { ViewRole } from "../../../role/view/view-role";
import { PrivilegeProfile } from "../../privilege-profile";
import { BaseModel } from "../../../common/model/base-model";

export class PrivilegeProfileViewRole extends BaseModel {
    id:number;
    inactive: boolean;
    viewRole:ViewRole;
    privilegeProfile:PrivilegeProfile;

    constructor(id:number, 
                privilegeProfile:PrivilegeProfile, 
                viewRole:ViewRole) {
        super();
        this.id = id;
        this.privilegeProfile = privilegeProfile;
        this.viewRole = viewRole;
    }
}