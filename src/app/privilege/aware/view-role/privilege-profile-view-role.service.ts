import { Injectable } from '@angular/core';
import { PrivilegeProfileViewRole } from './privilege-profile-view-role';
import { Http } from '@angular/http';
import { BaseService } from '../../../common/service/base.service';
import { Observable } from 'rxjs/Observable';
import { BaseModel } from '../../../common/model/base-model';
import { ModuleRole } from '../../../role/module/module-role';
import { Application } from '../../../structure/application/application';
import { PrivilegeProfileModuleRole } from '../module-role/privilege-profile-module-role';
import { AuthService } from '../../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PrivilegeProfileViewRoleService extends BaseService {
  static readonly URL_FILTER_BY_MODULE_ROLE = "/by-module";

  privilegeProfileModuleRole: PrivilegeProfileViewRole;

  constructor(protected http: HttpClient,
    protected authService:AuthService) {
    super(http, "ppvr", authService);
  }

  get model() {
    return this.privilegeProfileModuleRole;
  }

  set model(model: PrivilegeProfileViewRole) {
    this.privilegeProfileModuleRole = <PrivilegeProfileViewRole>model;
  }

  filterByAppAndModuleIds(pageNumber: number,
                    pageSize: number,
                    searchTerm: string,
                    sortBy: string,
                    sortOrder: string,
                    application:Application,
                    moduleRoles: PrivilegeProfileModuleRole[],
                    selectedRoles: boolean,
                    privilegeProfileId?: number): Observable<BaseModel> {
    let parameterList: { paramName: string, paramValue: string }[] = [];

    if (privilegeProfileId != undefined) {
      parameterList.push({ paramName: "privilege_profile_id", paramValue: String(privilegeProfileId) })
    }

    if(application) {
      parameterList.push({paramName: "app_id", paramValue: String(application.id)});
    }

    if (moduleRoles != undefined) {
      for (let i = 0; i < moduleRoles.length; i++) {
        parameterList.push({ paramName: "m_id", paramValue: String(moduleRoles[i].moduleRole.module.id) })
      }
    }

    if(selectedRoles != undefined) {
      parameterList.push({paramName:"selected_roles", paramValue: String(selectedRoles)})
    }
    
    return this.filter(pageNumber - 1,
      pageSize,
      searchTerm,
      sortBy,
      sortOrder,
      PrivilegeProfileViewRoleService.URL_FILTER_BY_MODULE_ROLE,
      (parameterList.length > 0 ? parameterList : undefined));
  }
}