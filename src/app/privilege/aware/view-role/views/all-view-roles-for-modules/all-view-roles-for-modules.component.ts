import { Component, OnInit } from '@angular/core';
import {  TdDataTableSortingOrder, 
          TdDataTableComponent, 
          ITdDataTableColumn } from '@covalent/core';
import { ViewRole } from '../../../../../role/view/view-role';
import { TdPagingBarComponent } from '@covalent/core';
import { SimpleChanges, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { TdDialogService } from '@covalent/core';
import { MatSnackBar } from '@angular/material';
import { ViewRoleService } from '../../../../../role/view/view-role.service';
import { ViewChild, Output, Input, EventEmitter } from '@angular/core';
import { BaseViewComponent } from '../../../../../common/view/base-view-component';
import { ModuleRole } from '../../../../../role/module/module-role';
import { environment } from '../../../../../../environments/environment';
import { PrivilegeProfileViewRoleService } from '../../../view-role/privilege-profile-view-role.service';
import { PrivilegeProfileViewRole } from '../../../view-role/privilege-profile-view-role';
import { ApplicationRole } from '../../../../../role/application/application-role';
import { PrivilegeProfile } from '../../../../privilege-profile';
import { PrivilegeProfileModuleRole } from '../../../module-role/privilege-profile-module-role';

@Component({
  selector: 'all-view-roles-for-modules',
  templateUrl: './all-view-roles-for-modules.component.html',
  styleUrls: ['./all-view-roles-for-modules.component.css']
})
export class AllViewRolesForModulesComponent extends BaseViewComponent implements OnInit {
	
  private _currentPage: number = 1;
  private _fromRow: number = 1;

  public privilegeProfileViewRole:PrivilegeProfileViewRole[] = [];

  private _errors: any = '';
  private _selectedRows: PrivilegeProfileViewRole[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string ='';

  @Output("rowSelected")
  selectedViewsEventEmitter = new EventEmitter();

  @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
  @ViewChild('dataTable') dataTable : TdDataTableComponent;
  
  @Input("selectedRolesSide")
  selectedRolesSide:boolean;
  
  columnsConfig: ITdDataTableColumn[] = [
    { name: 'viewRole.name', label: 'Role Name', sortable: false  },
    { name: 'viewRole.view.name', label: 'View', sortable: false   },
    { name: 'viewRole.view.module.name', label: 'Module', sortable: false   },
  ];

  constructor(protected privilegeProfileViewRoleService:PrivilegeProfileViewRoleService,
              protected viewRoleService: ViewRoleService,
              protected router:Router,
              protected dialogService : TdDialogService,
              protected snackBar:MatSnackBar) { 
      super(viewRoleService,
            router,
            dialogService,
            snackBar);
  }
  
  ngOnInit() {}

  compareWith(row: PrivilegeProfileViewRole, model: PrivilegeProfileViewRole): boolean { 
    if(row.id == null) {
      return row.viewRole.id == model.viewRole.id;
    } 

    return row.id === model.id;
  }

	onMoveRows() {
		this.selectedViewsEventEmitter.emit({	rows: this.selectedRows, 
                                          movedAsAssigned: !this.selectedRolesSide});

    this.selectedRows = [];
	}
  
  filterViewRoleByAppAndModules(privilegeProfile:PrivilegeProfile, moduleRoles:PrivilegeProfileModuleRole[]) {
    if(moduleRoles.length > 0) {

      this.privilegeProfileViewRoleService.filterByAppAndModuleIds(this.currentPage,
                                  environment.initialViewRowSize, 
                                  this.searchTerm, 
                                  this.sortBy, 
                                  this.sortOrder.toString(),
                                  privilegeProfile.applicationRole.application,
                                  moduleRoles,
                                  this.selectedRolesSide,
                                  privilegeProfile.id)
                          .subscribe(data => {
                                      this.privilegeProfileViewRole = data['content'];
                                      this.filteredTotal = data['totalElements'];
                          });
    }
  }

  resetSelectedRows() {
		this.selectedRows = [];
	}
  
  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): PrivilegeProfileViewRole[]  {
		return this.privilegeProfileViewRole;
	}

	public set allModels(value: PrivilegeProfileViewRole[] ) {
		this.privilegeProfileViewRole = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): PrivilegeProfileViewRole[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: PrivilegeProfileViewRole[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
  	}
}