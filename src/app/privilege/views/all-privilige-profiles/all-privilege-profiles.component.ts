import { VendorAdminService } from './../../../vendor-admin/service/vendor-admin.service';
import { AuthService } from './../../../auth/service/auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { TdDataTableSortingOrder, TdDialogService, ITdDataTableColumn, TdPagingBarComponent, TdDataTableComponent } from '@covalent/core';
import { PrivilegeProfile } from '../../privilege-profile';
import { PrivilegeProfileService } from '../../service/privilege-profile.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { NgxPermissionsService } from 'ngx-permissions';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'all-privilige-profiles',
	templateUrl: './all-privilege-profiles.component.html',
	styleUrls: ['./all-privilege-profiles.component.css']
})
export class AllPrivilegeProfilesComponent extends BaseViewComponent implements OnInit {
	private _currentPage: number = 1;
	private _fromRow: number = 1;

	private _allPrivilegeProfiles: PrivilegeProfile[] = [];
	private _errors: any = '';
	private _selectedRows: PrivilegeProfile[] = [];
	private _pageSize: number;
	private _filteredTotal: number;
	private _sortBy: string = '';
	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
	private _searchTerm: string = '';

	@ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
	@ViewChild('dataTable') dataTable: TdDataTableComponent;

	columnsConfig: ITdDataTableColumn[] = [
		{ name: 'name', label: 'Name', sortable: false, tooltip: "Name" },
		{ name: 'applicationRole.application.name', label: 'Application', sortable: false, tooltip: "Application", width: 200 },
		{ name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive", width: 100 },
		{ name: 'id', label: 'Edit', sortable: false, tooltip: "Edit", width: 100 }
	];

	constructor(protected privilegeProfileService: PrivilegeProfileService,
		protected router: Router,
		protected dialogService: TdDialogService,
		protected snackBar: MatSnackBar,
		protected permissionsService: NgxPermissionsService,
		protected athuService: AuthService,
		protected vendorAdminService: VendorAdminService) {
		super(privilegeProfileService,
			router,
			dialogService,
			snackBar,
			athuService);
	}


	ngOnInit() {
		super.ngOnInit();

		this.permissionsService.hasPermission("ROLE_PRIVILEGE_CENTER_EDIT").then((result: boolean) => {
			if (!result) {
				for (let i = 0; i < this.dataTable.columns.length; i++) {
					if (this.dataTable.columns[i].name == 'id') {
						this.dataTable.columns.splice(i, 1);
					}
				}
			}
		});


		this.permissionsService.hasPermission("ROLE_PRIVILEGE_CENTER_ACTIVATE_DEACTIVATE_PER_ROW").then((result: boolean) => {
			if (!result) {
				for (let i = 0; i < this.dataTable.columns.length; i++) {
					if (this.dataTable.columns[i].name == 'inactive') {
						this.dataTable.columns.splice(i, 1);
					}
				}
			}
		});
	}

	filterByVendor(pageNumber: number,
		pageSize: number,
		searchTerm: string,
		sortBy: string,
		sortOrder: string,
		vendorId: number) {
		this.privilegeProfileService.getProfilesForVendor(pageNumber, pageSize, vendorId, searchTerm, sortBy, sortOrder).subscribe(data => {
			this.allModels = data['content'];
			this.filteredTotal = data['totalElements'];
		});
	}

	onDeleteFailed(errorResponse:HttpErrorResponse) {
		this.snackBar.open(errorResponse.error.message, "close", {duration: 3000});
	}

	onDeactivationFailed(errorResponse:HttpErrorResponse) {
		this.snackBar.open(errorResponse.error.code, "close", {duration: 3000});
		super.onRefresh();
    }

	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}
	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

	public get currentPage(): number {
		return this._currentPage;
	}

	public set currentPage(value: number) {
		this._currentPage = value;
	}

	public get fromRow(): number {
		return this._fromRow;
	}

	public set fromRow(value: number) {
		this._fromRow = value;
	}

	public get allModels(): PrivilegeProfile[] {
		return this._allPrivilegeProfiles;
	}

	public set allModels(value: PrivilegeProfile[]) {
		this._allPrivilegeProfiles = value;
	}

	public get errors(): any {
		return this._errors;
	}

	public set errors(value: any) {
		this._errors = value;
	}

	public get selectedRows(): PrivilegeProfile[] {
		return this._selectedRows;
	}

	public set selectedRows(value: PrivilegeProfile[]) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string {
		return this._sortBy;
	}

	public set sortBy(value: string) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder) {
		this._sortOrder = value;
	}

	public get searchTerm(): string {
		return this._searchTerm;
	}

	public set searchTerm(value: string) {
		this._searchTerm = value;
	}
}
