import {BaseModel} from '../common/model/base-model';

export class PurchaseSetup extends BaseModel {
    id: number;
    inactive: boolean;
    name: string;
    checked: boolean;

    constructor () {
        super();
    }
}