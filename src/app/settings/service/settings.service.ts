import {BaseModel} from '../../common/model/base-model';
import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Settings } from '../settings.model';

@Injectable()
export class SettingsService extends BaseService{
  settings:Settings;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService:AuthService) {
   super(http, "settings", authService)
   }
 
   get model(){
     return this.settings;
   }
   set model(model:BaseModel) {
     this.settings = <Settings>model;
   }
}