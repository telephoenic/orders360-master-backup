import { TdDataTableService, TdDataTableSortingOrder, ITdDataTableSortChangeEvent, ITdDataTableColumn } from '@covalent/core';
import { IPageChangeEvent, TdPagingBarComponent, TdDialogService,
  TdDataTableComponent } from '@covalent/core';
import {Component, ViewChild, OnInit, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {
  MatTable, MatHeaderCellDef, MatCellDef,
  MatColumnDef, MatPaginator, MatSort,
  MatInput, MatCheckbox,
  MatButton,
  MatSnackBar
} from "@angular/material";

import { ActivatedRoute, Router } from '@angular/router';
import { Settings } from '../settings.model';
import { SettingsService } from '../service/settings.service';
import { BaseViewComponent } from '../../common/view/base-view-component';

@Component({
  selector: 'app-view',
  templateUrl: './view-settings.component.html',
  styleUrls: ['./view-settings.component.css']
})
export class ViewSettingsFormComponent extends BaseViewComponent implements OnInit {

  private _currentPage: number = 1;
  private _fromRow: number = 1;
  
  private _allSettings: Settings[] = [];
  private _errors: any = '';
  private _selectedRows: Settings[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string ='';
  
  @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
  @ViewChild('dataTable') dataTable : TdDataTableComponent;

  constructor(protected settingsService: SettingsService,
              protected router:Router,
              protected dialogService : TdDialogService,
              protected snackBar:MatSnackBar) { 
    super(settingsService, router, dialogService, snackBar);
  }

	ngOnInit() {}
	
	onEdit(posId:number, row:Settings){
		this.settingsService.settings = row;
	}
  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): Settings[]  {
		return this._allSettings;
	}

	public set allModels(value: Settings[] ) {
		this._allSettings = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): Settings[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: Settings[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
	}
}
