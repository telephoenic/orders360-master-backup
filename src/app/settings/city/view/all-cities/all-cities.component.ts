import { Component, ViewChild, Inject } from '@angular/core';
import { City } from '../../../../location/city/City.model';
import { TdDataTableComponent, TdDialogService, ITdDataTableColumn, TdPagingBarComponent, TdDataTableSortingOrder } from '@covalent/core';
import { Router } from '@angular/router';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VendorAdminService } from '../../../../vendor-admin/service/vendor-admin.service';
import { AuthService } from '../../../../auth/service/auth.service';
import { CityService } from '../../../../location/city/service/city.service';
import { BaseViewComponent } from '../../../../common/view/base-view-component';
import { CountryService } from '../../../../location/country/service/country.service';
import { environment } from '../../../../../environments/environment';
import { Country } from '../../../../location/country/country.model';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, Validators } from '@angular/forms';


@Component({
    selector: 'app-all-cities',
    templateUrl: './all-cities.component.html',
    styleUrls: ['./all-cities.component.css']
})

export class AllCitiesComponent extends BaseViewComponent {
    searchTerm: string;
    pageSize: number = 50;
    currentPage: number = 1;
    fromRow: number;
    errors: any;
    city: City;
    isEdit: boolean = false;
    private _selectedRows: City[] = [];
    private _filteredTotal: number;
    private _sortBy: string = '';
    private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
    listOfCountries: any[];
    getPagingBar(): TdPagingBarComponent {
        return this.pagingBar;
    }
    getDataTable(): TdDataTableComponent {
        return this.dataTable;
    }

    compareWith(row: any, model: any): boolean {
        return row.id === model.id;
    }


    public ngOnInit() {
        this.getCities();
        this.loadCountries();
    }
    private _allCities: City[] = [];

    @ViewChild('dataTable') dataTable: TdDataTableComponent;
    @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;

    columnsConfig: ITdDataTableColumn[] = [
        { name: 'name', label: 'City', tooltip: "City Name" },
        { name: 'country.name', label: 'Country', tooltip: "Country Name" },
        { name: 'code', label: 'Landline Code', tooltip: "Landline Code" },
        { name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive" },
        { name: 'id', label: 'Edit', tooltip: "Edit" }
    ]

    constructor(protected cityService: CityService,
        protected router: Router,
        protected dialogService: TdDialogService,
        protected snackBar: MatSnackBar,
        protected vendorAdminService: VendorAdminService,
        protected authService: AuthService,
        protected countryService: CountryService) {
        super(cityService,
            router,
            dialogService,
            snackBar, authService);

    }

    public onAdd() {
        this.city = new City();
        this.city.name = '';
        this.city.country = new Country();
        this.city.country.name = '';
        this.openDialog(this.city);
    }

    public onUpdate(id: number, row: City) {
        this.city = row;
        this.openDialog(this.city);
    }

    loadCountries() {
        this.countryService.filter(0, environment.maxListFieldOptions, "", "", "").subscribe(data => {
            this.listOfCountries = data['content'];
        });
    }

    public openDialog(row: City) {
        let isCityNameAndIdUndefiend: boolean = false;
        if (this.city.name == "" && this.city.country.id == undefined) {
            isCityNameAndIdUndefiend = true;
        } else {
            isCityNameAndIdUndefiend = false;
        }
        let dialogRef = this.dialogService.open(CityDialog, {
            width: '500px',
            data: {
                city: this.city, listOfCountries: this.listOfCountries,
                cityName: this.city.name
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            dialogRef.close();
            console.log(result);
            if(result == undefined){
                this.getCities();
            }else{
            if (!isCityNameAndIdUndefiend) {
                let city = new City();
                city.id = row.id;
                city.name = row.name;
                city.country = row.country;
                this.cityService.countByCityName(this.city.name, this.city.country.id, this.city.id, this.city.code)
                    .subscribe(data => {
                        let countCityName = data;
                        if (countCityName > 0) {
                            this.snackBar.open("City Name already in use.", "Close", { duration: 3000 });
                            throw new Error("City Name already in use.");
                        }
                        else {
                            this.cityService.save(row).subscribe(data =>{ 
                                console.log("faaat");
                                this.getCities();
                            }
                        );}
                    },
                    error=>{
                        this.snackBar.open("LandLine Number is already in use.","close",{duration: 3000});
                    });
            } else {
                this.cityService.countByCityNameAndCountryIdEquals(this.city.name, this.city.country.id, this.city.code)
                    .subscribe(data => {
                        let countCityName = data;
                        console.log(countCityName);
                        console.log(countCityName);
                        if (countCityName > 0) {
                            this.snackBar.open("City Name already in use.", "Close", { duration: 3000 });
                            throw new Error("City Name already in use.");
                        }
                        else {
                            this.cityService.save(row).subscribe(data =>{
                                this.getCities();
                            })
                        }
                    },
                    error=>{
                        this.snackBar.open("LandLine Number is already in use.","close",{duration: 3000});
                    });
            }
            this.getCities();
        }
        });
    }
    
    onDeleteFailed(errorResponse:HttpErrorResponse){
        this.snackBar.open("Process cannot be completed, the selected option is in use.", "close", {duration: 3000});
    }

    onDeactivationFailed(errorResponse:HttpErrorResponse) {
        this.snackBar.open("Process cannot be completed, the selected option is in use.", "close", {duration: 3000});
        this.getCities();
    }

    public saveCompleted(){}

    public getCities() {
        this.cityService.filter(this.currentPage - 1, this.pageSize
            , this.searchTerm, this.sortBy, this.sortOrder).subscribe(data => {
                this.allModels = data['content'];
                this.filteredTotal = data['totalElements'];
            })
    }
    public set allModels(value: City[]) {
        this._allCities = value;
    }

    public get allModels() {
        return this._allCities;
    }

    public set selectedRows(value: City[]) {
        this._selectedRows = value;
    }

    public get selectedRows(): City[] {
        return this._selectedRows;
    }

    public set filteredTotal(value: number) {
        this._filteredTotal = value;
    }

    public get filteredTotal() {
        return this._filteredTotal;
    }

    public set sortBy(value: string) {
        this._sortBy = value;
    }

    public get sortBy() {
        return this._sortBy;
    }

    public set sortOrder(value: TdDataTableSortingOrder) {
        this._sortOrder = value;
    }

    public get sortOrder() {
        return this._sortOrder;
    }
}

@Component({
    selector: 'city-dialog',
    templateUrl: 'city-dialog.html',
})
export class CityDialog {
    ngOnInit() {
        console.log(this.data.cityName.length)
    }
    constructor(
        public dialogRef: MatDialogRef<CityDialog>,
        protected cityService: CityService,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

        codeCity = new FormControl('', [
			Validators.required,
			Validators.maxLength(3),
			Validators.minLength(2),
			Validators.pattern('[0-9]{0,3}')]);

        city = new FormControl('', [
            Validators.required,
            Validators.maxLength(30),
            Validators.minLength(3)]);

    onCancle(): void {
        localStorage.setItem("valueOfSelectedButton","-1");
        this.dialogRef.close();
    }


}
