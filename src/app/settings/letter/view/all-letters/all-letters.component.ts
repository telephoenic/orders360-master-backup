import { AuthService } from './../../../../auth/service/auth.service';
import { VendorAdminService } from './../../../../vendor-admin/service/vendor-admin.service';
import { Component, OnInit } from '@angular/core';
import { BaseViewComponent } from '../../../../common/view/base-view-component';
import { TdPagingBarComponent } from '@covalent/core';
import { TdDataTableComponent, TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import { Letter } from '../../letter.model';
import { Router } from '@angular/router';
import { TdDialogService } from '@covalent/core';
import { LetterService } from '../../service/letter.service';
import { MatSnackBar } from '@angular/material';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-all-letters',
  templateUrl: './all-letters.component.html',
  styleUrls: ['./all-letters.component.css']
})
export class AllLettersComponent extends BaseViewComponent {
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  
  private _allLetters: Letter[] = [];
  private _errors: any = '';
  private _selectedRows: Letter[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string ='';

  @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
  @ViewChild('dataTable') dataTable : TdDataTableComponent;

  columnsConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name',tooltip:"Name" },
    { name: 'technicalName', label: 'Technical name',tooltip: "Technical name", width:300},
    { name: "inactive", label: "Active/Inactive",  tooltip : "Active or Inactive", width:100},
    { name: 'id', label: 'Edit',tooltip:"Edit", width:100 }
  ];

  constructor(protected letterService: LetterService,
              protected router:Router,
              protected dialogService : TdDialogService,
			  protected snackBar:MatSnackBar,
			  protected vendorAdminService: VendorAdminService,
			  protected authService: AuthService,) {

    super(letterService, 
          router,
          dialogService, 
          snackBar,authService);
  }

	public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): Letter[]  {
		return this._allLetters;
	}

	public set allModels(value: Letter[] ) {
		this._allLetters = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): Letter[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: Letter[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
  }

  getPagingBar() : TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable() : TdDataTableComponent {
    return this.dataTable;
  }

}
