import { BaseModel } from "../../common/model/base-model";

export class Letter extends BaseModel{
    id: any;
    inactive: boolean;
    name:string ;
    technicalName:string;
    subject:string;
    body:string;
    

}