import { Injectable } from '@angular/core';
import { BaseService } from '../../../common/service/base.service';
import { Letter } from '../letter.model';
import { AuthService } from '../../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { BaseModel } from '../../../common/model/base-model';

@Injectable()
export class LetterService extends BaseService{
  letter:Letter;
  navigateToAfterCompletion: string;

  constructor(http:HttpClient,
    protected authService:AuthService) {
    super(http, "letter", authService);
  }
  
  get model() {
    return this.letter;
  }

  set model(model:BaseModel) {
    this.letter = <Letter> model;
  }

}
