import { Component, OnInit } from '@angular/core';
import { BaseFormComponent } from '../../../../common/form/base-form-component';
import { NgForm } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { Letter } from '../../letter.model';
import { LetterService } from '../../service/letter.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { TdTextEditorComponent } from '@covalent/text-editor';
import { TdMarkdownComponent } from '@covalent/markdown/markdown.component';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';


@Component({
  selector: 'app-letter-form',
  templateUrl: './letter-form.component.html',
  styleUrls: ['./letter-form.component.css']
})
export class LetterFormComponent extends BaseFormComponent implements OnInit {
  
  @ViewChild("letterForm") letterForm : NgForm;
  @ViewChild("bodyTE") bodyTE:TdTextEditorComponent;

  _letter : Letter;
  _isEdit:boolean = false;

  constructor(protected letterService: LetterService,
              protected route:ActivatedRoute,
              protected router:Router,
              protected snackBar:MatSnackBar) { 

    super(letterService, 
      route,
      router,
      snackBar);
  }

  ngOnInit() {
    if(this.route.snapshot.queryParams["edit"] == '1') {
      this._isEdit = true;
      this._letter = this.letterService.letter;
  
    } else {
      this._letter = new Letter();
    }
  }

  options: any = {
    
    spellChecker:true,
    showIcons: ["code", "table"]
  };

  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
  
  get model() : Letter {
    return this._letter;
  }

  set model(letter:Letter){
    this._letter = letter;
  }
  
  initModel(): Letter {
    return new Letter();
  }

  ngFormInstance(): NgForm {
    return this.letterForm;
  } 
}