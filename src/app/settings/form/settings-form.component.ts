import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {NgForm} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseUserManagementForm } from '../../user-management/base-user-management-form';
import { UserPrivilegeProfileComponent } from '../../user-privilege/views/user-privilege-profile.component';
import { SettingsService } from '../service/settings.service';
import { Settings } from '../settings.model';

@Component({
  selector: 'app-settings-form',
  templateUrl: './settings-form.component.html',
  styleUrls: ['./settings-form.component.css']
})
export class SettingsFormComponent extends BaseUserManagementForm implements OnInit {

  @ViewChild("settingsForm") settingsForm : NgForm;

  _settings : Settings;
  listOfTypes:Settings[];
  _isEdit:boolean = false;

  
  @ViewChild("notSelectedPrivilegeProfiles")
  notSelectedPrivilegeProfiles:UserPrivilegeProfileComponent;

  @ViewChild("selectedPrivilegeProfiles")
  selectedPrivilegeProfiles:UserPrivilegeProfileComponent;

  constructor(protected settingsService: SettingsService,
              protected route:ActivatedRoute,
              protected router:Router,
              protected snackBar:MatSnackBar) { 

    super(settingsService, 
      route,
      router,
      snackBar);
  }

  ngOnInit() {
  }

  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
  
  get model() : Settings {
    return this._settings;
  }

  set model(settings:Settings){
    this._settings = settings;
  }
  
  initModel(): Settings {
    return new Settings();
  }

  ngFormInstance(): NgForm {
    return this.settingsForm;
  }

  getSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.selectedPrivilegeProfiles;
  }
  getNotSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.notSelectedPrivilegeProfiles;
  }
}
