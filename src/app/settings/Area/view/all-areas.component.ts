import { Component, ViewChild, Inject, OnInit } from '@angular/core';
import { TdDataTableComponent, TdDialogService, ITdDataTableColumn, TdPagingBarComponent, TdDataTableSortingOrder } from '@covalent/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Region } from '../../../location/region/Region.model';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { RegionService } from '../../../location/region/service/region.service';
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';
import { AuthService } from '../../../auth/service/auth.service';
import { City } from '../../../location/city/City.model';
import { CityService } from '../../../location/city/service/city.service';
import { FormControl, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
    selector: 'app-all-areas',
    templateUrl: './all-areas.component.html',
    styleUrls: ['./all-areas.component.css']
})

export class AllAreasComponent extends BaseViewComponent {
    searchTerm: string;
    pageSize: number = 50;
    currentPage: number = 1;
    fromRow: number;
    errors: any;
    region: Region;
    private _selectedRows: Region[] = [];
    private _filteredTotal: number;
    private _sortBy: string = '';
    private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
    _isEdit: boolean = false;
    listOfCities: City[];
    _region: Region;
    _city: City;
    getPagingBar(): TdPagingBarComponent {
        return this.pagingBar;
    }
    getDataTable(): TdDataTableComponent {
        return this.dataTable;
    }

    public ngOnInit() {
        this.getAreas();
        this.loadCities();
    }
    private _allRegions: Region[] = [];

    @ViewChild('dataTable') dataTable: TdDataTableComponent;
    @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;

    columnsConfig: ITdDataTableColumn[] = [
        { name: 'name', label: 'Area', tooltip: "Area Name" },
        { name: 'city.name', label: 'City', tooltip: "City Name" },
        { name: 'city.country.name', label: 'Country', tooltip: 'Country Name' },
        { name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive" },
        { name: 'id', label: 'Edit', tooltip: "Edit" }
    ]

    constructor(protected regionService: RegionService,
        protected router: Router,
        protected dialogService: TdDialogService,
        protected snackBar: MatSnackBar,
        protected vendorAdminService: VendorAdminService,
        protected cityService: CityService,
        protected route: ActivatedRoute,
        protected authService: AuthService, ) {
        super(regionService,
            router,
            dialogService,
            snackBar, authService);

    }

    public onAdd() {
        this.region = new Region();
        this.region.name = '';
        this.region.city = new City();
        this.region.city.name = '';
        this.openDialog(this.region);
    }

    public onUpdate(id: number, row: Region) {
        this.region = row;
        this.openDialog(this.region);
    }

    public openDialog(row: Region) {
        let flag: boolean = false;
        if (this.region.name == "" && this.region.city.id == undefined) {
            flag = true;
        } else {
            flag = false;
        }
        let dialogRef = this.dialogService.open(AreaDialog, {
            width: '500px',
            data: {
                region: this.region, listOfCities: this.listOfCities,
                regionName: this.region.name
            }
        })

        dialogRef.afterClosed().subscribe(result => {
            if (result == undefined) {
                this.getAreas();
            } else{
                if (!flag) {
                    let region = new Region();
                    region.id = row.id;
                    region.name = row.name;
                    region.city = row.city;
                    this.regionService.countByNameAndCityIdEqualsAndIdNot(this.region.name, this.region.city.id, this.region.id)
                        .subscribe(data => {
                            let countAreaName = data;
                            if (countAreaName > 0) {
                                this.snackBar.open("Area Name already in use.", "Close", { duration: 3000 });
                                throw new Error("Area Name already in use.");
                            }
                            else {
                                this.regionService.save(row).subscribe(data => {
                                    this.getAreas();
                                });
                            }
                        });
                } else {
                    this.regionService.countByNameAndCityIdEquals(this.region.name, this.region.city.id)
                        .subscribe(data => {
                            let countRegionName = data;
                            if (countRegionName > 0) {
                                this.snackBar.open("Area Name already in use.", "Close", { duration: 3000 });
                                throw new Error("Area Name already in use.");
                            }
                            else {
                                this.regionService.save(row).subscribe(data => {
                                    this.getAreas();
                                });
                            }
                        });
                }
            }
        });
        this.dataTable.refresh();
    }

    public getAreas() {
        this.regionService.filter(this.currentPage - 1, this.pageSize
            , this.searchTerm, this.sortBy, this.sortOrder).subscribe(data => {
                this.allModels = data['content'];
                this.filteredTotal = data['totalElements'];
            })
    }

    loadCities() {
        this.cityService.getActiveCities().subscribe(
            data => {
                this.listOfCities = data;
            }
        );
    }
    searchFilter(searchTerm: string) {
        console.log(searchTerm);
        if (searchTerm != "") {
            this.regionService.searchFilter(searchTerm, this.currentPage - 1, this.pageSize)
                .subscribe(data => {
                    this.allModels = data['content'];
                    this.filteredTotal = data['totalElements'];
                })
        } else {
            this.getAreas();
        }
    }

    onDeleteFailed(errorResponse: HttpErrorResponse) {
        this.snackBar.open("Process cannot be completed, the selected option is in use.", "close", { duration: 3000 });
    }

    onDeactivationFailed(errorResponse: HttpErrorResponse) {
        this.snackBar.open("Process cannot be completed, the selected option is in use.", "close", { duration: 3000 });
        this.getAreas();
    }


    compareWith(row: any, model: any): boolean {
        return row.id === model.id;
    }

    public set allModels(value: Region[]) {
        this._allRegions = value;
    }

    public get allModels() {
        return this._allRegions;
    }

    public set selectedRows(value: Region[]) {
        this._selectedRows = value;
    }

    public get selectedRows(): Region[] {
        return this._selectedRows;
    }

    public set filteredTotal(value: number) {
        this._filteredTotal = value;
    }

    public get filteredTotal() {
        return this._filteredTotal;
    }

    public set sortBy(value: string) {
        this._sortBy = value;
    }

    public get sortBy() {
        return this._sortBy;
    }

    public set sortOrder(value: TdDataTableSortingOrder) {
        this._sortOrder = value;
    }

    public get sortOrder() {
        return this._sortOrder;
    }


}

@Component({
    selector: 'area-dialog',
    templateUrl: 'area-dialog.html',
})
export class AreaDialog {
    ngOnInit() {
    }
    constructor(
        public dialogRef: MatDialogRef<AreaDialog>,
        protected cityService: CityService,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    region = new FormControl('', [
        Validators.required,
        Validators.maxLength(30),
        Validators.minLength(3)]);
    onCancle(): void {

        this.dialogRef.close();
    }

}


