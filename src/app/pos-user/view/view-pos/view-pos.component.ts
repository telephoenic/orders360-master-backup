import { AuthService } from './../../../auth/service/auth.service';
import { VendorAdminService } from './../../../vendor-admin/service/vendor-admin.service';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import {
	TdPagingBarComponent, TdDialogService,
	TdDataTableComponent
} from '@covalent/core';
import { Component, ViewChild, OnInit } from '@angular/core';
import {
	MatSnackBar
} from "@angular/material";

import { PosService } from '../../service/pos.service';
import { Router } from '@angular/router';
import { PosUser } from '../../pos-user.model';
import { NgxPermissionsService } from 'ngx-permissions';
import { AssignedVendorsService } from '../../service/assigned-vendors';
import { User } from '../../../auth/user.model';
import { PosAassignedVendor } from '../../pos-assigned-vendor';
import { PosAssignedVendorService } from '../../service/pos-assigned-vendor.service';
import { OrderService } from '../../../orders/service/order.service';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
	selector: 'app-view',
	templateUrl: './view-pos.component.html',
	styleUrls: ['./view-pos.component.css']
})
export class ViewPosFormComponent extends BaseViewComponent implements OnInit {

	private _currentPage: number = 1;
	private _fromRow: number = 1;
	private _allPoSs: PosUser[] = [];
	private _allPosAssignedVendor: PosAassignedVendor[] = [];
	private _errors: any = '';
	private _selectedRows: PosAassignedVendor[] = [];
	private _pageSize: number;
	private _filteredTotal: number;
	private _sortBy: string = '';
	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
	private _searchTerm: string = '';

	@ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
	@ViewChild('dataTable') dataTable: TdDataTableComponent;



	columnsConfig: ITdDataTableColumn[] = [
		{ name: 'posUser.name', label: 'Shop Name', tooltip: "Shop Name" },
		{ name: 'posUser.email', label: 'E-mail', tooltip: "E-mail", width: 382 },
		{ name: 'posUser.mobileNumber', label: 'Shop Mobile Number', tooltip: "Shop Mobile Number", width: 150 },
		{ name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive", width: 100 },
		{ name: 'posUser.id', label: 'Edit', tooltip: "Edit", width: 100 }
	];

	constructor(protected posService: PosService,
		protected posAssignedVendorService: PosAssignedVendorService,
		protected router: Router,
		protected dialogService: TdDialogService,
		protected snackBar: MatSnackBar,
		protected permissionsService: NgxPermissionsService,
		protected vendorAdminService: VendorAdminService,
		protected athuService: AuthService,
		protected assignedVendorsService: AssignedVendorsService,
		protected orderService: OrderService) {
		super(posService, router, dialogService, snackBar, athuService);
	}

	ngOnInit() {
		super.ngOnInit();

		this.permissionsService.hasPermission("ROLE_POS_EDIT").then((result: boolean) => {
			if (!result) {
				for (let i = 0; i < this.dataTable.columns.length; i++) {
					if (this.dataTable.columns[i].name == 'id') {
						this.dataTable.columns.splice(i, 1);
					}
				}
			}
		});


		this.permissionsService.hasPermission("ROLE_POS_ACTIVATE_DEACTIVATE_PER_ROW").then((result: boolean) => {
			if (!result) {
				for (let i = 0; i < this.dataTable.columns.length; i++) {
					if (this.dataTable.columns[i].name == 'inactive') {
						this.dataTable.columns.splice(i, 1);
					}
				}
			}
		});
	}

	filterByVendor(pageNumber: number,
		pageSize: number,
		searchTerm: string,
		sortBy: string,
		sortOrder: string,
		vendorId: number) {
		this.posAssignedVendorService.getAllPosByVendorId(pageNumber, pageSize, vendorId, searchTerm, sortBy, sortOrder).subscribe(
			data => {
				console.log(data["content"])
				this.allModels = data["content"];
				this.filteredTotal = data["totalElements"]
			}
		);
	}


	onActivateDeactivePerRow(value, row) {
		this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId => {
			let posIds: number[] = [];
			posIds.push(row.posUser.id)
			this.orderService.getAllOrderByPosAndVendorId(posIds, vendorId).subscribe(result => {
				if (result == true) {
					this.snackBar.open("One of the selected POS has active orders.", "Close", { duration: 3000 });
					super.onRefresh();
				} else {
					super.onActivateDeactivePerRow(value, row);
				}
				
			})

		})
	}

	onActivateDeactivateSelectedRows(activate: boolean) {
		if (this.selectedRows.length > 0) {
			this.dialogService.openConfirm({
				message: "Are you sure you want to " + (activate ? "activate" : "deactivate") + " the selected row(s)?",
				title: "confirmation",
				cancelButton: "Disagree",
				acceptButton: "Agree"
			})
				.afterClosed().subscribe((accept: boolean) => {
					if (accept) {
						let posId: number[] = [];
						let ids: number[] = [];
						this.selectedRows.forEach(obj => {
							ids.push(obj.id);
							posId.push(obj.posUser.id);
						});
						this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId => {
							this.orderService.getAllOrderByPosAndVendorId(posId, vendorId).subscribe(result => {
								if (result == true) {
									this.snackBar.open("One of the selected POS has active orders.", "Close", { duration: 3000 });
									throw new Error("One of the selected POS has active orders.")
								} else {
									this.posService.changeStatusTo(activate, ids).subscribe(
										data => {
											this.filter(this.currentPage - 1, this.pageSize);
											this.snackBar.open((activate ? "Activated" : "Deactivated") + " successfully", "Close", { duration: 3000 });
											this.selectedRows = [];
										},
										(errorResponse: HttpErrorResponse) => {
											if (activate) {
												super.onActivationFailed(errorResponse);
											} else {
												this.onDeactivationFailed(errorResponse);
											}
										}
									)
								}
							})
						})
					}
				});
		} else if (this.allModels.length == 0) {
			this.onNoDataFoundToActivateOrDeactivate(activate);
		} else {
			this.onNoSelectedRowsToActivateOrDeactivate(activate);
		}
	}


	onDelete() {
		if (this._selectedRows.length == 0) {
			this.snackBar.open("No selected rows.", "Close", { duration: 3000 });
		} else if (this._selectedRows.length > 0) {
			this.dialogService.openConfirm({
				message: "Are you sure you want to delete the selected row(s)?",
				title: "confirmation",
				cancelButton: "Disagree",
				acceptButton: "Agree"
			}).afterClosed().subscribe((accept: boolean) => {
				let ids: number[] = [];
				let selctedPoss: number[] = [];
				if (accept) {
					this._selectedRows.forEach(obj => {
						ids.push(obj.id);
						selctedPoss.push(obj.posUser.id);
					});

					this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(data => {
						this.orderService.getAllOrderByPosAndVendorId(selctedPoss, data).subscribe(result => {
							if (result == true) {
								this.snackBar.open("One of the selected POS has active orders.", "Close", { duration: 3000 });
								throw new Error("One of the selected POS has active orders.")
							} else {
								this.posAssignedVendorService.onDeletePosAssignVendor(ids).subscribe(data => {
									this.onAfterDelete();
									this.dataTable.refresh();
									this.selectedRows = [];
									selctedPoss = [];
								});
							}
						});
					})

				}
			}
			);
		} else {
			this.snackBar.open("Please select any item.", "Close", { duration: 3000 });
		}
	}


	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}
	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

	public get currentPage(): number {
		return this._currentPage;
	}

	public set currentPage(value: number) {
		this._currentPage = value;
	}

	public get fromRow(): number {
		return this._fromRow;
	}

	public set fromRow(value: number) {
		this._fromRow = value;
	}

	public get allModels(): PosAassignedVendor[] {
		return this._allPosAssignedVendor;
	}

	public set allModels(value: PosAassignedVendor[]) {
		this._allPosAssignedVendor = value;
	}

	public get errors(): any {
		return this._errors;
	}

	public set errors(value: any) {
		this._errors = value;
	}

	public get selectedRows(): PosAassignedVendor[] {
		return this._selectedRows;
	}

	public set selectedRows(value: PosAassignedVendor[]) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string {
		return this._sortBy;
	}

	public set sortBy(value: string) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder) {
		this._sortOrder = value;
	}

	public get searchTerm(): string {
		return this._searchTerm;
	}

	public set searchTerm(value: string) {
		this._searchTerm = value;
	}
}
