import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAssignedVendorFormComponent } from './view-assigned-vendor.component';

describe('ViewAssignedVendorFormComponent', () => {
  let component: ViewAssignedVendorFormComponent;
  let fixture: ComponentFixture<ViewAssignedVendorFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAssignedVendorFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAssignedVendorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
