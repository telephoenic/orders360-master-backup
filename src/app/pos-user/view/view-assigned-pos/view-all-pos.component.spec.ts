import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllPosComponent } from './view-all-pos.component';

describe('ViewAssignedPosFormComponent', () => {
  let component: ViewAllPosComponent;
  let fixture: ComponentFixture<ViewAllPosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAllPosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllPosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
