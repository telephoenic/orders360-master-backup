import {BaseViewComponent} from '../../../common/view/base-view-component';
import { TdDataTableService, TdDataTableSortingOrder, ITdDataTableSortChangeEvent, ITdDataTableColumn } from '@covalent/core';
import { IPageChangeEvent, TdPagingBarComponent, TdDialogService,
  TdDataTableComponent } from '@covalent/core';
import {Component, ViewChild, OnInit, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {
  MatTable, MatHeaderCellDef, MatCellDef,
  MatColumnDef, MatPaginator, MatSort,
  MatInput, MatCheckbox,
  MatButton,
  MatSnackBar
} from "@angular/material";
import { VendorService } from '../../../vendor/Services/vendor.service';
import { PosUser } from '../../pos-user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AssignedVendorsService } from '../../service/assigned-vendors';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Vendor } from '../../../vendor/vendor.model';

@Component({
  selector: 'view-all-pos',
  templateUrl: './view-all-pos.component.html',
  styleUrls: ['./view-all-pos.component.css']
})

export class ViewAllPosComponent extends BaseViewComponent implements OnInit {



  private _currentPage: number = 1;
	private _fromRow: number = 1;
  public _allAssignedPos: PosUser[] = [];
  private _errors: any = '';
  private _selectedRows: PosUser[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string ='';
	
  @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
  @ViewChild('dataTable') dataTable : TdDataTableComponent;

  columnsConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name' },
		{ name: 'code', label: 'Code', width: 200 }
  ];

	constructor(protected assignedVendorsService: AssignedVendorsService,
							protected vendorService: VendorService,
							protected route:ActivatedRoute,
              protected router:Router,
              protected dialogService : TdDialogService,
							protected snackBar:MatSnackBar
							, public dialog: MatDialog) { 
    super(assignedVendorsService, router, dialogService, snackBar);
  }

  ngOnInit() {
  }

	onSave(){
		
	}

	

	onDelete(){

	}

	
  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): PosUser[]  {
		return this._allAssignedPos;
	}

	public set allModels(value: PosUser[] ) {
		this._allAssignedPos = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): PosUser[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: PosUser[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
	}


	
}
