import {User} from '../../../auth/user.model';
import {AuthService} from '../../../auth/service/auth.service';
import {BaseViewComponent} from '../../../common/view/base-view-component';
import { TdDataTableSortingOrder, ITdDataTableColumn } from '@covalent/core';
import { TdPagingBarComponent, TdDialogService, TdDataTableComponent } from '@covalent/core';
import {Component, ViewChild, Input, OnInit, Inject} from '@angular/core';
import { MatSnackBar} from "@angular/material";
import {PosUser} from '../../pos-user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PosService } from '../../service/pos.service';
import { PosType } from '../../PosType.model';
import { PosTypeService } from '../../service/pos-type.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'view-data-pos',
  templateUrl: './view-data-pos.component.html',
  styleUrls: ['./view-data-pos.component.css']
})

export class ViewDataPosComponent extends BaseViewComponent implements OnInit {

	
  @Input() isAssign:boolean;

  private assign:string;
  private dataTitle:string;
  private user:User;
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  public _allPos: PosUser[] = [];
  listOfTypes:PosType[];
  private _errors: any = '';
  private _selectedRows: PosUser[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string ='';
  private snackMSG = "";
  @Input()
public filterationUrl:string;

@Input()
public viewDataPosComponent:ViewDataPosComponent;

  @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
  @ViewChild('dataTable') dataTable : TdDataTableComponent;

  columnsConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name' },
	{ name: 'mobileNumber', label: 'Mobile', width: 200 },
	{ name: 'posType.type', label: 'Type', width: 200 },
  ];

	constructor(protected posService: PosService,
				protected posTypeService: PosTypeService,
				protected authService: AuthService,
				protected route:ActivatedRoute,
              	protected router:Router,
              	protected dialogService : TdDialogService,
							protected snackBar:MatSnackBar
							, public dialog: MatDialog) { 

    super(posService, router, dialogService, snackBar);
  }

  refreshData() {
	if(!this.isAssign)
	{
		this.assign = "Assign";
		this.dataTitle = "All point of sales";
		this.posService.getPosWhereVendorNotId(this.user.id).subscribe(
			data => {
				this.allModels = data;
				this.dataTable.refresh();
			}
		);
	}
	else{
		this.assign = "Deassign";
		this.posService.getPosWhereVendorId(this.user.id).subscribe(
			data => {
				this.allModels = data;
				this.dataTable.refresh();
			}
		);

		this.dataTitle = "Assigned point of sales";
	}
  }
  ngOnInit() {

		this.user = this.authService.getCurrentLoggedInUser();
		this.refreshData();

		if(!this.isAssign){
			this.snackMSG = "Are you sure you want to assign the selected row(s)?";
		}else {
			this.snackMSG = "Are you sure you want to deassign the selected row(s)?";
			this.loadPosTypeOptions();
		}
			
  }

  loadPosTypeOptions() {
    this.posTypeService.filter(0,environment.maxListFieldOptions,"", "", "")
                        .subscribe(
						  data => {this.listOfTypes = data["content"]
						}
                        );
  }

  search(searchTerm:string ){
		if(this.isSearchTermCleared(searchTerm))
		{
			this.refreshData();
		} else{
		this.searchData(searchTerm, this.user.id);
		}
  }

  searchData(textSearch:string, vendorId:number){
	if(!this.isAssign)
	{
		this.posService.filterNotAssignedData(textSearch, this.user.id).subscribe(
			data => {
				this.allModels = data;
			}
		);
	}
	else{
		this.posService.filterAssignedData(textSearch, this.user.id).subscribe(
			data => {
				this.allModels = data;
			}
		);
	}
	}
	

	onChangeType(){
	  this.openDialog();
	}


	openDialog(): void {
		let dialogRef = this.dialog.open(DialogChangePosType, {
		  width: '500px',
		  data: { types:this.listOfTypes }
			
		});
	
		dialogRef.afterClosed().subscribe(result => {
				if(result!=null && result != ""){
					
		}});
			
		}


	onAssign(){
		if(this.selectedRows.length > 0) {
			let unSelected = 	this._allPos.filter(item => this.selectedRows.indexOf(item) < 0);
			this.dialogService.openConfirm({
			message: this.snackMSG,
			title: "confirmation",
			cancelButton: "Disagree",
			acceptButton: "Agree"
			}).afterClosed().subscribe((accept:boolean) => 
			{
					if(accept) {
						let selectedPossIDs : number[]=[];
						this.selectedRows.forEach(pos => {
							selectedPossIDs.push(pos.id);
						});
						if(!this.isAssign)
						{
							this.posService.saveToPosVendor(selectedPossIDs, this.user.id).subscribe(
								data => {
									this.viewDataPosComponent.refreshData();
									},	error => {
										this.viewDataPosComponent.refreshData();
									}            
							)
						}else{
							this.posService.deleteAssignedPosWhereVendorIdIn(selectedPossIDs, this.user.id).subscribe(
									data => {
										this.viewDataPosComponent.refreshData();
										}  ,	error => {
											this.viewDataPosComponent.refreshData();
										}            
								)
					
						}
						this.selectedRows = [];
						this._allPos = unSelected;
					}
			} 
			);
	}
		
	
	
}

	
  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): PosUser[]  {
		return this._allPos;
	}

	public set allModels(value: PosUser[] ) {
		this._allPos = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): PosUser[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: PosUser[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
	}


	
}


@Component({
	selector: 'pos-type-dialog',
	templateUrl: 'pos-type-dialog.html',
  })
  export class DialogChangePosType {
	  constructor(
		  public dialogRef: MatDialogRef<DialogChangePosType>,
		  @Inject(MAT_DIALOG_DATA) public data: any) { }
		  
	  onNoClick(): void {
	   
		  this.dialogRef.close();
	  }
	
	  }
  
