import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl, Validators, NgForm } from '@angular/forms';
import { Address } from '../../common/address.model';
import { MatSnackBar } from '@angular/material';
import { PosUser } from '../pos-user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { PosService } from '../service/pos.service';
import { AssignedVendorsService } from '../service/assigned-vendors';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { ViewAssignedVendorFormComponent } from '../view/view-assigned-vendor/view-assigned-vendor.component';
import { PosTypeService } from '../service/pos-type.service';
import { PosType } from '../PosType.model';
import { BaseUserManagementForm } from '../../user-management/base-user-management-form';
import { UserPrivilegeProfileComponent } from '../../user-privilege/views/user-privilege-profile.component';
import { Region } from '../../location/region/Region.model';
import { RegionService } from '../../location/region/service/region.service';
import { City } from '../../location/city/City.model';
import { CityService } from '../../location/city/service/city.service';
import { User } from './../../auth/user.model';
import { AuthService } from '../../auth/service/auth.service';
import { PosAassignedVendor } from '../pos-assigned-vendor';
import { PosAssignedVendorService } from '../service/pos-assigned-vendor.service';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';
import { VendorService } from '../../vendor/Services/vendor.service';
import { PosGroupManagementService } from '../../posgroupmanagement/service/pos-group-management.service';
import { PosGroupManagement } from '../../posgroupmanagement/pos-group-management.model';
import { PosSearchComponent } from '../pos-search/pos-search.component';
import { FcmService } from '../../fcm/services/fcm.service';
import { AppComponent } from '../../../app/app.component';

declare var google: any;

@Component({
  selector: 'app-pos-form',
  templateUrl: './pos-form.component.html',
  styleUrls: ['./pos-form.component.css']
})
export class PosFormComponent extends BaseUserManagementForm implements OnInit {

  @ViewChild("posForm") posForm: NgForm;
  @ViewChild(ViewAssignedVendorFormComponent) assignedVendorsComponent: ViewAssignedVendorFormComponent;
  _posUser: PosUser;
  posAssignedVendor: PosAassignedVendor;
  listOfTypes: PosType[];
  listOfRegions: Region[];
  listOfCities: City[] = [];
  listOfPosGroup: PosGroupManagement[] = [];
  _isEdit: boolean = false;
  options: any;
  overlays: any[];
  searchScreen: boolean;
  private _vendorId: number;
  private _isAssign;




  @ViewChild("notSelectedPrivilegeProfiles")
  notSelectedPrivilegeProfiles: UserPrivilegeProfileComponent;

  @ViewChild("selectedPrivilegeProfiles")
  selectedPrivilegeProfiles: UserPrivilegeProfileComponent;

  constructor(protected posService: PosService,
    protected posTypeService: PosTypeService,
    protected posAssignVednor: PosAssignedVendorService,
    protected vendorAdminService: VendorAdminService,
    protected posGroupManagementService: PosGroupManagementService,
    protected vendorService: VendorService,
    protected regionService: RegionService,
    protected cityService: CityService,
    protected assignedVendorsService: AssignedVendorsService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected snackBar: MatSnackBar,
    protected fcmService: FcmService,
    protected authService: AuthService, ) {

    super(posService,
      route,
      router,
      snackBar);
  }

  ngOnInit() {
    debugger
    this.searchScreen = true;
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    this.getVendorIdByLoggedVenodrAdmin();
    if (this.posService.selectedScreen)
      this.loadRegionOptions();
    this._posUser = this.posService.posUser;
    this.selectedSecreen = environment.ENUM_CODE_USER_TYPE.PosUser;
    this.loadCityOptions();
    this.loadPosGroup();
    if (this.route.snapshot.queryParams["edit"] == '1') {
      debugger
      this._isEdit = true;
      this._posUser = this.posService.posAssignedVednor.posUser;
      console.log(this._posUser);
      this.onLoadRegions(null);
      this.options = {
        center: { lat: this._posUser.latitude, lng: this._posUser.longitude },
        zoom: 12
      };
      this.overlays = [new google.maps.Marker({ position: { lat: this._posUser.latitude, lng: this._posUser.longitude }, title: "Konyaalti" })];
    } else {
      this.assignedVendorsService.vendors = [];
    }
  }

  loadRegionOptions() {
    this.regionService.filter(0, environment.maxListFieldOptions, "", "", "")
      .subscribe(
        data => this.listOfRegions = data["content"]
      );
  }

  loadPosGroup() {
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(
      vendorId => {
        this.posGroupManagementService.getGroupsByVednor(vendorId).subscribe(data => {
          this.listOfPosGroup = data;
        });

      });

  }

  onLoadRegions(event) {
    let region = new Region();
    if (event !== null)
      region.city.id = event.value;
    else
      region.city = this._posUser.region.city;

    this.regionService.findByExample(region)
      .subscribe(
        data => this.listOfRegions = data["content"]
      );
  }

  loadCityOptions() {
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(
      vendorId => {
        this.vendorService.findVendorById(vendorId).subscribe(data => {
          this.cityService.findAllCiityByCountryId(data.country.id).subscribe(
            cities => {
              this.listOfCities = cities;
            }

          );

        })
      });
  }

  getVendorIdByLoggedVenodrAdmin() {
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id)
      .subscribe((vendorId: number) => {
        this._vendorId = vendorId;
      });

  }

  onBeforeCancel() {
    this.posService.selectedScreen = false;
  }

  onBeforeSaveAndNew(posUser: PosUser) {
    this.showSpinnerOnSaveAndNew = true;
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    posUser.userCreated = this.loggedUser.id;
    posUser.vendorId = this._vendorId;
    super.onBeforeSaveAndNew(posUser);
  }

  onBeforeSave(posUser: PosUser) {
    debugger;
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    posUser.userCreated = this.loggedUser.id;
    posUser.vendorId = this._vendorId;
    super.onBeforeSave(posUser);

  }

  onBeforeEdit(posUser: PosUser) {
    debugger;
    this.showSpinner = true;
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    posUser.userCreated = this.loggedUser.id;
    posUser.vendorId = this._vendorId;
    super.onBeforeEdit(posUser);
  }
  onSaveAndNewCompleted() {
    this.showSpinnerOnSaveAndNew = false;
    this.getNotSelectedPrivilegeProfiles().filterPrivilegeProfileForUser();
    this.notSelectedPrivilegeProfiles.dataTable.refresh();

    this.selectedPrivilegeProfiles.allModels = [];
    this.selectedPrivilegeProfiles.dataTable.refresh();

  }

  onEditCompleted() {
    if (this._posUser.registrationId != null && !this._posUser.assignVendor) {
      this.onSendCompletedNotification();
    }
  }


  onSendCompletedNotification() {
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vId => {
      this.vendorService.findVendorById(vId).subscribe(data => {
        let title: string;
        let body: string;
        let pos: PosUser[] = [];

        title = "Assign To Vendor! ";
        body = "Your request accepted for, " + data.name + " you can login to the " + AppComponent.KEY_VALUE_ORDERS_360 + " using your user name and password ";
        pos.push(this._posUser);

        this.fcmService.onSend(pos, body, title).subscribe();

      })

    })
  }


  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): PosUser {
    return this._posUser;
  }

  set model(posUser: PosUser) {
    this._posUser = posUser;
  }

  initModel(): PosUser {
    return new PosUser();
  }

  ngFormInstance(): NgForm {
    return this.posForm;
  }

  getSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.selectedPrivilegeProfiles;
  }
  getNotSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.notSelectedPrivilegeProfiles;
  }
}
