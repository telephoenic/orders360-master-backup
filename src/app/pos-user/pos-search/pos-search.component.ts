import { Component, OnInit } from '@angular/core';
import { PosService } from '../service/pos.service';
import { User } from '../../auth/user.model';
import { AuthService } from '../../auth/service/auth.service';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';
import { Router } from '@angular/router';
import { PosAassignedVendor } from '../pos-assigned-vendor';
import { PosUser } from '../pos-user.model';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { TdDialogService } from '@covalent/core';

@Component({
  selector: 'app-pos-search',
  templateUrl: './pos-search.component.html',
  styleUrls: ['./pos-search.component.css']
})
export class PosSearchComponent implements OnInit {

  private _mobileNumber: string;
  private _loggedUser: User;
  possAssignVendor: PosAassignedVendor;
  private _isSearchScreen: boolean;


  constructor(
    protected posService: PosService,
    protected authService: AuthService,
    private router: Router,
    protected vendorAdminService: VendorAdminService,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar) { }

  ngOnInit() {
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    this.possAssignVendor = new PosAassignedVendor();
  }

  search() {
    this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(
      vendorId => {
        this.posService.findPosUser(this._mobileNumber, this._loggedUser.id, vendorId).subscribe(data => {
          if (data == null) {
            this.router.navigate(['/pos_user/pos-form']);
            this.possAssignVendor.posUser.mobileNumber = this.mobileNumber;
            let posUser: PosUser = new PosUser();
            posUser.mobileNumber = this.mobileNumber;
            this.posService.posUser = posUser;
          } else {
            if (data.assignVendor == false) {
              this.dialogService.openConfirm({
                message: "POS already exists in Order 360 but not added to your list,please press on OK button to add under your list",
                title: "Assign POS",
                cancelButton: "Disagree",
                acceptButton: "Agree"
              }).afterClosed().subscribe((accept: boolean) => {
                if (accept) {
                  this.posService.selectedScreen=true;
                  this.router.navigate(['/pos_user/pos-form'],{ queryParams: { edit: 1 } });
                  this.posService.posAssignedVednor.posUser = data;
                  this.posService.posAssignedVednor.posUser.vendorId=vendorId;
                }
              }
              );
            } else {
              this.snackBar.open("POS already exists in vendor list","Close", { duration: 3000 })
            }

          }
        });


      });
  }

  onSearchError(errorResponse: HttpErrorResponse) {
    this.snackBar.open(errorResponse.error.message, "Close", { duration: 3000 });
  }

  public get mobileNumber(): string {
    return this._mobileNumber;
  }
  public set mobileNumber(value: string) {
    this._mobileNumber = value;
  }

  public get loggedUser(): User {
    return this._loggedUser;
  }
  public set loggedUser(value: User) {
    this._loggedUser = value;
  }

  public get isSearchScreen(): boolean {
    return this._isSearchScreen;
  }
  public set isSearchScreen(value: boolean) {
    this._isSearchScreen = value;
  }


}
