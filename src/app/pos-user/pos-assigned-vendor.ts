import { PosUser } from "./pos-user.model";
import { Vendor } from "../vendor/vendor.model";
import { Region } from "../location/region/Region.model";
import { PosGroupManagement } from "../posgroupmanagement/pos-group-management.model";
import { BaseModel } from "../common/model/base-model";

export class PosAassignedVendor extends BaseModel{
    id: any;
    inactive: boolean;
    posUser:PosUser;
    vendor:Vendor;
    region:Region;
    posGroup:PosGroupManagement;

    constructor(){
        super();
        this.posUser=new PosUser();
        this.vendor=new Vendor();
        this.region=new Region();
        this.posGroup=new PosGroupManagement();
    }



}