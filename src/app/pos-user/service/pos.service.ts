import { BaseModel } from "../../common/model/base-model";
import { Injectable } from "@angular/core";
import { PosUser } from "../pos-user.model";
import { BaseService } from "../../common/service/base.service";
import { AuthService } from "../../auth/service/auth.service";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { Observable } from "rxjs/Observable";
import { PosAassignedVendor } from "../pos-assigned-vendor";
import { catchError, retry } from "rxjs/operators";
import { of } from "rxjs/internal/observable/of";
import { MatSnackBar } from "@angular/material";
import { FcmFilter } from "../../fcm/fcm-filter";
import { TSMap } from "typescript-map";

@Injectable()
export class PosService extends BaseService {
  posUser: PosUser = new PosUser();
  posAssignedVednor: PosAassignedVendor = new PosAassignedVendor();
  navigateToAfterCompletion: string;
  posUserList: PosUser[] = [];
  regionsIds: number[] = [];
  posGroupsIds: number[] = [];
  selectedScreen: boolean = false;

  constructor(
    http: HttpClient,
    protected authService: AuthService,
    protected snackBar: MatSnackBar
  ) {
    super(http, "pos_user", authService);
  }

  getPosWhereVendorNotId(userId: number): Observable<any> {
    return this.http.get(
      environment.baseURL + "pos_user/not_assigned_pos" + "/" + userId
    );
  }

  getPosWhereVendorId(userId: number): Observable<any> {
    return this.http.get(
      environment.baseURL + "pos_user/assigned_pos" + "/" + userId
    );
  }

  getPosAssignWhereVendorId(
    pageNumber: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    fcmFilter: TSMap<string, number[]>
  ): Observable<any> {
    let parameterList: { paramName: string; paramValue: any }[] = [];
    parameterList.push({ paramName: "search_term", paramValue: searchTerm });
    return this.http.post(
      environment.baseURL +
        "pos_user/find_assigned_pos_by_vendor_and_group/" +
        pageNumber +
        "/" +
        pageSize +
        "/" +
        vendorId,
      fcmFilter.toJSON(),
      {
        params: this.prepareSearchParameters(searchTerm, "", ""),
        headers: BaseService.HEADERS
      }
    );
  }

  deleteAssignedPosWhereVendorIdIn(possIds: number[], userId: number) {
    return this.http.post(
      environment.baseURL + "pos_user/assigned_pos_delete" + "/" + userId,
      JSON.stringify(possIds),
      { headers: BaseService.HEADERS }
    );
  }

  saveToPosVendor(possIds: number[], userId: number) {
    return this.http.post(
      environment.baseURL + "pos_user/assigned_pos_insert" + "/" + userId,
      JSON.stringify(possIds),
      { headers: BaseService.HEADERS }
    );
  }

  filterAssignedData(searchTerm: string, vendorId: number): Observable<any> {
    return this.http.get(
      environment.baseURL +
        "pos_user/filter_assigned_pos" +
        "/" +
        searchTerm +
        "/" +
        vendorId
    );
  }

  filterNotAssignedData(searchTerm: string, vendorId: number): Observable<any> {
    return this.http.get(
      environment.baseURL +
        "pos_user/filter_not_assigned_pos" +
        "/" +
        searchTerm +
        "/" +
        vendorId
    );
  }

  getPromotedPos(promotionId?: number): Observable<any> {
    return this.http.get(
      environment.baseURL + "pos_user/pos_for_promotion/" + promotionId
    );
  }

  getPosWhereSurveyNotId(surveyId: number, vendorId: number): Observable<any> {
    return this.http.get(
      environment.baseURL +
        "pos_user/not_assigned_pos_survey" +
        "/" +
        surveyId +
        "/" +
        vendorId
    );
  }

  getPosWhereSurveyId(
    vendorId: number,
    fcmFilter: TSMap<string, number[]>,
    surveyId: number
  ): Observable<any> {
    return this.http.post(
      environment.baseURL +
        "pos_user/assigned_pos_survey/" +
        vendorId +
        "/" +
        surveyId,
      fcmFilter.toJSON(),
      { headers: BaseService.HEADERS }
    );
  }

  findPosUser(
    mobileNumber: string,
    userId: number,
    vendorId: number
  ): Observable<any> {
    return this.http
      .get(
        environment.baseURL +
          "pos_user/find_pos_by_mobileNo" +
          "/" +
          mobileNumber +
          "/" +
          userId +
          "/" +
          vendorId
      )
      .pipe(
        retry(0),
        catchError(err => {
          debugger;
          err.error.code.charAt(0).toUpperCase() +
            err.error.code.slice(1).toLowerCase();
          this.snackBar.open(
            err.error.code.charAt(0).toUpperCase() +
              err.error.code.slice(1).toLowerCase(),
            "Close",
            { duration: 3000 }
          );
          // this.snackBar.open(err.error.code, "Close", { duration: 3000 });
          throw new Error(err.error.code.toLowerCase());
          return of(err);
        })
      );
  }

  public getPosUserIdByMobileNumber(mobileNumber: string) {
    return this.http.get(
      environment.baseURL + "pos_user/get-pos-user-id/" + mobileNumber
    );
  }

  get model() {
    return this.posAssignedVednor;
  }
  set model(model: BaseModel) {
    this.posAssignedVednor = <PosAassignedVendor>model;
  }
}
