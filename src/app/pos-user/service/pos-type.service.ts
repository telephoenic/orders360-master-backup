import {AuthService} from '../../auth/service/auth.service';
import {BaseService} from '../../common/service/base.service';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { PosType } from '../PosType.model';

@Injectable()
export class PosTypeService extends BaseService{
  navigateToAfterCompletion: string;
  posType:PosType;

  constructor(protected http:HttpClient,
    protected authService:AuthService) { 
    super(http, "pos_type", authService);
  }

  get model() {
    return this.posType;
  }

  set model(model:PosType) {
    this.posType = <PosType> model;
  }
}
