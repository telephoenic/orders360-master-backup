import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { PosAassignedVendor } from '../pos-assigned-vendor';
import { BaseModel } from '../../common/model/base-model';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PosAssignedVendorService extends BaseService {

  posAssignedVednor: PosAassignedVendor;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService) {
    super(http, "pos_assigned_vendor", authService)
  }

  getAllPosByVendorId(pageNum: number, pageSize: number,
    vendorId: number, searchTerm: string,
    sortBy: string, sortOrder: string): Observable<any> {
    return this.http.get(environment.baseURL + "pos_assigned_vendor/pos_for_vendor/" + pageNum + "/" + pageSize + "/" + vendorId,
      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      });
  }

  onDeletePosAssignVendor(ids: number[]) {
    return this.http.post(environment.baseURL + "pos_assigned_vendor/delete_pos_assigned_vendor",
      JSON.stringify(ids), { headers: this.getBasicHeaders() });

  }

  onFindPosAssignVendorByPosId(posId: number): Observable<any> {
    return this.http.get(environment.baseURL + "pos_assigned_vendor/find_pos_assign_vendor/" + posId)

  }

  assignToLoggedVendor(posId: number, vendorId: number):Observable<any>{
    return this.http.get(environment.baseURL + "pos_assigned_vendor/assign_to_vendor/" + posId + "/" + vendorId)
  }

  findPosUserById(id:number,vendorId:number):Observable<any>{
    return this.http.get(environment.baseURL+"pos_assigned_vendor/find_by_posId/"+id+"/"+vendorId);
  }


  get model() {
    return this.posAssignedVednor;
  }
  set model(model: BaseModel) {
    this.posAssignedVednor = <PosAassignedVendor>model;
  }
}
