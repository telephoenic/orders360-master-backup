import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { VendorAddress } from "../../common/vendor-address.model";
import { Address } from "../../common/address.model";
import { MatSort } from '@angular/material';
import { BaseModel } from '../../common/model/base-model';
import { environment } from "../../../environments/environment";
import { BaseService } from '../../common/service/base.service';
import { Vendor } from '../../vendor/vendor.model';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AssignedVendorsService extends BaseService {
  isDeleted: Boolean = false;
  vendors: Vendor[] = [];
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService) {
    super(http, "pos_assigned_vendor", authService)
  }




  get model() {
    // return this.vendorAddress;
    return null;
  }
  set model(model: BaseModel) {
    //this.vendorAddress = <VendorAddress>model;
  }
}