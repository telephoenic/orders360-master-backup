import {Vendor} from '../vendor/vendor.model';
import { PosType } from './PosType.model';
import { UserPrivilegeProfile } from '../user-privilege/user-privilege-profiles';
import { UserPrivilegeProfileAware } from '../user-management/user-privilege-profile-aware';
import { Region } from '../location/region/Region.model';
import { UserType } from '../lookups/user-type.model';
import { City } from '../location/city/City.model';
import { PosGroupManagement } from '../posgroupmanagement/pos-group-management.model';

const USER_TYPE:number = 3;
export class PosUser extends UserPrivilegeProfileAware{

    id: number;
    ownerName:string;
    ownerMobileNumber:string;
    landNumber:string;
    fax:string;
    name: string;
    type: UserType;
    email: string;
    mobileNumber: string;
    posType: PosType = new PosType();
    region:Region;
    registrationId:string;
    latitude:number;
    longitude:number;
    inactive:boolean;
    listOfVendors:Vendor[];
    privilegeProfiles:UserPrivilegeProfile[] = [];
    trn:number;
    userCreated:number;
    posGroup:number;
    assignVendor:boolean;
    vendorId:number;
    posCode:string;
    firstLogin:boolean;
    uuid:string;

    constructor ( ){
        super();
        this.type = new UserType(USER_TYPE);
        this.region = new Region();
    }
    

}