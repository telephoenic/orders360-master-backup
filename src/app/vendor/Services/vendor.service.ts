import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { Vendor } from "../vendor.model";
import {BaseModel} from '../../common/model/base-model';
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class VendorService  extends BaseService{
  vendor:Vendor;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService:AuthService) {
   super(http, "vendor", authService)
   }

   getCurrencyNameByVendorId(vendorId: number): Observable<any> {
    return this.http.get(environment.baseURL + "vendor/currency/" + vendorId,
    {responseType: 'text'});
  }

  getAllPromotionType():Observable<any>{
     return this.http.get(environment.baseURL + "promotion_type/get_all_promotion_type");
  }

  findVendorById(vendorId:number):Observable<any>{
    return this.http.get(environment.baseURL + "vendor/get_vendor_by_id/" + vendorId);
  }
  
  getCountryIdByVendorId(vendorId:number){
    return this.http.get(environment.baseURL + "vendor/country-for-vendor/" + vendorId);
  }

  getAllgetAllPurchaseSetup(){
    return this.http.get(environment.baseURL + "vendor/all-purchase-step");
  }

  getAllPurchaseSetUpIds(vendorId:number){
    return this.http.get(environment.baseURL + "vendor/all-purchase-setup-ids/"+vendorId);
  }

  findVendorAddressesByVendorId(vendorId: number) : Observable<any>{
    return this.http.get(environment.baseURL + "vendor/find-vendor-addreses-by-vendor-id/"+vendorId);
  }


   get model(){
     return this.vendor;
   }
   set model(model:BaseModel) {
     this.vendor = <Vendor>model;
   }
}