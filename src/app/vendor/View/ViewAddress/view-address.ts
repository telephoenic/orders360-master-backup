import { TdDataTableService, TdDataTableSortingOrder, ITdDataTableSortChangeEvent, ITdDataTableColumn } from '@covalent/core';
import { TdPagingBarComponent, TdDialogService, TdDataTableComponent  } from '@covalent/core';
import { Injectable, Inject, Input, EventEmitter, Output  } from '@angular/core';
import {Component, ViewChild, OnInit} from '@angular/core';
import {VendorAddressService} from '../../Services/vendor-address.service';
import {VendorAddress} from '../../../common/vendor-address.model';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import {BaseViewComponent} from '../../../common/view/base-view-component';
import { CityService } from '../../../location/city/service/city.service';
import { Country } from '../../../location/country/country.model';
import { VendorFormComponent } from '../../Form/vendor-form.component';
import { City } from '../../../location/city/City.model';
import { VendorService } from '../../Services/vendor.service';
const DECIMAL_FORMAT: (v: any) => any = (v: number) => v.toFixed(2);
const CODE_REGEX = /^[0-9]*$/;

/**
 * @title Table with sorting
 */
@Component({
  selector: 'view-address',
  styleUrls: ['view-address.css'],
  templateUrl: 'view-address.html',
})
@Injectable()
export class ViewAddress extends BaseViewComponent implements OnInit {

  private _currentPage: number = 1;
  private _fromRow: number = 1;
  
  public _allVendorAddresses: VendorAddress[] = [];
  private _errors: any = '';
  private _selectedRows: VendorAddress[] = [];
  private deletedRows: VendorAddress[] = [];
  private _pageSize: number = 50;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string ='';
  city: City;
  zipCode: string;
  line1: string;
  line2: string;
	
	@Input() country :Country;
	@Input() vendorId:number;
  @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
  @ViewChild('dataTable') dataTable : TdDataTableComponent;

  columnsConfig: ITdDataTableColumn[] = [
	  {name: 'address.city.country.name',label:'Country', sortable:true},
    { name: 'address.city.name',  label: 'City', sortable: true, width: 300 },
    { name: 'address.zipCode', label: 'zipCode', sortable: true,width:150 },
    { name: 'id', label: 'Edit' , width: 100, sortable: false }
  ];

  constructor(protected vendorAddressService: VendorAddressService,
			  protected _dataTableService: TdDataTableService,
			  protected router:Router,
			  protected route:ActivatedRoute,
       		  protected dialogService : TdDialogService,
			  protected snackBar:MatSnackBar,
			  protected vendorService:VendorService
			  , public dialog: MatDialog) { 
    super(null, router, dialogService, snackBar);
  }

  ngOnInit() {
		if(this.route.snapshot.queryParams["edit"] == '1') {
			this.loadSelectedVendorAddresses(this.vendorId);
		this.filteredTotal = this._allVendorAddresses.length;
	}
	else{
		this._allVendorAddresses = [];
	}
  }

  loadSelectedVendorAddresses(vendorId:number){
    this.vendorService.findVendorAddressesByVendorId(vendorId).subscribe(
      (data: VendorAddress[]) => {
		  this.vendorAddressService.vendorAddress = data;
		  this._allVendorAddresses = data;
      },
      (error) =>{console.log(error)}
      ,()=>{});
  }


	onEdit(id: number, row:VendorAddress) {
		 this.city = row.address.city;
     this.zipCode = row.address.zipCode;
     this.line1 = row.address.line1;
	   this.line2 = row.address.line2;
      this.openDialog(row);
    
	} 
	
	sort(sortEvent: ITdDataTableSortChangeEvent): void {
		this._sortBy = sortEvent.name;
		this._sortOrder = sortEvent.order;
		this.filter();
	  }
	
	  search(searchTerm: string): void {
		this.searchTerm = searchTerm;
		this.filter();
	  }

	filter(): void {
		let newData: any[] = this._allVendorAddresses;
		let excludedColumns: string[] = this.columnsConfig
		.filter((column: ITdDataTableColumn) => {
		  return ((column.filter === undefined && column.hidden === true) ||
				  (column.filter !== undefined && column.filter === false));
		}).map((column: ITdDataTableColumn) => {
		  return column.name;
		});
		newData = this._dataTableService.filterData(newData, this.searchTerm, true, excludedColumns);
		this.filteredTotal = newData.length;
		newData = this._dataTableService.sortData(newData, this._sortBy, this._sortOrder);
		newData = this._dataTableService.pageData(newData, this._fromRow, this._currentPage * 50);
		this._allVendorAddresses = newData;
	  }

	onAdd() {
		this.city = new City();
		this.zipCode = "";
		this.line1 = "";
		this.line2 = "";
	 this.openDialog(null);
	 
 } 
	openDialog(row:VendorAddress): void {
    let dialogRef = this.dialog.open(DialogAddresses, {
      width: '500px',
	  data: {  city:this.city, zipCode: this.zipCode,
		line1: this.line1,  line2: this.line2 }
        
    });

    dialogRef.afterClosed().subscribe(result => {
			if(result!=null && result != ""){
				
				if(row != null){
					row.address = result;
				}
				else{	
					let vendorAddress:VendorAddress = new VendorAddress(result);
					this._allVendorAddresses.push(vendorAddress);
				}

				this.dataTable.refresh();
    }});
		
	}
	
	onSave(){
		this.vendorAddressService.vendorAddress = this._allVendorAddresses;
	}
	onSaveDeleted(){
					this.baseService.delete(this.deletedRows).subscribe(
							data => {
									this.deletedRows = [];
							}           
					)
					this.vendorAddressService.isDeleted = false;
	}

	onDelete(){
		if(this._allVendorAddresses.length == 0){
			this.snackBar.open("No items added yet.", "Close", {duration: 3000});
		}
		else if(this.selectedRows.length > 0) {
			let unSelected = 	this._allVendorAddresses.filter(item => this.selectedRows.indexOf(item) < 0);
			this.dialogService.openConfirm({
			message: "Are you sure you want to delete the selected row(s)?",
			title: "confirmation",
			cancelButton: "Disagree",
			acceptButton: "Agree"
			}).afterClosed().subscribe((accept:boolean) => 
			{
					if(accept) {

						this._allVendorAddresses = unSelected;
						this.vendorAddressService.isDeleted = true;
						this.deletedRows = this.selectedRows;
						this.selectedRows = [];
					}
			} 
			);
	} 
	else {
		this.snackBar.open("Please select any item.", "Close", {duration: 3000});
	}
	}
  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): VendorAddress[]  {
		return this._allVendorAddresses;
	}

	public set allModels(value: VendorAddress[] ) {
		this._allVendorAddresses = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): VendorAddress[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: VendorAddress[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
	}
}

@Component({
  selector: 'address-dialog',
  templateUrl: 'address-dialog.html',
})
export class DialogAddresses {
	listOfCities: City;
	ngOnInit(){
		this.findCities();
	}
	constructor(
		public dialogRef: MatDialogRef<DialogAddresses>,
		protected cityService:CityService,
		@Inject(MAT_DIALOG_DATA) public data: any) { }
		zipCodeField = new FormControl('', [
			Validators.required,
			Validators.maxLength(10),
			Validators.minLength(3),
			Validators.pattern(CODE_REGEX)]);

			cityField = new FormControl( [
				Validators.required
			]);

					line1Field = new FormControl('', [
						Validators.maxLength(30)]);

						line2Field = new FormControl('', [
							Validators.maxLength(30)]);
		addAddress():void{}
		findCities() {
			let countryId = localStorage.getItem("countryId");
			this.cityService.findCities(countryId).subscribe(data => {
				this.listOfCities = data;
				});
		}
		onCityChange(event) {
			this.cityService.findByName(event.value).subscribe(
				data=>{
					this.data.city = data;
					console.log(data)
				}
			);
		}
	
	
	onNoClick(): void {
	 
		this.dialogRef.close();
	}
  
	}
