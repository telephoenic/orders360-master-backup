import { VendorAdminService } from './../../../vendor-admin/service/vendor-admin.service';
import { AuthService } from './../../../auth/service/auth.service';
import {BaseViewComponent} from '../../../common/view/base-view-component';
import { TdDataTableService, TdDataTableSortingOrder, ITdDataTableSortChangeEvent, ITdDataTableColumn } from '@covalent/core';
import { IPageChangeEvent, TdPagingBarComponent, TdDialogService,
  TdDataTableComponent } from '@covalent/core';
import {Component, ViewChild, OnInit, Inject} from '@angular/core';
import {VendorService} from '../../Services/vendor.service';
import {Vendor} from '../../vendor.model';
import {FormControl, Validators} from '@angular/forms';
import {
  MatTable, MatHeaderCellDef, MatCellDef,
  MatColumnDef, MatPaginator, MatSort,
  MatInput, MatCheckbox,
  MatButton,
  MatSnackBar
} from "@angular/material";
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';

@Component({
    selector: 'vendor-view',
    templateUrl: 'view-vendor.html'
  })
  export class ViewVendor extends BaseViewComponent implements OnInit{

    private _currentPage: number = 1;
    private _fromRow: number = 1;
    
    private _allVendors: Vendor[] = [];
    private _errors: any = '';
    private _selectedRows: Vendor[] = [];
    private _pageSize: number;
    private _filteredTotal: number;
    private _sortBy: string = '';
    private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
    private _searchTerm: string ='';
    
    @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
    @ViewChild('dataTable') dataTable : TdDataTableComponent;
  
  
    columnsConfig: ITdDataTableColumn[] = [
      { name: 'code', label: 'Code', filter: true,tooltip:"Code",width:130},
      { name: 'name',  label: 'Name', sortable: true,tooltip:"Name"},
      { name: 'currency.code',  label: 'Currency', sortable: true,tooltip:"Currency",width:90},
      { name: 'subscription.name', label: 'Subscription', filter: true ,tooltip:"Subscription",width:180},
      { name: "inactive", label: "Active/Inactive", tooltip : "Active or Inactive", width: 100},
      { name: 'id', label: 'Edit' , tooltip:"Edit",sortable:false,width:100 },
    ];
  
    constructor(protected vendorService: VendorService,
                protected router:Router,
                protected dialogService : TdDialogService,
                protected snackBar:MatSnackBar  , 
                protected permissionsService:NgxPermissionsService,
                protected authService: AuthService,
                protected vendorAdminService: VendorAdminService) { 

      super(vendorService, router, dialogService, snackBar, authService);
    }

    ngOnInit() {
      super.ngOnInit();
  
      this.permissionsService.hasPermission("ROLE_VENDOR_EDIT").then((result:boolean)=> {
        if(!result) {
          for(let i =0; i < this.dataTable.columns.length; i++) {
            if(this.dataTable.columns[i].name == 'id') {
              this.dataTable.columns.splice(i, 1);
            }
          }
        }
      });
  
  
      this.permissionsService.hasPermission("ROLE_VENDOR_ACTIVATE_DEACTIVATE_PER_ROW").then((result:boolean)=> {
        if(!result) {
          for(let i =0; i < this.dataTable.columns.length; i++) {
            if(this.dataTable.columns[i].name == 'inactive') {
              this.dataTable.columns.splice(i, 1);
            }
          }
        }
      });
    }
  

    
    filterByVendor(	pageNumber:number, 
      pageSize:number, 
      searchTerm:string,
      sortBy:string,
      sortOrder:string,
      vendorId:number) { 
        
        // how ??? 
        // return one vendor --- based on logged user 
      }

    
    // onEdit(posId:number, row:Vendor){
    //   // console.log(event);
    //   // //routerLink="../form" [queryParams]="{edit: 1}"
    //   // this.router.navigateByUrl("../form");
    //   this.vendorService.vendor = row;
    // }

    getPagingBar(): TdPagingBarComponent {
      return this.pagingBar;
    }
    getDataTable(): TdDataTableComponent {
      return this.dataTable;
    }
  
    public get currentPage(): number  {
      return this._currentPage;
    }
  
    public set currentPage(value: number ) {
      this._currentPage = value;
    }
  
    public get fromRow(): number  {
      return this._fromRow;
    }
  
    public set fromRow(value: number ) {
      this._fromRow = value;
    }
  
    public get allModels(): Vendor[]  {
      return this._allVendors;
    }
  
    public set allModels(value: Vendor[] ) {
      this._allVendors = value;
    }
  
    public get errors(): any  {
      return this._errors;
    }
  
    public set errors(value: any ) {
      this._errors = value;
    }
  
    public get selectedRows(): Vendor[]  {
      return this._selectedRows;
    }
  
    public set selectedRows(value: Vendor[] ) {
      this._selectedRows = value;
    }
  
    public get pageSize(): number {
      return this._pageSize;
    }
  
    public set pageSize(value: number) {
      this._pageSize = value;
    }
  
    public get filteredTotal(): number {
      return this._filteredTotal;
    }
  
    public set filteredTotal(value: number) {
      this._filteredTotal = value;
    }
  
    public get sortBy(): string  {
      return this._sortBy;
    }
  
    public set sortBy(value: string ) {
      this._sortBy = value;
    }
  
    public get sortOrder(): TdDataTableSortingOrder  {
      return this._sortOrder;
    }
  
    public set sortOrder(value: TdDataTableSortingOrder ) {
      this._sortOrder = value;
    }
  
    public get searchTerm(): string  {
      return this._searchTerm;
    }
  
    public set searchTerm(value: string ) {
      this._searchTerm = value;
    }

    onDeleteFailed(errorResponse:HttpErrorResponse) {
      this.snackBar.open(errorResponse.error["message"], "More", {duration: 10000})
                    .onAction()
                    .subscribe(() => this.showDetails(errorResponse));
    }

    showDetails(errorResponse:HttpErrorResponse) {
  
    }

  }