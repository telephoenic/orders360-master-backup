import { VendorAddress } from '../common/vendor-address.model';
import { PosSubscription } from '../subscription/pos-subscription.model';
import { Subscription } from '../subscription/subscription.model';
import { Currency } from '../lookups/currency.model';
import { Country } from '../location/country/country.model';
import { Industry } from '../common/industry/industry.model';
import { PromotionType } from '../promotion/model/promotion-type';


export class Vendor {

    public id: number;
    public name: string;
    public code: string;
    vat: number;
    public logoImageName: string;
    inactive: boolean;
    subscription: Subscription;
    posSubscription: PosSubscription;
    public listOfVendorAddressList: VendorAddress[] = [];
    currency: Currency;
    country: Country;
    industry:Industry;
    trn:number;
    promotionType: PromotionType;
    public purchasesSetupList: any[] =[];
    hasPromotionOther:boolean = false;

    constructor(id?: number, name?: string, vat?: number,trn?:number) {
        this.subscription = new Subscription();
        this.posSubscription = new PosSubscription();
        this.currency = new Currency();
        this.country = new Country();
        this.id = id;
        this.name = name;
        this.vat = vat;
        this.industry = new Industry();
        this.trn = trn;
        this.promotionType=new PromotionType();
        this.purchasesSetupList = [];
    }

}