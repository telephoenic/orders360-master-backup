import { PromotionService } from './../../promotion/services/promotion-service.service';
import { PromotionType } from './../../promotion/model/promotion-type';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgForm, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { VendorService } from '../Services/vendor.service';
import { VendorAddressService } from '../Services/vendor-address.service';
import { Vendor } from '../vendor.model';
import { ViewAddress } from '../View/ViewAddress/view-address';
import { MatSnackBar, MatCheckbox } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { TdDialogService } from '@covalent/core';
import { NgxGalleryOptions } from 'ngx-gallery';
import { NgxGalleryImage } from 'ngx-gallery';
import { NgxGalleryComponent } from 'ngx-gallery';
import { NgxGalleryAnimation } from 'ngx-gallery';
import { HttpErrorResponse } from '@angular/common/http';
import { SubscriptionType } from '../../subscription/subscription-type.model';
import { SubscriptionTypeService } from '../../subscription/service/subscription-type.service';
import { environment } from '../../../environments/environment';
import { SubscriptionMinToMaxDialog } from '../../subscription/form/subscription-form.component';
import { SubscriptionService } from '../../subscription/service/subscription.service';
import { Subscription } from '../../subscription/subscription.model';
import { PosSubscription } from '../../subscription/pos-subscription.model';
import { PosSubscriptionService } from '../../subscription/service/pos-subscription.service';
import { Currency } from '../../lookups/currency.model';
import { CurrencyService } from '../../lookups/service/currency.service';
import { Country } from '../../location/country/country.model';
import { CountryService } from '../../location/country/service/country.service';
import { Industry } from '../../common/industry/industry.model';
import { IndustryService } from '../../common/industry/service/industry.service';
import { VendorAddress } from '../../common/vendor-address.model';
import { PurchaseSetup } from '../../purchase-setup/purchase-setup.model';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor-form.component.html',
  styleUrls: ['./vendor-form.component.css']
})
export class VendorFormComponent extends BaseFormComponent implements OnInit {

  @ViewChild("vendorForm") vendorForm: NgForm;
  @ViewChild("ngxGallery") ngxGallery: NgxGalleryComponent;
  @ViewChild(ViewAddress) addressViewComponent: ViewAddress;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[] = [];
  attImagesAsFiles: File[] = [];
  imageUrlsArray: string[] = [];
  listOfSubscriptions: Subscription[];
  listOfPosSubscriptionOptions: { id: number, minNumber: number, maxNumber: number }[] = [];
  listOfCurrencies: Currency[];
  listOfCountries: Country[];
  listOfIndustries: Industry[];
  _vendor: Vendor;
  _isEdit: boolean = false;
  _isPosSubscription: boolean = false;
  listPromotionType: PromotionType[] = [];
  countryIsNull: boolean = true;
  private _isCountrySelected: boolean = false;
  listOfPurchaseSetup: PurchaseSetup[];
  isAllSelected: boolean = false;
  isButtonSelectClicked: boolean = false;
  purchahaseSetupTempList: any[] = [];

  constructor(protected vendorService: VendorService,
    protected vendorAddressService: VendorAddressService,
    protected subscriptionService: SubscriptionService,
    protected posSubscriptionService: PosSubscriptionService,
    protected currencyService: CurrencyService,
    protected countryService: CountryService,
    protected industryService: IndustryService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected snackBar: MatSnackBar,
    protected dialogService: TdDialogService,
    protected promotionService: PromotionService) {

    super(vendorService,
      route,
      router,
      snackBar);

  }

  ngOnInit() {
    if (this.route.snapshot.queryParams["edit"] == '1') {
      this._isEdit = true;
      this._vendor = this.vendorService.vendor;
      localStorage.setItem("countryId", this._vendor.country.id.toString())
      this.countryIsNull = false;
      if (this.isPosSubscription(this._vendor.subscription.type.code)) {
        this._isPosSubscription = true;
        this.loadSubscriptionForPosOptions();
      }

      if (this._vendor.country != null) {
        this.isCountrySelected = true;
      }
      this._vendor.listOfVendorAddressList = this.vendorAddressService.vendorAddress;
    } else {
      this._vendor = new Vendor();
      this.vendorAddressService.vendorAddress = [];
      localStorage.removeItem("countryId");

    }
    this.loadSubscriptionOptions();
    this.loadCurrencies();
    this.fillImages();
    this.initGalleryOptions();
    this.initGalleryImages();
    this.loadCountries();
    this.loadIndustries();
    this.loadPromotionType();
    this.loadAllPurchaseSetup();
  }

  loadSubscriptionOptions() {
    this.subscriptionService.findByExample(this.getLeafSubscriptionInstance()).subscribe(
      data => this.listOfSubscriptions = data["content"]
    );
  }

  loadPromotionType() {
    this.vendorService.getAllPromotionType().subscribe(data => {
      this.listPromotionType = data;
    });
  }

  selectAllPurchaseSetup() {
    this.isButtonSelectClicked = true;

    if (!this.isAllSelected) {
      this.checkAllPurchases(true);
    } else {
      this.checkAllPurchases(false);
      this._vendor.purchasesSetupList = [];
    }
  }

  checkAllPurchases(isChecked) {
    this._vendor.purchasesSetupList = this.listOfPurchaseSetup;
    this.listOfPurchaseSetup.forEach(purchaseSetup => {
      purchaseSetup.checked = isChecked;
    });
    this.isAllSelected = isChecked;
  }

  getLeafSubscriptionInstance() {
    let subscription = new Subscription();
    subscription.inactive = false;
    return subscription;
  }

  loadSubscriptionForPosOptions() {
    this.posSubscriptionService.filterNotPageable("", "", "", "/min_to_max_by_type/" + this._vendor.subscription.id).subscribe(
      (data: { id: number, minNumber: number, maxNumber: number }[]) => {
        this.listOfPosSubscriptionOptions = data;
      });
  }

  loadCurrencies() {
    this.currencyService.filter(0, environment.maxListFieldOptions, "", "", "").subscribe(
      data => this.listOfCurrencies = data["content"]
    );
  }

  loadCountries() {
    this.countryService.filter(0, environment.maxListFieldOptions, "", "", "").subscribe(data => {
      this.listOfCountries = data['content'];
    });
  }

  loadIndustries() {
    this.industryService.filter(0, environment.maxListFieldOptions, "", "", "").subscribe(
      data => {
        this.listOfIndustries = data['content'];
      }
    );
  }

  onSubscriptionChanged() {
    let subscription: Subscription;
    for (let i = 0; i < this.listOfSubscriptions.length; i++) {
      subscription = this.listOfSubscriptions[i];
      if (subscription.id == this._vendor.subscription.id) {
        break;
      }
    }
    this.onCompleteSubscriptionLoaded(subscription.type.code);
  }

  onChange(event: any): void {
    event.srcElement.value = "";
  }


  onCountryChange() {
    debugger;
    this.countryIsNull = false;
    let country: Country;
    for (let i = 0; i < this.listOfCountries.length; i++) {
      country = this.listOfCountries[i];
      if (country.id == this._vendor.country.id) {
        break;
      }
    }
    console.log(country)
    localStorage.setItem("countryId", country.id.toString());
    this.loadCountries();
  }

  onCountrySelection(country: Country) {
    this._vendor.country = country;
  }


  onCompleteSubscriptionLoaded(subscriptionTypeCode: string) {
    if (this.isPosSubscription(subscriptionTypeCode)) {
      this._isPosSubscription = true;
      this._vendor.posSubscription = new PosSubscription();
      this.loadSubscriptionForPosOptions();
    } else {
      this._isPosSubscription = false;
      this._vendor.posSubscription = null;
      this.listOfPosSubscriptionOptions = [];
    }
  }

  isPosSubscription(subscriptionCode: string) {//TODO define as enum
    return subscriptionCode == 'PS' ? true : false;
  }

  fillImages() {
    if (this._isEdit) {
      let imageNamesArr = this._vendor.logoImageName.split(";");

      imageNamesArr.forEach((imageName) => {
        let imgURL = this.vendorService.getAttachmentUrl(imageName);
        this.imageUrlsArray.push(imgURL);

        this.vendorService.getAttachment(imgURL).subscribe((blob: Blob) => {
          let fileReader = new FileReader();
          fileReader.onload = () => {
            this.attImagesAsFiles.push(new File([fileReader.result], imageName));
          };
          let img = new Image();
          img.src = imgURL;
          if (blob) {
            fileReader.readAsArrayBuffer(blob);
          }
        });
      });
    }
  }

  initGalleryImages() {
    for (let i = 0; i < this.imageUrlsArray.length; i++) {

      this.galleryImages.push({
        small: this.imageUrlsArray[i],
        medium: this.imageUrlsArray[i],
        big: this.imageUrlsArray[i]
      });
    }
  }

  initGalleryOptions() {
    this.galleryOptions = [
      {
        previewInfinityMove: true,
        imageArrowsAutoHide: true,
        imageInfinityMove: true,
        previewCloseOnEsc: true,
        previewKeyboardNavigation: true,
        previewZoom: true,
        width: '400px',
        height: '300px',
        thumbnails: false,
        imageAnimation: NgxGalleryAnimation.Slide,
        imageSize: 'contain'
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '400px',
        imagePercent: 80
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];
  }

  onSaveAndNew() {
    this.addressViewComponent.onSave();
    this._vendor.listOfVendorAddressList = this.vendorAddressService.vendorAddress;
    super.onSaveAndNew();
  }
  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  onSaveWithAttachments() {
    this.addressViewComponent.onSave();
    this._vendor.listOfVendorAddressList = this.vendorAddressService.vendorAddress;
    super.onSaveWithAttachments(this.attImagesAsFiles);
  }

  onSaveAndNewWithAttachments() {
    this.addressViewComponent.onSave();
    this._vendor.listOfVendorAddressList = this.vendorAddressService.vendorAddress;
    super.onSaveAndNewWithAttachments(this.attImagesAsFiles);
  }

  get model(): Vendor {
    return this._vendor;
  }

  set model(vendor: Vendor) {
    this._vendor = vendor;
  }

  onBeforeSaveAndNewWithAttachments(vendor: Vendor, files: File | FileList) {
    this.showSpinnerOnSaveAndNew = true;
    this.validateSelectedImages();
    this.validateSelectedAddresses();
    this.validateSelectedPurchaseSetup();
  }

  onBeforeEditWithAttachments(vendor: Vendor, files: File | FileList) {
    this.showSpinner = true;
    this.validateSelectedImages();
    this.validateSelectedAddresses();
    this.validateSelectedPurchaseSetup();
  }

  onBeforeSaveWithAttachments(vendor: Vendor, files: File | FileList) {
    this.showSpinner = true;
    this.validateSelectedImages();
    this.validateSelectedAddresses();
    this.validateSelectedPurchaseSetup();
  }

  onBeforeSave(vendor: Vendor) {
    this.showSpinner = true;
    this.validateSelectedImages();
    this.validateSelectedAddresses();
  }

  onBeforeSaveAndNew() {
    this.showSpinnerOnSaveAndNew = true;
    this.validateSelectedImages();
    this.validateSelectedAddresses();
  }

  onBeforeEdit() {
    this.showSpinner = true;
    this.validateSelectedImages();
    this.validateSelectedAddresses();
  }

  validateSelectedImages() {
    if (this.attImagesAsFiles == null || this.attImagesAsFiles.length == 0) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("At least one image required for the vendor.", "Close", { duration: 3000 })
      throw new Error("At least one image required for the vendor");
    }
  }

  validateSelectedPurchaseSetup() {
    console.log(this._vendor.purchasesSetupList);
    
    if (this._vendor.purchasesSetupList.length < 1) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("At least one Purchase Modules required for the vendor.", "close", { duration: 3000 });
      throw new Error("At least one Purchase Modules required for the vendor.");
    }
  }


  validatePromotionType() {
    this.promotionService.countPromotionByVendor(this._vendor.id).subscribe(data => {
      let count = data;
      if (count > 0) {
        this.showSpinnerOnSaveAndNew = false;
        this.showSpinner = false;
        this.snackBar.open("Could not complete process,because there are active promotions.", "Close", { duration: 3000 })
        throw new Error("Could not complete process,because there are active promotions");
      }
    })
  }

  validateSelectedAddresses() {
    if (this._vendor.listOfVendorAddressList == null || this._vendor.listOfVendorAddressList.length == 0) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("At least one address required for the vendor.", "Close", { duration: 3000 })
      throw new Error("At least one address required for the vendor");
    }
  }

  onSelectImages(selectedImage: File | FileList) {
    this.resetAttachmentsFields();
    if (selectedImage instanceof File) {
      this.attImagesAsFiles.push(selectedImage);

      let fileReader = new FileReader();
      fileReader.onload = () => {
        let image = new Image();
        image.src = fileReader.result;

        this.ngxGallery.images.push({
          small: image.src,
          medium: image.src,
          big: image.src
        });
      }
      fileReader.readAsDataURL(selectedImage);
    } else {
      for (let i: number = 0; i < selectedImage.length; i++) {
        this.attImagesAsFiles.push(selectedImage[i]);

        let fileReader = new FileReader();
        fileReader.onload = () => {
          let image = new Image();
          image.src = fileReader.result;

          this.ngxGallery.images.push({
            small: image.src,
            medium: image.src,
            big: image.src
          });
        }
        fileReader.readAsDataURL(selectedImage[i]);
      }
    }
  }

  public loadAllPurchaseSetup() {
    this.vendorService.getAllgetAllPurchaseSetup().subscribe(
      (data: PurchaseSetup[]) => {
        this.listOfPurchaseSetup = data;
      },
      (error) => { console.log(error) },
      () => {
        if (this.isEdit) {
          this.prepareCheckPurchases();
        }
      });
  }


  prepareCheckPurchases() {
    this._vendor.purchasesSetupList = [];
    let purchaseSetupsIds: any[] = [];

    this.vendorService.getAllPurchaseSetUpIds(this._vendor.id)
      .subscribe((data : any[]) => {
        purchaseSetupsIds = data;
        this.isAllSelected = (data.length == this.listOfPurchaseSetup.length) ? true : false;

        purchaseSetupsIds.forEach(id => {
          this.listOfPurchaseSetup.forEach(purchase => {
            if (id === purchase.id) {
              purchase.checked = true;
            }
          });
        });

        data.forEach(id => {
          this.listOfPurchaseSetup.forEach(purchaseSetup =>{
            if(purchaseSetup.id == id){
              this._vendor.purchasesSetupList.push(purchaseSetup)
            }
          })
        })
      });
  }

  onPurchaseChange(checkbox: MatCheckbox, purchase) {
    if (checkbox.checked && !this.isAllSelected) {
      purchase.checked = true;
      this.listOfPurchaseSetup[this.listOfPurchaseSetup.indexOf(purchase)].checked = true;
      this._vendor.purchasesSetupList.push(purchase);

    } else if (!checkbox.checked) {
      this._vendor.purchasesSetupList = this._vendor.purchasesSetupList.filter(obj => obj.id != purchase.id)
    }

    this.isAllSelected = (this._vendor.purchasesSetupList.length < this.listOfPurchaseSetup.length) ? false : true;
  }

  onOtherPromotionCahnge(checkbox: MatCheckbox) {
    if(checkbox.checked){
      this._vendor.hasPromotionOther = true;
    }else{
      this._vendor.hasPromotionOther = false;
    }
  }

  onSaveAndNewWithAttachmentsCompleted() {
    this.resetAttachmentsFields();
    this.addressViewComponent._allVendorAddresses = [];
    this.listOfPurchaseSetup.forEach(purchase => {
      purchase.checked = false;
    });
    this.countryIsNull = true;
    this.isAllSelected = false;
    localStorage.removeItem("countryId")
    this.loadCountries();
  }

  onSaveWithAttachmentsCompleted() {
    this.resetAttachmentsFields();
    this.addressViewComponent._allVendorAddresses = [];
  }

  resetAttachmentsFields() {
    this.attImagesAsFiles = [];
    this.ngxGallery.images = [];
  }

  onDeleteImage() {
    this.dialogService.openConfirm({
      message: "Are you sure you want to delete the selected image?",
      title: "Confirmation",
      cancelButton: "Disagree",
      acceptButton: "Agree"
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.attImagesAsFiles.splice(this.ngxGallery.selectedIndex, 1);
        this.ngxGallery.images.splice(this.ngxGallery.selectedIndex, 1);
      }
    });
  }

  initModel(): Vendor {
    return new Vendor();
  }

  ngFormInstance(): NgForm {
    return this.vendorForm;
  }

  onSaveWithAttachmentsError(errorResponse: HttpErrorResponse) {
    this.snackBar.open(errorResponse.error.message, "Close", { duration: 5000 });
  }

  onSaveAndNewWithAttachmentsError(errorResponse: HttpErrorResponse) {
    this.snackBar.open(errorResponse.error.message, "Close", { duration: 5000 });
  }

  onEditWithAttachmentsError(errorResponse: HttpErrorResponse) {
    this.snackBar.open(errorResponse.error.message, "Close", { duration: 5000 });
  }

  public get isCountrySelected() {
    return this._isCountrySelected;
  }

  public set isCountrySelected(_isCountrySelected: boolean) {
    this._isCountrySelected = _isCountrySelected;
  }

}


