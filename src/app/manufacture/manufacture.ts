import { Store } from './../store/store';
import { Vendor } from '../vendor/vendor.model';
import { BaseModel } from '../common/model/base-model';

export class Manufacture extends BaseModel{
    id: any;
    inactive: boolean;
    name: String;
    vendor: Vendor;
    
}