import { Vendor } from './../../vendor/vendor.model';
import { BaseModel } from "../../common/model/base-model";

export class PromotionAttribute extends BaseModel {
    id: any;
    inactive: boolean;

    attribute: string;
    value: number;
    vendor: Vendor;

}
