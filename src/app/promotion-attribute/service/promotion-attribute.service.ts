import { AuthService } from './../../auth/service/auth.service';
import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { HttpClient } from '@angular/common/http';
import { PromotionAttribute } from '../model/promotion-attribute';
import { BaseModel } from '../../common/model/base-model';
import { environment } from '../../../environments/environment';
import { Observable } from '../../../../node_modules/rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class PromotionAttributeService extends BaseService {

  promotionAttribute: PromotionAttribute;

  constructor(protected http: HttpClient,
    protected authService: AuthService) {
    super(http, "promotion-attribute", authService);
  }

  getAllPromotionAttributeByVendor(vendorId: number): Observable<any> {
    return this.http.get(environment.baseURL + "promotion-attribute/" + "find_by_vendor" + "/" + vendorId);
  }

  onDeletePromotionAttribute(ids: number[]) {
    return this.http.post(environment.baseURL + "promotion-attribute/" + "delete_promotion_attribute",
      JSON.stringify(ids),
      { headers: this.getBasicHeaders() });

  }


  getCountPromotionAttribute(attribute: string, vendorId: number): Observable<any> {
    return this.http.get(environment.baseURL + "promotion-attribute/count_promotion_attribute/" + attribute + "/" + vendorId);
  }

  checkAttributeHasPromotion(ids: number[]) {
    return this.http.post(environment.baseURL + "promotion-attribute/count_attribute_has_promotion",
      JSON.stringify(ids),
      { headers: this.getBasicHeaders() });

  }

  get model() {
    return this.promotionAttribute;
  }
  set model(model: BaseModel) {
    this.promotionAttribute = <PromotionAttribute>model;
  }
}
