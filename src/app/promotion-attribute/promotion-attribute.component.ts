import { Vendor } from './../vendor/vendor.model';
import { User } from './../auth/user.model';
import { AuthService } from './../auth/service/auth.service';
import { VendorAdminService } from './../vendor-admin/service/vendor-admin.service';
import { PosService } from './../pos-user/service/pos.service';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { BaseFormComponent } from '../common/form/base-form-component';
import { PromotionAttribute } from './model/promotion-attribute';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TdDialogService, ITdDataTableColumn, TdDataTableComponent } from '@covalent/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PromotionAttributeService } from './service/promotion-attribute.service';

@Component({
  selector: 'app-promotion-attribute',
  templateUrl: './promotion-attribute.component.html',
  styleUrls: ['./promotion-attribute.component.css']
})
export class PromotionAttributeComponent extends BaseFormComponent implements OnInit {


  columnsConfig: ITdDataTableColumn[] = [

    { name: 'attribute', label: 'Attribute'},
    { name: 'value', label: 'QTY', width: 400 },
    { name: 'id', label: 'Edit',width:100},
  ];

  @ViewChild("promotionAttributeForm") promotionAttributeForm: NgForm;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;

  private _isEdit: boolean = false;
  private _promotionAttribute: PromotionAttribute = new PromotionAttribute();
  private _promotionAttributeList: PromotionAttribute[] = [];
  private _user: User;
  private _selectedRows: PromotionAttribute[] = [];

  constructor(
    protected promotionAttributeService: PromotionAttributeService,
    protected posService: PosService,
    protected vendorAdminService: VendorAdminService,
    protected authService: AuthService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
  ) {

    super(promotionAttributeService,
      route,
      router,
      snackBar);
  }

  ngOnInit() {
    this._user = this.authService.getCurrentLoggedInUser();
    this.getPromotionAttributeList();
  }


  getPromotionAttributeList() {

    this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(
      vendorId => {
        this.promotionAttributeService.getAllPromotionAttributeByVendor(vendorId).subscribe(data => {
          this.promotionAttributeList=data;
        })

      });


  }

  onDelete() {
    if (this._selectedRows.length == 0) {
      this.snackBar.open("No selected rows.", "Close", { duration: 3000 });
    } else if (this._selectedRows.length > 0) {
      this.dialogService.openConfirm({
        message: "Are you sure you want to delete the selected row(s)?",
        title: "confirmation",
        cancelButton: "Disagree",
        acceptButton: "Agree"
      }).afterClosed().subscribe((accept: boolean) => {
        let ids:number[]=[];
        if (accept) {
          this._selectedRows.forEach(obj=>{
            ids.push(obj.id);
          })

          this.promotionAttributeService.checkAttributeHasPromotion(ids).subscribe(data=>{
            if(data > 0 ){
              this.snackBar.open("One of these attribute has promotion.", "Close", { duration: 3000 })
              throw new Error("One of these attribute has promotion.");
            }else{
              this.promotionAttributeService.onDeletePromotionAttribute(ids).subscribe(data=>{
                this.getPromotionAttributeList();
                this.dataTable.refresh();
              });
     
            }
          });
      
        
        }
      }
      );
    } else {
      this.snackBar.open("Please select any item.", "Close", { duration: 3000 });
    }
  }


  addPromotionQuantity() {
    this._isEdit=false;
    this._promotionAttribute = new PromotionAttribute();
    this.openPromotionAttributeDlg(this._promotionAttribute);
  }

  onEditPromotionQuantity(row) {
    this._isEdit=true;
    this.openPromotionAttributeDlg(row);
  }

  openPromotionAttributeDlg(row: PromotionAttribute) {
    let dialogRef = this.dialogService.open(PromotionAttributeQtyAndValueDialog, {
      width: '500px',
      data: {
        attribute: row.attribute,
        value: row.value,
        isEdit:this._isEdit,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(
        vendorId => {
          if (row.attribute != null && row.value != null) {
            let promotionAttribute: PromotionAttribute = new PromotionAttribute();
            promotionAttribute.id=row.id
            promotionAttribute.attribute = result.attribute.toUpperCase();
            promotionAttribute.value = result.value;
            let vendor: Vendor = new Vendor();
            vendor.id = vendorId;
            promotionAttribute.vendor = vendor;
          
            this.baseService.save(promotionAttribute).subscribe(data => {
              this.getPromotionAttributeList();
            })
            
          } else {
            let promotionAttribute: PromotionAttribute = new PromotionAttribute();
            promotionAttribute.attribute = result.attribute.toUpperCase();
            promotionAttribute.value = result.value;
            let vendor: Vendor = new Vendor();
            vendor.id = vendorId;
            promotionAttribute.vendor = vendor;

            this.promotionAttributeService.getCountPromotionAttribute(promotionAttribute.attribute,vendor.id).subscribe(data=>{
              let count =data;
              if(count > 0){
                this.snackBar.open("Attribute already in use.", "Close", { duration: 3000 })
                throw new Error("Attribute already in use.");
              }else{
                this.baseService.save(promotionAttribute).subscribe(data => {
                  this.getPromotionAttributeList();
                })
              }
            });


         
          }});
    });
    this.dataTable.refresh();
  }

  
  compareWith(row: any, model: any): boolean { 
    return row.id === model.id;
}

  public get isEdit(): boolean {
    return this._isEdit;
  }
  public set isEdit(value: boolean) {
    this._isEdit = value;
  }

  public get promotionAttribute(): PromotionAttribute {
    return this._promotionAttribute;
  }
  public set promotionAttribute(value: PromotionAttribute) {
    this._promotionAttribute = value;
  }

  initModel(): PromotionAttribute {
    return new PromotionAttribute();
  }

  get model(): PromotionAttribute {
    return this._promotionAttribute;
  }

  set model(promotionAttribute: PromotionAttribute) {
    this._promotionAttribute = promotionAttribute;
  }

  ngFormInstance(): NgForm {
    return this.promotionAttributeForm;
  }

  public get promotionAttributeList(): PromotionAttribute[] {
    return this._promotionAttributeList;
  }
  public set promotionAttributeList(value: PromotionAttribute[]) {
    this._promotionAttributeList = value;
  }
  public get user(): User {
    return this._user;
  }
  public set user(value: User) {
    this._user = value;
  }

  public get selectedRows(): PromotionAttribute[] {
    return this._selectedRows;
  }
  public set selectedRows(value: PromotionAttribute[]) {
    this._selectedRows = value;
  }

}

@Component({
  selector: 'promotion-attribute-qty-dlg',
  templateUrl: './dialog/promotion-attribute-qty-dlg.html',
})
export class PromotionAttributeQtyAndValueDialog implements OnInit {



  constructor(public dialogRef: MatDialogRef<PromotionAttributeQtyAndValueDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
