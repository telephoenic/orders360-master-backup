import { OnInit, Component } from '@angular/core';
import { PowerBIService } from '../power-bi/service/power-bi.service';
import { PowerBIDTO } from '../power-bi/power-bi-dto.model';
import { VendorAdminService } from '../vendor-admin/service/vendor-admin.service';
import { AuthService } from '../auth/service/auth.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  private pbiContainerElement: HTMLElement;
  /* report id and group id should be fetched dynamically, 
  each report should have a unique report id and group id..
  cause we have only one report which is the "dashboard"
  we use a static data for both */
  //Note: the report id and group id is generated through the power bi reporting tool (microsoft)
 
  private reportId = "3fa769b8-710d-47da-93cb-7814f95b3b41";
  private groupId = "3cd270f7-248a-468d-94a6-7889351ef077";
  private filterName = "Order/City";
  private vendorId: number;
  
  constructor(protected powerBIService: PowerBIService,
              protected authService: AuthService,
              protected vendorAdminService: VendorAdminService) { 
  }

  ngOnInit() {
     this.pbiContainerElement = <HTMLElement>(document.getElementById('pbi-container'));
     let loggedUser = this.authService.getCurrentLoggedInUser();
     let powerBiDto = new PowerBIDTO( this.pbiContainerElement, 
                                      this.reportId, 
                                      this.groupId,
                                      this.filterName);
     
     if (loggedUser.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
      this.vendorAdminService.getVendorByVendorAdminId(loggedUser.id).subscribe(vendorId => {
        powerBiDto.vendorId = vendorId;
        this.powerBIService.embedReport(powerBiDto);
      });
    } else {
      this.powerBIService.embedReport(powerBiDto);
    }   
  }

  getLoggedInVendorId() {
    let loggedUser = this.authService.getCurrentLoggedInUser();

    this.vendorAdminService.getVendorByVendorAdminId(loggedUser.id).subscribe(result => {
      this.vendorId = result;
    });
  }
}