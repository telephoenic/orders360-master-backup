import { BaseModel } from "../common/model/base-model";

export class TicketStatus extends BaseModel{
    
    id:number;
    inactive:boolean;
    code:string;
    status: string;

    constructor ( id:number) {
        super();
        this.id = id;
    }
}