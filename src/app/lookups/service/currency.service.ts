import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { BaseModel } from '../../common/model/base-model';
import { Currency } from '../currency.model';

@Injectable()
export class CurrencyService extends BaseService {
  currency: Currency;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient, protected authService: AuthService) {
    super(http, "currency", authService)
  }

  get model() {
    return this.currency;
  }

  set model(model: BaseModel) {
    this.currency = <Currency>model;
  }
}