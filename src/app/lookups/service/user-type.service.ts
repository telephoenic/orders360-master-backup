import { Injectable, ViewChild } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../common/service/base.service';
import { UserType } from '../user-type.model';
import { AuthService } from '../../auth/service/auth.service';
import { BaseModel } from '../../common/model/base-model';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class UserTypeService extends BaseService {
  userType:UserType;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient, protected authService:AuthService) {
    super(http, "user_type", authService)
  }

  findUserTypeByApplicationId(id: number , userType:UserType): Observable<any> {
    return this.http.get(environment.baseURL + "user_type/type_by_applicationId/" + userType.id + "/"+id);
  }


  get model() {
    return this.userType;
  }
  
  set model(model:BaseModel) {
    this.userType = <UserType>model;
  }
}