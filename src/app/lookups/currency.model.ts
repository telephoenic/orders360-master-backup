import { BaseModel } from "../common/model/base-model";

export class Currency extends BaseModel {
    
    id:number;
    inactive:boolean;
    code:string;
    name:string;

    constructor () {
        super();
    }
}