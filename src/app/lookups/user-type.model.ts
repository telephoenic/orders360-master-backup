import { BaseModel } from "../common/model/base-model";

export class UserType extends BaseModel{
    
    id:number;
    inactive:boolean;
    code:string;
    type: string;

    constructor();
    constructor(id: number);
    constructor (id?:number) {
        super();
        this.id = id;
    }
}