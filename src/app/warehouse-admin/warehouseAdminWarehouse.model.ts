import { Warehouse } from "../warehouse/warehouse-model";
import { WarehouseAdmin } from "./warehouse-admin.model";

export class WarehouseAdminWarehouse{
    id: number;   
    inactive: boolean;
    warehouse:Warehouse;
    warehouseAdmin:WarehouseAdmin;
 
    constructor(id?:number,
                inactive?:boolean,
                warehouse?:Warehouse,
                warehouseAdmin?:WarehouseAdmin){
                    this.id = id;
                    this.inactive = inactive;
                    this.warehouse = warehouse;
                    this.warehouseAdmin = warehouseAdmin;
    }
}