import { BaseFormComponent } from "../../common/form/base-form-component";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar, MatOption } from "@angular/material";
import {
  NgForm,
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { AuthService } from "../../auth/service/auth.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { VendorService } from "../../vendor/Services/vendor.service";
import { Vendor } from "../../vendor/vendor.model";
import { WarehouseAdmin } from "../warehouse-admin.model";
import { Warehouse } from "../../warehouse/warehouse-model";
import { WarehouseService } from "../../warehouse/service/warehouse.service";
import { UserPrivilegeProfileComponent } from "../../user-privilege/views/user-privilege-profile.component";
import { UserPrivilegeProfileAware } from "../../user-management/user-privilege-profile-aware";
import { BaseUserManagementForm } from "../../user-management/base-user-management-form";
import { User } from "../../auth/user.model";
import { environment } from "../../../environments/environment";
import { WarehouseAdminService } from "../service/warehouse-admin.service";
import { WarehouseAdminWarehouse } from "../warehouseAdminWarehouse.model";
import { Industry } from "../../common/industry/industry.model";
import { Currency } from "../../lookups/currency.model";
import { Country } from "../../location/country/country.model";
import { $ } from "protractor";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: "app-warehouse-user",
  templateUrl: "./warehouse-admin-form.component.html",
  styleUrls: ["./warehouse-admin-form.component.css"],
})
export class WarehouseAdminFormComponent extends BaseUserManagementForm {
  user: any;
  _isEdit: boolean = false;
  _vendorId: number;
  warehouseAdmin: WarehouseAdmin;
  listOfWarehouses: Warehouse[] = [];
  warehouseFormControl: FormControl = new FormControl();
  loggedUser: User;
  regex;
  isSaveFormError: boolean = false;
  selectedWarehouse: Warehouse[] = [];
  userForm: FormGroup;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  @ViewChild("warehouseAdminForm") warehouseAdminForm: NgForm;
  @ViewChild("allWarehouseSelected") private allWarehouseSelected: MatOption;

  @ViewChild("notSelectedPrivilegeProfiles")
  notSelectedPrivilegeProfiles: UserPrivilegeProfileComponent;

  @ViewChild("selectedPrivilegeProfiles")
  selectedPrivilegeProfiles: UserPrivilegeProfileComponent;

  constructor(
    protected warehouseAdminService: WarehouseAdminService,
    protected authService: AuthService,
    protected vendorAdminService: VendorAdminService,
    protected warehouseService: WarehouseService,
    protected vendorService: VendorService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {
    super(warehouseAdminService, route, router, snackBar);
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      skills: [[], Validators.required],
    });
    this.dropdownSettings = {
      singleSelection: false,
      text: "warehouse",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      enableSearchFilter: true,
      classes: "myclass custom-class-example",
      labelKey: "name",
      maxHeight:100,
      badgeShowLimit: 2
    };

    this.selectedSecreen = environment.ENUM_CODE_USER_TYPE.WarehouseAdmin;
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    this.getVendorIdByLoggedVenodrAdmin();
    if (this.route.snapshot.queryParams["edit"] == "1") {
      this._isEdit = true;
      this.warehouseAdmin = this.warehouseAdminService.warehouseAdmin;
      this.warehouseService
        .getSelectedWarehouses(this.warehouseAdmin.id)
        .subscribe((data) => {
          this.warehouseAdmin.warehouseAdminWarehouse = data;
          this.selectedItems = data;
          this.warehouseFormControl.setValue(
            this.warehouseAdmin.warehouseAdminWarehouse
          );
        });
    } else {
      this.warehouseAdmin = new WarehouseAdmin();
      this.warehouseAdmin.name = "";
      this.warehouseFormControl = new FormControl();
    }
    this.warehouseFormControl.setValidators(Validators.required);
  }

  getVendorIdByLoggedVenodrAdmin() {
    this.user = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService
      .getVendorByVendorAdminId(this.user.id)
      .subscribe((vendorId: number) => {
        this._vendorId = vendorId;
        this.vendorService.findVendorById(this._vendorId).subscribe(
          (data: Vendor) => {
            this.warehouseAdmin.vendor = data;
            this.regex = new RegExp(
              "^" +
                "(" +
                this.warehouseAdmin.vendor.country.mobileCode +
                ")" +
                "[0-9]{8,8}$"
            );
            if (!this.isEdit && !this.isSaveFormError) {
              this.warehouseAdmin.mobileNumber = "";
            }
          },
          (error) => {},
          () => {
            this.warehouseService
              .getAllWarehousesForWarehouseAdmin(vendorId)
              .subscribe((data) => {
                this.listOfWarehouses = data;
                this.dropdownList = this.listOfWarehouses;
              });
          }
        );
      });
  }

  onItemSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  public options = function (option, value): boolean {
    if (value != null) {
      return option.id === value.id;
    }
  };

  selectWarehousePerOne(all) {
    if (this.allWarehouseSelected.selected) {
      this.allWarehouseSelected.deselect();
      return false;
    }

    if (
      this.warehouseFormControl.value.length == this.listOfWarehouses.length
    ) {
      this.allWarehouseSelected.select();
    }
  }

  warehouseUserNameChange(event) {
    this.warehouseAdmin.name = event.target.value.trim();
  }

  getSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.selectedPrivilegeProfiles;
  }
  getNotSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.notSelectedPrivilegeProfiles;
  }
  onBeforeEdit(warehouseAdmin: WarehouseAdmin) {
    super.onBeforeEdit(warehouseAdmin);
    this.fillSelectedPrivilegeProfiles(warehouseAdmin);
    this.setSelectedWarehouses();
  }
  setSelectedWarehouses() {
    if (this.isEdit) {
      this.warehouseAdmin.warehouseAdminWarehouse = [];
    }
    this.selectedItems.forEach((warehouse) => {
      let warehouseAdminWuthWarehouse: WarehouseAdminWarehouse = new WarehouseAdminWarehouse();
      warehouseAdminWuthWarehouse.warehouse = warehouse;
      this.warehouseAdmin.warehouseAdminWarehouse.push(
        warehouseAdminWuthWarehouse
      );
    });
    let index = 0;
    this.warehouseAdmin.warehouseAdminWarehouse = this.warehouseAdmin.warehouseAdminWarehouse.splice(
      index
    );
    this.warehouseAdmin.vendor.id = this._vendorId;
    this.warehouseAdmin.vendor.industry = new Industry();
    this.warehouseAdmin.vendor.currency = new Currency();
    this.warehouseAdmin.vendor.country = new Country();
    this.warehouseAdmin.vendor.purchasesSetupList = [];
  }

  onBeforeSave(warehouseAdmin: WarehouseAdmin) {
    super.onBeforeSave(warehouseAdmin);
    this.setSelectedWarehouses();
    this.fillSelectedPrivilegeProfiles(warehouseAdmin);
  }

  onBeforeSaveAndNew(warehouseAdmin: WarehouseAdmin) {
    this.setSelectedWarehouses();
    super.onBeforeSaveAndNew(warehouseAdmin);
    this.fillSelectedPrivilegeProfiles(warehouseAdmin);
  }

  resetAfterSaveNew() {
    this.warehouseFormControl = new FormControl();
    this.warehouseAdmin = new WarehouseAdmin();
  }

  onSaveAndNewCompleted() {
    this.getNotSelectedPrivilegeProfiles().filterPrivilegeProfileForUser(
      this.loggedUser.id,
      null,
      this.selectedSecreen
    );
    super.onSaveAndNewCompleted();
    this.getVendorIdByLoggedVenodrAdmin();
    this.resetAfterSaveNew();
    this.selectedPrivilegeProfiles.allModels = [];
    this.selectedPrivilegeProfiles.dataTable.refresh();
  }

  fillSelectedPrivilegeProfiles(userModel: UserPrivilegeProfileAware) {
    userModel.privilegeProfiles = this.getSelectedPrivilegeProfiles().allModels;
    if (userModel.privilegeProfiles.length === 0) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open(
        "Warehouse Admin must be have at least one profile privilege.",
        "Close",
        { duration: 3000 }
      );
      throw new Error(
        "Warehouse Admin must be have at least one profile privilege"
      );
    }
  }
  onEditError(errorResponse: HttpErrorResponse) {
    super.onEditError(errorResponse);
    this.isSaveFormError = true;
    this.getVendorIdByLoggedVenodrAdmin();
  }
  onSaveError(errorResponse: HttpErrorResponse) {
    super.onSaveError(errorResponse);
    this.isSaveFormError = true;
    this.getVendorIdByLoggedVenodrAdmin();
  }

  onSaveAndNewError(errorResponse: HttpErrorResponse) {
    super.onSaveAndNewError(errorResponse);
    this.isSaveFormError = true;
    this.getVendorIdByLoggedVenodrAdmin();
  }

  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): WarehouseAdmin {
    return this.warehouseAdmin;
  }
  set model(warehouseAdmin: WarehouseAdmin) {
    this.warehouseAdmin = warehouseAdmin;
  }
  initModel(): WarehouseAdmin {
    return new WarehouseAdmin();
  }
  ngFormInstance(): NgForm {
    return this.warehouseAdminForm;
  }
}
