import { UserPrivilegeProfileAware } from "../user-management/user-privilege-profile-aware";
import { UserType } from "../lookups/user-type.model";
import { Vendor } from "../vendor/vendor.model";
import { UserPrivilegeProfile } from "../user-privilege/user-privilege-profiles";
import { Warehouse } from "../warehouse/warehouse-model";

export class WarehouseAdmin extends UserPrivilegeProfileAware{
    
    id: number;
    name: string;
    email: string;
    mobileNumber: string;
    type: UserType;
    vendor:Vendor;
    inactive:boolean;
    warehouseAdminWarehouse: any[] = [];
    privilegeProfiles:UserPrivilegeProfile[] = [];
    
    constructor (){
        super();
        this.vendor = new Vendor();
        this.type = new UserType(6);
    }

}