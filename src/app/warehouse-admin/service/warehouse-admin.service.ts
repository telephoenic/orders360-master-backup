import { BaseService } from "../../common/service/base.service";
import { AuthService } from "../../auth/service/auth.service";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { BaseModel } from "../../common/model/base-model";
import { Observable } from "rxjs/Observable";
import { WarehouseAdmin } from "../warehouse-admin.model";

@Injectable()
export class WarehouseAdminService extends BaseService {
  warehouseAdmin: WarehouseAdmin;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient, protected authService: AuthService) {
    super(http, "warehouse_admin", authService);
  }

  getWarehouseAdmins(
    vendorId: number,
    pageNumber: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string
  ): Observable<any> {
    return this.http.get(
      environment.baseURL +
        "warehouse_admin/warehouse-admins/" +
        pageNumber +
        "/" +
        pageSize +
        "/" +
        vendorId,
      {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      }
    );
  }

  saveWarehouseAdmin(warhouesAdmin: WarehouseAdmin) {
    return this.http.post<WarehouseAdmin>(
      environment.baseURL + "warehouse_admin/save-warehouse-Admin",
      WarehouseAdmin,
      {
        params: this.prepareSearchParameters("", "", ""),
        headers: this.getBasicHeaders()
      }
    );
  }

  getVendorIdByUserId(userId: number) {
    return this.http.get(environment.baseURL + "warehouse_admin/get-user-vendor-id/" + userId);
  }

  get model() {
    return this.warehouseAdmin;
  }
  set model(model: BaseModel) {
    this.warehouseAdmin = <WarehouseAdmin>model;
  }
}
