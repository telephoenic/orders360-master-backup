import { Body } from '@angular/http/src/body';
import { FcmModel } from './../fcm-model';
import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { BaseModel } from '../../common/model/base-model';
import { Title } from '@angular/platform-browser';
import { PosUser } from '../../pos-user/pos-user.model';

@Injectable({
  providedIn: 'root'
})
export class FcmService extends BaseService{
 fcm:FcmModel  ;
 //maping controller
  constructor(http: HttpClient, protected authService: AuthService) {
    super(http, "letter", authService);
  }

  onSend(posUsers: PosUser[], body: String , title : String) {
    
    return this.http.post(environment.baseURL + "fcm/send"+"/"+body+"/"+title, 
    JSON.stringify(posUsers), 
    { headers: BaseService.HEADERS });
    }

  get model() {
    return this.fcm;
  }

  set model(model: BaseModel) {
    this.fcm = <FcmModel>model;
  }



}
