import { PosUser } from './../pos-user/pos-user.model';
import {BaseModel} from '../common/model/base-model';
export class FcmModel extends BaseModel  {
    title: string;
    body: string;
    posUsers:PosUser[]=[];
    inactive: boolean;
    id:number ;
    parent:FcmModel;
    hasChilds:boolean;
    childs:FcmModel[];
    
    constructor () {
        super();
}
}
