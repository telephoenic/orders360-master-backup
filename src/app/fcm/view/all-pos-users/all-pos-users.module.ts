// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { AllPosUsersComponent } from './all-pos-users.component';

@NgModule({
    imports: [

    ],
    declarations: [
        AllPosUsersComponent,
    ],
    exports: [
        AllPosUsersComponent,
    ]
})
export class AllPosUsersModule {

}
