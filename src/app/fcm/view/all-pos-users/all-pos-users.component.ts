import { VendorAdminService } from './../../../vendor-admin/service/vendor-admin.service';
import { AuthService } from './../../../auth/service/auth.service';
import { User } from './../../../auth/user.model';
import { PosService } from './../../../pos-user/service/pos.service';
import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { TdDataTableComponent, TdPagingBarComponent, TdDataTableSortingOrder, TdDialogService, ITdDataTableColumn, IPageChangeEvent } from '@covalent/core';
import { Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';

import { BaseViewComponent } from '../../../common/view/base-view-component';
//import { RelatedCategoriesAndProductsDialogComponent } from '../related-categories-and-products-dialog/related-categories-and-products-dialog.component';

import { NgxPermissionsService } from 'ngx-permissions';
import { FcmService } from '../../services/fcm.service';
import { FcmModel } from '../../fcm-model';
import { PosUser } from '../../../pos-user/pos-user.model';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { PosGroupManagement } from '../../../posgroupmanagement/pos-group-management.model';
import { PosGroupManagementService } from '../../../posgroupmanagement/service/pos-group-management.service';
import { Region } from '../../../location/region/Region.model';
import { RegionService } from '../../../location/region/service/region.service';
import { environment } from '../../../../environments/environment';
import { FcmFilter } from '../../fcm-filter';
import { TSMap } from "typescript-map"
import { PosAassignedVendor } from '../../../pos-user/pos-assigned-vendor';
import { VendorService } from '../../../vendor/Services/vendor.service';
import { CityService } from '../../../location/city/service/city.service';
import { City } from '../../../location/city/City.model';

@Component({
    moduleId: module.id,
    selector: 'all-pos-users',
    templateUrl: 'all-pos-users.component.html',
    styleUrls: ['all-pos-users.component.scss']
})
export class AllPosUsersComponent extends BaseViewComponent implements OnInit {
    private msg: string = '';
    private count: number = 0;
    private _currentPage: number = 1;
    private _fromRow: number = 1;
    private _allPosUsers: PosAassignedVendor[] = [];
    private _errors: any = '';
    private _selectedRows: PosAassignedVendor[] = [];
    private _pageSize: number = 50;
    private _filteredTotal: number;
    private _sortBy: string = '';
    private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
    private _searchTerm: string = '';
    private user: User;
    listOfPosGroup: PosGroupManagement[] = [];
    listOfRegion: Region[] = [];
    selectedGroup: number[] = [];
    selectedRegion: number[] = [];
    fcmFilterList: FcmFilter[] = [];
    filterOptionMap = new TSMap<string, number[]>();
    _vendorId: number;
    listOfCitiesIds: number[] = [];
    @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
    @ViewChild('dataTable') dataTable: TdDataTableComponent;

    constructor(protected fcmService: FcmService, protected posService: PosService,
        protected router: Router,
        protected dialogService: TdDialogService,
        protected snackBar: MatSnackBar,
        private matDialog: MatDialog,
        protected permissionsService: NgxPermissionsService,
        protected athuService: AuthService,
        protected vendorService: VendorService,
        protected cityService: CityService,
        protected vendorAdminService: VendorAdminService,
        protected posGroupManagementService: PosGroupManagementService,
        protected regionService: RegionService) {

        super(fcmService,
            router,
            dialogService,
            snackBar);
    }

    columnsConfig: ITdDataTableColumn[] = [
        { name: 'posUser.mobileNumber', label: 'Mobile Number', width: 200 },
        { name: 'posUser.name', label: 'Name', width: 350 },
        { name: 'posGroup.name', label: 'POS Group', width: 130 },
        { name: 'posUser.region.name', label: 'Region', width: 300 },
        { name: 'posUser.registrationId', label: '', width: 100, hidden: true },
        //{ name: 'registrationId', label: 'R ID' }

    ];

    ngOnInit() {
        this.user = this.athuService.getCurrentLoggedInUser();
        //  console.log(this.user); 
        this.filterByVendor();
        this.loadPosGroup();
        this.loadRegionOptions();

    }
    filterByVendor() {
        this.getFilterPos(this.filterOptionMap);
    }

    search(searchTerm:string){
        debugger;
        this.searchTerm = searchTerm;
        this.getFilterPos(this.filterOptionMap);
    }
    getFilterPos(fcmFilter: TSMap<string, number[]>) {
        this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(vendorId => {
            this.posService.getPosAssignWhereVendorId(this.currentPage - 1, this.pageSize, vendorId, this.searchTerm,fcmFilter).subscribe(data => {
                this._allPosUsers = data['content'];
                this.filteredTotal = data['totalElements']
            })
        });
    }

    paging(pagingEvent: IPageChangeEvent) {
        this.fromRow = pagingEvent.fromRow;
        this.currentPage = pagingEvent.page;
        this.pageSize = pagingEvent.pageSize;
        this.getFilterPos(this.filterOptionMap);
    }

    loadRegionOptions() {
        this.vendorAdminService.getVendorByVendorAdminId(this.user.id)
            .subscribe((vendorId: number) => {
                this._vendorId = vendorId;
                this.vendorService.getCountryIdByVendorId(this._vendorId).subscribe(
                    data => {
                        this.cityService.findAllCiityByCountryId(data).subscribe(
                            (data: City[]) => {
                                data.forEach(city => {
                                    this.listOfCitiesIds.push(city.id);
                                });
                                this.regionService.findByCityIdWhereIn(this.listOfCitiesIds).subscribe(
                                    result => {
                                        this.listOfRegion = result;
                                    }
                                )
                            });
                    })
            });
    }

    selectRegionEvent(event) {
        this.filterOptionMap.set("region", event.value)
        if (this.filterOptionMap.get("region").length === 0) {
            this.filterOptionMap.set("region", null);
        }

        this.getFilterPos(this.filterOptionMap);
    }


    selectGroupEvent(event) {
        this.filterOptionMap.set("group", event.value)
        if (this.filterOptionMap.get("group").length === 0) {
            this.filterOptionMap.set("group", null);
        }

        this.getFilterPos(this.filterOptionMap);
    }


    loadPosGroup() {
        this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(
            vendorId => {
                this.posGroupManagementService.getGroupsByVednor(vendorId).subscribe(data => {
                    this.listOfPosGroup = data;
                });

            });

    }


    onSend() {

        let hasRegId :string ="";
        let notHasRegId:string="";

        debugger;
        this._selectedRows.forEach(posUser => {
            this.fcmService.fcm.posUsers.push(posUser.posUser);
        });

        if (this.fcmService.fcm.posUsers.length > 0) {
            debugger;
            this.fcmService.fcm.posUsers.forEach(element => {
                debugger;
                if (element.registrationId == null) {
                    debugger;
                    this.count++;
                    notHasRegId+=element.name+","
                }
                else {
                    debugger;
                    hasRegId += element.name + "," 
                }
            });
            if (this.count == 0) {
                this.snackBar.open("Notification sent successfully to " + hasRegId + "" + this.msg, "Close", { duration: 6000 })
                this.router.navigateByUrl("/home");
            }
            else {
                if(hasRegId.length > 0 && notHasRegId.length>0){
                    this.msg ="Notification sent successfully to "+hasRegId+ "" +notHasRegId + " POSs not login to the system ";
                    this.snackBar.open(this.msg, "Close", { duration: 6000 })
                }else{
                    this.msg = notHasRegId + " POSs not login to the system ";
                    this.snackBar.open(this.msg, "Close", { duration: 6000 })
                }
                
                //throw new Error(" some user did'nt have registration id ");
            }
        }


        this.msg = '';
        this.count = 0;
    }

    deletePosUsersFromList(posUser: PosUser) {
        const index: number = this.fcmService.fcm.posUsers.indexOf(posUser);
        if (index !== -1) {
            this.fcmService.fcm.posUsers.splice(index, 1);
        }
    }

    public get currentPage(): number {
        return this._currentPage;
    }

    public set currentPage(value: number) {
        this._currentPage = value;
    }

    public get fromRow(): number {
        return this._fromRow;
    }

    public set fromRow(value: number) {
        this._fromRow = value;
    }

    public get allModels(): PosAassignedVendor[] {
        return this._allPosUsers;
    }

    public set allModels(value: PosAassignedVendor[]) {
        this._allPosUsers = value;
    }

    public get errors(): any {
        return this._errors;
    }



    public set errors(value: any) {
        this._errors = value;
    }

    public get selectedRows(): PosAassignedVendor[] {
        return this._selectedRows;
    }

    public set selectedRows(value: PosAassignedVendor[]) {
        this._selectedRows = value;
    }

    public get pageSize(): number {
        return this._pageSize;
    }

    public set pageSize(value: number) {
        this._pageSize = value;
    }

    public get filteredTotal(): number {
        return this._filteredTotal;
    }

    public set filteredTotal(value: number) {
        this._filteredTotal = value;
    }

    public get sortBy(): string {
        return this._sortBy;
    }

    public set sortBy(value: string) {
        this._sortBy = value;
    }

    public get sortOrder(): TdDataTableSortingOrder {
        return this._sortOrder;
    }

    public set sortOrder(value: TdDataTableSortingOrder) {
        this._sortOrder = value;
    }

    public get searchTerm(): string {
        return this._searchTerm;
    }

    public set searchTerm(value: string) {
        this._searchTerm = value;
    }

    getPagingBar(): TdPagingBarComponent {
        return this.pagingBar;
    }
    getDataTable(): TdDataTableComponent {
        return this.dataTable;
    }


    //add dialog for send 



}
