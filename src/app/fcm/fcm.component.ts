//import { ViewAllPosComponent } from './../pos-user/view/view-assigned-pos/view-all-pos.component';
import { FcmService } from './services/fcm.service';
import { FcmModel } from './fcm-model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { extend } from 'webdriver-js-extender';
import { BaseFormComponent } from '../common/form/base-form-component';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatSnackBar } from '@angular/material';
import { NgForm } from '@angular/forms';
import { ViewPosFormComponent } from '../pos-user/view/view-pos/view-pos.component';
import { ITdDataTableColumn } from '@covalent/core';
import { AllPosUsersComponent } from './view/all-pos-users/all-pos-users.component';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-fcm',
  templateUrl: './fcm.component.html',
  styleUrls: ['./fcm.component.css']
})
export class FcmComponent extends BaseFormComponent implements OnInit {
@ViewChild("fcm") fcmForm: NgForm;
@ViewChild(ViewPosFormComponent) posViewComponent: ViewPosFormComponent;
@ViewChild(AllPosUsersComponent) allPosUsersComponent: AllPosUsersComponent;
 private _fcm: FcmModel ;
  _isEdit: any;
  showSpinnerOnSendButton : boolean = false;
  

  constructor(protected fcmService: FcmService,
    protected route: ActivatedRoute,
    protected router: Router,
    private datePipe: DatePipe,
    protected snackBar: MatSnackBar) {

    super(fcmService,
      route,
      router,
      snackBar);
    this._fcm =  new FcmModel();
  }

  ngOnInit() {

  }

  onSend() {
    this.showSpinnerOnSendButton = true;
    this.fcmService.fcm = this._fcm;
    this.allPosUsersComponent.onSend();
    this.validateSelectedPosUsers();   
    this.fcmService.onSend(this._fcm.posUsers, this._fcm.body,this._fcm.title).subscribe(result =>{
      this.showSpinnerOnSendButton = false;
    });
    this._fcm.posUsers=[];
  }
  
  validateSelectedPosUsers() {
    this.showSpinnerOnSendButton = false;
    if(  this.fcmService.fcm.posUsers == null ||  this.fcmService.fcm.posUsers.length==0 ) {
      this.snackBar.open("At least one point of sale should be selected", "Close", {duration: 3000})
      throw new Error("At least one point of sale should be selected ");
    }
  }
    onSendError(errorResponse:HttpErrorResponse) {
      this.showSpinnerOnSendButton = false;
      this.snackBar.open(errorResponse.error.message, "Close", { duration: 5000 });

    }

  public get fcm(): FcmModel {
    return this._fcm;
  }
  public set fcm(value: FcmModel) {
    this._fcm = value;
  }



  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): FcmModel {
    return this._fcm;
  }

  set model(fcmModel: FcmModel) {
    this._fcm = fcmModel;
  }

  initModel(): FcmModel {
    return new FcmModel();
  }

  ngFormInstance(): NgForm {
    return this.fcmForm;
  }



}
