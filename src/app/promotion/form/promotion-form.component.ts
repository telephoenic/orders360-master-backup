import { PromotionValueAttribute } from './../model/promotion-value-attribute';
import { PromotionValueQuantity } from './../model/promotion-Value-quantity';
import { PromotionAttributeService } from './../../promotion-attribute/service/promotion-attribute.service';
import { VendorService } from './../../vendor/Services/vendor.service';
import { PromotionAttribute } from './../../promotion-attribute/model/promotion-attribute';
import { PromotionValue } from './../model/promotion-value';
import { Vendor } from './../../vendor/vendor.model';
import { User } from './../../auth/user.model';
import { ProductService } from './../../product/service/product.service';
import { ProductPromotionComponent } from './../views/product-promotion/product-promotion.component';
import { PosService } from './../../pos-user/service/pos.service';
import { Promotion } from './../model/promotion';
import { PromotionService } from './../services/promotion-service.service';
import { AuthService } from './../../auth/service/auth.service';
import { VendorAdminService } from './../../vendor-admin/service/vendor-admin.service';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgForm } from '@angular/forms';
import { ITdDataTableColumn, TdDialogService, TdDataTableComponent } from '@covalent/core';
import { forEach } from '@angular/router/src/utils/collection';
import { PromotionProductPosGroup } from '../model/promotionProductPosGroup';
import { TSMap } from 'typescript-map';

@Component({
  selector: 'app-promotion-form',
  templateUrl: './promotion-form.component.html',
  styleUrls: ['./promotion-form.component.css']
})
export class PromotionFormComponent extends BaseFormComponent implements OnInit {

  @ViewChild("promotionForm") promotionForm: NgForm;
  @ViewChild("qtyToValueDataTable") qtyToValueDataTable: TdDataTableComponent;
  @ViewChild("dataTable") dataTable: TdDataTableComponent;

  @ViewChild(ProductPromotionComponent) productPromotionView: ProductPromotionComponent;


  qtyToValueDataTableColumnsConfig: ITdDataTableColumn[] = [
    { name: 'quantity', label: 'Quantity', width: 187 },
    { name: 'freeItem', label: 'Free Items', width: 130 },
    { name: 'id', label: '', width: 130 }
  ];

  attrToValueDataTableColumnsConfig: ITdDataTableColumn[] = [
    { name: 'promotionAttribute.attribute', label: 'Attribute', width: 135 },
    { name: 'freeItem', label: 'Free Items', width: 100 },
    { name: 'id', label: 'Edit' }
  ];




  private _promotion: Promotion = new Promotion();;
  private _isEdit: boolean = false;
  private _promotoinQuantityList: PromotionValueQuantity[] = [];
  private _promotoinAttributeList: PromotionValueAttribute[] = [];
  private _selectedPromotoinAttributeList: PromotionValueAttribute[] = [];
  private _promotoinQuantity: PromotionValueQuantity = new PromotionValueQuantity();
  private _promotionValue: PromotionValue = new PromotionValue();
  private _selectedPromotoinQuantity: PromotionValueQuantity[] = [];
  private _qtyDeletedIds: number[] = [];
  private user: User = new User();
  private _vendorId: number;
  private _vendor: Vendor;
  private _attribute: PromotionValueAttribute = new PromotionValueAttribute();
  private _attributeList: PromotionAttribute[] = [];
  qtyFlag: boolean = false;
  attFlag: boolean = false;
  selectedAttr: string;
  test: PromotionProductPosGroup[] = [];

  constructor(
    protected promotionService: PromotionService,
    protected posService: PosService,
    protected vendorAdminService: VendorAdminService,
    protected authService: AuthService,
    protected route: ActivatedRoute,
    protected router: Router,
    private datePipe: DatePipe,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    protected productService: ProductService,
    protected vendorService: VendorService,
    protected promotionAttributeService: PromotionAttributeService) {

    super(promotionService,
      route,
      router,
      snackBar);
  }


  ngOnInit() {
    this.user = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(vendorId => {
      this._vendorId = vendorId;
      this.vendorService.findVendorById(vendorId).subscribe(data => {
        this._vendor = data;
        if (this._vendor.promotionType.type == "Attribute") {
          this.qtyFlag = false;
          this.attFlag = true;
          this.promotionAttributeService.getAllPromotionAttributeByVendor(this._vendorId).subscribe(data => {
            this.attributeList = data;
          })
        } else {
          this.qtyFlag = true;
          this.attFlag = false;
        }

        if (this.route.snapshot.queryParams["edit"] == '1') {
          this._isEdit = true;
          this._promotion = this.promotionService.promotion;
          this._promotion.startDate = new Date(this.datePipe.transform(this._promotion.startDate, 'mediumDate'));
          if (this._promotion.endDate != undefined) {
            this._promotion.endDate = new Date(this.datePipe.transform(this._promotion.endDate, 'mediumDate'));
          }
          this.loadQtyToValue();
        } else {
          this._promotion = new Promotion();
        }

      })
    });
  }

  addPromotionQuantity() {
    if (this._vendor.promotionType.type == "Attribute") {
      this.openPromotionAttributeDialog(this._attribute);
    } else {
      this.promotoinQuantity = new PromotionValueQuantity();
      this.openPromotionQuantityDialog(this.promotoinQuantity);
    }
  }


  onEditPromotionQuantity(id, row) {
    if (this._vendor.promotionType.type == "Attribute") {
      this.openPromotionAttributeDialog(row);
    } else {
      this.openPromotionQuantityDialog(row);
    }


  }
  openPromotionQuantityDialog(row: PromotionValueQuantity): void {
    let dialogRef = this.dialogService.open(PromotionQuantityDialog, {
      width: '500px',
      data: {
        quantity: row.quantity,
        freeItem: row.freeItem
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (row.quantity != null && row.freeItem != null) {
        row.quantity = result.quantity;
        row.freeItem = result.freeItem;
      } else {
        let promotionQty: PromotionValueQuantity = new PromotionValueQuantity();
        promotionQty.quantity = result.quantity;
        promotionQty.freeItem = result.freeItem;
        this._promotoinQuantityList.push(promotionQty);
      }
      this.qtyToValueDataTable.refresh();
    });

  }


  checkDiscountHasProductsInAddCase() {
    this.productPromotionView.posGroups.forEach(posGroup => {
      if (this.productPromotionView.cashingAssignProduct.has(posGroup.id)) {
        if (this.productPromotionView.cashingAssignProduct.get(posGroup.id).length > 0) {
          this.productPromotionView.isPromotionHasProduct = true;
          return;
        } else {
          if (this.isEdit)
            this.productPromotionView.isPromotionHasProduct = false;
        }
      }
    });
  }

  openPromotionAttributeDialog(row: PromotionValueAttribute): void {
    this.selectedAttr = row.promotionAttribute.attribute;
    let dialogRef = this.dialogService.open(PromotionAttributeDialog, {
      width: '500px',
      data: {
        attribute: this.selectedAttr,
        attributeList: this.attributeList,
        freeItem: row.freeItem
      }
    });

    dialogRef.afterClosed().subscribe(result => {

      if (row.promotionAttribute.id != null && row.freeItem != null) {
        let promotionAttt: PromotionAttribute = new PromotionAttribute();
        promotionAttt = this.attributeList.find(obj => obj.attribute == result.attribute);
        if (promotionAttt.id != null) {
          row.promotionAttribute = promotionAttt;
        }
        row.freeItem = result.freeItem;
      } else {
        let promotionAtr: PromotionValueAttribute = new PromotionValueAttribute();
        let promotionAttribute: PromotionAttribute = new PromotionAttribute();
        promotionAttribute = this.attributeList.find(obj => obj.attribute == result.attribute);

        promotionAtr.freeItem = result.freeItem;
        promotionAtr.promotionAttribute = promotionAttribute;
        this._promotoinAttributeList.push(promotionAtr);
      }
      this.qtyToValueDataTable.refresh();
    });

  }





  onDelete() {
    if (this.qtyFlag) {
      if (this._promotoinQuantityList.length == 0) {
        this.snackBar.open("No quantity and value added yet.", "Close", { duration: 3000 });
      }

    } if (this.attFlag) {
      if (this._promotoinAttributeList.length == 0) {
        this.snackBar.open("No attribute and value added yet.", "Close", { duration: 3000 });
      }
    } if (this._selectedPromotoinQuantity.length > 0 || this._selectedPromotoinAttributeList.length > 0) {
      this.dialogService.openConfirm({
        message: "Are you sure you want to delete the selected row(s)?",
        title: "confirmation",
        cancelButton: "Disagree",
        acceptButton: "Agree"
      }).afterClosed().subscribe((accept: boolean) => {
        if (accept) {
          if (this.qtyFlag) {
            for (let i = 0; i < this._selectedPromotoinQuantity.length; i++) {
              let elementIndexToDelete =
                this._promotoinQuantityList.indexOf(this._selectedPromotoinQuantity[i]);
              this._promotoinQuantityList.splice(elementIndexToDelete, 1);
            }
            this._selectedPromotoinQuantity.forEach(obj => {
              this.qtyDeletedIds.push(obj.id);
            })

            this._selectedPromotoinQuantity.splice(0, this._selectedPromotoinQuantity.length);
          } else if (this.attFlag) {

            for (let i = 0; i < this._selectedPromotoinAttributeList.length; i++) {
              let elementIndexToDelete =
                this._promotoinAttributeList.indexOf(this._selectedPromotoinAttributeList[i]);
              this._promotoinAttributeList.splice(elementIndexToDelete, 1);
            }
            this._selectedPromotoinAttributeList.forEach(obj => {
              this.qtyDeletedIds.push(obj.id);
            })

            this._selectedPromotoinAttributeList.splice(0, this._selectedPromotoinAttributeList.length);
          }

          this.qtyToValueDataTable.refresh();
        }
      }
      );
    } else {
      this.snackBar.open("Please select any item.", "Close", { duration: 3000 });
    }
  }



  onBeforeSave() {

    if (!(this._promotoinQuantityList.length > 0 || this._promotoinAttributeList.length > 0)) {
      this.snackBar.open("Promotion Quantity and Value are required.", "Close", { duration: 3000 })
      throw new Error("Promotion Quantity and Value are required.");
    }

    this.showSpinner = true;
    this.productPromotionView.onSavePromotionProductList();
    this.checkDiscountHasProductsInAddCase();
    if (!this.productPromotionView.isPromotionHasProduct) {
      this.showSpinner = false;
      this.snackBar.open("promotion must be have at least one product.", "Close", { duration: 3000 })
      throw new Error("promotion must be have at least one product.");
    } else {
      this.validateSelectedDates();
      if (this.promotionService.overritePromotionProduct != null) {
        this.promotionService.deletePromotionProductPosGroup(this.promotionService.overritePromotionProduct).subscribe();

      }

      this._promotion.promotionProductPosGroup = this.promotionService.assignProduct;
      this._promotion.promotionValueQuantity = this._promotoinQuantityList;
      this._promotion.promotionValueAttribute = this._promotoinAttributeList;
      this._promotion.vendor.id = this._vendorId;
      super.onBeforeSave(this._promotion);
    }

  }


  onBeforeEdit() {
    if (!(this._promotoinQuantityList.length > 0 || this._promotoinAttributeList.length > 0)) {
      this.snackBar.open("Promotion Quantity and Value are required.", "Close", { duration: 3000 })
      throw new Error("Promotion Quantity and Value are required.");
    }


    this.showSpinner = true;
    this.validateSelectedDates();
    if (this.qtyDeletedIds.length > 0) {
      this.promotionService.deletePromotionQtyToValue(this.qtyDeletedIds).subscribe(data => {
      })
    }

    this.productPromotionView.onSavePromotionProductList();
    if (this.productPromotionView.productDbCount == 0) {
      this.showSpinner = false;
      this.snackBar.open("Promotion must be have at least one product.", "close", { duration: 3000 });
      throw new Error("Promotion must be have at least one product.")
    }


    if (this.promotionService.overritePromotionProduct != null) {
      this.promotionService.deletePromotionProductPosGroup(this.promotionService.overritePromotionProduct).subscribe(data => {
      })

    }

    if (this.promotionService.deletedListInEdit !== undefined && this.promotionService.deletedListInEdit !== null) {
      let ids: number[] = [];
      this.promotionService.deletedListInEdit.forEach(value => {
        if (value != null) {
          ids.push(value);
        }
      })
      if (ids !== null) {
        this.promotionService.deletePromotionProductPosGroup(ids).subscribe();
      }

    }
    this._promotion.promotionProductPosGroup = this.promotionService.assignProduct;
    this._promotion.promotionValueQuantity = this._promotoinQuantityList;
    this._promotion.promotionValueAttribute = this._promotoinAttributeList;
    let vendor: Vendor = new Vendor();
    vendor.id = this._vendorId;
    this._promotion.vendor = vendor;

    super.onBeforeEdit(this._promotion);
  } 


  onEditCompleted() {
    let promotionWithoutProduct: number[] = [];
    if (this.promotionService.promotionIds.length > 0) {
      this.promotionService.productPromotionCount(this.promotionService.promotionIds).subscribe(data => {
        this.test = data;
        this.promotionService.promotionIds.forEach(obj => {
          let promotionList = this.test.filter(item => item.promotion.id == obj && item.deleted == false)

          if (promotionList.length == 0) {
            promotionWithoutProduct.push(obj)
          } else {
            promotionList.forEach(object => {
              if (object.productCount < 1) {
                promotionWithoutProduct.push(object.promotion.id);
              }
            })
          }

        });
        if (promotionWithoutProduct.length != 0) {
          this.promotionService.deletePromotionById(promotionWithoutProduct).subscribe();
        }

      })
    }


    super.onSaveCompleted();
  }

  onSaveCompleted() {
    let promotionWithoutProduct: number[] = [];
    if (this.promotionService.promotionIds.length > 0) {
      this.promotionService.productPromotionCount(this.promotionService.promotionIds).subscribe(data => {
        this.test = data;
        this.promotionService.promotionIds.forEach(obj => {
          let promotionList = this.test.filter(item => item.promotion.id == obj && item.deleted == false)
          if (promotionList.length == 0) {
            promotionWithoutProduct.push(obj)
          } else {
            promotionList.forEach(object => {
              if (object.productCount < 1) {
                promotionWithoutProduct.push(object.promotion.id);
              }
            })
          }

        });
        if (promotionWithoutProduct.length != 0) {
          this.promotionService.deletePromotionById(promotionWithoutProduct).subscribe();
        }

      })
    }


    super.onSaveCompleted();
  }

  onBeforeSaveAndNew() {

    if (!(this._promotoinQuantityList.length > 0 || this._promotoinAttributeList.length > 0)) {
      this.snackBar.open("Promotion Quantity and Value are required.", "Close", { duration: 3000 })
      throw new Error("Promotion Quantity and Value are required.");
    }

    this.showSpinnerOnSaveAndNew = true;
    this.productPromotionView.onSavePromotionProductList();

    if (this.promotionService.assignProduct.length == 0) {
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("promotion must be have at least one product.", "Close", { duration: 3000 })
      throw new Error("promotion must be have at least one product.");
    } else {
      this.validateSelectedDates();
      if (this.promotionService.overritePromotionProduct != null) {
        this.promotionService.deletePromotionProductPosGroup(this.promotionService.overritePromotionProduct).subscribe();
      }
      this._promotion.promotionProductPosGroup = this.promotionService.assignProduct;
      this._promotion.promotionValueQuantity = this._promotoinQuantityList;
      this._promotion.promotionValueAttribute = this._promotoinAttributeList;
      this._promotion.vendor.id = this._vendorId;
      super.onBeforeSaveAndNew(this._promotion);
    }
  }

  onSaveAndNew() {
    super.onSaveAndNew();
  }

  onSaveAndNewCompleted() {
    this.showSpinnerOnSaveAndNew = false;
    let promotionWithoutProduct: number[] = [];
    if(this.promotionService.promotionIds!=null){
      if (this.promotionService.promotionIds.length > 0) {
        this.promotionService.productPromotionCount(this.promotionService.promotionIds).subscribe(data => {
          this.test = data;
          this.promotionService.promotionIds.forEach(obj => {
            let promotionList = this.test.filter(item => item.promotion.id == obj && item.deleted == false)
  
            if (promotionList.length == 0) {
              promotionWithoutProduct.push(obj)
            } else {
              promotionList.forEach(object => {
                if (object.productCount < 1) {
                  promotionWithoutProduct.push(object.promotion.id);
                }
              })
            }
  
          });
          if (promotionWithoutProduct.length != 0) {
            this.promotionService.deletePromotionById(promotionWithoutProduct).subscribe();
          }
  
        })
  
      }
    }
    
    this.resetAfterSaveNew();
    super.onSaveCompleted();
  }


  loadQtyToValue() {
    if (this.qtyFlag) {
      this.promotionService.findAllQuantityByPromotionId(this.promotion.id).subscribe(data => {
        this._promotoinQuantityList = data;
      });

    } else {
      this.promotionService.findAllAttributeByPromotionId(this.promotion.id).subscribe(data => {
        this._promotoinAttributeList = data;
      });

    }

  }




  resetAfterSaveNew() {
    this._promotion = new Promotion();
    this.productPromotionView.assignProduct = [];
    this.productPromotionView.allProduct = [];
    this._promotoinQuantityList = [];
    this._promotoinAttributeList = [];
    this.productPromotionView.selectedPosGroup = null;
    this.productPromotionView.cashingAssignProduct.clear();
  }

  validateSelectedDates() {
    let tempCurrentDate: Date = new Date();
    tempCurrentDate.setHours(0);
    tempCurrentDate.setMinutes(0);
    tempCurrentDate.setSeconds(0);
    tempCurrentDate.setMilliseconds(0);

    if (this._promotion.endDate != undefined && this._promotion.startDate > this._promotion.endDate) {
      this.showSpinnerOnSaveAndNew = false;
      this.showSpinner = false;
      this.snackBar.open("End date must be after than start date.", "Close", { duration: 3000 })
      throw new Error("End date must be after than start date.");
    }
  }


  public get selectedPromotoinAttributeList(): PromotionValueAttribute[] {
    return this._selectedPromotoinAttributeList;
  }
  public set selectedPromotoinAttributeList(value: PromotionValueAttribute[]) {
    this._selectedPromotoinAttributeList = value;
  }

  public get attributeList(): PromotionAttribute[] {
    return this._attributeList;
  }
  public set attributeList(value: PromotionAttribute[]) {
    this._attributeList = value;
  }


  public get attribute(): PromotionValueAttribute {
    return this._attribute;
  }
  public set attribute(value: PromotionValueAttribute) {
    this._attribute = value;
  }


  public get vendor(): Vendor {
    return this._vendor;
  }
  public set vendor(value: Vendor) {
    this._vendor = value;
  }


  public get promotoinAttributeList(): PromotionValueAttribute[] {
    return this._promotoinAttributeList;
  }
  public set promotoinAttributeList(value: PromotionValueAttribute[]) {
    this._promotoinAttributeList = value;
  }


  public get promotionValue(): PromotionValue {
    return this._promotionValue;
  }
  public set promotionValue(value: PromotionValue) {
    this._promotionValue = value;
  }

  public get selectedPromotoinQuantity(): PromotionValueQuantity[] {
    return this._selectedPromotoinQuantity;
  }
  public set selectedPromotoinQuantity(value: PromotionValueQuantity[]) {
    this._selectedPromotoinQuantity = value;
  }

  public get promotion(): Promotion {
    return this._promotion;
  }
  public set promotion(value: Promotion) {
    this._promotion = value;
  }

  public get vendorId(): number {
    return this._vendorId;
  }
  public set vendorId(value: number) {
    this._vendorId = value;
  }

  public get qtyDeletedIds(): number[] {
    return this._qtyDeletedIds;
  }
  public set qtyDeletedIds(value: number[]) {
    this._qtyDeletedIds = value;
  }


  public get isEdit(): boolean {
    return this._isEdit;
  }
  public set isEdit(value: boolean) {
    this._isEdit = value;
  }

  ngFormInstance(): NgForm {
    return this.promotionForm;
  }

  initModel(): Promotion {
    return new Promotion();
  }

  get model(): Promotion {
    return this._promotion;
  }

  set model(promotion: Promotion) {
    this._promotion = promotion;
  }


  public get promotoinQuantity(): PromotionValueQuantity {
    return this._promotoinQuantity;
  }
  public set promotoinQuantity(value: PromotionValueQuantity) {
    this._promotoinQuantity = value;
  }
}

@Component({
  selector: 'promotion-quantity-dialog',
  templateUrl: '../dialogs/promotion-quantity-dialog.html',
})
export class PromotionQuantityDialog implements OnInit {



  constructor(public dialogRef: MatDialogRef<PromotionQuantityDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
  }

  cancel(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'promotion-quantity-dialog',
  templateUrl: '../dialogs/promotion-attribute-dialog.html',
})
export class PromotionAttributeDialog implements OnInit {

  constructor(public dialogRef: MatDialogRef<PromotionAttributeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
  }

  cancel(): void {
    this.dialogRef.close();
  }
}


export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
