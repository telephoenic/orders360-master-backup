import { PromotionService } from './../../services/promotion-service.service';
import { PosService } from './../../../pos-user/service/pos.service';
import { ProductService } from './../../../product/service/product.service';
import { VendorAdminService } from './../../../vendor-admin/service/vendor-admin.service';
import { AuthService } from './../../../auth/service/auth.service';
import { User } from './../../../auth/user.model';
import { environment } from './../../../../environments/environment';
import { Component, OnInit, ViewChild, Inject, TemplateRef } from '@angular/core';
import { ITdDataTableColumn, TdDataTableComponent, TdDataTableSortingOrder, TdPagingBarComponent, TdDialogService, IPageChangeEvent, ITdDataTableSortChangeEvent, TdSearchInputComponent, TdSearchBoxComponent } from '@covalent/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { SelectItem } from 'primeng/components/common/selectitem';
import { PosGroupManagementService } from '../../../posgroupmanagement/service/pos-group-management.service';
import { PosGroupManagement } from '../../../posgroupmanagement/pos-group-management.model';
import { PromotionProductPosGroup } from '../../model/promotionProductPosGroup';
import { TSMap } from 'typescript-map';
import { DatePipe } from '@angular/common';
import { Overlay } from '@angular/cdk/overlay';



@Component({
  selector: 'app-product-promotion',
  templateUrl: './product-promotion.component.html',
  styleUrls: ['./product-promotion.component.css']
})
export class ProductPromotionComponent extends BaseViewComponent implements OnInit {


  @ViewChild('allProductDt') allProductDt: TdDataTableComponent;
  @ViewChild('selectedProductDt') selectedProductDt: TdDataTableComponent;
  @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;


  allProductColumnConfig: ITdDataTableColumn[] = [
    { name: 'product.name', label: 'Name', width: 260 },
    { name: 'product.price', label: 'Price', width: 90 },
    { name: 'promotion.id', label: 'Promotion', width: 80 },
    { name: 'product.id', label: 'Details', width: 90 }
  ];

  selectedProductColumnConfig: ITdDataTableColumn[] = [
    { name: 'product.name', label: 'Name', width: 215, },
    { name: 'product.price', label: 'Price', width: 160 },
  ];

  @ViewChild('searchBox') searchBox: TdSearchBoxComponent;

  private _user: User;
  private _errors: any = '';
  private _selectedRows: PromotionProductPosGroup[] = [];
  private _assignSelectedRows: PromotionProductPosGroup[] = [];
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _pageSize: number = environment.initialViewRowSize;;
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private _searchTerm: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _allProduct: PromotionProductPosGroup[] = [];
  private _assignProduct: PromotionProductPosGroup[] = [];
  selectedPosGroup: number;
  groups: SelectItem[] = [];
  posGroups: PosGroupManagement[];
  cashingAssignProduct = new TSMap<number, any[]>();
  _isEdit: boolean = false;
  overritePromotionProducts: number[] = [];
  deassignSelectedEdit = new TSMap<number, any[]>();
  dbMap = new TSMap<number, any[]>();
  promotionIds: number[] = [];
  arr: PromotionProductPosGroup[] = [];
  isPromotionHasProduct: boolean = false;
  productDbCount: number = 0;

  constructor(protected authService: AuthService,
    protected vendorAdminService: VendorAdminService,
    protected promotionService: PromotionService,
    protected posService: PosService,
    protected route: ActivatedRoute,
    protected router: Router,
    private datePipe: DatePipe,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    protected posGroupManagementService: PosGroupManagementService,
    protected productService: ProductService,
    protected overlay: Overlay) {
    super(promotionService, router, dialogService, snackBar);

  }



  ngOnInit() {
    if (this.route.snapshot.queryParams["edit"] == 1) {
      this._isEdit = true;
      this.promotionService.findPromotionHasProduct(this.promotionService.promotion.id).subscribe(data => {
        if (data > 0) {
          this.isPromotionHasProduct = true;
          this.productDbCount = data
        }
      })
    }
    this.loadPosGroupsOptions();
  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize = pagingEvent.pageSize;
    debugger;
    this.getAllProductByPosGroup(this.selectedPosGroup, pagingEvent.page - 1, this._pageSize, this.searchTerm, this._sortBy, this._sortOrder);

  }

  sort(sortEvent: ITdDataTableSortChangeEvent): void {
    this.sortBy = sortEvent.name;
    this.sortOrder = sortEvent.order;
    this.getAllProductByPosGroup(this.selectedPosGroup, 0, this._pageSize, "", this._sortBy, this._sortOrder);
  }

  loadPosGroupsOptions() {
    this._user = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService.getVendorByVendorAdminId(this._user.id).subscribe(vendorId => {
      this.posGroupManagementService.getGroupsByVednor(vendorId).subscribe(
        data => {
          this.posGroups = data;
          this.posGroups.forEach((posGroupObj: PosGroupManagement) => {
            this.groups.push({ label: posGroupObj.name, value: posGroupObj.id });
          });
        });

    })

  }

  onSelectButtonPosGroupChange(event) {
    this.getPagingBar().navigateToPage(1);
    this.assignProduct = [];
    this._assignSelectedRows = [];
    this._selectedRows = [];
    this.selectedPosGroup = event.value;
    this.searchBox.value = "";
    this.getAllProductByPosGroup(this.selectedPosGroup, 0, environment.initialViewRowSize, "", this.sortBy, this._sortOrder);
    if (this._isEdit) {
      this._user = this.authService.getCurrentLoggedInUser();
      this.vendorAdminService.getVendorByVendorAdminId(this._user.id).subscribe(vendorId => {
        this.promotionService.findAllAssignProductByPromotionId(this.promotionService.promotion.id, this.selectedPosGroup, vendorId).subscribe(data => {
          this.dbMap.set(this.selectedPosGroup, data);
        })
      })
    }

    if (this.cashingAssignProduct.length > 0) {
      if (this.cashingAssignProduct.has(this.selectedPosGroup)) {
        this.cashingAssignProduct.get(this.selectedPosGroup).forEach(cashingSelected => {
          this.assignProduct.push(cashingSelected);
        });
      }
    }

    if (this._isEdit) {
      this.getAssignProductByPosGroupAndPromotion();
    }
  }

  getAssignProductByPosGroupAndPromotion() {
    this._user = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService.getVendorByVendorAdminId(this._user.id).subscribe(vendorId => {
      this.promotionService.findAllAssignProductByPromotionId(this.promotionService.promotion.id, this.selectedPosGroup, vendorId).subscribe(data => {
        this.assignProduct = data;
        if (this.cashingAssignProduct.length > 0) {
          if (this.cashingAssignProduct.has(this.selectedPosGroup)) {
            let unSelected = this.cashingAssignProduct.get(this.selectedPosGroup).filter(item => this.assignSelectedRows.indexOf(item) < 0);
            this._assignProduct = unSelected;
            this.cashingAssignProduct.set(this.selectedPosGroup, this._assignProduct);
          }
        }
        this.deleteDublicateProduct();
      })
    })

  }

  deleteDublicateProduct() {
    if (this.deassignSelectedEdit.length > 0) {
      if (this.deassignSelectedEdit.has(this.selectedPosGroup)) {
        this.deassignSelectedEdit.get(this.selectedPosGroup).forEach(obj => {
          if (obj !== undefined) {
            let deleteProduct = this._assignProduct.find(item => (obj.product.id === item.product.id) && obj.posGroup.id === item.posGroup.id);
            if (deleteProduct != null) {
              this.deleteFormList(deleteProduct, this._assignProduct);
            }
          }

        });
      }

    }

  }

  getAllProductByPosGroup(posGroup: number, pageNumber: number, pageSize: number, searchTerm: string, sortBy: string, sortOrder: TdDataTableSortingOrder) {
    this._user = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService.getVendorByVendorAdminId(this._user.id).subscribe(vendorId => {
      this.productService.getAllPrmotionProductsByPosGroup(posGroup, vendorId, pageNumber, pageSize, sortBy, sortOrder, searchTerm).subscribe(products => {
        this.allProduct = products["content"];
        this.filteredTotal = products["totalElements"];
      });
    });
  }

  public onAssign() {
    debugger;
    if (this.checkProductHasPromotion(this._selectedRows)) {
      this.openPromotionProductsConfirmDialog();
    } else {
      let count = 0;
      this._selectedRows.forEach(temp => {
        debugger;
        if (this._isEdit) {
          if (Array.isArray(this.deassignSelectedEdit.get(this.selectedPosGroup))) {
            let obj: any;
            obj = this.deassignSelectedEdit.get(this.selectedPosGroup).find(x => x.product.id == temp.product.id);
            if (obj !== null) {
              const cashIndex: number = this.deassignSelectedEdit.get(this.selectedPosGroup).indexOf(obj);
              if (cashIndex !== -1) {
                this.deassignSelectedEdit.get(this.selectedPosGroup).splice(cashIndex, 1);
              }
            }

          }
        }
        if (!this.checkDublicteProductinAssignOrDeassign(temp, this.assignProduct)) {
          this.assignProduct.push(temp);
          this.productDbCount++;
        } else {
          if (this._selectedRows.filter(object => object.promotion.id != null).length == 0) {
            count += this.assignProduct.filter(obj => obj.product.id == temp.product.id).length;
            if (this._selectedRows.length == count) {
              this.snackBar.open("This Product is already added.", "close", { duration: 3000 });
            }
          }else{
            count += this.assignProduct.filter(obj => obj.product.id == temp.product.id).length;
            if (this._selectedRows.length == count) {
              this.snackBar.open("This Product is already added.", "close", { duration: 3000 });
            }
          }
        }
        if (!this._isEdit) {
          this.deleteFormList(temp, this._allProduct);
        }

      });
      this.cashingAssignProduct.set(this.selectedPosGroup, this._assignProduct);
      this.selectedProductDt.refresh();
      this.allProductDt.refresh();
      this.selectedRows = [];
    }
  }



  public onDeassign() {
    let temDeletedArr: any[] = [];
    this._assignSelectedRows.forEach(temp => {
      this.productDbCount--;
      if (!this.checkDublicteProductinAssignOrDeassign(temp, this.allProduct)) {
        this.allProduct.push(temp);
      }
      if (this.cashingAssignProduct.length > 0) {
        if (this.cashingAssignProduct.has(this.selectedPosGroup)) {
          const cashIndex: number = this.cashingAssignProduct.get(this.selectedPosGroup).indexOf(temp);
          if (cashIndex !== -1) {
            this.cashingAssignProduct.get(this.selectedPosGroup).splice(cashIndex, 1);
          }
        }

      }

      this.deleteFormList(temp, this._assignProduct)
      if (Array.isArray(this.overritePromotionProducts)) {
        if (this.overritePromotionProducts.length > 0) {
          this.overritePromotionProducts.forEach(deletedCash => {
            if (deletedCash === temp.id) {
              this.deleteFormList(deletedCash, this.overritePromotionProducts)
            }
          })

          this.promotionIds.forEach(promotion => {
            if (promotion === temp.id) {
              this.deleteFormList(promotion, this.promotionIds)
            }
          })
        }
      }

      if (this._isEdit) {
        temDeletedArr = this._assignSelectedRows;
      }
    });

    if (this._isEdit) {
      temDeletedArr.forEach(row => {
        if (!this.dbMap.get(this.selectedPosGroup).find(element => element.product.id === row.product.id)) {
          this.deleteFormList(row, temDeletedArr);
        };
      })

      if (temDeletedArr !== undefined && temDeletedArr.length > 0) {
        temDeletedArr.forEach(obj => {
          if (this.deassignSelectedEdit.get(this.selectedPosGroup) != null) {
            if (!this.deassignSelectedEdit.get(this.selectedPosGroup).find(row => row.product.id === obj.product.id)) {
              temDeletedArr = temDeletedArr.concat(this.deassignSelectedEdit.get(this.selectedPosGroup));
              this.deassignSelectedEdit.set(this.selectedPosGroup, temDeletedArr);
            }
          } else {
            this.deassignSelectedEdit.set(this.selectedPosGroup, temDeletedArr);
          }
        })
      }
    }
    this.selectedProductDt.refresh();
    this.allProductDt.refresh();
    this._assignSelectedRows = [];
  }

  search(searchTerm: string): void {
    debugger;
    this.searchTerm = searchTerm;
    this.getPagingBar().navigateToPage(1);
    this.getAllProductByPosGroup(this.selectedPosGroup, 0, this._pageSize, searchTerm, null, null);
  }


  openPromotionProductInfoDialog(row: PromotionProductPosGroup): void {

    if (row.promotion.id == null) {
      this.snackBar.open("Product is not assigned to promotion.", "Close", { duration: 3000 })
    } else {
      let promotionDetails: string = "";
      let arr: PromotionProductPosGroup[] = [];
      let posGroups: number[] = [];
      let products: number[] = [];
      products.push(row.product.id);
      posGroups.push(this.selectedPosGroup);

      let map = new TSMap<string, number[]>();

      map.set("products", products)
      map.set("groups", posGroups)

      this.promotionService.findPromotionDetailsByProductsAndPosGroups(map).subscribe(data => {
        if (data.length == 0) {
          this.snackBar.open("Product is not assigned to promotion.", "Close", { duration: 3000 })
          throw new Error("Product is not assigned to promotion.");
        }
        let arrayLength = data.length;

        data.forEach(element => {
          --arrayLength;
          if (element.promotionType == "ATR") {
            promotionDetails = promotionDetails + element.promotionAttribute.attribute + "+" + element.freeItem
          } else {
            promotionDetails = promotionDetails + element.quantity + "+" + element.freeItem
          }
          if (arrayLength != 0) {
            promotionDetails = promotionDetails.concat(",");
          }


        });

        row.promotionDetails = promotionDetails;
        arr.push(row);
        let dialogRef = this.dialogService.open(PromotionProductInformationDialog, {
          width: '750px',
          data: { items: arr, promotionType: data[0].promotionType, }
        });
      })

    }



  }

  openPromotionProductsConfirmDialog(): void {
    let productIds: number[] = [];
    let groupsIds: number[] = [];

    this.selectedRows.filter(obj => obj.promotion.id != null).forEach(t => {
      productIds.push(t.product.id);
    })

    groupsIds.push(this.selectedPosGroup);
    let dialogRef = this.dialogService.open(PromotionProductsConfirmDialog, {
      width: '500px',
      disableClose: true,
      data: {
        selectedProduct: this._selectedRows.filter(obj => obj.promotion.id != null),
        productIds: productIds,
        groupIds: groupsIds,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      let promotionProduct: number[] = [];
      let promotionIds: number[] = [];
      if (localStorage.getItem("overriteButtonClick") === "true") {
        this.selectedRows.forEach(productHasPromotion => {
          if (!this.checkDublicteProductinAssignOrDeassign(productHasPromotion, this._assignProduct)) {
            this.assignProduct.push(productHasPromotion);
            if (productHasPromotion.promotion.id != null) {
              promotionProduct.push(productHasPromotion.id);
              promotionIds.push(productHasPromotion.promotion.id);
            }
            this.deleteFormList(productHasPromotion, this._allProduct);
            this.productDbCount++;
          } else {
            if (this._selectedRows.filter(object => object.promotion.id != null).length == 0) {
              this.snackBar.open("This Product is already added.", "close", { duration: 3000 });
            }

          }
        })
        if (promotionProduct.length > 0) {
          promotionProduct.forEach(obj => {
            if (this.overritePromotionProducts != undefined) {
              this.overritePromotionProducts.push(obj);
            }
          })

          if (promotionIds != undefined && promotionIds != null) {
            promotionIds.forEach(object => {
              if (this.promotionIds != null && this.promotionIds != undefined) {
                this.promotionIds.push(object);
              }
            })
          }
        }
        this.cashingAssignProduct.set(this.selectedPosGroup, this.assignProduct);
        this.selectedRows = [];
        this.allProductDt.refresh();
        this.selectedProductDt.refresh();
        localStorage.setItem("overriteButtonClick", "false");
      }
    })
  }

  onSavePromotionProductList() {
    this.arr = [];
    for (let key of this.cashingAssignProduct.keys()) {
      this.cashingAssignProduct.get(key).forEach(data => {
        let promotionProductPosGroup: PromotionProductPosGroup = new PromotionProductPosGroup();
        let posGroup: PosGroupManagement = new PosGroupManagement();
        promotionProductPosGroup.product = data.product;
        posGroup.id = key;
        promotionProductPosGroup.posGroup = posGroup;
        promotionProductPosGroup.promotion = null;

        if (!this._isEdit) {
          this.arr.push(promotionProductPosGroup);
        } else {
          if (data.id == null || this.overritePromotionProducts.find(item => item === data.id)) {
            this.arr.push(promotionProductPosGroup);
          }
        }

      });
    }

    if (this._isEdit) {
      let deletedIds: number[] = [];
      for (let key of this.deassignSelectedEdit.keys()) {
        this.deassignSelectedEdit.get(key).forEach(item => {
          if (item == undefined) {
          } else {
            deletedIds.push(item.id)
          }

        })
      }
      this.promotionService.deletedListInEdit = deletedIds;
    }

    this.promotionService.assignProduct = this.arr;
    this.promotionService.overritePromotionProduct = this.overritePromotionProducts;
    this.promotionService.promotionIds = this.promotionIds;
    this.overritePromotionProducts = [];
    this.promotionIds = [];
  }


  checkProductHasPromotion(selectedPromotionProduct: PromotionProductPosGroup[]): boolean {
    let productHasPromotion: number = 0;
    if (!this._isEdit) {
      selectedPromotionProduct.forEach(selectedRow => {
        if (selectedRow.promotion.id != null) {
          productHasPromotion++;
        }
      })

    } else {
      selectedPromotionProduct.forEach(row => {
        if (row.promotion.id === this.promotionService.promotion.id) {
          if (productHasPromotion == 0) {
            productHasPromotion = 0;
          }
        } else if (row.promotion.id != null) {
          productHasPromotion++;
        } else {
          if (productHasPromotion == 0) {
            productHasPromotion = 0;
          }

        }
      })

    }
    if (productHasPromotion > 0) {
      return true;
    } else {
      return false;
    }
  }

  deleteFormList(obj: any, arr: any[]) {
    const index: number = arr.indexOf(obj);
    if (index !== -1) {
      arr.splice(index, 1);
    }
  }

  checkDublicteProductinAssignOrDeassign(obj: PromotionProductPosGroup, arr: PromotionProductPosGroup[]): boolean {
    let count = 0;
    arr.forEach(element => {
      if ((element.product.id === obj.product.id) && (element.posGroup.id === obj.posGroup.id)) {
        count++
      }
    })
    if (count > 0) {
      return true;
    } else {
      return false;
    }
  }

  compareWith(row: any, model: any): boolean {
    return row.product.id === model.product.id;
  }

  public get user(): User {
    return this._user;
  }
  public set user(value: User) {
    this._user = value;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }

  getDataTable(): TdDataTableComponent {
    return this.allProductDt;
  }

  public get errors(): any {
    return this._errors;
  }
  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): PromotionProductPosGroup[] {
    return this._selectedRows;
  }
  public set selectedRows(value: PromotionProductPosGroup[]) {
    this._selectedRows = value;
  }

  public get allModels(): PromotionProductPosGroup[] {
    return null;
  }

  public set allModels(value: PromotionProductPosGroup[]) {
    this._allProduct = value;
  }
  public get filteredTotal(): number {
    return this._filteredTotal;
  }
  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }
  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }
  public set pageSize(value: number) {
    this._pageSize = value;
  }


  public get currentPage(): number {
    return this._currentPage;
  }
  public set currentPage(value: number) {
    this._currentPage = value;
  }
  public get fromRow(): number {
    return this._fromRow;
  }
  public set fromRow(value: number) {
    this._fromRow = value;
  }


  public get searchTerm(): string {
    return this._searchTerm;
  }
  public set searchTerm(value: string) {
    this._searchTerm = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }
  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get allProduct(): PromotionProductPosGroup[] {
    return this._allProduct;
  }
  public set allProduct(value: PromotionProductPosGroup[]) {
    this._allProduct = value;
  }

  public get assignProduct(): PromotionProductPosGroup[] {
    return this._assignProduct;
  }
  public set assignProduct(value: PromotionProductPosGroup[]) {
    this._assignProduct = value;
  }

  public get assignSelectedRows(): PromotionProductPosGroup[] {
    return this._assignSelectedRows;
  }
  public set assignSelectedRows(value: PromotionProductPosGroup[]) {
    this._assignSelectedRows = value;
  }
}

@Component({
  selector: 'app-promotion-product-information-dialog',
  templateUrl: '../../dialogs/promotion-product-information-dlg.html',
  styleUrls: ['./product-promotion.component.css']
})
export class PromotionProductInformationDialog implements OnInit {

  @ViewChild('promotionProductDt') promotionProductDt: TdDataTableComponent;


  PromotionProductColumnConfig: ITdDataTableColumn[] = [
    { name: 'product.name', label: 'Product Name', width: 187 },
    { name: 'promotion.name', label: 'Promotion Name', width: 187 },
    { name: 'promotion.endDate', label: 'End Date', width: 187, format: v => this.datePipe.transform(v, 'mediumDate') },
    { name: 'promotionDetails', label: 'Promotion Details', width: 140 }
  ];

  constructor(public dialogRef: MatDialogRef<PromotionProductInformationDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected datePipe: DatePipe) {

  }

  ngOnInit() {
  }


  cancel(): void {
    this.dialogRef.close();
  }


}


@Component({
  selector: 'app-promotion-products-information-dialog',
  templateUrl: '../../dialogs/promotion-products-information-dlg.html',
  styleUrls: ['./product-promotion.component.css']
})
export class PromotionProductsInformationDialog implements OnInit {

  @ViewChild('promotionProductsDt') promotionProductsDt: TdDataTableComponent;

  PromotionProductsColumnConfig: ITdDataTableColumn[] = [
    { name: 'product.name', label: 'Product Name', width: 187 },
    { name: 'promotion.name', label: 'Promotion Name', width: 187 },
    { name: 'promotion.endDate', label: 'End Date', width: 187, format: v => this.datePipe.transform(v, 'mediumDate') },
    { name: 'promotionDetails', label: 'Promotion Details', width: 140 }
  ];

  constructor(public dialogRef: MatDialogRef<PromotionProductsInformationDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, protected datePipe: DatePipe) {

  }

  ngOnInit() {
  }


  cancel(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'app-promotion-product-confirm-dialog',
  templateUrl: '../../dialogs/promotion-product-confirm-dlg.html',
})
export class PromotionProductsConfirmDialog implements OnInit {
  overriteButtonClick: string = "false";
  constructor(public dialogRef: MatDialogRef<PromotionProductsConfirmDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected dialogService: TdDialogService,
    private datePipe: DatePipe,
    protected promotionService: PromotionService) {

  }

  ngOnInit() {
  }


  overritePromotion(): void {
    this.overriteButtonClick = "true";
    localStorage.setItem("overriteButtonClick", this.overriteButtonClick);
    this.cancel();
  }


  openPromotionProductsInfoDialog(): void {
    // this.cancel();
    let map = new TSMap<string, number[]>();
    map.set("products", this.data.productIds)
    map.set("groups", this.data.groupIds)
    this.promotionService.findPromotionDetailsByProductsAndPosGroups(map).subscribe(objects => {
      this.data.selectedProduct.forEach(t => {
        let objectsLength: number = objects.filter(promotion => promotion.promoitonTransient == t.promotion.id).length;
        t.promotionDetails = "";
        objects.filter(o => t.promotion.id == o.promoitonTransient).forEach(obj => {
          --objectsLength;

          if (obj.promotionType == "ATR") {
            t.promotionDetails = t.promotionDetails + obj.promotionAttribute.attribute + "+" + obj.freeItem
          } else {
            t.promotionDetails = t.promotionDetails + obj.quantity + "+" + obj.freeItem
          }

          if (objectsLength != 0) {
            t.promotionDetails = t.promotionDetails.concat(",");
          }
        })
      })
      let dialogRef = this.dialogService.open(PromotionProductsInformationDialog, {
        data: {
          width: 750,
          selectedProducts: this.data.selectedProduct,
          promotionType: objects[0].promotionType,
        }
      });
    });


  }

  cancel(): void {
    this.dialogRef.close();
  }


}

