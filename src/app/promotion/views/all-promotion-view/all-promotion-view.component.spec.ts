import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPromotionViewComponent } from './all-promotion-view.component';

describe('AllPromotionViewComponent', () => {
  let component: AllPromotionViewComponent;
  let fixture: ComponentFixture<AllPromotionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllPromotionViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPromotionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
