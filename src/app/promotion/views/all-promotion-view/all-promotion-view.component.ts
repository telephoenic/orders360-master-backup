import { BaseViewComponent } from '../../../common/view/base-view-component';
import { VendorAdminService } from './../../../vendor-admin/service/vendor-admin.service';
import { AuthService } from './../../../auth/service/auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TdDataTableSortingOrder, TdPagingBarComponent, TdDataTableComponent, ITdDataTableColumn, TdDialogService } from '@covalent/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { NgxPermissionsService } from 'ngx-permissions';
import { Promotion } from '../../model/promotion';
import { PromotionService } from '../../services/promotion-service.service';


@Component({
  selector: 'app-all-promotion-view',
  templateUrl: './all-promotion-view.component.html',
  styleUrls: ['./all-promotion-view.component.css']
})
export class AllPromotionViewComponent extends BaseViewComponent implements OnInit {

  private _currentPage: number = 1;
  private _fromRow: number = 1;

  private _allPromotions: Promotion[] = [];
  private _errors: any = '';
  private _selectedRows: Promotion[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = '';

  @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
  @ViewChild('dataTable') dataTable: TdDataTableComponent;

  columnsConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name' ,tooltip:"Name"}, 
    { name: 'startDate', label: 'Start Date' , format: v => this.datePipe.transform(v, 'mediumDate'), tooltip:"Start Date", width:160},
    { name: 'endDate', label: 'End Date' , format: v => this.datePipe.transform(v, 'mediumDate'), tooltip:"End Date", width:160},
    { name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive", width:100 },
    { name: 'id', label: 'Edit',tooltip:"Edit",sortable:false, width:100 }
    
  ];
  constructor(protected promotionService: PromotionService,
    protected authService: AuthService,
    protected vendorAdminService: VendorAdminService,
    protected datePipe:DatePipe,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    protected permissionsService: NgxPermissionsService) {
    super(promotionService, router, dialogService, snackBar);
  }

  ngOnInit() {
    super.ngOnInit();
    this.permissionsService.hasPermission("ROLE_COMMISSION_EDIT").then((result: boolean) => {
      if (!result) {
        for (let i = 0; i < this.dataTable.columns.length; i++) {
          if (this.dataTable.columns[i].name == 'id') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });

    this.permissionsService.hasPermission("ROLE_COMMISSION_ACTIVATE_DEACTIVATE_PER_ROW").then((result: boolean) => {
      if (!result) {
        for (let i = 0; i < this.dataTable.columns.length; i++) {
          if (this.dataTable.columns[i].name == 'inactive') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });
  }



  filterByVendor(pageNumber: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    vendorId: number) {

    this.promotionService.getAllPromotionsByVendorId(pageNumber, pageSize, vendorId, searchTerm, sortBy, sortOrder).subscribe(
      data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"]
      }
    );
  }


  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get allModels(): Promotion[] {
    return this._allPromotions;
  }

  public set allModels(value: Promotion[]) {
    this._allPromotions = value;
  }

  public get errors(): any {
    return this._errors;
  }

  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): Promotion[] {
    return this._selectedRows;
  }

  public set selectedRows(value: Promotion[]) {
    this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }

  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }

  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get searchTerm(): string {
    return this._searchTerm;
  }

  public set searchTerm(value: string) {
    this._searchTerm = value;
  }


}
