import { PromotionValueAttribute } from './promotion-value-attribute';
import { Vendor } from './../../vendor/vendor.model';
import {BaseModel} from '../../common/model/base-model';
import { PosUser } from './../../pos-user/pos-user.model';
import { Product } from './../../product/product';
import { PromotionValueQuantity } from './promotion-Value-quantity';
import { PromotionProductPosGroup } from './promotionProductPosGroup';



export class Promotion extends BaseModel{

    id: any;
    inactive: boolean;
    name:string;
    startDate:Date;
    endDate:Date;
    description:String;
    promotionQtyToValueTransient:{quantity:number, value:number}[]=[];
    products:Product[]=[];
    pos:PosUser[]=[];
    vendor:Vendor=new Vendor();
    promotionValueQuantity:PromotionValueQuantity[]=[];
    promotionValueAttribute:PromotionValueAttribute[]=[];
    promotionProductPosGroup:PromotionProductPosGroup[]=[];
}

