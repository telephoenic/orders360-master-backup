import { BaseModel } from "../../common/model/base-model";
export class PromotionType extends BaseModel {

    id: any;   
    inactive: boolean;
    code:string;
    type:string;

    constructor(){
        super();
    }

}
