import { Promotion } from './promotion';
import { BaseModel } from "../../common/model/base-model";

export class PromotionValue extends BaseModel{
    id: any;   
    inactive: boolean;
    freeItem: number;
    promotion:Promotion = new Promotion();
    promotionType:string;

    constructor(){
        super();
    }

}

