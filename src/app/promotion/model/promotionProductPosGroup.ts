import { Promotion } from "./promotion";
import { Product } from "../../product/product";
import { PosGroupManagement } from "../../posgroupmanagement/pos-group-management.model";

export class PromotionProductPosGroup{

    id:number;
    inactive:boolean;
    deleted:boolean;
    promotion:Promotion;
    product:Product;
    posGroup:PosGroupManagement;
    productCount:number;
    promotionDetails:string;


    constructor(){
        this.promotion=new Promotion();
        this.product=new Product();
        this.posGroup=new PosGroupManagement();
    }
    
}