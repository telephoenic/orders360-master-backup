import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { BaseModel } from '../../common/model/base-model';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { Promotion } from '../model/promotion';
import { PromotionProductPosGroup } from '../model/promotionProductPosGroup';
import { TSMap } from 'typescript-map';

@Injectable()
export class PromotionService extends BaseService {
  promotion: Promotion;
  navigateToAfterCompletion: string;
  assignProduct: PromotionProductPosGroup[] = [];
  overritePromotionProduct: number[] = [];
  deletedListInEdit: number[] = [];
  promotionIds: number[] = [];

  constructor(protected http: HttpClient,
    protected authService: AuthService) {
    super(http, "promotion", authService);
  }

  getAllPromotionsByVendorId(pageNum: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string): Observable<any> {
    return this.http.get(environment.baseURL + "promotion/promotion_for_vendor/" + pageNum + "/" + pageSize + "/" + vendorId, {
      params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
      headers: this.getBasicHeaders()
    });
  }

  findAllQuantityByPromotionId(id: number): Observable<any> {
    return this.http.get(environment.baseURL + "promotion/quantity_by_promotion/" + id);
  }


  findAllAttributeByPromotionId(id: number): Observable<any> {
    return this.http.get(environment.baseURL + "promotion/attribute_by_promotion/" + id);
  }

  deletePromotionQtyToValue(ids: number[]) {
    return this.http.post(environment.baseURL + "promotion/delete_qty_to_value",
      JSON.stringify(ids),
      { headers: this.getBasicHeaders() });
  }

  countPromotionByVendor(id: number): Observable<any> {
    return this.http.get(environment.baseURL + "promotion/count_promotion_by_vendor/" + id);
  }

  deletePromotionProductPosGroup(prmotionIds: number[]): Observable<any> {
    return this.http.post(environment.baseURL + "promotion/delete_promotion_product_pos_group", JSON.stringify(prmotionIds),
      { headers: this.getBasicHeaders() });
  }

  findAllAssignProductByPromotionId(promotionId: number, posGroup: number, vendorId: number): Observable<any> {
    return this.http.get(environment.baseURL + "promotion/find_all_assign_promoton_product/" + promotionId + "/" + posGroup + "/" + vendorId);
  }


  findAllAssignProductByPromotionIds(promotionId: number, posGroup: number, vendorId: number): Observable<any> {
    return this.http.get(environment.baseURL + "promotion/find_all_assign_promoton_products/" + promotionId + "/" + posGroup + "/" + vendorId);
  }

  deletePromotionById(prmotionIds: number[]): Observable<any> {
    return this.http.post(environment.baseURL + "promotion/delete_promotion_by_ids", JSON.stringify(prmotionIds), { headers: this.getBasicHeaders() });
  }

  productPromotionCount(prmotionIds: number[]): Observable<any> {
    return this.http.post(environment.baseURL + "promotion/check_product_count", JSON.stringify(prmotionIds), { headers: this.getBasicHeaders() });
  }

  findPromotionDetailsByProductAndPosGroup(productId: number, posGroupId: number): Observable<any> {
    return this.http.get(environment.baseURL + "promotion/by_pos_and_prod/" + productId + "/" + posGroupId)
  }

  findPromotionHasProduct(promotionId: number): Observable<any> {
    return this.http.get(environment.baseURL + "promotion/promotion_has_product/" + promotionId)
  }


  findPromotionDetailsByProductsAndPosGroups(promotionOptions: TSMap<string, number[]>): Observable<any> {
    return this.http.post(environment.baseURL + "promotion/find_promotion_details", promotionOptions.toJSON(), { headers: BaseService.HEADERS });
  }

  get model() {
    return this.promotion;
  }
  set model(model: BaseModel) {
    this.promotion = <Promotion>model;
  }

}
