import {BaseModel} from '../../common/model/base-model';
import { Injectable, ViewChild } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { DataSource } from '@angular/cdk/collections';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { SystemAdmin } from "../system-admin";


import { AllSystemAdminsComponent } from '../views/all-system-admins/all-system-admins.component';
import { environment } from "../../../environments/environment";
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class SystemAdminService extends BaseService{
  systemAdmin:SystemAdmin;
  navigateToAfterCompletion: string;

  constructor(http:HttpClient,
    protected authService:AuthService) {
    super(http, "system_admin", authService);
  }
  
  get model() {
    return this.systemAdmin;
  }

  set model(model:BaseModel) {
    this.systemAdmin = <SystemAdmin> model;
  }
}