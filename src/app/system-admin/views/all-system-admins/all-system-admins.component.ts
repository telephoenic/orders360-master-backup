import { AuthService } from './../../../auth/service/auth.service';
import { VendorAdminService } from './../../../vendor-admin/service/vendor-admin.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
  MatTable, MatHeaderCellDef, MatCellDef,
  MatColumnDef, MatPaginator, MatSort,
  MatInput, MatCheckbox,
  MatButton,
  MatSnackBar
} from "@angular/material";
import { DataSource } from '@angular/cdk/collections';
import { SystemAdmin } from "../../system-admin";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { SystemAdminService } from "../../service/system-admin.service";
import 'rxjs/add/observable/fromEvent';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import {
  ITdDataTableColumn,
  TdSearchBoxComponent,
  TdSearchInputComponent,
  TdPagingBarComponent,
  ITdDataTableSortChangeEvent,
  TdDataTableSortingOrder,
  IPageChangeEvent,
  TdDataTableService,
  TdDialogService,
  TdDataTableComponent
} from '@covalent/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { NgxPermissionsService } from 'ngx-permissions';

@Component({
  selector: 'all-system-admins',
  templateUrl: './all-system-admins.component.html',
  styleUrls: ['./all-system-admins.component.css']
})
export class AllSystemAdminsComponent extends BaseViewComponent implements OnInit{
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  
  private _allSystemAdmins: SystemAdmin[] = [];
  private _errors: any = '';
  private _selectedRows: SystemAdmin[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string ='';

  @ViewChild('pagingBar') pagingBar : TdPagingBarComponent;
  @ViewChild('dataTable') dataTable : TdDataTableComponent;

  columnsConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name' ,tooltip:"Name",width:400},
    { name: 'email', label: 'Email' ,tooltip:"Email", width:400 },
    { name: 'mobileNumber', label: 'Mobile',  tooltip:"Mobile", width:180},
    { name: "inactive", label: "Active/Inactive", tooltip : "Active or Inactive", width:130},
    { name: 'id', label: 'Edit',  tooltip:"Edit"}
  ];

  constructor(protected systemAdminService: SystemAdminService,
              protected router:Router,
              protected dialogService : TdDialogService,
              protected snackBar:MatSnackBar  , 
              protected permissionsService:NgxPermissionsService,
              protected athuService:AuthService,
              protected vendorAdminService: VendorAdminService) {

    super(systemAdminService, 
          router,
          dialogService, 
          snackBar,
          athuService);
  }

  ngOnInit() {
    super.ngOnInit();

    this.permissionsService.hasPermission("ROLE_SYSTEM_ADMIN_EDIT").then((result:boolean)=> {
      if(!result) {
        for(let i =0; i < this.dataTable.columns.length; i++) {
          if(this.dataTable.columns[i].name == 'id') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });


    this.permissionsService.hasPermission("ROLE_SYSTEM_ADMIN_ACTIVATE_DEACTIVATE_PER_ROW").then((result:boolean)=> {
      if(!result) {
        for(let i =0; i < this.dataTable.columns.length; i++) {
          if(this.dataTable.columns[i].name == 'inactive') {
            this.dataTable.columns.splice(i, 1);
          }
        }
      }
    });
  }
  

	public get currentPage(): number  {
		return this._currentPage;
	}

	public set currentPage(value: number ) {
		this._currentPage = value;
	}

	public get fromRow(): number  {
		return this._fromRow;
	}

	public set fromRow(value: number ) {
		this._fromRow = value;
	}

	public get allModels(): SystemAdmin[]  {
		return this._allSystemAdmins;
	}

	public set allModels(value: SystemAdmin[] ) {
		this._allSystemAdmins = value;
	}

	public get errors(): any  {
		return this._errors;
	}

	public set errors(value: any ) {
		this._errors = value;
	}

	public get selectedRows(): SystemAdmin[]  {
		return this._selectedRows;
	}

	public set selectedRows(value: SystemAdmin[] ) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string  {
		return this._sortBy;
	}

	public set sortBy(value: string ) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder  {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder ) {
		this._sortOrder = value;
	}

	public get searchTerm(): string  {
		return this._searchTerm;
	}

	public set searchTerm(value: string ) {
		this._searchTerm = value;
  }

  getPagingBar() : TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable() : TdDataTableComponent {
    return this.dataTable;
  }
  
}