import { BaseModel } from "../common/model/base-model";
import { UserPrivilegeProfile } from "../user-privilege/user-privilege-profiles";
import { UserPrivilegeProfileAware } from "../user-management/user-privilege-profile-aware";
import { UserType } from "../lookups/user-type.model";

export class SystemAdmin extends UserPrivilegeProfileAware {
    id: number;
    name: string;
    email: string;
    mobileNumber: string;
    inactive:boolean;
    privilegeProfiles:UserPrivilegeProfile[] = [];
    type: UserType;

    constructor (){
        super();
        this.type = new UserType(1); //TODO Change it//not eligible to use id to get the object
    }

}