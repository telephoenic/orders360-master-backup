import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, NgControlStatus, NgForm } from '@angular/forms';
import { MatFormFieldControl, MatButton, MatList, MatSuffix, MatInput, MatSnackBar } from "@angular/material";
import { SystemAdmin } from "../system-admin";
import { SystemAdminService } from "../service/system-admin.service";
import { ActivatedRoute, Router } from '@angular/router';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { BaseViewComponent } from '../../common/view/base-view-component';
import { UserPrivilegeProfileComponent } from '../../user-privilege/views/user-privilege-profile.component';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { BaseUserManagementForm } from '../../user-management/base-user-management-form';
import { environment } from "../../../environments/environment";
import { AuthService } from '../../auth/service/auth.service';

@Component({
  selector: 'system-admin-form',
  templateUrl: './system-admin-form.component.html',
  styleUrls: ['./system-admin-form.component.css']
})
export class SystemAdminFormComponent extends BaseUserManagementForm implements OnInit {
 
  
  @ViewChild("systemAdminForm") systemAdminForm : NgForm;
  _systemAdmin : SystemAdmin;
  _isEdit:boolean = false;

  @ViewChild("notSelectedPrivilegeProfiles")
  notSelectedPrivilegeProfiles:UserPrivilegeProfileComponent;

  @ViewChild("selectedPrivilegeProfiles")
  selectedPrivilegeProfiles:UserPrivilegeProfileComponent;


  constructor(protected systemAdminService: SystemAdminService,
              protected route:ActivatedRoute,
              protected router:Router,
              protected snackBar:MatSnackBar,
              protected authService:AuthService) { 

    super(systemAdminService, 
      route,
      router,
      snackBar);
  }

  ngOnInit() {
    this.selectedSecreen=environment.ENUM_CODE_USER_TYPE.TelephoenicAdmin;
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    if(this.route.snapshot.queryParams["edit"] == '1') {
      this._isEdit = true;
      this._systemAdmin = this.systemAdminService.systemAdmin;
  
    } else {
      this._systemAdmin = new SystemAdmin();
    }
  }

  onSaveAndNewCompleted() {
    this.showSpinnerOnSaveAndNew = false;
    this.selectedPrivilegeProfiles.allModels = [];
    this.selectedPrivilegeProfiles.dataTable.refresh();
    
    this.getNotSelectedPrivilegeProfiles().filterPrivilegeProfileForUser();
    this.notSelectedPrivilegeProfiles.dataTable.refresh();
  }

  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }
  
  get model() : SystemAdmin {
    return this._systemAdmin;
  }

  set model(systemAdmin:SystemAdmin){
    this._systemAdmin = systemAdmin;
  }
  
  initModel(): SystemAdmin {
    return new SystemAdmin();
  }

  ngFormInstance(): NgForm {
    return this.systemAdminForm;
  }

  getSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.selectedPrivilegeProfiles;
  }
  getNotSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.notSelectedPrivilegeProfiles;
  }
}