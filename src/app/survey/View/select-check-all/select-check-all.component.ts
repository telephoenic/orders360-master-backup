import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material';

@Component({
  selector: 'app-select-check-all',
  templateUrl: './select-check-all.component.html',
  styleUrls: ['./select-check-all.component.css']
})
export class SelectCheckAllComponent implements OnInit {

  @Input() model: FormControl;
  @Input() values = [];
  @Input() text = 'Select All'; 

  constructor() { }

  ngOnInit() {
  }


  isChecked(): boolean {
    if(this.model.value !=null)
      return this.model.value.length === this.values.length;  
  }

  isIndeterminate(): boolean {
    if(this.model.value !=null)
    return this.model.value.length < this.values.length;
  }

  toggleSelection(change: MatCheckboxChange): void {
    if (change.checked) {
      this.model.setValue(this.values);
    } else {
      this.model.setValue([]);
    }
  }

}
