import { VendorAdminService } from './../../../vendor-admin/service/vendor-admin.service';
import { SurveyService } from './../../Services/survey.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ITdDataTableColumn, TdDialogService, TdDataTableSortingOrder, TdDataTableComponent, TdPagingBarComponent, IPageChangeEvent } from '@covalent/core';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { PosService } from '../../../pos-user/service/pos.service';
import { AuthService } from '../../../auth/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { PosUser } from '../../../pos-user/pos-user.model';
import { User } from '../../../auth/user.model';
import { environment } from '../../../../environments/environment';
import { TSMap } from 'typescript-map';
import { PosAassignedVendor } from '../../../pos-user/pos-assigned-vendor';
import { Region } from '../../../location/region/Region.model';
import { VendorService } from '../../../vendor/Services/vendor.service';
import { CityService } from '../../../location/city/service/city.service';
import { City } from '../../../location/city/City.model';
import { RegionService } from '../../../location/region/service/region.service';
import { PosGroupManagement } from '../../../posgroupmanagement/pos-group-management.model';
import { PosGroupManagementService } from '../../../posgroupmanagement/service/pos-group-management.service';

@Component({
  selector: 'app-pos-survey',
  templateUrl: './pos-survey.component.html',
  styleUrls: ['./pos-survey.component.css']
})
export class PosSurveyComponent extends BaseViewComponent implements OnInit {

  allPossConfig: ITdDataTableColumn[] = [
    { name: 'posUser.name', label: 'Name' },
    { name: 'posUser.mobileNumber', label: 'Mobile' },
    { name: 'posGroup.name', label: 'Group' },
    { name: 'posUser.region.name', label: 'Area' },
  ];


  assigendPossConfig: ITdDataTableColumn[] = [
    { name: 'posUser.name', label: 'Name' },
    { name: 'posUser.mobileNumber', label: 'Mobile' },
    { name: 'posGroup.name', label: 'Group' },
    { name: 'posUser.region.name', label: 'Area' },

  ];


  @ViewChild('dataTable') dataTable: TdDataTableComponent;
  @ViewChild('assigned') assigned: TdDataTableComponent;

  public allPos: PosAassignedVendor[] = [];
  public assignedPos: PosAassignedVendor[] = [];
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private _errors: any = '';
  private _selectedRows: PosAassignedVendor[] = [];
  private _assigendSelectedRows: PosAassignedVendor[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = '';
  pagingBar: any;
  private _allInfo: PosUser[] = [];
  publish: boolean = false;
  private _user: User;
  possFilter:TSMap<string, number[]>;
  selectedRegion:number[]=[];
  selectedRegionList:number[]=[];
  filterOptionMap = new TSMap<string, number[]>();
  listOfRegion:Region[]=[];
  _vendorId:number;
  listOfCitiesIds : number[] = [];
  selectedGroup:number[]=[];
  listOfPosGroup:PosGroupManagement[]=[];
  selectedPosGroup:number[]=[];




  constructor(protected posService: PosService,
    protected surveyService: SurveyService,
    protected authService: AuthService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    protected vendorService: VendorService,
    public dialog: MatDialog,
    protected cityService: CityService,
    protected vendorAdminService: VendorAdminService,
    protected regionService:RegionService,
    protected posGroupManagementService:PosGroupManagementService) {

    super(posService, router, dialogService, snackBar);
  }

  ngOnInit() {
    this._user = this.authService.getCurrentLoggedInUser();
    if (this.route.snapshot.queryParams["edit"] == 1) {
      let survey = this.surveyService.survey;
      survey.posGroup.forEach(element => {
        this.selectedGroup.push(element.id);
      });

       survey.region.forEach(element => {
        this.selectedRegion.push(element.id);
      });


      if (survey.surveyStatus.id === 2) {
        this.publish = true;
      }
      if (this.user.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
        this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(vendorId => {
          let possFilter: TSMap<string, number[]> = new TSMap();
          let selectedGroup: number[] = [];
          let selectedRegion: number[] = [];

          survey.posGroup.forEach(element => {
            selectedGroup.push(element.id);
          });

          survey.region.forEach(element => {
            selectedRegion.push(element.id);
          });
          this.selectedPosGroup = selectedGroup;
          possFilter.set("region", selectedRegion);
          possFilter.set("group", selectedGroup);
          this.posService.getPosWhereSurveyId(vendorId,possFilter,this.surveyService.survey.id).subscribe(
            data => {
              this.assignedPos = data;
            }
          );
          
          this.findPossAssignVendorByRegionAndgroup(possFilter,environment.initialViewRowSize);          
        });
      }
  
      this.assigned.refresh();
      this.dataTable.refresh();

    }

    this.loadRegionOptions();
    this.loadPosGroup();

  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize = pagingEvent.pageSize;
    this.findPossAssignVendorByRegionAndgroup(this.possFilter,this.pageSize);
  } 
  
  
  findPossAssignVendorByRegionAndgroup(possFilter: TSMap<string, number[]>,initalPageSize:number) {
    if(this._pageSize===undefined){
      this._pageSize=initalPageSize;
    }
    this.possFilter=possFilter;
    if (this._user.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
      this.vendorAdminService.getVendorByVendorAdminId(this._user.id).subscribe(vendorId => {
        this.posService.getPosAssignWhereVendorId(this._currentPage - 1,this.pageSize, vendorId,this.searchTerm, this.possFilter).subscribe(data => {
          this.allPos=data["content"];
          this._filteredTotal=data["totalElements"];
          if(this.route.snapshot.queryParams["edit"] == 1){
            this.deletePosUsersFromList();
          }
        });

      });
    }


  }

  loadRegionOptions() {

    this.vendorAdminService.getVendorByVendorAdminId(this.user.id)
    .subscribe((vendorId:number) => {
     this._vendorId = vendorId;
     this.vendorService.getCountryIdByVendorId(this._vendorId).subscribe(
       data=>{
        this.cityService.findAllCiityByCountryId(data).subscribe(
          (data:City[])=>{
            data.forEach(city => {
              this.listOfCitiesIds.push(city.id);
            });
            this.regionService.findByCityIdWhereIn(this.listOfCitiesIds).subscribe(
              result=>{
                this.listOfRegion = result;
              }
            )
          });
       })
    });
  
  }


  loadPosGroup() {
    this.vendorAdminService.getVendorByVendorAdminId(this._user.id).subscribe(
      vendorId => {
        this.posGroupManagementService.getGroupsByVednor(vendorId).subscribe(data => {
          this.listOfPosGroup=data;
        });

      });
}

  selectRegionEvent(event){
    this.filterOptionMap.set("region", event.value)
    if (this.filterOptionMap.get("region").length === 0) {
        this.filterOptionMap.set("region", null);
    }
    this.findPossAssignVendorByRegionAndgroup(this.filterOptionMap,environment.initialViewRowSize);
  }

  selectGroupEvent(event){
    this.filterOptionMap.set("group", event.value)
    if (this.filterOptionMap.get("group").length === 0) {
        this.filterOptionMap.set("group", null);
    }

    this.findPossAssignVendorByRegionAndgroup(this.filterOptionMap,environment.initialViewRowSize);
  }




  public onDeassign() {
    this._assigendSelectedRows.forEach(temp => {
      this.allPos.push(temp);
      const index: number = this.assignedPos.indexOf(temp);
      if (index !== -1) {
        this.assignedPos.splice(index, 1);
      }
    });
    this.assigned.refresh();
    this.dataTable.refresh();
    this._assigendSelectedRows = [];
  }

  public onAssign() {
    this._selectedRows.forEach(temp => {
    if(!this.assignedPos.find(posAssignedVendor=> posAssignedVendor.posUser.id === temp.posUser.id)){
      this.assignedPos.push(temp);
      const index: number = this.allPos.indexOf(temp);
      if (index !== -1) {
        this.allPos.splice(index, 1);
      }

    }else{
      this.snackBar.open("This pos is already added.", "close", { duration: 3000 });
    }  
    });
    this.assigned.refresh();
    this.dataTable.refresh();
    this.selectedRows = [];

  }
 
  onSave() {
    let list :PosUser[]=[];
    this.assignedPos.forEach(obj=>{
      let posUser:PosUser=new PosUser();
      posUser.name=obj.posUser.name;
      posUser.mobileNumber=obj.posUser.mobileNumber;
      posUser.id=obj.posUser.id;
      list.push(posUser);
    })

    this.posService.posUserList = list;
    
  }






  public get user(): User {
    return this._user;
  }
  public set user(value: User) {
    this._user = value;
  }


  public get allModels(): PosUser[] {
    return null;
  }

  public set allModels(value: PosUser[]) {
    this._allInfo = value;
  }


  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }

  public get currentPage(): number {
    return this._currentPage;
  }
  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }
  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get errors(): any {
    return this._errors;
  }
  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): PosAassignedVendor[] {
    return this._selectedRows;
  }
  public set selectedRows(value: PosAassignedVendor[]) {
    this._selectedRows = value;
  }

  public get assigendSelectedRows(): PosAassignedVendor[] {
    return this._assigendSelectedRows;
  }
  public set assigendSelectedRows(value: PosAassignedVendor[]) {
    this._assigendSelectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }
  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }
  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }
  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }
  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get searchTerm(): string {
    return this._searchTerm;
  }
  public set searchTerm(value: string) {
    this._searchTerm = value;
  }

  public get allInfo(): PosUser[] {
    return this._allInfo;
  }
  public set allInfo(value: PosUser[]) {
    this._allInfo = value;
  }
  deletePosUsersFromList() {
    for(let i=0;i<this.allPos.length;i++){
      for(let j=0;j<this.assignedPos.length;j++){
        if(this.allPos[i].posUser.id===this.assignedPos[j].posUser.id){
          const index: number = this.allPos.indexOf(this.allPos[i]);
          if (index !== -1) {
            this.allPos.splice(index, 1);
          }  
        }
      }
    }

  }
}
