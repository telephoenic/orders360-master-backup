import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosSurveyComponent } from './pos-survey.component';

describe('PosSurveyComponent', () => {
  let component: PosSurveyComponent;
  let fixture: ComponentFixture<PosSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
