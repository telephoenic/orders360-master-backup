import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ITdDataTableColumn, TdDataTableSortingOrder, TdDataTableComponent, TdDialogService, TdPagingBarComponent } from '@covalent/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { Question } from '../../model/question';
import { Answer } from '../../model/answer';
import { QuestionService } from '../../Services/question.service';
import { AnswerService } from '../../Services/answer.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SurveyService } from '../../Services/survey.service';
import { AuthService } from '../../../auth/service/auth.service';
import { DatePipe } from '@angular/common';
import { Survey } from '../../model/survey';


@Component({
  selector: 'app-question-radio',
  templateUrl: './question-radio.component.html',
  styleUrls: ['./question-radio.component.css']
})
export class QuestionRadioComponent extends BaseViewComponent implements OnInit {


  private _questionList: Question[] = [];
  private _answer: Answer = new Answer();
  private _quesstion: Question = new Question();
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  public _allInfo: Question[] = [];
  private _errors: any = '';
  private _selectedRows: Question[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = '';
  private isDeleted: boolean;
  private deletedRows: Question[] = [];
  private _answerList: Answer[] = [];
  private _answerSelectedRows: Answer[] = [];
  private answerDeleteRows: Answer[] = [];
  flag: string;
  pagingBar: any;
  publish: boolean = false;


  @ViewChild('dataTable') dataTable: TdDataTableComponent;
  @ViewChild('answerDt') answerDt: TdDataTableComponent;

  questionColumns: ITdDataTableColumn[] = [
    { name: 'name', label: 'Question' },
    { name: 'id', label: 'Edit', width: 50 },
    { name: 'answer_id', label: 'Add', width: 50 },
    { name: 'question_id', label: 'View', width: 90 }];

  answerConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Answer', width: 480 },
    { name: 'id', label: '' }


  ];


  constructor(protected questionService: QuestionService,
    protected answerService: AnswerService,
    protected surveyService: SurveyService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected authService: AuthService,
    protected snackBar: MatSnackBar,
    protected matDialog: MatDialog,
    protected route: ActivatedRoute,
    private datePipe: DatePipe, ) {
    super(null, router, dialogService, snackBar);
  }



  ngOnInit() {

    if (this.route.snapshot.queryParams["edit"] == '1') {
      let survey: Survey = new Survey();
      survey = this.surveyService.survey;
      if (survey.surveyStatus.id === 2) {
        this.publish = true;
      }
      this._questionList = this.questionService.questionRadioList;
      for (let i = 0; i < this._questionList.length; i++) {
        for (let j = 0; j < this._questionList[i].answerList.length; j++) {
          this._answerList.push(this._questionList[i].answerList[j]);
        }

      }

    } else {
      this._questionList = [];
      this._answerList = [];
    }

  }

  onAdd() {
    this._quesstion.name = "";
    this._quesstion.questionType = null;
    this._quesstion.answerList = null;
    this.openQuestionDialog(null);

  }

  onShowAnswer(id: number, question: Question) {
    this._answerList = [];
    this.answerList = question.answerList;
    this.answerDt.refresh();
  }



  onEdit(id: number, row: Question) {
    this._quesstion.name = row.name;
    this.openQuestionDialog(row);
  }

  onAnswerEdit(id: number, row: Answer) {
    this._answer.name = row.name;
    this.flag = "edit";
    this.openAnswerDialog(row);
  }


  onAddAnswer(id: number, row: Question) {
    this._answer.name = "";
    this.flag = "add";
    this.onShowAnswer(id, row);
    this.openAnswerDialog(row);
  }


  onSave() {
    for (let i in this._questionList) {
      this._questionList[i].questionType.id = 2;
      this._questionList[i].questionType.code = "R";
      this._questionList[i].questionType.type = "radio";
    }

    if (this.route.snapshot.queryParams["edit"] == '1') {
      let questionIds: number[] = [];
      this.deletedRows.forEach(element => {
        questionIds.push(element.id);
      });
      this.questionService.onDeleteQuestion(questionIds).subscribe(data => {
      });
      let answerIds: number[] = [];
      this.answerDeleteRows.forEach(obj => {
        answerIds.push(obj.id);
      });
      this.answerService.radioAnswer = answerIds;
    }
    this.questionService.questionRadioList = this._questionList;

  }

  openQuestionDialog(row: Question): void {
    let dialogRef = this.dialogService.open(RadioQuestionDialog, {
      width: '500px',
      data: {
        name: this._quesstion.name,
      }
    });


    dialogRef.afterClosed().subscribe(result => {
      if (row != null) {
        row.name = result.name;
      } else {
        let tempQuestion: Question = new Question();
        tempQuestion.name = result.name;
        this._questionList.push(tempQuestion);
        this.questionService.questionList = this._questionList;
      }
      this.dataTable.refresh();
    });
  }

  openAnswerDialog(row: any): void {
    let dialogRef = this.dialogService.open(AnswerRadioDialog, {
      width: '500px',
      data: {
        answerName: this._answer.name,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!this._answerList.find(answer => answer.name.toLocaleLowerCase() === result.answerName.toLocaleLowerCase().trim())) {

        if (this.flag == "edit") {
          row.name = result.answerName;
        } else {
          let tempAnswer: Answer = new Answer();
          tempAnswer.name = result.answerName;
          this._answerList = null;
          row.answerList.push(tempAnswer);
          this._answerList = row.answerList;


        }
        this.answerDt.refresh();
        this.dataTable.refresh();

      } else {
        if (this.flag != "edit" || (this.flag == "edit" && result.answerName.toLocaleLowerCase().trim() != this._answer.name.toLowerCase().trim())) {
          this.snackBar.open("Question answer shouldn't be duplicate ", "Close", { duration: 3000 });
        }
      }
    });
  }





  onDelete() {
    if (this._questionList.length == 0) {
      this.snackBar.open("No Question added yet.", "Close", { duration: 3000 });
    } else if (this.selectedRows.length > 0) {
      let unSelected = this._questionList.filter(item => this.selectedRows.indexOf(item) < 0);
      this.dialogService.openConfirm({
        message: "Are you sure you want to delete the selected row(s)?",
        title: "confirmation",
        cancelButton: "Disagree",
        acceptButton: "Agree"
      }).afterClosed().subscribe((accept: boolean) => {
        if (accept) {

          this._questionList = unSelected;
          this.deletedRows = this.selectedRows;
          for (let i = 0; i < this.selectedRows.length; i++) {
            for (let j = 0; j < this.selectedRows[i].answerList.length; j++) {
              let deletedAnswer: Answer = new Answer();
              deletedAnswer = this.selectedRows[i].answerList[j];
              this._answerList = this._answerList.filter(item => item.name !== deletedAnswer.name);
            }

          }

          this.isDeleted = true;
          this.selectedRows = [];
        }
      }
      );
    } else {
      this.snackBar.open("Please select any item.", "Close", { duration: 3000 });
    }
  }




  onDeleteAnswer() {
    if (this._answerList.length == 0) {
      this.snackBar.open("No Answer added yet.", "Close", { duration: 3000 });
    } else if (this.answerSelectedRows.length > 0) {
      let unSelected = this._answerList.filter(item => this.answerSelectedRows.indexOf(item) < 0);
      this.dialogService.openConfirm({
        message: "Are you sure you want to delete the selected row(s)?",
        title: "confirmation",
        cancelButton: "Disagree",
        acceptButton: "Agree"
      }).afterClosed().subscribe((accept: boolean) => {
        if (accept) {

          this._answerList = unSelected;
          this.answerDeleteRows = this.answerSelectedRows;

          for (let i = 0; i < this.answerSelectedRows.length; i++) {
            let tempAnswer: Answer = new Answer();
            tempAnswer = this.answerSelectedRows[i];
            this.deleteAnswerFromQuestion(tempAnswer);
          }

          this.isDeleted = true;
          this.answerSelectedRows = [];
        }
      }
      );
    } else {
      this.snackBar.open("Please select any item.", "Close", { duration: 3000 });
    }
  }

  deleteAnswerFromQuestion(answer: Answer) {
    for (let i = 0; i < this._questionList.length; i++) {
      for (let j = 0; j < this._questionList[i].answerList.length; j++) {
        const index: number = this._questionList[i].answerList.indexOf(answer);
        if (index !== -1) {
          this._questionList[i].answerList.splice(index, 1);
        }
      }
    }

  }


  public get answerSelectedRows(): Answer[] {
    return this._answerSelectedRows;
  }
  public set answerSelectedRows(value: Answer[]) {
    this._answerSelectedRows = value;
  }

  public get answerList(): Answer[] {
    return this._answerList;
  }
  public set answerList(value: Answer[]) {
    this._answerList = value;
  }

  public get answer(): Answer {
    return this._answer;
  }
  public set answer(value: Answer) {
    this._answer = value;
  }
  public get quesstion(): Question {
    return this._quesstion;
  }
  public set quesstion(value: Question) {
    this._quesstion = value;
  }
  public get questionList(): Question[] {
    return this._questionList;
  }
  public set questionList(value: Question[]) {
    this._questionList = value;
  }

  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get allModels(): Question[] {
    return null;
  }

  public set allModels(value: Question[]) {
    this._allInfo = value;
  }

  public get errors(): any {
    return this._errors;
  }

  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): Question[] {
    return this._selectedRows;
  }

  public set selectedRows(value: Question[]) {
    this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }

  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }

  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get searchTerm(): string {
    return this._searchTerm;
  }

  public set searchTerm(value: string) {
    this._searchTerm = value;
  }





}


@Component({
  selector: 'app-question-radio-dialog',
  templateUrl: './question-radio-dlg.html',
})
export class RadioQuestionDialog implements OnInit {

  constructor(public dialogRef: MatDialogRef<RadioQuestionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
  }

  cancel(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'app-answer-radio-dialog',
  templateUrl: './answer-radio-dlg.html',
})
export class AnswerRadioDialog implements OnInit {

  constructor(public dialogRef: MatDialogRef<AnswerRadioDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
  }

  public cancel() {
    this.dialogRef.close();
  }
}



