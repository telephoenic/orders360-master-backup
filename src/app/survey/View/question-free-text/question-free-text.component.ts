import { Question } from './../../model/question';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { TdDataTableSortingOrder, TdDataTableComponent, ITdDataTableColumn, TdDialogService, TdPagingBarComponent } from '@covalent/core';
import { QuestionService } from '../../Services/question.service';
import { SurveyService } from '../../Services/survey.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../auth/service/auth.service';
import { MatSnackBar, MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DatePipe } from '@angular/common';
import { Survey } from '../../model/survey';

@Component({
  selector: 'app-question-free-text',
  templateUrl: './question-free-text.component.html',
  styleUrls: ['./question-free-text.component.css']
})
export class QuestionFreeTextComponent extends BaseViewComponent implements OnInit {

  private _questionList: Question[] = [];
  private _quesstion: Question = new Question();
  private _currentPage: number = 1;
  private _fromRow: number = 1;
  public _allInfo: Question[] = [];
  private _errors: any = '';
  private _selectedRows: Question[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = '';
  private isDeleted: boolean;
  private deletedRows: Question[] = [];
  flag: string;
  pagingBar: any;
  publish: boolean = false;


  @ViewChild('dataTable') dataTable: TdDataTableComponent;

  questionColumns: ITdDataTableColumn[] = [
    { name: 'name', label: 'Question',width:700},
    { name: 'id', label: '' },
  ];




  constructor(protected questionService: QuestionService,
    protected surveyService: SurveyService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected authService: AuthService,
    protected snackBar: MatSnackBar,
    protected matDialog: MatDialog,
    protected route: ActivatedRoute,
    private datePipe: DatePipe, ) {
    super(null, router, dialogService, snackBar);
  }



  ngOnInit() {
    if (this.route.snapshot.queryParams["edit"] == '1') {
      let survey: Survey = new Survey();
      survey = this.surveyService.survey;
      if (survey.surveyStatus.id === 2) {
        this.publish = true;
      }
      this._questionList = this.questionService.questionFreeText;
    } else {
      this._questionList = [];
    }
  }

  onAdd() {
    this._quesstion.name = "";
    this._quesstion.questionType = null;
    this._quesstion.answerList = null;
    this.openQuestionDialog(null);
  }


  onEdit(id: number, row: Question) {
    this._quesstion.name = row.name;
    this.openQuestionDialog(row);
  }



  onSave() {
    for (let i in this._questionList) {
      this._questionList[i].questionType.id = 3;
      this._questionList[i].questionType.code = "F";
      this._questionList[i].questionType.type = "FreeText";
    }

    if(this.route.snapshot.queryParams["edit"] == '1'){
      let questionIds: number[] = [];
      this.deletedRows.forEach(element => {
        questionIds.push(element.id);
      }); 
      this.questionService.onDeleteQuestion(questionIds).subscribe(data => {
        console.log("After Service");
      });
    }


    this.questionService.questionFreeText = this._questionList;

  }





  openQuestionDialog(row: Question): void {
    let dialogRef = this.dialogService.open(FreeTextQuestionDialog, {
      width: '500px',

      data: {
        name: this._quesstion.name,
      }
    });


    dialogRef.afterClosed().subscribe(result => {
        if (row != null) {
          row.name = result.name;
        } else {
          let tempQuestion: Question = new Question();
          tempQuestion.name = result.name;
          this._questionList.push(tempQuestion);
          this.questionService.questionList = this._questionList;
        }
      this.dataTable.refresh();
    });
  }




  onDelete() {
    if (this._questionList.length == 0) {
      this.snackBar.open("No Question added yet.", "Close", { duration: 3000 });
    } else if (this.selectedRows.length > 0) {
      let unSelected = this._questionList.filter(item => this.selectedRows.indexOf(item) < 0);
      this.dialogService.openConfirm({
        message: "Are you sure you want to delete the selected row(s)?",
        title: "confirmation",
        cancelButton: "Disagree",
        acceptButton: "Agree"
      }).afterClosed().subscribe((accept: boolean) => {
        if (accept) {

          this._questionList = unSelected;
          this.deletedRows = this.selectedRows;

          this.isDeleted = true;
          this.selectedRows = [];
        }
      }
      );
    } else {
      this.snackBar.open("Please select any item.", "Close", { duration: 3000 });
    }
  }



  public get quesstion(): Question {
    return this._quesstion;
  }
  public set quesstion(value: Question) {
    this._quesstion = value;
  }
  public get questionList(): Question[] {
    return this._questionList;
  }
  public set questionList(value: Question[]) {
    this._questionList = value;
  }

  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get allModels(): Question[] {
    return null;
  }

  public set allModels(value: Question[]) {
    this._allInfo = value;
  }

  public get errors(): any {
    return this._errors;
  }

  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): Question[] {
    return this._selectedRows;
  }

  public set selectedRows(value: Question[]) {
    this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }

  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }

  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }

  public get searchTerm(): string {
    return this._searchTerm;
  }

  public set searchTerm(value: string) {
    this._searchTerm = value;
  }





}


@Component({
  selector: 'app-question-text-dialog',
  templateUrl: './question-text-dlg.html',
})
export class FreeTextQuestionDialog implements OnInit {



  constructor(public dialogRef: MatDialogRef<FreeTextQuestionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
