import { User } from './../../../auth/user.model';
import { Survey } from './../../model/survey';
import { MatIcon, MatDialogRef, MAT_DIALOG_DATA, MatOption } from '@angular/material';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ITdDataTableColumn, TdPagingBarComponent, TdDataTableComponent, TdDialogService, TdDataTableSortingOrder } from '@covalent/core';
import { BaseViewComponent } from '../../../common/view/base-view-component';
import { SurveyService } from '../../Services/survey.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../auth/service/auth.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DatePipe } from '@angular/common';
import { NgxPermissionsService } from 'ngx-permissions';
import { SurveyFormComponent } from '../../Form/survey-form.component';
import { VendorAdminService } from '../../../vendor-admin/service/vendor-admin.service';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Region } from '../../../location/region/Region.model';
import { VendorService } from '../../../vendor/Services/vendor.service';
import { CityService } from '../../../location/city/service/city.service';
import { City } from '../../../location/city/City.model';
import { RegionService } from '../../../location/region/service/region.service';
import { PosGroupManagementService } from '../../../posgroupmanagement/service/pos-group-management.service';
import { PosGroupManagement } from '../../../posgroupmanagement/pos-group-management.model';

@Component({
  selector: 'app-survey-view',
  templateUrl: './survey-view.component.html',
  styleUrls: ['./survey-view.component.css']
})
export class SurveyViewComponent extends BaseViewComponent implements OnInit {

  private _currentPage: number = 1;
  private _fromRow: number = 1;
  private _allSurvey: Survey[] = [];
  private _errors: any = '';
  private _selectedRows: Survey[] = [];
  private _pageSize: number;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = '';
  private _user: User;
  _vendorId: number;



  @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
  @ViewChild('dataTable') dataTable: TdDataTableComponent;
  @ViewChild(SurveyFormComponent) viewQuestion: SurveyFormComponent;




  columnsConfig: ITdDataTableColumn[] = [
    { name: 'title', label: 'Name', tooltip: "Name", width: 280 },
    { name: 'startDate', label: 'Start Date', format: v => this.datePipe.transform(v, 'mediumDate'), tooltip: "Start Date" },
    { name: 'endDate', label: 'End Date', format: v => this.datePipe.transform(v, 'mediumDate'), tooltip: "End Date" },
    { name: 'loyaltyPoints', label: 'Points Amount', tooltip: "Points Amount" },
    { name: 'surveyStatus.name', label: 'Status', tooltip: "Status" },
    { name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive" },
    { name: 'id', label: 'Edit', tooltip: "Edit" },
    { name: 'copy', label: 'Copy', tooltip: "Copy" },



  ];

  constructor(protected surveyService: SurveyService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected authService: AuthService,
    protected vendorAdminService: VendorAdminService,
    protected vendorService: VendorService,
    protected snackBar: MatSnackBar,
    protected matDialog: MatDialog,
    protected cityService: CityService,
    private datePipe: DatePipe,
    protected permissionsService: NgxPermissionsService,
    protected regionService: RegionService) {
    super(surveyService, router, dialogService, snackBar);
  }
  ngOnInit() {
    super.ngOnInit();
    this._user=this.authService.getCurrentLoggedInUser();
  }

  filterByVendor(pageNumber: number, pageSize: number,
    searchTerm: string, sortBy: string, sortOrder: string, vendorId: number) {
    this.surveyService.getAllSurveyByVendorId(pageNumber, pageSize, vendorId, searchTerm, sortBy, sortOrder).subscribe(
      data => {
        this.allModels = data["content"];
        this.filteredTotal = data["totalElements"]
      }
    );
  }


  onUpdateStatus() {
    if (this._selectedRows.length > 0) {
      this.dialogService.openConfirm({
        message: "Are you sure you want to publish the selected row(s)?",
        title: "confirmation",
        cancelButton: "Disagree",
        acceptButton: "Agree"
      }).afterClosed().subscribe((accept: boolean) => {
        if (accept) {

          if (this.checkSurveyPublishStatus(this._selectedRows)) {
            if (this.checkSurveyStatus(this._selectedRows)) {
              let selectedIds: number[] = [];
              this._selectedRows.forEach(survey => {
                selectedIds.push(survey.id);
              });
              this.surveyService.onUpdateSurveyStatus(2, selectedIds).subscribe(data => {
                this.selectedRows = [];
                this.onRefresh();
              }, error => {
                this.selectedRows = [];
                this.onRefresh();
              });
            } else {
              this.snackBar.open("One or more survey already published.", "close", { duration: 3000 });

            }
          } else {
            this.snackBar.open("One or more survey are not active.", "close", { duration: 3000 });
          }

        }
      }
      )

    } else if (this.allModels.length == 0) {
      this.onNoDataFound();
    } else {
      this.onNoSelectedRows();
    }
  }

  onCopySurvey(id: number, survey: Survey) {
    let dialogRef = this.dialogService.open(CopySurveyDialog, {
      width: '60%',
      data: {
        regionList: survey.region,
        posGroupList: survey.posGroup,
        survey: survey
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.pageSize = this.pagingBar.pageSize;
      this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(
        vendorId => {
          this.surveyService.getAllSurveyByVendorId(0, this.pageSize, vendorId, "", "", "").subscribe(
            data => {
              this.allModels = data["content"];
              this.filteredTotal = data["totalElements"]

            }
          );

        },

      );
    });
    this.dataTable.refresh
  }


  validateDates(startDate: Date, endDate: Date) {
    let currentDate: Date = new Date();
    currentDate.setHours(0, 0, 0, 0);
    startDate.setHours(0, 0, 0, 0);

    if (startDate > endDate) {
      this.snackBar.open("End date must be after than start date.", "Close", { duration: 4000 })
      throw new Error("End date must be after than start date.");
    } if (startDate < currentDate) {
      this.snackBar.open("Start date must be current date or after .", "Close", { duration: 4000 })
      throw new Error("Start date must be current date or after.");
    }
  }

  onNoSelectedRows() {
    this.snackBar.open("No selected rows.", "close", { duration: 3000 });
  }

  onNoDataFound() {
    this.snackBar.open("No data found.", "close", { duration: 3000 });
  }


  checkSurveyStatus(list: Survey[]): boolean {
    for (let i = 0; i < list.length; i++) {
      if (list[i].surveyStatus.id === 2) {
        return false;
      }
    }
    return true;
  }


  checkSurveyPublishStatus(list: Survey[]): boolean {
    for (let i = 0; i < list.length; i++) {
      if (list[i].inactive === true) {
        return false;
      }
    }
    return true;
  }




  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number {
    return this._currentPage;
  }
  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }
  public set fromRow(value: number) {
    this._fromRow = value;
  }


  public get allModels(): Survey[] {
    return this._allSurvey;
  }

  public set allModels(value: Survey[]) {
    this._allSurvey = value;
  }

  public get allSurvey(): Survey[] {
    return this._allSurvey;
  }
  public set allSurvey(value: Survey[]) {
    this._allSurvey = value;

  }

  public get errors(): any {
    return this._errors;
  }
  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): Survey[] {
    return this._selectedRows;
  }
  public set selectedRows(value: Survey[]) {
    this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }
  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }
  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }
  public get sortBy(): string {
    return this._sortBy;
  }
  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }
  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }
  public get searchTerm(): string {
    return this._searchTerm;
  }
  public set searchTerm(value: string) {
    this._searchTerm = value;
  }

  public get user(): User {
    return this._user;
  }
  public set user(value: User) {
    this._user = value;
  }

}

@Component({
  selector: 'app-copy-survey',
  templateUrl: './copy-survey-dlg.html',
})
export class CopySurveyDialog implements OnInit {

  regionsFormControl = new FormControl();
  posGroupFormControl = new FormControl();
  @ViewChild('allRegionSelected') private allRegionSelected: MatOption;
  @ViewChild('allPosGroupSelected') private allPosGroupSelected: MatOption;


  listOfCitiesIds: number[] = [];
  listOfRegion: Region[] = [];
  posGroupList: PosGroupManagement[] = [];
  user: User;

  constructor(public dialogRef: MatDialogRef<CopySurveyDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected vendorAdminService: VendorAdminService,
    protected vendorService: VendorService,
    protected cityService: CityService,
    protected regionService: RegionService,
    protected authService: AuthService,
    protected snackBar: MatSnackBar,
    protected surveyService: SurveyService,
    protected posGroupManagementService: PosGroupManagementService) {

  }

  ngOnInit() {
    this.user = this.authService.getCurrentLoggedInUser();
    this.loadRegionOptions();
    this.loadPosGroup();
   }

  loadPosGroup() {
    this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(
      vendorId => {
        this.posGroupManagementService.getGroupsByVednor(vendorId).subscribe(data => {
          this.posGroupList = data;
          if(this.posGroupList.length == this.data.posGroupList.length){
            this.posGroupFormControl
            .setValue(this.data.posGroupList);
          }else{
            this.posGroupFormControl
            .setValue(this.data.posGroupList);
          }
        });
      });
  }

  loadRegionOptions() {
      this.vendorAdminService.getVendorByVendorAdminId(this.user.id)
      .subscribe((vendorId: number) => {
        this.vendorService.getCountryIdByVendorId(vendorId).subscribe(
          data => {
            this.cityService.findAllCiityByCountryId(data).subscribe(
              (data: City[]) => {
                data.forEach(city => {
                  this.listOfCitiesIds.push(city.id);
                });
                this.regionService.findByCityIdWhereIn(this.listOfCitiesIds).subscribe(
                  result => {
                    this.listOfRegion = result;
                    if(this.listOfRegion.length == this.data.regionList.length){
                      this.regionsFormControl.setValue(this.data.regionList)
                    }else{
                      this.regionsFormControl
                      .setValue(this.data.regionList)
                    }
                  }
                )
              });
          })
      });
  }


  selectRegionPerOne(all) {
    if (this.allRegionSelected.selected) {
      this.allRegionSelected.deselect();
      return false;
    }
    if(this.regionsFormControl.value.length == this.listOfRegion.length){
      this.allRegionSelected.select();
    }
  }


  selectAllRegion(allRegion) {
    if (this.allRegionSelected.selected) {
      this.regionsFormControl
        .patchValue([...this.listOfRegion, true]);
    } else {
      this.regionsFormControl.patchValue([]);
    }
  }


  selectPosGroupPerOne(all) {
    if (this.allPosGroupSelected.selected) {
      this.allPosGroupSelected.deselect();
      return false;
    }
    if(this.posGroupFormControl.value.length == this.posGroupList.length){
      this.allPosGroupSelected.select();
    }
  }


  selectAllPosGroup(allRegion) {
    if (this.allPosGroupSelected.selected) {
      this.posGroupFormControl
        .patchValue([...this.posGroupList, true]);
    } else {
      this.posGroupFormControl.patchValue([]);
    }
  }


  public options = function (option, value): boolean {
    return option.id === value.id;
  }

  save() {
    this.validateDates(this.data.startDate, this.data.endDate);
    this.data.survey.id = null;
    this.data.survey.title = this.data.title;
    this.data.survey.startDate = this.data.startDate;
    this.data.survey.endDate = this.data.endDate;
    this.data.survey.region = this.regionsFormControl.value;
    this.data.survey.posGroup = this.posGroupFormControl.value;
    this.deleteFromList(this.data.survey.region, true);
    this.deleteFromList(this.data.survey.posGroup, true);
    this.surveyService.onCopySurvey(this.data.survey).subscribe(data => {
      this.cancel()
    }, (errorResponse: HttpErrorResponse) => {
      this.snackBar.open(errorResponse.error.message, "Close", { duration: 3000 });
      throw new Error(errorResponse.error.message);
    });



  }

  deleteFromList(arr: any[], key: any) {
    const index = arr.indexOf(key, 0);
    if (index > -1) {
      arr.splice(index, 1);
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }


  validateDates(startDate: Date, endDate: Date) {
    let currentDate: Date = new Date();
    currentDate.setHours(0, 0, 0, 0);
    startDate.setHours(0, 0, 0, 0);

    if (startDate > endDate) {
      this.snackBar.open("End date must be after than start date.", "Close", { duration: 4000 })
      throw new Error("End date must be after than start date.");
    } if (startDate < currentDate) {
      this.snackBar.open("Start date must be current date or after .", "Close", { duration: 4000 })
      throw new Error("Start date must be current date or after.");
    }
  }

}
