import { Answer } from './../model/answer';
import { Question } from './../model/question';
import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { HttpClient } from '@angular/common/http';
import { BaseModel} from '../../common/model/base-model';
import { AuthService } from '../../auth/service/auth.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class QuestionService extends BaseService{

  navigateToAfterCompletion: string;
  question:Question;
  questionList:Question[]=[];
  questionRadioList:Question[]=[];
  questionFreeText:Question[]=[];
  answerList:Answer[]=[];
  deletedQuestion:number[]=[];
  questionIds:number[]=[];
  qAnswerIds:number[]=[];

  constructor(http:HttpClient,protected authService: AuthService) { 
    super(http, "question", authService);
                 
  }

  onDeleteQuestion(questionIds:number[]){
    return this.http.post(environment.baseURL + "question/update_question_status",
    JSON.stringify(questionIds),
    { headers: this.getBasicHeaders() });
  }

   
  get model() {
    return null;
  }

  set model(model:BaseModel) {
    this.question = <Question>model;
  }
  
}
