import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { Survey } from '../model/survey';
import { BaseModel } from '../../common/model/base-model';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class SurveyService extends BaseService {

  survey: Survey;
  surveyList: Survey[] = [];
  navigateToAfterCompletion: string;
  selectedRegion:number[]=[];
  selectedGroup:number[]=[];

  constructor(http: HttpClient, protected authService: AuthService) {
    super(http, "survey", authService);
  }

  onUpdateSurveyStatus(status: number, surveyIds: number[]) {
    return this.http.post(environment.baseURL + "survey/update_survey_status" + "/" + status,
      JSON.stringify(surveyIds),
      { headers: this.getBasicHeaders() });

  }

  

  getAllSurveyByVendorId(pageNum:number, 
                         pageSize:number, 
                         vendorId:number , 
                         searchTerm:string, 
                         sortBy:string, 
                         sortOrder:string ) : Observable<any>{
    return this.http.get(environment.baseURL + "survey/survey_for_vendor/"+ pageNum+"/"+pageSize+"/"+vendorId, 
                        { params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder), 
                          headers: this.getBasicHeaders()});
  }



  onCopySurvey(survey: Survey) {
    console.log(survey);
    return this.http.post(environment.baseURL + "survey/copy_survey"+ "/",
      JSON.stringify(survey),
      { headers: this.getBasicHeaders() });

  }

 


  get model() {
    return this.survey;
  }

  set model(model: BaseModel) {
    this.survey = <Survey>model;
  }

}
