import { Answer } from './../model/answer';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/service/auth.service';
import { BaseModel } from '../../common/model/base-model';
import { BaseService } from '../../common/service/base.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AnswerService extends BaseService{

  public answerList:Answer[]=[];
  navigateToAfterCompletion: string;
  answerIds:number[]=[];
  radioAnswer:number[]=[];


  constructor(http:HttpClient,protected authService: AuthService) { 
    super(http, "answer", authService);
                 
  }


  updateAnswerStatus(answerIds:number[]){
    return this.http.post(environment.baseURL + "answer/update_answer_status",
    JSON.stringify(answerIds),
    { headers: this.getBasicHeaders() });
  }



  get model() {
    return null;
  }

  set model(model: BaseModel) {
  }
}
