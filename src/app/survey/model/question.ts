import { Answer } from './answer';
import { Survey } from './survey';
import { QuestionType } from './question-type';

export class Question {

    inactive: boolean;
    id: number;
    name: string;
    questionType:QuestionType;
    answerList:Answer[]=[];

    constructor() {
        this.questionType=new QuestionType();
    }


}
