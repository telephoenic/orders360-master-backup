import { PosUser } from './../../pos-user/pos-user.model';
import { Question } from "./question";
import { SurveyStatus } from "./survey-status";
import { Vendor } from '../../vendor/vendor.model';
import { PosGroupManagement } from '../../posgroupmanagement/pos-group-management.model';
import { Region } from '../../location/region/Region.model';

export class Survey {
    inactive: boolean;
    id: number;
    title: string;
    startDate: Date;
    endDate: Date;
    questionList:Question[]=[];
    surveyStatus:SurveyStatus;
    posUser:PosUser[]=[];
    vendor:Vendor;
    loyaltyPoints:number;
    posGroup:PosGroupManagement[]=[];
    region:Region[]=[];
    

    constructor ( ){}
}
