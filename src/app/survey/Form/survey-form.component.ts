import { Vendor } from './../../vendor/vendor.model';
import { PosService } from './../../pos-user/service/pos.service';
import { SurveyStatus } from './../model/survey-status';
import { SurveyService } from './../Services/survey.service';
import { Survey } from './../model/survey';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatSnackBar, MatOption } from '@angular/material';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { FormControl, Validators, NgForm, FormGroup, FormBuilder } from '@angular/forms';
import { QuestionCheckBoxComponent } from '../View/question-check-box/question-check-box.component';
import { QuestionService } from '../Services/question.service';
import { AnswerService } from '../Services/answer.service';
import { QuestionRadioComponent } from '../View/question-radio/question-radio.component';
import { QuestionFreeTextComponent } from '../View/question-free-text/question-free-text.component';
import { User } from '../../auth/user.model';
import { AuthService } from '../../auth/service/auth.service';
import { environment } from '../../../environments/environment';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';
import { PosGroupManagementService } from '../../posgroupmanagement/service/pos-group-management.service';
import { PosGroupManagement } from '../../posgroupmanagement/pos-group-management.model';
import { RegionService } from '../../location/region/service/region.service';
import { Region } from '../../location/region/Region.model';
import { CityService } from '../../location/city/service/city.service';
import { VendorService } from '../../vendor/Services/vendor.service';
import { TSMap } from 'typescript-map';
import { City } from '../../location/city/City.model';


@Component({
  selector: 'app-survey-form',
  templateUrl: './survey-form.component.html',
  styleUrls: ['./survey-form.component.css']
})
export class SurveyFormComponent extends BaseFormComponent implements OnInit {

  @ViewChild("surveyForm") surveyForm: NgForm;
  @ViewChild(QuestionCheckBoxComponent) viewQuestion: QuestionCheckBoxComponent;
  @ViewChild(QuestionRadioComponent) viewRadioQuestion: QuestionRadioComponent;
  @ViewChild(QuestionFreeTextComponent) viewTextQuestion: QuestionFreeTextComponent;

  searchUserForm: FormGroup;

  @ViewChild('allRegionSelected') private allRegionSelected: MatOption;
  @ViewChild('allPosGroupSelected') private allPosGroupSelected: MatOption;

  regionsFormControl = new FormControl();
  posGroupFormControl = new FormControl();


  private _survey: Survey;
  _isEdit: boolean = false;
  publish: boolean = false;
  private _user: User;
  listOfPosGroup: PosGroupManagement[] = [];
  listOfRegion: Region[] = [];
  _vendorId: number;
  listOfCitiesIds: number[] = [];

  constructor(protected surveyService: SurveyService,
    protected posService: PosService,
    protected vendorAdminService: VendorAdminService,
    protected questionService: QuestionService,
    protected answerService: AnswerService,
    protected authService: AuthService,
    protected route: ActivatedRoute,
    protected router: Router,
    private datePipe: DatePipe,
    protected cityService: CityService,
    protected vendorService: VendorService,
    protected snackBar: MatSnackBar,
    protected posGroupManagementService: PosGroupManagementService,
    protected regionService: RegionService,
    private fb: FormBuilder) {
    super(surveyService,
      route,
      router,
      snackBar);
    this._survey = new Survey();
  }


  ngOnInit() {
    this._user = this.authService.getCurrentLoggedInUser();
    if (this.route.snapshot.queryParams["edit"] == 1) {
      this._isEdit = true;
      this._survey = this.surveyService.survey;
      this.loadRegionOptions();
      this.loadPosGroup();

      if (this._survey.surveyStatus.id === 2) {
        this.publish = true;
      } else {
        this.publish = false;
      }
      this._survey.startDate = new Date(this.datePipe.transform(this._survey.startDate, 'mediumDate'));
      this._survey.endDate = new Date(this.datePipe.transform(this._survey.endDate, 'mediumDate'));
      this.questionService.questionList = [];
      this.questionService.questionRadioList = [];
      this.questionService.questionFreeText = [];
      for (let i = 0; i < this.surveyService.survey.questionList.length; i++) {
        if (this.surveyService.survey.questionList[i].questionType.id === 1) {
          this.questionService.questionList.push(this._survey.questionList[i]);
        } else if (this.surveyService.survey.questionList[i].questionType.id === 2) {
          this.questionService.questionRadioList.push(this._survey.questionList[i]);
        } else if (this.surveyService.survey.questionList[i].questionType.id === 3) {
          this.questionService.questionFreeText.push(this._survey.questionList[i]);
        }
      }
    } else {
      this._survey = new Survey();
      this.regionsFormControl = new FormControl();
      this.posGroupFormControl = new FormControl();

      this.searchUserForm = this.fb.group({
        regionFormControl: new FormControl(),
        posGroupFormControl: new FormControl()
      });
      this.loadRegionOptions();
      this.loadPosGroup();
    }
    this.regionsFormControl.setValidators(Validators.required);
    this.posGroupFormControl.setValidators(Validators.required);

  }


  loadRegionOptions() {
    this.vendorAdminService.getVendorByVendorAdminId(this.user.id)
      .subscribe((vendorId: number) => {
        this._vendorId = vendorId;
        this.vendorService.getCountryIdByVendorId(this._vendorId).subscribe(
          data => {
            this.cityService.findAllCiityByCountryId(data).subscribe(
              (data: City[]) => {
                data.forEach(city => {
                  this.listOfCitiesIds.push(city.id);
                });
                this.regionService.findByCityIdWhereIn(this.listOfCitiesIds).subscribe(
                  result => {
                    this.listOfRegion = result;
                    if (this.isEdit) {
                      if (this.listOfRegion.length == this.survey.region.length) {
                        this.regionsFormControl.setValue(this.survey.region)
                      } else {
                        this.regionsFormControl
                          .setValue(this.survey.region)
                      }
                    }
                  }
                )
              });
          })
      });
  }



  onSaveAndNew() {
    this.validateDates();
    this.viewQuestion.onSave();
    this.viewRadioQuestion.onSave();
    this.viewTextQuestion.onSave();
    this._user = this.authService.getCurrentLoggedInUser();

    if (this.user.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
      this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(
        vendorId => {
          if (this.questionService.questionList.length >= 1 || this.questionService.questionRadioList.length >= 1 || this.questionService.questionFreeText.length >= 1) {
            this.questionService.questionList.forEach(ans => {
              if (ans.answerList.length < 2) {
                this.showSpinner = false;
                this.snackBar.open("Question must have at least two answers.", "Close", { duration: 3000 })
                throw new Error("Question must have at least two answers.");
              }
            })

            this.questionService.questionRadioList.forEach(ans => {
              if (ans.answerList.length < 2) {
                this.snackBar.open("Question must have at least two answers.", "Close", { duration: 3000 })
                throw new Error("Question must have at least two answers.");
              }
            })

          } else {
            this.showSpinner = false;
            this.snackBar.open("Survey must be have at least one question.", "Close", { duration: 3000 })
            throw new Error("Survey must be have at least one question.");
          }



          this._survey.questionList = this.questionService.questionList.concat(this.questionService.questionRadioList, this.questionService.questionFreeText);
          let survyStatus: SurveyStatus = new SurveyStatus();
          let vendor: Vendor = new Vendor();
          vendor.id = vendorId;
          survyStatus.id = 1;
          survyStatus.code = "N";
          survyStatus.name = "New";
          this._survey.vendor = vendor;
          this._survey.surveyStatus = survyStatus;
          this._survey.region = this.regionsFormControl.value;
          this._survey.posGroup = this.posGroupFormControl.value;
          this.deleteFromList(this._survey.region, true);
          this.deleteFromList(this._survey.posGroup, true);
          super.onSaveAndNew();
        }
      );

    } else {
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("The logged user not vendor admin", "close", { duration: 3000 })
    }

  }

  resetAfterSaveNew() {
    this.regionsFormControl = new FormControl();
    this.posGroupFormControl = new FormControl();
    this.viewQuestion.questionList = [];
    this.viewQuestion.answerList = [];
    this.viewRadioQuestion.questionList = [];
    this.viewRadioQuestion.answerList = [];
    this.viewTextQuestion.questionList = [];
  }

  onSaveAndNewCompleted() {
    this.resetAfterSaveNew();
    this.showSpinnerOnSaveAndNew = false;
  }

  onSave() {
    this.validateDates();
    this.viewQuestion.onSave();
    this.viewRadioQuestion.onSave();
    this.viewTextQuestion.onSave();

    if (this.user.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
      this.vendorAdminService.getVendorByVendorAdminId(this.user.id).subscribe(
        vendorId => {
          if (this.questionService.questionList.length >= 1 || this.questionService.questionRadioList.length >= 1 || this.questionService.questionFreeText.length >= 1) {
            this.questionService.questionList.forEach(ans => {
              if (ans.answerList.length < 2) {
                this.showSpinner = false;
                this.snackBar.open("Question must have at least two answers.", "Close", { duration: 3000 })
                throw new Error("Question must have at least two answers.");
              }
            })

            this.questionService.questionRadioList.forEach(ans => {
              if (ans.answerList.length < 2) {
                this.snackBar.open("Question must have at least two answers.", "Close", { duration: 3000 })
                throw new Error("Question must have at least two answers.");
              }
            })

          } else {
            this.showSpinner = false;
            this.snackBar.open("Survey must be have at least one question.", "Close", { duration: 3000 })
            throw new Error("Survey must be have at least one question.");
          }

          if (this._isEdit) {
            this.questionService.onDeleteQuestion(this.questionService.questionIds).subscribe(data => {
            });

            this.answerService.updateAnswerStatus(this.questionService.qAnswerIds).subscribe(data => {
            });
            let deletedAnswer: number[] = [];
            deletedAnswer = this.answerService.answerIds.concat(this.answerService.radioAnswer);

            this.answerService.updateAnswerStatus(deletedAnswer).subscribe(data => {
            });



          }


          this._survey.questionList = this.questionService.questionList.concat(this.questionService.questionRadioList, this.questionService.questionFreeText);
          let survyStatus: SurveyStatus = new SurveyStatus();
          let vendor: Vendor = new Vendor();
          vendor.id = vendorId;
          survyStatus.id = 1;
          survyStatus.code = "N";
          survyStatus.name = "New";
          this._survey.vendor = vendor;
          this._survey.surveyStatus = survyStatus;
          this._survey.region = this.regionsFormControl.value;
          this._survey.posGroup = this.posGroupFormControl.value;
          super.onSave();
        }
      );

    } else {
      this.showSpinner = false;
      this.snackBar.open("The logged user not vendor admin", "close", { duration: 3000 })
    }
  }


  loadPosGroup() {
    this.vendorAdminService.getVendorByVendorAdminId(this._user.id).subscribe(
      vendorId => {
        this.posGroupManagementService.getGroupsByVednor(vendorId).subscribe(data => {
          this.listOfPosGroup = data;

          if (this.isEdit) {
            if (this.listOfPosGroup.length == this.survey.posGroup.length) {
              this.posGroupFormControl
                .setValue(this.survey.posGroup);
            } else {
              this.posGroupFormControl
                .setValue(this.survey.posGroup);
            }
          }
        });

      });
  }

  validateDates() {
    let currentDate: Date = new Date();
    currentDate.setHours(0, 0, 0, 0);
    this._survey.startDate.setHours(0, 0, 0, 0);

    if (this._survey.startDate > this._survey.endDate) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("End date must be after than start date.", "Close", { duration: 3000 })
      throw new Error("End date must be after than start date.");
    }
  }

  deleteFromList(arr: any[], key: any) {
    const index = arr.indexOf(key, 0);
    if (index > -1) {
      arr.splice(index, 1);
    }
  }

  selectRegionPerOne(all) {
    if (this.allRegionSelected.selected) {
      this.allRegionSelected.deselect();
      return false;
    }
    if (this.regionsFormControl.value.length == this.listOfRegion.length) {
      this.allRegionSelected.select();
    }

  }

  selectPosGroupPerOne(all) {
    if (this.allPosGroupSelected.selected) {
      this.allPosGroupSelected.deselect();
      return false;
    }

    if (this.posGroupFormControl.value.length == this.listOfPosGroup.length) {
      this.allPosGroupSelected.select();
    }
  }

  selectAllRegion(allRegion) {
    if (this.allRegionSelected.selected) {
      this.regionsFormControl
        .patchValue([...this.listOfRegion, 0]);
      this.deleteFromList(this.regionsFormControl.value, 0)

    } else {
      this.regionsFormControl.patchValue([]);
    }
  }

  selectAllPosGroup(allPosGroup) {
    if (this.allPosGroupSelected.selected) {
      this.posGroupFormControl
        .patchValue([...this.listOfPosGroup, true]);
    } else {
      this.posGroupFormControl.patchValue([]);
    }
  }


  public options = function (option, value): boolean {
    if (value != null) {
      return option.id === value.id;
    }
  }

  public get user(): User {
    return this._user;
  }
  public set user(value: User) {
    this._user = value;
  }


  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): Survey {
    return this._survey;
  }

  set model(survey: Survey) {
    this._survey = survey;
  }

  initModel(): Survey {
    return new Survey();
  }

  ngFormInstance(): NgForm {
    return this.surveyForm;
  }

  public get survey(): Survey {
    return this._survey;
  }
  public set survey(value: Survey) {
    this._survey = value;
  }

}

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
