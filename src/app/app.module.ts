import { DatePipe, DecimalPipe } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatSelectModule, MatTreeModule, MatMenuModule, MatBadgeModule, MatDialogModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from "@angular/router";
import { CovalentFileModule, CovalentMessageModule, CovalentPagingModule, CovalentSearchModule, TdDialogService } from '@covalent/core';
import { CovalentTextEditorModule } from '@covalent/text-editor';
import { MatProgressButtonsModule } from 'mat-progress-buttons';
import { NgxGalleryModule } from 'ngx-gallery';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxPermissionsGuard, NgxPermissionsModule } from 'ngx-permissions';
import { GMapModule } from 'primeng/gmap';
import { SelectButtonModule } from 'primeng/selectbutton';
import { AppComponent, SessionTimeOutDialog } from './app.component';
import { AuthInterceptor } from './auth/interceptor/auth-interceptor';
import { LoginComponent } from './auth/login/login.component';
import { AuthService } from './auth/service/auth.service';
import { BrandComponent } from './brand/brand.component';
import { BrandService } from './brand/service/brand.service';
import { RelatedProductDialogComponent } from './brand/views/related-product-dialog/related-product-dialog.component';
import { BrandDialog, ViewBrandsComponent } from './brand/views/view-brands/view-brands.component';
import { CategoryManagmentComponent } from './category/category-managment.component';
import { CategoryManagementForm } from './category/form/category-management-form.component';
import { CategoryNodeTreeComponent } from './category/node-tree/category-node-tree.component';
import { CategoryService } from './category/service/category.service';
import { CategoryTreeComponent } from './category/tree/category-tree.component';
import { RelatedCategoriesAndProductsDialogComponent } from './category/views/related-categories-and-products-dialog/related-categories-and-products-dialog.component';
import { ComboComponent } from './combo/combo.component';
import { ComboFormComponent } from './combo/form/combo-form.component';
import { ComboService } from './combo/service/combo.service';
import { ComboInfoService } from './combo/service/comboinfo.service';
import { RelatedItemsDialogComponent } from './combo/view/related-items-dialog/related-items-dialog.component';
import { ViewComboComponent } from './combo/view/view-combo/view-combo.component';
import { DialogAddProductCombo, DialogComboInfo, ViewComboInfoComponent } from './combo/view/view-products/view-comboinfo.component';
import { IndustryService } from './common/industry/service/industry.service';
import { RelatedTicketsComponent } from './communicationcenter/supporticket-type/related-tickets/related-tickets.component';
import { TicketTypeService } from './communicationcenter/supporticket-type/service/ticket-type.service';
import { SupporticketTypeComponent } from './communicationcenter/supporticket-type/supporticket-type.component';
import { SupportTicketTypeDialog, ViewTicketTypeComponent } from './communicationcenter/supporticket-type/view-ticket-type/view-ticket-type.component';
import { SupportTicketService } from './communicationcenter/supporticket/service/supporticket.service';
import { SupportTicketComponent } from './communicationcenter/supporticket/supporticket.component';
import { DialogTicket, ViewSupportTicketComponent } from './communicationcenter/supporticket/view/view-supporticket.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DiscountComponent } from './discount/discount.component';
import { DiscountFormComponent } from './discount/form/discount-form-component';
import { ConfrimationDialog, ProductDiscountComponent, ProductsHaveDiscountDialog, SingleProductHasDiscountDialog } from './discount/product-discount/product-discount-component';
import { DiscountProductsPosGroupsService } from './discount/service/discount-products-posgroups.service';
import { DiscountService } from './discount/service/discount-service.service';
import { AllDiscountsViewComponent } from './discount/view/all-discounts-component';
import { FcmComponent } from './fcm/fcm.component';
import { FcmService } from './fcm/services/fcm.service';
import { AllPosUsersComponent } from './fcm/view/all-pos-users/all-pos-users.component';
import { HeaderComponent } from "./header/header.component";
import { HomeComponent } from './home/home.component';
import { CityService } from './location/city/service/city.service';
import { CountryService } from './location/country/service/country.service';
import { RegionService } from './location/region/service/region.service';
import { CurrencyService } from './lookups/service/currency.service';
import { UserTypeService } from './lookups/service/user-type.service';
import { LoyaltyFormComponent } from './loyalty/form/loyalty-form.component';
import { LoyaltyComponent } from './loyalty/loyalty.component';
import { LoyaltyPointsTrxSourceService } from './loyalty/service/loyalty-points-trx-source.service';
import { LoyaltyPointsTrxService } from './loyalty/service/loyalty-points-trx.services';
import { LoyaltyService } from './loyalty/service/loyalty.service';
import { ProductLoyaltyService } from './loyalty/service/product-loyalty.service';
import { ViewLoyaltyComponent } from './loyalty/view/view-loyalty/view-loyalty.component';
import { ViewProductLoyaltyComponent } from './loyalty/view/view-products/view-product-loyalty.component';
import { OrdersFormComponent } from './orders/form/orders-form.component';
import { OrdersComponent } from './orders/orders.component';
import { ComboItemService } from './orders/service/combo-item.service';
import { OrderItemHistoryService } from './orders/service/order-item-history';
import { OrderItemService } from './orders/service/order-item.service';
import { OrderService } from './orders/service/order.service';
import { PromotionItemService } from './orders/service/promotion-item.service';
import { SingleItemService } from './orders/service/single-item.service';
import { DeliveryStatusService } from './orders/view/delevery-status-service';
import { AllComboItemsComponent } from './orders/view/view-details/combo/view-combo-item.component';
import { OrderPromotionComponent } from './orders/view/view-details/order-promotion/order-promotion.component';
import { AllSingleItemsComponent } from './orders/view/view-details/single/view-single-item.component';
import { AllOrderItemsComponent, DialogComboHistory, DialogSingleHistory, PromotionDetailsDialog ,RejectedOrdersDlg, OrderStatusHistoryDlg, ShipmentSerialDlg} from './orders/view/view-details/view-order-details.component';
import { AllOrdersComponent } from './orders/view/view-orders/view-orders.component';
import { Orders360MaterialModule } from "./orders360-material.module";
import { PosFormComponent } from './pos-user/form/pos-form.component';
import { PosSearchComponent } from './pos-user/pos-search/pos-search.component';
import { PosComponent } from './pos-user/pos.component';
import { AssignedVendorsService } from "./pos-user/service/assigned-vendors";
import { PosTypeService } from './pos-user/service/pos-type.service';
import { PosService } from "./pos-user/service/pos.service";
import { ViewAllPosComponent } from './pos-user/view/view-assigned-pos/view-all-pos.component';
import { DialogChangePosType, ViewDataPosComponent } from './pos-user/view/view-assigned-pos/view-data-pos.component';
import { DialogAssignedVendors, ViewAssignedVendorFormComponent } from './pos-user/view/view-assigned-vendor/view-assigned-vendor.component';
import { ViewPosFormComponent } from './pos-user/view/view-pos/view-pos.component';
import { PosGroupManagementFormComponent } from './posgroupmanagement/form/pos-group-management-form.component';
import { PosGroupManagementComponent } from './posgroupmanagement/pos-group-management.component';
import { PosGroupManagementService } from './posgroupmanagement/service/pos-group-management.service';
import { ViewPosGroupManagementComponent } from './posgroupmanagement/view/view-pos-group-management.component';
import { PowerBIService } from './power-bi/service/power-bi.service';
import { PriorityService } from './priority/priority.service';
import { PrivilegeProfileBlockRoleService } from './privilege/aware/block-role/privilege-profile-block-role.service';
import { AllBlockRolesForModulesComponent } from './privilege/aware/block-role/views/all-block-roles-for-modules/all-block-roles-for-modules.component';
import { PrivilegeProfileModuleRoleService } from './privilege/aware/module-role/privilege-profile-module-role.service';
import { AllModuleRolesByAppComponent } from './privilege/aware/module-role/views/all-module-roles-by-app/all-module-roles-by-app.component';
import { PrivilegeProfileViewRoleService } from "./privilege/aware/view-role/privilege-profile-view-role.service";
import { AllViewRolesForModulesComponent } from './privilege/aware/view-role/views/all-view-roles-for-modules/all-view-roles-for-modules.component';
import { PrivilegeProfileFormComponent } from './privilege/form/privilege-profile-form.component';
import { PrivilegeProfileComponent } from './privilege/privilege-profile.component';
import { PrivilegeProfileService } from './privilege/service/privilege-profile.service';
import { AllPrivilegeProfilesComponent } from './privilege/views/all-privilige-profiles/all-privilege-profiles.component';
import { AllProducts } from './product/all-products.component';
import { ProductForm } from './product/form/product-form.component';
import { NodeTreeComponent } from './product/node-tree/node-tree.component';
import { ProductManagmentComponent } from './product/product-managment/product-managment.component';
import { ProductService } from './product/service/product.service';
import { PromotionAttributeComponent, PromotionAttributeQtyAndValueDialog } from './promotion-attribute/promotion-attribute.component';
import { PromotionAttributeDialog, PromotionFormComponent, PromotionQuantityDialog } from './promotion/form/promotion-form.component';
import { PromotionComponent } from './promotion/promotion.component';
import { PromotionService } from './promotion/services/promotion-service.service';
import { AllPromotionViewComponent } from './promotion/views/all-promotion-view/all-promotion-view.component';
import { ProductPromotionComponent, PromotionProductInformationDialog, PromotionProductsConfirmDialog, PromotionProductsInformationDialog } from './promotion/views/product-promotion/product-promotion.component';
import { RewardFormComponent } from './reward/form/reward-form.component';
import { RewardComponent } from './reward/reward.component';
import { ProductRewardService } from './reward/service/product-reward.service';
import { RewardService } from './reward/service/reward.service';
import { DialogProductReward, ViewProductRewardComponent } from './reward/view/view-products/view-product-reward.component';
import { ViewRewardsComponent } from './reward/view/view-reward/view-reward.component';
import { ApplicationRoleService } from './role/application/application-role.service';
import { BlockRoleService } from './role/block/block-role.service';
import { ModuleRoleService } from './role/module/module-role.service';
import { ViewRoleService } from './role/view/view-role.service';
import { MatSelectSearchModule } from './sales-agent/form/mat-select-search/mat-select-search-module';
import { SalesAgentFormComponent } from './sales-agent/form/sales-agent-form.component';
import { SalesAgentComponent } from './sales-agent/sales-agent.component';
import { SalesAgentService } from './sales-agent/service/sales-agent.service';
import { AllSalesAgentsComponent } from './sales-agent/views/all-sales-agents/all-sales-agents.component';
import { SalesAgentRegionComponent } from './sales-agent/views/region/sales-agent-region.component';
import { SalesSupervisorFormComponent } from './sales-supervisor/form/sales-supervisor-form.component';
import { SalesSupervisorComponent } from './sales-supervisor/sales-supervisor.component';
import { SalesSupervisorService } from './sales-supervisor/service/sales-supervisor.service';
import { AllSalesSupervisorsComponent } from './sales-supervisor/views/all-sales-supervisors/all-sales-supervisors.component';
import { SalesSupervisorRegionComponent } from './sales-supervisor/views/region/sales-supervisor-region.component';
import { AreaComponent } from './settings/Area/area.component';
import { AllAreasComponent, AreaDialog } from './settings/Area/view/all-areas.component';
import { CityComponent } from './settings/city/city.component';
import { AllCitiesComponent, CityDialog } from './settings/city/view/all-cities/all-cities.component';
import { SettingsFormComponent } from './settings/form/settings-form.component';
import { LetterFormComponent } from './settings/letter/form/letter-form/letter-form.component';
import { LetterComponent } from './settings/letter/Letter.component';
import { LetterService } from './settings/letter/service/letter.service';
import { AllLettersComponent } from './settings/letter/view/all-letters/all-letters.component';
import { SettingsService } from './settings/service/settings.service';
import { SettingsComponent } from './settings/settings.component';
import { ViewSettingsFormComponent } from './settings/view/view-settings.component';
import { ApplicationService } from './structure/application/application.service';
import { BlockService } from './structure/block/block.service';
import { ModuleService } from './structure/module/module.service';
import { ViewService } from './structure/view/view.service';
import { SubscriptionFormComponent, SubscriptionMinToMaxDialog } from './subscription/form/subscription-form.component';
import { PosSubscriptionService } from './subscription/service/pos-subscription.service';
import { SubscriptionTypeService } from './subscription/service/subscription-type.service';
import { SubscriptionService } from './subscription/service/subscription.service';
import { SubscriptionComponent } from './subscription/subscription.component';
import { AllSubscriptionsComponent } from './subscription/views/all-subscriptions/all-subscriptions.component';
import { SurveyFormComponent } from './survey/Form/survey-form.component';
import { AnswerService } from './survey/Services/answer.service';
import { QuestionService } from './survey/Services/question.service';
import { SurveyService } from './survey/Services/survey.service';
import { SurveyComponent } from './survey/survey.component';
import { PosSurveyComponent } from './survey/View/pos-survey/pos-survey.component';
import { AnswerDialog, CheckBoxQuestionDialog, QuestionCheckBoxComponent } from './survey/View/question-check-box/question-check-box.component';
import { FreeTextQuestionDialog, QuestionFreeTextComponent } from './survey/View/question-free-text/question-free-text.component';
import { AnswerRadioDialog, QuestionRadioComponent, RadioQuestionDialog } from './survey/View/question-radio/question-radio.component';
import { SelectCheckAllComponent } from './survey/view/select-check-all/select-check-all.component';
import { CopySurveyDialog, SurveyViewComponent } from './survey/View/survey-view/survey-view.component';
import { SystemAdminFormComponent } from './system-admin/form/system-admin-form.component';
import { SystemAdminService } from './system-admin/service/system-admin.service';
import { SystemAdminComponent } from './system-admin/system-admin.component';
import { AllSystemAdminsComponent } from './system-admin/views/all-system-admins/all-system-admins.component';
import { UserPrivilegeProfileService } from './user-privilege/service/user-privilege-profile.service';
import { UserPrivilegeProfileComponent } from './user-privilege/views/user-privilege-profile.component';
import { VendorAdFormComponent } from './vendor-ad/form/vendor-ad-form.component';
import { VendorAdAllComponent } from './vendor-ad/views/all-vendor-ads/all-vendor-ads.component';
import { VendorAdMainroutingComponent } from './vendor-ad/views/component/vendor-ad.component';
import { VendorAdminFormComponent } from './vendor-admin/form/vendor-admin-form.component';
import { VendorAdminService } from './vendor-admin/service/vendor-admin.service';
import { VendorAdminComponent } from "./vendor-admin/vendor-admin.component";
import { AllVendorAdminsComponent } from './vendor-admin/views/all-vendor-admins/all-vendor-admins.component';
import { VendorFormComponent } from './vendor/Form/vendor-form.component';
import { VendorAddressService } from "./vendor/Services/vendor-address.service";
import { VendorService } from "./vendor/Services/vendor.service";
import { VendorComponent } from './vendor/vendor.component';
import { DialogAddresses, ViewAddress } from './vendor/View/ViewAddress/view-address';
import { ViewVendor } from './vendor/View/ViewVendor/view-vendor';
import { WarehouseFormComponent } from './warehouse/form/warehouse-form.component';
import { WarehouseService } from './warehouse/service/warehouse.service';
import { ViewWarehouse } from './warehouse/view/warehouse-view';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { WarehouseAdminComponent } from './warehouse-admin/warehouse-admin.component';
import { WarehouseAdminFormComponent } from './warehouse-admin/form/warehouse-admin-form.component';
import { WarehouseAdminService } from './warehouse-admin/service/warehouse-admin.service';
import { ViewWarehouseAdmin } from './warehouse-admin/views/warehouse-admin-view-component';
import { ViewDeliveryUser } from './delivery-user/views/delivery-user-view.component';
import { DeliveryUserFormComponent } from './delivery-user/form/delivery-user-form.component';
import { DeliveryUserComponent } from './delivery-user/delivery-user.component';
import { DeliveryUserSerivce } from './delivery-user/services/delivery-user.service';
import { DeliveryUserRegionComponent } from './delivery-user/region/delivery-user-region.component';
import { OrderRejectedReasonComponent } from './reasons/reason.component';
import { ViewOrderRejectedReason, OrderRejectedReasonDialog } from './reasons/views/reason-view.component';
import { OrderRejectedReason } from './reasons/reason.model';
import { OrderRejectedReasonService } from './reasons/services/reason-service';
import { ProductPosGroupService } from './product/service/product-pos-group.service';
import { OrderStatusService } from './orders/service/order-status.service';
import { RejectedOrderService } from './orders/service/rejected-order.service';
import { ShippmentOrderComponent } from './shippment-order/shippment-order.component';
import { ShippmentOrderViewComponent } from './shippment-order/view/shippment-order-view/shippment-order-view.component';
import { ShippmentOrderService } from './shippment-order/service/shippment-order.service';
import { ShipmentOrderItemComponent, ShipmentOrderItemHistoryDialog, AssignToDeliveryDlg ,ShipmentOrderHistoryDlg} from './shippment-order/view/shipment-order-item/shipment-order-item.component';
import { UserNotifiactionComponent } from './user-notification/user-notification-view.component';
import { NotifiactionComponent } from './notification/notification.component';
import { NotificationFormComponent } from './notification/form/notification-form.component';
import { NotificationViewComponent } from './notification/view/notification-view.component';
import { MessageService } from 'primeng/api';
import { NotificationService } from './notification/notification-service.service';
import { UserNotificationService } from './user-notification/user-notification.service';
<<<<<<< HEAD
import { PosRequestVendorComponent } from './pos-request-vendor/pos-request-vendor.component';
import { ViewPosRequestComponent } from './pos-request-vendor/views/view-pos-request/view-pos-request.component';
import { PosRequestVendorFormComponent } from './pos-request-vendor/form/pos-request-vendor-form/pos-request-vendor-form.component';
import { RejectReasonDialogComponent } from './pos-request-vendor/form/reject-reason-dialog/reject-reason-dialog.component';
=======
import { WebSocketAPI } from './WebSocketAPI';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

>>>>>>> 57ee39f687fd438480e67563e084aadfaf9534bc

const appRoutes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "", component: LoginComponent },

  {
    path: "home", component: HomeComponent,
    canActivate: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: [
          'ROLE_PRODUCT', 'ROLE_SYSTEM_ADMIN', 'ROLE_PRIVILEGE_CENTER', 'ROLE_ORDER', 'ROLE_POS', 'ROLE_DASHBOARD',
          'ROLE_VENDOR', 'ROLE_CATEGORY', 'ROLE_VENDOR_ADMIN', 'ROLE_LOYALTY', 'ROLE_SETTING', 'ROLE_REWARD',
          'ROLE_COMBO', 'ROLE_COMMISSION', 'ROLE_SALES_SUPERVISOR', 'ROLE_APPLICATION', 'ROLE_POINTS', 'ROLE_POS_',
          'ROLE_ORDER', 'ROLE_SUBSCRIPTION', 'ROLE_COMMUNICATION_CENTER', 'ROLE_SURVEY', 'ROLE_SALES_AGENT',
          'ROLE_POS_GROUP_MANAGEMENT', 'ROLE_SUPPORT_TICKET', 'ROLE_SURVEY', 'ROLE_FCM_NOTIFICATION',
          'ROLE_TICKET_SUPPORT', 'ROLE_PROMOTION_ATTRIBUTE', 'ROLE_DISCOUNT', 'ROLE_BRAND', 'ROLE_SUPPORT_TICKET_TYPE','ROLE_WAREHOUSE_ADMIN'
        ],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "dashboard", component: DashboardComponent, children: [
      { path: "app-dashboard", component: DashboardComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_DASHBOARD'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "combo", component: ComboComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-combos" },
      { path: "form", component: ComboFormComponent },
      { path: "all-combos", component: ViewComboComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_COMBO'],
        redirectTo: '/login'
      }
    }
  },


  {
    path: "survey", component: SurveyComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-survey" },
      { path: "form", component: SurveyFormComponent },
      { path: "all-survey", component: SurveyViewComponent }
    ],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_SURVEY'],
        redirectTo: '/login'
      }
    }
  },{
    path: "notification",
    component: NotifiactionComponent,
    children: [
      { path: "", pathMatch: "full", redirectTo: "all-notifications" },
      { path: "form", component: NotificationFormComponent },
      { path: "all-notifications", component: NotificationViewComponent }
    ],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ["ROLE_NOTIFICATION"],
        redirectTo: "/login"
      }
    }
  },{
    path: "promotion_attribute", component: PromotionAttributeComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "home" },
      { path: "home", component: DashboardComponent }
    ],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_PROMOTION_ATTRIBUTE'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "loyalty", component: LoyaltyComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-loyalties" },
      { path: "form", component: LoyaltyFormComponent },
      { path: "all-loyalties", component: ViewLoyaltyComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_LOYALTY'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "ticket", component: SupportTicketComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-tickets" },
      { path: "all-tickets", component: ViewSupportTicketComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_TICKET_SUPPORT'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "ticket-type", component: ViewTicketTypeComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "view-ticket-type" }
    ],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_SUPPORT_TICKET_TYPE'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "fcm_notification", component: FcmComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "fcm_notification" },
      { path: "form", component: FcmComponent },
      { path: "all-pos-users", component: AllPosUsersComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_FCM_NOTIFICATION'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "reward", component: RewardComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-rewards" },
      { path: "form", component: RewardFormComponent },
      { path: "all-rewards", component: ViewRewardsComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_REWARD'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "letter", component: LetterComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-letters" },
      { path: "form", component: LetterFormComponent },
      { path: "all-letters", component: AllLettersComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_SETTING'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "subscription", component: SubscriptionComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-subscriptions" },
      { path: "form", component: SubscriptionFormComponent },
      { path: "all-subscriptions", component: AllSubscriptionsComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_SUBSCRIPTION'],
        redirectTo: '/login'
      }
    }
  },
  {
    path: "city", component: CityComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-cities" },
      { path: "all-cities", component: AllCitiesComponent }],
    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_SETTING'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "area", component: AreaComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-areas" },
      { path: "all-areas", component: AllAreasComponent }],
    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_SETTING'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "vendor", component: VendorComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-vendors" },
      { path: "form", component: VendorFormComponent },
      { path: "all-vendors", component: ViewVendor }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_VENDOR'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "pos_user", component: PosComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "pos-users" },
      { path: "search", component: PosSearchComponent },
      { path: "pos-form", component: PosFormComponent },
      { path: "pos-users", component: ViewPosFormComponent },
      { path: "all-assigned-poss", component: ViewAllPosComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_POS'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "pos-group-management", component: PosGroupManagementComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-pos-groups" },
      { path: "form", component: PosGroupManagementFormComponent },
      { path: "all-pos-groups", component: ViewPosGroupManagementComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_POS_GROUP_MANAGEMENT'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "orders", component: OrdersComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-orders" },
      { path: "form", component: OrdersFormComponent },
      { path: "all-orders", component: AllOrdersComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_ORDER'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "reasons", component: OrderRejectedReasonComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-reasons" },
      { path: "all-reasons", component: ViewOrderRejectedReason }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_ORDER_REJECTED_REASON'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "vendor-admins", component: VendorAdminComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-vendor-admins" },
      { path: "form", component: VendorAdminFormComponent },
      { path: "all-vendor-admins", component: AllVendorAdminsComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_VENDOR_ADMIN'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "sales-supervisors", component: SalesSupervisorComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-sales-supervisors" },
      { path: "form", component: SalesSupervisorFormComponent },
      { path: "all-sales-supervisors", component: AllSalesSupervisorsComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_SALES_SUPERVISOR'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "sales-agents", component: SalesAgentComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-sales-agents" },
      { path: "form", component: SalesAgentFormComponent },
      { path: "all-sales-agents", component: AllSalesAgentsComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_SALES_AGENT'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "system-admins", component: SystemAdminComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-system-admins" },
      { path: "form", component: SystemAdminFormComponent },
      { path: "all-system-admins", component: AllSystemAdminsComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_SYSTEM_ADMIN'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "product-category", component: ProductManagmentComponent, children: [
      { path: "", component: AllProducts },
      { path: "product-form", component: ProductForm }
    ],
    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_PRODUCT', 'ROLE_CATEGORY'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "category-management", component: CategoryManagmentComponent, children: [
      { path: "", component: CategoryTreeComponent },
      { path: "category-management-form", component: CategoryManagementForm },

    ],
    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_CATEGORY'],
        redirectTo: '/login'
      }
    }
  },


  {
    path: "promotion", component: PromotionComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "app-all-promotion-view" },
      { path: "form", component: PromotionFormComponent },
      { path: "app-all-promotion-view", component: AllPromotionViewComponent },
      { path: "promotion-product", component: ProductPromotionComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_COMMISSION'],
        redirectTo: '/login'
      }
    }
  },


  {
    path: "discount", component: DiscountComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "app-all-discounts-view" },
      { path: "form", component: DiscountFormComponent },
      { path: "app-all-discounts-view", component: AllDiscountsViewComponent }
    ],
    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_DISCOUNT'],
        redirectTo: '/login'
      }
    }
  },
  {
    path: "warehouse", component: WarehouseComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "warehouse-view" },
      { path: "form", component: WarehouseFormComponent },
      { path: "warehouse-view", component: ViewWarehouse }
    ],
    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_WAREHOUSE'],
        redirectTo: '/login'
      }
    }
  },
  {
    path: "warehouse-admin", component: WarehouseAdminComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "warehouse-admin-view" },
      { path: "form", component: WarehouseAdminFormComponent },
      { path: "warehouse-admin-view", component: ViewWarehouseAdmin }
    ],
    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['ROLE_WAREHOUSE_ADMIN'],
        redirectTo: '/login'
      }
    }
  },
  {
    path: "delivery-user", component: DeliveryUserComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "delivery-user-view" },
      { path: "form", component: DeliveryUserFormComponent },
      { path: "delivery-user-view", component: ViewDeliveryUser }
    ],
    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['ROLE_DELIVERY_USER'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "privilege", component: PrivilegeProfileComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "all-privilege-profiles" },
      { path: "form", component: PrivilegeProfileFormComponent },
      { path: "all-privilege-profiles", component: AllPrivilegeProfilesComponent }],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_PRIVILEGE_CENTER'],
        redirectTo: '/login'
      }
    }
  },

  {
    path: "brand", component: ViewBrandsComponent, children: [
      { path: "", pathMatch: "full", redirectTo: "app-view-brands" }
    ],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ['ROLE_BRAND'],
        redirectTo: '/login'
      }
    }
  },


  {path: "shipment_order", component:ShippmentOrderComponent , children: [
    {path: "",pathMatch:"full" ,redirectTo: "all-shipment" },
    {path: "shipment_item",component:ShipmentOrderItemComponent },
    // {path: "form",component:ShipmentOrderFormComponent},
    {path: "all-shipment", component:ShippmentOrderViewComponent }
    ],

    canActivate : [NgxPermissionsGuard],
    canActivateChild : [NgxPermissionsGuard],

    data : {
      permissions: {
        only:['ROLE_ORDER'],
        redirectTo: '/login'
      }
    }
  },


  {
    path: "vendor-ad",
    component: VendorAdMainroutingComponent,
    children: [
      { path: "", component: VendorAdAllComponent },
      {
        path: "vendor-ad-form",
        component: VendorAdFormComponent
      }
    ],

    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only: ["ROLE_AD"],
        redirectTo: "/login"
      }
    }
  },
  {
    path: "all-notifications",
    component: UserNotifiactionComponent,
    canActivate: [NgxPermissionsGuard],
    canActivateChild: [NgxPermissionsGuard],

    data: {
      permissions: {
        only:['ROLE_VIEW_NOTIFICATIONS'],
        redirectTo: "/login"
      }
    }
  
},

{
  path: "pos-requests", component: PosRequestVendorComponent, children: [
    { path: "", pathMatch: "full", redirectTo: "all-pos_requests-vendor" },
    { path: "search", component: PosSearchComponent },
    { path: "form", component: PosRequestVendorFormComponent },
    { path: "all-pos_requests-vendor", component: ViewPosRequestComponent }],

  canActivate: [NgxPermissionsGuard],
  canActivateChild: [NgxPermissionsGuard],

  data: {
    permissions: {
      only: ['ROLE_POS'],
      redirectTo: '/login'
    }
  }
}

]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    VendorAdminFormComponent,
    AllVendorAdminsComponent,
    VendorAdminComponent,
    SalesSupervisorFormComponent,
    AllSalesSupervisorsComponent,
    SalesSupervisorComponent,
    SalesSupervisorRegionComponent,
    SalesAgentFormComponent,
    ShippmentOrderComponent,
    ShipmentSerialDlg,
    ShipmentOrderItemComponent,
    AllSalesAgentsComponent,
    SalesAgentComponent,
    RejectedOrdersDlg,
    ShipmentOrderHistoryDlg,
    OrderStatusHistoryDlg,
    SalesAgentRegionComponent,
    HomeComponent,
    DashboardComponent,
    AllSystemAdminsComponent,
    SystemAdminFormComponent,
    SystemAdminComponent,
    AssignToDeliveryDlg,
    VendorComponent,
    VendorFormComponent,
    ViewAddress,
    ViewVendor,
    DialogAddresses,
    ShipmentOrderItemHistoryDialog,
    ShippmentOrderViewComponent,
    PromotionAttributeQtyAndValueDialog,
    DialogChangePosType,
    DialogComboInfo,
    DialogAssignedVendors,
    DialogSingleHistory,
    DialogComboHistory,
    DialogAddProductCombo,
    PosFormComponent,
    ViewPosFormComponent,
    PosComponent,
    ViewAssignedVendorFormComponent,
    ProductForm,
    OrdersComponent,
    OrdersFormComponent,
    AllOrdersComponent,
    AllOrderItemsComponent,
    AllSingleItemsComponent,
    AllComboItemsComponent,
    RelatedCategoriesAndProductsDialogComponent,
    RelatedItemsDialogComponent,
    PrivilegeProfileFormComponent,
    AllPrivilegeProfilesComponent,
    PrivilegeProfileComponent,
    ViewAllPosComponent,
    ViewDataPosComponent,
    LoyaltyComponent,
    RewardComponent,
    LoyaltyFormComponent,
    ViewLoyaltyComponent,
    RewardFormComponent,
    ViewRewardsComponent,
    AllModuleRolesByAppComponent,
    AllViewRolesForModulesComponent,
    AllBlockRolesForModulesComponent,
    LoginComponent,
    UserPrivilegeProfileComponent,
    SettingsComponent,
    SettingsFormComponent,
    ViewSettingsFormComponent,
    AllLettersComponent,
    LetterFormComponent,
    LetterComponent,
    ComboComponent,
    ComboFormComponent,
    ViewComboComponent,
    ViewComboInfoComponent,
    ViewProductLoyaltyComponent,
    SubscriptionComponent,
    SubscriptionFormComponent,
    AllSubscriptionsComponent,
    SubscriptionMinToMaxDialog,
    ViewProductRewardComponent,
    DialogProductReward,
    SupportTicketComponent,
    ViewSupportTicketComponent,
    SessionTimeOutDialog,
    DialogTicket,
    SurveyComponent,
    QuestionCheckBoxComponent,
    QuestionRadioComponent,
    QuestionFreeTextComponent,
    SurveyFormComponent,
    PosSurveyComponent,
    SurveyViewComponent,
    CheckBoxQuestionDialog,
    PromotionProductInformationDialog,
    PromotionProductsConfirmDialog,
    PromotionProductsInformationDialog,
    RadioQuestionDialog,
    PromotionQuantityDialog,
    PromotionAttributeDialog,
    AnswerRadioDialog,
    FreeTextQuestionDialog,
    CopySurveyDialog,
    FcmComponent,
    AllPosUsersComponent,
    AnswerDialog,
    ProductForm,
    AllProducts,
    NodeTreeComponent,
    CategoryManagmentComponent,
    CategoryManagementForm,
    CategoryTreeComponent,
    CategoryNodeTreeComponent,
    ProductManagmentComponent,
    PromotionComponent,
    AllPromotionViewComponent,
    PromotionFormComponent,
    ProductPromotionComponent,
    OrderPromotionComponent,
    PromotionDetailsDialog,
    PosGroupManagementComponent,
    PosGroupManagementFormComponent,
    ViewPosGroupManagementComponent,
    PromotionAttributeComponent,
    CityComponent,
    AreaComponent,
    AllCitiesComponent,
    AllAreasComponent,
    CityDialog,
    AreaDialog,
    DiscountComponent,
    AllDiscountsViewComponent,
    DiscountFormComponent,
    ProductDiscountComponent,
    ProductsHaveDiscountDialog,
    ConfrimationDialog,
    SingleProductHasDiscountDialog,
    PosSearchComponent,
    BrandComponent,
    ViewBrandsComponent,
    BrandDialog,
    RelatedProductDialogComponent,
    VendorAdMainroutingComponent,
    VendorAdFormComponent,
    VendorAdAllComponent,
    SupporticketTypeComponent,
    SupportTicketTypeDialog,
    ViewTicketTypeComponent,
    RelatedTicketsComponent,
    SelectCheckAllComponent,
    WarehouseComponent,
    WarehouseFormComponent,
    ViewWarehouse,
    WarehouseAdminFormComponent,
    ViewWarehouseAdmin,
    WarehouseAdminComponent,
    DeliveryUserFormComponent,
    ViewDeliveryUser,
    DeliveryUserComponent,
    DeliveryUserRegionComponent,
    ViewOrderRejectedReason,
    OrderRejectedReasonComponent,
    OrderRejectedReasonDialog,
    NotifiactionComponent,
    NotificationFormComponent,
    NotificationViewComponent ,
<<<<<<< HEAD
    UserNotifiactionComponent,
    PosRequestVendorComponent,
    ViewPosRequestComponent,
    PosRequestVendorFormComponent,
    RejectReasonDialogComponent 
  ],
=======
    UserNotifiactionComponent
    ],
>>>>>>> 57ee39f687fd438480e67563e084aadfaf9534bc
  imports: [
    BrowserModule,
    GMapModule,
    BrowserAnimationsModule,
    Orders360MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    CovalentFileModule, CovalentSearchModule, CovalentPagingModule,
    RouterModule.forRoot(appRoutes),
    NgxGalleryModule,
    NgxPermissionsModule.forRoot(),
    CovalentTextEditorModule,
    CovalentMessageModule,
    MatTreeModule,
    SelectButtonModule,
    MatProgressButtonsModule,
    MatSelectModule,
    NgxMatSelectSearchModule,
    MatSelectSearchModule,
    MatBadgeModule,
    MatMenuModule,
<<<<<<< HEAD
    MatDialogModule
    ],
=======
    AngularMultiSelectModule
  ],
>>>>>>> 57ee39f687fd438480e67563e084aadfaf9534bc
  providers: [
    VendorAdminService,
    SalesSupervisorService,
    SalesAgentService,
    IndustryService,
    RegionService,
    AssignToDeliveryDlg,
    CityService,
    CountryService,
    SystemAdminService,
    ShippmentOrderService,
    PromotionService,
    ShipmentOrderItemHistoryDialog,
    RejectedOrderService,
    OrderStatusService,
    CategoryService,
    RejectedOrdersDlg,
    TdDialogService,
    DialogAddresses,
    OrderStatusHistoryDlg,
    ShipmentOrderHistoryDlg,
    PromotionAttributeQtyAndValueDialog,
    DialogChangePosType,
    VendorAddressService,
    AssignedVendorsService,
    VendorService,
    ShipmentSerialDlg,
    PosService,
    SurveyService,
    PromotionItemService,
    QuestionService,
    AnswerService,
    ProductService,
    PriorityService,
    OrderService,
    OrderItemHistoryService,
    OrderItemService,
    AllOrdersComponent,
    TdDialogService,
    PrivilegeProfileService,
    ProductLoyaltyService,
    LoyaltyPointsTrxSourceService,
    LoyaltyPointsTrxService,
    ProductRewardService,
    LoyaltyService,
    RewardService,
    ApplicationRoleService,
    ModuleRoleService,
    BlockRoleService,
    ViewRoleService,
    ApplicationService,
    ModuleService,
    ViewService,
    BlockService,
    PrivilegeProfileModuleRoleService,
    PrivilegeProfileViewRoleService,
    PrivilegeProfileBlockRoleService,
    AuthService,
    UserPrivilegeProfileService,
    PosTypeService,
    SettingsService,
    LetterService,
    ComboService,
    ComboInfoService,
    SingleItemService,
    ComboItemService,
    DatePipe,
    SubscriptionService,
    SubscriptionTypeService,
    CheckBoxQuestionDialog,
    PromotionProductInformationDialog,
    PromotionProductsConfirmDialog,
    PromotionProductsInformationDialog,
    RadioQuestionDialog,
    PromotionQuantityDialog,
    PromotionAttributeDialog,
    AnswerRadioDialog,
    FreeTextQuestionDialog,
    CopySurveyDialog,
    AnswerDialog,
    SubscriptionMinToMaxDialog,
    PosSubscriptionService,
    UserTypeService,
    SupportTicketService,
    FcmService,
    DecimalPipe,
    CurrencyService,
    PowerBIService,
    PromotionDetailsDialog,
    CityService,
    CityService,
    PosGroupManagementService,
    DiscountService,
    DiscountProductsPosGroupsService,
    DeliveryStatusService,
    BrandService,
    TicketTypeService,
    WarehouseService,
    WarehouseAdminService,
    DeliveryUserSerivce,
    OrderRejectedReasonService,
    ProductPosGroupService,
    MessageService,
    NotificationService,
    UserNotificationService,
    WebSocketAPI,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    }
  ],

  entryComponents: [
    RelatedCategoriesAndProductsDialogComponent,
    RelatedItemsDialogComponent,
    CityDialog,
    AreaDialog,
    OrderStatusHistoryDlg,
    ProductsHaveDiscountDialog,
    ConfrimationDialog,
    AssignToDeliveryDlg,
    SingleProductHasDiscountDialog,
    DialogTicket,
    PromotionQuantityDialog,
    ShipmentSerialDlg,
    RejectedOrdersDlg,
    ShipmentOrderItemHistoryDialog,
    PromotionAttributeDialog,
    DialogProductReward,
    DialogAddresses,
    DialogChangePosType,
    DialogAssignedVendors,
    RelatedCategoriesAndProductsDialogComponent,
    DialogComboInfo,
    CheckBoxQuestionDialog,
    PromotionProductInformationDialog,
    PromotionProductsConfirmDialog,
    PromotionProductsInformationDialog,
    RadioQuestionDialog,
    ShipmentOrderHistoryDlg,
    AnswerRadioDialog,
    PromotionAttributeQtyAndValueDialog,
    FreeTextQuestionDialog,
    CopySurveyDialog,
    AnswerDialog, DialogSingleHistory,
    DialogComboHistory,
    SubscriptionMinToMaxDialog,
    PromotionDetailsDialog,
    DialogAddProductCombo,
    BrandDialog,
    RelatedProductDialogComponent,
    SupportTicketTypeDialog,
    RelatedTicketsComponent,
    OrderRejectedReasonDialog,
<<<<<<< HEAD
    RejectReasonDialogComponent
=======
    SessionTimeOutDialog
>>>>>>> 57ee39f687fd438480e67563e084aadfaf9534bc
  ],
  bootstrap: [AppComponent
  ]
})
export class AppModule { }
