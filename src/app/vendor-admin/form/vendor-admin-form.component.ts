import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from "@angular/material";
import { VendorAdmin } from "../vendor-admin";
import { VendorAdminService } from "../service/vendor-admin.service";
import { ActivatedRoute, Router } from '@angular/router';
import { VendorService } from '../../vendor/Services/vendor.service';
import { environment } from '../../../environments/environment';
import { Vendor } from '../../vendor/vendor.model';
import { UserPrivilegeProfileComponent } from '../../user-privilege/views/user-privilege-profile.component';
import { BaseUserManagementForm } from '../../user-management/base-user-management-form';
import { AuthService } from '../../auth/service/auth.service';
import { User } from '../../auth/user.model';

@Component({
  selector: 'vendor-admin-form',
  templateUrl: './vendor-admin-form.component.html',
  styleUrls: ['./vendor-admin-form.component.css']
})
export class VendorAdminFormComponent extends BaseUserManagementForm{
 
  @ViewChild("vendorAdminForm") vendorAdminForm : NgForm;
  vendorAdmin : VendorAdmin;
  _isEdit:boolean = false;
  listOfVendors:Vendor[];  
  _isUserLoggedInVendor:boolean = false;
  loggedInVendor:Vendor;

  @ViewChild("notSelectedPrivilegeProfiles")
  notSelectedPrivilegeProfiles:UserPrivilegeProfileComponent;

  @ViewChild("selectedPrivilegeProfiles")
  selectedPrivilegeProfiles:UserPrivilegeProfileComponent;

  constructor(protected vendorAdminService: VendorAdminService,
              protected authService:AuthService,
              private vendorService: VendorService,
              protected route:ActivatedRoute,
              protected router:Router,
              protected snackBar:MatSnackBar) { 
    super(vendorAdminService, 
          route,
          router,
          snackBar);
  }

  ngOnInit() {
   this.selectedSecreen= environment.ENUM_CODE_USER_TYPE.VendorAdmin;
   this.loggedUser = this.authService.getCurrentLoggedInUser();
    if(this.route.snapshot.queryParams["edit"] == '1') {
      this.isEdit = true;
      this.vendorAdmin = this.vendorAdminService.vendorAdmin;
    } else {
      this.vendorAdmin = new VendorAdmin();
      
    }
    this.loadVendorOptions();
  }

  loadVendorOptions() {
    let loggedInUser:User = this.authService.getCurrentLoggedInUser();

    if(loggedInUser != undefined) {
      if(loggedInUser.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
        this.vendorService.filterNotPageable("", "", "", "/vendor_by_vendor_admin_id/" + loggedInUser.id).subscribe(
            (data:Vendor) => { 
              data => this.loggedInVendor = data;
              if(data != null) {
                this.loggedInVendor = new Vendor(data['id'], data['name']);
              }
              this.onCompleteLoadingVendor(this.loggedInVendor);
            } 
        );
      } else if(loggedInUser.type.id == environment.ENUM_ID_USER_TYPE.TelephoenicAdmin) {
        this.vendorService.filterNotPageable("", "", "", "/vendor_active_list")
                      .subscribe((data:Vendor[]) => { 
                          this.listOfVendors = data; }
                      );
      }
    }
  }

  onCompleteLoadingVendor(loggedInVendor:Vendor) {
    if(loggedInVendor == undefined) {
      this._isUserLoggedInVendor = false;
      this.vendorService.filter(0, environment.maxListFieldOptions, "", "", "").subscribe(
          data => this.listOfVendors = data['content'] 
      );
    } else {
      this._isUserLoggedInVendor = true;
      this.vendorAdmin.vendor = loggedInVendor;
    }
  }

  getLeafVendorInstance() {
    let vendor = new Vendor();
    return vendor;
  }

  onSaveAndNewCompleted() {
    this.showSpinnerOnSaveAndNew = false;
    this.selectedPrivilegeProfiles.allModels = [];
    this.selectedPrivilegeProfiles.dataTable.refresh();
    
    this.getNotSelectedPrivilegeProfiles().filterPrivilegeProfileForUser(this.loggedUser.id, null, this.selectedSecreen);
    this.notSelectedPrivilegeProfiles.dataTable.refresh();

    if(this.isUserLoggedInVendor) {
      this.vendorAdmin.vendor = this.loggedInVendor;
    }
  }

  onBeforeSaveAndNew(vendorAdmin:VendorAdmin) {
    super.onBeforeSave(vendorAdmin);
    this.showSpinner = false;
    this.showSpinnerOnSaveAndNew = true;
    if(this.isUserLoggedInVendor) {
      this.vendorAdmin.vendor = this.loggedInVendor;
    }
  }

  onVendorChange(event){
    this.notSelectedPrivilegeProfiles.filterPrivilegeProfileForUser(this.loggedUser.id, null, this.selectedSecreen,event.value);
  }

  get isEdit() : boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model() : VendorAdmin {
    return this.vendorAdmin;
  }

  set model(vendorAdmin:VendorAdmin){
    this.vendorAdmin = vendorAdmin;
  }

  get isUserLoggedInVendor() : boolean {
    return this._isUserLoggedInVendor;
  }

  set isUserLoggedInVendor(isUserLoggedInVendor: boolean) {
    this._isUserLoggedInVendor = isUserLoggedInVendor;
  }
  
  initModel(): VendorAdmin {
    return new VendorAdmin();
  }
  
  ngFormInstance(): NgForm {
    return this.vendorAdminForm;
  }

  getSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.selectedPrivilegeProfiles;
  }
  getNotSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.notSelectedPrivilegeProfiles;
  }
}