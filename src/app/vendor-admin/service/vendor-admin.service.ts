import { BaseModel } from '../../common/model/base-model';
import { Injectable, ViewChild } from '@angular/core';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { VendorAdmin } from "../vendor-admin";
import { environment } from "../../../environments/environment";
import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class VendorAdminService extends BaseService {
  vendorAdmin: VendorAdmin;
  navigateToAfterCompletion: string;

  constructor(http: HttpClient,
    protected authService: AuthService) {
    super(http, "vendor_admin", authService)
  }

  getVendorByAdminType(userId: number): Observable<any> {
    let user = this.authService.getCurrentLoggedInUser();
    if (user.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin) {
      return this.getVendorByVendorAdminId(userId);
    } else if (user.type.code == environment.ENUM_CODE_USER_TYPE.WarehouseAdmin) {
      return this.getVendorByWarehouseUserId(userId);
    }
  }

  getVendorByWarehouseUserId(userId: number): Observable<any> {
    return this.http.get(environment.baseURL + "vendor_admin/warehouse_user/" + userId);
  }

  getVendorByVendorAdminId(vendorAdminId: number): Observable<any> {
    return this.http.get(environment.baseURL + "vendor_admin/vendor/" + vendorAdminId);
  }

  getAllVendorAdminsByVendorId(pageNumber: number,
    pageSize: number,
    vendorId: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string): Observable<any> {
    return this.http.get(environment.baseURL +"vendor_admin/vendoradmin_for_vendor/" +pageNumber + "/" +
      pageSize + "/" +
      vendorId, {
        params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
        headers: this.getBasicHeaders()
      });
  }






  get model() {
    return this.vendorAdmin;
  }
  set model(model: BaseModel) {
    this.vendorAdmin = <VendorAdmin>model;
  }
}