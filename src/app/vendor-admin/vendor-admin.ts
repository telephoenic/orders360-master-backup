import {BaseModel} from '../common/model/base-model';
import { Vendor } from '../vendor/vendor.model';
import { UserPrivilegeProfile } from '../user-privilege/user-privilege-profiles';
import { UserPrivilegeProfileAware } from '../user-management/user-privilege-profile-aware';
import { UserType } from '../lookups/user-type.model';

export class VendorAdmin extends UserPrivilegeProfileAware{
    
    id: number;
    name: string;
    email: string;
    mobileNumber: string;
    type: UserType;
    vendor:Vendor;
    inactive:boolean;
    privilegeProfiles:UserPrivilegeProfile[] = [];
    
    constructor (){
        super();

        this.vendor = new Vendor();
        this.type = new UserType(2); //TODO Change it//not eligible to use id to get the object
    }

}