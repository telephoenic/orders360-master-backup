import { BaseService } from '../../common/service/base.service';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';
import { BaseModel } from '../../common/model/base-model';
import { Observable } from 'rxjs/Observable';
import { DeliveryUser } from '../delivery-user.model';


@Injectable()
export class DeliveryUserSerivce extends BaseService {

    deliveryUser: DeliveryUser;
    navigateToAfterCompletion: string;

    constructor(http: HttpClient,
        protected authService: AuthService) {
        super(http, "delivery_user", authService)
    }

    public findDeliveryUserByVendor(vendorId: number, pageNumber: number, pageSize: number,
        searchTerm: string, sortBy: string, sortOrder: string): Observable<any> {
        return this.http.get<any>(environment.baseURL + "delivery_user/find-delivery-users/"
            + pageNumber + "/" + pageSize + "/" + vendorId, {
                params: this.prepareSearchParameters(searchTerm, sortBy, sortOrder),
            headers: this.getBasicHeaders()
        });
    }

    public deleteDeliveryUserOnRegionsWhereIdIn(deliveryUserIds: number[]): Observable<any> {
        return this.http.post<any>(environment.baseURL + "delivery-user-region/deassign-regions", deliveryUserIds);
    }

    public findDeliveryUserByVendorId(vendorId: number): Observable<any> {
        console.log(vendorId);
        return this.http.get(environment.baseURL + "delivery_user/find_delivery_user_by_vendorId/" + vendorId)
    }


    get model() {
        return this.deliveryUser;
    }
    set model(model: BaseModel) {
        this.deliveryUser = <DeliveryUser>model;
    }


}