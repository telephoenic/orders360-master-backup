import { DeliveryUser } from "./delivery-user.model";
import { Region } from "../location/region/Region.model";

export class DeliveryUserRegion{
    id: number;   
    inactive: boolean;
    deliveryUser:DeliveryUser;
    region:Region;
 
    constructor(id?:number,
                inactive?:boolean,
                deliveryUser?:DeliveryUser,
                region?:Region){
                    this.id = id;
                    this.inactive = inactive;
                    this.deliveryUser = deliveryUser;
                    this.region = region;
    }
}