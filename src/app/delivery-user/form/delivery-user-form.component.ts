import { BaseFormComponent } from "../../common/form/base-form-component";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar, MatOption } from "@angular/material";
import { NgForm, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../auth/service/auth.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { VendorService } from "../../vendor/Services/vendor.service";
import { Vendor } from "../../vendor/vendor.model";
import { Warehouse } from "../../warehouse/warehouse-model";
import { WarehouseService } from "../../warehouse/service/warehouse.service";
import { UserPrivilegeProfileComponent } from "../../user-privilege/views/user-privilege-profile.component";
import { UserPrivilegeProfileAware } from "../../user-management/user-privilege-profile-aware";
import { BaseUserManagementForm } from "../../user-management/base-user-management-form";
import { User } from "../../auth/user.model";
import { environment } from "../../../environments/environment";
import { Industry } from "../../common/industry/industry.model";
import { Currency } from "../../lookups/currency.model";
import { Country } from "../../location/country/country.model";
import { DeliveryUser } from "../delivery-user.model";
import { DeliveryUserSerivce } from "../services/delivery-user.service";
import { City } from "../../location/city/City.model";
import { CityService } from "../../location/city/service/city.service";
import { RegionService } from "../../location/region/service/region.service";
import { Region } from "../../location/region/Region.model";
import { DeliveryUserRegionComponent } from "../region/delivery-user-region.component";
import { DeliveryUserRegion } from "../delivery-user-region.model";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: "app-delivery-user",
  templateUrl: "./delivery-user-form.component.html",
  styleUrls: ["./delivery-user-form.component.css"]
})
export class DeliveryUserFormComponent extends BaseUserManagementForm {
  user: any;
  _isEdit: boolean = false;
  _vendorId: number;
  deliveryUser: DeliveryUser;
  loggedUser: User;
  listOfCities: City[] = [];
  cityIds: number[] = [];
  regex;
  minLength: number = 10;
  maxLength: number = 10;
  isSaveFormError: boolean = false;
  isRegionNotExist:boolean = false;
  @ViewChild("deliveryUserForm") deliveryUserForm: NgForm;

  @ViewChild("notSelectedPrivilegeProfiles")
  notSelectedPrivilegeProfiles: UserPrivilegeProfileComponent;

  @ViewChild("selectedPrivilegeProfiles")
  selectedPrivilegeProfiles: UserPrivilegeProfileComponent;

  @ViewChild("allNotSelectedDeliveryUserRegion")
  allNotSelectedDeliveryUserRegion: DeliveryUserRegionComponent;

  @ViewChild("allSelectedDeliveryUserRegion")
  allSelectedDeliveryUserRegion: DeliveryUserRegionComponent;

  constructor(
    protected deliveryUserService: DeliveryUserSerivce,
    protected authService: AuthService,
    protected vendorAdminService: VendorAdminService,
    protected warehouseService: WarehouseService,
    protected vendorService: VendorService,
    protected cityService: CityService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected regionService: RegionService,
    protected snackBar: MatSnackBar
  ) {
    super(deliveryUserService, route, router, snackBar);
  }

  ngOnInit() {
    this.selectedSecreen = environment.ENUM_CODE_USER_TYPE.DeliveryUser;
    this.loggedUser = this.authService.getCurrentLoggedInUser();
    this.getVendorIdByLoggedVenodrAdmin();
    if (this.route.snapshot.queryParams["edit"] == "1") {
      this._isEdit = true;
      this.deliveryUser = this.deliveryUserService.deliveryUser;
    } else {
      this.deliveryUser = new DeliveryUser();
      this.deliveryUser.name = "";
    }
  }

  getVendorIdByLoggedVenodrAdmin() {
    this.user = this.authService.getCurrentLoggedInUser();
    this.vendorAdminService
      .getVendorByVendorAdminId(this.user.id)
      .subscribe((vendorId: number) => {
        this._vendorId = vendorId;
        this.vendorService.findVendorById(this._vendorId).subscribe(
          (data: Vendor) => {
            this.deliveryUser.vendor = data;
           this.regex =  new RegExp(
              "^" +
                "(" +
                this.deliveryUser.vendor.country.mobileCode +
                ")" +
                "[0-9]{8,8}$"
            );
            if (!this.isEdit && !this.isSaveFormError) {
              this.deliveryUser.mobileNumber = "";
            }
          },
          error => {
            console.log(error);
          },
          () => {
            this.cityService.findCityByVendorId(this._vendorId).subscribe(
              data => {
                this.listOfCities = data;
                this.listOfCities.forEach(city => {
                  this.cityIds.push(city.id);
                });
              },
              error => {
                console.log(error);
              },
              () => {
                this.loadNotSelectedRegions();
                this.loadSelectedRegions();
              }
            );
          }
        );
      });
  }

  public options = function(option, value): boolean {
    if (value != null) {
      return option.id === value.id;
    }
  };

  onCityChange(event) {
    this.cityIds = [];
    if (event.value == "a") {
      this.listOfCities.forEach(city => {
        this.cityIds.push(city.id);
      });
    } else {
      this.cityIds.push(event.value);
    }
    this.loadNotSelectedRegions();
    this.allNotSelectedDeliveryUserRegion.dataTable.refresh();
  }

  loadNotSelectedRegions() {
    this.regionService
      .getNotSelectedRegionsForDeliveryUsers(this.cityIds, this.deliveryUser.id)
      .subscribe(data => {
        this.allNotSelectedDeliveryUserRegion.allModels = data;
      },
      (error)=>{console.log(error)},
      ()=>{
        this.allNotSelectedDeliveryUserRegion.allModels = 
        this.allNotSelectedDeliveryUserRegion.allModels
        .filter(obj =>!this.allSelectedDeliveryUserRegion.allModels.find(temp=>obj.id == temp.id))
      });
  }

  onSelectingRegion(event) {
    if (event.movedAsAssigned) {
      this.pushTo(this.allSelectedDeliveryUserRegion, event);
      this.removeFrom(this.allNotSelectedDeliveryUserRegion, event);
    } else {
      let deliveryUserRegionsList :number[] = [];
      if(this._isEdit){
        deliveryUserRegionsList.push(this.deliveryUser.id);
        event.rows.forEach(region =>{
          deliveryUserRegionsList.push(region.id);
        });
        this.deliveryUserService.deleteDeliveryUserOnRegionsWhereIdIn(deliveryUserRegionsList).subscribe(
            data=>{
              console.log(data);
            });
      }
      this.pushTo(this.allNotSelectedDeliveryUserRegion, event);
      this.removeFrom(this.allSelectedDeliveryUserRegion, event);
    }
    this.allSelectedDeliveryUserRegion.dataTable.refresh();
    this.allNotSelectedDeliveryUserRegion.dataTable.refresh();
  }

  loadSelectedRegions() {
    if (this.isEdit) {
      this.regionService
        .filterNotPageable(
          "",
          "",
          "",
          "/get-regions-for-delivery-users/" + this.deliveryUser.id
        )
        .subscribe((data: any[]) => {
          this.allSelectedDeliveryUserRegion.allModels = data;
        });
    }
  }

  warehouseUserNameChange(event) {
    this.deliveryUser.name = event.target.value.trim();
  }

  getSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.selectedPrivilegeProfiles;
  }
  getNotSelectedPrivilegeProfiles(): UserPrivilegeProfileComponent {
    return this.notSelectedPrivilegeProfiles;
  }
  onBeforeEdit(deliveryUser: DeliveryUser) {
    super.onBeforeEdit(deliveryUser);
    this.prepareModelProperties(deliveryUser);
    this.fillSelectedPrivilegeProfiles(deliveryUser);
  }
  onBeforeSave(deliveryUser: DeliveryUser) {
    super.onBeforeSave(deliveryUser);
    this.prepareModelProperties(deliveryUser);
    this.fillSelectedPrivilegeProfiles(deliveryUser);
  }

  onBeforeSaveAndNew(deliveryUser: DeliveryUser) {
    super.onBeforeSaveAndNew(deliveryUser);
    this.prepareModelProperties(deliveryUser);
    this.fillSelectedPrivilegeProfiles(deliveryUser);
  }

  prepareModelProperties(deliveryUser: DeliveryUser) {
    this.deliveryUser.deliveryUserRegions = [];
    this.allSelectedDeliveryUserRegion.allModels.forEach(region => {
      let deliveryUserRegion = new DeliveryUserRegion();

      deliveryUserRegion.region = new Region();
      deliveryUserRegion.region.name = region.name;
      deliveryUserRegion.region.id = region.id;
      this.deliveryUser.deliveryUserRegions.push(deliveryUserRegion);
    });
    if (this.allSelectedDeliveryUserRegion.allModels.length == 0) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open("At least one Area should be selected.", "close", {
        duration: 3000
      });

      throw Error("At least one Area should be selected.");
    }
  }

  resetAfterSaveNew() {
    this.deliveryUser = new DeliveryUser();
  }

  onSaveAndNewCompleted() {
    this.getNotSelectedPrivilegeProfiles().filterPrivilegeProfileForUser(
      this.loggedUser.id,
      null,
      this.selectedSecreen
    );
    super.onSaveAndNewCompleted();
    this.getVendorIdByLoggedVenodrAdmin();
    this.resetAfterSaveNew();
    this.selectedPrivilegeProfiles.allModels = [];
    this.allSelectedDeliveryUserRegion.allModels = [];
    this.selectedPrivilegeProfiles.dataTable.refresh();
    this.allSelectedDeliveryUserRegion.dataTable.refresh();
  }

  fillSelectedPrivilegeProfiles(userModel: UserPrivilegeProfileAware) {
    userModel.privilegeProfiles = this.getSelectedPrivilegeProfiles().allModels;
    if (userModel.privilegeProfiles.length === 0) {
      this.showSpinner = false;
      this.showSpinnerOnSaveAndNew = false;
      this.snackBar.open(
        "Delivery User must be have at least one profile privilege.",
        "Close",
        { duration: 3000 }
      );
      throw new Error(
        "Delivery User must be have at least one profile privilege"
      );
    }
  }
  onSaveCompleted() {
    super.onSaveCompleted();
    this.snackBar.open("Adding Delivery User successfully", "close", {
      duration: 3000
    });
  }
  onEditError(errorResponse: HttpErrorResponse) {
    super.onEditError(errorResponse);
    this.isSaveFormError = true;
    this.getVendorIdByLoggedVenodrAdmin();
  }
  onSaveError(errorResponse: HttpErrorResponse) {
    super.onSaveError(errorResponse);
    this.isSaveFormError = true;
    this.getVendorIdByLoggedVenodrAdmin();
  }

  onSaveAndNewError(errorResponse: HttpErrorResponse) {
    super.onSaveAndNewError(errorResponse);
    this.isSaveFormError = true;
    this.getVendorIdByLoggedVenodrAdmin();
  }

  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): DeliveryUser {
    return this.deliveryUser;
  }
  set model(deliveryUser: DeliveryUser) {
    this.deliveryUser = deliveryUser;
  }
  initModel(): DeliveryUser {
    return new DeliveryUser();
  }
  ngFormInstance(): NgForm {
    return this.deliveryUserForm;
  }
}
