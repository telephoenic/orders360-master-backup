import { BaseViewComponent } from "../../common/view/base-view-component";
import { TdDataTableSortingOrder, TdPagingBarComponent, TdDataTableComponent, ITdDataTableColumn, TdDialogService } from "@covalent/core";
import { OnInit, Component, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { AuthService } from "../../auth/service/auth.service";
import { VendorAdminService } from "../../vendor-admin/service/vendor-admin.service";
import { environment } from "../../../environments/environment";
import { DeliveryUser } from "../delivery-user.model";
import { DeliveryUserSerivce } from "../services/delivery-user.service";

@Component({
  selector: 'delivery-user-view',
  templateUrl: '../../delivery-user/views/delivery-user-view.component.html',
  styleUrls: ['../../delivery-user/views/delivery-user-view.component.css']
})
export class ViewDeliveryUser extends BaseViewComponent implements OnInit {

  private _currentPage: number = 1;
  private _fromRow: number = 1;

  private _allDeliveryUsers: DeliveryUser[] = [];
  private _errors: any = '';
  private _selectedRows: DeliveryUser[] = [];
  private _pageSize: number = 50;
  private _filteredTotal: number;
  private _sortBy: string = '';
  private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
  private _searchTerm: string = '';

  @ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
  @ViewChild('dataTable') dataTable: TdDataTableComponent;

  constructor(protected deliveryUserService: DeliveryUserSerivce,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    protected authService?: AuthService,
    protected vendorAdminService?: VendorAdminService) {
    super(deliveryUserService, router, dialogService, snackBar, authService, vendorAdminService);
  }

  columnsConfig: ITdDataTableColumn[] = [
    { name: 'name', label: 'Name', tooltip: "Name" },
    { name: 'email', label: 'Email', tooltip: "Email", width: 400 },
    { name: 'mobileNumber', label: 'Mobile', tooltip: "Mobile", width: 200 },
    { name: "inactive", label: "Active/Inactive", tooltip: "Active or Inactive", width: 100 },
    { name: 'id', label: 'Edit', tooltip: "Edit", sortable: false, width: 100 }
  ];


  ngOnInit() {
    super.ngOnInit();
  }

  filterByVendor(pageNumber: number,
    pageSize: number,
    searchTerm: string,
    sortBy: string,
    sortOrder: string,
    vendorId: number) {
        this.getDeliveryUsers();
  }

  public getDeliveryUsers(){
    let currentUser = this.vendorAdminService.getCurrentLoggedInUser();
    let vendorId;

    if (currentUser.type.code == environment.ENUM_CODE_USER_TYPE.VendorAdmin) {
      this.vendorAdminService.getVendorByVendorAdminId(currentUser.id)
        .subscribe(result => {
          vendorId = result;
          this.deliveryUserService.findDeliveryUserByVendor(vendorId, this._currentPage - 1, this.pageSize,
            this.searchTerm, this.sortBy, this.sortOrder).subscribe(
              data => {
                this._allDeliveryUsers = data['content'];
                this._filteredTotal = data['totalElements'];
              });
        });

    }
  }

  getPagingBar(): TdPagingBarComponent {
    return this.pagingBar;
  }
  getDataTable(): TdDataTableComponent {
    return this.dataTable;
  }

  public get currentPage(): number {
    return this._currentPage;
  }

  public set currentPage(value: number) {
    this._currentPage = value;
  }

  public get fromRow(): number {
    return this._fromRow;
  }

  public set fromRow(value: number) {
    this._fromRow = value;
  }

  public get allModels(): DeliveryUser[] {
    return this._allDeliveryUsers;
  }

  public set allModels(value: DeliveryUser[]) {
    this._allDeliveryUsers = value;
  }

  public get errors(): any {
    return this._errors;
  }

  public set errors(value: any) {
    this._errors = value;
  }

  public get selectedRows(): DeliveryUser[] {
    return this._selectedRows;
  }

  public set selectedRows(value: DeliveryUser[]) {
    this._selectedRows = value;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  public set pageSize(value: number) {
    this._pageSize = value;
  }

  public get filteredTotal(): number {
    return this._filteredTotal;
  }

  public set filteredTotal(value: number) {
    this._filteredTotal = value;
  }

  public get sortBy(): string {
    return this._sortBy;
  }

  public set sortBy(value: string) {
    this._sortBy = value;
  }

  public get sortOrder(): TdDataTableSortingOrder {
    return this._sortOrder;
  }

  public set sortOrder(value: TdDataTableSortingOrder) {
    this._sortOrder = value;
  }
  public get searchTerm(): string {
    return this._searchTerm;
  }

  public set searchTerm(value: string) {
    this._searchTerm = value;
  }
}