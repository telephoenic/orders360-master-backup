import { Category } from './category';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';

@Injectable()
export class ReturnsJsonArrayService {
  
  getTreeAsArrayOfNodes(_allCategories: Category[]): Observable<any> {
    console.log(Observable.of(_allCategories));
    return Observable.of(_allCategories);

  }
}