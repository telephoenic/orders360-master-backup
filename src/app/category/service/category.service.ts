import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseModel } from '../../common/model/base-model';
import { Http } from '@angular/http';
import { BaseService } from '../../common/service/base.service';
import { Category } from '../category';
import { AuthService } from '../../auth/service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CategoryService extends BaseService{
  category:Category;
  navigateToAfterCompletion: string;

  constructor(protected http:HttpClient,
              protected authService:AuthService) {
    super(http, "category", authService);
  }
  
  get model() {
    return this.category;
  }

  set model(model:BaseModel) {
    this.category = <Category> model;
  }

  getParentAsObject(id:number):Category {
    let parentCategory : Category;

    this.find(id).subscribe(data => {
      parentCategory = <Category>data;
      console.log(data);
    });

    return parentCategory;
  }

  // getCategoryAsJson(): Observable<any> {
  //   return this.http.get(environment.baseURL +this.serviceURL + "/findAllCategories",
  //     { headers: this.getBasicHeaders() });
  // }

  getCategoryAsJson(vendorId:number): Observable<any> {
    return this.http.get(environment.baseURL +this.serviceURL + "/all_vendor_category/"+vendorId,
      { headers: this.getBasicHeaders() });
  }

  getLeaveCategoryThatHasNoProduct(): Observable<any> {
    return this.http.get(environment.baseURL +this.serviceURL + "/findLeavesCategories",
      { headers: this.getBasicHeaders() });
  }

  findLeaveCategory(vendorId:number): Observable<any> {
    return this.http.get(environment.baseURL +this.serviceURL + "/find_leaves_categories/"+vendorId,
      { headers: this.getBasicHeaders() });
  }

  delete<T extends BaseModel>(systemAdmins: T[]) {
    return this.http.post(environment.baseURL + this.serviceURL + BaseService.URL_DELETE, 
                          JSON.stringify(systemAdmins), 
                          { headers:  this.getBasicHeaders() } );
  }
  changeStatusTo<T extends BaseModel>(status, Ids: number[]) {
    return this.http.post(environment.baseURL + this.serviceURL + BaseService.URL_STATUS+"/"+status, 
                          JSON.stringify(Ids), 
                          { headers:  this.getBasicHeaders() });
  }

  getAllCategoryListByVendor(vendorId:number):Observable<any>{
    return this.http.get(environment.baseURL+"category/active_categories/"+vendorId);

  }



}
