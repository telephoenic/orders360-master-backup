import {BaseModel} from '../common/model/base-model';
import { Vendor } from '../vendor/vendor.model';

export class Category extends BaseModel {
    id:number;
    inactive:boolean;
    name:string;
    description:string;
    parent:Category;
    hasChilds:boolean;
    childs:Category[];
    index: number;
    vendor:Vendor;

    constructor () {
        super();
        this.vendor=new Vendor();
    }
}