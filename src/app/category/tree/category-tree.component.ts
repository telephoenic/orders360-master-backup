import { RelatedCategoriesAndProductsDialogComponent } from '../views/related-categories-and-products-dialog/related-categories-and-products-dialog.component';
import { Category } from '../category';
import { CategoryService } from '../service/category.service';
import { ReturnsJsonArrayService } from './../returns-json-array.service';
import { Input, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Node } from '../node.class';
import { Product } from '../../product/product';
import { BaseViewComponent } from '../../common/view/base-view-component';
import { Router, ActivatedRoute } from '@angular/router';
import { ITdDataTableColumn, TdDialogService, TdDataTableComponent, TdPagingBarComponent, TdDataTableSortingOrder, TdDataTableService, IPageChangeEvent, ITdDataTableSortChangeEvent } from '@covalent/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../product/service/product.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { NgxPermissionsService } from 'ngx-permissions';
import { User } from '../../auth/user.model';
import { AuthService } from '../../auth/service/auth.service';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';
import { environment } from '../../../environments/environment';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	moduleId: module.id,
	selector: 'tree',
	templateUrl: 'category-tree.component.html',
	styleUrls: ['category-tree.component.css'],
	providers: [ReturnsJsonArrayService,
		TdDataTableService],
})


export class CategoryTreeComponent extends BaseViewComponent implements OnInit {
	@Input() panelOpenState = false;
	categorySatus: string = '';
	categoryName: string = '';
	parentCategory: string = '';
	countOfInit: number;
	count: number;
	@Input() tree: Observable<Array<Node>>;
	private _allCategories: Category[] = [];
	private node: Category = new Category();
	private _currentPage: number = 1;
	private _fromRow: number = 1;
	private user: User;
	public _allProducts: Product[] = [];
	private _errors: any = '';
	private _selectedRows: Product[] = [];
	public _pageSize: number;
	private _filteredTotal: number;
	private _sortBy: string = '';
	private _sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;
	private _searchTerm: string = '';
	public selectedNode: Category;
	public selectedNodes: Category[] = [];

	@ViewChild('pagingBar') pagingBar: TdPagingBarComponent;
	@ViewChild('dataTable') dataTable: TdDataTableComponent;

	categoryId: number;
	hasProduct: boolean;
	hasLeaf: boolean;
	vendorId: number;
	edit: number;
	categoryIdFromUrl: number;

	path: string;

	constructor(protected productService: ProductService,
		protected vendorAdminService: VendorAdminService,
		protected authService: AuthService,
		protected router: Router,
		protected dialogService: TdDialogService,
		protected snackBar: MatSnackBar,
		private matDialog: MatDialog,
		protected permissionsService: NgxPermissionsService,
		protected categoryService: CategoryService,
		protected jsonArrayService: ReturnsJsonArrayService, protected route: ActivatedRoute, ) {

		super(productService, router, dialogService, snackBar);

	}

	columnsConfig: ITdDataTableColumn[] = [
		{ name: 'barcode', label: 'Barcode', width: 130 },
		{ name: 'name', label: 'Name', width: 370 },
		{ name: 'price', label: 'Price', width: 70 },
		{ name: 'vendor.name', label: 'Vendor', width: 150 },
		{ name: 'category.name', label: 'Category', width: 150 },
		{ name: 'priority.description', label: 'Priority', width: 70 },
		{ name: "inactive", label: "Active/Inactive", width: 100, tooltip: "Active or Inactive" },
		{ name: 'id', label: 'Edit', width: 80 }
	];

	ngOnInit() {
		this.loadCategoriesAsTree();
		this.countOfInit = 0;
		this.count = 0;
		this.hasProduct = false;
		this.hasLeaf = false;
		//this.path = String(this.route.snapshot.queryParams["Path"]).replace("%20"," ");
		/*this.permissionsService.hasPermission("ROLE_PRODUCT_EDIT").then((result: boolean) => {
			if (!result) {
				for (let i = 0; i < this.dataTable.columns.length; i++) {
					if (this.dataTable.columns[i].name == 'id') {
						this.dataTable.columns.splice(i, 1);
					}
				}
			}
		});

		this.permissionsService.hasPermission("ROLE_PRODUCT_ACTIVATE_DEACTIVATE_PER_ROW").then((result: boolean) => {
			if (!result) {
				for (let i = 0; i < this.dataTable.columns.length; i++) {
					if (this.dataTable.columns[i].name == 'inactive') {
						this.dataTable.columns.splice(i, 1);
					}
				}
			}
		});


		this.pageSize = this.getPagingBar().pageSize;
		this.filter(0, this.pageSize);
		//this.filterProductsByUser();*/
		if (this.route.snapshot.queryParams["categoryId"] != undefined) {
			this.categoryIdFromUrl = this.route.snapshot.queryParams["categoryId"];
		}
	}

	page(pagingEvent: IPageChangeEvent): void {
		this.fromRow = pagingEvent.fromRow;
		this.currentPage = pagingEvent.page;
		this.pageSize = pagingEvent.pageSize;

		this.filter(pagingEvent.page - 1, this.pageSize);
	}

	filter(pageNumber: number, pageSize: number) {
		this.loggedUser = this.authService.getCurrentLoggedInUser();

		if (this.loggedUser.type.id == environment.ENUM_ID_USER_TYPE.TelephoenicAdmin) {
			this.baseService.filter(pageNumber,
				pageSize,
				this.searchTerm,
				this.sortBy,
				this.sortOrder.toString())
				.subscribe(data => {
					this.allModels = data['content'];
					this._allProducts = data['content'];
					this.filteredTotal = data['totalElements'];
				});
		} else if (this.loggedUser.type.id == environment.ENUM_ID_USER_TYPE.VendorAdmin) {
			this.vendorAdminService.getVendorByVendorAdminId(this.loggedUser.id).subscribe(vendorId => {
				this.vendorId = vendorId;
				this.filterByVendor(pageNumber, pageSize, this.searchTerm, this.sortBy, this.sortOrder, vendorId);
			});


		}
	}


	filterByVendor(pageNumber: number, pageSize: number,
		searchTerm: string, sortBy: string, sortOrder: string, vendorId: number) {
		if (this.route.snapshot.queryParams["categoryId"] != undefined) {

			this.categoryIdFromUrl = this.route.snapshot.queryParams["categoryId"];
			this.categoryId = this.categoryIdFromUrl;
		}
		this.productService.getProductsByVendorCategoryId(pageNumber, pageSize, vendorId, this.categoryId, searchTerm, sortBy, sortOrder).subscribe(
			data => {
				this.allModels = data["content"];
				this._allProducts = data["content"];
				this.filteredTotal = data["totalElements"]
			}
		);
	}

	sort(sortEvent: ITdDataTableSortChangeEvent): void {
		this.sortBy = sortEvent.name;
		this.sortOrder = sortEvent.order;

		this.filter(0, this.pageSize);
	}

	loadCategoriesAsTree() {
		var c = { id: 0, name: "Parent Category", children: [] };
		let user: User = this.authService.getCurrentLoggedInUser();
		this.vendorAdminService.getVendorByVendorAdminId(user.id).subscribe(vendorId => {
			this.categoryService.getCategoryAsJson(vendorId).subscribe(
				data => {
					this._allCategories = data;
					var idToNodeMap = {};
					var root = [];

					root.push(c);
					for (var i = 0; i < data.length; i++) {
						var category = data[i];
						category.index = i;
						var parentNode = data[i];
						category.children = [];
						idToNodeMap[category.id] = category;

						if (typeof category.parent === "undefined" || category.parent === null) {
							parentNode = c;
							category.parent = c;
							parentNode.children.push(category);


						} else {
							parentNode = idToNodeMap[category.parent.id];
							parentNode.children.push(category);

						}
					}

					//	c.children.push(root);
					//	root2.push(c)
					this._allCategories = root;
					this.tree = this.jsonArrayService.getTreeAsArrayOfNodes(this._allCategories);
					this.countOfInit = data.length;
				}
			);


		})
	}

	getPagingBar(): TdPagingBarComponent {
		return this.pagingBar;
	}
	getDataTable(): TdDataTableComponent {
		return this.dataTable;
	}

	public get currentPage(): number {
		return this._currentPage;
	}

	public set currentPage(value: number) {
		this._currentPage = value;
	}

	public get fromRow(): number {
		return this._fromRow;
	}

	public set fromRow(value: number) {
		this._fromRow = value;
	}

	public get allModels(): Product[] {
		return this._allProducts;
	}

	public set allModels(value: Product[]) {
		this._allProducts = value;
	}

	public get errors(): any {
		return this._errors;
	}

	public set errors(value: any) {
		this._errors = value;
	}

	public get selectedRows(): Product[] {
		return this._selectedRows;
	}

	public set selectedRows(value: Product[]) {
		this._selectedRows = value;
	}

	public get pageSize(): number {
		return this._pageSize;
	}

	public set pageSize(value: number) {
		this._pageSize = value;
	}

	public get filteredTotal(): number {
		return this._filteredTotal;
	}

	public set filteredTotal(value: number) {
		this._filteredTotal = value;
	}

	public get sortBy(): string {
		return this._sortBy;
	}

	public set sortBy(value: string) {
		this._sortBy = value;
	}

	public get sortOrder(): TdDataTableSortingOrder {
		return this._sortOrder;
	}

	public set sortOrder(value: TdDataTableSortingOrder) {
		this._sortOrder = value;
	}

	public get searchTerm(): string {
		return this._searchTerm;
	}

	public set searchTerm(value: string) {
		this._searchTerm = value;
	}

	onNewCategory() {
		this.hasProduct = this.route.snapshot.queryParams["hasProducts"];
		if (this.hasProduct == true) {
			this.snackBar.open("you cannot add category ! there are products under this category   ", "Close", { duration: 5000 })
			this.router.navigate(['category-management']);
		} else {
			if (this.categoryIdFromUrl == undefined) {
				this.parentCategory = "Path";
				this.snackBar.open("No category selected   ", "Close", { duration: 5000 })
				this.parentCategory = "Path";
				this.router.navigate(['category-management']);
			} else {
				this.path = String(this.route.snapshot.queryParams["Path"]).replace("%20", " ");
			}
		}



		//	this.router.navigate(["category-management-form"], { queryParams: { categoryId: this.route.snapshot.queryParams["categoryId"], Path: this.route.snapshot.queryParams["Path"] } });
	}

	//edit
	onEditCategory() {
		this.categoryIdFromUrl = this.route.snapshot.queryParams["categoryId"];
		if (this.categoryIdFromUrl == undefined) {
			this.snackBar.open("No category selected   ", "Close", { duration: 5000 })
			this.router.navigate(['category-management']);
			this.parentCategory = "Path";
			this.path = " ";
		} else {
			this.path = String(this.route.snapshot.queryParams["Path"]).replace("%20", " ");
			this.parentCategory = this.path;
		}

		if (this.categoryIdFromUrl == 0) {
			this.snackBar.open(" you cannot edit Parent Category ", "close", { duration: 3000 });
			this.router.navigate(['category-management'], { queryParams: { categoryId: this.categoryIdFromUrl } });

		} else {
			this.baseService.navigateToAfterCompletion = this.router.url;
		}

	}

	//delete
	onDeleteCategory() {
		if (this.route.snapshot.queryParams["categoryId"] != undefined) {
			this.categoryIdFromUrl = this.route.snapshot.queryParams["categoryId"];
			this.categoryId = this.categoryIdFromUrl
		}

		if (this.categoryId != undefined && this.categoryId != 0) {
			// if (this.hasProduct == true || this.hasLeaf == true) {
			// 	this.snackBar.open("You cant delete this category beacause there are Product(s)/Categories  under it  ", "Close", { duration: 3000 });
			// } else {
			this.dialogService.openConfirm({
				message: "Are you sure you want to delete the selected Category?",
				title: "confirmation",
				cancelButton: "Disagree",
				acceptButton: "Agree"
			})
				.afterClosed().subscribe((accept: boolean) => {
					if (accept) {


						this.categoryService.delete(this.selectedNodes).subscribe(
							data => {
								this.onAfterDeleteCategory();
								this.snackBar.open("Deleted successfully", "Close", { duration: 3000 });
							},
							(errorResponse: HttpErrorResponse) => {
								this.onDeleteFailedCategory(errorResponse);
							}
						)
					}
				});
			// }
		} else {
			if (this.categoryId != 0) {
				this.onNoSelectedCategoryToDelete();
			} else {
				this.snackBar.open("you cannot delete Parent Category", "close", { duration: 5000 });
				this.router.navigate(['category-management'], { queryParams: { categoryId: this.categoryIdFromUrl } });
			}
		}
	}
	onAfterDeleteCategory() {
		this.loadCategoriesAsTree();
		this.parentCategory = "Path";
		this.categoryName = "Category Name";
		this.categorySatus = "Category Satus";
		this.path = "path";
		this.router.navigate(['category-management']);


	}
	onDeleteFailedCategory(errorResponse: HttpErrorResponse) {
		this.snackBar.open(errorResponse.error["message"], "More", { duration: 10000 })
			.onAction()
			.subscribe(() => this.showDetails(errorResponse));
	}
	showDetails(errorResponse: HttpErrorResponse) {
		let childCategoriesObject: any[] = errorResponse.error['extraInfo']['CATEGORY_PARENT_CHILD'];
		let relatedProductsObject: any[] = errorResponse.error['extraInfo']['CATEGORY_RELATED_PRODUCTS'];

		this.matDialog.open(RelatedCategoriesAndProductsDialogComponent,
			{ data: { relatedProducts: relatedProductsObject, childCategories: childCategoriesObject } });

	}

	onNoDataFoundToDeleteCategory() {
		this.snackBar.open("No data found.", "close", { duration: 3000 });
	}

	onNoSelectedCategoryToDelete() {
		this.snackBar.open("No selected Category.", "close", { duration: 3000 });
	}

	//activate and deactivate

	onActivateDeactivateCategory(activate: boolean) {
		if (this.route.snapshot.queryParams["categoryId"] != undefined) {
			this.categoryIdFromUrl = this.route.snapshot.queryParams["categoryId"];
			this.categoryId = this.categoryIdFromUrl
		}

		if (this.categoryId != undefined && this.categoryId != 0) {
			this.dialogService.openConfirm({
				message: "Are you sure you want to " + (activate ? "activate" : "deactivate") + " the selected row(s)?",
				title: "confirmation",
				cancelButton: "Disagree",
				acceptButton: "Agree"
			})
				.afterClosed().subscribe((accept: boolean) => {
					if (accept) {
						let selectedIDs: number[] = [];

						this.selectedNodes.forEach(catgory => {
							selectedIDs.push(catgory.id);
						});

						this.categoryService.changeStatusTo(activate, selectedIDs).subscribe(
							data => {

								this.snackBar.open((activate ? "Activated" : "Deactivated") + " successfully", "Close", { duration: 3000 });
								this.categorySatus = activate ? "Activate" : "inactivate";
								this.loadCategoriesAsTree();
								this.parentCategory = "Path";
								this.categoryName = "Category Name";
								this.categorySatus = "Category Satus";
								this.path = "path";
								this.router.navigate(['category-management']);
							},
							(errorResponse: HttpErrorResponse) => {
								if (activate) {
									this.onActivationFailed(errorResponse);
								} else {
									this.onDeleteFailedCategory(errorResponse);
								}
							}
						)
					}
				});
			//this.loadCategoriesAsTree();
		} else {
			if (this.categoryIdFromUrl == 0) {
				this.snackBar.open("you cannot Activate /Deactivate Parent Category", "close", { duration: 5000 });
				this.router.navigate(['category-management']);
			} else {

				this.onNoSelectedCategoryToActivateOrDeactivate(activate);
			}
		}
	}
	onNoSelectedCategoryToActivateOrDeactivate(activate: boolean) {
		this.snackBar.open("No selected Category.", "close", { duration: 3000 });
	}

	//======================product 
	onActivateDeactivateSelectedRows(activate: boolean) {

		if (this.selectedRows.length > 0) {
			this.dialogService.openConfirm({
				message: "Are you sure you want to " + (activate ? "activate" : "deactivate") + " the selected row(s)?",
				title: "confirmation",
				cancelButton: "Disagree",
				acceptButton: "Agree"
			})
				.afterClosed().subscribe((accept: boolean) => {
					if (accept) {
						let selectedIds: number[] = [];

						this.selectedRows.forEach(systemAdmin => {
							selectedIds.push(systemAdmin.id);
						});

						this.baseService.changeStatusTo(activate, selectedIds).subscribe(
							data => {
								this.snackBar.open((activate ? "Activated" : "Deactivated") + " successfully", "Close", { duration: 3000 });
								this.selectedRows = [];
								this.filter(this.currentPage - 1, this.pageSize);
								this.router.navigate(["category-management"], { queryParams: { categoryId: this.route.snapshot.queryParams["categoryId"] } });
							},
							(errorResponse: HttpErrorResponse) => {
								if (activate) {
									this.onActivationFailed(errorResponse);
									this.router.navigate(["category-management"]);
								} else {
									this.onDeactivationFailed(errorResponse);
									this.router.navigate(["category-management"]);
								}

							}
						)
					}
					//disagree
					this.router.navigate(["category-management"], { queryParams: { categoryId: this.route.snapshot.queryParams["categoryId"] } });
				});
		} else if (this.allModels.length == 0) {
			this.onNoDataFoundToActivateOrDeactivate(activate);
		} else {
			this.onNoSelectedRowsToActivateOrDeactivate(activate);
		}
	}

	onNoDataFoundToActivateOrDeactivate(activate: boolean) {
		this.snackBar.open("No category selected.", "close", { duration: 3000 });
		this.parentCategory = "Path";
	}

	onNoSelectedRowsToActivateOrDeactivate(activate: boolean) {
		this.snackBar.open("No selected rows.", "close", { duration: 3000 });
	}

	onActivationFailed(errorResponse: HttpErrorResponse) {
		this.snackBar.open("Activation process failed. Please try again. ", "close", { duration: 3000 });
	}

	onNew() {

		if (this.hasLeaf == true) {
			this.snackBar.open("you cannot add Product ! there are categories under this category   ", "Close", { duration: 5000 })
			this.router.navigate(['category-management']);
		} else {
			if (this.categoryIdFromUrl == undefined || this.categoryId == undefined) {
				this.snackBar.open("No selected Category.", "close", { duration: 5000 });
				this.snackBar.open("No selected Category.", "close", { duration: 5000 });
				this.router.navigate(['category-management']);
			}

		}

	}
	onAfterDelete() {
		this.filter(this.currentPage - 1, this.pageSize);

	}
	onNoDataFoundToDelete() {
		this.snackBar.open("No selected rows.", "close", { duration: 3000 });
	}

	/*onDeactivationFailed(errorResponse: HttpErrorResponse) {
		this.snackBar.open(errorResponse.error.message, "More", { duration: 10000 })
			.onAction()
			.subscribe(() => this.showDetails(errorResponse));
	}

	showDetails(errorResponse: HttpErrorResponse) {
		let childCategoriesObject: any[] = errorResponse.error['extraInfo']['CATEGORY_PARENT_CHILD'];
		let relatedProductsObject: any[] = errorResponse.error['extraInfo']['CATEGORY_RELATED_PRODUCTS'];

		this.matDialog.open(RelatedCategoriesAndProductsDialogComponent,
			{ data: { relatedProducts: relatedProductsObject, childCategories: childCategoriesObject } });

	}*/


}
