import { ReturnsJsonArrayService } from '../returns-json-array.service';
import { environment } from '../../../environments/environment';
import { CategoryService } from '../service/category.service';
import { Category } from '../category';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BaseFormComponent } from '../../common/form/base-form-component';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { BaseModel } from '../../common/model/base-model';
import { HttpErrorResponse } from '@angular/common/http';
import { CategoryTreeComponent } from '../tree/category-tree.component';
import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';
import { User } from '../../auth/user.model';
import { AuthService } from '../../auth/service/auth.service';

@Component({
  moduleId: module.id,
  selector: 'category-management-form',
  templateUrl: 'category-management-form.component.html',
  styleUrls: ['category-management-form.component.scss'],
  providers: [CategoryTreeComponent, ReturnsJsonArrayService]
})
export class CategoryManagementForm extends BaseFormComponent implements OnInit {
  selected :string ="" ;

  @ViewChild("categoryAddEditForm") categoryAddEditForm: NgForm;
  _category: Category;
  _selectedParentCategory: Category = new Category();
  _isEdit: boolean = false;
  listOfCategories: Category[];
  edit: number;
  path: string;
  parentCategory: Category = new Category;
  constructor(protected categoryService: CategoryService, private categoryTreeComponent: CategoryTreeComponent,
    protected route: ActivatedRoute,
    protected router: Router,
    protected snackBar: MatSnackBar,
    protected vendorAdminService:VendorAdminService,
    protected authService: AuthService,) {

    super(categoryService,
      route,
      router,
      snackBar);
  }


  ngOnInit() {
    this.parentCategory.id = 0;
    this.parentCategory.name = "Parent Category";
    this.parentCategory.parent = null;

    if (this.route.snapshot.queryParams["categoryId"] != 0 && this.route.snapshot.queryParams["categoryId"] != undefined) {
      this.categoryService.find(parseInt(this.route.snapshot.queryParams["categoryId"])).subscribe(
        data => {
          this._selectedParentCategory.id = data["id"];
          this._selectedParentCategory.name = data["name"];
          this._selectedParentCategory.description = data["description"];
          this._selectedParentCategory.hasChilds = data["hasChilds"];
          if (data["parent"]!=null){
          this._selectedParentCategory.parent = data["parent"];
          }
          
        }
      );
    }


   /* if (this.route.snapshot.queryParams["edit"] == '1') {
      this._isEdit = true;
      this._category = this._selectedParentCategory;
    } else {
      this._category = new Category();
      this._category.parent = this._selectedParentCategory;
    }*/
    if(this.route.snapshot.queryParams["edit"] == '1') {
      this._isEdit = true;
      this._category = this._selectedParentCategory ;
      this.loadAllCategoryOptions();
    } else {
      this._category = new Category();
      this._category.parent =new Category() ;
      this.categoryService.find(parseInt(this.route.snapshot.queryParams["categoryId"])).subscribe(
        data =>{
          this._category.parent.id =  data["id"] ;
          this._category.parent.name= data["name"]
        });
     
      this.loadAllCategoryOptions();
    }
if (this._category!=undefined){
    if (this._category.parent == null) {
      this._category.parent = new Category();
    }
  }
 

  }
  loadAllCategoryOptions() {
    let user:User=this.authService.getCurrentLoggedInUser();
    this.vendorAdminService.getVendorByVendorAdminId(user.id).subscribe(vendorId=>{
      this.categoryService.getAllCategoryListByVendor(vendorId).subscribe(data=>{
        this.listOfCategories=data;
      });
    });
 

    // this.categoryService.filterNotPageable("", "", "", "/all")
    //   .subscribe(data => this.listOfCategories = data['content']);
  }
 /* loadCategoryOptions() {

    this.categoryService.filterNotPageable("", "", "", "/all")
      .subscribe(data => this.listOfCategories = data['content']);
  }*/
  loadCategoryOptions() {

    this.categoryService.getLeaveCategoryThatHasNoProduct()
      .subscribe(data => {
        this.listOfCategories = data
      }
      );
  }

  get isEdit(): boolean {
    return this._isEdit;
  }

  set isEdit(isEdit: boolean) {
    this._isEdit = isEdit;
  }

  get model(): Category {
    return this._category;
  }

  set model(category: Category) {
    this._category = category;
  }

  initModel(): Category {
    let category = new Category();
    category.parent = new Category();

    return category;
  }

  ngFormInstance(): NgForm {
    return this.categoryAddEditForm;
  }

  onSave() {
    if (this._selectedParentCategory.id == 0) {
      this._selectedParentCategory = null;
    }
    this.doValidationThenCall(super.onSave);
  }
  onSaveCompleted() {
		this.categoryTreeComponent.parentCategory = "Path";
		this.categoryTreeComponent.categoryName = "Category Name";
		this.categoryTreeComponent.categorySatus = "Category Satus";
		this.path = "path";

    this.path = this.route.snapshot.queryParams["Path"];
    this.categoryTreeComponent.parentCategory = this.path;

    //this.selected
    this.router.navigate(["category-management"], { queryParams: { categoryId: this.route.snapshot.queryParams["categoryId"], Path: this.route.snapshot.queryParams["Path"] } });
  }

  onSaveAndNew() {
    let user:User = new User();
    user=this.authService.getCurrentLoggedInUser();
    this.vendorAdminService.getVendorByVendorAdminId(user.id).subscribe(vendorId=>{
      this._category.vendor.id=vendorId
      if (this._category.parent.id == undefined) {
        this._category.parent = new Category();
      }
      this.doValidation();
    });
    
  }

  onEditCompleted() {
    this.path = this.route.snapshot.queryParams["Path"];
    this.categoryTreeComponent.parentCategory = this.path;
    this.router.navigate(["category-management"], { queryParams: { categoryId: this.route.snapshot.queryParams["categoryId"], Path: this.route.snapshot.queryParams["Path"] } });
  }

  doValidationThenCall(saveFunction: Function) {
    let user:User = new User();
    user=this.authService.getCurrentLoggedInUser();
    this.vendorAdminService.getVendorByVendorAdminId(user.id).subscribe(vendorId=>{
      if (this.isParentEmpty()) {
        this._category.parent = new Category();
        this._category.vendor.id=vendorId;
        saveFunction.call(this);
      } else {
        let sameParent: boolean = this.isSameCategoryChosenAsParent();
        if (sameParent) {
          this.snackBar.open("The same category can not be parent to itself", "Close", { duration: 3000 });
        } else {
          this._category.vendor.id=vendorId;
          saveFunction.call(this);
  
        }
      }

    })
    
  }
  doValidation() {
    let sameParent: boolean = this.isSameCategoryChosenAsParent();
    if (sameParent) {
      this.snackBar.open("The same category can not be parent to itself", "Close", { duration: 3000 });
    } else {
      this.onBeforeSaveAndNew(this.model);
      this.baseService.save(this.model).subscribe(
        data => {
          this.model = this.initModel();
          this.snackBar.open("Saved successfully", "Close", { duration: 3000 });

        },
        (errorResponse: HttpErrorResponse) => {
          this.onSaveAndNewError(errorResponse);
        },
        () => this.onSaveAndNewCompleted()
      );

      this.onAfterSaveAndNew(this.model);
    }

  }

  onSaveAndNewCompleted() {
    this._category.parent = this._selectedParentCategory;
    this.showSpinnerOnSaveAndNew = false;

  }

  isParentEmpty(): boolean {
    return this._category.parent.id == null;
  }

  isSameCategoryChosenAsParent(): boolean {
    if (this.isEdit) {
      if (this._category.parent.id == this._category.id) {
        return true
      }
    }

    return false;
  }
  onCancel() {
    this.router.navigate(['category-management']);
  }
}