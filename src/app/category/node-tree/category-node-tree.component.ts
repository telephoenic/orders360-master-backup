
import { AfterViewInit } from '@angular/core';

import { Product } from '../../product/product';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../service/category.service';

import { Subscription } from '../../subscription/subscription.model';
import { ProductService } from '../../product/service/product.service';

import { VendorAdminService } from '../../vendor-admin/service/vendor-admin.service';
import { AuthService } from '../../auth/service/auth.service';
import { NgxPermissionsService } from 'ngx-permissions';
import { MatSnackBar, MatDialog } from '@angular/material';
import { PosService } from '../../pos-user/service/pos.service';
import { Component, OnInit, Input } from '@angular/core';
import { Node } from '../node.class';
import { Router } from '@angular/router';
import { TdDialogService } from '@covalent/core';
import { Inject, ElementRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Category } from '../category';
import { CategoryTreeComponent } from '../tree/category-tree.component';


@Component({
  selector: 'node-tree',
  templateUrl: 'category-node-tree.component.html',
  styleUrls: ['category-node-tree.component.css'],

})

export class CategoryNodeTreeComponent implements OnInit, AfterViewInit {


  @Input() node: Node;
  @Input() parent: Node;
  @Input() firstNode;
  @Input() lastNode;
  inactive = false;
  returnSelectedNode: boolean = false;
  public _allProducts: Product[] = [];
  constructor(@Inject(DOCUMENT) private document,
    private elementRef: ElementRef,
    protected posService: PosService,
    protected router: Router,
    protected dialogService: TdDialogService,
    protected snackBar: MatSnackBar,
    private matDialog: MatDialog,
    protected permissionsService: NgxPermissionsService,
    protected athuService: AuthService,
    protected vendorAdminService: VendorAdminService,
    private productService: ProductService,
    private treeComponent: CategoryTreeComponent,
    private categoryService: CategoryService, protected route: ActivatedRoute) {
  }

  ngOnInit() {
    this.loadNode();
    this.returnSelectedNode = false;
    if (this.route.snapshot.queryParams["categoryId"] != null) {
      this.categoryService.find(this.route.snapshot.queryParams["categoryId"]).subscribe(
        data => {
          if (data!=null){
          this.treeComponent.selectedNode = new Category();
          this.treeComponent.categorySatus= data["inactive"]  == true ? "inactive" : "active";
          this.treeComponent.categoryName = data["name"];
          }
        });
    }
    if (this.route.snapshot.queryParams["Path"] != null){
      this.treeComponent.parentCategory = String(this.route.snapshot.queryParams["Path"] ).replace('%20' ,'');
    } 
  }


  ngAfterViewInit(): void {
    this.treeComponent.count = this.treeComponent.count + 1;
    // every  node call ngInit fun ,count of init = categories length 
    // you cannot select Node unless all node loaded 
    if (this.treeComponent.count == this.treeComponent.countOfInit) {
      if (this.treeComponent.categoryIdFromUrl != undefined) {
        this.returnSelectedNode = true;
      //  this.selectNode();
      }
      // }
    }
    // }
  }
  selectNode() {
    if (this.treeComponent.categoryIdFromUrl != undefined && this.returnSelectedNode == true) {
      this.node.id = this.treeComponent.categoryIdFromUrl;
    }
    if(!document.getElementsByClassName("btn" + this.node.id)[0].classList.contains("active")){
    document.getElementsByClassName("btn" + this.node.id)[0].className += " active";
    }
    var btn = document.getElementsByClassName("btn" + this.node.id)[0];
    var actives = document.getElementsByClassName("active");

    for (var x = 0; x < (actives.length); x++) {
      if (actives[x] !== btn)
        actives[x].className = actives[x].className.replace(" active", "");
    }
    this.returnSelectedNode == false;
  }


  highlightSelectedNode() {

    if (this.treeComponent.categoryIdFromUrl != undefined) {
      this.selectNode();
    }
    this.treeComponent.selectedNodes = [];
    this.treeComponent.categoryId = this.node.id;
    var hasProducts;
    this.treeComponent.countOfInit = 0;
    this.treeComponent.hasLeaf = this.hasLeaf();
    if (this.treeComponent.categoryIdFromUrl == undefined) {
      this.treeComponent.categoryIdFromUrl = this.node.id;
      this.treeComponent.categoryId = this.node.id;
    }
    this.treeComponent.hasProduct = this.hasLeaf();
    this.productService.setCategoryId(this.node.id);
    this.treeComponent.categorySatus = this.node.inactive == true ? "inactive" : "active";

    // this.treeComponent.categorySatus = this.treeComponent.categorySatus;
    this.treeComponent.panelOpenState = true;
    this.treeComponent.categoryName = this.node.name;
    var path = "";
    var i = this.node.ancestors.length - 1;

    if (this.node.ancestors.length > 0) {
      this.node.ancestors.forEach(element => {
        path = path + element.name.toString();
        if (i >= 0) {
          path = path + " ➩ "
        }
      });
    }
    console.log(path);


    this.treeComponent.parentCategory = path + this.node.name;
    if (this.node.id != 0) {
      this.categoryService.find(this.node.id).subscribe(
        data => {
          this.treeComponent.selectedNode = new Category();
          this.treeComponent.selectedNode.id = data["id"];
          this.treeComponent.selectedNode.name = data["name"];
          this.treeComponent.selectedNode.description = data["description"];
          this.treeComponent.selectedNode.parent = data["parent"]
          this.treeComponent.selectedNodes.push(this.treeComponent.selectedNode);
        }
      );
    } else {
      this.treeComponent.selectedNode = new Category();
      this.treeComponent.selectedNode.id = 0;
      this.treeComponent.selectedNode.name = "Parent Category ";
    }
    if (this.hasLeaf() == true) {
      //this.snackBar.open("No product to show beacuse category has children , try to select leaves    ", "Close", { duration: 3000 })
    } //TODO 
    var id;
    if (this.node.id == 0) {
      id = undefined;
    } else {
      id = this.node.id;
    }


    this.selectNode();

    this.router.navigate(['category-management'], { queryParams: { categoryId: this.node.id, hasProducts: this.node.id == 0 ? false : this.treeComponent.hasProduct ,Path:this.treeComponent.parentCategory } });
  }

  clickExpandCollapse() {
    this.node.expand = !this.node.expand;
  }
  setInactive(inactive: boolean): boolean {
    return this.inactive = inactive;
  }
  getInactive(): boolean {
    return this.inactive;
  }
  hasLeaf(): boolean {
    return ((this.node.children != null) && (this.node.children.length > 0));
  }

  hasParent(): boolean {
    return ((this.node.ancestors != null) && (this.node.ancestors.length > 1));
  }
  getId(): number {
    return this.node.id;
  }

  getParentId(): number {
    if (this.node.ancestors != null && this.node.ancestors.length > 0) {
      return this.node.ancestors[this.node.ancestors.length - 1].id;
    } else {
      return this.node.id;
    }
  }


  getParentName(): string {
    if (this.node.ancestors != null && this.node.ancestors.length > 0) {
      return this.node.id + " " + "his parent :" + this.node.ancestors[this.node.ancestors.length - 1].name;
    } else {
      return "Parent Category";
    }
  }

  loadNode() {
    this.node.firstNode = this.firstNode;
    this.node.lastNode = this.lastNode;
    this.setInactive(this.node.inactive);
    if (this.node.children == null) {
      this.node.children = [];
    }

    if (this.node.ancestors == null) {
      this.node.ancestors = [];
    }
    if (this.parent != null) {
      for (let p of this.parent.ancestors) {
        if (!this.node.ancestors.filter(x => x.id === p.id)[0]) {
          this.node.ancestors.push(p);
        }
      }
      if (!this.node.ancestors.filter(x => x.id === this.parent.id)[0]) {
        this.node.ancestors.push(this.parent);
      }
    }


  }
}
