import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatTabGroup, MatTab } from '@angular/material';
import { TdVirtualScrollContainerComponent } from '@covalent/core'; 

@Component({
  selector: 'app-related-categories-and-products-dialog',
  templateUrl: './related-categories-and-products-dialog.component.html',
  styleUrls: ['./related-categories-and-products-dialog.component.css']
})
export class RelatedCategoriesAndProductsDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data:any) { }

  ngOnInit() {
  }

}
